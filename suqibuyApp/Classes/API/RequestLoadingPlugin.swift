//
//  RequestLoadingPlugin.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/8.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import Moya
import Result

class RequestLoadingPlugin: PluginType {
    
    var HUD:MBProgressHUD = MBProgressHUD.init()
    
    func willSend(_ request: RequestType, target: TargetType) {

        
        if let keyViewController = UIApplication.shared.keyWindow?.rootViewController {
            
            HUD.mode = MBProgressHUDModeCustomView
//            HUD.bezelView.color = UIColor.lightGray
//            HUD.labelText = "加载中...";
//            HUD.labelFont = UIFont.systemFont(ofSize: 12)
//            HUD.customView = uiacti
            HUD.removeFromSuperViewOnHide = true
//            HUD.backgroundView.style = .solidColor
            HUD = MBProgressHUD.showAdded(to: keyViewController.view, animated: true)
            
        }
    }
    
    func didReceive(_ result: Result<Response, MoyaError>, target: TargetType) {

        HUD.hide(true)

    }
}
