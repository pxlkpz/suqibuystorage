//
//  SuqibuyAPI.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/6.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import Moya

// MARK: - Provider support

let suqiBuyProvider = MoyaProvider<suqiBuy>(plugins: [RequestLoadingPlugin(), networkPlugin])

let networkPlugin = NetworkActivityPlugin { (type,change) in
    switch type {
    case .began:
        /// 状态栏转圈
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
    case .ended:
        /// 状态栏停止转圈
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

/// suqiBuy:网络请求
///
/// - homepage: 代购首页
/// - daigouCartitems:代购模块-购物车：物品列表
/// - daigouQuote:代购模块 - 购物车：结算中心
public enum suqiBuy {
    
    case homepage
    case transshipIndex
    
    case daigouCartitems
    case daigouQuote(data:Array<String>)
    case daigouBatchDelItem(data:Dictionary<String, Any>)
    case daigouDelitem(data:Dictionary<String, Any>)
    //--清单 ---
    case daigouList(data:Dictionary<String, Any>) //1 -> 1++
    case daigouDelete(data:Dictionary<String, Any>)
    case daigouCancel(data:Dictionary<String, Any>)
    case daigouBatchDelete(data:Dictionary<String, Any>)
    
    //--- 2 等待入库包裹
    case warehouseDelete(data:Dictionary<String, Any>)
    case warehouseBatchDelete(data:Dictionary<String, Any>)
    case warehousePackagelist(data:Dictionary<String, Any>)
    //--- 3 库存待寄送
    case transshipExpressitemsgroup(data:Dictionary<String, Any>)
    case transshipAddall
    //--- 4. 创建发货订单
    case transshipCartitems(data:Dictionary<String, Any>)
    case transshipQuote(data:Dictionary<String, Any>)
    case transshipUsecoupon(data:Dictionary<String, Any>)
    case transshipUsecredit(data:Dictionary<String, Any>)
    case transshipMycoupons(data:Dictionary<String, Any>)
    case transshipSaveorder(data:Dictionary<String, Any>)
    case transshipShippingmethods(data:Dictionary<String, Any>)
    case transshipInsurancepost(data:Dictionary<String, Any>)
    case transshipSelectaddress(data:Dictionary<String, Any>)
    case transshipSelectshippingmethod(data:Dictionary<String, Any>)
    
    //-- 发货订单
    case transshipOrderlist(data:Dictionary<String, Any>)
    case transshipDetail(data:Dictionary<String, Any>)
    case transshipCancel(data:Dictionary<String, Any>)
    case transshipPaymentpost(data:Dictionary<String, Any>)
    case transshipTrace(data:Dictionary<String, Any>)
    case transshipOrderDel(data:Dictionary<String, Any>)
    case transshipBatchDel(data:Dictionary<String, Any>)
    // -- 评论
    case transshipOrderWaitingReview(data:Dictionary<String, Any>)
    case transshipOrderReview(data:Dictionary<String, Any>)
    
    /// mine
    case userGetInfo
    case userLogout
    
    case softheleHelpList
    case softhelpHelpDetail
    case userSignIn
    case softhelpHelpinfo(data:Dictionary<String, Any>)

    

    case withdrawList(data:Dictionary<String, Any>)
    case withdrawSave(data:Dictionary<String, Any>)
    case withdrawIntro
    
    // 社区
    case groupAddPost(data:Dictionary<String, Any>)
    case groupList(data:Dictionary<String, Any>)
    case groupMe(data:Dictionary<String, Any>)

    case groupPostDetail(data:Dictionary<String, Any>)
    case groupAddStar(data:Dictionary<String, Any>)
    case groupAddComment(data:Dictionary<String, Any>)
    case groupAddCommentStar(data:Dictionary<String, Any>)

    case uploadImage(data:Dictionary<String, Any>)
    
    //min
    case softhelpAsk(data:Dictionary<String, Any>)
    
}


extension suqiBuy : TargetType {
    public var baseURL: URL { return URL(string: API_MAIN_URL)! }
    public var path: String {
        switch self {
            
        case .homepage:
            return "/homepage/index"
        case .transshipIndex :
            return "/transship/index"
            
        case .daigouCartitems:
            return "/daigou/cartitems"
        case .daigouQuote:
            return "/daigou/quote"
        case .daigouBatchDelItem:
            return "/daigou/batchDelItem"
        case .daigouDelitem:
            return "/daigou/delitem"
        /// 清单---
        case .daigouList:
            return "/daigou/list"
        case .daigouDelete:
            return "/daigou/delete"
        case .daigouCancel:
            return "/daigou/cancel"
        case .daigouBatchDelete:
            return "/daigou/batchDelete"
            
            
        /// 等待入库
        case .warehouseDelete( _):
            return "/warehouse/delete"
        case .warehouseBatchDelete( _):
            return "/warehouse/batchDelete"
        case .warehousePackagelist( _):
            return "/warehouse/packagelist"
            
        case .transshipAddall:
            return "/transship/addall"
        case .transshipExpressitemsgroup( _):
            return "/transship/expressitemsgroup"
            
        case .transshipCartitems:
            return "/transship/cartitems"
        case .transshipQuote( _):
            return "/transship/quote"
        case .transshipUsecoupon( _):
            return "/transship/usecoupon"
        case .transshipUsecredit( _):
            return "/transship/usecredit"
        case .transshipMycoupons(_):
            return "/transship/mycoupons"
        case .transshipSaveorder( _):
            return "/transship/saveorder"
        case .transshipShippingmethods( _):
            return "/transship/shippingmethods"
        case .transshipInsurancepost( _):
            return "/transship/insurancepost"
        case .transshipSelectaddress( _):
            return "/transship/selectaddress"
        case .transshipSelectshippingmethod( _):
            return "/transship/selectshippingmethod"
            
            
        case .transshipOrderlist(_ ):
            return "/transship/orderlist"
            
        case .transshipDetail(_):
            return "/transship/detail"
        case .transshipCancel(_):
            return "/transship/cancel"
        case .transshipPaymentpost(_):
            return "/transship/paymentpost"
        case .transshipTrace(_):
            return "/transship/trace"
        case .transshipOrderDel(_):
            return "/transship/orderDel"
        case .transshipBatchDel(_):
            return "/transship/batchDel"
            
        case .transshipOrderWaitingReview:
            return "/transship/orderWaitingReview"
        case .transshipOrderReview(_):
            return "/transship/orderReview"
            
        case .userGetInfo:
            return "/user/get_info"
        case .userLogout:
            return "/user/logout"
            
        case .groupAddPost:
            return "/group/addPost"
        case .groupList:
            return "/group/list"
        case .groupMe:
            return "/group/me"
        case .userSignIn:
            return "/user/signIn"

        case .groupPostDetail:
            return "/group/postDetail"
        case .groupAddStar:
            return "/group/addStar"
        case .groupAddComment:
            return "/group/addComment"
        case .groupAddCommentStar:
            return "/group/addCommentStar"

        case .uploadImage:
            return "/group/uploadImage"
            
        case .softheleHelpList:
            return "/softhelp/helpList"
            
        case .softhelpHelpDetail:
            return "softhelp/helpDetail"
        
        case .withdrawList:
            return "withdraw/list"
        case .withdrawSave:
            return "withdraw/save"
        case .withdrawIntro:
            return "withdraw/intro"
            
        case .softhelpHelpinfo:
            return "softhelp/helpinfo"
        case .softhelpAsk:
            return "/softhelp/ask"
        }
    }
    
    public var method: Moya.Method {
        return .post
    }
    public var parameters: [String: Any]? {
        switch self {
            
        default:
            return nil
        }
    }
    public var task: Task {
        switch self {
        case .daigouCartitems, .transshipAddall, .userGetInfo, .userLogout, .userSignIn, .withdrawIntro: //不需要穿值 已经登录
            return .requestParameters(parameters: SuqibuyAPIDataProcessing.getUserTokon(), encoding: URLEncoding.default)
        case let .daigouDelitem(datas), let .daigouBatchDelItem(datas),
             let .daigouList(datas),let .daigouDelete(datas),
             let .daigouCancel(datas),let .daigouBatchDelete(datas),
             let .warehouseDelete(datas),let .warehouseBatchDelete(datas),let .warehousePackagelist(datas),
             let .transshipExpressitemsgroup(datas) , let .softhelpHelpinfo(datas),
             let .transshipQuote(datas), let .transshipUsecoupon(datas),
             let .transshipUsecredit(datas), let .transshipMycoupons(datas),
             let .transshipSaveorder(datas),let .transshipShippingmethods(datas),
             let .transshipInsurancepost(datas),let .transshipSelectaddress(datas),
             let .transshipSelectshippingmethod(datas),
             
             let .transshipOrderlist(datas),let .transshipCancel(datas),
             let .transshipDetail(datas),let .transshipPaymentpost(datas),
             let .transshipTrace(datas),let .transshipOrderDel(datas),
             let .transshipBatchDel(datas), let .transshipOrderReview(datas),
             let .transshipOrderWaitingReview(datas), let .transshipCartitems(datas),
             let .groupList(datas),let .groupMe(datas), let .groupAddPost(datas), let .groupPostDetail(datas),
             let .groupAddStar(datas), let .groupAddComment(datas), let .withdrawList(datas), let .withdrawSave(datas),
            let .softhelpAsk(datas), let .groupAddCommentStar(datas):
            
            return .requestParameters(parameters: requestTaskDispose(datas), encoding: URLEncoding.default)
            
            
        case let .uploadImage(datas):// 上传图片
            let formData4 = MultipartFormData(provider: .data(datas["pics"] as! Data), name: "file",fileName: "h.png", mimeType: "image/png")
            let multipartData = [formData4]
            
            return .uploadCompositeMultipart(multipartData, urlParameters: SuqibuyAPIDataProcessing.getUserTokon())
            
        default: //不需要登录
            return .requestParameters(parameters: SuqibuyAPIDataProcessing.getToken(), encoding: URLEncoding.default)
        }
        
    }
    
    func requestTaskDispose(_ datas:Dictionary<String, Any>) -> Dictionary<String,Any>{
        var dic = SuqibuyAPIDataProcessing.getUserTokon()
        for e in datas {
            dic[e.key] = e.value
        }
        return dic
        
    }
    
    public var headers: [String : String]? {
        return nil
    }
    public var sampleData: Data {
        return "{}".data(using: String.Encoding.utf8)!
    }
    
    
}

