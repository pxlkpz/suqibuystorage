//
//  SuqibuyAPIDataProcessing.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/21.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Moya

class SuqibuyAPIDataProcessing {

    class func getToken() -> Dictionary<String, Any>{
        
        var tokenString = "IphoneOtherEquipment"
        if UIScreen.main.scale > 1 {
            if kScreenH == 480 {
                tokenString = "IWantToGetAIphone6"
            } else if kScreenH == 568 {
                tokenString = "IphoneRetinaEquipment"
            }
        }
        return ["api_token":tokenString]
    }
    
    class func getUserTokon() -> Dictionary<String, Any>{
        var dic = getToken()

        if let model = SQUser.loadmodel() {
            dic["user_token"] = model.user_token
            return dic;
        }
        dic["user_token"] = ""
        return dic
    }
}
