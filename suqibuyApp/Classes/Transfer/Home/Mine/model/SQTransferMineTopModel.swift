//
//  SQTransferMineTopModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/23.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ObjectMapper

struct SQTransferMineTopModel: Mappable {
    var topImageName: String?
    var bottomTitle: String?
    var tag: NSInteger?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        topImageName <- map["topImageName"]
        bottomTitle <- map["bottomTitle"]
        tag <- map["tag"]
    }
}
