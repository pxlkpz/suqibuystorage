
//
//  SQTransferMineController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/6.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
class SQTransferMineController: SQBaseViewController {
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQTransferMineTopCell", "SQDaigouTopCell", "SQDaigouCommonTransitCell"])
    private let viewModel = SQTransferManagerViewModel()
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self.view)
        }
        
        
        self.listTableView.separatorInset = .zero
        self.listTableView.layoutMargins = .zero
        
        setupBindings()
        
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
    }
    func resetUI() {
        self.listTableView.reloadData()
    }
}

extension SQTransferMineController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section < 2 {
            return 1
        }
        return self.viewModel.dataListVariable.value.transships?.count ?? 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 159
        } else if indexPath.section == 1 {
            return 175
        } else {
            return 103
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 10
        } else if section == 2 {
            return 63
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1 {
            return 10
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            let headView:SQTransferCommonTransitTopView = UIView.instanceFromNib("SQTransferCommonTransitTopView") as! SQTransferCommonTransitTopView
            //        headView.selectButton.rx.tap
            //            .subscribe{ [weak self] event in
            //
            //            }
            //            .disposed(by: disposeBag)
            return headView
        }
        return UIView()
    }
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cellIdentifier = "SQTransferMineTopCell"
            let gouwuViewCell:SQTransferMineTopCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQTransferMineTopCell
            gouwuViewCell.setData()
            gouwuViewCell.tag = 1;
            return gouwuViewCell
            
        } else if indexPath.section == 1 {
            let cellIdentifier = "SQDaigouTopCell"
            let topViewCell : SQDaigouTopCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouTopCell
            topViewCell.setData(viewModel.dataListVariable.value)
            return topViewCell
        } else if indexPath.section == 2 {
            let cellIdentifier = "SQDaigouCommonTransitCell"
            let gouwuViewCell: SQDaigouCommonTransitCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouCommonTransitCell
            gouwuViewCell.setData(viewModel.dataListVariable.value.transships![indexPath.row])
            return gouwuViewCell
        }
        return UITableViewCell()
    }

    
}

