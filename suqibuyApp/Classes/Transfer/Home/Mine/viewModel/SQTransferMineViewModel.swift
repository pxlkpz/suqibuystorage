//
//  SQTransferMineViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/23.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ObjectMapper

class SQTransferMineViewModel {

    
    class func getData() -> [SQTransferMineTopModel] {
        let dataArray : Array = NSMutableArray(contentsOfFile: Bundle.main.path(forResource: "SQTransferMineTopList", ofType: "plist")!)! as Array
        let dataModelArray : [SQTransferMineTopModel] = Mapper<SQTransferMineTopModel>().mapArray(JSONArray: dataArray as! [[String : Any]])
        return dataModelArray
    }
    
    
}
