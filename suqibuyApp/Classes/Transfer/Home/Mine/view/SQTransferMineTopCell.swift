//
//  SQTransferMineTopCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/23.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

/// 按钮类型
///
/// - suqibuy: 仓库
/// - yubao: 预报
/// - liucheng: 流程
/// - xianzhi: 限制
/// - free: 免费
/// - gusuan: 估算
enum zhuanYunMineButtonType : Int {
    case suqibuy = 0
    case yubao
    case liucheng
    case xianzhi
    case free
    case gusuan
    
}


/// button 类型
///
/// - waitRuku: 等待入库
/// - waitSend: 等待寄送
/// - newOrder: 新建订单
/// - orderList: 订单列表
/// - shouhou: 售后
/// - pinglun: 评论
enum zhuanYunManagerButtonType : Int {
    case waitRuku = 0
    case waitSend
    case newOrder
    case orderList
    case shouhou
    case pinglun
    
}


class SQTransferMineTopCell: UITableViewCell {
    
    @IBOutlet var buttonArray: [SQUpDownButtonTwo]!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    @IBAction func ButtonAction(_ sender: UIButton) {

        if self.tag == 1 {
            let actionType:zhuanYunMineButtonType = zhuanYunMineButtonType(rawValue: sender.tag)!
            switch actionType {
            case .suqibuy:
                pushNewController(SQSuqibuyStorageAddriessVC())
                break
            case .yubao:
                pushNewController(SQdaishouForecastVC())
                break
            case .liucheng:
                pushNewController(SQdaishouFlowVC())
                break
            case .xianzhi:
            let controller = SQConfineController()
            controller.title = "转运物品限制"
            pushNewController(controller)
                break
            case .free:
                pushNewController(SQFreeRuleController())
                break
                
            case .gusuan:
                pushNewController(SQzhuanyunFeeVC())
                break
            }

        } else {
            let actionType:zhuanYunManagerButtonType = zhuanYunManagerButtonType(rawValue: sender.tag)!
            switch actionType{
            case .waitRuku:
                pushNewController(SQMangerWaitPutInStorageController())
                break
            case .waitSend:
                pushNewController(SQMangeWaitingSendController())
                break
            case .newOrder:
                pushNewController(SQMangerNewDeliveryListController())
                break
            case .orderList:
                pushNewController(SQMangeConsignmentOrderController())
                break
            case .shouhou:
                break
            case .pinglun:
                pushNewController(SQMangeWaitCommentsController())
                break
            }
            
        }
        
    }
    
    func setData() {
        let dataArray = SQTransferMineViewModel.getData()
        for button in buttonArray {
            let model = dataArray[button.tag]
            
            button.setImage(UIImage(named: model.topImageName!), for: .normal)
            button.setTitle(model.bottomTitle, for: .normal)
        }
    }
    
    func setDataInManager() {
        let dataArray = SQTransferManagerViewModel.getData()
        for button in buttonArray {
            let model = dataArray[button.tag]
            
            button.setImage(UIImage(named: model.topImageName!), for: .normal)
            button.setTitle(model.bottomTitle, for: .normal)
        }
    }
    
    func pushNewController(_ viewController: UIViewController)  {
        if SQUser.loginWitchController(self.viewController()) {return}

        viewController.hidesBottomBarWhenPushed = true
        self.viewController().navigationController?.pushViewController(viewController, animated: true);
        
    }
    
}

