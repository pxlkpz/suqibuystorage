//
//  SQTransferMainViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/6.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQTransferMainViewController: SQBaseViewController,CPMainTitleViewDelelgate, CPMainTitleViewDataSource {
    
    fileprivate lazy var mainTitleView : CPMainTitleView = {[weak self] in
        let titleView = CPMainTitleView()
        titleView.dataSource = self
        titleView.delegate = self
        return titleView
        }()
    
    fileprivate var leftController : SQTransferMineController = SQTransferMineController()
    fileprivate var rightController : SQTransferManagerController = SQTransferManagerController()
    fileprivate var currentViewController : UIViewController = UIViewController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = self.mainTitleView
        
        self.addChildViewController(leftController)
        leftController.view.frame = self.view.bounds
        self.view.addSubview(leftController.view)
        
        self.addChildViewController(rightController)
        rightController.view.frame = self.view.bounds
        currentViewController = leftController
    }
    
    func naviSwitch(for open:Bool) {
        if (((self.currentViewController as? SQTransferMineController) != nil)   && open == true) || (((self.currentViewController as? SQTransferManagerController) != nil) && open == false) {
            return
        }
        let oldController = self.currentViewController
        if open == false {
            self.transition(from: self.currentViewController, to: self.rightController, duration: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: nil, completion: { (finished) in
                if finished {
                    self.currentViewController = self.rightController
                } else {
                    self.currentViewController = oldController
                }
            })
        } else {
            self.transition(from: self.currentViewController, to: self.leftController, duration: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: nil, completion: { (finished) in
                if finished {
                    self.currentViewController = self.leftController
                } else {
                    self.currentViewController = oldController
                }
            })

        }
    }
    
    func titles(for titleView: CPMainTitleView!) -> [Any]! {
        return ["我的转运", "发货管理"]
    }
    
    func titleView(_ titleView: CPMainTitleView!, didSelectTitleAt index: Int) {
        if index == 0 {
            naviSwitch(for: true)
        } else {
            naviSwitch(for: false)
        }
    }
    
}

