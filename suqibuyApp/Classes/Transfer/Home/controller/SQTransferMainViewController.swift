//
//  SQTransferMainViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/6.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
class SQTransferMainViewController: SQBaseViewController{
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQTransferMineTopCell", "SQDaigouTopCell", "SQDaigouCommonTransitCell", "SQDaigouCommonTransitCell5SE"])
    private let viewModel = SQTransferMainViewModel()
    private let disposeBag = DisposeBag()

    
    fileprivate lazy var mainTitleView : CPMainTitleView = {[weak self] in
        let titleView = CPMainTitleView()
        titleView.dataSource = self
        titleView.delegate = self
        return titleView
        }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = self.mainTitleView
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self.view)
        }
        self.listTableView.separatorInset = .zero
        self.listTableView.layoutMargins = .zero
        
        setupBindings()

    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
    }
    
    func resetUI() {
        self.listTableView.reloadData()
    }
    
    func naviSwitch(for open:Bool) {
        if open { //我的转运
            self.viewModel.editSelectMode = .normalMode
            
        } else {
            self.viewModel.editSelectMode = .editMode
        }
        resetUI()
    }
    

    
    
}

extension SQTransferMainViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section < 2 {
            return 1
        }
        return self.viewModel.dataListVariable.value.transships?.count ?? 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 159
        } else if indexPath.section == 1 {
            return 175
        } else if indexPath.section == 3{
            if DeviceSzie.currentSize() == .iphone5 {
                return 125
            }
            
            return 103
        } else {
            return 103

        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 10
        } else if section == 2 {
            return 63
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1 {
            return 10
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            let headView:SQTransferCommonTransitTopView = UIView.instanceFromNib("SQTransferCommonTransitTopView") as! SQTransferCommonTransitTopView
            return headView
        }
        return UIView()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cellIdentifier = "SQTransferMineTopCell"
            let gouwuViewCell:SQTransferMineTopCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQTransferMineTopCell
            if self.viewModel.editSelectMode == .normalMode {
                gouwuViewCell.tag = 1
                gouwuViewCell.setData()
            } else {
                gouwuViewCell.tag = 10
                gouwuViewCell.setDataInManager()
            }
            
            return gouwuViewCell
            
        } else if indexPath.section == 1 {
            let cellIdentifier = "SQDaigouTopCell"
            let topViewCell : SQDaigouTopCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouTopCell
            topViewCell.setData(viewModel.dataListVariable.value)
            return topViewCell
        } else if indexPath.section == 2 {
            
            var cellIdentifier = "SQDaigouCommonTransitCell"
            
            if DeviceSzie.currentSize() == .iphone5 {
                cellIdentifier = "SQDaigouCommonTransitCell5SE"
                let gouwuViewCell: SQDaigouCommonTransitCell5SE = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouCommonTransitCell5SE
                gouwuViewCell.setData(viewModel.dataListVariable.value.transships![indexPath.row])
                return gouwuViewCell

            }
            let gouwuViewCell: SQDaigouCommonTransitCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouCommonTransitCell
            gouwuViewCell.setData(viewModel.dataListVariable.value.transships![indexPath.row])
            return gouwuViewCell
        }
        return UITableViewCell()
    }
    
    
}

extension SQTransferMainViewController : CPMainTitleViewDelelgate, CPMainTitleViewDataSource{
    
    func titles(for titleView: CPMainTitleView!) -> [Any]! {
        return ["我的转运", "发货管理"]
    }
    
    func titleView(_ titleView: CPMainTitleView!, didSelectTitleAt index: Int) {
        if index == 0 {
            naviSwitch(for: true)
        } else {
            naviSwitch(for: false)
        }
    }
}



