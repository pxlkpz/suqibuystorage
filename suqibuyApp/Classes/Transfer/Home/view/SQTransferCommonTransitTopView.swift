//
//  SQTransferCommonTransitTopView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/7.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

class SQTransferCommonTransitTopView: UIView {
    
    @IBOutlet weak var selectButton: UIButton!
    @IBAction func selectButtonAction(_ sender: Any) {
        let webController = CommonWebViewController.init(urlStr: Web_helpDetail38, andNavTitle: "包裹产生问题如何赔偿")
        webController?.hidesBottomBarWhenPushed = true
        self.viewController().navigationController?.pushViewController(webController!, animated: true)

    }
}
