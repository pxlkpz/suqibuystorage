//
//  SQSuqibuyStorageAddriessVC.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQSuqibuyStorageAddriessVC.h"
#import "SuqibuyStorageAddriessModel.h"
#import "MBZPKTextField.h"
#import "MBZPKTextView.h"
#import <ChameleonFramework/Chameleon.h>
#import "SQSuqibuyStorageAddriessView.h"
@interface SQSuqibuyStorageAddriessVC ()
{
    
    MBZPKTextView *_tfAddress;
    MBZPKTextField *_tfMobile;
    MBZPKTextField *_tfTitle;
    MBZPKTextField *_tfZipcode;
    
}
@end

@implementation SQSuqibuyStorageAddriessVC

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"Suqibuy仓库地址";
    
    [self buildUI];
    
    [self requst];
}

#pragma mark - ---- lefe cycle 🍏🌚

#pragma mark - ---- event response 🍐🌛
-(void)buttonAction:(UIButton*)sender{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    switch (sender.tag) {
        case 1:
        {
            showDefaultPromptTextHUD(@"收件人复制成功");
            pasteboard.string = _tfTitle.text;
            
        }
            break;
        case 2:
        {
            showDefaultPromptTextHUD(@"收货地址复制成功");
            pasteboard.string = _tfAddress.text;
            
        }
            break;
        case 3:
        {
            showDefaultPromptTextHUD(@"邮政编码复制成功");
            pasteboard.string = _tfZipcode.text;
            
        }
            break;
        case 4:
        {
            showDefaultPromptTextHUD(@"联系手机复制成功");
            pasteboard.string = _tfMobile.text;
            
        }
            break;
            
        default:
            break;
    }
    
}
#pragma mark - ---- private methods 🍊🌜


-(void)buildUI{
    
    SQSuqibuyStorageAddriessView *topView = [[SQSuqibuyStorageAddriessView alloc] initWithNibName:@"SQSuqibuyStorageAddriessView"];
    [self.view addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(26.5);
    }];
    
    _tfTitle = [self creatCellUI:@"收货人" andTopMASViewAttribute:topView.mas_bottom andTopNumber:30 andTag:1] ;
    
    _tfAddress = [self creatCellINTextViewUI:@"收货地址" andTopMASViewAttribute:_tfTitle.mas_bottom andTopNumber:55.5 andTag:2];
    
    _tfZipcode = [self creatCellUI:@"邮政编码" andTopMASViewAttribute:_tfAddress.mas_bottom andTopNumber:25 andTag:3];
    
    _tfMobile = [self creatCellUI:@"联系手机" andTopMASViewAttribute:_tfZipcode.mas_bottom andTopNumber:25 andTag:4];
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = KColorFromRGB(0x999999);
    label1.text = @"提示：一定要写我们提供收件人姓名，\n否则包裹无法入库";
    label1.numberOfLines = 0;
    label1.textAlignment = NSTextAlignmentLeft;
    [label1 setFont:[UIFont systemFontOfSize:12]];
    [label1 sizeToFit];
    [self.view addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_tfTitle).offset(0);
        make.top.equalTo(_tfTitle.mas_bottom).offset(4.5);
    }];
    
    UILabel *label2 = [[UILabel alloc] init];
    label2.textColor = [UIColor colorWithHexString:@"#333333"];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"特别提醒：购物完成，卖家给出了物流单号；下一步在“转运预报”里面填写你的验货要求，仓库收到你的包裹之后会按照你填写的要求验货；如果不填写“转运预报”系统默认不需要验货；在你收到包裹之后，如果出现了商品缺失的情况我们是无法提供赔偿服务的；建议一定要填写“转运预报”，尤其是贵重物品；"];
    [attributedString addAttributes:@{
                                      NSFontAttributeName: [UIFont systemFontOfSize:13.0f],
                                      NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#F10808"]
                                      } range:NSMakeRange(0, 5)];
    label2.attributedText = attributedString;
    label2.numberOfLines = 0;//根据最大行数需求来设置
    
    label2.lineBreakMode = NSLineBreakByCharWrapping;

    label1.textAlignment = NSTextAlignmentLeft;
    [label2 setFont:[UIFont systemFontOfSize:12]];
    [label2 sizeToFit];
    [self.view addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-16);
        make.left.equalTo(self.view).offset(16);
        make.top.equalTo(_tfMobile.mas_bottom).offset(49);

    }];

    
}

-(void)showData:(SuqibuyStorageAddriessModel*)model{
    _tfTitle.text = model.title;
    _tfMobile.text = model.mobile;
    _tfZipcode.text = model.zipcode;
    _tfAddress.text = model.address;
}

-(__kindof MBZPKTextField*)creatCellUI:(NSString*)Str andTopMASViewAttribute:(MASViewAttribute *)viewAttribute andTopNumber:(NSInteger)num andTag:(NSInteger)tag{
    
    MBZPKTextField *textfield = [[MBZPKTextField alloc] init];
    [textfield setFont:[UIFont systemFontOfSize:14]];
    textfield.textColor =[UIColor colorWithHexString:@"#666666"];
    [textfield setTextAlignment:NSTextAlignmentLeft];
    [self.view addSubview:textfield];
    textfield.enabled = NO;
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-72);
        make.left.equalTo(self.view).offset(83);
        make.top.equalTo(viewAttribute).offset(num);
        make.height.equalTo(@35);
    }];
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:@"#333333"];
    label1.text = Str;
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [label1 sizeToFit];
    [self.view addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-10.5);
        make.centerY.equalTo(textfield);
    }];
    
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"复制" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    button.layer.cornerRadius = allCornerRadius;
    button.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleDiagonal withFrame:CGRectMake(0, 0, 60, 60) andColors:@[[UIColor colorWithHexString:@"#FF7F02"], [UIColor colorWithHexString:@"#FFA902"]]];

    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = tag;
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(textfield);
        make.right.equalTo(self.view.mas_right).offset(-15.5);
        make.left.equalTo(textfield.mas_right).offset(10.5);
        make.bottom.equalTo(textfield.mas_bottom);
    }];
    
    return textfield;
    
}


-(__kindof MBZPKTextView*)creatCellINTextViewUI:(NSString*)Str andTopMASViewAttribute:(MASViewAttribute *)viewAttribute andTopNumber:(NSInteger)num andTag:(NSInteger)tag{
    
    MBZPKTextView *textfield = [[MBZPKTextView alloc] init];
    [textfield setFont:[UIFont systemFontOfSize:14]];
    textfield.userInteractionEnabled = NO;
    textfield.textColor =[UIColor colorWithHexString:@"#666666"];
    [textfield setTextAlignment:NSTextAlignmentLeft];
    [self.view addSubview:textfield];
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-72);
        make.left.equalTo(self.view).offset(83);
        make.top.equalTo(viewAttribute).offset(num);
        make.height.equalTo(@50);
    }];
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:@"#333333"];
    label1.text = Str;
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [label1 sizeToFit];
    [self.view addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-10.5);
        make.centerY.equalTo(textfield);
    }];

    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"复制" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    button.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleDiagonal withFrame:CGRectMake(0, 0, 60, 60) andColors:@[[UIColor colorWithHexString:@"#FF7F02"], [UIColor colorWithHexString:@"#FFA902"]]];
    button.layer.cornerRadius = allCornerRadius;
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = tag;
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(textfield);
        make.right.equalTo(self.view.mas_right).offset(-15.5);
        make.left.equalTo(textfield.mas_right).offset(10.5);
        make.bottom.equalTo(textfield.mas_bottom);
    }];
    
    return textfield;
}

-(void)requst{
    [self showLoadingHUD];
    NSDictionary *dic = @{@"user_token":[SQUser loadmodel].user_token};
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAISHOU_Suqiaddress WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                
                NSDictionary *result = dic[@"result"];
                SuqibuyStorageAddriessModel *model = [SuqibuyStorageAddriessModel mj_objectWithKeyValues:result];
                [self showData:model];
                
                
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}
#pragma mark - ---- delegate 🍎🌝

#pragma mark - ---- getters and setters 🍋🌞


@end
