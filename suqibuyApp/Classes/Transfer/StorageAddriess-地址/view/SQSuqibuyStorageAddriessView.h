//
//  SQSuqibuyStorageAddriessView.h
//  suqibuyApp
//
//  Created by pxl on 2018/2/6.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

#import "SQBaseXiBView.h"

@interface SQSuqibuyStorageAddriessView : SQBaseXiBView
- (IBAction)selectButtonAction:(id)sender;

@end
