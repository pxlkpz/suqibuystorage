//
//  SuqibuyStorageAddriessModel.h
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SuqibuyStorageAddriessModel : NSObject

//result =     {
//    address = "\U4e0a\U6d77\U5e02\U9752\U6d66\U533a\U5f90\U6cfe\U9547\U53cc\U8054\U8def375\U53f7B8\U5e62\U697c303\U5ba4";
//    "credits_amount" = 0;
//    mobile = 15800863453;
//    title = "\U674e\U9707";
//    zipcode = 201702;
//};
/** 地址 */
@property (nonatomic, copy) NSString* address;
/** 号码 */
@property (nonatomic, copy) NSString* mobile;
/** 标题 */
@property (nonatomic, copy) NSString* title;
/** 编号 */
@property (nonatomic, copy) NSString* zipcode;
@end
