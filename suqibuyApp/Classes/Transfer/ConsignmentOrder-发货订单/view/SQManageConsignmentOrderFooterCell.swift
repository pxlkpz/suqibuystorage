//
//  SQManageConsignmentOrderFooterCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/7.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ChameleonFramework
import ObjectMapper
class SQManageConsignmentOrderFooterCell: SQBaseSwiftTableCell {

    @IBOutlet weak var zhuizongButton: UIButton!
    @IBOutlet weak var chakanButton: UIButton!
    
    public var commonItemModel : SQMangeConsignmentOrderItem? {
        didSet {
//            guard let `commonItemModel` = commonItemModel else { return }

            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

        zhuizongButton.layer.cornerRadius = 10
        zhuizongButton.layer.masksToBounds = true
        zhuizongButton.layer.borderWidth = 1
        zhuizongButton.layer.borderColor = HexColor("#666666")?.cgColor

        
        
        chakanButton.layer.cornerRadius = 10
        chakanButton.layer.masksToBounds = true
        chakanButton.layer.borderWidth = 1
        chakanButton.layer.borderColor = HexColor("#666666")?.cgColor
    }
    
    @IBAction func chakanButtonAction(_ sender: Any) {

        if self.commonItemModel != nil {
            self.selectClosure!("buttonaction")

            let vc = SQzhuanyunListOrderDetails()
            let dic = self.commonItemModel?.toJSON()
            vc.modelDic = NSDictionary(dictionary: dic!) as! [AnyHashable : Any]
            self.viewController().navigationController?.pushViewController(vc, animated: true)

        }

    }
    
    @IBAction func zhuizongButtonAction(_ sender: Any) {
        self.selectClosure!("buttonaction")

        let urlString = API_MAIN_URL_HTML + "tracking/transorder?order_no=" + (self.commonItemModel?.packageNo)!
        let webController = CommonWebViewController.init(urlStr: urlString, andNavTitle: "发货订单追踪查询")
        self.viewController().navigationController?.pushViewController(webController!, animated: true)
        
    }

    
}
