//
//  SQMangeConsignmentOrderCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/7.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQMangeConsignmentOrderCell: UITableViewCell {
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var transferFeeLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var expressNo: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
 
    @IBOutlet weak var selectButton: UIButton!
    
    
    @IBOutlet weak var lastHistoryLabel: UILabel!
    
    
    @IBOutlet weak var topVIewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topVIewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var leftImageLeftConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }


    
    func setData(_ model:SQMangeConsignmentOrderItem, mode: SQViewEditMode) {
        statusLabel.text = model.statusLabel
        expressNo.text = (model.shippingMethodName ?? "")+" "+(model.packageNo ?? "")

        
        leftImageView.sd_setImage(with:  URL(string: model.thumbnail!), placeholderImage: UIImage(named: "zhuanyun"))
        locationLabel.text = model.shippingAddressCountry
        weightLabel.text = model.weight
        transferFeeLabel.text = (model.agencyFee! as NSString).gtm_stringByUnescapingFromHTML()
        typeLabel.text = "转运方式："+(model.shippingMethodName ?? "")
        timeLabel.text = model.shippingMethodDescription
        lastHistoryLabel.text = "最新包装状态：" + (model.lastHistory ?? "")
        if mode == .normalMode {
            selectButton.isHidden = true
            leftImageLeftConstraint.constant = 15
            topVIewLeftConstraint.constant = 0
            topVIewRightConstraint.constant = 0
        } else {
            selectButton.isHidden = false
            selectButton.isSelected = model.isCanDelete
            leftImageLeftConstraint.constant = 46
            topVIewLeftConstraint.constant = 31
            topVIewRightConstraint.constant = -31

        }
    }
}
