//
//	SQGood.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQMangeConsignmentOrderGood : Mappable{

    var detail : String?
    var id : String?
    var itemCode : String?
    var itemName : String?
    var itemParams : String?
    var lastHistory : AnyObject?
    var qty : String?
    var shippingAddress : String?
    var shippingAddressCountry : String?
    var shippingMethod : String?
    var shippingMethodDescription : String?
    var shippingMethodName : String?
    var size : String?
    var thumbnail : String?
    var weight : String?


	func mapping(map: Map)
	{
        detail <- map["detail"]
        id <- map["id"]
        itemCode <- map["item_code"]
        itemName <- map["item_name"]
        itemParams <- map["item_params"]
        lastHistory <- map["last_history"]
        qty <- map["qty"]
        shippingAddress <- map["shipping_address"]
        shippingAddressCountry <- map["shipping_address_country"]
        shippingMethod <- map["shipping_method"]
        shippingMethodDescription <- map["shipping_method_description"]
        shippingMethodName <- map["shipping_method_name"]
        size <- map["size"]
        thumbnail <- map["thumbnail"]
        weight <- map["weight"]
		
	}

   
    required init?(map: Map) {
        
    }}
