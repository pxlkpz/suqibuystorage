//
//	SQItem.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQMangeConsignmentOrderItem :  Mappable{

    var agencyFee : String?
    var applyValue : String?
    var canCancel : Bool?
    var canPayment : Bool?
    var completeAt : AnyObject?
    var couponCode : String?
    var couponDiscountAmount : String?
    var createdAt : String?
    var creditsAmount : String?
    var creditsPointTotal : String?
    var customerNote : String?
    var goods : [SQMangeConsignmentOrderGood]?
    var grandTotal : String?
    var id : String?
    var insuranceAmount : String?
    var insuranceFee : String?
    var needIncharge : Bool?
    var packageNo : String?
    var paidAt : AnyObject?
    var paidTotal : String?
    var shippingAddress : String?
    var shippingAddressCountry : String?
    var shippingAt : AnyObject?
    var shippingMethod : String?
    var shippingMethodDescription : String?
    var shippingMethodName : String?
    var shippingNo : AnyObject?
    var sizeTotal : String?
    var status : String?
    var statusLabel : String?
    var subTotal : String?
    var thumbnail : String?
    var totalQty : String?
    var transferNo : AnyObject?
    var userRemainCash : String?
    var usergroupDiscountAmount : String?
    var waitingPaymentTotal : String?
    var weight : String?
    var lastHistory : String?
    var isCanDelete : Bool = false //自定义

    

	func mapping(map: Map)
	{
        agencyFee <- map["agency_fee"]
        applyValue <- map["apply_value"]
        canCancel <- map["can_cancel"]
        canPayment <- map["can_payment"]
        completeAt <- map["complete_at"]
        couponCode <- map["coupon_code"]
        couponDiscountAmount <- map["coupon_discount_amount"]
        createdAt <- map["created_at"]
        creditsAmount <- map["credits_amount"]
        creditsPointTotal <- map["credits_point_total"]
        customerNote <- map["customer_note"]
        goods <- map["goods"]
        grandTotal <- map["grand_total"]
        id <- map["id"]
        insuranceAmount <- map["insurance_amount"]
        insuranceFee <- map["insurance_fee"]
        needIncharge <- map["need_incharge"]
        packageNo <- map["package_no"]
        paidAt <- map["paid_at"]
        paidTotal <- map["paid_total"]
        shippingAddress <- map["shipping_address"]
        shippingAddressCountry <- map["shipping_address_country"]
        shippingAt <- map["shipping_at"]
        shippingMethod <- map["shipping_method"]
        shippingMethodDescription <- map["shipping_method_description"]
        shippingMethodName <- map["shipping_method_name"]
        shippingNo <- map["shipping_no"]
        sizeTotal <- map["size_total"]
        status <- map["status"]
        statusLabel <- map["status_label"]
        subTotal <- map["sub_total"]
        thumbnail <- map["thumbnail"]
        totalQty <- map["total_qty"]
        transferNo <- map["transfer_no"]
        userRemainCash <- map["user_remain_cash"]
        usergroupDiscountAmount <- map["usergroup_discount_amount"]
        waitingPaymentTotal <- map["waiting_payment_total"]
        weight <- map["weight"]
        lastHistory <- map["last_history"]
	}

    
    required init?(map: Map) {
        
    }
}
