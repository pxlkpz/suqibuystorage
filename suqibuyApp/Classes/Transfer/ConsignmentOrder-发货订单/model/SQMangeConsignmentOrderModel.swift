//
//	SQModel.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQMangeConsignmentOrderModel : Mappable{

	var currentPage : Int?
	var items : [SQMangeConsignmentOrderItem]?
	var page : Int?
	var pageNum : Int?
	var totalPages : Int?
	var totalResults : Int?


	func mapping(map: Map)
	{
		currentPage <- map["current_page"]
		items <- map["items"]
		page <- map["page"]
		pageNum <- map["page_num"]
		totalPages <- map["total_pages"]
		totalResults <- map["total_results"]
		
	}
    required init?(map: Map) {
        
    }

    

}
