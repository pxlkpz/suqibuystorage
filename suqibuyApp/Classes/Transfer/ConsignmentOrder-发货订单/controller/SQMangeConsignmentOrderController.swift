//
//  SQMangeWaitingSendController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ChameleonFramework
import MJRefresh

class SQMangeConsignmentOrderController: SQBaseViewController{
    private let viewModel = SQMangeConsignmentOrderViewModel()
    private let disposeBag = DisposeBag()
    private let bottomEditView:SQDaigouShopBottomEditView = SQDaigouShopBottomEditView.instanceFromNib("SQDaigouShopBottomEditView") as! SQDaigouShopBottomEditView
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQMangeConsignmentOrderCell","SQManageConsignmentOrderFooterCell" ])
    var searchController: UISearchController = UISearchController.createSearchController("输入国内快递单号或物品信息，收件人名，电话快速搜索订单")

    fileprivate lazy var bgView = UIView.initWithNoneDataView("当前列表为空！")

    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
        setupBindings()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        
    }
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            
            self?.resetUI()
            }.disposed(by: disposeBag)
        
        
        /// .Edit
        self.bottomEditView.deleteButton.rx.tap.subscribe { [weak self] event in
            self?.viewModel.requesTransshipBatchDel()
            }.disposed(by: disposeBag)
        
        self.bottomEditView.allSelctctButton.rx.tap.subscribe { [weak self]  event in
            self?.changeSelectAll(!(self?.bottomEditView.allSelctctButton.isSelected)!)
            }.disposed(by: disposeBag)
    }
    
    func resetUI() {
        
        self.listTableView.mj_header.endRefreshing()
        self.listTableView.mj_footer.endRefreshing()
        if self.viewModel.dataListVariable.value.totalPages == self.viewModel.currentValue {
            self.listTableView.mj_footer.endRefreshingWithNoMoreData()
        }
        self.listTableView.reloadData()
        
        //bottom
        self.bottomEditView.setData(viewModel.dataListVariable.value)
        if (self.viewModel.dataListVariable.value.items?.count ?? 0) == 0 {
            self.listTableView.mj_footer.isHidden = true

            self.view.addSubview(bgView)
        } else {
            self.listTableView.mj_footer.isHidden = false

            bgView.removeFromSuperview()
        }
    }
    
    func buildUI()  {
        
        self.title = "发货订单"
        
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view).offset(0)
        }
        
        self.listTableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
            self?.viewModel.requestWarehouseList(1)
            self?.viewModel.currentValue = 1
        })
        
        self.listTableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {[weak self] in
            self?.viewModel.currentValue += 1
            self?.viewModel.requestWarehouseList((self?.viewModel.currentValue)!)
        })
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = searchController;
        } else {
            self.listTableView.tableHeaderView = searchController.searchBar
        }

        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self

        
        let rightButton = UIButton.createRightBianji()
        rightButton.addTarget(self, action: #selector(rightBarItemBtnClick(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        
        self.view.addSubview(self.bottomEditView)
        self.bottomEditView.isHidden = true
        self.bottomEditView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.height.equalTo(45)
//            make.bottom.equalTo(view).offset(-kSafeAreaBottomHeight)

        }
        
    }
    
    func changeSelectAll(_ isSelect:Bool) {

        for numi in 0..<(viewModel.dataListVariable.value.items?.count ?? 0) {
            let model1 = viewModel.dataListVariable.value.items![numi]
            model1.isCanDelete = isSelect
            viewModel.dataListVariable.value.items![numi] = model1
            resetUI()
        }
    }
    
    @objc func rightBarItemBtnClick(_ button : UIButton) {
        button.isSelected = !button.isSelected
        if button.isSelected { //编辑状态
            viewModel.editSelectMode = .editMode
            self.bottomEditView.isHidden = false
            
        } else {
            viewModel.editSelectMode = .normalMode
            self.bottomEditView.isHidden = true
            
        }
        self.listTableView.reloadData()
    }
}

extension SQMangeConsignmentOrderController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.dataListVariable.value.items?.count ?? 0
    }

    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1{
            return 30
        }
        return 114
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1{ //footCell
            let cellIdentifier = "SQManageConsignmentOrderFooterCell"
            let gouwuViewCell:SQManageConsignmentOrderFooterCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQManageConsignmentOrderFooterCell
            gouwuViewCell.commonItemModel = self.viewModel.dataListVariable.value.items![indexPath.section]
            gouwuViewCell.selectClosure = {[weak self] type ->Void in
                self?.searchController.isActive = false
            }

            return gouwuViewCell

        }
        
        let cellIdentifier = "SQMangeConsignmentOrderCell"
        let gouwuViewCell:SQMangeConsignmentOrderCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQMangeConsignmentOrderCell
        gouwuViewCell.setData(self.viewModel.dataListVariable.value.items![indexPath.section], mode: self.viewModel.editSelectMode)

        return gouwuViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 , viewModel.editSelectMode == .editMode{
            let cell:SQMangeConsignmentOrderCell = tableView.cellForRow(at: indexPath) as! SQMangeConsignmentOrderCell
            cell.selectButton.isSelected = !cell.selectButton.isSelected
            self.viewModel.dataListVariable.value.items![indexPath.section].isCanDelete = cell.selectButton.isSelected
            resetUI()
        }
    }
}

extension SQMangeConsignmentOrderController: UISearchResultsUpdating, UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarSearchButtonClicked")
        if let searchText = searchBar.text {
            print(searchText)
            self.viewModel.keywordValue = searchText
            self.viewModel.currentValue = 1
            self.viewModel.requestWarehouseList(1)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.viewModel.keywordValue = ""
        self.viewModel.currentValue = 1
        self.viewModel.requestWarehouseList(1)
        

        if #available(iOS 11.0, *) {
            //UI调整
            self.listTableView.contentInset.top = 0
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
        if #available(iOS 11.0, *) {
            self.listTableView.contentInset.top = -10
        }
        
    }
    

    
    
}
