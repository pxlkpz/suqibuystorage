//
//  SQMangeWaitingSendViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

class SQMangeConsignmentOrderViewModel {
    
    let dataListVariable = Variable<SQMangeConsignmentOrderModel>(SQMangeConsignmentOrderModel(JSON: ["string" : "Any"])!)
    var editSelectMode:SQViewEditMode = SQViewEditMode.normalMode
    var currentValue = 1
    var keywordValue = ""

    init() {
        requestWarehouseList(1)
    }
    
    func requestWarehouseList(_ index: NSInteger) {

        suqiBuyProvider.request(.transshipOrderlist(data: ["page":"\(index)","keyword":keywordValue])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQMangeConsignmentOrderModel = Mapper<SQMangeConsignmentOrderModel>().map(JSON: data!["result"] as! [String : Any])!
                    if index == 1 {
                        self.dataListVariable.value = model
                    } else {
                        if let items = model.items {
                            let array = self.dataListVariable.value.items! + items;
                            model.items = array
                            self.dataListVariable.value = model
                        }
                    }

                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }


            }
        }
        
    }
    
    func requesTransshipBatchDel() {

        suqiBuyProvider.request(.transshipBatchDel(data: getIds()!)) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    self.requestWarehouseList(1)
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }


            }
        }

    }
    
    func getIds() -> Dictionary<String, Any>? {
        var ids:Array<String>? = []
        for model in self.dataListVariable.value.items! {
            if self.editSelectMode == .editMode {
                if model.isCanDelete == true {
                        ids?.append(model.packageNo!)
                }
            }
        }

         var dic = [String:Any]()
        
        dic["order_nos"] = ids

        return dic
    }
    
    

}
