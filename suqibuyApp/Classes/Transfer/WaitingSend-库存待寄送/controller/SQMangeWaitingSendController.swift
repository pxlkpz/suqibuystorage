//
//  SQMangeWaitingSendController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ChameleonFramework
import MJRefresh

class SQMangeWaitingSendController: SQBaseViewController{
    private let viewModel = SQMangeWaitingSendViewModel()
    private let disposeBag = DisposeBag()
    
    
    var searchController: UISearchController = UISearchController.createSearchController("输入快递单号或物品信息快速搜索")
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQManagerWaitingSendCell" ])
    fileprivate lazy var bgView = UIView.initWithNoneDataView("当前列表为空！")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
        setupBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            
            self?.resetUI()
            }.disposed(by: disposeBag)
        
    }
    
    func resetUI() {
        
        self.listTableView.mj_header.endRefreshing()
        self.listTableView.mj_footer.endRefreshing()
        if self.viewModel.dataListVariable.value.totalPages == self.viewModel.currentValue {
            self.listTableView.mj_footer.endRefreshingWithNoMoreData()
        }
        self.listTableView.reloadData()
        
        
        
        if (self.viewModel.dataListVariable.value.items?.count ?? 0) == 0 {
            self.listTableView.mj_footer.isHidden = true
            
            self.view.addSubview(bgView)
        } else {
            self.listTableView.mj_footer.isHidden = false
            
            bgView.removeFromSuperview()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute:
            {
                self.title = "库存待寄送(\(self.viewModel.dataListVariable.value.items?.count ?? 0))"
        })
        
    }
    
    func buildUI()  {
        
        self.title = "库存待寄送"
        
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view).offset(0)
        }
        
        
        self.listTableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
            self?.viewModel.requestTransshipExpressitemsgroup(1)
            self?.viewModel.currentValue = 1
        })
        
        self.listTableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {[weak self] in
            self?.viewModel.currentValue += 1
            self?.viewModel.requestTransshipExpressitemsgroup((self?.viewModel.currentValue)!)
        })
        
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = searchController;
        } else {
            
            self.listTableView.tableHeaderView = searchController.searchBar
        }
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        
    }
    
    func changeSelectAll(_ isSelect:Bool) {
        
        for numi in 0..<(viewModel.dataListVariable.value.items?.count ?? 0) {
            let model1 = viewModel.dataListVariable.value.items![numi]
            model1.isCanDelete = isSelect
            viewModel.dataListVariable.value.items![numi] = model1
            resetUI()
        }
    }
    
    
    
    
    func changeSelectSection(_ isSelect:Bool, _ section: NSInteger) {
        viewModel.dataListVariable.value.items?[section].isCanDelete = isSelect
        resetUI()
    }
    
}

extension SQMangeWaitingSendController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.dataListVariable.value.items?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 30
        }
        return 0;
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let headView:SQMangerWaitingSendWeightView = UIView.instanceFromNib("SQMangerWaitingSendWeightView") as! SQMangerWaitingSendWeightView
            headView.weightLabel.text = "库存总重量\(self.viewModel.dataListVariable.value.waitingTranshippWeightTotal!)"
            return headView
        }
        return UIView()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 136
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SQManagerWaitingSendCell"
        let gouwuViewCell:SQManagerWaitingSendCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQManagerWaitingSendCell
        gouwuViewCell.setData(self.viewModel.dataListVariable.value.items![indexPath.section], viewModel.editSelectMode)
        
        gouwuViewCell.selectClosure = { [weak self] (type)->Void in
            if type == "20" {
                let vc = SQdaishouListDetailsView()
                vc.dataDic = self?.viewModel.dataListVariable.value.items![indexPath.section].toJSON()
                self?.navigationController?.pushViewController(vc, animated: true)
            } else{
                self?.changeSelectSection(gouwuViewCell.selectButton.isSelected, indexPath.section)
            }
        }
        
        return gouwuViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:SQManagerWaitingSendCell = tableView.cellForRow(at: indexPath) as! SQManagerWaitingSendCell
        cell.selectButtonAction(cell.selectButton)
    }
    
    
    
}

extension SQMangeWaitingSendController: UISearchResultsUpdating, UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarSearchButtonClicked")
        if let searchText = searchBar.text {
            print(searchText)
            self.viewModel.keywordValue = searchText
            self.viewModel.currentValue = 1
            self.viewModel.requestTransshipExpressitemsgroup(1)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.viewModel.keywordValue = ""
        self.viewModel.currentValue = 1
        self.viewModel.requestTransshipExpressitemsgroup(1)
        if #available(iOS 11.0, *) {
            //UI调整
            self.listTableView.contentInset.top = 0
        }
        
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
        if #available(iOS 11.0, *) {
            self.listTableView.contentInset.top = -10
        } else {
            
        }
        
    }
    
}

