//
//	SQItem.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQMangerWaitSendItem : Mappable{
    required init?(map: Map) {
        
    }
    
    var isCanDelete : Bool = false
    
	var catName : String?
	var daishouPackageId : String?
	var expressNo : String?
	var expresssCompanyName : String?
	var id : String?
	var isAdded : Bool?
	var itemDescription : String?
	var itemName : String?
	var qty : String?
	var size : String?
	var weight : String?

    var price : String?
    var itemThumb : String?
    var itemStatusLabel : String?

    var signedAt : String?

 
    
	func mapping(map: Map)
	{
		catName <- map["cat_name"]
		daishouPackageId <- map["daishou_package_id"]
		expressNo <- map["express_no"]
		expresssCompanyName <- map["expresss_company_name"]
		id <- map["id"]
		isAdded <- map["is_added"]
		itemDescription <- map["item_description"]
		itemName <- map["item_name"]
		qty <- map["qty"]
		size <- map["size"]
		weight <- map["weight"]
		
        price <- map["price"]
        itemThumb <- map["item_thumb"]
        itemStatusLabel <- map["item_status_label"]
        signedAt <- map["signed_at"]

	}

    
}
