//
//	SQWaitPutInStorageModel.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQWarehouseItemsModelItems : Mappable{
    required init?(map: Map) {
        
    }
    

	var createdAt : String?
	var descriptionField : String?
	var id : String?
	var isNeedCheck : Bool?
	var itemCode : AnyObject?
	var itemName : String?
	var itemQty : String?
	var itemThumb : String?
	var status : String?
	var statusLabel : String?
	var weight : String?

	func mapping(map: Map)
	{
		createdAt <- map["created_at"]
		descriptionField <- map["description"]
		id <- map["id"]
		isNeedCheck <- map["is_need_check"]
		itemCode <- map["item_code"]
		itemName <- map["item_name"]
		itemQty <- map["item_qty"]
		itemThumb <- map["item_thumb"]
		status <- map["status"]
		statusLabel <- map["status_label"]
		weight <- map["weight"]
		
	}

}
