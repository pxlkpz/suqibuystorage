//
//	SQMangeWaitingSendModel.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQMangeWaitingSendModel :Mappable{
    required init?(map: Map) {
        
    }
    

	var currentPage : Int?
	var items : [SQMangerWaitSendItem]?
	var lastDaishouPackageId : String?
	var page : String?
	var pageNum : Int?
	var totalPages : Int?
	var totalResults : Int?
	var waitingTranshippWeightTotal : String?

 

	func mapping(map: Map)
	{
		currentPage <- map["current_page"]
		items <- map["items"]
		lastDaishouPackageId <- map["last_daishou_package_id"]
		page <- map["page"]
		pageNum <- map["page_num"]
		totalPages <- map["total_pages"]
		totalResults <- map["total_results"]
		waitingTranshippWeightTotal <- map["waiting_transhipp_weight_total"]
		
	}

    

}
