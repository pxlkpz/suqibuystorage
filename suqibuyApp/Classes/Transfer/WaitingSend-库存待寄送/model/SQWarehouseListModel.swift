//
//	SQRootClass.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQWarehouseListModel : Mappable{
    required init?(map: Map) {
        
    }
    
	var currentPage : Int?
	var items : [SQWarehouseItemsModel]?
	var page : String?
	var pageNum : Int?
	var totalPages : Int?
	var totalResults : Int?
	var totalWeight : String?

	func mapping(map: Map)
	{
		currentPage <- map["current_page"]
		items <- map["items"]
		page <- map["page"]
		pageNum <- map["page_num"]
		totalPages <- map["total_pages"]
		totalResults <- map["total_results"]
		totalWeight <- map["total_weight"]
		
	}
}
