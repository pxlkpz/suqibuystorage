//
//  SQWaitPutInStorageCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation


class SQWaitPutInStorageCell: UITableViewCell {
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var moneLabel: UILabel!
    
    override func awakeFromNib() {
        
    }
    
    func setData(_ model: SQWarehouseItemsModelItems) {
//        moneyLabel.text = (model.itemPurchasingPrice! as NSString).gtm_stringByUnescapingFromHTML()
        titleView.text = model.itemName
        contentLabel.text = model.descriptionField
//        QLabel.text = "×" + model.itemQty!
        leftImageView.sd_setImage(with:  URL(string: model.itemThumb!), placeholderImage: UIImage(named: "zhuanyun"))

    }
}
