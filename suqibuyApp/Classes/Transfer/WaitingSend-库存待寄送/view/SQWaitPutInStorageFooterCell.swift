//
//  SQWaitPutInStorageFooterView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ChameleonFramework
class SQWaitPutInStorageFooterCell: UITableViewCell {
    
    @IBOutlet weak var selectButton: UIButton!
    @IBAction func selectButtonAction(_ sender: Any) {
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectButton.layer.cornerRadius = 3
        selectButton.layer.borderWidth = 1
        selectButton.layer.borderColor = (HexColor("#FF7F02") as! CGColor)
    }
    func setData() {
        
    }
}
