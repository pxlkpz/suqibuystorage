//
//  SQManagerWaitingSendCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/4.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

class SQManagerWaitingSendCell: SQBaseSwiftTableCell {

    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var sendNoLabel: UILabel!
    @IBOutlet weak var packageNameLabel: UILabel!
    @IBOutlet weak var packageTypyLabel: UILabel!
    @IBOutlet weak var packageWightLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!

    @IBOutlet weak var selectButtonWidth: NSLayoutConstraint!
    
    @IBAction func selectButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        selectClosure!("10")

    }
    @IBAction func moreButtonAction(_ sender: UIButton) {
        selectClosure!("20")
    }
    
    func setData(_ model: SQMangerWaitSendItem, _ mode:SQViewEditMode) {
        
        selectButton.isSelected = model.isCanDelete
        sendNoLabel.text = "\(model.expresssCompanyName ?? "") \(model.expressNo ?? "")"
        packageNameLabel.text = model.itemName
        packageTypyLabel.text = model.itemDescription
        packageWightLabel.text = "重量：\(model.weight ?? "")"
        if mode == .editMode{
            selectButton.isHidden = false
            selectButtonWidth.constant = 19
        } else {
            selectButton.isHidden = true
            selectButtonWidth.constant = 0
        }
        

        statusLabel.text = model.itemStatusLabel
        timeLabel.text = "入库日期：\(model.signedAt ?? "")"
        moneyLabel.text = (model.price! as NSString).gtm_stringByUnescapingFromHTML()
        leftImageView.sd_setImage(with:  URL(string: model.itemThumb!), placeholderImage: UIImage(named: "zhuanyun"))

    }
    
    
}
