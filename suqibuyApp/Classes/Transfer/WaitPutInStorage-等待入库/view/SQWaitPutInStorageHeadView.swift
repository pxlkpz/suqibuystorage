//
//  SQWaitPutInStorageHeadView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

class SQWaitPutInStorageHeadView: UIView {
    @IBOutlet weak var selectButtonWidth: NSLayoutConstraint!
    
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var centerLabel: UILabel!
    @IBAction func selectAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    func setData(_ model: SQWarehouseItemsModel, _ mode:SQViewEditMode) {
        if mode == .normalMode {
            selectButton.isHidden = true
            selectButtonWidth.constant = 0
        } else {
            selectButtonWidth.constant = 20
            selectButton.isHidden = false
        }
        selectButton.isSelected = model.isCanDelete
        centerLabel.text = "\(model.companyName ?? ""))：\(model.packageExpressNo ?? "")"

    }
}
