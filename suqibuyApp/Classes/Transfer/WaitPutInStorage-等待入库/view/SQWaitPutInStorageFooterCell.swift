//
//  SQWaitPutInStorageFooterView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ChameleonFramework
class SQWaitPutInStorageFooterCell: SQBaseSwiftTableCell {

    @IBOutlet weak var selectButton: UIButton!
    @IBAction func selectButtonAction(_ sender: Any) {
        selectClosure!("10")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectButton.layer.cornerRadius = 10
        selectButton.layer.masksToBounds = true
        selectButton.layer.borderWidth = 1
        selectButton.layer.borderColor = HexColor("#FF7F02")?.cgColor
    }
    func setData(_ mode:SQViewEditMode) {
        if mode == .editMode {
            selectButton.layer.borderColor = HexColor("#FF7F02")?.cgColor
            selectButton.setTitleColor(HexColor("#FF7F02"), for: .normal)
            selectButton.setTitle("删除", for: .normal)

        } else { //修改
            selectButton.layer.borderColor = HexColor("#0081CC")?.cgColor
            selectButton.setTitleColor(HexColor("#0081CC"), for: .normal)
            selectButton.setTitle("修改", for: .normal)
        }
    }
}
