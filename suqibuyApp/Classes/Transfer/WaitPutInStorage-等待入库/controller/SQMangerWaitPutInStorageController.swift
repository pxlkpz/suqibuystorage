//
//  SQNewDeliveryController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ChameleonFramework
import MJRefresh

class SQMangerWaitPutInStorageController: SQBaseViewController{
    private let viewModel = SQWaitPutInStorageViewModel()
    private let disposeBag = DisposeBag()
    private let bottomEditView:SQDaigouShopBottomEditView = SQDaigouShopBottomEditView.instanceFromNib("SQDaigouShopBottomEditView") as! SQDaigouShopBottomEditView
    var searchController: UISearchController = UISearchController.createSearchController("输入快递单号或物品信息快速搜索")
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQWaitPutInStorageCell","SQWaitPutInStorageFooterCell" ])
    
    fileprivate lazy var bgView = UIView.initWithNoneDataView("当前列表为空\n马上开始创建吧！")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
        setupBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            
            self?.resetUI()
            }.disposed(by: disposeBag)
        
        
        /// .Edit
        self.bottomEditView.deleteButton.rx.tap.subscribe { [weak self] event in
            self?.viewModel.requestWarehouseBatchDelete()
            }.disposed(by: disposeBag)
        
        self.bottomEditView.allSelctctButton.rx.tap.subscribe { [weak self]  event in
            self?.changeSelectAll(!(self?.bottomEditView.allSelctctButton.isSelected)!)
            }.disposed(by: disposeBag)
    }
    
    func resetUI() {
        
        self.listTableView.mj_header.endRefreshing()
        self.listTableView.mj_footer.endRefreshing()
        if self.viewModel.dataListVariable.value.totalPages == self.viewModel.currentValue {
            self.listTableView.mj_footer.endRefreshingWithNoMoreData()
        }
        self.listTableView.reloadData()
        
        
        //bottom
        self.bottomEditView.setData(viewModel.dataListVariable.value)
        
        if (self.viewModel.dataListVariable.value.items?.count ?? 0) == 0 {
            self.listTableView.mj_footer.isHidden = true

            self.view.addSubview(bgView)
        } else {
            self.listTableView.mj_footer.isHidden = false

            bgView.removeFromSuperview()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5, execute:
            {
                self.title = "等待入库包裹(\(self.viewModel.dataListVariable.value.items?.count ?? 0))"
        })
        
        
    }
    
    func buildUI()  {
        
        self.title = "等待入库包裹"
        
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view).offset(0)
        }
        
        self.listTableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
            self?.viewModel.requestWarehouseList(1)
            self?.viewModel.currentValue = 1
        })
        
        self.listTableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {[weak self] in
            self?.viewModel.currentValue += 1
            self?.viewModel.requestWarehouseList((self?.viewModel.currentValue)!)
        })
        
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = searchController;
        } else {
            self.listTableView.tableHeaderView = searchController.searchBar
        }
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        
        let rightButton = UIButton.createRightBianji()
        rightButton.addTarget(self, action: #selector(rightBarItemBtnClick(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        
        self.view.addSubview(self.bottomEditView)
        self.bottomEditView.isHidden = true
        self.bottomEditView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.height.equalTo(45)
            
        }
        
    }
    
    func changeSelectAll(_ isSelect:Bool) {
        
        for numi in 0..<(viewModel.dataListVariable.value.items?.count ?? 0) {
            let model1 = viewModel.dataListVariable.value.items![numi]
            model1.isCanDelete = isSelect
            viewModel.dataListVariable.value.items![numi] = model1
            resetUI()
        }
    }
    
    
    
    @objc func rightBarItemBtnClick(_ button : UIButton) {
        button.isSelected = !button.isSelected
        if button.isSelected { //编辑状态
            viewModel.editSelectMode = .editMode
            self.bottomEditView.isHidden = false
            
        } else {
            viewModel.editSelectMode = .normalMode
            self.bottomEditView.isHidden = true
            
        }
        self.listTableView.reloadData()
    }
    
    func changeSelectSection(_ isSelect:Bool, _ section: NSInteger) {
        
        viewModel.dataListVariable.value.items?[section].isCanDelete = isSelect
        resetUI()
    }
    
    func selectDeleteButton(_ section: NSInteger) {
        if viewModel.editSelectMode == .editMode {
            self.viewModel.requestWarehouseDelete(viewModel.dataListVariable.value.items![section].daishouPackageId ?? "0")
        }
    }
    
}

extension SQMangerWaitPutInStorageController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.editSelectMode == .editMode{
            return (self.viewModel.dataListVariable.value.items![section].items?.count ?? 0) + 1
        }
        return self.viewModel.dataListVariable.value.items![section].items?.count ?? 0
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.dataListVariable.value.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 31
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headView:SQWaitPutInStorageHeadView = UIView.instanceFromNib("SQWaitPutInStorageHeadView") as! SQWaitPutInStorageHeadView
        headView.setData(self.viewModel.dataListVariable.value.items![section], viewModel.editSelectMode)
        
        headView.selectButton.rx.tap
            .subscribe{ [weak self] event in
                self?.changeSelectSection(headView.selectButton.isSelected, section)
            }
            .disposed(by: disposeBag)
        return headView
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == self.viewModel.dataListVariable.value.items![indexPath.section].items?.count {
            return 30
        }
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if viewModel.editSelectMode == .editMode{
            if indexPath.row == self.viewModel.dataListVariable.value.items![indexPath.section].items?.count {
                let cellIdentifier = "SQWaitPutInStorageFooterCell"
                let gouwuViewCell:SQWaitPutInStorageFooterCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQWaitPutInStorageFooterCell
                gouwuViewCell.setData(viewModel.editSelectMode)
                gouwuViewCell.selectClosure = { [weak self] (type)->Void in
                    self?.selectDeleteButton(indexPath.section)
                    
                }
                
                return gouwuViewCell
                
            }
        }
        
        let cellIdentifier = "SQWaitPutInStorageCell"
        let gouwuViewCell:SQWaitPutInStorageCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQWaitPutInStorageCell
        gouwuViewCell.setData((self.viewModel.dataListVariable.value.items![indexPath.section].items?[indexPath.row])!)
        return gouwuViewCell
    }
}

extension SQMangerWaitPutInStorageController: UISearchResultsUpdating, UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarSearchButtonClicked")
        if let searchText = searchBar.text {
            print(searchText)
            self.viewModel.keywordValue = searchText
            self.viewModel.currentValue = 1
            self.viewModel.requestWarehouseList(1)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.viewModel.keywordValue = ""
        self.viewModel.currentValue = 1
        self.viewModel.requestWarehouseList(1)
        if #available(iOS 11.0, *) {
            //UI调整
            self.listTableView.contentInset.top = 0
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
        if #available(iOS 11.0, *) {
            self.listTableView.contentInset.top = -10
        }
    }
    
}


