//
//	SQItem.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQWarehouseItemsModel : Mappable{
    required init?(map: Map) {
        
    }
    var isCanDelete : Bool = false

	var createdAt : String?
	var descriptionField : String?
	var id : String?
	var isNeedCheck : Bool?
	var itemCode : AnyObject?
	var itemName : String?
	var itemQty : String?
	var itemThumb : String?
	var status : String?
	var statusLabel : String?
	var weight : String?
	var canDelete : Bool?
	var canPayment : Bool?
	var checkResult : AnyObject?
	var companyName : String?
	var daishouPackageId : String?

    var grandTotal : String?
	var items : [SQWarehouseItemsModelItems]?
	var needIncharge : Bool?
	var packageExpressNo : String?
	var paidTotal : String?
	var payDaifuAmount : String?
	var subQty : String?
	var userRemainCash : String?
	var waitingPayDaifu : Bool?
	var waitingPaymentTotal : String?

    
 

	func mapping(map: Map)
	{
		createdAt <- map["created_at"]
		descriptionField <- map["description"]
		id <- map["id"]
		isNeedCheck <- map["is_need_check"]
		itemCode <- map["item_code"]
		itemName <- map["item_name"]
		itemQty <- map["item_qty"]
		itemThumb <- map["item_thumb"]
		status <- map["status"]
		statusLabel <- map["status_label"]
		weight <- map["weight"]
		canDelete <- map["can_delete"]
		canPayment <- map["can_payment"]
		checkResult <- map["check_result"]
		companyName <- map["company_name"]
		daishouPackageId <- map["daishou_package_id"]

        grandTotal <- map["grand_total"]
		items <- map["items"]
		needIncharge <- map["need_incharge"]
		packageExpressNo <- map["package_express_no"]
		paidTotal <- map["paid_total"]
		payDaifuAmount <- map["pay_daifu_amount"]
		subQty <- map["sub_qty"]
		userRemainCash <- map["user_remain_cash"]
		waitingPayDaifu <- map["waiting_pay_daifu"]
		waitingPaymentTotal <- map["waiting_payment_total"]

        
	}

 

}
