//
//  SQMangerNewDeliveryRequestRemarkCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/9.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQMangerNewDeliveryRequestRemarkCell: SQBaseSwiftTableCell {
    @IBOutlet weak var textFieldView: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.textFieldView.delegate = self

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}


extension SQMangerNewDeliveryRequestRemarkCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        // 结束编辑
        self.selectClosure!(textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}

