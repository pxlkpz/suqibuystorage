//
//  SQMangerNewDeliveryRequestBottomView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/9.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQMangerNewDeliveryRequestBottomView: UIView {

    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var sureButton: UIButton!

    func setData(_ model:SQMangerNewDeliveryReqeustModel) {
        guard model.grandTotal != nil else {
            return
        }
        self.moneyLabel.text = (model.grandTotal! as NSString).gtm_stringByUnescapingFromHTML()
        self.sureButton.setTitle("结算(\(model.items?.count ?? 0))", for: .normal)
        if model.notice == "请选择转运方式" || model.notice == "请选择转运地址" {
            self.sureButton.isEnabled = false
        } else {
            self.sureButton.isEnabled = true

        }
    }
}


