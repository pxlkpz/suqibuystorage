//
//  SQNewDeliveryRequestItemTopCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/15.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQNewDeliveryRequestItemTopCell: UITableViewCell {

    @IBOutlet weak var centerLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var dataArray : SQMangerNewDeliveryReqeustItem? {
        didSet {
            guard let `dataArray` = dataArray else {
                return
            }
            
            centerLabel.text = dataArray.expresssCompanyName
        }
    }
    
}
