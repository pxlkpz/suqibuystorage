//
//  SQNewDeliveryReqeustCommonOneCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/9.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

enum cellNumberType {
    case senderType
    case freeType
    case addressType
}

class SQNewDeliveryReqeustCommonOneCell: UITableViewCell {
    @IBOutlet weak var moneyLabelRightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var leftTitleLabel: UILabel!
    @IBOutlet weak var isInputLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ model:SQMangerNewDeliveryReqeustModel, _ index:NSInteger, type:cellNumberType) {
        if type == .senderType {
            switch index {
            case 0:
                self.leftTitleLabel.text = "选择转运方式（必选）"
                self.rightImageView.isHidden = false
                guard model.shippingInfo != nil else { return }
                self.leftTitleLabel.text = model.shippingInfo
            break
            case 1:
                self.leftTitleLabel.text = "转运运费："
                self.rightImageView.isHidden = true

                self.moneyLabel.isHidden = false
                guard model.subTotal != nil else {
                    self.moneyLabel.text = ""
                    return
                }
                self.moneyLabel.text = (model.subTotal! as NSString).gtm_stringByUnescapingFromHTML()
                break
            case 2:
                self.leftTitleLabel.text = "仓储费用："
                self.rightImageView.isHidden = true
                
                self.moneyLabel.isHidden = false
                guard model.warehouseFee != nil else {
                    self.moneyLabel.text = ""
                    return
                }
                self.moneyLabel.text = (model.warehouseFee! as NSString).gtm_stringByUnescapingFromHTML()
                break
            default:
                break
            }
        } else if type == .freeType{
            switch index {
            case 0:
                self.leftTitleLabel.text = "使用优惠券："
                self.rightImageView.isHidden = false
                self.isInputLabel.isHidden = false

                guard model.couponDiscountAmount != nil else { return }
                self.moneyLabel.text = (model.couponDiscountAmount! as NSString).gtm_stringByUnescapingFromHTML()
                self.moneyLabelRightConstraint.constant = 30
                guard model.couponCode != nil, model.couponCode != "" else { return }
                self.leftTitleLabel.text = "优惠券抵扣 [ \(model.couponCode ?? "")) ]"
                self.isInputLabel.isHidden = true

                
                break
            case 2:
                self.leftTitleLabel.text = "会员折扣："
                self.isInputLabel.isHidden = true
                self.rightImageView.isHidden = true
                self.moneyLabel.isHidden = false
                
                guard model.usergroupDiscoutAmount != nil else { return }
                self.moneyLabel.text = (model.usergroupDiscoutAmount! as NSString).gtm_stringByUnescapingFromHTML()

                break
            default:
                break
            }
        } else if type == .addressType{
            self.leftTitleLabel.text = "请选择转运地址"
            self.rightImageView.isHidden = false
            isInputLabel.isHidden = true
        }
    }
    
}
