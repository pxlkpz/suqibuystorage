//
//  SQNewDeliveryRequestCommonTwoCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/9.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQNewDeliveryRequestCommonTwoCell: SQBaseSwiftTableCell {

    @IBOutlet weak var bottomDetailLabel: UILabel!
    @IBOutlet weak var textFieldView: UITextField!
    @IBOutlet weak var rightImageView: UIImageView!
    @IBOutlet weak var leftTitleLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let preferredMaxWidth = UIScreen.main.bounds.size.width - 15*2
        bottomDetailLabel.preferredMaxLayoutWidth = preferredMaxWidth
        bottomDetailLabel.setContentHuggingPriority(.required, for: .vertical)
        
        self.textFieldView.keyboardType = .numbersAndPunctuation
        self.textFieldView.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setData(_ model:SQMangerNewDeliveryReqeustModel, _ index:NSInteger, type:cellNumberType) {
        if type == .senderType {
            switch index {
            case 2:
                self.leftTitleLabel.text = "申报价值(USD）：" //非必填
                self.bottomDetailLabel.text = "向海关申报，关系到关税与清关。如不填写我司代申报，产生问题客户 承担后果"
                break
            case 3:
                self.leftTitleLabel.text = "购买保险（自愿购买）：" //非必填
                self.bottomDetailLabel.text = model.insuranceMsg ?? ""
                guard model.insuranceFee != nil else { return }
                self.moneyLabel.text = (model.insuranceFee! as NSString).gtm_stringByUnescapingFromHTML()
                self.moneyLabel.isHidden = false
                
                
                guard model.insuranceAmount != nil else { return }
                self.textFieldView.text = model.insuranceAmount
                
                break
            default:
                break
            }
        } else {
            switch index {
            case 1:
                self.leftTitleLabel.text = "使用积分抵扣："//非必填
                self.rightImageView.isHidden = true
                self.bottomDetailLabel.text = model.creditsMsg ?? ""

                guard model.creditsAmount != nil else { return }
                self.moneyLabel.text = (model.creditsAmount! as NSString).gtm_stringByUnescapingFromHTML()
                self.moneyLabel.isHidden = false
                

                guard model.creditsPointTotal != nil else { return }
                self.textFieldView.text = model.creditsPointTotal
                break
             default:
                break
            }
        }
    }
}

extension SQNewDeliveryRequestCommonTwoCell: UITextFieldDelegate {
 
    func textFieldDidEndEditing(_ textField: UITextField) {
        // 结束编辑
        self.selectClosure!(textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return self.vaildataNumber(string as NSString)
    }
    
    func vaildataNumber(_ number:NSString) -> Bool {
        var res = true
        let tmpSet:CharacterSet = NSCharacterSet(charactersIn: "0123456789") as CharacterSet
        var i = 0
        while i < number.length {
            let string: NSString = number.substring(with: NSMakeRange(i, 1)) as NSString
            let range:NSRange = string.rangeOfCharacter(from: tmpSet)
            if range.length == 0 {
            res = false
                break
            }
            i += 1
        }
        return res
    }

    
    
}
