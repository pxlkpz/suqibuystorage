//
//  SQManagerNewDeliveryRequestAddressCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/9.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQManagerNewDeliveryRequestAddressCell: UITableViewCell {
    @IBOutlet weak var morenLabel: UILabel!
    @IBOutlet weak var shouhuoLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var morenLabelWeith: NSLayoutConstraint!
    @IBOutlet weak var nameLeft: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        morenLabel.layer.cornerRadius = 2
        morenLabel.layer.masksToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)


    }
    
    var dataArray : SQMangerNewDeliveryReqeustModel? {
        didSet {
            guard let `dataArray` = dataArray else {
                return
            }

            guard dataArray.name != nil else {return }
            shouhuoLabel.text = "收货人：" + dataArray.name!
            guard dataArray.telephone != nil else {return }
            phoneLabel.text = dataArray.telephone
            guard dataArray.addressInfo != nil else {return }
            addressLabel.text = "收获地址：" + dataArray.addressInfo!
            guard dataArray.is_default_address == true else {
                morenLabelWeith.constant = 0
                nameLeft.constant = 0
                morenLabel.isHidden = true
                return
            }
            
            if dataArray.is_default_address == true {
                morenLabelWeith.constant = 30
                nameLeft.constant = 5
                morenLabel.isHidden = false
            } else {
                morenLabelWeith.constant = 0
                nameLeft.constant = 0
                morenLabel.isHidden = true
            }
        }
    }

 }
