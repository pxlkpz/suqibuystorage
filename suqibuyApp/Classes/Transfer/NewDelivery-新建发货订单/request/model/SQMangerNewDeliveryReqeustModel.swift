//
//	SQModel.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation

import ObjectMapper
class SQMangerNewDeliveryReqeustModel :   Mappable{

	var addressDetail : String?
	var addressId : String?
	var creditsPointTotal : String?
	var grandTotal : String?
	var hasError : Bool?
	var insuranceAmount : String?
	var insuranceMsg : String?
	var items : [SQMangerNewDeliveryReqeustItem]?
	var notice : String?
	var shippingMethodId : AnyObject?
	var totalQty : Int?
	var usergroupDiscountRate : String?
	var usergroupDiscoutAmount : String?
    
    var name : String?
    var telephone : String?
    var is_default_address : Bool?

    var itemArrays : [Array<SQMangerNewDeliveryReqeustItem>]?
    //--手动添加
    var addressInfo : String?
    
    var agencyFeeMsg : String?
    var baseSizeWeight : String?
    var calculatedWeight : String?
    var couponCode : String?
    var couponDiscountAmount : String?
    var creditsAmount : String?
    var creditsMsg : String?
    var errorCoupon : String?
    var errorCredit : String?
    var errorInsurance : String?
    var insuranceFee : String?
    var maxSideLength : String?
    var shippingInfo : String?
    var size : String?
    var sizeRate : String?
    var subTotal : String?
    var usergroupDiscountMsg : String?
    var weight : String?
    var warehouseFee : String?
    var warehouseFeeLabel : String?

    


    //    "agency_fee" = "&yen;10.53";
//    "agency_fee_msg" = "VIP1 \U7528\U6237\U7684\U8f6c\U8fd0\U64cd\U4f5c\U8d39\U7387\U4e3a\Uff1a5%";
//    "base_size_weight" = 0;
//    "calculated_weight" = "1000.00";
//    "coupon_code" = "";
//    "coupon_discount_amount" = "- &yen;0.00";
//    "credits_amount" = "- &yen;0.00";
//    "credits_msg" = "\U6700\U591a\U53ef\U7528  22103 \U4e2a\U79ef\U5206";
//    "credits_point_total" = "<null>";
//    "error_coupon" = "";
//    "error_credit" = "";
//    "error_insurance" = "";



    //    "insurance_fee" = "&yen;0.00";

    //
//    "max_side_length" = 1;

    //    "shipping_info" = "EMS[\U7ea65-8\U5de5\U4f5c\U65e5\Uff0c\U65f6\U6548\U4e0d\U7a33\U5b9a]";

    //    size = 1;
//    "size_rate" = "<null>";
//    "sub_total" = "&yen;210.50";

    //    "usergroup_discount_msg" = "";


    //    weight = 1000;
	func mapping(map: Map)
	{
		addressDetail <- map["address_detail"]
		addressId <- map["address_id"]
		creditsPointTotal <- map["credits_point_total"]
		grandTotal <- map["grand_total"]
		hasError <- map["has_error"]
		insuranceAmount <- map["insurance_amount"]
		insuranceMsg <- map["insurance_msg"]
		items <- map["items"]
		notice <- map["notice"]
		shippingMethodId <- map["shipping_method_id"]
		totalQty <- map["total_qty"]
		usergroupDiscountRate <- map["usergroup_discount_rate"]
		usergroupDiscoutAmount <- map["usergroup_discout_amount"]
		//手动添加
        addressInfo <- map["address_info"]
        name <- map["name"]

        telephone <- map["telephone"]
        is_default_address <- map["is_default_address"]

        
        agencyFeeMsg <- map["agency_fee_msg"]
        baseSizeWeight <- map["base_size_weight"]
        calculatedWeight <- map["calculated_weight"]
        couponCode <- map["coupon_code"]
        couponDiscountAmount <- map["coupon_discount_amount"]
        creditsAmount <- map["credits_amount"]
        creditsMsg <- map["credits_msg"]
        errorCoupon <- map["error_coupon"]
        errorCredit <- map["error_credit"]
        errorInsurance <- map["error_insurance"]
        insuranceFee <- map["insurance_fee"]
        maxSideLength <- map["max_side_length"]
        shippingInfo <- map["shipping_info"]
        size <- map["size"]
        sizeRate <- map["size_rate"]
        subTotal <- map["sub_total"]
        warehouseFee <- map["warehouse_fee"]
        warehouseFeeLabel <- map["warehouse_fee_label"]
        subTotal <- map["sub_total"]
        usergroupDiscountMsg <- map["usergroup_discount_msg"]
        weight <- map["weight"]
     
	}

   
    required init?(map: Map) {
        
    }

}
