//
//  SQManagerNewRequestSaveModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/16.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ObjectMapper
class SQManagerNewRequestSaveModel: Mappable {
 
    var custom_remark : String = ""
    var apply_value : String = ""
    
    
    func mapping(map: Map)
    {
        custom_remark <- map["custom_remark"]
        apply_value <- map["apply_value"]
    }
    
    
    required init?(map: Map) {
        
    } 
    
    
}
