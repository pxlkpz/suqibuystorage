//
//  SQMangerNewDeliveryRequestViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/10.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxSwift
import ObjectMapper

class SQMangerNewDeliveryRequestViewModel:SQBaseViewModel {
    let dataListVariable = Variable<SQMangerNewDeliveryReqeustModel>(SQMangerNewDeliveryReqeustModel(JSON: ["string" : "Any"])!)
    
    var saveMdoel:SQManagerNewRequestSaveModel = SQManagerNewRequestSaveModel(JSON: ["" : ""])!
    
    override init() {}
    func requestTransshipQuote(_ dataDic:Dictionary<String, Any>) {
        
        suqiBuyProvider.request(.transshipQuote(data:dataDic)) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQMangerNewDeliveryReqeustModel = Mapper<SQMangerNewDeliveryReqeustModel>().map(JSON: data!["result"] as! [String : Any])!

                    self.dataListVariable.value = self.quoteData(model)!
 
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
        
    }
    
    
   
    
    
    
    
    /// 使用积分
    func requestTransshipUsecredit(_ pointTotal:String) {
        
        suqiBuyProvider.request(.transshipUsecredit(data:["credits_point_total":pointTotal])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQMangerNewDeliveryReqeustModel = Mapper<SQMangerNewDeliveryReqeustModel>().map(JSON: data!["result"] as! [String : Any])!
                    
                    self.dataListVariable.value = self.quoteData(model)!

                    
                    
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
        
    }
    /// 购买保险
    func requestTransshipInsurancepost(_ insuranceAmount:String) {
        
        suqiBuyProvider.request(.transshipInsurancepost(data:["insurance_amount":insuranceAmount])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQMangerNewDeliveryReqeustModel = Mapper<SQMangerNewDeliveryReqeustModel>().map(JSON: data!["result"] as! [String : Any])!
                    
                    self.dataListVariable.value = self.quoteData(model)!
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
        
    }
    
    /// 保存订单
    func requestTransshipSaveorder() {
        
        suqiBuyProvider.request(.transshipSaveorder(data:saveMdoel.toJSON())) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}

                if data!["success"] as! Int == 1 {

                    self.sureClosure!("saveOrder",data!["result"] as Any)
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
        
    }



    
    
    // dataChange
    
    private func quoteData(_ model:SQMangerNewDeliveryReqeustModel?) -> SQMangerNewDeliveryReqeustModel? {
        guard model != nil else {
            return nil
        }
        var mutableArray = Array<Array<SQMangerNewDeliveryReqeustItem>>()
        var pId = ""
        for i in 0...((model?.items?.count ?? 0)-1) {
            let model1:SQMangerNewDeliveryReqeustItem = model!.items![i]
            guard model1.daishouPackageId != nil else{continue}
            if model1.daishouPackageId == pId, pId != "" {
                mutableArray[mutableArray.count-1].append(model1)
            } else {
                let dataArray = [model1]
                mutableArray.append(dataArray)
            }
            pId = model1.daishouPackageId!
        }
        model?.itemArrays = mutableArray
        return model
    }
}



