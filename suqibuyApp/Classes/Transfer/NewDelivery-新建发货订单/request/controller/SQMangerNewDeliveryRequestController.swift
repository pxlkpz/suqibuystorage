//
//  SQMangerNewDeliveryRequestController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/5.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class SQMangerNewDeliveryRequestController: SQBaseViewController {
    private let viewModel = SQMangerNewDeliveryRequestViewModel()
    private let disposeBag = DisposeBag()
    private let bottomNormalView:SQMangerNewDeliveryRequestBottomView = UIView.instanceFromNib("SQMangerNewDeliveryRequestBottomView") as! SQMangerNewDeliveryRequestBottomView
    
    var quoteData:Dictionary<String,Any> = Dictionary()
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQDaigouShopItemCell","SQNewDeliveryRequestItemTopCell" ,"SQManagerNewDeliveryRequestAddressCell","SQNewDeliveryReqeustCommonOneCell","SQNewDeliveryRequestCommonTwoCell","SQMangerNewDeliveryRequestRemarkCell"])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
        setupBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.requestTransshipQuote(quoteData)
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            
            self?.resetUI()
            }.disposed(by: disposeBag)
        
        /// 完成 结算
        self.bottomNormalView.sureButton.rx.tap
            .subscribe{ [weak self] event in

                self?.viewModel.requestTransshipSaveorder()
            }
            .disposed(by: disposeBag)
        
        
        self.viewModel.sureClosure = { [weak self] type,data in
            let vc = SQzhuanyunPay()
            vc.dataDic = data as! [AnyHashable : Any]
            self?.navigationController?.setViewControllers([(self?.navigationController?.viewControllers[0])!,(self?.navigationController?.viewControllers[1])!,vc], animated: true)
        }
       
    }
    
    func resetUI() {
        
        self.listTableView.reloadData()
        //bottom
        self.bottomNormalView.setData(viewModel.dataListVariable.value)
        
    }
    
    func buildUI()  {
        
        self.title = "创建发货订单"
        
        self.listTableView.estimatedRowHeight = 80;
        self.listTableView.rowHeight = UITableViewAutomaticDimension;
        
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view).offset(-45)
            make.bottom.equalTo(view).offset(-kSafeAreaBottomHeight-45)

        }
        
        
        self.view.addSubview(self.bottomNormalView)
        self.bottomNormalView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.height.equalTo(45)
            make.bottom.equalTo(view).offset(-kSafeAreaBottomHeight)

        }
    }
    
    
    func requetTextFieldData(_ index:NSInteger, dataStr:String) {
        switch index {
        case 0:
            self.viewModel.saveMdoel.custom_remark = dataStr
            break
        case 1://积分
            self.viewModel.requestTransshipUsecredit(dataStr)
        break
        case 2://申报价值(USD）
            self.viewModel.saveMdoel.apply_value = dataStr
            break
        case 3://购买保险（自愿购买）
            self.viewModel.requestTransshipInsurancepost(dataStr)
            break
            
        default:
            break
        }
    }
}

extension SQMangerNewDeliveryRequestController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 1) {
            return 5
        } else if section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 2) {
            return 3
        } else if section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 3) {
            return 1
        }else {
            guard self.viewModel.dataListVariable.value.itemArrays != nil else {return 0}
            return (self.viewModel.dataListVariable.value.itemArrays![section-1].count ) + 1
        }
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 4
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if self.viewModel.dataListVariable.value.addressId == nil {
                return 40
            }
            return 66
        }
            
        else if indexPath.section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 1) {
            if indexPath.row < 3 {
                return 40
            } else  {
                return UITableViewAutomaticDimension
            }
        } else if indexPath.section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 2) {
            if indexPath.row == 1 {
                
                return UITableViewAutomaticDimension
                
            } else  {
                return 40
            }
            
        } else if indexPath.section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 3) {
            return 95
        } else {
            if indexPath.row == 0 {
                return 40
            } else {
                return 104
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if self.viewModel.dataListVariable.value.addressId == nil {
                let gouwuViewCell:SQNewDeliveryReqeustCommonOneCell = getCell("SQNewDeliveryReqeustCommonOneCell", tableView: tableView)
                gouwuViewCell.setData(self.viewModel.dataListVariable.value, indexPath.row, type: .addressType)
                return gouwuViewCell
            } else {
                let cellIdentifier = "SQManagerNewDeliveryRequestAddressCell"
                let gouwuViewCell:SQManagerNewDeliveryRequestAddressCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQManagerNewDeliveryRequestAddressCell
                gouwuViewCell.dataArray = self.viewModel.dataListVariable.value
                return gouwuViewCell
            }
        } else if indexPath.section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 1){
            if indexPath.row < 3 {
                let gouwuViewCell:SQNewDeliveryReqeustCommonOneCell = getCell("SQNewDeliveryReqeustCommonOneCell", tableView: tableView)
                gouwuViewCell.setData(self.viewModel.dataListVariable.value, indexPath.row, type: .senderType)

                return gouwuViewCell
                
            } else  {
                let gouwuViewCell:SQNewDeliveryRequestCommonTwoCell = getCell("SQNewDeliveryRequestCommonTwoCell", tableView: tableView)
                gouwuViewCell.setData(self.viewModel.dataListVariable.value, indexPath.row, type: .senderType)
                gouwuViewCell.selectClosure = {  [weak self] (type)->Void in
                    self?.requetTextFieldData(indexPath.row, dataStr: type)
                }
                return gouwuViewCell
            }
            
        } else if indexPath.section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 2) {
            if indexPath.row == 1{
                let gouwuViewCell:SQNewDeliveryRequestCommonTwoCell = getCell("SQNewDeliveryRequestCommonTwoCell", tableView: tableView)
                gouwuViewCell.setData(self.viewModel.dataListVariable.value, indexPath.row, type: .freeType)
                gouwuViewCell.selectClosure = {  [weak self] (type)->Void in
                    self?.requetTextFieldData(indexPath.row, dataStr: type)
                }
                return gouwuViewCell
                
            } else  {
                let gouwuViewCell:SQNewDeliveryReqeustCommonOneCell = getCell("SQNewDeliveryReqeustCommonOneCell", tableView: tableView)
                gouwuViewCell.setData(self.viewModel.dataListVariable.value, indexPath.row, type: .freeType)
                return gouwuViewCell
            }
            
        } else if indexPath.section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 3) {

            let gouwuViewCell:SQMangerNewDeliveryRequestRemarkCell = getCell("SQMangerNewDeliveryRequestRemarkCell", tableView: tableView)
            gouwuViewCell.backgroundColor = UIColor.clear
            gouwuViewCell.selectClosure = {  [weak self] (type)->Void in
                self?.requetTextFieldData(indexPath.row, dataStr: type)
            }

            
            return gouwuViewCell

        } else { // 商品内容
            if indexPath.row == 0 {
                let cellIdentifier = "SQNewDeliveryRequestItemTopCell"
                let gouwuViewCell:SQNewDeliveryRequestItemTopCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQNewDeliveryRequestItemTopCell
                gouwuViewCell.dataArray = self.viewModel.dataListVariable.value.itemArrays![indexPath.section-1][indexPath.row]
                return gouwuViewCell
            } else {
                let cellIdentifier = "SQDaigouShopItemCell"
                let gouwuViewCell:SQDaigouShopItemCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouShopItemCell
                gouwuViewCell.dataArray = self.viewModel.dataListVariable.value.itemArrays![indexPath.section-1][indexPath.row-1]

                return gouwuViewCell
            }


        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 { // 地址
            let cv:SQwoUserAddress = SQwoUserAddress()
            cv.type = 10;
            self.navigationController?.pushViewController(cv, animated: true)
            
        } else if indexPath.section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 1){
            if indexPath.row == 0 { // 选择转运方式

                let fangshiController:SQzhuanyunFangshi = SQzhuanyunFangshi()

                self.navigationController?.pushViewController(fangshiController, animated: true)

            }
            
        } else if indexPath.section == ((self.viewModel.dataListVariable.value.itemArrays?.count ?? 0) + 2) {
            if indexPath.row == 0{// 选择优惠券
                let youhuiquanController:SQzhuanyunYouhuiquan = SQzhuanyunYouhuiquan()
                youhuiquanController.fromController = FromControllerFahuo
                self.navigationController?.pushViewController(youhuiquanController, animated: true)
                
            }
            
        }
        
    }
    
    
    func getCell<T>(_ identifier: String, tableView:UITableView) -> T {
        
        let gouwuViewCell = tableView.dequeueReusableCell(withIdentifier: identifier)
        
        return gouwuViewCell! as! T
    }
}



