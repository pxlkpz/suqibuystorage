//
//  SQNewDeliveryViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

class SQMangerNewDeliveryListViewModel {
    let dataListVariable = Variable<SQManagerNewDeliveryListModel>(SQManagerNewDeliveryListModel(JSON: ["string" : "Any"])!)
    var keywordValue = ""

    init() {

    }
    func requestTransshipCartitems() {
        
        suqiBuyProvider.request(.transshipCartitems(data: ["keyword":keywordValue])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQManagerNewDeliveryListModel = Mapper<SQManagerNewDeliveryListModel>().map(JSON: data!["result"] as! [String : Any])!                    
                    self.dataListVariable.value = model
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
    }
    
    
    
    func getItemIds() -> Dictionary<String, Any>? {
        var ids:Array<String>? = []
        for model in self.dataListVariable.value.items! {
            if model.isCanSelect == true {
                ids?.append(model.cartId!)
                
            }
        }

        var dic = [String:Any]()
        for num in 0..<ids!.count {
            dic["itemIds[\(num+1)]"] = ids?[num]
        }
        
        return dic
    }
    
}

