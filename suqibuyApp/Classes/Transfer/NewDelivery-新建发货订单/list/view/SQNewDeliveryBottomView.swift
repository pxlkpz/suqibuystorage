//
//  SQNewDeliveryBottomView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation


class SQNewDeliveryBottomView: UIView {
    
    @IBOutlet weak var sureButton: UIButton!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var selectNumberLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!
    @IBAction func selectButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    public var dataModel:SQManagerNewDeliveryListModel? {
        didSet{
            guard let `dataModel` = dataModel else {return}
            
            guard dataModel.items != nil else {return}
            
            var weightF = 0.0
            
            for model3 in dataModel.items!{
                if model3.isCanSelect {

                    weightF += Double(model3.weight?.replacingOccurrences(of: ",", with: "") ?? "0")!
                    
                }
            }
            weightLabel.text = "\(weightF)克"
            var num = 0
            for model1 in dataModel.items! {
                if model1.isCanSelect {
                    num += 1
                }
            }
            if num == 0 {
                sureButton.isEnabled = false
            } else {
                sureButton.isEnabled = true
            }
            selectNumberLabel.text = "已选\(num)件商品"

            
        }
    }
}
