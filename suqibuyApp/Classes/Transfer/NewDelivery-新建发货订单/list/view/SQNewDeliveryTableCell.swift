//
//  SQNewDeliveryTableCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQNewDeliveryTableCell: UITableViewCell {
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var expressNoLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var catNameLabel: UILabel!
    
    @IBOutlet weak var selectButton: UIButton!
    @IBAction func selectButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(_ model: SQManagerNewDeliveryListItem) {
        leftImageView.sd_setImage(with:  URL(string: model.itemThumb!), placeholderImage: UIImage(named: "zhuanyun"))
        topLabel.text = model.itemName
        typeLabel.text = model.itemDescription
        moneyLabel.text = (model.price! as NSString).gtm_stringByUnescapingFromHTML()
        expressNoLabel.text = "\(model.expresssCompanyName ?? ""):\(model.expressNo ?? "")"
        weightLabel.text = model.weight
        catNameLabel.text = model.catName
        selectButton.isSelected = model.isCanSelect
    }
}

