//
//	SQModel.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQManagerNewDeliveryListModel : Mappable{

	var cartTotal : Int?
	var items : [SQManagerNewDeliveryListItem]?
	var totalWeight : String?


	func mapping(map: Map)
	{
		cartTotal <- map["cart_total"]
		items <- map["items"]
		totalWeight <- map["total_weight"]
		
	}

   
    required init?(map: Map) {
        
    }

}
