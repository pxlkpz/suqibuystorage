//
//	SQItem.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQManagerNewDeliveryListItem : Mappable{

	var cartId : String?
	var catName : String?
	var daishouPackageId : String?
	var expressNo : String?
	var expresssCompanyName : String?
	var goodId : String?
	var isActived : Bool?
	var itemDescription : String?
	var itemName : String?
	var itemThumb : String?
	var price : String?
	var qty : String?
	var size : String?
	var weight : String?

    var isCanSelect : Bool = false

	func mapping(map: Map)
	{
		cartId <- map["cart_id"]
		catName <- map["cat_name"]
		daishouPackageId <- map["daishou_package_id"]
		expressNo <- map["express_no"]
		expresssCompanyName <- map["expresss_company_name"]
		goodId <- map["good_id"]
		isActived <- map["is_actived"]
		itemDescription <- map["item_description"]
		itemName <- map["item_name"]
		itemThumb <- map["item_thumb"]
		price <- map["price"]
		qty <- map["qty"]
		size <- map["size"]
		weight <- map["weight"]
		
	}

   
    required init?(map: Map) {
    
    }

}
