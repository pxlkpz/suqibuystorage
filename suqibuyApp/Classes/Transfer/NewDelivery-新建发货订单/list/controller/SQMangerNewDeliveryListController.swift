//
//  SQNewDeliveryController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class SQMangerNewDeliveryListController: SQBaseViewController{
    private let viewModel = SQMangerNewDeliveryListViewModel()
    private let disposeBag = DisposeBag()
    private let bottomNormalView:SQNewDeliveryBottomView = UIView.instanceFromNib("SQNewDeliveryBottomView") as! SQNewDeliveryBottomView
    var searchController: UISearchController = UISearchController.createSearchController("输入快递单号或物品信息快速搜索")

    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQNewDeliveryTableCell" ])
    
    
    
    fileprivate lazy var bgView = UIView.initWithNoneDataView("您尚未添加任何内容！")

    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
        setupBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.requestTransshipCartitems()
        self.navigationController?.navigationBar.isTranslucent = true

    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            
            self?.resetUI()
            }.disposed(by: disposeBag)
        

        /// .Normal
        self.bottomNormalView.selectButton.rx.tap
            .subscribe{ [weak self] event in
                
                self?.changeSelectAll((self?.bottomNormalView.selectButton.isSelected)!)
            }
            .disposed(by: disposeBag)
        
        /// 完成
        self.bottomNormalView.sureButton.rx.tap
            .subscribe{ [weak self] event in
                let clearVC = SQMangerNewDeliveryRequestController()
                clearVC.quoteData = (self?.viewModel.getItemIds())!
                self?.searchController.isActive = false
                self?.navigationController?.pushViewController(clearVC, animated: true);
            }
            .disposed(by: disposeBag)

    }
    
    func resetUI() {
        
        self.listTableView.reloadData()
        

        //bottom
        self.bottomNormalView.dataModel = viewModel.dataListVariable.value
        if (self.viewModel.dataListVariable.value.items?.count ?? 0) == 0 {
            self.view.addSubview(bgView)
        } else {
            bgView.removeFromSuperview()
        }
    }
    
    func buildUI()  {
        
        self.title = "新建发货订单"
        
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.top.equalTo(self.view)
            make.bottom.equalTo(view).offset(-kSafeAreaBottomHeight-45)

        }
        
        self.view.addSubview(self.bottomNormalView)
        self.bottomNormalView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.height.equalTo(45)
            make.bottom.equalTo(view).offset(-kSafeAreaBottomHeight)

            
        }
       
        if #available(iOS 11.0, *) {
            self.navigationItem.searchController = searchController;
        } else {
            self.listTableView.tableHeaderView = searchController.searchBar
        }
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
    }
    
    func changeSelectAll(_ isSelect:Bool) {
        for numi in 0..<(viewModel.dataListVariable.value.items?.count ?? 0){

            viewModel.dataListVariable.value.items![numi].isCanSelect = isSelect

        }

        resetUI()

    }
    
    func changeSelectSection(_ isSelect:Bool, _ section: NSInteger) {
        viewModel.dataListVariable.value.items?[section].isCanSelect = !isSelect
        resetUI()
    }
    
}

extension SQMangerNewDeliveryListController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.dataListVariable.value.items?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SQNewDeliveryTableCell"
        let gouwuViewCell:SQNewDeliveryTableCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQNewDeliveryTableCell
        gouwuViewCell.setData(self.viewModel.dataListVariable.value.items![indexPath.section])
        
        return gouwuViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:SQNewDeliveryTableCell = tableView.cellForRow(at: indexPath) as! SQNewDeliveryTableCell

        changeSelectSection(cell.selectButton.isSelected, indexPath.section)
        
    }
    
}

extension SQMangerNewDeliveryListController: UISearchResultsUpdating, UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchBarSearchButtonClicked")
        if let searchText = searchBar.text {
            print(searchText)
            self.viewModel.keywordValue = searchText

            self.viewModel.requestTransshipCartitems()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.viewModel.keywordValue = ""
         self.viewModel.requestTransshipCartitems()
        if #available(iOS 11.0, *) {
            //UI调整
            self.listTableView.contentInset.top = 0
        }
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("searchBarTextDidBeginEditing")
        if #available(iOS 11.0, *) {
            self.listTableView.contentInset.top = -10
        }
        
    }
}

