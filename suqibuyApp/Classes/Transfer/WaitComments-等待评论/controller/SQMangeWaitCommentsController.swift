//
//  SQMangeWaitingSendController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ChameleonFramework
import MJRefresh

class SQMangeWaitCommentsController: SQBaseViewController{
    private let viewModel = SQMangeWaitCommentsViewModel()
    private let disposeBag = DisposeBag()
    
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQWaitCommentsBottomCell","SQWaitCommentsCenterCell" ])
    
    private let successView:SQWaitCommonsSuccessView = UIView.instanceFromNib("SQWaitCommonsSuccessView") as! SQWaitCommonsSuccessView

    fileprivate lazy var bgView = UIView.initWithNoneDataView("当前列表为空！")

    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
        setupBindings()
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            
            self?.resetUI()
            }.disposed(by: disposeBag)
        self.viewModel.dataOrderReview.asObservable().subscribe { [weak self] num in
            if self?.viewModel.dataOrderReview.value == 10 { //评论完成
                self?.commonSuccess()
            } else if self?.viewModel.dataOrderReview.value == 20 {
//                self?.commonSuccess()
            }

        }.disposed(by: disposeBag)
    }
    
    func commonSuccess() {
        self.view.addSubview(self.successView)
        self.successView.snp.makeConstraints({ (make) in
            make.edges.equalTo(self.view)
        })
        self.title = "评价完成"
    }
    
    func resetUI() {
        
        self.listTableView.mj_header.endRefreshing()
        self.listTableView.mj_footer.endRefreshing()
        if self.viewModel.dataListVariable.value.totalPages == self.viewModel.currentValue {
            self.listTableView.mj_footer.endRefreshingWithNoMoreData()
        }
        self.listTableView.reloadData()
        
        if (self.viewModel.dataListVariable.value.items?.count ?? 0) == 0 {
            self.listTableView.mj_footer.isHidden = true

            self.view.addSubview(bgView)
        } else {
            self.listTableView.mj_footer.isHidden = false

            bgView.removeFromSuperview()
        }
        
    }
    
    func buildUI()  {
        
        self.title = "等待评论"
        
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view).offset(0)
        }
        
        self.listTableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
            self?.viewModel.requestTransshipOrderWaitingReview(1)
            self?.viewModel.currentValue = 1
        })
        
        self.listTableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {[weak self] in
            self?.viewModel.currentValue += 1
            self?.viewModel.requestTransshipOrderWaitingReview((self?.viewModel.currentValue)!)
        })
    }
    
}

extension SQMangeWaitCommentsController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.dataListVariable.value.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 34
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headView:SQWaitCommentsTopView = UIView.instanceFromNib("SQWaitCommentsTopView") as! SQWaitCommentsTopView
        headView.setData(self.viewModel.dataListVariable.value.items![section])

        return headView
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 79
        } else if indexPath.row == 1{
            return 197
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            
            let cellIdentifier = "SQWaitCommentsCenterCell"
            let gouwuViewCell:SQWaitCommentsCenterCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQWaitCommentsCenterCell
            gouwuViewCell.setData(self.viewModel.dataListVariable.value.items![indexPath.section])

            return gouwuViewCell
        }
        else if indexPath.row == 1 {
            let cellIdentifier = "SQWaitCommentsBottomCell"
            let gouwuViewCell:SQWaitCommentsBottomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQWaitCommentsBottomCell
            gouwuViewCell.setData(self.viewModel.dataListVariable.value.items![indexPath.section])
            gouwuViewCell.selectClosure = { [weak self] (type)->Void in
                if type == "10" {
                    self?.viewModel.requestTransshipOrderReview(gouwuViewCell.requestModel!)
                } else {
                    self?.viewModel.dataListVariable.value.items![indexPath.section].rate = gouwuViewCell.requestModel?.rate
                }
            }
            return gouwuViewCell
        }
        return UITableViewCell()
    }

    
}

