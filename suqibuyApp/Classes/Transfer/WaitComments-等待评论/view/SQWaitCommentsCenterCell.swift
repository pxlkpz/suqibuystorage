//
//  SQWaitCommentsCenterCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/5.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQWaitCommentsCenterCell: UITableViewCell {

    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var transferFeeLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var leftImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ model:SQMangeWaitCommentsItem) {
//        shipping_method_name package_no
        locationLabel.text = model.shippingAddressCountry
        weightLabel.text = model.weight ?? "" + "克"
        transferFeeLabel.text = "运费:\(((model.grandTotal ?? "") as NSString).gtm_stringByUnescapingFromHTML()!)"
        contentLabel.text = "\(model.shippingMethodName ?? "") | \(model.shippingMethodDescription ?? "")"

        leftImageView.sd_setImage(with:  URL(string: model.thumbnail!), placeholderImage: UIImage(named: "zhuanyun"))

    }
    
}
