//
//  SQWaitCommentsTopView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/5.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

class SQWaitCommentsTopView: UIView {
    
    @IBOutlet weak var transferNoLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    func setData(_ model:SQMangeWaitCommentsItem) {
        self.transferNoLabel.text = "\(model.shippingMethodName ?? "") \(model.packageNo ?? "")"
        statusLabel.text = model.statusLabel
    }
}
