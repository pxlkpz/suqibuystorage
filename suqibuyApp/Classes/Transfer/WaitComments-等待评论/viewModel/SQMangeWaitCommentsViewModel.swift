//
//  SQMangeWaitingSendViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

class SQMangeWaitCommentsViewModel {
    
    let dataListVariable = Variable<SQMangeWaitCommentsModel>(SQMangeWaitCommentsModel(JSON: ["string" : "Any"])!)
    let dataOrderReview = Variable<Int>(0)

    var currentValue = 1

    init() {
        requestTransshipOrderWaitingReview(1)
    }
    
    func requestTransshipOrderWaitingReview(_ index:NSInteger) {
        
        suqiBuyProvider.request(.transshipOrderWaitingReview(data: ["page":"\(index)"])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQMangeWaitCommentsModel = Mapper<SQMangeWaitCommentsModel>().map(JSON: data!["result"] as! [String : Any])!
                    if index == 1 {
                        self.dataListVariable.value = model
                    } else {
                        if let items = model.items {
                            let array = self.dataListVariable.value.items! + items;
                            model.items = array
                            self.dataListVariable.value = model
                        }
                    }

                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }


            }
        }
        
    }

    /// 评论
    func requestTransshipOrderReview(_ model:SQMangeWaitCommentRequestModel) {

        suqiBuyProvider.request(.transshipOrderReview(data: model.toJSON())) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    self.dataOrderReview.value = 10
                }else {
                    //ALter 错误提示
                    self.dataOrderReview.value = 20
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                 }
                
            }
        }
        
    }
}
