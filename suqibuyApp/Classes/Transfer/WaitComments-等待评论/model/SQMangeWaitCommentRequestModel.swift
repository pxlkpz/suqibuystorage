//
//  File.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/8.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ObjectMapper

class SQMangeWaitCommentRequestModel : Mappable{
    
    var order_no : String?
    var rate : String?
    var content :String?

    func mapping(map: Map)
    {
        order_no <- map["order_no"]
        rate <- map["rate"]
        content <- map["content"]
     }
    
    required init?(map: Map) {
        
    }
}
