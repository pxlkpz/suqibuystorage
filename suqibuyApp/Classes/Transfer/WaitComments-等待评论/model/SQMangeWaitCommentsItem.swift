//
//	SQItem.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQMangeWaitCommentsItem : Mappable{

	var canCancel : Bool?
	var canPayment : Bool?
	var completeAt : AnyObject?
	var createdAt : String?
	var customerNote : String?
	var grandTotal : String?
	var id : String?
	var lastHistory : String?
	var packageNo : String?
	var paidAt : AnyObject?
	var paidTotal : String?
	var shippingAddressCountry : String?
	var shippingAt : AnyObject?
	var shippingMethod : String?
	var shippingMethodDescription : String?
	var shippingMethodName : String?
	var shippingNo : AnyObject?
	var sizeTotal : String?
	var status : String?
	var statusLabel : String?
	var subTotal : String?
	var totalQty : String?
	var transferNo : String?
	var weight : String?
    var thumbnail : String?
    var rate : String?


	func mapping(map: Map)
	{
		canCancel <- map["can_cancel"]
		canPayment <- map["can_payment"]
		completeAt <- map["complete_at"]
		createdAt <- map["created_at"]
		customerNote <- map["customer_note"]
		grandTotal <- map["grand_total"]
		id <- map["id"]
		lastHistory <- map["last_history"]
		packageNo <- map["package_no"]
		paidAt <- map["paid_at"]
		paidTotal <- map["paid_total"]
		shippingAddressCountry <- map["shipping_address_country"]
		shippingAt <- map["shipping_at"]
		shippingMethod <- map["shipping_method"]
		shippingMethodDescription <- map["shipping_method_description"]
		shippingMethodName <- map["shipping_method_name"]
		shippingNo <- map["shipping_no"]
		sizeTotal <- map["size_total"]
		status <- map["status"]
		statusLabel <- map["status_label"]
		subTotal <- map["sub_total"]
		totalQty <- map["total_qty"]
		transferNo <- map["transfer_no"]
		weight <- map["weight"]
        thumbnail <- map["thumbnail"]
        rate <- map["rate"]

		
	}

    
    required init?(map: Map) {
        
    }
}
