//
//  SQMineHelpViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ObjectMapper
import RxSwift

class SQMineHelpViewModel: NSObject {

    var dataArray:Array<SQConfineTreeModel> = getData()
    
    class func getData() -> [SQConfineTreeModel] {
        
        let dataArray : Array = NSMutableArray(contentsOfFile: Bundle.main.path(forResource: "SQMineUserHelpDataList", ofType: "plist")!)! as Array
        let dataModelArray : [SQConfineTreeModel] = Mapper<SQConfineTreeModel>().mapArray(JSONArray: dataArray as! [[String : Any]])
        
        return dataModelArray
    }

    
    override init() {
        super.init()
        
    }

    
    let dataListVariable = Variable<[SQMineHelpListModel]>(Array.init())

    
    func requestSoftheleHelpList() {
        
        suqiBuyProvider.request(.softheleHelpList) { (result) in
            if case let .success(response) = result {
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let result = data!["result"] as! [String : Any]
                    let items = Mapper<SQMineHelpListModel>().mapArray(JSONArray: result["items"] as! [[String : Any]])

                    self.dataListVariable.value = items
                    
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
            }
        }
        
    }
}
