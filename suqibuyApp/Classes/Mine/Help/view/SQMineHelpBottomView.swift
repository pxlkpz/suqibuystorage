//
//  SQMineHelpBottomView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQMineHelpBottomView: UIView {

    @IBAction func selectButtonAction(_ sender: Any) {
        let vc = SQCustomServiceController()
        vc.title = "在线客服"
        self.viewController().navigationController?.pushViewController(vc, animated: true)
    }
}
