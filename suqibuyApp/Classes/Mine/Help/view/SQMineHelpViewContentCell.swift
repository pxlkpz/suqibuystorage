//
//  SQMineHelpViewContentCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQMineHelpViewContentCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var contentCell: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public var dataModel : SQMineHelpListModel? {
        didSet {
            guard let `dataModel` = dataModel else { return }
            guard dataModel.title != nil else { return }
            contentCell.text = dataModel.title
        }
    }

    
    
}
