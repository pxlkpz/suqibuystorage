//
//  SQMineHelpViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class SQMineHelpViewController: SQBaseViewController {

    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQMineHelpViewContentCell"])

    
    
    private let viewModel = SQMineHelpViewModel()
    private let disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "客户服务"
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self.view)
        }
        self.listTableView.estimatedRowHeight = 80;
        self.listTableView.rowHeight = UITableViewAutomaticDimension;
        
        let bottomView:SQMineHelpBottomView = UIView.instanceFromNib("SQMineHelpBottomView") as! SQMineHelpBottomView
        bottomView.backgroundColor = UIColor.clear
        let bgview = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenW, height:80))
        bgview.addSubview(bottomView)
        bottomView.snp.makeConstraints{
            $0.left.right.top.bottom.equalToSuperview()
        }
        self.listTableView.tableFooterView = bgview
        self.viewModel.requestSoftheleHelpList()
                
        setupBindings()
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
    }
    
    func resetUI() {
        self.listTableView.reloadData()
    }

}

extension SQMineHelpViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.dataListVariable.value.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellIdentifier = "SQMineHelpViewContentCell"
            let topViewCell : SQMineHelpViewContentCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQMineHelpViewContentCell

        let model = self.viewModel.dataListVariable.value[indexPath.row]
        topViewCell.contentCell.text = "\(indexPath.row+1)." + model.title!
            return topViewCell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.viewModel.dataListVariable.value[indexPath.row]
        let webController = CommonWebViewController.init(urlStr: model.link, andNavTitle: model.title)
        self.navigationController?.pushViewController(webController!, animated: true)


    }
}
