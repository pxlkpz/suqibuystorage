//
//  SQMineHelpListModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ObjectMapper

class SQMineHelpListModel : Mappable{
    required init?(map: Map) {}
    
    var id : String?
    var title : String?
    var title_color : String?
    var viewed_count : String?
    var content : String?
    var link : String?

    
    func mapping(map: Map)
    {
        id <- map["id"]
        title <- map["title"]

        title_color <- map["title_color"]
        viewed_count <- map["viewed_count"]
        content <- map["content"]
        link <- map["link"]

    }
    
}
