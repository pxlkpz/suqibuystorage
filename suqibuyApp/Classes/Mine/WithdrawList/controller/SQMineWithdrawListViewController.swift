//
//  SQMineHelpViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import MJRefresh

class SQMineWithdrawListViewController: SQBaseViewController {
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQMiineWithdrawListCell"])
    
    private let viewModel = SQMineWithdrawListViewModel()
    private let disposeBag = DisposeBag()
    fileprivate lazy var bgView = UIView.initWithNoneDataView("当前列表为空！")

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "提现历史"
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self.view)
        }
        self.listTableView.estimatedRowHeight = 80;
        self.listTableView.rowHeight = UITableViewAutomaticDimension;
        self.listTableView.separatorInset = .zero
        self.listTableView.layoutMargins = .zero

        self.listTableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
            self?.viewModel.requestWithdrawList(1)
            self?.viewModel.currentValue = 1
        })
        
        self.listTableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {[weak self] in
            self?.viewModel.currentValue += 1
            self?.viewModel.requestWithdrawList((self?.viewModel.currentValue)!)
        })

        self.viewModel.requestWithdrawList(1)
        setupBindings()
        
        
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
        
    }
    
    func resetUI() {
        
        self.listTableView.mj_header.endRefreshing()
        self.listTableView.mj_footer.endRefreshing()
        if self.viewModel.dataListVariable.value.totalPages == self.viewModel.currentValue {
            self.listTableView.mj_footer.endRefreshingWithNoMoreData()
        }
        self.listTableView.reloadData()
        
        if (self.viewModel.dataListVariable.value.items?.count ?? 0) == 0 {
            self.view.addSubview(bgView)
        } else {
            bgView.removeFromSuperview()
        }
        
        
    }
    
}

extension SQMineWithdrawListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.viewModel.dataListVariable.value.items?.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "SQMiineWithdrawListCell"
        let topViewCell : SQMiineWithdrawListCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQMiineWithdrawListCell
        topViewCell.dataModel = self.viewModel.dataListVariable.value.items![indexPath.row]
        return topViewCell
        
    }
}

