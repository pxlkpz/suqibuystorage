//
//  SQMineHelpViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ObjectMapper
import RxSwift

class SQMineWithdrawListViewModel {
    let dataListVariable = Variable<SQMineWithdrawListModel>(SQMineWithdrawListModel(JSON: ["string" : "Any"])!)
    var currentValue = 1

    
    func requestWithdrawList( _ index:NSInteger) {
        
        suqiBuyProvider.request(.withdrawList(data: ["page":"\(index)"])) { (result) in
            if case let .success(response) = result {
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQMineWithdrawListModel = Mapper<SQMineWithdrawListModel>().map(JSON: data!["result"] as! [String : Any])!
                    if index == 1 {
                        self.dataListVariable.value = model
                    } else {
                        if let items = model.items {
                            let array = self.dataListVariable.value.items! + items;
                            model.items = array
                            self.dataListVariable.value = model
                        }
                    }
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
            }
        }
        
    }
}
