//
//  SQMiineWithdrawListCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQMiineWithdrawListCell: UITableViewCell {

    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public var dataModel : SQMineWithdrawListItem? {
        didSet {
            guard let `dataModel` = dataModel else { return }
            guard (dataModel.id != nil) else { return }

            self.moneyLabel.text = "提现金额：" + (dataModel.refundAmount! as NSString).gtm_stringByUnescapingFromHTML()
            self.timeLabel.text = "提现时间：" + (dataModel.createdAt ?? "")
            self.detailLabel.text = "提现说明：" + (dataModel.note ?? "")
            self.statusLabel.text = dataModel.statusLabel
        }
    }

    
}
