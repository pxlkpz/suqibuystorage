//
//  SQMineWithdrawListModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

import ObjectMapper


class SQMineWithdrawListItem : Mappable{
    required init?(map: Map) {}
    
    var createdAt : String?
    var id : String?
    var note : String?
    var refundAmount : String?
    var status : String?
    var statusLabel : String?
    
    func mapping(map: Map)
    {
        createdAt <- map["created_at"]
        id <- map["id"]
        note <- map["note"]
        refundAmount <- map["refund_amount"]
        status <- map["status"]
        statusLabel <- map["status_label"]    }
    
}


class SQMineWithdrawListModel : Mappable{
    required init?(map: Map) {}
    
    var currentPage : Int?
    var items : [SQMineWithdrawListItem]?
    var page : String?
    var pageNum : Int?
    var totalPages : Int?
    var totalResults : Int?
    
    func mapping(map: Map)
    {
        currentPage <- map["current_page"]
        items <- map["items"]
        page <- map["page"]
        pageNum <- map["page_num"]
        totalPages <- map["total_pages"]
        totalResults <- map["total_results"]
    }
    
}
