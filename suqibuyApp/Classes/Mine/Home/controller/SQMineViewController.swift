//
//  SQMineViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/1/30.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class SQMineViewController: SQBaseViewController {
    
    private let viewModel = SQMineViewModel()
    private let disposeBag = DisposeBag()
    
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQMineCommonCell","SQMineTopViewCell","SQMineMyPropertyCell"])
    
    fileprivate lazy var listBottomView : UIView = {
        let bottomView:SQMineTableViewBottomView = UIView.instanceFromNib("SQMineTableViewBottomView") as! SQMineTableViewBottomView
        let bgview = UIView.init(frame: CGRect(x: 0, y: 0, width: kScreenW, height:61))
        bgview.addSubview(bottomView)
        bottomView.snp.makeConstraints{
            $0.left.right.top.bottom.equalToSuperview()
        }
        
        bottomView.selectButton.rx.tap
            .subscribe{ [weak self] event in
                self?.viewModel.requestUserLogout()

            }
            .disposed(by: disposeBag)

        

        return bgview
    }()

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(self.view).offset(-statusBarHeight)
        }
        
        self.listTableView.tableFooterView = listBottomView
        

        setupBindings()

        
        self.viewModel.requestUserGetInfo()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "SQwoVCIsRequst"), object: nil, queue: OperationQueue.main) { (noti) in
            self.viewModel.requestUserGetInfo()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.getUserData()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
    }

    func resetUI() {
        
        self.listTableView.reloadData()

        if SQUser.isLogin() {
            self.listTableView.tableFooterView = listBottomView
        } else {
            self.listTableView.tableFooterView = UIView()
        }
    }
    
    func pushNewController(_ viewController: UIViewController)  {
        viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: true);
    }
    
    func pushUserInfo() {
        if SQUser.loginWitchController(self) {return}
        let vc = SQWoUserInfo()
        vc.dataDic = self.viewModel.userInfoData
        self.pushNewController(vc)
    }
}

extension SQMineViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 || section == 3 || section == 4 {
            return 1
        }
        if section == 2 {
            return (self.viewModel.dataArray?.count)!-1
        }
        return 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 197
        } else if indexPath.section == 1 {
            return 124
        } else if indexPath.section > 1 && indexPath.section < 4 {
            return 40
        }else {
            return 64
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 15
        } else if section >= 2 {
            return 10
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cellIdentifier = "SQMineTopViewCell"
            let topViewCell : SQMineTopViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQMineTopViewCell
            topViewCell.dataModel = self.viewModel.dataListVariable.value
            topViewCell.selectClosure = { [weak self] (type)->Void in
                if type == "setting" {
                    self?.pushUserInfo()
                } else if type == "qiandao" {
                    self?.viewModel.requestUserSignIn()
                }
                
            }
            return topViewCell
        }
        else if indexPath.section == 1 {
            let cellIdentifier1 = "SQMineMyPropertyCell"
            let myPropertyCell : SQMineMyPropertyCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier1) as! SQMineMyPropertyCell
            
            myPropertyCell.dataModel = self.viewModel.dataListVariable.value
            
            return myPropertyCell
        }
        else if indexPath.section >= 2 && indexPath.section < 4 {
            let cellIdentifier = "SQMineCommonCell"
            let commtonCell : SQMineCommonCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQMineCommonCell
            if indexPath.section == 3 {
                commtonCell.setDataModel(self.viewModel.dataArray!.last!)
            } else {
                commtonCell.setDataModel(self.viewModel.dataArray![indexPath.row])
            }
            return commtonCell
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 {
            switch indexPath.row {
            case 0: //我的通知
                if SQUser.loginWitchController(self) {return}
                pushNewController(SQWoMyNotiVC())
                break
            case 1://立即充值
                if SQUser.loginWitchController(self) {return}
                pushNewController(SQWoChongzhiVC())
                break
            case 2://收获地址
                if SQUser.loginWitchController(self) {return}
                pushNewController(SQwoUserAddress())
                break
            case 3://用户服务
                pushNewController(SQMineUserHelpViewController())
                break
            case 4://余额历史
                if SQUser.loginWitchController(self) {return}
                pushNewController(SQWoCashHistoryVC())
                break
            case 5://余额提现
                if SQUser.loginWitchController(self) {return}
                pushNewController(SQMineWithdrawViewController())                
                break
            default:
                break
            }
        } else if indexPath.section == 3 {
            let vc = SQCustomServiceController()
            vc.title = "在线客服"
            pushNewController(vc)
        }
        
    }
    
}

