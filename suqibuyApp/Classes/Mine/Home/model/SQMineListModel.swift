//
//  SQMineListModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import ObjectMapper

struct SQMineListModel: Mappable {
    
    var iconName : String?
    var title : String?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        iconName <- map["iconName"]
        title    <- map["title"]
    }
}
