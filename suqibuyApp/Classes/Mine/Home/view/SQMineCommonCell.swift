//
//  SQMineCommonCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/1/31.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQMineCommonCell: UITableViewCell {

    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var leftLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
    }
    
    func setDataModel(_ model: SQMineListModel) {
        self.leftImageView.image = UIImage(named: model.iconName!)
        self.leftLabel.text = model.title!
    }
    
}
