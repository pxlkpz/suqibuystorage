//
//  SQMineMyPropertyCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/2.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQMineMyPropertyCell: UITableViewCell {

    @IBOutlet weak var view_balance: UIView!
    @IBOutlet weak var view_youhuiquan: UIView!
    @IBOutlet weak var view_jifen: UIView!
    
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var youhuiquanLabel: UILabel!
    @IBOutlet weak var jifenLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(self.tapPropertyAction(_:)))
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.tapPropertyAction(_:)))
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.tapPropertyAction(_:)))

        view_balance.addGestureRecognizer(tap1)
        view_jifen.addGestureRecognizer(tap2)
        view_youhuiquan.addGestureRecognizer(tap3)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @objc func tapPropertyAction(_ sender: UITapGestureRecognizer) {
        
        switch sender.view!.tag {
        case 0: break
        case 1: break
        case 2: break
        default:
            break
        }
    }
    
    public var dataModel : SQSwiftUserModel? {
        didSet{
            guard let `dataModel`=dataModel else { return }
            if SQUser.isLogin() {
                balanceLabel.text = ((dataModel.remain_cash ?? "￥0") as NSString).gtm_stringByUnescapingFromHTML()
                youhuiquanLabel.text = (dataModel.coupon_count ?? "0") + "张"
                jifenLabel.text = (dataModel.credits_amount ?? "0") + "分"
            } else {
                balanceLabel.text = ""
                youhuiquanLabel.text = ""
                jifenLabel.text = ""
            }
            

        }
    }
    
    
}
