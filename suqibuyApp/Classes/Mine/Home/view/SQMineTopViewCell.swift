//
//  SQMineTopViewCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQMineTopViewCell: SQBaseSwiftTableCell {

    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userDetail: UILabel!
    @IBOutlet weak var qingdaoButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(imageTapAction(_:)))
        userImage.isUserInteractionEnabled = true
        userImage.addGestureRecognizer(tap)
    }
   
    
    
    @objc func imageTapAction(_ sender: Any) {

        SQUser.loginWitchController(self.viewController())
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.userImage.layer.cornerRadius = 31.5;
        self.userImage.layer.masksToBounds = true
    }
    
    @IBAction func qingdaoButtonAction(_ sender: Any) {
        //1. 请求 2. 提示已经签到几次了
        self.selectClosure!("qiandao")

    }
    @IBAction func settingButtonAction(_ sender: Any) {
        self.selectClosure!("setting")
    }
    
    public var dataModel : SQSwiftUserModel? {
        didSet{
            if SQUser.isLogin() {
                guard let `dataModel`=dataModel else { return }
                guard dataModel.avatar_url != nil else {
                    return
                }
                userImage.sd_setImage(with:  URL(string: dataModel.avatar_url!), placeholderImage: UIImage(named: "sq_mine_home_head"))
                userName.text = dataModel.displayname
                
                userDetail.text = "等级 "+dataModel.group_name!+" | 邮箱 "+dataModel.email!
                
                
                if dataModel.signedToday != "0" || dataModel.signedToday == "" {
                    self.qingdaoButton.isHidden = true
                } else {
                    self.qingdaoButton.isHidden = false
                }

            } else {
                userImage.image = UIImage.init(named: "sq_mine_home_head")
                userName.text = "登录"
                userDetail.text = ""
                self.qingdaoButton.isHidden = true
            }

        }
    }
    
}
