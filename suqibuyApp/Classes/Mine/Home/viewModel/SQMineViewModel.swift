//
//  SQMineViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ObjectMapper
import RxSwift

class SQMineViewModel {
    let dataListVariable = Variable<SQSwiftUserModel>(SQSwiftUserModel(JSON: ["String" : "Any"])!)
    
    var dataArray : [SQMineListModel]? = getData()
    
    var userInfoData : NSDictionary?
    
    
    class func getData() -> [SQMineListModel] {
        
        let dataArray : Array = NSMutableArray(contentsOfFile: Bundle.main.path(forResource: "mineListData", ofType: "plist")!)! as Array
        let dataModelArray : [SQMineListModel] = Mapper<SQMineListModel>().mapArray(JSONArray: dataArray as! [[String : Any]])
        
        return dataModelArray
    }
    
    init() {}
    
    func getUserData(){
        if  let data = SQUser.loadDictionary()  {
            let model: SQSwiftUserModel = Mapper<SQSwiftUserModel>().map(JSON: data as! [String : Any])!
            self.dataListVariable.value = model
        }
    }
    
    
    
    /// 获取个人信息
    func requestUserGetInfo() {
        if !SQUser.isLogin() {return}
        suqiBuyProvider.request(.userGetInfo) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQSwiftUserModel = Mapper<SQSwiftUserModel>().map(JSON: data?["result"] as! [String : Any])!

                    self.dataChange(model)
                    self.userInfoData = (data?["result"] as! NSDictionary)
                    SQUser.saveDiction(self.dataListVariable.value.toJSON())

                    
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
            }
        }
        
    }
    
    /// 签到
    func requestUserSignIn() {
        
        suqiBuyProvider.request(.userSignIn) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let singedDay = (data?["result"] as! [String:Any])["sequentialSignedDays"]
                    
                    showDefaultPromptTextHUD("连续签到"+"\(singedDay ?? 1)"+"天")

                    var dic =  SQUser.loadDictionary()
                    dic!["signedToday"] = "1"
                    SQUser.saveDiction(dic)

                    let model: SQSwiftUserModel = Mapper<SQSwiftUserModel>().map(JSON: dic as! [String : Any])!
                    self.dataListVariable.value = model
                    
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
            }
        }
        
    }

    
    
    
    func dataChange(_ inModel:SQSwiftUserModel){
        let commonModel = SQSwiftUserModel(JSON: self.dataListVariable.value.toJSON())
        commonModel?.avatar_url = inModel.avatar_url
        commonModel?.credits_amount = inModel.credits_amount
        commonModel?.displayname = inModel.displayname
        commonModel?.email = inModel.email
        commonModel?.freeze_amount = inModel.freeze_amount
        commonModel?.group_id = inModel.group_id
        commonModel?.group_name = inModel.group_name
        commonModel?.growth_score = inModel.growth_score
        commonModel?.mobile = inModel.mobile
        commonModel?.remain_cash = inModel.remain_cash
        commonModel?.user_id = inModel.user_id
        commonModel?.user_token = inModel.user_token
        
        self.dataListVariable.value = commonModel!
    }
    
    
    
    
    /// 退出登录
    func requestUserLogout() {
        suqiBuyProvider.request(.userLogout) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    SQUser.clear()
                    ///FIXME

                    let model: SQSwiftUserModel = Mapper<SQSwiftUserModel>().map(JSON: data as! [String : Any])!
                    self.dataListVariable.value = model
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.autoLoginMothed()
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
            }
        }
        
    }
}

