//
//  SQMineWithdrawCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ChameleonFramework
class SQMineWithdrawCell: SQBaseSwiftTableCell {

    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    @IBOutlet weak var sureButton: UIButton!
    
    @IBOutlet weak var inputMoneyTextFeild: UITextField!
    @IBOutlet weak var inputPasswordTextField: UITextField!
    @IBOutlet weak var acceptAcountTextView: UITextView!
    
    fileprivate let placeholderLabel:UILabel = {
        let placeholder = UILabel.init()
        placeholder.isEnabled = false
        placeholder.text = "请输入接收提现账号信息"
        placeholder.font = UIFont.systemFont(ofSize: 15)
        placeholder.textColor = HexColor("#BDBDBD")
        
        return placeholder
    }()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clear
        
        acceptAcountTextView.delegate = self
        acceptAcountTextView.addSubview(self.placeholderLabel)
        placeholderLabel.snp.makeConstraints { (make) in
            make.top.equalTo(acceptAcountTextView).offset(8)
            make.left.equalTo(acceptAcountTextView).offset(7)
        }
        
        acceptAcountTextView.layer.borderWidth = 1
        acceptAcountTextView.layer.borderColor = UIColor(r: 242, g: 242, b: 242).cgColor
//            HexColor("#BDBDBD")?.cgColor
        
        acceptAcountTextView.layer.cornerRadius = 3
        acceptAcountTextView.layer.masksToBounds = true
        
        sureButton.layer.cornerRadius = 3
        sureButton.layer.masksToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    @IBAction func historyListButtonAction(_ sender: Any) {
        self.viewController().navigationController?.pushViewController(SQMineWithdrawListViewController(), animated: true)
    }
    
    @IBAction func sureButtonAction(_ sender: Any) {
        self.selectClosure!("sureButton")
        
    }
    
    public var dataModel : SQMineWithdrawModel? {
        didSet {
            guard let `dataModel` = dataModel else { return }
            guard (dataModel.canWithdrawAmount != nil) else { return }
            self.moneyLabel.text = "￥" + "\(dataModel.canWithdrawAmount ?? 0)"
            self.detailLabel.text = (dataModel.withdrawIntro! as NSString).gtm_stringByUnescapingFromHTML()
        }
    }
    
    
}

extension SQMineWithdrawCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.placeholderLabel.isHidden = !textView.text.isEmpty
    }
}
extension SQMineWithdrawCell : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        // 结束编辑
        self.selectClosure!(textField.text!)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.inputMoneyTextFeild {
            return self.vaildataNumber(string as NSString)
        }
        return true
        
    }
    
    func vaildataNumber(_ number:NSString) -> Bool {
        var res = true
        let tmpSet:CharacterSet = NSCharacterSet(charactersIn: "0123456789") as CharacterSet
        var i = 0
        while i < number.length {
            let string: NSString = number.substring(with: NSMakeRange(i, 1)) as NSString
            let range:NSRange = string.rangeOfCharacter(from: tmpSet)
            if range.length == 0 {
                res = false
                break
            }
            i += 1
        }
        return res
    }
}
