//
//  SQMineHelpViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ObjectMapper
import RxSwift

class SQMineWithdrawViewModel: SQBaseViewModel {
    let dataListVariable = Variable<SQMineWithdrawModel>(SQMineWithdrawModel(JSON: ["string":"json"])!)

    var postTitleContent = ("","","")

    func requestWithdrawIntro() {
        
        suqiBuyProvider.request(.withdrawIntro) { (result) in
            if case let .success(response) = result {
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {

                    let model: SQMineWithdrawModel = Mapper<SQMineWithdrawModel>().map(JSON: data?["result"] as! [String : Any])!

                    self.dataListVariable.value = model
                    
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
            }
        }
        
    }
    
    
    func requestWithdrawSave() {
        
        suqiBuyProvider.request(.withdrawSave(data: ["refund_amount":postTitleContent.0,"confirm_password":postTitleContent.1,"account_info":postTitleContent.2])) { (result) in
            if case let .success(response) = result {
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    showDefaultPromptTextHUD("提交成功")
                    self.sureClosure!("save",data as Any)
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
            }
        }
        
    }
}
