//
//  SQMineWithdrawModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/28.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

import ObjectMapper

class SQMineWithdrawModel : Mappable{
    required init?(map: Map) {}
    
    var canWithdrawAmount : Double?
    var withdrawIntro : String?
   
    
    func mapping(map: Map)
    {
        canWithdrawAmount <- map["canWithdrawAmount"]
        withdrawIntro <- map["withdrawIntro"]
    }
    
}

