//
//  SQMineHelpViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/27.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ChameleonFramework
class SQMineWithdrawViewController: SQBaseViewController {
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQMineWithdrawCell"])
    
    private let viewModel = SQMineWithdrawViewModel()
    private let disposeBag = DisposeBag()
    private var onceInt = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "提现申请"
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self.view)
        }

        self.listTableView.separatorStyle = .none
        self.listTableView.backgroundColor = UIColor.clear
        self.view.backgroundColor = UIColor(r: 245, g: 250, b: 254)
        setupBindings()
        
        self.viewModel.requestWithdrawIntro()
        
        self.viewModel.sureClosure = {[weak self] (type,data)->Void in
            self?.navigationController?.pushViewController(SQMineWithdrawListViewController(), animated: true)
        }
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
    }
    
    func oncesetupBinding(_ num:Int, _ cell1:SQMineWithdrawCell) {
        if num ==  onceInt{
            return
        }
        onceInt = num
        //动态绑定
        Observable.combineLatest(cell1.inputMoneyTextFeild.rx.text.orEmpty, cell1.inputPasswordTextField.rx.text.orEmpty,cell1.acceptAcountTextView.rx.text.orEmpty) { textValue1, textValue2 ,textValue3-> Bool in
            self.viewModel.postTitleContent.0 = textValue1
            self.viewModel.postTitleContent.1 = textValue2
            self.viewModel.postTitleContent.2 = textValue3
            
            if textValue1 != "" , textValue2 != "", textValue3 != "", Double(textValue1)! <= self.viewModel.dataListVariable.value.canWithdrawAmount! {
                return true
            }
            return false
            }
            .subscribe{ model in
                cell1.sureButton.isEnabled = model.element!
            }
            .disposed(by: disposeBag)
    }
    
    
    
    func resetUI() {
        self.listTableView.reloadData()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension SQMineWithdrawViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 538
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "SQMineWithdrawCell"
        let topViewCell : SQMineWithdrawCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQMineWithdrawCell
        topViewCell.dataModel = self.viewModel.dataListVariable.value
        oncesetupBinding(10, topViewCell)
        topViewCell.selectClosure = {[weak self] type -> Void in
            self?.viewModel.requestWithdrawSave()
        }
        return topViewCell
        
    }
    
    
}

