//
//	DaigouListOrderModelItem.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation

import ObjectMapper
class SQDaigouListOrderModelItem : Mappable{
    required init?(map: Map) {
        
    }
    var isCanDelete : Bool = false //自定义

	var agencyFee : String?
	var canCancel : Bool?
	var couponCode : String?
	var couponDiscountAmount : String?
	var createdAt : String?
	var creditsAmount : String?
	var creditsPointTotal : String?
	var customRemark : String?
	var daigouOrderNo : String?
	var goods : [SQDaigouListOrderModelGood]?
	var grandTotal : String?
	var id : String?
	var isWaitingPayment : Bool?
	var paidAt : AnyObject?
	var paidTotal : String?
	var showChongzhiLink : Bool?
	var status : String?
	var statusLabel : String?
	var subTotal : String?
	var totalQty : Int?
	var userCash : String?
	var waitingPayTotal : String?


	
    
	func mapping(map: Map)
	{
		agencyFee <- map["agency_fee"]
		canCancel <- map["can_cancel"]
		couponCode <- map["coupon_code"]
		couponDiscountAmount <- map["coupon_discount_amount"]
		createdAt <- map["created_at"]
		creditsAmount <- map["credits_amount"]
		creditsPointTotal <- map["credits_point_total"]
		customRemark <- map["custom_remark"]
		daigouOrderNo <- map["daigou_order_no"]
		goods <- map["goods"]
		grandTotal <- map["grand_total"]
		id <- map["id"]
		isWaitingPayment <- map["is_waiting_payment"]
		paidAt <- map["paid_at"]
		paidTotal <- map["paid_total"]
		showChongzhiLink <- map["show_chongzhi_link"]
		status <- map["status"]
		statusLabel <- map["status_label"]
		subTotal <- map["sub_total"]
		totalQty <- map["total_qty"]
		userCash <- map["user_cash"]
		waitingPayTotal <- map["waiting_pay_total"]
		
	}

}
