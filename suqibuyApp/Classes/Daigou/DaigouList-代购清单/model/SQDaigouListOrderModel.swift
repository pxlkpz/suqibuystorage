//
//	DaigouListOrderModelSQ.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQDaigouListOrderModel :Mappable{
  
    

	var currentPage : Int?
	var items : [SQDaigouListOrderModelItem]?
	var page : String?
	var pageNum : Int?
	var totalPages : Int?
	var totalResults : Int?

//    var isCanDelete : Bool? //自定义
    required init?(map: Map) {
        
    }
    
	func mapping(map: Map)
	{
		currentPage <- map["current_page"]
		items <- map["items"]
		page <- map["page"]
		pageNum <- map["page_num"]
		totalPages <- map["total_pages"]
		totalResults <- map["total_results"]
		
	}
}
