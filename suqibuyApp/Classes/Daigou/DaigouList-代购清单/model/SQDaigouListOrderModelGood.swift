//
//	DaigouListOrderModelGood.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQDaigouListOrderModelGood : Mappable{
    required init?(map: Map) {
        
    }
	var categoryName : String?
	var detail : String?
	var itemLink : String?
	var itemName : String?
	var itemParams : String?
	var itemPurchasingPrice : String?
	var itemQty : String?
	var itemRowTotals : String?
	var itemShippingPrice : String?
	var itemSku : String?
	var itemThumb : String?
	var status : String?
	var statusLabel : String?

	func mapping(map: Map)
	{
		categoryName <- map["category_name"]
		detail <- map["detail"]
		itemLink <- map["item_link"]
		itemName <- map["item_name"]
		itemParams <- map["item_params"]
		itemPurchasingPrice <- map["item_purchasing_price"]
		itemQty <- map["item_qty"]
		itemRowTotals <- map["item_row_totals"]
		itemShippingPrice <- map["item_shipping_price"]
		itemSku <- map["item_sku"]
		itemThumb <- map["item_thumb"]
		status <- map["status"]
		statusLabel <- map["status_label"]
		
	}

}
