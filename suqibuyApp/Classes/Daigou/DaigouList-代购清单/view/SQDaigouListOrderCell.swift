//
//  SQDaigouListOrderCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/24.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQDaigouListOrderCell: UITableViewCell {

    @IBOutlet weak var leftImageView: UIImageView!
    
    @IBOutlet weak var topTitleLabel: UILabel!
    
    @IBOutlet weak var centerDetailLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var QLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ model: SQDaigouListOrderModelGood) {
        moneyLabel.text = (model.itemPurchasingPrice! as NSString).gtm_stringByUnescapingFromHTML()
        topTitleLabel.text = model.itemName
        centerDetailLabel.text = model.detail
        QLabel.text = "×" + model.itemQty!
        leftImageView.sd_setImage(with:  URL(string: model.itemThumb!), placeholderImage: UIImage(named: "zhuanyun"))

    }
}
