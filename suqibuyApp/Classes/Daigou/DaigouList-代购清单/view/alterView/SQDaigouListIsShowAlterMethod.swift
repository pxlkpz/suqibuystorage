//
//  SQDaigouListIsShowAlterMethod.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/14.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
class SQDaigouListIsShowAlterMethod {
    class func popViewDisAnima(_ baseView: UIView){
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationCurve(.easeInOut)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(0.3)
        baseView.alpha = 0
        UIView.commitAnimations()
        
    }
    class func popViewInAnima(_ baseView: UIView){
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationCurve(.easeIn)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(0.5)
        baseView.alpha = 1
        UIView.commitAnimations()
    }
    
    class func showErrorView() {

        let keyWindow = UIApplication.shared.keyWindow

        
        let bgView = UIView(frame: (keyWindow?.bounds)!)
        bgView.backgroundColor = UIColor.black
        bgView.alpha = 0.5
        keyWindow?.addSubview(bgView)
        
        let bottomErrorView:SQDaigouListAlterView = UIView.instanceFromNib("SQDaigouListAlterView") as! SQDaigouListAlterView
        bottomErrorView.alpha = 0
        
        keyWindow?.addSubview(bottomErrorView)
        bottomErrorView.snp.makeConstraints { (make) in
            make.left.equalTo(bgView).offset(15)
            make.right.equalTo(bgView).offset(-15)
            make.bottom.equalTo(bgView)
            make.height.equalTo(214)
        }
        
       _ = bottomErrorView.cancelButton.rx.tap
            .subscribe{ event in
                popViewDisAnima(bottomErrorView)
                bgView.removeFromSuperview()
                bottomErrorView.removeFromSuperview()
        }

        
        popViewInAnima(bottomErrorView)
    }
    
    
    
}
