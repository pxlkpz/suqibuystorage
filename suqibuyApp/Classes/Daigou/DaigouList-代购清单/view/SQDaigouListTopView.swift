//
//  SQDaigouListTopView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/24.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

class SQDaigouListTopView: UIView {
    
    
    @IBOutlet weak var selectButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var orderStatus: UILabel!
    
    @IBOutlet weak var selectButton: UIButton!
    
    @IBAction func selectButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    func setData(_ model: SQDaigouListOrderModelItem ,_ mode:SQViewEditMode) {
        if mode == .normalMode {
            selectButton.isHidden = true
            selectButtonWidth.constant = 0
        } else {
            selectButtonWidth.constant = 19
            selectButton.isHidden = false
        }
        selectButton.isSelected = model.isCanDelete
//        selectButton.isEnabled = model.canCancel ?? false
        orderStatus.text = model.statusLabel
        orderNumberLabel.text = "订单号：\(model.daigouOrderNo ?? "")"
    }
    
    
}
