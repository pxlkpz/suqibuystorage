//
//  SQDaigouListOrderCenterCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/25.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

class SQDaigouListOrderCenterCell: UITableViewCell {
    
    @IBOutlet weak var leftQLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var yunfeiLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(_ model: SQDaigouListOrderModelItem) {
        leftQLabel.text = "共\(model.totalQty ?? 0)件商品 合计："
        moneyLabel.text = ((model.grandTotal ?? "0") as NSString).gtm_stringByUnescapingFromHTML()
    }
    
}
