//
//  SQDaigouListOrderBottomCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/25.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ChameleonFramework
class SQDaigouListOrderBottomCell: UITableViewCell {
    
    @IBOutlet weak var getDetailButton: UIButton!
    @IBOutlet weak var sureButton: UIButton!
    @IBOutlet weak var sureButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var detailButtonRightConstraint: NSLayoutConstraint!
    private var dataModel:SQDaigouListOrderModelItem?
    override func awakeFromNib() {
        super.awakeFromNib()
        sureButton.layer.cornerRadius = 11
        sureButton.layer.masksToBounds = true
        sureButton.layer.borderWidth = 1
        sureButton.layer.borderColor = HexColor("#FF7F02")?.cgColor
        
    }
    @IBAction func selectDetailAction(_ sender: UIButton) {

        let controller = SQOrderInformationVC()
        controller.order_no = dataModel?.daigouOrderNo
        self.viewController().navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func sureButtonAction(_ sender: UIButton) {
        let controller = SQPayOrderVC()
        let dic = self.dataModel?.toJSON()
        controller.dataModel = NSDictionary(dictionary: dic!) as! [AnyHashable : Any]
        self.viewController().navigationController?.pushViewController(controller, animated: true)

        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(_ model: SQDaigouListOrderModelItem) {
        dataModel = model
        if model.isWaitingPayment! {
            sureButtonWidth.constant = 61
            detailButtonRightConstraint.constant = 19
            sureButton.isHidden = false
        } else {
            sureButtonWidth.constant = 0
            detailButtonRightConstraint.constant = 9
            sureButton.isHidden = true
        }
    }
    
}
