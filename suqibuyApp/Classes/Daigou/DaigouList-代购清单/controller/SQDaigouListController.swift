//
//  SQDaigouViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/2.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ChameleonFramework
import MJRefresh

class SQDaigouListController: SQBaseViewController {
 
    private let viewModel = SQDaigouListViewModel(initialLanguage: "SQDaigouListController")
    private let disposeBag = DisposeBag()
    

    private let bottomEditView:SQDaigouShopBottomEditView = SQDaigouShopBottomEditView.instanceFromNib("SQDaigouShopBottomEditView") as! SQDaigouShopBottomEditView
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQDaigouListOrderBottomCell", "SQDaigouListOrderCell", "SQDaigouListOrderCenterCell"])

    fileprivate lazy var bgView = UIView.initWithNoneDataView("当前列表为空\n马上开始创建吧！")

 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "代购清单"
        
        buildUI()
        
        setupBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.currentValue = 1
        self.viewModel.requestDaigouCartitems(1)
    }
    
    private func setupBindings() {
        
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
        
        
        /// .Edit
        self.bottomEditView.deleteButton.rx.tap.subscribe { [weak self] event in
            self?.viewModel.requestDaigouBatchDelete()
            }.disposed(by: disposeBag)
        
        self.bottomEditView.allSelctctButton.rx.tap.subscribe { [weak self]  event in
            self?.changeSelectAll(!(self?.bottomEditView.allSelctctButton.isSelected)!)
            }.disposed(by: disposeBag)
    }
    
    func resetUI() {
        self.listTableView.mj_header.endRefreshing()
        self.listTableView.mj_footer.endRefreshing()
        if self.viewModel.dataListVariable.value.totalPages == self.viewModel.currentValue {
            self.listTableView.mj_footer.endRefreshingWithNoMoreData()
            self.listTableView.mj_footer.isHidden = true
        }
        self.listTableView.reloadData()

        //bottom
        self.bottomEditView.setData(viewModel.dataListVariable.value)
        
        if (self.viewModel.dataListVariable.value.items?.count ?? 0) == 0 {
            self.view.addSubview(bgView)
        } else {
            bgView.removeFromSuperview()
        }
        
    }
    
    func buildUI()  {
        
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.top.equalTo(self.view)
            make.bottom.equalTo(self.view).offset(0)
        }
        
        self.listTableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in
            self?.viewModel.requestDaigouCartitems(1)
            self?.viewModel.currentValue = 1
        })
        
        self.listTableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {[weak self] in
            self?.viewModel.currentValue += 1
            self?.viewModel.requestDaigouCartitems((self?.viewModel.currentValue)!)
        })
        
        
        
        let rightButton = UIButton.createRightBianji()
        rightButton.addTarget(self, action: #selector(rightBarItemBtnClick(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        

        self.view.addSubview(self.bottomEditView)
        self.bottomEditView.isHidden = true
        self.bottomEditView.snp.makeConstraints { (make) in
            make.left.right.equalTo(self.view)
            make.height.equalTo(45)
            make.bottom.equalTo(view)
        }
    }
    
    func changeSelectAll(_ isSelect:Bool) {
        
        switch viewModel.editSelectMode {
        case .editMode:
            for numi in 0..<(viewModel.dataListVariable.value.items?.count ?? 0) {
                let model1 = viewModel.dataListVariable.value.items![numi]
                model1.isCanDelete = isSelect
                viewModel.dataListVariable.value.items![numi] = model1
                resetUI()
            }
            
            break
        case .normalMode:
            break
        }
        
    }
    
    
    @objc func rightBarItemBtnClick(_ button : UIButton) {
        button.isSelected = !button.isSelected
        if button.isSelected { //编辑状态
            viewModel.editSelectMode = .editMode

            self.bottomEditView.isHidden = false
            self.listTableView.snp.remakeConstraints({ (make) in
                make.left.right.equalTo(self.view)
                make.top.equalTo(self.view)
                make.bottom.equalTo(self.view).offset(-45)
            })

        } else {
            viewModel.editSelectMode = .normalMode

            self.bottomEditView.isHidden = true
            self.listTableView.snp.remakeConstraints({ (make) in
                make.left.right.equalTo(self.view)
                make.top.equalTo(self.view)
                make.bottom.equalTo(self.view).offset(0)
            })
        }
        self.listTableView.reloadData()
    }
}

extension SQDaigouListController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.viewModel.dataListVariable.value.items![section].goods?.count ?? 0)+2
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.dataListVariable.value.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headView:SQDaigouListTopView = UIView.instanceFromNib("SQDaigouListTopView") as! SQDaigouListTopView
        headView.setData(viewModel.dataListVariable.value.items![section], viewModel.editSelectMode)

        
    headView.selectButton.rx.tap
        .subscribe{ [weak self] event in
            self?.viewModel.dataListVariable.value.items![section].isCanDelete = headView.selectButton.isSelected
            self?.resetUI()
 
        }
        .disposed(by: disposeBag)
        return headView
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (self.viewModel.dataListVariable.value.items![indexPath.section].goods?.count)! > indexPath.row {
            return 100
        } else if (self.viewModel.dataListVariable.value.items![indexPath.section].goods?.count)! == indexPath.row{ //center

            return 35
        } else {

            return 35
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (self.viewModel.dataListVariable.value.items![indexPath.section].goods?.count)! > indexPath.row {
            let cellIdentifier = "SQDaigouListOrderCell"
            let gouwuViewCell:SQDaigouListOrderCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouListOrderCell
            gouwuViewCell.setData(self.viewModel.dataListVariable.value.items![indexPath.section].goods![indexPath.row])
            return gouwuViewCell
        } else if (self.viewModel.dataListVariable.value.items![indexPath.section].goods?.count)! == indexPath.row{ //center
            let cellIdentifier = "SQDaigouListOrderCenterCell"
            let gouwuViewCell:SQDaigouListOrderCenterCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouListOrderCenterCell
            gouwuViewCell.setData(viewModel.dataListVariable.value.items![indexPath.section])
            return gouwuViewCell
        } else {
            let cellIdentifier = "SQDaigouListOrderBottomCell"
            let gouwuViewCell:SQDaigouListOrderBottomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouListOrderBottomCell
            gouwuViewCell.setData(viewModel.dataListVariable.value.items![indexPath.section])
            return gouwuViewCell
        }
    }
    
}


