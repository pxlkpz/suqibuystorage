//
//  SQDaigouListViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/24.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

class SQDaigouListViewModel {
    
    let dataListVariable = Variable<SQDaigouListOrderModel>(SQDaigouListOrderModel(JSON: ["string" : "Any"])!)
    var currentValue = 1
    var editSelectMode:SQViewEditMode = SQViewEditMode.normalMode
    
    init(initialLanguage: String) {
        
//        self.requestDaigouCartitems(1)
        
    }
    
    func requestDaigouCartitems(_ index: NSInteger) {
        _ = suqiBuyProvider.rx.request(.daigouList(data: ["page":"\(index)"])).subscribe { event in
            
            if case let .success(response) = event {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQDaigouListOrderModel = Mapper<SQDaigouListOrderModel>().map(JSON: data!["result"] as! [String : Any])!
                    if index == 1 {
                        self.dataListVariable.value = model
                    } else {
                        if let items = model.items {
                            self.dataListVariable.value.items! += items
                        }
                    }
                    
                }
            }
        }
    }
    
    func requestDaigouBatchDelete() {
        _ = suqiBuyProvider.rx.request(.daigouBatchDelete(data: getIds()!)).subscribe { event in
            
            if case let .success(response) = event {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let isBatch:Bool = (data!["result"] as! [String : Any])["is_batch_all"] as! Bool
                    if isBatch {
                        
                    } else {
                        SQDaigouListIsShowAlterMethod.showErrorView()
                    }
                    self.requestDaigouCartitems(1)
                }
            }
        }
    }
    
    func getIds() -> Dictionary<String, Any>? {
        var ids:Array<String>? = []
        for model in self.dataListVariable.value.items! {
            if self.editSelectMode == .editMode {
                if model.isCanDelete == true {
                    ids?.append(model.daigouOrderNo!)
                }
            }
        }
        
        var dic = [String:Any]()

        dic["order_nos"] = ids

        
        return dic
    }
    
    
    
}

