//
//  SQConfineTreeModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/8.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import ObjectMapper

struct SQConfineTreeModel: Mappable {
    var sectionTitle: String?
    var isShow: Bool?
    var listArray: Array<String>?
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        sectionTitle <- map["sectionTitle"]
        isShow <- map["isShow"]
        listArray <- map["listArray"]
    }
}
