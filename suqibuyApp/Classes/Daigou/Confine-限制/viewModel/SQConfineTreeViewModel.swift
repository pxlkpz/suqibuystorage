//
//  SQConfineTreeViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/9.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ObjectMapper

class SQConfineTreeViewModel: NSObject {
    var dataArray:Array<SQConfineTreeModel> = getData()
    
    class func getData() -> [SQConfineTreeModel] {
        
        let dataArray : Array = NSMutableArray(contentsOfFile: Bundle.main.path(forResource: "SQConfineTreeDataList", ofType: "plist")!)! as Array
        let dataModelArray : [SQConfineTreeModel] = Mapper<SQConfineTreeModel>().mapArray(JSONArray: dataArray as! [[String : Any]])
        
        return dataModelArray
    }

    override init() {
        super.init()
        
    }
}
