//
//  SQConfineTableCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/9.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ChameleonFramework
class SQConfineTableCell: UITableViewCell {

    @IBOutlet weak var textDataLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

        let preferredMaxWidth = kScreenW - 40
        textDataLabel.preferredMaxLayoutWidth = preferredMaxWidth
        textDataLabel.setContentHuggingPriority(.required, for: .vertical)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setDataModel(_ indexSection:NSInteger , _ indexRow:NSInteger, dataModel: SQConfineTreeModel) {
        
        if indexSection == 5 {
            textDataLabel.font = UIFont.systemFont(ofSize: 13)
            textDataLabel.textColor = HexColor("#000000")
            let attrStr = attributedText((dataModel.listArray?[indexRow])!, HexColor("#000000")!)
            textDataLabel.attributedText = attrStr
            //修改字体粗细
            return
        }
        
        if indexRow == 1 {
            textDataLabel.font = UIFont.systemFont(ofSize: 13)
            let attrStr = attributedText((dataModel.listArray?[indexRow])!, HexColor("#FF7F02")!)
            textDataLabel.attributedText = attrStr
            
        } else {
            textDataLabel.font = UIFont.systemFont(ofSize: 14)
            self.textDataLabel.text = dataModel.listArray?[indexRow]
        }
        
    }
    
    func setHelpDataModel(_ indexSection:NSInteger , _ indexRow:NSInteger, dataModel: SQConfineTreeModel) {
        
        textDataLabel.font = UIFont.systemFont(ofSize: 14)
        self.textDataLabel.text = dataModel.listArray?[indexRow]
    }

    
    
    func attributedText(_ string: String, _ firstColor:UIColor) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttributes([NSAttributedStringKey.foregroundColor:firstColor,
                                        NSAttributedStringKey.font:UIFont.boldSystemFont(ofSize: 13)], range: NSMakeRange(0, 3))
        return attributedString
    }

}
