//
//  SQConfineHeadView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/9.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ChameleonFramework
class SQConfineHeadView: UIView {
    @IBOutlet weak var headTitleLabel: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!
    func setData(_ model:SQConfineTreeModel ,_ indexSection: NSInteger) {
        self.tag = 2018+indexSection

        if model.isShow! {
            rightImageView.image = #imageLiteral(resourceName: "sq_common_xianzhi_icon_up")
        } else {
            rightImageView.image = #imageLiteral(resourceName: "sq_common_xianzhi_icon")
        }
        
        if indexSection == 0 {
            headTitleLabel.attributedText = attributedText(model.sectionTitle!, HexColor("#EC0000")!)
            return
        } else if indexSection == 4 {
            rightImageView.isHidden = true
        }
        
        headTitleLabel.text = model.sectionTitle


    }
    
    func setHelpData(_ model:SQConfineTreeModel ,_ indexSection: NSInteger) {
        self.tag = 2018+indexSection
        
        if model.isShow! {
            rightImageView.image = #imageLiteral(resourceName: "sq_common_xianzhi_icon_up")
        } else {
            rightImageView.image = #imageLiteral(resourceName: "sq_common_xianzhi_icon")
        }
        headTitleLabel.textColor = HexColor("#333333");
        headTitleLabel.font = UIFont(name: "PingFangSC-Regular", size: 15)
        
        headTitleLabel.text = model.sectionTitle
        
    }

    
    func attributedText(_ string: String, _ firstColor:UIColor) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttributes([NSAttributedStringKey.foregroundColor:firstColor], range: NSMakeRange(4, 6))
        return attributedString
    }
}
