//
//  SQConfineController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/4.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQConfineController: SQBaseTableViewController {

    var viewModel:SQConfineTreeViewModel = SQConfineTreeViewModel()
    fileprivate lazy var listTableView : UITableView = { [weak self] in
        var contentTable : UITableView = UITableView.createBaseTableView(false, self, ["SQConfineTableCell"])
  
        contentTable.rowHeight = UITableViewAutomaticDimension;
        contentTable.estimatedRowHeight = 80.0;

        return contentTable;
        }()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(self.listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self.view)
        }
        
        listTableView.reloadData()
        
    }

    @objc func tapGR(_ tap: UITapGestureRecognizer)  {
        let section = (tap.view?.tag)! - 2018
        if section == 4 {
            return
        }
        if viewModel.dataArray[section].isShow == true {
            viewModel.dataArray[section].isShow = false
        } else {
            viewModel.dataArray[section].isShow = true
        }
        
        self.listTableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .fade)
    }

}

extension SQConfineController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.dataArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.viewModel.dataArray[section].isShow == false {
            return 0
        }
        else {
            return self.viewModel.dataArray[section].listArray!.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 5 {
            return UIView()
        }
        
        let headView:SQConfineHeadView = SQConfineHeadView.instanceFromNib("SQConfineHeadView") as! SQConfineHeadView
        headView.frame = CGRect(x: 0, y: 0, width: kScreenW, height: 40)
        headView.setData(viewModel.dataArray[section], section)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapGR(_:)))
        headView.addGestureRecognizer(tap)
        return headView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 3 || section == 4 {
            return 15
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 5 {
            return 0
        }
        return 40
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SQConfineTableCell"
        let cell : SQConfineTableCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQConfineTableCell
        cell.setDataModel(indexPath.section, indexPath.row, dataModel: self.viewModel.dataArray[indexPath.section])
        return cell
        
     }
}
