//
//  SQFreeRuleCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/6.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ChameleonFramework
class SQFreeRuleCell: UITableViewCell {
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let preferredMaxWidth = UIScreen.main.bounds.size.width - 16*2
        messageLabel.preferredMaxLayoutWidth = preferredMaxWidth
        messageLabel.setContentHuggingPriority(.required, for: .vertical)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataModel(_ indexNum:NSInteger) {
        switch indexNum {
        case 0:
            self.messageLabel.text = "商品包裹期限为360天，如果超过此期限，suqibuy将自动视为放弃保管并销毁。"
            break
        case 1:
            self.messageLabel.attributedText = attributedText("相关细则:保管期限自商品状态显示为“已入库”日开始计算。在保管期内由suqibuy负责对商品进行保管并承担相应责任。（凡购买商品保质期在suqibuy保管期以内suqibuy将不承担此责任）免费仓促时间为90天，超过每天收取1元/客户/天，保管期到180天会发送站内信温馨提示提交包裹运单，满360天任未提交包裹运单或做任何反馈将视放弃商品，我们将统一对过期的商品做销毁处理。", HexColor("#0094EA")!)
            break
        case 2:
            self.messageLabel.textColor = HexColor("#666666")
            self.messageLabel.attributedText = attributedText("温馨提示:销毁的商品不再做任何提示和告知，为避免商品销毁，请您一定在保管有效期内提交包裹运单，感谢您的配合与理解！", HexColor("#FF7F02")!)

            break
        default:
            break
        }
    }

    func attributedText(_ string: String, _ firstColor:UIColor) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: string)
        attributedString.addAttributes([NSAttributedStringKey.foregroundColor:firstColor], range: NSMakeRange(0, 5))
        return attributedString
    }
}
