//
//  SQFreeRuleController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/4.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQFreeRuleController: SQBaseViewController {
    
    fileprivate lazy var listTableView : UITableView = { [weak self] in
        var contentTable : UITableView = UITableView.createBaseTableView(false, self, ["SQFreeRuleCell"])
        contentTable.rowHeight = UITableViewAutomaticDimension;
        contentTable.estimatedRowHeight = 80.0;

        return contentTable;
        }()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "免费仓储90天，超长仓储360天";
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(self.view)
        }
    }
}

extension SQFreeRuleController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SQFreeRuleCell"
        let cell : SQFreeRuleCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQFreeRuleCell
        cell.setDataModel(indexPath.row)
        return cell
    }
    
    
}

