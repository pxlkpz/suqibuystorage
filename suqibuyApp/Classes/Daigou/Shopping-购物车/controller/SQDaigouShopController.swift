//
//  SQDaigouShopController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/2.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import ChameleonFramework

class SQDaigouShopController: SQBaseViewController {
    
    private let viewModel = SQDaigouShopViewModel(initialLanguage: "SQDaigouShopController")
    private let disposeBag = DisposeBag()
    
    private let bottomNormalView:SQDaigouShopBottomNormalView = SQDaigouShopBottomNormalView.instanceFromNib("SQDaigouShopBottomNormalView") as! SQDaigouShopBottomNormalView
    private let bottomEditView:SQDaigouShopBottomEditView = SQDaigouShopBottomEditView.instanceFromNib("SQDaigouShopBottomEditView") as! SQDaigouShopBottomEditView
    
    fileprivate lazy var bgView = { () -> UIView in
        let bgView = UIView()
        bgView.backgroundColor = UIColor.clear
        bgView.frame = CGRect(x: 0, y: 0, width: kScreenW, height: kScreenH-kTopHeight-45)
        
        let noItemLabel = UILabel(frame: CGRect(x: 0, y: 0, width: kScreenW, height: 200))
        noItemLabel.text = "您尚未添加任何内容\n马上点击下面的按钮开始添加吧！"
        noItemLabel.textColor = HexColor("#333333")
        noItemLabel.textAlignment = .center
        noItemLabel.numberOfLines = 2
        noItemLabel.font = UIFont.systemFont(ofSize: 14)
        bgView.addSubview(noItemLabel)
        
        
        let addItemButton = UIButton(frame: CGRect(x: 10, y: 210, width: kScreenW - 20, height: 40))
        addItemButton.layer.cornerRadius = 4
        addItemButton.setTitle("添加代购物品", for: .normal)
        addItemButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        addItemButton.backgroundColor = HexColor("#a0a0a0")
        addItemButton.addTarget(self, action: #selector(addItemAction), for: .touchUpInside)
        addItemButton.setTitleColor(UIColor.white, for: .normal)
        bgView.addSubview(addItemButton)
        
        return bgView
    }()

    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQDaigouShopItemCell"])

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            self.view.insetsLayoutMarginsFromSafeArea = true
        } else {
            // Fallback on earlier versions
        }
        
        buildUI()
        
        setupBindings()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.requestDaigouCartitems()
    }
    
    private func setupBindings() {
        
        self.viewModel.shopListDatas.asObservable().subscribe { [weak self] model in
            self?.listTableView.reloadData()
            self?.resetUI()
            }.disposed(by: disposeBag)
        
        self.bottomNormalView.selectButton.rx.tap
            .subscribe{ [weak self] event in
                
                self?.changeSelectAll((self?.bottomNormalView.selectButton.isSelected)!)
            }
            .disposed(by: disposeBag)
        /// .Normal
        self.bottomNormalView.sureButton.rx.tap
            .subscribe{ [weak self] event in
                
                let clearVC = SQNitaoClearVC()
                clearVC.requestDic = self?.viewModel.getIds()
                self?.navigationController?.pushViewController(clearVC, animated: true);
            }
            .disposed(by: disposeBag)
        
        /// .Edit
        self.bottomEditView.deleteButton.rx.tap.subscribe { [weak self] event in
            self?.viewModel.requestDaigouBatchDelItem()
        }.disposed(by: disposeBag)
        
        self.bottomEditView.allSelctctButton.rx.tap.subscribe { [weak self]  event in
            self?.changeSelectAll(!(self?.bottomEditView.allSelctctButton.isSelected)!)
            }.disposed(by: disposeBag)
    }
    
    func changeSelectAll(_ isSelect:Bool) {
        
        switch viewModel.editSelectMode {
        case .editMode:
            for numi in 0..<viewModel.shopListDatas.value.count{
                for num in 0..<viewModel.shopListDatas.value[numi].count {
                    viewModel.shopListDatas.value[numi][num].isCanDelete = isSelect
                }
            }

            break
        case .normalMode:
            for numi in 0..<viewModel.shopListDatas.value.count{
                for num in 0..<viewModel.shopListDatas.value[numi].count {
                    viewModel.shopListDatas.value[numi][num].isActived = isSelect
                }
            }
            break
        }
        
    }
    func changeSelectSection(_ isSelect:Bool, _ section: NSInteger) {
        switch viewModel.editSelectMode {
        case .normalMode:
            for num in 0..<viewModel.shopListDatas.value[section].count {
                viewModel.shopListDatas.value[section][num].isActived = isSelect
            }
            break
        case .editMode:
            for num in 0..<viewModel.shopListDatas.value[section].count {
                viewModel.shopListDatas.value[section][num].isCanDelete = isSelect
            }
            break
        }
       
    }
    
    func changeSelectCell(_ isSelect:Bool, _ indexPath: IndexPath) {
        switch viewModel.editSelectMode {
        case .normalMode:
                self.viewModel.shopListDatas.value[indexPath.section][indexPath.row].isActived = isSelect
            break
        case .editMode:
            self.viewModel.shopListDatas.value[indexPath.section][indexPath.row].isCanDelete = isSelect

            break
        }
        
    }
    
    func resetUI() {
        //bottom
        self.title = "购物车(\(self.viewModel.shopDatasModel.value.items?.count ?? 0))"
        self.bottomNormalView.setData(self.viewModel.shopDatasModel.value, self.viewModel.shopListDatas.value)
        self.bottomEditView.setData(self.viewModel.shopDatasModel.value, self.viewModel.shopListDatas.value)

        if (self.viewModel.shopDatasModel.value.items?.count ?? 0) == 0 {
            self.view.addSubview(bgView)
        } else {
            bgView.removeFromSuperview()
        }
    }
    
    func buildUI()  {
        
        
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints {
            $0.left.right.equalTo(self.view)
            $0.top.equalTo(self.view)

            $0.bottom.equalTo(view).offset(-kSafeAreaBottomHeight-45)

        }
        
        let rightButton = UIButton(frame: CGRect(x: 10, y: 10, width: 44, height: 44))
        rightButton.setTitle("编辑", for: .normal)
        rightButton.setTitle("完成", for: .selected)
        rightButton.setTitleColor(HexColor("#333333"), for: .normal)
        rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        rightButton.addTarget(self, action: #selector(rightBarItemBtnClick(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
        
        self.view.addSubview(self.bottomNormalView)
        self.bottomNormalView.snp.makeConstraints {
            $0.left.right.equalTo(self.view)
            $0.height.equalTo(45)
            
            $0.bottom.equalTo(view).offset(-kSafeAreaBottomHeight)

        }
        self.view.addSubview(self.bottomEditView)
        self.bottomEditView.isHidden = true
        self.bottomEditView.snp.makeConstraints {
            $0.left.right.equalTo(self.view)
            $0.height.equalTo(45)
            $0.bottom.equalTo(view).offset(-kSafeAreaBottomHeight)
        }
        
    }
    
 
    @objc func addItemAction(){
        self.navigationController?.pushViewController(SQNitaoAddNewItemVC(), animated: true)
    }
    
    
    
    @objc func rightBarItemBtnClick(_ button : UIButton) {
        button.isSelected = !button.isSelected
        if button.isSelected { //编辑状态
            viewModel.editSelectMode = .editMode
            self.bottomNormalView.isHidden = true
            self.bottomEditView.isHidden = false
            
        } else {
            viewModel.editSelectMode = .normalMode
            self.bottomNormalView.isHidden = false
            self.bottomEditView.isHidden = true
            
        }
        self.listTableView.reloadData()
    }
}

extension SQDaigouShopController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.shopListDatas.value[section].count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.shopListDatas.value.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headView:SQDaigouShopSectionTopView = UIView.instanceFromNib("SQDaigouShopSectionTopView") as! SQDaigouShopSectionTopView
        headView.setData(viewModel.shopListDatas.value[section], viewModel.editSelectMode)
        
        headView.selectButton.rx.tap
            .subscribe{ [weak self] event in
                //改变数据
                self?.changeSelectSection(headView.selectButton.isSelected, section)
            }
            .disposed(by: disposeBag)
        return headView
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SQDaigouShopItemCell"
        let gouwuViewCell:SQDaigouShopItemCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouShopItemCell
        gouwuViewCell.setData(viewModel.shopListDatas.value[indexPath.section][indexPath.row], viewModel.editSelectMode)
        gouwuViewCell.selectClosure = { [weak self] (type)->Void in
            self?.changeSelectCell(gouwuViewCell.selectButton.isSelected, indexPath)
        }
        return gouwuViewCell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:SQDaigouShopItemCell = tableView.cellForRow(at: indexPath) as! SQDaigouShopItemCell
        cell.selectButtonAction(cell.selectButton)
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.viewModel.requestDaigouDelitem(indexPath)
        }
    }
}

