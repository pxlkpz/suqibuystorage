//
//  SQDaigouShopBottomNormalView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/22.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

class SQDaigouShopBottomNormalView: UIView {
    @IBOutlet weak var sureButton: UIButton!
    @IBOutlet weak var moenyLabel: UILabel!
    @IBOutlet weak var selectButton: UIButton!

    @IBAction func selectButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
    }
    @IBAction func sureButtonAction(_ sender: Any) {
       
    }
    
    func setData(_ model1:SQDaigouShopModel, _ model2: Array<Any>) {

    
        var isNum = 0
        var moneyF = 0.0
        for dataArray in model2 {
            for model3 in dataArray as! Array<SQDaigouShopItemModel> {
                

                if model3.isActived! {
                    isNum += 1
                    if var priceN = model3.itemPurchasingPrice{
                        let index = priceN.startIndex ... priceN.index(priceN.startIndex, offsetBy: 4)
                        priceN.removeSubrange(index)
                        
                        // war
                        moneyF += Double(priceN.replacingOccurrences(of: ",", with: ""))! * Double(model3.itemQty!)!
                    }
                }

            }
        }
        if isNum == model1.items?.count {
            selectButton.isSelected = true
        } else {
            selectButton.isSelected = false
        }
        sureButton.setTitle("结算(\(isNum))", for: .normal)
        sureButton.isEnabled = isNum > 0


        self.moenyLabel.text = String.init(format:"¥%.2f", moneyF)
    }
    
}
