//
//  SQDaigouShopSectionTopView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/22.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

class SQDaigouShopSectionTopView: UIView {
    
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var shopNameLabel: UILabel!
    
    
    @IBAction func selectButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    func setData(_ modelArray: Array<SQDaigouShopItemModel>,  _ mode:SQViewEditMode) {
        if modelArray.count == 0 {
            return
        }
        let model = modelArray[0]
        shopNameLabel.text = model.itemStore
        selectButton.isSelected = true

        if mode == .normalMode{
            for model1 in modelArray {
//                if let isActived = model1.isActived {
                if !model1.isActived! {
                        selectButton.isSelected = false
                        continue
                    }
//                }
            }

        } else {
            for model1 in modelArray {
                if !model1.isCanDelete! {
                    selectButton.isSelected = false
                    continue
                }
            }

        }
        
    }
}
