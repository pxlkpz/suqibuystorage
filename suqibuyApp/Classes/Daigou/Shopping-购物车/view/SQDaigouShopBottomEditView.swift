//
//  SQDaigouShopBottomEditView.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/22.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

class SQDaigouShopBottomEditView: UIView {
    
    @IBOutlet weak var allSelctctButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    func setData(_ model1:SQDaigouShopModel, _ model2: Array<Any>) {
        
        
        var isEnd = false
        var isNum = 0
        
        
        for dataArray in model2 {
            for model3 in dataArray as! Array<SQDaigouShopItemModel> {
                if model3.isCanDelete! {
                    isEnd = model3.isCanDelete!
                    isNum += 1
                }
            }
        }
        if isNum == model1.items?.count {
            deleteButton.isSelected = true
            allSelctctButton.isSelected = true

        } else {
            allSelctctButton.isSelected = false
            deleteButton.isSelected = false
        }
        deleteButton.setTitle("删除(\(isNum))", for: .normal)
        deleteButton.isEnabled = isEnd
        
    }
    
    /// order list view controller
    ///
    /// - Parameter model: SQDaigouListOrderModel
    func setData(_ model:SQDaigouListOrderModel) {
        
        
        
        guard (model.items != nil) else {
            return
        }
        
        commonHandle {isEndC,isAllSelectEndC,isNumC in
            var isEnd = isEndC
            var isAllSelectEnd = isAllSelectEndC
            var isNum = isNumC
            
            for model1 in model.items! {
                if model1.isCanDelete {
                    isEnd = model1.isCanDelete
                    isNum += 1
                }
            }
            if isNum == model.items?.count {
                isAllSelectEnd = false
            }
            
            return (isEnd, isAllSelectEnd, isNum)
        }

    }
    
    /// 等待入库
    ///
    /// - Parameter model: SQDaigouListOrderModel
    func setData(_ model:SQWarehouseListModel) {
        
        guard (model.items != nil) else {
            return
        }
        
        commonHandle {isEndC,isAllSelectEndC,isNumC in
            var isEnd = isEndC
            var isAllSelectEnd = isAllSelectEndC
            var isNum = isNumC
            
            for model1 in model.items! {
                if model1.isCanDelete {
                    isEnd = model1.isCanDelete
                    isNum += 1
                }
            }
            if isNum == model.items?.count , isNum > 0 {
                isAllSelectEnd = false
            }
            
            return (isEnd, isAllSelectEnd, isNum)
        }
        
        
    }
    
    /// 库存待寄送
    ///
    /// - Parameter model: SQMangeWaitingSendModel
    func setData(_ model:SQMangeWaitingSendModel) {
        
        
        guard (model.items != nil) else {
            return
        }
        
        commonHandle {isEndC,isAllSelectEndC,isNumC in
            var isEnd = isEndC
            var isAllSelectEnd = isAllSelectEndC
            var isNum = isNumC
            
            for model1 in model.items! {
                if model1.isCanDelete {
                    isEnd = model1.isCanDelete
                    isNum += 1
                }
            }
            if isNum == model.items?.count {
                isAllSelectEnd = false
            }

            return (isEnd, isAllSelectEnd, isNum)
        }
    }
    
    
    /// 发货订单
    ///
    /// - Parameter model: SQMangeWaitingSendModel
    func setData(_ model:SQMangeConsignmentOrderModel) {
        
        
        guard (model.items != nil) else {
            return
        }
        
        commonHandle {isEndC,isAllSelectEndC,isNumC in
            var isEnd = isEndC
            var isAllSelectEnd = isAllSelectEndC
            var isNum = isNumC
            
            for model1 in model.items! {
                if model1.isCanDelete {
                    isEnd = model1.isCanDelete
                    isNum += 1
                }
            }
            if isNum == model.items?.count {
                isAllSelectEnd = false
            }
            
            return (isEnd, isAllSelectEnd, isNum)
        }
    }
    
    
    private func commonHandle(_ block: @escaping (_ isEnd: Bool,_ isAllSelectEnd: Bool,_ isNum: Int) -> (Bool,Bool,Int)){
        let data = block(false, true, 0)
        let isEnd = data.0
        let isAllSelectEnd = data.1
        let isNum = data.2

        deleteButton.setTitle("删除(\(isNum))", for: .normal)
        deleteButton.isEnabled = isEnd
//        allSelctctButton.isEnabled = isAllSelectEnd
        allSelctctButton.isSelected = !isAllSelectEnd
    }
    
 
}

