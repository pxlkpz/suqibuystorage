//
//  SQDaigouShopItemCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/9.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

//typealias valueClosure = (_ type:String) -> Void


class SQDaigouShopItemCell: SQBaseSwiftTableCell {

    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var itemTypeLabel: UILabel!
    @IBOutlet weak var itemMoneyLabel: UILabel!
    @IBOutlet weak var itemNumberLabel: UILabel!
    
    @IBOutlet weak var selectButtonWeithConstaint: NSLayoutConstraint!
    
    @IBOutlet weak var imageViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    ///新建发货订单
    
//    var dataModel:
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func selectButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        selectClosure!("10")
    }
    
    func setData(_ model: SQDaigouShopItemModel, _ mode:SQViewEditMode) {
        itemMoneyLabel.text = (model.itemPurchasingPrice! as NSString).gtm_stringByUnescapingFromHTML()
        topTitleLabel.text = model.itemName
        itemTypeLabel.text = model.attr
        itemNumberLabel.text = "×" + model.itemQty!
        leftImageView.sd_setImage(with:  URL(string: model.itemImage!), placeholderImage: UIImage(named: "zhuanyun"))
        if mode == .normalMode {
//            if let isActived = model.isActived {
                selectButton.isSelected = model.isActived!
//            }

        } else {
            selectButton.isSelected = model.isCanDelete!
        }
    }
    
    
    var dataArray : SQMangerNewDeliveryReqeustItem? {
        didSet {
            guard let `dataArray` = dataArray else {
                return
            }
            itemMoneyLabel.text = (dataArray.price! as NSString).gtm_stringByUnescapingFromHTML()
            topTitleLabel.text = dataArray.itemName
            itemTypeLabel.text = dataArray.itemDescription
            itemNumberLabel.text = "×" + dataArray.qty!
            leftImageView.sd_setImage(with:  URL(string: dataArray.itemThumb!), placeholderImage: UIImage(named: "zhuanyun"))


            selectButton.isHidden = true
            selectButtonWeithConstaint.constant = 0
            imageViewLeftConstraint.constant = 0
            bgView.backgroundColor = UIColor.white
        }
    }
    
    
    
}
