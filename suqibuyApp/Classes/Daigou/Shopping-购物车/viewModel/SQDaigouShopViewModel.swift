//
//  SQDaigouShopViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/21.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

enum SQViewEditMode {
    case normalMode
    case editMode
}

class SQDaigouShopViewModel {

    /// Call to reload repositories.

    let shopListDatas = Variable<Array<Array<SQDaigouShopItemModel>>>([])
    let shopDatasModel = Variable<SQDaigouShopModel>(SQDaigouShopModel(JSON: ["string" : "Any"])!)
    
    let selectAllShopItem: AnyObserver<UIButton>

    var editSelectMode:SQViewEditMode = SQViewEditMode.normalMode
    
    init(initialLanguage: String) {

        let _chooseLanguage = PublishSubject<UIButton>()
        self.selectAllShopItem = _chooseLanguage.asObserver()        
        
    }
    
    func requestDaigouCartitems() {
        _ = suqiBuyProvider.rx.request(.daigouCartitems).subscribe { event in
            
            if case let .success(response) = event {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    self.dataProcess(data!)
                }
            }
        }
    }
    
    func requestDaigouBatchDelItem() {
        
        _ = suqiBuyProvider.rx.request(.daigouBatchDelItem(data: getIds()!)).subscribe { event in
            
            if case let .success(response) = event {
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    self.dataProcess(data!)
                }
                
            }
        }
    }

    func requestDaigouDelitem(_ index:IndexPath) {
        
        let idString = self.shopListDatas.value[index.section][index.row].id
        _ = suqiBuyProvider.rx.request(.daigouDelitem(data: ["id":idString ?? ""])).subscribe { event in
            if case let .success(response) = event {
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    self.dataProcess(data!)
                }
            }
        }
    }

    func dataProcess(_ data: Dictionary<String, Any>) {
        let model: SQDaigouShopModel = Mapper<SQDaigouShopModel>().map(JSON: data["result"] as! [String : Any])!
        self.shopDatasModel.value = model
        var commonModel1:SQDaigouShopItemModel?
        
        var dataArray = Array<Array<SQDaigouShopItemModel>>()
        for var model1 in model.items! {
            if model1.itemStore == nil {
                model1.itemStore = ""
            }
            if model1.isActived == nil {
                model1.isActived = false;
            }
            if commonModel1 != nil {
                if model1.itemStore != ""{
                    if (commonModel1?.itemStore?.compare(model1.itemStore!))!.rawValue != 0 {
                        commonModel1 = model1
                        dataArray.append([model1])
                    }
                } else {
                    dataArray[dataArray.count-1].append(model1)
                }
                
            }
            else {
                commonModel1 = model1;
                dataArray.append([model1])
            }
        }
        self.shopListDatas.value = dataArray

    }
    
    /// 获取ids
    ///
    /// - Returns: dic
    func getIds() -> Dictionary<String, Any>? {
        var ids:Array<String>? = []
        for dataArray in self.shopListDatas.value {
            for model3 in dataArray {
                if self.editSelectMode == .editMode {
                    if model3.isCanDelete == true {
                        ids?.append(model3.id!)
                        
                    }
                } else {
                    if model3.isActived == true {
                        ids?.append(model3.id!)
                    }
                }
            }
        }
        
        var dic = [String:Any]()
        for num in 0..<ids!.count {
            dic["ids[\(num+1)]"] = ids?[num]
        }
        
        return dic
    }
    
    
}
