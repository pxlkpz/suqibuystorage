//
//  SQDaigouShopModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/15.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ObjectMapper

struct SQDaigouShopItemModel: Mappable {
    
    var attr : String?
    var id : String?
    var isActived : Bool?
    var itemCapacity : AnyObject?
    var itemColor : AnyObject?
    var itemDescription : AnyObject?
    var itemImage : String?
    var itemName : String?
    var itemOther : AnyObject?
    var itemPurchasingPrice : String?
    var itemQty : String?
    var itemRowTotal : String?
    var itemShippingPrice : String?
    var itemSize : AnyObject?
    var itemStore : String?
    var itemUrl : String?
    var itemVersion : AnyObject?
    
    var isCanDelete : Bool?

    init?(map: Map) {
        isCanDelete = false
    }
    
    mutating func mapping(map: Map) {
        attr <- map["attr"]
        id <- map["id"]
        isActived <- map["is_actived"]
        itemCapacity <- map["item_capacity"]
        itemColor <- map["item_color"]
        itemDescription <- map["item_description"]
        itemImage <- map["item_image"]
        itemName <- map["item_name"]
        itemOther <- map["item_other"]
        itemPurchasingPrice <- map["item_purchasing_price"]
        itemQty <- map["item_qty"]
        itemRowTotal <- map["item_row_total"]
        itemShippingPrice <- map["item_shipping_price"]
        itemSize <- map["item_size"]
        itemStore <- map["item_store"]
        itemUrl <- map["item_url"]
        itemVersion <- map["item_version"]
        
    }
    
    
}

struct SQDaigouShopModel:  Mappable{
    var grandTotal : String?
    var items : [SQDaigouShopItemModel]?
    var shippingTotal : String?
 
    init?(map: Map) {

    }
    
    mutating func mapping(map: Map) {
        grandTotal <- map["grand_total"]
        items <- map["items"]
        shippingTotal <- map["shipping_total"]
    }
    
    
    
}


