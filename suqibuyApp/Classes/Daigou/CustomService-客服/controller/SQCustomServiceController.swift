//
//  SQCustomServiceController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/4.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SQCustomServiceController: SQBaseViewController {
    
    fileprivate var viewModel: SQCustomServiceViewModel = SQCustomServiceViewModel()
    private let disposeBag = DisposeBag()
    
    var infoType = "other"

    fileprivate lazy var listTableView : UITableView = { [weak self] in
        var contentTable : UITableView = UITableView.createBaseTableView(false, self, ["SQCustomServiceTableCell","SQCustomServiceWorkInfoCell", "SQMineCustomServiceTimeCell", "SQCustomServiceCommitCell"])
        return contentTable;
        }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(self.view)
        }
        
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.listTableView.reloadData()

            }.disposed(by: disposeBag)
        
        
        self.viewModel.requestSofthelpHelpinfo(infoType:self.infoType)
    }
}

extension SQCustomServiceController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return self.viewModel.dataListVariable.value.count > 1 ? self.viewModel.dataListVariable.value.count - 1 : 0
        }
        return 1;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 45
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 10
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 1 {
            let headView:SQCustomServiceCellHeadView = UIView.instanceFromNib("SQCustomServiceCellHeadView") as! SQCustomServiceCellHeadView
            return headView
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 124
        }
        else if indexPath.section == 2 {
            return 150
        }
        return 40

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cellIdentifier = "SQMineCustomServiceTimeCell"
            let cell : SQMineCustomServiceTimeCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQMineCustomServiceTimeCell
            return cell
        } else if indexPath.section == 2 {
            let cellIdentifier = "SQCustomServiceCommitCell"
            let cell : SQCustomServiceCommitCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQCustomServiceCommitCell
            cell.selectClosure = { [weak self] (type)->Void in
                if type == "10" {
                    self?.viewModel.requestSofthelpAsk(content: cell.contentTextView.text)
                }
            }

            return cell
        }
        
        let cellIdentifier = "SQCustomServiceTableCell"
        let cell : SQCustomServiceTableCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQCustomServiceTableCell
        cell.setDataModel(self.viewModel.dataListVariable.value[indexPath.row+1])
        
        
        return cell
    }
    
    
}



