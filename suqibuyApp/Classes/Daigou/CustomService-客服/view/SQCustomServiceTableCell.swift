//
//  SQCustomServiceTableCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/4.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ChameleonFramework

class SQCustomServiceTableCell: UITableViewCell {
    
    @IBOutlet weak var copyButton: UIButton!
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var leftLabel: UILabel!
    
    @IBOutlet weak var leftLabelLeading: NSLayoutConstraint!
    @IBOutlet weak var leftImageWidth: NSLayoutConstraint!
    
    var dataModel: SQCustomServiceModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        copyButton.layer.cornerRadius = 3
        copyButton.layer.masksToBounds = true
        copyButton.layer.borderWidth = 0.5
        copyButton.layer.borderColor = HexColor("#666666")?.cgColor
    }
    
    func setDataModel(_ model:SQCustomServiceModel) {
        dataModel = model
        self.leftImage.image = UIImage(named: model.imageName!)
        self.leftLabel.text =  model.titleType!+":"+model.action!
        self.copyButton .setTitle("复制"+model.titleType!, for: .normal)
    }
    
    func setDataModelOne(_ model:SQCustomServiceModel) {
        dataModel = model
        self.leftLabel.text = model.action!
        self.leftLabel.adjustsFontSizeToFitWidth = true
        self.leftImageWidth.constant = 0
        self.leftLabelLeading.constant = 0
        self.leftImage.isHidden = true
        self.copyButton.isHidden = true
    }
    
    
    @IBAction func copyButtonAction(_ sender: Any) {
        let pasteboard = UIPasteboard.general;
        pasteboard.string = dataModel?.action
        showDefaultPromptTextHUD("复制"+(dataModel?.titleType)!+"成功")
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
