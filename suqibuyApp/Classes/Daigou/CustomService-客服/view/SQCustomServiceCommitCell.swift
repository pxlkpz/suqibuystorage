//
//  SQCustomServiceCommitCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/6/3.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ChameleonFramework

class SQCustomServiceCommitCell: SQBaseSwiftTableCell {

    
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var sureButton: UIButton!
    
    fileprivate let placeholderLabel:UILabel = {
        let placeholder = UILabel.init()
        placeholder.isEnabled = false
        placeholder.text = "分享你的购物心得"
        placeholder.font = UIFont.systemFont(ofSize: 13)
        placeholder.textColor = HexColor("#999999")
        
        return placeholder
    }()

    
    @IBAction func sureButtonAction(_ sender: UIButton) {
//        requestModel?.content = self.contentTextView.text
        selectClosure!("10")
        
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        sureButton.layer.cornerRadius = 3
        sureButton.layer.masksToBounds = true
        sureButton.layer.borderWidth = 1
        sureButton.layer.borderColor = HexColor("#FF7F02")?.cgColor
        
        contentTextView.delegate = self
        contentTextView.layer.cornerRadius = 2
        contentTextView.layer.masksToBounds = true
        contentTextView.layer.borderWidth = 1
        contentTextView.layer.borderColor = HexColor("#EAEAEA")?.cgColor
        
        contentTextView.addSubview(self.placeholderLabel)
        placeholderLabel.snp.makeConstraints { (make) in
            make.top.equalTo(contentTextView).offset(8)
            make.left.equalTo(contentTextView).offset(4)
            
        }
        
        self.placeholderLabel.layoutIfNeeded()
    }

    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension SQCustomServiceCommitCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.placeholderLabel.isHidden = !textView.text.isEmpty
    }
}
