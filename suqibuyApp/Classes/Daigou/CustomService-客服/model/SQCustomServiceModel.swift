//
//  SQCustomServiceModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/4.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import ObjectMapper

class SQCustomServiceModel: Mappable {
    
    var imageName : String?
    var titleType : String?
    var action : String?

    required init?(map: Map) {}
    
    func mapping(map: Map) {
        imageName <- map["imageName"]
        titleType    <- map["titleType"]
        action    <- map["action"]
    }
}
