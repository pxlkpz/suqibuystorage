//
//  SQCustomServiceViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/4.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxSwift
import ObjectMapper

class SQCustomServiceViewModel: NSObject {

    let dataListVariable = Variable<Array<SQCustomServiceModel>>(Array())

    override init() {
    }
    
    ///请求
    func requestSofthelpHelpinfo(infoType: String) {
        
        suqiBuyProvider.request(.softhelpHelpinfo(data: ["infoType":infoType])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    /// 点赞
                    let result = data!["result"] as! Array<Dictionary<String, Any>>
                    self.dataListVariable.value = Mapper<SQCustomServiceModel>().mapArray(JSONArray: result)

                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
                
            }
        }
    }

    
    
    ///请求
    func requestSofthelpAsk(content: String) {
        
        if content == "" {
            return
        }
        suqiBuyProvider.request(.softhelpAsk(data: ["content":content])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    showDefaultPromptTextHUD("提交成功")
                }else {
                    //ALter 错误提示

                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

            }
        }
    }

    
}
