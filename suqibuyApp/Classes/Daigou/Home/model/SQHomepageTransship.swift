//
//	SQTransship.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQHomepageTransship : Mappable{

	var comments : String?
	var cost : String?
	var destination : String?
	var note : String?
	var period : String?
	var shippingMethod : String?
	var thumb : String?
	var weight : String?

 
    required init?(map: Map) {
        
    } 

	func mapping(map: Map)
	{
		comments <- map["comments"]
		cost <- map["cost"]
		destination <- map["destination"]
		note <- map["note"]
		period <- map["period"]
		shippingMethod <- map["shipping_method"]
		thumb <- map["thumb"]
		weight <- map["weight"]
		
	}
    
}
