//
//	SQRootClass.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQHomepageModel : Mappable{
    required init?(map: Map) {
    }
    

	var banner : String?
	var supportSites : SQHomepageSupportSite?
	var transships : [SQHomepageTransship]?

	func mapping(map: Map)
	{
		banner <- map["banner"]
		supportSites <- map["support_sites"]
		transships <- map["transships"]
		
	}


}
