//
//	SQSupportSite.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class SQHomepageSupportSite : Mappable{
    required init?(map: Map) {
        
    }
    

	var dangdang : Bool?
	var jd : Bool?
	var taobao : Bool?
	var tmall : Bool?


    
	func mapping(map: Map)
	{
		dangdang <- map["dangdang"]
		jd <- map["jd"]
		taobao <- map["taobao"]
		tmall <- map["tmall"]
		
	}

 
    
}
