//
//  SQDaigouViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/2.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SQDaigouViewController: SQBaseViewController {
    private let viewModel = SQDaigouHomeViewModel()
    private let disposeBag = DisposeBag()

    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQDaigouTopCell","SQDaigouShopingViewCell","SQDaigouMyViewCell", "SQDaigouCommonTransitCell", "SQDaigouCommonTransitCell5SE"])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalTo(self.view)
            make.top.equalTo(self.view).offset(-statusBarHeight)
        }
        
        self.listTableView.separatorInset = .zero
        self.listTableView.layoutMargins = .zero
        
        
        
        setupBindings()
        
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
        
        
    }
    
    func resetUI() {
        self.listTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}

extension SQDaigouViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < 3 {
            return 1
        }
        return self.viewModel.dataListVariable.value.transships?.count ?? 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 7
        } else if section == 2 {
            return 10
        } else if section == 3 {
            return 33
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 2 {
            return 10
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 3 {
            let headView:SQDaigouCommonTransitHeadView = UIView.instanceFromNib("SQDaigouCommonTransitHeadView") as! SQDaigouCommonTransitHeadView

            return headView
        }
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 166
        } else if indexPath.section == 1 {
            return 68
        } else if indexPath.section == 2 {
            return 181
        } else if indexPath.section == 3 {
            if DeviceSzie.currentSize() == .iphone5 {
                return 125
            }

            
            return 103
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cellIdentifier = "SQDaigouTopCell"
            let topViewCell : SQDaigouTopCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouTopCell
            topViewCell.setData(viewModel.dataListVariable.value)
            return topViewCell
        } else if indexPath.section == 1 {
            let cellIdentifier = "SQDaigouShopingViewCell"
            let gouwuViewCell:SQDaigouShopingViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouShopingViewCell
            gouwuViewCell.setData(viewModel.dataListVariable.value)
            return gouwuViewCell
        } else if indexPath.section == 2 {
            let cellIdentifier = "SQDaigouMyViewCell"
            let gouwuViewCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
            return gouwuViewCell!
        }
        else if indexPath.section == 3 {
            // 使用

            var cellIdentifier = "SQDaigouCommonTransitCell"

            if DeviceSzie.currentSize() == .iphone5 {
                cellIdentifier = "SQDaigouCommonTransitCell5SE"
                let gouwuViewCell: SQDaigouCommonTransitCell5SE = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouCommonTransitCell5SE
                gouwuViewCell.setData(viewModel.dataListVariable.value.transships![indexPath.row])
                return gouwuViewCell
            }
             let gouwuViewCell: SQDaigouCommonTransitCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouCommonTransitCell
            gouwuViewCell.setData(viewModel.dataListVariable.value.transships![indexPath.row])
            return gouwuViewCell
        }
        
        
        return UITableViewCell()
        
    }
    
    
}

