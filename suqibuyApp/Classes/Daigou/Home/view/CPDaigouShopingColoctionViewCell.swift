//
//  CPDaigouShopingColoctionViewCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/14.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ChameleonFramework


class CPDaigouShopingColoctionViewCell: UICollectionViewCell {
    var topImageView:UIImageView!
    var label:UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.topImageView = UIImageView.init(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.width))
        self.addSubview(self.topImageView)
        
        self.label = UILabel.init(frame: CGRect(x: -10, y: (self.topImageView?.frame.width)! + 5, width: self.frame.width+20, height: 15))
        self.label?.font = UIFont.systemFont(ofSize: 12)
        self.label?.textColor = HexColor("#111111")
        self.label?.textAlignment = .center
        self.addSubview(self.label!)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    func setData(_ model: String) {
        switch model {
        case "tmall":
            self.topImageView.image = UIImage(named: "sq_daigou_gouwu_tianmao")
            self.label.text = "天猫"
            break
        case "taobao":
            self.topImageView.image = UIImage(named: "sq_daigou_gouwu_taobao")
            self.label.text = "淘宝"
            break
        case "jd":
            self.topImageView.image = UIImage(named: "sq_daigou_gouwu_JD")
            self.label.text = "JD"
            break
        case "dangdang":
            self.topImageView.image = UIImage(named: "sq_daigou_gouwu_dangdang")
            self.label.text = "当当"
            break
        default: break
        }
    }
}

