//
//  SQDaigouCommonTransitCell5SE.swift
//  suqibuyApp
//
//  Created by pxl on 2018/4/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQDaigouCommonTransitCell5SE: UITableViewCell {
    
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var shipping_methodLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    
    @IBOutlet weak var leftImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        leftImageView.layer.cornerRadius = 5;
        leftImageView.layer.masksToBounds = true;

        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setData(_ model: SQHomepageTransship) {
        weightLabel.text = model.weight
        costLabel.text = model.cost
        shipping_methodLabel.text = model.shippingMethod
        destinationLabel.text = model.destination
        noteLabel.text = model.note
        commentsLabel.text = model.comments
        periodLabel.text = model.period
        leftImageView.sd_setImage(with:  URL(string: model.thumb!), placeholderImage: UIImage(named: "zhuanyun"))
        
    }
    
}
