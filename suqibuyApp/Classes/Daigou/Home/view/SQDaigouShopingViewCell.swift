//
//  SQDaigouShopingViewCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/2.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQDaigouShopingViewCell: UITableViewCell {
    var dataModel : SQHomepageModel? = nil
    var dataArray : Array<String> = Array()
    
    @IBOutlet weak var colloctionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        colloctionView.setCollectionViewLayout(flowLayout, animated: false, completion: nil)
        self.colloctionView.backgroundColor = UIColor.clear;
        self.colloctionView.delegate = self;
        self.colloctionView.dataSource = self;
        self.colloctionView.isScrollEnabled = true;
        
        
        self.colloctionView.register(CPDaigouShopingColoctionViewCell.self, forCellWithReuseIdentifier: "cell")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(_ model:SQHomepageModel) {
        self.dataModel = model
        self.dataArray.removeAll()
        self.colloctionView.reloadData()
        
    }
    
    func getRow() -> NSInteger{
        if self.dataModel == nil {
            return 0
        }
        var num = 0
        if self.dataModel?.supportSites?.tmall ?? false{
            num += 1
            dataArray.append("tmall")
        }
        if self.dataModel?.supportSites?.taobao ?? false{
            num += 1
            dataArray.append("taobao")
            
        }
        if self.dataModel?.supportSites?.jd ?? false{
            num += 1
            dataArray.append("jd")
            
        }
        
        if self.dataModel?.supportSites?.dangdang ?? false{
            num += 1
            dataArray.append("dangdang")
        }
        return num
    }
    
}


extension SQDaigouShopingViewCell: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return getRow()
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let colloctionCell : CPDaigouShopingColoctionViewCell = colloctionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CPDaigouShopingColoctionViewCell
        colloctionCell.setData(dataArray[indexPath.row])
        return colloctionCell
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 44, height: 60)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        return UIEdgeInsetsMake(5, (CGFloat(kScreenW)-44.0*CGFloat(dataArray.count))/CGFloat(dataArray.count+1), 0, (CGFloat(kScreenW)-44.0*CGFloat(dataArray.count))/CGFloat(dataArray.count+1))//（上、左、下、右）
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return (CGFloat(kScreenW)-44.0*CGFloat(dataArray.count))/CGFloat(dataArray.count+1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model = dataArray[indexPath.row]
        if SQUser.loginWitchController(self.viewController()) {return}

        switch model {
        case "tmall":
            let  controller = SQTaobaoVC()
            controller.type = 1
            controller.hidesBottomBarWhenPushed = true
            self.viewController().navigationController?.pushViewController(controller, animated: true)
            break
        case "taobao":
            let  controller = SQTaobaoVC()
            controller.type = 0
            controller.hidesBottomBarWhenPushed = true
            self.viewController().navigationController?.pushViewController(controller, animated: true)
            break
        case "jd":
            let  controller = SQTaobaoVC()
            controller.type = 2
            controller.hidesBottomBarWhenPushed = true
            self.viewController().navigationController?.pushViewController(controller, animated: true)

            break
        case "dangdang":
            let  controller = SQTaobaoVC()
            controller.type = 3
            controller.hidesBottomBarWhenPushed = true
            self.viewController().navigationController?.pushViewController(controller, animated: true)

            break
        default: break
        }
    }
}






