//
//  SQDaigouTopCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/2.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQDaigouTopCell: UITableViewCell {
    @IBOutlet weak var topImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ model: SQHomepageModel){
        guard model.banner != nil else {
            return
        }
        topImageView.sd_setImage(with:  URL(string: model.banner!), placeholderImage: UIImage(named: "sq_daigou_top_bg"))
    }
    
}
