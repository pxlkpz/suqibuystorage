//
//  SQDaigouMyViewCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/2.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

/// 按钮类型
///
/// - gouwuche: 购物车
/// - daigouqingdan: 代购清单
/// - xianzhi: 代购物品限制
/// - yanshi: 代购流程演示
/// - kefu: 代购专属客服
/// - gusuan: 费用估算
enum buttonType : Int {
    case gouwuche = 0
    case daigouqingdan
    case xianzhi
    case yanshi
    case kefu
    case gusuan
    
}

class SQDaigouMyViewCell: UITableViewCell {
    
    @IBOutlet weak var leftCenterLayout: NSLayoutConstraint!
    @IBOutlet weak var rightCenterLayout: NSLayoutConstraint!
    @IBOutlet weak var leftBottomCenterLayout: NSLayoutConstraint!
    @IBOutlet weak var rightBottomCenterLayout: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let constant = self.bounds.width/4 + self.bounds.width/14
        
        leftCenterLayout.constant = -constant
        rightCenterLayout.constant = constant
        
        leftBottomCenterLayout.constant = -constant
        rightBottomCenterLayout.constant = constant
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    
    
    @IBAction func buttonAcrtion(_ sender: UIButton) {
        let actionType:buttonType = buttonType(rawValue: sender.tag)!
        
        switch actionType {
        case .gouwuche:

            pushNewController(SQDaigouShopController())
            break
        case .daigouqingdan:
            
            pushNewController(SQDaigouListController())

            break
        case .xianzhi:
            let controller = SQConfineController()
            controller.title = "代购物品限制"
            pushNewController(controller)
            break
        case .yanshi:
            pushNewController(SQNitaoFlowVC())
            break
        case .kefu:
            let vc = SQCustomServiceController()
            vc.title = "代购专属客服"
            vc.infoType = "daigou"
            vc.hidesBottomBarWhenPushed = true
            self.viewController().navigationController?.pushViewController(vc, animated: true);
            break
        case .gusuan:
            pushNewController(SQzhuanyunFeeVC())
            break
        }
    }
    
    func pushNewController(_ viewController: UIViewController)  {
        if SQUser.loginWitchController(self.viewController()) {return}

        viewController.hidesBottomBarWhenPushed = true
        self.viewController().navigationController?.pushViewController(viewController, animated: true);

    }
}

