//
//  SQDaigouHomeViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/6.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

import RxSwift
import ObjectMapper

class SQDaigouHomeViewModel {
    
    let dataListVariable = Variable<SQHomepageModel>(SQHomepageModel(JSON: ["string" : "Any"])!)
    
    
    init() {
        self.requestHomepage()
        
    }
    
    func requestHomepage() {

        suqiBuyProvider.request(.homepage) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
 
                if data!["success"] as! Int == 1 {
                    let model: SQHomepageModel = Mapper<SQHomepageModel>().map(JSON: data!["result"] as! [String : Any])!
                    self.dataListVariable.value = model
                    
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
            }
        }
        
    }
        
}

