//
//  SQMineViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ObjectMapper
import RxSwift

class SQConsignmentHomeViewModel: NSObject {
    

    var currentValue = 1
    
    let dataListVariable = Variable<SQConsignmenListModel>(SQConsignmenListModel(JSON: ["string" : "Any"])!)
    var currentMainTitleValue = 0

    var sendAddComment = ("","","", Array<String>.init(),0)
    let imagesListVariable = Variable<Array<UIImage>>(Array.init())
    var totalPages = 0;
    
    /// 获取数据 TopHeadView
    ///
    /// - Returns: [SQMineListModel]
    class func getData() -> [SQMineListModel] {
        
        let dataArray : Array = NSMutableArray(contentsOfFile: Bundle.main.path(forResource: "mineListData", ofType: "plist")!)! as Array
        let dataModelArray : [SQMineListModel] = Mapper<SQMineListModel>().mapArray(JSONArray: dataArray as! [[String : Any]])
        
        return dataModelArray
    }
    
    func findGroupItmes() {
        //currentMainTitleValue == 0 集体分享 ： 1 ： 个人分享
        if currentMainTitleValue == 0 {
           self.requestGroupList(currentValue, "share")
        } else {
            self.requestGroupMe(currentValue, "share")
        }
        
    }
    
    /// 用户分析
    ///
    /// - Parameters:
    ///   - index: 页数
    ///   - typy: 类型
    func requestGroupList(_ index:NSInteger, _ type: String) {
        
        suqiBuyProvider.request(.groupList(data: ["page":"\(index)", "type":type])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQConsignmenListModel = Mapper<SQConsignmenListModel>().map(JSON: data!["result"] as! [String : Any])!
                    self.totalPages = model.totalPages!;
                    if index == 1 {
                        self.dataListVariable.value = model
                    } else {
                        if let items = model.items {
                            let array = self.dataListVariable.value.items! + items;
                            model.items = array
                            self.dataListVariable.value = model
                        }
                    }
                    
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
        
    }
    
    
    /// 我的分享
    ///
    /// - Parameters:
    ///   - index: 页数
    ///   - typy: 类型
    func requestGroupMe(_ index:NSInteger, _ typy: String) {
        
        suqiBuyProvider.request(.groupMe(data: ["page":"\(index)", "type":typy])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQConsignmenListModel = Mapper<SQConsignmenListModel>().map(JSON: data!["result"] as! [String : Any])!
                    self.totalPages = model.totalPages!;

                     if index == 1 {
                        self.dataListVariable.value = model
                    } else {
                        if let items = model.items {
                            let array = self.dataListVariable.value.items! + items;
                            model.items = array
                            self.dataListVariable.value = model
                        }
                    }
                    
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
    }
    
    
    /// 评论
    func requestGroupAddComment() {
        suqiBuyProvider.request(.groupAddComment(data: ["groupId":sendAddComment.0, "repliedGroupPostCommentId":sendAddComment.1, "commentContent":sendAddComment.2, "images":sendAddComment.3])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    ////alter
 
                    let result = data!["result"] as! Dictionary<String, Any>
                    let model:SQConsignmenListItem = self.dataListVariable.value.items![self.sendAddComment.4]
                    let valueModel = self.dataListVariable.value
                    model.repliedCount = "\(result["repliedCount"] as! Int)"
                    valueModel.items![self.sendAddComment.4] = model
                    self.dataListVariable.value = valueModel

                    if let message = result["message"] {
                            showDefaultPromptTextHUD(message as! String)
                        }

                    //clear
                    self.sendAddComment = ("","","", Array<String>.init(),0)
                    self.imagesListVariable.value.removeAll()

                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
    }
    
    /// 点赞
    func requestGroupAddStar() {
        
        suqiBuyProvider.request(.groupAddStar(data: ["groupId":sendAddComment.0])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    /// 点赞
                    let result = data!["result"] as! Dictionary<String, Any>
                    
                    let model:SQConsignmenListItem = self.dataListVariable.value.items![self.sendAddComment.4]
                    let valueModel = self.dataListVariable.value
                    model.starCount = "\(result["starCount"] as! Int)"
                    valueModel.items![self.sendAddComment.4] = model
                    self.dataListVariable.value = valueModel

                    if let message = result["message"] {
                        showDefaultPromptTextHUD(message as! String)
                    }
 
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
                
            }
        }
    }

    func requestUploadImage(_ upImage: UIImage){
        
        
        let imageData = UIImageJPEGRepresentation(upImage, 0.5)
        guard (imageData != nil) else {
            return
        }
        suqiBuyProvider.request(.uploadImage(data: ["pics":imageData! as NSData])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
//                    print(data)
                    let imagePath = (data!["result"]! as! Dictionary<String, Any>)["imagePath"] as! String
                    self.sendAddComment.3.append(imagePath)
                    self.imagesListVariable.value.append(upImage)
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
            }
        }
    }

    
}
