//
//  SQConsignmentHoemCommonCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/21.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import SnapKit
class SQConsignmentHoemCommonCell: SQBaseSwiftTableCell {
    
    @IBOutlet weak var contontLabel: UILabel!
    
    @IBOutlet weak var imageViewsBgViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imageViewBgView: UIView!
    
    
    @IBOutlet weak var headImageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var time: UILabel!

    @IBOutlet weak var lvImage: UIImageView!
    
    /// bottom
    
    @IBOutlet weak var starCountButton: UIButton!
    @IBOutlet weak var repliedCountButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    
    var imageViews: [UIImageView]? = Array()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let preferredMaxWidth = UIScreen.main.bounds.size.width - 15*2
        contontLabel.preferredMaxLayoutWidth = preferredMaxWidth
        contontLabel.setContentHuggingPriority(.required, for: .vertical)
        
        headImageView.layer.cornerRadius = 19
        headImageView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func buttonAction(_ sender: UIButton) {
        
        switch sender.tag {
        case 10:
            self.selectClosure!("fx")
            break
        case 20:
            self.selectClosure!("pl")

            break
        case 30:
            self.selectClosure!("dz")

            break
        default:
            break
        }
    }
    
    public var dataModel : SQConsignmenListItem? {
        didSet {
            guard let `dataModel` = dataModel else { return }
            guard dataModel.authorGroupId != nil else { return }
            
            contontLabel.text = self.getHTMLString(content: dataModel.content!)
            
            headImageView.sd_setImage(with:  URL(string: dataModel.authorIcon!), placeholderImage: UIImage(named: "zhuanyun"))
            userName.text = dataModel.authorName
            location.text = dataModel.authorLocation
            time.text = dataModel.createdAt

            lvImage.image = UIImage(named: "SQ_\(dataModel.authorVipCode!)")
            setDataWithImages(dataModel.images!)

            // bottom View
            starCountButton.setTitle("\(dataModel.starCount ?? "点赞")", for: .normal)
            repliedCountButton.setTitle("\(dataModel.repliedCount ?? "评论")", for: .normal)
            
        }
    }
    
    func setDataWithImages(_ images:[imageUrl]) {
        if images.count == 0 {
            self.imageViewBgView.removeAllSubViews()
            imageViewsBgViewHeight.constant = 0
            self.imageViewBgView.isHidden = true
            return
        }
        self.imageViewBgView.isHidden = false
        self.imageViewBgView.removeAllSubViews()
        let numInCell = Int((kScreenW-30)/62)

        if images.count % numInCell == 0 , images.count/numInCell > 0 {
            imageViewsBgViewHeight.constant = CGFloat((images.count/numInCell) * 65)
        } else {
            imageViewsBgViewHeight.constant = CGFloat((images.count/numInCell + 1) * 65)
        }
        for i in 0..<images.count {
            let imageUrl = images[i]
            let imageView = UIImageView()

            self.imageViewBgView.addSubview(imageView)
            imageView.sd_setImage(with:  URL(string: imageUrl.url!), placeholderImage: UIImage(named: "zhuanyun"))

            imageView.snp.makeConstraints {
                $0.top.equalToSuperview().inset((i/numInCell)*(58+7))
                $0.left.equalToSuperview().inset((i%numInCell)*(59) + 11)
                $0.size.equalTo(CGSize(width: 55, height: 58))
            }
            imageViews?.append(imageView)
        }
    }
    
    func getHTMLString(content : String) -> String {
        do{
            let attrStr = try NSAttributedString(data: (content.data(using: String.Encoding.unicode, allowLossyConversion: true)!),
                                                 options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            
            return attrStr.string
        }catch let error as NSError {
            print(error.localizedDescription)
        }
        return "";
    }


    
    
}

