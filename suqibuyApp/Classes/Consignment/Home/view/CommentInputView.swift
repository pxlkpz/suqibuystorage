import Foundation
import UIKit
import YYText
import SnapKit
import RxSwift
import RxCocoa
import ChameleonFramework
let KcommentInputViewHeight: CGFloat = 50


public typealias Action = () -> Void

class CommentInputView: UIView {

    private let disposeBag = DisposeBag()

    
     public lazy var textView: YYTextView = {
        let view = YYTextView()
        view.placeholderAttributedText = NSAttributedString(
            string: "写评论",
            attributes: [NSAttributedStringKey.foregroundColor: UIColor(red: 0.6, green: 0.6, blue: 0.6, alpha: 0.6)])

//        view.placeholder = "添加一条新回复"  // 奔溃 。。。
//        view.setCornerRadius = 17.5
        view.setCornerRadius = 5
        view.font = UIFont.systemFont(ofSize: 15)
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1).cgColor
        view.scrollsToTop = false
        view.textContainerInset = UIEdgeInsets(top: 8, left: 14, bottom: 5, right: 14)
        view.backgroundColor = #colorLiteral(red: 0.9366690335, green: 0.9459429843, blue: 0.9459429843, alpha: 1) //
        view.returnKeyType = .send
        view.enablesReturnKeyAutomatically = true
        view.delegate = self
        view.textParser = MentionedParser()
        view.tintColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2666666667, alpha: 1)
        self.addSubview(view)
        return view
    }()


    
    
    private lazy var leftImage: UIImageView = {
        let view = UIImageView()
        
        view.image = #imageLiteral(resourceName: "sq_shequ_commit");
        
        self.addSubview(view)
        return view
    }()
    
    
    private lazy var topLineView: UIView = {
        let view = UIView()
        view.backgroundColor = HexColor("#b3b3b3")
        self.addSubview(view)
        return view
    }()

    private lazy var imagesView: UIView = {
        let view = UIView()

        self.addSubview(view)
        return view
    }()
    
    
    
    public var inputViewHeight: CGFloat {
        if #available(iOS 11.0, *) {
            return KcommentInputViewHeight + AppWindow.shared.window.safeAreaInsets.bottom
        } else {
            return KcommentInputViewHeight
        }
    }
    
    public var inputViewBottom: CGFloat {
        if #available(iOS 11.0, *) {
            return AppWindow.shared.window.safeAreaInsets.bottom
        } else {
            return 0
        }
    }
    

    private struct Misc {
        static let maxLine = 5
        static let textViewContentHeight: CGFloat = KcommentInputViewHeight - 20
    }

    public var sendHandle: Action?
    public var atUserHandle: Action?
    public var uploadPictureHandle: Action?
    public var updateHeightHandle: ((CGFloat) -> Void)?


    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setup() {

        backgroundColor = .white
        
        textView.snp.makeConstraints {
            $0.top.equalToSuperview().inset(8).priority(.high)
            $0.left.equalToSuperview().inset(50).priority(.high)
            $0.right.equalToSuperview().inset(10)
            $0.bottom.equalToSuperview().inset(8)
        }


        
        
        leftImage.snp.makeConstraints {
            $0.left.equalToSuperview().inset(6)
            $0.top.equalToSuperview().inset(6)
            $0.width.equalTo(35)
            $0.height.equalTo(30)
        }
        
        topLineView.snp.makeConstraints{
            $0.top.left.right.equalToSuperview()
            $0.height.equalTo(0.5)
        }


        

    }

}

extension CommentInputView: YYTextViewDelegate {

    func textViewDidBeginEditing(_ textView: YYTextView) {
        UIView.animate(withDuration: 1) {
//            self.uploadPictureRightConstraint?.update(offset: -60)
//            self.rightSendBtfn.layoutIfNeeded()
            
        }
    }

    func textViewDidEndEditing(_ textView: YYTextView) {
//        uploadPictureRightConstraint?.update(offset: 0)
        
    }

    func textView(_ textView: YYTextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            sendHandle?()
            textView.resignFirstResponder()
            return false
        }
        return true
    }

    func textViewDidChange(_ textView: YYTextView) {

        guard let lineHeight = textView.font?.lineHeight else { return }

        // 调用代理方法
        let contentHeight = (textView.contentSize.height - textView.textContainerInset.top - textView.textContainerInset.bottom)
        let rows =  Int(contentHeight / lineHeight)

        guard rows <= Misc.maxLine else { return }

        var height = Misc.textViewContentHeight * CGFloat(rows)
        height = height < inputViewHeight ? inputViewHeight : height


        self.updateHeightHandle?(height)
    }
}
