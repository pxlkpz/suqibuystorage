//
//	SQItem.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper

class imageUrl : Mappable{
    required init?(map: Map) {}
    
    var url : String?
    
    func mapping(map: Map)
    {
        url <- map["url"]
    }
    
}


class SQConsignmenListPostTypeCount : Mappable{
    required init?(map: Map) {}

    var count : Int?
    var key : String?
    var label : String?
    
    
    func mapping(map: Map)
    {
        count <- map["count"]
        key <- map["key"]
        label <- map["label"]
        
    }
}

class SQConsignmenListModel : Mappable{
    required init?(map: Map) { }
    
    var id : String?
    var totalPages : Int?
    var page : String?
    var pageNum : Int?
    var totalResults : Int?
    var currentPage : Int?
    var postTypeCount : [SQConsignmenListPostTypeCount]?
    var items : [SQConsignmenListItem]?

    func mapping(map: Map)
    {
        id <- map["id"]
        currentPage <- map["current_page"]
        items <- map["items"]
        page <- map["page"]
        pageNum <- map["page_num"]
        postTypeCount <- map["postTypeCount"]
        totalPages <- map["total_pages"]
        totalResults <- map["total_results"]
        
    }
}

class SQConsignmenListItem : Mappable{
    required init?(map: Map) { }

	var authorGroupId : String?
	var authorIcon : String?
	var authorLocation : String?
	var authorName : String?
	var authorVipCode : String?
	var clickCount : String?
	var content : String?
	var createdAt : String?

    var images : [imageUrl]?
	var repliedCount : String?
	var starCount : String?
	var title : String?
 var id : String?
    
	func mapping(map: Map)
	{
		authorGroupId <- map["authorGroupId"]
		authorIcon <- map["authorIcon"]
		authorLocation <- map["authorLocation"]
		authorName <- map["authorName"]
		authorVipCode <- map["authorVipCode"]
		clickCount <- map["clickCount"]
		content <- map["content"]
		createdAt <- map["created_at"]
 		images <- map["images"]
		repliedCount <- map["repliedCount"]
		starCount <- map["starCount"]
		title <- map["title"]
        id <- map["id"]

	}

  
    
}
