//
//  SQConsignmentHomeViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/13.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//  社区首页

import UIKit
import RxSwift
import RxCocoa
import ChameleonFramework
import SnapKit
import MJRefresh

class SQConsignmentHomeViewController: SQBaseViewController {
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQConsignmentHomewCenterCell", "SQDaigouTopCell", "SQConsignmentHoemCommonCell"])
    private let viewModel = SQConsignmentHomeViewModel()
    private let disposeBag = DisposeBag()
    
    
    private var inputViewBottomConstranit: Constraint?
    private var inputViewHeightConstraint: Constraint?
    
    
    
    fileprivate lazy var mainTitleView : CPMainTitleView = {[weak self] in
        let titleView = CPMainTitleView()
        titleView.dataSource = self
        titleView.delegate = self
        return titleView
        }()
    
    
    private lazy var commentInputView: CommentInputView = {
        let view = CommentInputView(frame: .zero)
        view.isHidden = true
        self.view.addSubview(view)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "社区"
        
        setupConstraints()
        
        setupBindings()
        
        setupSubviews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewModel.currentValue = 1
        self.viewModel.findGroupItmes()

        mainTitleView.reloadDataSelect(self.viewModel.currentMainTitleValue)
        
    }
    
    func setupSubviews() {
        
        
        commentInputView.sendHandle = { [weak self] in
            self?.viewModel.sendAddComment.2 = (self?.commentInputView.textView.text)!
            self?.viewModel.requestGroupAddComment()
            self?.commentInputView.textView.text = ""
            self?.commentInputView.isHidden = true
            print("commentInputView.send")
        }
        
        commentInputView.uploadPictureHandle = { [weak self] in
            guard let `self` = self else { return }
            
            self.tabBarController?.tabBar.isHidden = true
            self.commentInputView.isHidden = true

            CameraTakeManager.sharedInstance().cameraSheet(in: self, handler: { (image, imagePath) in
                self.viewModel.requestUploadImage(image!)
                self.commentInputView.isHidden = false
                self.tabBarController?.tabBar.isHidden = false
                self.commentInputView.textView.becomeFirstResponder()
            }, cancelHandler: {
                self.commentInputView.isHidden = false
                self.commentInputView.textView.becomeFirstResponder()
                self.tabBarController?.tabBar.isHidden = false
            })
        }
    }
    
    func setupConstraints() {
        // Override point
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self.view)
        }
        self.listTableView.estimatedRowHeight = 80;
        self.listTableView.rowHeight = UITableViewAutomaticDimension;
        
        
        self.listTableView.separatorInset = .zero
        self.listTableView.layoutMargins = .zero
        
        self.listTableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {[weak self] in
            self?.viewModel.currentValue += 1
            self?.viewModel.findGroupItmes()
        })

        var inputViewHeight = KcommentInputViewHeight
        
        
        if #available(iOS 11.0, *) {
            inputViewHeight = KcommentInputViewHeight + view.safeAreaInsets.bottom
        }
        
        commentInputView.snp.makeConstraints {
            $0.left.right.equalToSuperview()
            self.inputViewBottomConstranit = $0.bottom.equalToSuperview().constraint
            self.inputViewHeightConstraint = $0.height.equalTo(inputViewHeight).constraint
        }
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
        
        
        Observable.of(NotificationCenter.default.rx.notification(.UIKeyboardWillShow),
                      NotificationCenter.default.rx.notification(.UIKeyboardWillHide),
                      NotificationCenter.default.rx.notification(.UIKeyboardDidShow),
                      NotificationCenter.default.rx.notification(.UIKeyboardDidHide)).merge()
            .subscribe { [weak self] notification in
                guard let `self` = self else { return }
                guard var userInfo = notification.element?.userInfo,
                    let keyboardRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
                let convertedFrame = self.view.convert(keyboardRect, from: nil)
                let heightOffset = self.view.bounds.size.height - convertedFrame.origin.y
                let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double
                self.inputViewBottomConstranit?.update(offset: -heightOffset)
                
                UIView.animate(withDuration: duration ?? 0.25) {
                    self.view.layoutIfNeeded()
                }
            }.disposed(by: disposeBag)
    }
    
    func resetUI() {
        self.listTableView.mj_footer.endRefreshing()
        if (self.viewModel.dataListVariable.value.items?.count ?? 0) == 0 {
            self.listTableView.mj_footer.isHidden = true
        } else {
            self.listTableView.mj_footer.isHidden = false
            if self.viewModel.totalPages == self.viewModel.currentValue {
                self.listTableView.mj_footer.endRefreshingWithNoMoreData()
            }
        }

        self.listTableView.reloadData()
    }
    
    func bottomThreeButton(_ type: String, _ model:SQConsignmenListItem) {
        switch type {
        case "fx":
            break
        case "pl":
            commentInputView.isHidden = false
            self.viewModel.sendAddComment.0 = model.id!
            self.commentInputView.textView.becomeFirstResponder()
            break
        case "dz":
            self.viewModel.sendAddComment.0 = model.id!
            self.viewModel.requestGroupAddStar()
            break
            
        default:
            break
        }
    }
    
}

extension SQConsignmentHomeViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < 2 {
            return 1
        }
        return self.viewModel.dataListVariable.value.items?.count ?? 0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 147
        } else if indexPath.section == 1 {
            return 39
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 10
        } else if section == 2 {
            return 44
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 1 {
            return 10
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            
            return mainTitleView
            
        }
        return UIView()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cellIdentifier = "SQDaigouTopCell"
            let topViewCell : SQDaigouTopCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQDaigouTopCell
            
            topViewCell.topImageView?.image = UIImage(named: "sq_shequ_home_top_ico")
            return topViewCell
            
            
        } else if indexPath.section == 1 {
            let cellIdentifier = "SQConsignmentHomewCenterCell"
            let topViewCell : SQConsignmentHomewCenterCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQConsignmentHomewCenterCell
            
            return topViewCell
        }
        else if indexPath.section == 2 {
            let cellIdentifier = "SQConsignmentHoemCommonCell"
            let gouwuViewCell: SQConsignmentHoemCommonCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQConsignmentHoemCommonCell
            gouwuViewCell.dataModel = self.viewModel.dataListVariable.value.items?[indexPath.row]
            gouwuViewCell.selectClosure = { [weak self] type -> Void in
                self?.viewModel.sendAddComment.4 = indexPath.row
                self?.bottomThreeButton(type,gouwuViewCell.dataModel!)
            }
            return gouwuViewCell
            
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 2 {
            let vc = SQConsignmentDetailController()
            let model = self.viewModel.dataListVariable.value.items?[indexPath.row]
            vc.groupId = (model?.id)!;
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}
extension SQConsignmentHomeViewController : CPMainTitleViewDelelgate, CPMainTitleViewDataSource{
    
    func titles(for titleView: CPMainTitleView!) -> [Any]! {
        return ["用户分享", "我的分享", "发帖"]
    }
    
    func titleView(_ titleView: CPMainTitleView!, didSelectTitleAt index: Int) {
        if index == 0 { // 用户分享
            self.viewModel.currentMainTitleValue = 0
            self.viewModel.currentValue = 1
            self.viewModel.findGroupItmes()
        } else if index == 1{
            if SQUser.isLogin() {
                self.viewModel.currentMainTitleValue = 1
                self.viewModel.currentValue = 1
                self.viewModel.findGroupItmes()
            } else {
                if SQUser.loginWitchController(self) {return}
                self.mainTitleView.reloadData()
            }
            
        } else {
            if SQUser.isLogin() {
                let vc = SQConsignmentNewInvitationViewController()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                if SQUser.loginWitchController(self) {return}
                self.mainTitleView.reloadData()
            }
            
        }
    }
}

extension SQConsignmentHomeViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)

    }
}



