//
//  SQMineViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ObjectMapper
import RxSwift

class SQConsignmentNewInvitationViewModel: SQBaseViewModel {
    
    let imagesListVariable = Variable<Array<UIImage>>(Array.init())

    var imagePathsArray = Array<String>.init()

    var postTitleContent = ("","")

    
    class func getData() -> [SQMineListModel] {
        
        let dataArray : Array = NSMutableArray(contentsOfFile: Bundle.main.path(forResource: "mineListData", ofType: "plist")!)! as Array
        let dataModelArray : [SQMineListModel] = Mapper<SQMineListModel>().mapArray(JSONArray: dataArray as! [[String : Any]])
        
        return dataModelArray
    }
    
    
    /// postType : help, share, other, friend
    /// postTitle
    /// postContent
    func requestGroupAddPost(_ postTitle: String, _ postContent: String ){
        
        suqiBuyProvider.request(.groupAddPost(data: ["images":self.imagePathsArray,"postType":"share","postTitle":postTitle, "postContent":postContent])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                     self.sureClosure!("addPost",data ?? "")
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
    }
    
    
    
    func requestUploadImage(_ upImage: UIImage){
        

        let imageData = UIImageJPEGRepresentation(upImage, 0.5)
        guard (imageData != nil) else {
            return
        }
        suqiBuyProvider.request(.uploadImage(data: ["pics":imageData! as NSData])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                     let imagePath = (data!["result"]! as! Dictionary<String, Any>)["imagePath"] as! String
                    self.imagePathsArray.append(imagePath)
                    self.imagesListVariable.value.append(upImage)
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
    }
    
    
}
