//
//  SQConsignmentHomeViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/13.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//  社区首页

import UIKit
import RxSwift
import RxCocoa
import ChameleonFramework
import SnapKit

class SQConsignmentNewInvitationViewController: SQBaseViewController {
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, [ "SQConsigmentNewInvitationBottomCell"])
    private let viewModel = SQConsignmentNewInvitationViewModel()
    let rightButton = UIButton.createRightBianji()
    
    
    private let disposeBag = DisposeBag()
    
    private var onceInt = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildUI()
        
        setupBindings()
    }
    
    func buildUI() {
        
        self.navigationItem.title = "发帖"
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(self.view)
        }
        self.listTableView.estimatedRowHeight = 80;
        self.listTableView.rowHeight = UITableViewAutomaticDimension;
        
        self.listTableView.separatorInset = .zero
        self.listTableView.layoutMargins = .zero
        
        rightButton.setTitle("发表", for: .normal)
        rightButton.addTarget(self, action: #selector(rightBarItemBtnClick(_:)), for: .touchUpInside)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightButton)
    }
    
    @objc func rightBarItemBtnClick(_ button : UIButton) {
        // 判断// 发表内容// 返回
        self.viewModel.requestGroupAddPost(self.viewModel.postTitleContent.0, self.viewModel.postTitleContent.1)
    }
    
    func setupBindings() {
        self.viewModel.imagesListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
        
        self.viewModel.sureClosure = {  [weak self] (type,data)->Void in
            self?.navigationController?.popViewController(animated: true)
        }
    }
    
    func oncesetupBinding(_ num:Int, _ cell1:SQConsigmentNewInvitationBottomCell) {
        if num ==  onceInt{
            return
        }
        onceInt = num
        //动态绑定
        Observable.combineLatest(cell1.titleTextField.rx.text.orEmpty, cell1.contentTextView.rx.text.orEmpty) { textValue1, textValue2 -> Bool in
            self.viewModel.postTitleContent.0 = textValue1
            self.viewModel.postTitleContent.1 = textValue2
            if textValue1 != "" , textValue2 != "" {
                return true
            }
            return false
            }
            .subscribe{ [weak self] model in
                self?.rightButton.isEnabled = model.element!
                //                print(model)
            }
            .disposed(by: disposeBag)
    }
    
    
    
    
    func resetUI() {
        self.listTableView.reloadData()
    }
    
    func cellSelctButtonAction(_ type: String)  {
        if type == "location" {
            
        } else {
            CameraTakeManager.sharedInstance().cameraSheet(in: self, handler: { (image, imagePath) in
                self.viewModel.requestUploadImage(image!)
                
            }, cancelHandler: {})
        }
    }
    
}

extension SQConsignmentNewInvitationViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SQConsigmentNewInvitationBottomCell"
        let bottomCell : SQConsigmentNewInvitationBottomCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQConsigmentNewInvitationBottomCell
        bottomCell.selectClosure = {  [weak self] (type)->Void in
            self?.cellSelctButtonAction(type)
        }
        bottomCell.dataModel = self.viewModel
        
        self.oncesetupBinding(10,bottomCell)
        
        return bottomCell
        
    }
    
    
}

