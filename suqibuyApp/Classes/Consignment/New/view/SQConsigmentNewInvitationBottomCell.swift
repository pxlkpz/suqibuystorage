//
//  SQConsigmentNewInvitationBottomCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/22.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ChameleonFramework
import SnapKit
class SQConsigmentNewInvitationBottomCell: SQBaseSwiftTableCell {

    @IBOutlet weak var contentTextView: UITextView!
    
    
    @IBOutlet weak var selectImageButton: UIButton!
    @IBOutlet weak var imageNumberLabel: UILabel!
    @IBOutlet weak var selectLocation: UIButton!
 
    
    @IBOutlet weak var imagesView: UIView!
    @IBOutlet weak var imageViewsBgViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var titleTextField: UITextField!

    var imageViews: [UIImageView]? = Array()

    fileprivate let placeholderLabel:UILabel = {
        let placeholder = UILabel.init()
        placeholder.isEnabled = false
        placeholder.text = "说点什么吧···"
        placeholder.font = UIFont.systemFont(ofSize: 11)
        placeholder.textColor = HexColor("#B3B3B3")
        
        return placeholder
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectImageButton.layer.borderWidth = 1
        selectImageButton.layer.borderColor = HexColor("#B3B3B3")?.cgColor
        

        selectLocation.layer.cornerRadius = 9
        selectLocation.layer.masksToBounds = true
        
        contentTextView.delegate = self
        
        contentTextView.addSubview(self.placeholderLabel)
        placeholderLabel.snp.makeConstraints { (make) in
            make.top.equalTo(contentTextView).offset(8)
            make.left.equalTo(contentTextView).offset(4)
            
        }
        
        
        
        self.placeholderLabel.layoutIfNeeded()

        // Initialization code
    }

    @IBAction func selectImageButtonAction(_ sender: Any) {
        self.selectClosure!("image")

    }
    @IBAction func locationButtonAction(_ sender: Any) {
        
        self.selectClosure!("location")
    }
    
    
    public var dataModel : SQConsignmentNewInvitationViewModel? {
        didSet {
            guard let `dataModel` = dataModel else { return }
            
            setDataWithImages(dataModel.imagesListVariable.value)
            self.imageNumberLabel.text = "\(dataModel.imagesListVariable.value.count)" + "/10"
            
            if dataModel.imagesListVariable.value.count == 10 {
                self.selectImageButton.isEnabled = false
            } else {
                self.selectImageButton.isEnabled = true
            }
        }
    }

    
    func setDataWithImages(_ images:[UIImage]) {
        
        guard images.count > 0 else {  return}
        
        if imageViews?.count == images.count {
            return
        }
        let numInCell = Int((kScreenW-30)/62)
        if images.count % numInCell == 0 , images.count/numInCell > 0{
            imageViewsBgViewHeight.constant = CGFloat((images.count/numInCell) * 65)
        } else {
            imageViewsBgViewHeight.constant = CGFloat((images.count/numInCell + 1) * 65)
        }
        imageViews?.removeAll()
        for i in 0..<images.count {
            let imageUrl = images[i]
            let imageView = UIImageView()
            self.imagesView.addSubview(imageView)
            imageView.image = imageUrl
            imageView.snp.makeConstraints {
                $0.top.equalToSuperview().inset((i/numInCell)*(58+7))
                $0.left.equalToSuperview().inset((i%numInCell)*(59) + 11)
                $0.size.equalTo(CGSize(width: 55, height: 58))
            }
            imageViews?.append(imageView)
        }
    }

    
    
}

extension SQConsigmentNewInvitationBottomCell: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        self.placeholderLabel.isHidden = !textView.text.isEmpty
    }
    
    
}
