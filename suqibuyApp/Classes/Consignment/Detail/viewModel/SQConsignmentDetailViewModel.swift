//
//  SQMineViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ObjectMapper
import RxSwift

class SQConsignmentDetailViewModel: NSObject {
    var currentItemIndex = 0

    var currentValue = 1
    var groupId: String?;
    let dataListVariable = Variable<SQConsignmentDetailModel>(SQConsignmentDetailModel(JSON: ["string" : "Any"])!)

    
    var sendAddComment = ("","")

    
    
    func findGroupPostDetails() {
        self.requestGroupPostDetail(currentValue, groupId!)
    }
    
    
    /// 我的分享 详细
    ///
    /// - Parameters:
    ///   - index: 页数
    ///   - groupId: id
    func requestGroupPostDetail(_ index:NSInteger, _ groupId: String) {
        
        suqiBuyProvider.request(.groupPostDetail(data: ["page":"\(index)", "groupId":groupId])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    let model: SQConsignmentDetailModel = Mapper<SQConsignmentDetailModel>().map(JSON: data!["result"] as! [String : Any])!
                     if index == 1 {
                        self.dataListVariable.value = model
                    } else {
                        if let items = model.items {
                            let array = self.dataListVariable.value.items! + items;
                            model.items = array
                            self.dataListVariable.value = model
                        }
                    }
                    
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
    }
    
    
    /// 评论
    func requestGroupAddComment() {
//        groupId, repliedGroupPostCommentId 可选    ,commentContent,images
        suqiBuyProvider.request(.groupAddComment(data: ["groupId": groupId!, "repliedGroupPostCommentId":sendAddComment.0, "commentContent":sendAddComment.1])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    ////alter
                    let result = data!["result"] as! Dictionary<String, Any>

                    if let message = result["message"] {
                            showDefaultPromptTextHUD(message as! String)
                        }

                    self.currentValue = 1;
                    self.findGroupPostDetails()
                    //clear
                    self.sendAddComment = ("","")

                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }

                
            }
        }
    }
    
    /// 点赞
    func requestAddCommentStar(_ index: NSInteger, _ commentId: String) {
        suqiBuyProvider.request(.groupAddCommentStar(data: ["commentId":commentId])) { (result) in
            if case let .success(response) = result {
                //解析数据
                let data = try? response.mapJSON() as! Dictionary<String, Any>
                guard data != nil else {return}
                if data!["success"] as! Int == 1 {
                    
                    let result = data!["result"] as! Dictionary<String, Any>
                    
                    if let message = result["message"] {
                        showDefaultPromptTextHUD(message as! String)
                    }
                    
                } else {
                    if let error_msg = data!["error_msg"] {
                        showDefaultPromptTextHUD(error_msg as! String)
                    }
                }
                
                
            }
        }
    }

    
    
}
