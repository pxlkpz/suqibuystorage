//
//  SQConsignmentDetailCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/5/23.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQConsignmentDetailCell: UITableViewCell {

    @IBOutlet weak var contontLabel: UILabel!
    
    @IBOutlet weak var imageViewsBgViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var imageViewBgView: UIView!
    
    
    @IBOutlet weak var headImageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var lvImage: UIImageView!
    @IBOutlet weak var number: UILabel!
    
    var imageViews: [UIImageView]? = Array()

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let preferredMaxWidth = UIScreen.main.bounds.size.width - 20*2
        contontLabel.preferredMaxLayoutWidth = preferredMaxWidth
        contontLabel.setContentHuggingPriority(.required, for: .vertical)
        
        headImageView.layer.cornerRadius = 24
        headImageView.layer.masksToBounds = true

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var dataModel : SQConsignmentDetailPostModel? {
        didSet {
            guard let `dataModel` = dataModel else { return }
            guard dataModel.authorGroupId != nil else { return }
            
            contontLabel.text = getHTMLString(content: dataModel.content!)

            
            headImageView.sd_setImage(with:  URL(string: dataModel.authorIcon!), placeholderImage: UIImage(named: "zhuanyun"))
            userName.text = dataModel.authorName
            location.text = dataModel.authorLocation
            time.text = dataModel.createdAt

            lvImage.image = UIImage(named: "SQ_\(dataModel.authorVipCode!)")

            setDataWithImages(dataModel.images!);
        }
    }
    
    func setDataWithImages(_ images:[imageUrl]) {
        if images.count == 0 {
            self.imageViewBgView.removeAllSubViews()
            imageViewsBgViewHeight.constant = 0
            self.imageViewBgView.isHidden = true
            return
        }
        self.imageViewBgView.isHidden = false
        self.imageViewBgView.removeAllSubViews()

        var imageTotalHeight = 14.0
        for i in 0..<images.count {
            let imageUrl = images[i]
            let imageView = UIImageView()
            
            self.imageViewBgView.addSubview(imageView)            
            imageView.sd_setImage(with: URL(string: imageUrl.url!), placeholderImage: UIImage(named: "zhuanyun"), options: .retryFailed) { (image, error, type, url) in

//                CGIMAGEGETWI
                let fixelW = CGFloat((image?.cgImage?.width)!)
                let fixelH =  CGFloat((image?.cgImage?.height)!)
                let kW = kScreenW - 42.0
                let kH = (fixelH/fixelW) * kW
                
                imageView.snp.makeConstraints {
                    $0.top.equalToSuperview().inset(imageTotalHeight)
                    $0.left.equalToSuperview().inset(21)
                    $0.right.equalToSuperview().inset(21)
                    $0.height.equalTo(kH)
                }
                imageTotalHeight += Double(kH) + 10
                self.imageViews?.append(imageView)
            }
            
        }
        
        imageViewsBgViewHeight.constant = CGFloat(imageTotalHeight)

    }

    func getHTMLString(content : String) -> String {
        do{
            let attrStr = try NSAttributedString(data: (content.data(using: String.Encoding.unicode, allowLossyConversion: true)!),
                                                 options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            
            return attrStr.string
        }catch let error as NSError {
            print(error.localizedDescription)
        }
        return "";
    }
    
    
}
