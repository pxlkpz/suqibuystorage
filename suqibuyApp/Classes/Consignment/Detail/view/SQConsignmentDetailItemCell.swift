//
//  SQConsignmentDetailItemCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/5/24.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit

class SQConsignmentDetailItemCell: UITableViewCell {
    @IBOutlet weak var contontLabel: UILabel!

    
    @IBOutlet weak var headImageView: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var lvImage: UIImageView!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var contentTopConstraint: NSLayoutConstraint!

    @IBOutlet weak var replyLabel: UILabel!
    @IBOutlet weak var replycommonView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let preferredMaxWidth = UIScreen.main.bounds.size.width - 57
        contontLabel.preferredMaxLayoutWidth = preferredMaxWidth
        contontLabel.setContentHuggingPriority(.required, for: .vertical)
        
        headImageView.layer.cornerRadius = 24
        headImageView.layer.masksToBounds = true
        
        replycommonView.layer.borderWidth = 1
        replycommonView.layer.borderColor = UIColor.init(hexString: "#C3C3C3")?.cgColor;
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    var dataModel : SQConsignmentDetailItemModel? {
        didSet {
            guard let `dataModel` = dataModel else { return }
            guard dataModel.authorGroupId != nil else { return }
            
            
            contontLabel.text = getHTMLString(content: dataModel.content!)


            headImageView.sd_setImage(with:  URL(string: dataModel.authorIcon!), placeholderImage: UIImage(named: "zhuanyun"))
            userName.text = dataModel.authorName
            location.text = dataModel.authorLocation
            time.text = dataModel.createdAt

            lvImage.image = UIImage(named: "SQ_\(dataModel.authorVipCode!)")

            number.text = "#" + String(dataModel.floor!)
            
            if dataModel.hasReplyComment! {
                let replyStr = "\(dataModel.replyCommentAuthor ?? ""):\(dataModel.replyCommentContent!)"
                replyLabel.text = self.getHTMLString(content: replyStr)
                replycommonView.isHidden = false
                contentTopConstraint.constant = 21;
            } else {
                replycommonView.isHidden = true
                replyLabel.text = ""
                contentTopConstraint.constant = -35;
            }
            
        }
    }
    
    func getHTMLString(content : String) -> String {
        do{
            let attrStr = try NSAttributedString(data: (content.data(using: String.Encoding.unicode, allowLossyConversion: true)!),
                                                 options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
            
           return attrStr.string
        }catch let error as NSError {
            print(error.localizedDescription)
        }
        return "";
    }

    
}
