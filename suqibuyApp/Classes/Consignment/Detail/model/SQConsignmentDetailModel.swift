//
//	Result.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper


class SQConsignmentDetailModel :  Mappable{
    required init?(map: Map) {
    }
    

	var currentPage : Int?
	var items : [SQConsignmentDetailItemModel]?
	var page : String?
	var pageNum : Int?
	var post : SQConsignmentDetailPostModel?
	var totalPages : Int?
	var totalResults : Int?



    

	func mapping(map: Map)
	{
		currentPage <- map["current_page"]
		items <- map["items"]
		page <- map["page"]
		pageNum <- map["page_num"]
		post <- map["post"]
		totalPages <- map["total_pages"]
		totalResults <- map["total_results"]
		
	}

    

}
