//
//	Post.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation
import ObjectMapper


class SQConsignmentDetailPostModel : Mappable{

	var authorGroupId : String?
	var authorIcon : String?
	var authorLocation : String?
	var authorName : String?
	var authorVipCode : String?
	var clickCount : String?
	var commentTotal : String?
	var content : String?
	var createdAt : String?
	var featuredPostTotal : String?
	var id : String?
	var isFeatured : Bool?
	var postTotal : String?
	var repliedCount : String?
	var starCount : String?
	var title : String?
	var userCreditsAmount : String?
    var images: [imageUrl]?

	 
    required init?(map: Map) {
    }
	func mapping(map: Map)
	{
		authorGroupId <- map["authorGroupId"]
		authorIcon <- map["authorIcon"]
		authorLocation <- map["authorLocation"]
		authorName <- map["authorName"]
		authorVipCode <- map["authorVipCode"]
		clickCount <- map["clickCount"]
		commentTotal <- map["commentTotal"]
		content <- map["content"]
		createdAt <- map["createdAt"]
		featuredPostTotal <- map["featuredPostTotal"]
		id <- map["id"]
		isFeatured <- map["isFeatured"]
		postTotal <- map["postTotal"]
		repliedCount <- map["repliedCount"]
		starCount <- map["starCount"]
		title <- map["title"]
		userCreditsAmount <- map["userCreditsAmount"]
        images <- map["images"]

	}

    
    

}
