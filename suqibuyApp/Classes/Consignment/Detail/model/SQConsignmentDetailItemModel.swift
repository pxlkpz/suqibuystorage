//
//	RootClass.swift
//	Model file Generated using: 
//	Vin.Favara's JSONExportV https://github.com/vivi7/JSONExport 
//	(forked from Ahmed-Ali's JSONExport)
//

import Foundation

import ObjectMapper

class SQConsignmentDetailItemModel : Mappable{

	var authorGroupId : String?
	var authorIcon : String?
	var authorLocation : String?
	var authorName : String?
	var authorVipCode : String?
	var content : String?
	var createdAt : String?
	var floor : Int?
	var hasReplyComment : Bool?
    var replyCommentContent : String?
	var id : String?
    var starCount : NSInteger?
    var replyCommentAuthor : String?



    required init?(map: Map) {
    }
	func mapping(map: Map)
	{
		authorGroupId <- map["authorGroupId"]
		authorIcon <- map["authorIcon"]
		authorLocation <- map["authorLocation"]
		authorName <- map["authorName"]
		authorVipCode <- map["authorVipCode"]
		content <- map["content"]
		createdAt <- map["created_at"]
		floor <- map["floor"]
		hasReplyComment <- map["hasReplyComment"]
		id <- map["id"]
        replyCommentContent <- map["replyCommentContent"]
        starCount <- map["starCount"]
        replyCommentAuthor <- map["replyCommentAuthor"]

		
	}

    
    

    
}
