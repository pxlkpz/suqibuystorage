//
//  SQConsignmentHomeViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/13.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//  社区首页

import UIKit
import RxSwift
import RxCocoa
import ChameleonFramework
import SnapKit

class SQConsignmentDetailController: SQBaseViewController {
    
    fileprivate lazy var listTableView : UITableView = UITableView.createBaseTableView(false, self, ["SQConsignmentDetailCell", "SQConsignmentDetailItemCell"])
    private let viewModel = SQConsignmentDetailViewModel()
    private let disposeBag = DisposeBag()
    
    
    private var inputViewBottomConstranit: Constraint?
    private var inputViewHeightConstraint: Constraint?
    
    public var groupId = "";
    
    
    private lazy var commentInputView: CommentInputView = {
        let view = CommentInputView(frame: .zero)
        view.isHidden = false
        self.view.addSubview(view)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "帖子详情"
        
        setupConstraints()
        
        setupBindings()
        
        setupSubviews()
        
        self.viewModel.groupId = groupId;
        self.viewModel.findGroupPostDetails()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func setupSubviews() {
        commentInputView.sendHandle = { [weak self] in
            self?.viewModel.sendAddComment.1 = (self?.commentInputView.textView.text)!
            self?.viewModel.requestGroupAddComment()
            self?.commentInputView.textView.text = ""
            self?.commentInputView.textView.placeholderText = "写评论"
        }
    }
    
    func setupConstraints() {
        
        // Override point
        self.view.addSubview(listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(self.view)
            make.bottom.equalTo(-commentInputView.inputViewHeight);
        }
        self.listTableView.estimatedRowHeight = 80;
        self.listTableView.rowHeight = UITableViewAutomaticDimension;
        self.listTableView.separatorInset = .zero
        self.listTableView.layoutMargins = .zero

        self.listTableView.mj_header = MJRefreshNormalHeader.init(refreshingBlock: { [weak self] in

            self?.viewModel.currentValue = 1
            self?.viewModel.findGroupPostDetails()
        })
        
        self.listTableView.mj_footer = MJRefreshAutoNormalFooter.init(refreshingBlock: {[weak self] in
            
            self?.viewModel.currentValue += 1
            self?.viewModel.findGroupPostDetails()
            
        })

        commentInputView.snp.makeConstraints {
            $0.left.right.equalToSuperview()
            self.inputViewBottomConstranit = $0.bottom.equalToSuperview().offset(-commentInputView.inputViewBottom).constraint

            self.inputViewHeightConstraint = $0.height.equalTo(KcommentInputViewHeight).constraint
        }
    }
    
    func setupBindings() {
        self.viewModel.dataListVariable.asObservable().subscribe { [weak self] model in
            self?.resetUI()
            }.disposed(by: disposeBag)
        
        Observable.of(NotificationCenter.default.rx.notification(.UIKeyboardWillShow),
                      NotificationCenter.default.rx.notification(.UIKeyboardWillHide),
                      NotificationCenter.default.rx.notification(.UIKeyboardDidShow),
                      NotificationCenter.default.rx.notification(.UIKeyboardDidHide)).merge()
            .subscribe { [weak self] notification in
                guard let `self` = self else { return }
                guard var userInfo = notification.element?.userInfo,
                    let keyboardRect = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
                let convertedFrame = self.view.convert(keyboardRect, from: nil)
                var heightOffset = self.view.bounds.size.height - convertedFrame.origin.y
                let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? Double
                if heightOffset == 0 {
                    heightOffset = self.commentInputView.inputViewBottom;
                }
                self.inputViewBottomConstranit?.update(offset: -heightOffset)
                
                UIView.animate(withDuration: duration ?? 0.25) {
                    self.view.layoutIfNeeded()
                }
            }.disposed(by: disposeBag)
    }
    
    func resetUI() {
        self.listTableView.mj_footer.endRefreshing()
        self.listTableView.mj_header.endRefreshing()
        
        if self.viewModel.dataListVariable.value.totalPages == self.viewModel.currentValue {
            self.listTableView.mj_footer.endRefreshingWithNoMoreData()
        }

        if (self.viewModel.dataListVariable.value.items?.count ?? 0) == 0 {
            self.listTableView.mj_footer.isHidden = true
        } else {
            self.listTableView.mj_footer.isHidden = false
        }

        
        self.title = self.viewModel.dataListVariable.value.post?.title;
        self.listTableView.reloadData()
    }
    
    @objc private func starAction() {
        commentInputView.textView.text = ""
        guard let model = self.viewModel.dataListVariable.value.items?[self.viewModel.currentItemIndex] else { return }

        self.viewModel.requestAddCommentStar(self.viewModel.currentItemIndex, model.id!)
    }
    
    @objc private func copyAction()  {
        commentInputView.textView.text = ""
        guard let model = self.viewModel.dataListVariable.value.items?[self.viewModel.currentItemIndex] else { return }
        let pasteboard = UIPasteboard.general;
        pasteboard.string = model.content
        showDefaultPromptTextHUD("复制成功")
    }
    
    @objc private func replyCommentAction()  {
        commentInputView.textView.text = ""
        guard let model = self.viewModel.dataListVariable.value.items?[self.viewModel.currentItemIndex] else { return }
        self.viewModel.sendAddComment.0 = model.id!
        self.commentInputView.textView.placeholderText = "回复" + model.authorName! + ":"
        self.commentInputView.textView.becomeFirstResponder()
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        // 如果当前 textView 是第一响应者，则忽略自定义的 MenuItemAction， 不在 Menu视图上显示自定义的 item
        if !isFirstResponder, [#selector(starAction),
                               #selector(copyAction),
                               #selector(replyCommentAction)].contains(action) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }


}

extension SQConsignmentDetailController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1;
        }
        return self.viewModel.dataListVariable.value.items?.count ?? 0
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.viewModel.dataListVariable.value.post == nil ? 0 : 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cellIdentifier = "SQConsignmentDetailCell"
            let gouwuViewCell: SQConsignmentDetailCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQConsignmentDetailCell
            gouwuViewCell.dataModel = self.viewModel.dataListVariable.value.post
            return gouwuViewCell
        } else if indexPath.section == 1 {
            let cellIdentifier = "SQConsignmentDetailItemCell"
            let gouwuViewCell: SQConsignmentDetailItemCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! SQConsignmentDetailItemCell
            gouwuViewCell.dataModel = self.viewModel.dataListVariable.value.items?[indexPath.row]
            
            return gouwuViewCell
            
        }
        return UITableViewCell();
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0 {
            return
        }
        // 强制结束 HeaderView 中 WebView 的第一响应者， 不然无法显示 MenuView
        if commentInputView.textView.isFirstResponder {
            view.endEditing(true)
        }
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        

        self.viewModel.currentItemIndex = indexPath.row
        let menuVC = UIMenuController.shared
        if menuVC.isMenuVisible { return }

        
        var targetRectangle = cell.frame
        targetRectangle.origin.y = targetRectangle.height * 0.4
        targetRectangle.size.height = 1
        
        let thankItem = UIMenuItem(title: "点赞", action: #selector(starAction))

        let copyItem = UIMenuItem(title: "复制", action: #selector(copyAction))
        let replyItem = UIMenuItem(title: "回复", action: #selector(replyCommentAction))


        menuVC.setTargetRect(targetRectangle, in: cell)
        menuVC.menuItems = [thankItem, copyItem, replyItem]
        menuVC.setMenuVisible(true, animated: true)
    }

    
}

extension SQConsignmentDetailController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        
    }
}




