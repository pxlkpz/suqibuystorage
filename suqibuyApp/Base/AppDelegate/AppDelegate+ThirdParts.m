//
//  AppDelegate+ThirdParts.m
//  yunbo2017
//
//  Created by pxl on 2017/8/17.
//  Copyright © 2017年 pxl. All rights reserved.
//

#import "AppDelegate+ThirdParts.h"
#import <UMMobClick/MobClick.h>
#import "PayPalMobile.h"
#import "JPUSHService.h"

#import <UMMobClick/MobClick.h>

@interface AppDelegate ()<JPUSHRegisterDelegate>

@end


@implementation AppDelegate (ThirdParts)

#pragma Register Third Parks


- (void)registerThreeParts {
    [self registerPayPalMobile];
    [self registerMobClick];
}

- (void)registerPayPalMobile {
    //#warning "Enter your credentials"
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction:PayPalMobileProduction,
                                                           PayPalEnvironmentSandbox : PayPalMobileSandbox}];
}

- (void)registerMobClick {
    UMConfigInstance.appKey = UMMobAPPKey;
    [MobClick startWithConfigure:UMConfigInstance];

}


@end
