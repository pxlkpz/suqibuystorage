//
//  AppDelegate+ThirdParts.h
//  yunbo2017
//
//  Created by pxl on 2017/8/17.
//  Copyright © 2017年 pxl. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (ThirdParts)

- (void)registerThreeParts;

@end
