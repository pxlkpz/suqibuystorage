//
//  AppDelegate.h
//  MBPeopleBaseApp
//
//  Created by MOLBASE on 16/2/22.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "SQRootViewController.h"
#import "WXApi.h"

#define MB_AppDelegate  ((AppDelegate*)[UIApplication sharedApplication].delegate)

@interface AppDelegate : UIResponder <UIApplicationDelegate, WXApiDelegate>
{
    SQRootViewController* viewCtrl;
}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain) SQRootViewController *viewCtrl;

//-(void) autoLoginMothed;

@end

