//
//  AppDelegate.m
//  MBPeopleBaseApp
//
//  Created by MOLBASE on 16/2/22.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import "AppDelegate.h"
#import "MBGuidePageVC.h"
#import "SQLoginVC.h"
#import "SQUser.h"
#import <AlipaySDK/AlipaySDK.h>
//#import "PayPalMobile.h"
#import <UMMobClick/MobClick.h>

#import "AppDelegate+XHLaunchAd.h"
#import "SQRootViewController.h"
#import "XHLaunchAd.h"
#import "JPUSHService.h"
#import "AppDelegate+ThirdParts.h"
// iOS10注册APNs所需头文件
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif


@interface AppDelegate ()<JPUSHRegisterDelegate>

@end

@implementation AppDelegate

@synthesize viewCtrl = _viewCtrl;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [NSThread sleepForTimeInterval:1.0];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor colorWithHexString:NAV_BAR_COLOR];
    
    //添加引导页
    [self addGuidePageVC];
    
    
    [self registerThreeParts];
    
    [WXApi registerApp:wxApiRegister];

    //JPush 3.0.2 start
    //Required
    //notice: 3.0.0及以后版本注册可以这样写，也可以继续用之前的注册方式
    JPUSHRegisterEntity * entity = [[JPUSHRegisterEntity alloc] init];
    entity.types = JPAuthorizationOptionAlert|JPAuthorizationOptionBadge|JPAuthorizationOptionSound;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        // 可以添加自定义categories
        // NSSet<UNNotificationCategory *> *categories for iOS10 or later
        // NSSet<UIUserNotificationCategory *> *categories for iOS8 and iOS9
    }
    [JPUSHService registerForRemoteNotificationConfig:entity delegate:self];
    
    // Required
    // init Push
    // notice: 2.1.5版本的SDK新增的注册方法，改成可上报IDFA，如果没有使用IDFA直接传nil
    // 如需继续使用pushConfig.plist文件声明appKey等配置内容，请依旧使用[JPUSHService setupWithOption:launchOptions]方式初始化。
    [JPUSHService setupWithOption:launchOptions appKey:appKey
                          channel:channel
                 apsForProduction:isProduction
            advertisingIdentifier:nil];
    // JPuch end

    return YES;
}

#pragma -mark GuidePage引导图
- (void)addGuidePageVC
{
    NSString *key = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    
    NSString *lastVersionCode = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    NSString *currentVersionCode = key;
    
    if ([lastVersionCode isEqualToString:currentVersionCode]) // 加载中广告图片
    {
        if ([SQUser isLogin]) {
            [self autoLogin];
        } else {

            [self MianViewControllerMothedNoMake];
            [self advertisementMothed_XHLanch];
            [self.window makeKeyAndVisible];
        }
    } else { //引导图
        [[NSUserDefaults standardUserDefaults] setObject:currentVersionCode forKey:key];
        [[NSUserDefaults standardUserDefaults] synchronize];
        MBGuidePageVC *new = [[MBGuidePageVC alloc] init];
        new.startBlock = ^(BOOL shared){
            ///FIXME
            [self MianViewControllerMothed];
        };
        self.window.rootViewController = new;
        [self.window makeKeyAndVisible];
    }
}

/**
 登录请求。
 */
-(void) autoLogin
{
    NSString *user_token = [SQUser loadmodel].user_token;
    
    NSDictionary *dic = @{@"user_token":user_token};
    [self MianViewControllerMothedNoMake];

    [XHLaunchAd setWaitDataDuration:3];//请求广告数据前,必须设置

    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_USER_AUTO_LOGIN WithParameter:dic WithFinishBlock:^(id returnValue) {
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    SQUserModel * model = [SQUserModel mj_objectWithKeyValues:result];
                    [SQUser save:model];
                    [self setupXHLaunchAd:model.ad_image];
                 }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
                ///FIXME :
//                [self autoLoginMothed];
            }
        }
    } WithErrorBlock:^(id errorCode) {
        showDefaultPromptTextHUD(@"网络异常");
//        [self autoLoginMothed];
    } WithFailedBlock:^{
        showDefaultPromptTextHUD(@"网络异常");
//        [self autoLoginMothed];
    }];
    
    [self.window makeKeyAndVisible];

}

-(void)MianViewControllerMothedNoMake{

    SQRootViewController*rootVc = [[SQRootViewController alloc]init];
    self.window.rootViewController  = rootVc;
    
}

-(void)MianViewControllerMothed{
    [self MianViewControllerMothedNoMake];
    [self.window makeKeyAndVisible];
}

- (void)onlineConfigCallBack:(NSNotification *)note {
    NSLog(@"online config has fininshed and note = %@", note.userInfo);
}

#pragma mark- JPUSHRegisterDelegate

// iOS 10 Support
- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(NSInteger))completionHandler {
    // Required
    NSDictionary * userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler(UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以选择设置
}

// iOS 10 Support

- (void)jpushNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler {
    // Required
    NSDictionary * userInfo = response.notification.request.content.userInfo;
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        [JPUSHService handleRemoteNotification:userInfo];
    }
    completionHandler();  // 系统要求执行这个方法
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@"-----通知 noti %@", userInfo);
    
    // Required, iOS 7 Support
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"JPushNotifaction" object:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    NSLog(@"---通知的-====---%@",userInfo.description);
    [self apnsDataGo2VC:userInfo];
    
    
    // Required,For systems with less than or equal to iOS6
    [JPUSHService handleRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    /// Required - 注册 DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
}


// log NSSet with UTF8
// if not ,log will be \Uxxx
- (NSString *)logDic:(NSDictionary *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString *str =
    [NSPropertyListSerialization propertyListFromData:tempData
                                     mutabilityOption:NSPropertyListImmutable
                                               format:NULL
                                     errorDescription:NULL];
    return str;
}



#pragma mark - apnsDataGo2VC
- (void) apnsDataGo2VC:(NSDictionary*)userInfo
{
    if (!wal_is_null(userInfo)) {
        
    }
}



- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //Optional
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    //    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    

    [application setApplicationIconBadgeNumber:0];
    [application cancelAllLocalNotifications];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
//https://t1.shtag.com/uploads/global/20170102/148332029021629.png

//    [self example02_imageAd_localData];
//    [self.window makeKeyAndVisible];

    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    //    [self saveContext];
}

#pragma mark - Core Data stack



#pragma mark //支付宝调用客户端成功后的回调
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    
    [WXApi handleOpenURL:url delegate:self];
    
    
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AlipayWEICHATPAYRESULT" object:resultDic];
            
        }];
    }
    
    
    return YES;
}

//独立客户端回调函数
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    return [WXApi handleOpenURL:url delegate:self];
    
    
}

// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options
{
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            NSLog(@"result = %@",resultDic);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AlipayWEICHATPAYRESULT" object:resultDic];
            
        }];
    }
    
    [WXApi handleOpenURL:url delegate:self];
    
    return YES;
}

#pragma mark ------ 微信支付回调
-(void) onResp:(BaseResp*)resp{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"WEICHATPAYRESULT" object:resp];
}




@end
