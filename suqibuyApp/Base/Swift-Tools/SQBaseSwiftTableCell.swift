//
//  SQBaseSwiftTableCell.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/6.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
typealias valueClosure = (_ type:String) -> Void

class SQBaseSwiftTableCell: UITableViewCell {
    public var selectClosure : valueClosure?

}
