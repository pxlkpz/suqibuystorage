//
//  SQBaseViewModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/18.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation

typealias viewModelClosure = (_ type:String, _ data: Any) -> Void


class SQBaseViewModel {
    public var sureClosure : viewModelClosure?

}
