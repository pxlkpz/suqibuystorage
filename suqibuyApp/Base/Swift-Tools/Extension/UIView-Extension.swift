//
//  UIView-Extension.swift
//  suqibuyApp
//
//  Created by pxl on 2018/2/4.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import UIKit
import ChameleonFramework
extension UIView {
 
    /** swift方法   在 View 中实现这个方法即可
     
     *viewController () -> (UIViewController) 作用：根据调用这个方法的对象 来 获取他的控制器对象
     
     */
    
    func viewController () -> UIViewController {
        //1.通过响应者链关系，取得此视图的下一个响应者
        var next:UIResponder?
        next = self.next!
        repeat {
            //2.判断响应者对象是否是视图控制器类型
            if ((next as?UIViewController) != nil) {
                return (next as! UIViewController)
            }else {
                next = next?.next
            }
        } while next != nil
        return UIViewController()
    }
    
    func removeAllSubViews() {
        while self.subviews.count > 0 {
            var child = self.subviews.last
            if (child?.isKind(of: UIImageView.self))! {
                (child as! UIImageView).image = nil
            }
            child?.removeFromSuperview()
            child = nil
            
        }
    }

    class func instanceFromNib(_ nibName:String) -> UIView {
        return UINib(nibName: nibName, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }

    class func initWithNoneDataView(_ noneString:String) -> UIView {
        let bgView = UIView()
        bgView.backgroundColor = UIColor.clear
        bgView.frame = CGRect(x: 0, y: 0, width: kScreenW, height: kScreenH-kTopHeight)
        let noItemLabel = UILabel(frame: CGRect(x: 0, y: 0, width: kScreenW, height: kScreenH-kTopHeight))
        noItemLabel.text = noneString
        noItemLabel.textColor = HexColor("#333333")
        noItemLabel.textAlignment = .center
        noItemLabel.numberOfLines = 0
        noItemLabel.font = UIFont.systemFont(ofSize: 14)
        bgView.addSubview(noItemLabel)
        
        return bgView
    }
    
    
    
    
    var layoutGuide: UILayoutGuide {
        if #available(iOS 11, *) {
            return safeAreaLayoutGuide
        } else {
            return layoutMarginsGuide
        }
    }
    
    var layoutInsets: UIEdgeInsets {
        if #available(iOS 11, *) {
            return safeAreaInsets
        } else {
            return layoutMargins
        }
    }
    
    /// 给View加上圆角
    @IBInspectable var setCornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = newValue > 0
        }
    }
    

}
