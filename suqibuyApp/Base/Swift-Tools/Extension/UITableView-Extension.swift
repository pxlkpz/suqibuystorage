//
//  UITableView-Extension.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ChameleonFramework

extension UITableView {
    class func createBaseTableView(_ showVertical: Bool, _ baseSelf:UIViewController?, _ nibName:Array<String> ) -> UITableView{
        let contentTable : UITableView = UITableView()
        contentTable.tableFooterView = UIView()
        contentTable.delegate = (baseSelf as! UITableViewDelegate)
        contentTable.dataSource = (baseSelf as! UITableViewDataSource)
        contentTable.backgroundColor = UIColor(r: 224.0, g: 224.0, b: 224.0)
        contentTable.showsVerticalScrollIndicator = false
        for name in nibName {
            contentTable.register(UINib(nibName: name, bundle: nil), forCellReuseIdentifier: name)
        }
        return contentTable
    }
}
