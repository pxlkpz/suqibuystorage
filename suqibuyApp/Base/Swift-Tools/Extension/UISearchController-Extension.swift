//
//  UISearchController-Extension.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/13.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ChameleonFramework
extension UISearchController {
    class func createSearchController(_ placeholder:String) -> UISearchController{
        let searchController = UISearchController(searchResultsController: nil)
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = placeholder
        searchController.searchBar.tintColor = UIColor.black
        searchController.searchBar.isTranslucent = true
        searchController.searchBar.searchBarStyle = .prominent;
//        searchController.view.frame = CGRect(x: 0, y: 0, width: kScreenH, height: kScreenH)

        
        searchController.searchBar.barTintColor = HexColor("#F4FAFF")

        let searchField :UITextField = searchController.searchBar.value(forKey: "_searchField") as! UITextField
        searchField.font = UIFont.systemFont(ofSize: 12)
        searchField.setValue(UIFont.systemFont(ofSize: 12), forKeyPath: "_placeholderLabel.font")
        searchField.setValue(HexColor("#666666"), forKeyPath: "_placeholderLabel.textColor")

        return searchController
    }
    
}
