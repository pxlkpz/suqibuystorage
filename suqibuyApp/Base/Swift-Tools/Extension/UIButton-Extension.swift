//
//  UIButton-Extension.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/1.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ChameleonFramework
extension UIButton {
    class func createRightBianji() -> UIButton{
        let rightButton = UIButton(frame: CGRect(x: 10, y: 10, width: 44, height: 44))
        rightButton.setTitle("编辑", for: .normal)
        rightButton.setTitle("完成", for: .selected)
        rightButton.setTitleColor(HexColor("#333333"), for: .normal)
        rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        
        return rightButton
        
    }
    
}
