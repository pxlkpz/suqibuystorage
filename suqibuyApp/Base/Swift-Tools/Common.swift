//
//  Common.swift
//  LXLive
//
//  Created by pxl on 2017/1/31.
//  Copyright © 2017年 pxl. All rights reserved.
//

import UIKit

let kStatusBarH : CGFloat = UIApplication.shared.statusBarFrame.size.height
let kNavigationBarH : CGFloat = 44
let kTabbarH : CGFloat = 48
let kTopHeight : CGFloat = kStatusBarH + kNavigationBarH

let kScreenW = UIScreen.main.bounds.width
let kScreenH = UIScreen.main.bounds.height




let kSafeAreaBottomHeight = kScreenH == 812.0 ? 34 : 0

struct DeviceSzie {
    
    enum DeviceType {
        case iphone4
        case iphone5
        case iphone6
        case iphone6p
    }
    
    //判断屏幕类型
    static func currentSize() -> DeviceType {
        let screenWidth = UIScreen.main.bounds.width
        let screenHeight = UIScreen.main.bounds.height
        
        switch (screenWidth, screenHeight) {
        case (320, 480),(480, 320):
            return .iphone4
        case (320, 568),(568, 320):
            return .iphone5
        case (375, 667),(667, 375):
            return .iphone6
        case (414, 736),(736, 414):
            return .iphone6p
        default:
            return .iphone6
        }
    }
}


//#define SafeAreaBottomHeight (kWJScreenHeight == 812.0 ? 34 : 0)

