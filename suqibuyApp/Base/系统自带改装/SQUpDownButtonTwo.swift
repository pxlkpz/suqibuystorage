//
//  SQUpDownButtonTwo.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/13.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//  字体上下居中

import UIKit

class SQUpDownButtonTwo: UIButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel?.textAlignment = .center
        self.titleLabel?.numberOfLines = 0
        self.titleLabel?.lineBreakMode = .byWordWrapping
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        var tempImageViewRect = self.imageView?.frame
        tempImageViewRect?.origin.y = 0
        
        tempImageViewRect!.origin.x = (self.bounds.size.width-(tempImageViewRect?.size.width)!)/2
        self.imageView?.frame = tempImageViewRect!
        
        //title
        var tempLabelRect = self.titleLabel?.frame
        tempLabelRect?.origin.x = 0
        
        //Y = 整体高度-imageH-titleLabelH
        let labelY_H = self.bounds.size.height - (self.imageView?.frame.size.height)! - (self.titleLabel?.frame.size.height)!
        if  labelY_H > 0 && Int((tempLabelRect?.size.height)!) > 0{
            tempLabelRect?.origin.y = (self.imageView?.frame.size.height)! + labelY_H/2
        } else {
            tempLabelRect?.origin.y = (self.imageView?.frame.size.height)!
            tempLabelRect?.size.height = self.bounds.size.height - (self.imageView?.frame.size.height)!
        }
        
        
        tempLabelRect?.size.width = self.bounds.size.width
        self.titleLabel?.frame = tempLabelRect!
    }
    
}

