//
//  NSDictionary+SQLog.m
//  suqibuyApp
//
//  Created by pxl on 2018/2/23.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

#import "NSDictionary+SQLog.h"

@implementation NSDictionary (SQLog)
#if DEBUG
- (NSString *)descriptionWithLocale:(nullable id)locale{
    
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding];
}
#endif

@end
