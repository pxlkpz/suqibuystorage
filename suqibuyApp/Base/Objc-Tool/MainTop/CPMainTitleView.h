//
//  CPMainTitleView.h
//  yunbo2017
//
//  Created by Shengtao Liu on 2018/1/12.
//  Copyright © 2018年 pxl. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CPMainTitleView;

@protocol CPMainTitleViewDataSource <NSObject>

- (NSArray *)titlesForMainTitleView:(CPMainTitleView *)titleView;

@end

@protocol CPMainTitleViewDelelgate <NSObject>

- (void)titleView:(CPMainTitleView *)titleView didSelectTitleAtIndex:(NSInteger)index;

@end

@interface CPMainTitleView : UIView

@property (weak, nonatomic) id <CPMainTitleViewDataSource> dataSource;
@property (weak, nonatomic) id <CPMainTitleViewDelelgate> delegate;

- (void)reloadData;


- (void)reloadDataSelectIndex:(NSInteger)itemIndex;
@end
