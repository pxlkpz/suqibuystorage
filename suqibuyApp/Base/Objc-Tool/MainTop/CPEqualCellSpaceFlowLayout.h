//
//  CPEqualCellSpaceFlowLayout.h
//  yunbo2017
//
//  Created by Shengtao Liu on 2018/1/12.
//  Copyright © 2018年 pxl. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, CPAlignType){
    CPAlignWithLeft,
    CPAlignWithCenter,
    CPAlignWithRight
};

@interface CPEqualCellSpaceFlowLayout : UICollectionViewFlowLayout

//两个Cell之间的距离
@property (nonatomic, assign) CGFloat betweenOfCell;

//cell对齐方式
@property (nonatomic, assign) CPAlignType cellType;

- (instancetype)initWthType:(CPAlignType)cellType;

//全能初始化方法 其他方式初始化最终都会�走到这里
- (instancetype)initWithType:(CPAlignType)cellType betweenOfCell:(CGFloat)betweenOfCell;

@end
