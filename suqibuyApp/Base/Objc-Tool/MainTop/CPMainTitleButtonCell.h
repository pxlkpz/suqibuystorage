//
//  CPMainTitleButtonCell.h
//  yunbo2017
//
//  Created by Shengtao Liu on 2018/1/12.
//  Copyright © 2018年 pxl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CPMainTitleButtonCell : UICollectionViewCell

- (void)setTitle:(NSString *)title indexPath:(NSIndexPath *)indexPath;
@property (strong, nonatomic) UILabel *titleLabel;


@end
