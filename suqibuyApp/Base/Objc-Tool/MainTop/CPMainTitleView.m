//
//  CPMainTitleView.m
//  yunbo2017
//
//  Created by Shengtao Liu on 2018/1/12.
//  Copyright © 2018年 pxl. All rights reserved.
//

#import "CPMainTitleView.h"
#import "CPMainTitleButtonCell.h"
#import "CPEqualCellSpaceFlowLayout.h"

@interface CPMainTitleView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) UICollectionView *collectionView;/** 集合视图 */

@property (nonatomic,strong) UIView *lineView;


@end

@implementation CPMainTitleView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(KScreenWidth - 100, 40);
}

- (void)layoutSubviews {
    [super layoutSubviews];

    CPEqualCellSpaceFlowLayout *layout = (CPEqualCellSpaceFlowLayout *)self.collectionView.collectionViewLayout;
    [layout invalidateLayout];

    self.collectionView.frame = self.bounds;
    if (!CGSizeEqualToSize(self.bounds.size, [self intrinsicContentSize])) {
        [self invalidateIntrinsicContentSize];
    }
    CGFloat width = self.collectionView.collectionViewLayout.collectionViewContentSize.width;
    if (width != 0 && width != self.bounds.size.width) {
        CGRect frame = self.frame;
        frame.size.width = width;
        self.frame = frame;
        self.collectionView.frame = self.bounds;
    }
    
    [self.collectionView addSubview:self.lineView];
}

- (void)setupUI
{
    self.bounds = CGRectMake(0, 0, KScreenWidth - 100, 40);
    [self addSubview:self.collectionView];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self);
    }];
    
}

- (void)reloadData
{
    [self.collectionView reloadData];
    
    NSIndexPath * indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    [self.collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:(UICollectionViewScrollPositionNone)];
    CPMainTitleButtonCell* cell = (CPMainTitleButtonCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    [self bottomLineViewAnimation:cell isAnimation:NO];

}

- (void)reloadDataSelectIndex:(NSInteger)itemIndex {
//    [self.collectionView reloadData];
    
    NSIndexPath * indexPath = [NSIndexPath indexPathForItem:itemIndex inSection:0];
    
    
    [self.collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:(UICollectionViewScrollPositionNone)];
    CPMainTitleButtonCell* cell = (CPMainTitleButtonCell*)[self.collectionView cellForItemAtIndexPath:indexPath];
    [self bottomLineViewAnimation:cell isAnimation:NO];
}

- (void)setDataSource:(id<CPMainTitleViewDataSource>)dataSource
{
    if (_dataSource == dataSource) {
        return;
    }
    _dataSource = dataSource;
    
    [self.collectionView reloadData];
    [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:NO scrollPosition:(UICollectionViewScrollPositionNone)];
 
}

- (void)bottomLineViewAnimation:(CPMainTitleButtonCell*)selectLabelCell isAnimation:(BOOL)isAnimation
{

    CGRect rect = [selectLabelCell.titleLabel.text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]} context:nil];

    CGFloat x = selectLabelCell.frame.origin.x + (selectLabelCell.frame.size.width - rect.size.width)/2;
    CGFloat y = selectLabelCell.frame.origin.y + selectLabelCell.frame.size.height - 1;
    CGFloat width = rect.size.width;
    CGFloat height = 2;
    
    if (isAnimation) {
        [UIView animateWithDuration:0.5 animations:^{
            self.lineView.frame = CGRectMake(x, y, width, height);
        }];

    } else {
        self.lineView.frame = CGRectMake(x, y, width, height);
    }
}



#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if ([self.dataSource respondsToSelector:@selector(titlesForMainTitleView:)] ) {
        return [self.dataSource titlesForMainTitleView:self].count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CPMainTitleButtonCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CPMainTitleButtonCell" forIndexPath:indexPath];
    NSString *title = [self.dataSource titlesForMainTitleView:self][indexPath.item];
    [cell setTitle:title indexPath:indexPath];
    if (indexPath.row == 0) {
        [self bottomLineViewAnimation:cell isAnimation:NO];
    }
    return cell;
}


#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(titleView:didSelectTitleAtIndex:)]) {
        [self.delegate titleView:self didSelectTitleAtIndex:indexPath.item];
    }
    CPMainTitleButtonCell* cell = (CPMainTitleButtonCell*)[collectionView cellForItemAtIndexPath:indexPath];
    [self bottomLineViewAnimation:cell isAnimation:YES];
}


#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
//    NSString *title = [self.dataSource titlesForMainTitleView:self][indexPath.item];
//    CGRect rect = [title boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 30) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:17]} context:nil];
//    CGFloat width = rect.size.width + 40;
    /// FIXME : 需要调整成动态环境
    CGFloat width = KScreenWidth/3 - 20;
    return CGSizeMake(width, self.bounds.size.height);
}


#pragma mark - Getter

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        CPEqualCellSpaceFlowLayout *layout = [[CPEqualCellSpaceFlowLayout alloc] initWithType:CPAlignWithCenter betweenOfCell:0];
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.scrollEnabled = NO;
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        [_collectionView registerClass:[CPMainTitleButtonCell class] forCellWithReuseIdentifier:@"CPMainTitleButtonCell"];
    }
    return _collectionView;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];

        _lineView.backgroundColor = [UIColor colorWithHexString:@"#0081CC"];
    }
    return _lineView;
}

@end
