//
//  CPMainTitleButtonCell.m
//  yunbo2017
//
//  Created by Shengtao Liu on 2018/1/12.
//  Copyright © 2018年 pxl. All rights reserved.
//

#import "CPMainTitleButtonCell.h"

@interface CPMainTitleButtonCell ()


@end

@implementation CPMainTitleButtonCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

- (void)setupUI
{
    self.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.titleLabel];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];

    self.titleLabel.textColor = selected ? [UIColor colorWithHexString:@"#0081CC"]:[UIColor colorWithHexString:@"#333333"];
}

- (void)setTitle:(NSString *)title indexPath:(NSIndexPath *)indexPath
{
    self.titleLabel.text = title;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor colorWithHexString:@"#333333"];
        _titleLabel.font = [UIFont systemFontOfSize:17];
    }
    return _titleLabel;
}


@end
