//
//  CPBaseXiBView.h
//  yunbo2017
//
//  Created by pxl on 2017/8/23.
//  Copyright © 2017年 pxl. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ViewModelBlock) (NSUInteger Type, id returnValue);

@interface SQBaseXiBView : UIView

- (instancetype)initWithNibName:(NSString *)nibNameOrNil;

@property (nonatomic,strong) ViewModelBlock viewBlock;

- (void)setDataWithModel:(id)model;


@end
