//
//  CPBaseXiBView.m
//  yunbo2017
//
//  Created by pxl on 2017/8/23.
//  Copyright © 2017年 pxl. All rights reserved.
//

#import "SQBaseXiBView.h"

@implementation SQBaseXiBView

- (instancetype)initWithNibName:(NSString *)nibNameOrNil {
    NSArray *nibs = [[NSBundle bundleForClass:[self class]] loadNibNamed:nibNameOrNil owner:nil options:nil];
    self = [nibs objectAtIndex:0];
 
    return self;
}

- (void)setDataWithModel:(id)model{
    
}

@end
