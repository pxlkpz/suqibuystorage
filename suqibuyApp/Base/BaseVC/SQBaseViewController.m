//
//  MBBaseViewController.m
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import "SQBaseViewController.h"

#import "AppDelegate.h"
#import "MBProgressHUD.h"

@interface SQBaseViewController ()<MBProgressHUDDelegate>
@property (nonatomic, retain) MBProgressHUD* HUD;

@end

@implementation SQBaseViewController

-(void)dealloc
{
    NSLog(@"%@\n🌚退出🌚", NSStringFromClass([self class]));
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithHexString:bg_white];//此处要改成宏定义的背景颜色值
    
    if (self!=[self.navigationController.viewControllers objectAtIndex:0]) {
        [self setLeftNavigationItemWithCustomView:[self createBackButton]];
    }
}

- (UIButton *) createBackButton{
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:NAV_BACK_IMG] forState:UIControlStateNormal];
    [backButton setFrame:CGRectMake(0, 0, 40 ,45)];
    [backButton addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -35, 0, 0)];
    
    //获取当前手机的系统版本
    if (IOS_VERSION >= 7.0)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    return backButton;
}


- (void)setTitle:(NSString *)title
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18.0];
    label.textAlignment = NSTextAlignmentLeft;
    label.textColor=[UIColor colorWithHexString:@"#333333"];
    label.shadowOffset = CGSizeMake(1, 1);
    self.navigationItem.titleView = label;
    label.text = title;
    [label sizeToFit];
}





#pragma mark - NavgationItem 相关
- (void)setLeftNavigationItemWithCustomView:(UIView*)cusView
{
    UIBarButtonItem *m_buttonItem = [[UIBarButtonItem alloc] initWithCustomView:cusView];
    self.navigationItem.leftBarButtonItem = m_buttonItem;
}

- (void)setRightNavigationItemWithCustomView:(UIView*)cusView
{
    UIBarButtonItem *m_buttonItem = [[UIBarButtonItem alloc] initWithCustomView:cusView];
    self.navigationItem.rightBarButtonItem = m_buttonItem;
}

-(void)setLeftButton:(NSString*)buttonTitle action:(SEL)action
{
    UIBarButtonItem *leftBtn = [[UIBarButtonItem alloc] initWithTitle:buttonTitle style:UIBarButtonItemStylePlain target:self action:action];
    self.navigationItem.rightBarButtonItem = leftBtn;
}

-(__kindof UIButton*)setRightButton:(NSString*)buttonTitle action:(SEL)action
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:15.0];
    CGSize size = [buttonTitle sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:16.0]}];
    [button setFrame:CGRectMake(0, 0, MAX((size.width+30),40), MAX(30,size.height))];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, 0)];
    [button setTitle:buttonTitle forState:UIControlStateNormal];
    if (action != nil) {
        [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    }
    [self setRightNavigationItemWithCustomView:button];
    return button;
}

- (void)setLeftBackButtonWithAction:(SEL)action
{
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(0, 0, 60, 45)];
    [backButton addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [backButton setImage:[UIImage imageNamed:NAV_BACK_IMG] forState:UIControlStateNormal];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -35, 0, 0)];
    [self setLeftNavigationItemWithCustomView:backButton];
}

- (void)setRightButtonWithImage:(NSString*)imageName action:(SEL)action
{
    UIImage* image = [UIImage imageNamed:imageName];
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setBackgroundImage:image forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    [self setRightNavigationItemWithCustomView:button];
}

- (void)onBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 菊花相关
- (void)showLoadingHUD
{
    [self showHUD:@"加载中..."];
}

- (void)hideLoadingHUD
{
    [self hideHUD];
}

- (void)showHUD:(NSString*)text
{
    if (self.HUD==nil) {
        UIWindow* window = [[UIApplication sharedApplication].delegate window];
        self.HUD = [[MBProgressHUD alloc] initWithView:window];
        [window addSubview:self.HUD];
        
        self.HUD.mode = MBProgressHUDModeCustomView;
        self.HUD.delegate = self;
        self.HUD.labelFont = [UIFont systemFontOfSize:12.0f];
        self.HUD.customView = [[UIActivityIndicatorView alloc]
                               initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [(UIActivityIndicatorView*)self.HUD.customView startAnimating];
        [self.HUD show:YES];
    }
    self.HUD.labelText = text;
}

- (void)showHUD:(NSString *)text hideAfterDelay:(CGFloat)delay
{
    [self showHUD:text];
    [self performSelector:@selector(hideHUD) withObject:nil afterDelay:delay];
}

- (void)hideHUD
{
    [self.HUD hide:YES];
}

#pragma mark - MBProgressHUDDelegate
- (void)hudWasHidden:(MBProgressHUD *)hud
{
    [self.HUD removeFromSuperview];
    self.HUD = nil;
}

#pragma mark - ShowMainView
-(void)showDynamicVC
{
    [MB_AppDelegate.viewCtrl setSelectedIndex:0];
}

-(void)showContactsVC
{
    [MB_AppDelegate.viewCtrl setSelectedIndex:1];
}

-(void)showUserCenterVC
{
    [MB_AppDelegate.viewCtrl setSelectedIndex:2];
}



@end
