//
//  MBGuidePageVC.m
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import "MBGuidePageVC.h"

@interface MBGuidePageVC ()<UIScrollViewDelegate>
{
    UIScrollView *scrollView;
    UIScrollView *smallScrollView;
}

@end

@implementation MBGuidePageVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, screen_width, screen_height)];
    scrollView.contentSize = CGSizeMake(screen_width*4, screen_height);
    scrollView.delegate = self;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    for (int i=0; i<4; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(screen_width*i, 0, screen_width, screen_height)];
//        imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"slide_%d.jpg",i+1]];
        if (IsiPhoneX) {
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"slide_%d_x.jpg",i+1]];
        } else {
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"slide_%d.jpg",i+1]];
        }

        [scrollView addSubview:imageView];
    }
    
    UIImageView *smallImageView = [[UIImageView alloc] initWithFrame:CGRectMake(screen_width/4, screen_height/4, screen_width/2, screen_height/2)];
    smallImageView.userInteractionEnabled = NO;
    [self.view addSubview:smallImageView];
    
    smallScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(smallImageView.bounds), CGRectGetHeight(smallImageView.bounds))];
    smallScrollView.backgroundColor = [UIColor grayColor];
    smallScrollView.scrollEnabled = NO;
    smallScrollView.delegate = self;
    smallScrollView.contentSize = CGSizeMake(CGRectGetWidth(smallImageView.bounds)*4, CGRectGetHeight(smallImageView.bounds));
    smallScrollView.pagingEnabled = YES;
    smallScrollView.bounces = NO;
    for (int i=0; i<4; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(smallImageView.bounds)*i, 0, CGRectGetWidth(smallImageView.bounds), CGRectGetHeight(smallImageView.bounds))];
        if (IsiPhoneX) {
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"slide_%d_x.jpg",i+1]];
        } else {
            imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"slide_%d.jpg",i+1]];
        }
        
        [smallScrollView addSubview:imageView];
    }
    
    
    
    [smallImageView addSubview:smallScrollView];
    [self.view addSubview:scrollView];
    [self.view bringSubviewToFront:smallImageView];
    //隐藏小图
    smallImageView.hidden = YES;
    
    //  添加页数控制视图 new = alloc + init
    UIPageControl *pageControl = [UIPageControl new];
    //不要加到滚动视图中， 会随着滚动消失掉
    [self.view addSubview:pageControl];
    //    设置常用属性,距离屏幕下方60像素。
    pageControl.frame = CGRectMake(0, self.view.frame.size.height - 36, self.view.frame.size.width, 36);
    //    设置圆点的个数
    pageControl.numberOfPages = 4;
    //    设置没有被选中时圆点的颜色
    pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:gray];
    //    设置选中时圆点的颜色
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:text_yellow];
    //    关闭分页控件的用户交互功能
    pageControl.userInteractionEnabled = YES;
    
    [pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
    // 为了检测滚动视图的偏移量，引入代理
    scrollView.delegate = self;
    
    pageControl.tag = 1000;
    //为最后一页添加按钮
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    [btn setTitle:@"开始使用" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithHexString:text_white] forState:UIControlStateNormal];
    btn.layer.cornerRadius = 4;
    btn.layer.borderWidth = 1;
    btn.layer.borderColor = [UIColor colorWithHexString:simple_button_yellow].CGColor;
    btn.layer.backgroundColor = [UIColor colorWithHexString:simple_button_yellow].CGColor;
    //因为是滚动视图最后一页，所以要添加到滚动视图中
    [scrollView addSubview:btn];
    
    btn.frame = CGRectMake(0, 0, screen_width - 160, 32);
    //把按钮添加到第四页的中心
    btn.center = CGPointMake((4 - 0.5) * scrollView.frame.size.width, scrollView.frame.size.height - 66);
    if (IPHONE3_5INCH) {
        btn.center = CGPointMake((4 - 0.5) * scrollView.frame.size.width, scrollView.frame.size.height - 40);
    }
    [btn addTarget:self action:@selector(goToNext) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)pageTurn:(UIPageControl*) aPageControl
{
    NSInteger current = aPageControl.currentPage;
    [scrollView setContentOffset:CGPointMake(current * screen_width, 0) animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)ascrollView{
    //减速拖拽结束了 视图自动把当前正在交互的滚动视图对象传过来
    //结束拖拽的时候调用
    if (ascrollView.contentOffset.x / screen_width == 3) {
        UIButton *btnLogIn = [UIButton buttonWithType:UIButtonTypeSystem];
        [btnLogIn setFrame:CGRectMake(60,screen_height-125, screen_width-120, 40)];
        [btnLogIn addTarget:self action:@selector(goToNext) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnLogIn];
    }
}

- (void)goToNext
{
    if (_startBlock) {
        _startBlock(YES);
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView{
    if (aScrollView == scrollView) {
        CGPoint point = scrollView.contentOffset;
        point.y = point.y*4;
        smallScrollView.contentOffset = CGPointMake(scrollView.contentOffset.x/2, scrollView.contentOffset.y);
    }
    
    UIPageControl *pageControl = (UIPageControl *)[self.view viewWithTag:1000];
    //取得偏移量
    CGPoint point = scrollView.contentOffset;
    //根据滚动的位置来决定当前是第几页
    //可以用 round()  C语言方法进行 四舍五入操作
    NSInteger index = round(point.x/scrollView.frame.size.width);
    //设置分页控制器的当前页面
    pageControl.currentPage = index;
}

@end
