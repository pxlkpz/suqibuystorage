//
//  MBBaseViewController.h
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import <UIKit/UIKit.h>
/** 已经登录的回调 */
typedef void (^AlreadyLoginBlock)(void);
/** 登录成功的回调 */
typedef void (^LoginSuccessBlock)(void);
@interface SQBaseViewController : UIViewController


- (void)setLeftButton:(NSString*)buttonTitle action:(SEL)action;
-(__kindof UIButton*)setRightButton:(NSString*)buttonTitle action:(SEL)action;

- (void)setLeftBackButtonWithAction:(SEL)action;
- (void)setRightButtonWithImage:(NSString*)imageName action:(SEL)action;
/**
 *	请求加载提示，显示涡轮
 */
-(void)showLoadingHUD;

/**
 *	请求结束时隐藏加载提示，隐藏涡轮
 */
-(void)hideLoadingHUD;

/**
 *	加载时文字提示
 *
 *	@param	text	提示文字
 */
-(void)showHUD:(NSString*)text;

/**
 *	加载时文字提示
 *
 *	@param	text	提示文字
 *  @param  delay   隐藏
 */
-(void)showHUD:(NSString*)text hideAfterDelay:(CGFloat)delay;

/**
 *	隐藏提示文字
 */
-(void)hideHUD;

/**
 *	返回
 */
- (void)onBack;

/**
 *  显示动态
 */
- (void)showDynamicVC;
/**
 *  显示人脉
 */
- (void)showContactsVC;

/**
 *  显示个人中心
 */
- (void)showUserCenterVC;

- (void)setLeftNavigationItemWithCustomView:(UIView*)cusView;
- (UIButton *) createBackButton;
@end
