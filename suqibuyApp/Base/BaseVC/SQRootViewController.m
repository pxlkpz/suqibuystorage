//
//  ViewController.m
//  MBPeopleBaseApp
//
//  Created by MOLBASE on 16/2/22.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import "SQRootViewController.h"

#import "SQnitaoVC.h"
#import "SQdaishouVC.h"
#import "SQzhuanyunVC.h"


#import "UITabBar+PxlBadge.h"
#import "SQWoMyNotiVC.h"
#import "suqibuyApp-Swift.h"
#import "SQTransferMainHomeViewController.h"

@interface SQRootViewController ()
{
    NSInteger   _didSelectIndex;
    
    UINavigationController* _nav1;
    //代收
    UINavigationController* _nav2 ;
    //转运
    UINavigationController* _nav3 ;
    //我
    UINavigationController* _nav4 ;
}
@property (nonatomic, retain) SQDaigouViewController *daigouVC;
@property (nonatomic, retain) SQTransferMainViewController *daishouVC;
@property (nonatomic, retain) SQConsignmentHomeViewController *zhuangyunVC;
@property (nonatomic, retain) SQMineViewController *woVC;

@end

@implementation SQRootViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    _didSelectIndex = 0;
    [self initWithRootViewCtrls];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(JpushNoti:) name:@"JPushNotifaction" object:nil];
}
-(void)JpushNoti:(NSNotification*)noti{
    NSDictionary * dic = [noti object];
    NSString * aimtype = [dic objectForKey:@"aimtype"];
    if (!aimtype) {
        return;
    }
    if ([aimtype isEqualToString:@"me"]) {
        [self showWo];
    }else if ([aimtype isEqualToString:@"index"]){
        [self showHome];//noticedetail
    }else if ([aimtype isEqualToString:@"notice_detail"]){
        [self showNotiAdd];
    }
    
}
#pragma -mark --设置tabbar试图控制器
-(void)initWithRootViewCtrls
{
    //代购
    self.daigouVC = [[SQDaigouViewController alloc] init];
    _nav1 = [self createNaviController:self.daigouVC andTitle:TABBAR_ONE andImageName:@"sq_base_bar_daigou"];
    
    //转运
    self.daishouVC = [[SQTransferMainViewController alloc]init];
    _nav2 = [self createNaviController:self.daishouVC andTitle:TABBAR_TWO andImageName:@"sq_base_bar_zhuanyun"];
    
    //社区
    self.zhuangyunVC = [[SQConsignmentHomeViewController alloc] init];
    _nav3 = [self createNaviController:self.zhuangyunVC andTitle:TABBAR_THREE andImageName:@"sq_base_bar_shequ"];

    //我
    self.woVC = [[SQMineViewController alloc] init];
    _nav4 = [self createNaviController:self.woVC andTitle:TABBAR_FOUR andImageName:@"sq_base_bar_mine"];
    
    
    //文字未选中
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:[UIColor grayColor],                                                                                                              NSForegroundColorAttributeName, nil]
                                             forState:UIControlStateNormal];
    //文字选中
    [[UITabBarItem appearance] setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIColor colorWithHexString:@"#0081cc"],
      NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
//    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];

    
    CGRect frame = self.tabBar.frame;
    frame.size.height = 49;
    self.tabBar.frame = frame;
    self.tabBar.tintColor = [UIColor colorWithHexString:@"#0081cc"];
    self.tabBar.translucent = NO;//是否透明
    self.delegate = self;
    self.viewControllers = [NSArray arrayWithObjects:
                            _nav2,
                            _nav1,
                            _nav3,
                            _nav4,
                            nil];
    
    [self.tabBar setBackgroundImage:[UIImage imageNamed:@"back_image_icon"]];
    
    //#warning  2 need clear
    SQUserModel *user  = [SQUser loadmodel];

    NSString* count = user.msg_count;
    if (count.length != 0 && [count integerValue] > 0)
    {
        [self.woVC.tabBarController.tabBar showBadgeOnItemIndex:3];
    }
}

- (UINavigationController*)createNaviController:(UIViewController *)myController andTitle:(NSString *)titleName andImageName:(NSString *)imageName {
    
    myController.tabBarItem.title = titleName;
    myController.tabBarItem.image = [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    myController.tabBarItem.selectedImage = [UIImage imageNamed:imageName];
    [myController.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"Helvetica" size:12.0], NSFontAttributeName, [UIColor grayColor], NSForegroundColorAttributeName, nil]
                                           forState:UIControlStateNormal];
    UINavigationController* navi1 = [[UINavigationController alloc] initWithRootViewController:myController];
//    navi1.navigationBar.barTintColor = [UIColor colorWithHexString:mainBg];
    navi1.navigationBar.translucent = NO;
    
    [self.navigationController.navigationBar setBackgroundImage:[self imageWithColor:[UIColor whiteColor]] forBarMetrics:(UIBarMetricsDefault)];
   
    
//    navi1.navigationBar.backIndicatorImage = [UIImage imageNamed:@"navi_bar_background_white"];
    navi1.navigationBar.backgroundColor = [UIColor whiteColor];
    [navi1.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    
    navi1.interactivePopGestureRecognizer.enabled = YES;
    navi1.interactivePopGestureRecognizer.delegate = (id)self;

    return navi1;
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)showWo
{
    self.selectedIndex = 3;
    [_nav4 popToRootViewControllerAnimated:YES];
}

- (void)showHome
{
    self.selectedIndex = 0;
    [_nav1 popToRootViewControllerAnimated:YES];
    
}

- (void)showNotiAdd
{
    self.selectedIndex = 3;
    
    [_nav4 setViewControllers:@[_nav4.viewControllers[0],[[SQWoMyNotiVC alloc] init]]];
    
}
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if (self.selectedIndex == 0 && _didSelectIndex != self.selectedIndex) {
        
    }
    if (self.selectedIndex == 1 && _didSelectIndex != self.selectedIndex) {
        
    }
    
    if (self.selectedIndex == 3)
    {
        [self.daigouVC.tabBarController.tabBar hideBadgeOnItemIndex:3];
    }
    _didSelectIndex = self.selectedIndex;
}

@end
