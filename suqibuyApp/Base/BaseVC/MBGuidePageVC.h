//
//  MBGuidePageVC.h
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBGuidePageVC : UIViewController

@property (nonatomic, copy) void (^startBlock)(BOOL shared);

@end
