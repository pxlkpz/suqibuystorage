//
//  CPNavigationController.m
//  yunbo2017
//
//  Created by pxl on 2017/8/14.
//  Copyright © 2017年 pxl. All rights reserved.
//

#import "SQNavigationController.h"


@interface SQNavigationController ()<UIGestureRecognizerDelegate>

@end

@implementation SQNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}


/**
 * 可以在这个方法中拦截所有push进来的控制器
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.childViewControllers.count > 0) { // 如果push进来的不是第一个控制器
        
        }
    
    // 这句super的push要放在后面, 让viewController可以覆盖上面设置的leftBarButtonItem
    [super pushViewController:viewController animated:animated];
    
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    //导航的rootViewController关闭右滑返回功能
    if (self.viewControllers.count <= 1)
    {
        return NO;
    }
    
    return YES;
}




@end
