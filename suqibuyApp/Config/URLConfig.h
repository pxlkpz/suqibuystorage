//
//  URLConfig.h
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#ifndef URLConfig_h
#define URLConfig_h

/*正式接口
 */
#define API_DOMAIN_DIS          @"https://www.suqibuy.com/apiv3"

/*测试接口
 */
#define API_DOMAIN_DEV          @"http://t1.shtag.com/apiv3"


/**切换不同接口的开关
 */
#define APP_MD5TOKEN_VALUE             @"hdkCMA!qxH8Qv&IVZHEn2ar#oqCW!%mM"//加密token
#define APP_MD5TOKEN_KEY             @"TOKEN"//加密token

#define httpHeader @"Mozilla/5.0 (Windows NT 6.1; WoW64; rv:9.0.1) Gecko/20100101 Firefox/9.0.1"
//正式网页
#define API_DOMAIN_DIS_HTML     @"https://www.suqibuy.com/h5/"
//测试网页
#define API_DOMAIN_DEV_HTML     @"https://t1.shtag.com/h5/"

#define API_MAIN_URL_HTML            API_DOMAIN_DIS_HTML //改测试网址接口只改这个地方(网站)******//////

#define API_MAIN_URL            API_DOMAIN_DEV //改测试接口只改这个地方******//////

//＝＝＝＝＝＝＝充值===============／／
#define wxApiRegister @"wx9c3dcc358768f20f" // 微信

//paypal
#define PayPalMobileProduction @"ASBb2p_hi4d0_1Q3hTUDaE1Jt5YwpgZfZTTgLDHPE7NMuNRWMkLGXP6HncMz-QfiFX5-u3tAgLTIO3_J"
#define PayPalMobileSandbox @"AZZkptPQTnAqAz7_RFiAsTnVmYIZmGlHcpUtjFeLSXnrknMhnnK51h7sDBSopp-ErTnpft0cxsAkKvPR"
//模式选择 PayPalEnvironmentSandbox 测试模式  PayPalEnvironmentProduction 正式模式
#define kPayPalEnvironment PayPalEnvironmentProduction

//友盟
#define UMMobAPPKey @"576a1b1ce0f55a7a8b002925"

//极光
static NSString *appKey = @"b77c1e45fd2fb3e550982c10";
static NSString *channel = @"Publish channel";
static BOOL isProduction = YES;// * @param isProduction 是否生产环境. 如果为开发状态,设置为 NO; 如果为生产状态,应改为 YES.

//----------介绍用网址-------------------

#define Web_helpDetail38 @"https://www.suqibuy.com/h5/help/detail/38" //包裹产生问题如何赔偿

//----------通用-------------------
#define API_Ads @"/user/ads"

//==========登录注册部分=============///
///网络检测
#define API_SOFTHELP_NETWORK @"/softhelp/network"

///登录
#define API_USER_LOGIN @"/user/login"

///自动登录
#define API_USER_AUTO_LOGIN @"/user/auto_login"

///注册
#define API_USER_SIGNUP @"/user/signup"

///找回密码
#define API_USER_FORGOTTENPWD @"/user/forgottenpwd"

//==========代购部分=============///
///代购购物车
#define API_DAIGOU_CARTITEMS @"/daigou/cartitems"

///代购购物车，商品数量
#define API_DAIGOU_CARTCOUNT @"/daigou/cartcount"

///代购购物车，商品详情抓取
#define API_DAIGOU_FETCHURL @"/daigou/fetchurl"

#define API_DAIGOU_ANALYZE @"/daigou/analyze"


///添加新的物品
#define API_DAIGOU_ADDCART @"/daigou/addcart"

///删除单个物品
#define API_DAIGOU_DELITEM @"/daigou/delitem"

///代购结算中心
#define API_DAIGOU_QUOTE @"/daigou/quote"

///我的优惠券
#define API_DAIGOU_MYCOUPON @"/daigou/mycoupon"

///使用优惠券
#define API_DAIGOU_USERCOUPON @"/daigou/usecoupon"

///使用积分
#define API_DAIGOU_USERCREDIT @"/daigou/usercredit"

///保存代购订单
#define API_DAIGOU_SAVE @"/daigou/save"

///支付代购订单
#define API_DAIGOU_PAYMENT @"/daigou/payment"

///代购订单详情
#define API_DAIGOU_DETAIL @"/daigou/detail"

///取消订单
#define API_DAIGOU_CANCEL @"/daigou/cancel"

///订单列表
#define API_DAIGOU_LIST @"/daigou/list"

//==========转运部分=============///

#define API_DAISHOU_Suqiaddress @"/warehouse/suqiaddress"

//仓库列表
#define API_CANGKU_WarehouseList @"/warehouse/list"

#define API_CANGKU_WarehousePackageList @"/warehouse/packagelist"


#define API_CANGKU_WarhouseDelete @"/warehouse/delete"

#define API_CANGKU_WarehousePackagedetail @"/warehouse/packagedetail"

//获取快递数组
#define API_DAISHOU_Getexpress @"/softhelp/getexpress"

//代收包裹创建
#define API_DAISHOU_SAVE @"/daishou/save"


#define API_DAISHOU_warehouse_payment @"/warehouse/payment"

#define API_Zhuanyun_Dishou_uploadImage @"/daishou/uploadImage"



///代收包裹预报 提交
//#define API_DAIGOU_LIST_Getexpress @"/softhelp/getexpress"

#endif /* URLConfig_h */

//==========发货中心部分=============///

//批量添加所有可转运物品
#define API_ZHUANGYUN_Transship_Addall @"/transship/addall"

//加入转运购物车
#define API_ZHUANGYUN_Transship_Addtocart @"/transship/addtocart"

//可转运物品列表 /transship/expressitemsgroup
#define API_ZHUANYUN_TransshipExpressitems @"/transship/expressitemsgroup"
//转运购物车列表
#define API_ZHUANYUN_TransshipCartitems @"/transship/cartitems"

// 取消订单
#define API_ZHUANYUN_transship_cancel @"/transship/cancel"
//购买保险
#define API_ZHUANYUN_transship_insurancepost @"/transship/insurancepost"
//使用积分
#define API_ZHUANYUN_transship_usecredit @"/transship/usecredit"

//结算
#define API_ZHUANYUN_transship_saveorder @"/transship/saveorder"

//结算
#define API_ZHUANYUN_transship_paymentpost @"/transship/paymentpost"




//结算页面
#define API_ZHUANYUN_TransshipQuote @"/transship/quote"

//可用的转用方式
#define API_ZHUANYUN_transship_shippingmethods @"/transship/shippingmethods"
//选择转用方式
#define API_ZHUANYUN_transship_selectshippingmethod @"/transship/selectshippingmethod"



//删除转动购物车中的物品
#define API_ZHUANYUN_TransshipDelitem @"/transship/delitem"

#define API_ZHUANYUN_TransshipSelectaddress @"/transship/selectaddress"
//转运订单列表
#define API_ZHUANYUN_transship_orderlist @"/transship/orderlist"

//可用优惠券
#define API_ZHUANYUN_transship_mycoupons @"/transship/mycoupons"

//使用优惠券
#define API_ZHUANYUN_transship_usecoupon @"/transship/usecoupon"
// 获取转运订单详情
#define API_ZHUANYUN_transship_detail @"/transship/detail"






//==========我=============///
//用户地址列表
#define API_WO_Useraddress_grouplist @"/useraddress/grouplist"

#define API_WO_Useraddress_list @"/useraddress/list"

//用户地址	用户地址详情
#define API_WO_useraddress_detail @"/useraddress/detail"

//用户地址	创建/编辑地址
#define API_WO_UserAddress_Save @"/useraddress/save"
//用户地址	删除地址
#define API_WO_Useraddress_Delete @"/useraddress/delete"
//User	根据user_token取得用户信息
#define API_WO_user_get_info @"/user/get_info"
//User	退出
#define API_WO_user_logout @"/user/logout"
//User	修改密码
#define API_WO_user_resetpass @"/user/resetpass"
//User	头像上传
#define API_WO_user_uploadavatar @"/user/uploadavatar"
//User	修改个人信息
#define API_WO_Softhelp_user_editprofile @"/user/editprofile"

//全局接口	取得国家列表
#define API_WO_Softhelp_GetCountries @"/softhelp/getcountries"

#define API_WO_Softhelp_softhelp_helpdata @"/softhelp/helpinfo"
//我的通知列表	用户中心通知列表
#define API_WO_inbox_list @"/inbox/list"
//我的通知列表	删除单条通知
#define API_WO_inbox_delmsg @"/inbox/delmsg"

#define API_WO_cashhistory_list @"/cashhistory/list"

//充值	充值历史
#define API_WO_recharge_historylist @"/recharge/historylist"
//充值 普通充值
#define API_WO_recharge_prepaid @"/recharge/prepaid"
//充值  微信订单生成
#define API_WO_weixinpayment_pay @"/weixinpaymentsuqigou/pay"



#define API_WO_recharge_list @"/recharge/list"
//充值	国际汇款充值
#define API_WO_recharge_banktransfer @"/recharge/banktransfer"
//充值   订单详情
#define API_WO_recharge_chongzhiorderdetail @"/recharge/chongzhiorderdetail"

//充值   确认是否充值成功  微信 支付宝 order_no
#define API_WO_recharge_verify @"/recharge/verify"

//充值   确认是否充值成功  paypal
#define API_WO_paypalwebhook_getpayment @"/paypalwebhook/getpayment"



//---代充    取消代充
#define API_Wo_daichong_cancel @"/daichong/cancel"

//---代充	微信代充
#define API_Wo_daichong_weixin @"/daichong/saveorder"
//---代充	支付宝代充
#define API_Wo_daichong_alipay @"/daichong/saveorder"
//---代充    订单详情
#define API_Wo_daichong_orderdetail @"/daichong/orderdetail"
//---代充    代充余额支付
#define API_Wo_daichong_orderpayment @"/daichong/orderpayment"
//

//

//-- 代充	代充说明
#define API_Wo_daichong_intro @"/daichong/intro"
//-- 代充    历史记录
#define API_Wo_daichong_list @"/daichong/list"
/*所有的网络请求接口地址放在这个地方*/
