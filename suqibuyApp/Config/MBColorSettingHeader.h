//
//  MBColorSettingHeader.h
//  MBPeopleBaseApp
//
//  Created by molbase on 16/2/24.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#ifndef MBColorSettingHeader_h
#define MBColorSettingHeader_h


#define allCornerRadius 4

///主题色
#define mainBg @"#FFFFFF"

///按钮橘黄
#define simple_button_yellow @"#ff5000"

//blue button
#define button_blue @"#027fd1"

//gray button
#define button_gray @"cccccc"

///页面背景色
#define bg_white @"#ffffff"

///大按钮文本颜色
#define text_color @"#717171"

///边框线颜色
#define border_color @"dfdede"

///灰色
#define gray @"#cccccc"

///分割线
#define seperator_color @"#E5E5E5"

///文字灰色
#define text_gray @"#cbcbcb"

///文字灰色
#define text_gray_666666 @"#666666"

///粉红色
#define pink_color @"FF699C"

///红色
#define text_red @"FF0000"

///订单号灰色
#define order_back_color @"cccccc"

///绿色 操作成功等
#define order_status_green @"008000"

///灰色 按钮不可点
#define button_disable @"AAAAAA"




///红色 255 75 64
#define MBred    @"ff4b40"

///蓝色 55 161 255
#define MBblue   @"37a1ff"

///tableview 索引条字体颜色
#define MBsectionIndexColor @"3C3C3C"

///黑1 70 70 70
#define MBblack1 @"#464646"

///黑2 120 120 120
#define MBblack2 @"#787878"

///黑3 150 150 150
#define MBblack3 @"#969696"

///灰1 180 180 180
#define MBgray1  @"#B4B4B4"

///灰2 220 220 220
#define MBgray2  @"#DCDCDC"


///忘记密码 其他登录颜色 89 108 152
#define MBColor_forgetPassword @"596C98"

#pragma mark //---------------------字体颜色统一 -----------------

///分割线
#define seperator_color_gray @"#E5E5E5"


#define text_blue @"#1F9EFF"

///选中橘黄色
#define text_yellow @"#fa3c06"

//浅灰色
#define text_shallow_gray @"#a0a0a0"

//白色
#define text_white @"#ffffff"

//普通文本颜色
#define text_color_gray_black @"#717171"

#define bg_gray @"#f0f5f8"

#pragma mark - 供应链身份颜色
///原料
#define MBColor_material @"FE3EAF"

///工厂
#define MBColor_factory  @"45E60D"

///贸易
#define MBColor_trade    @"3F8FFE"

///定制
#define MBColor_customized @"FF3F4C"

///试剂
#define MBColor_reagent  @"FFB43F"

///设备
#define MBColor_device   @"7A3EFE"

///服务
#define MBColor_service  @"FE6A2A"

///其他
#define MBColor_other    @"9ECF25"

//字体

#define text_heit_Blod @"Heiti SC-Bold"


#define text_heit @"Heiti SC"



#endif /* MBColorSettingHeader_h */
