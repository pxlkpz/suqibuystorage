//
//  ThirdConfig.h
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#ifndef ThirdConfig_h
#define ThirdConfig_h

/**
 * 腾讯Bugly的APPID
 */
#define TencentBuglyAPPID @"900003869"

/**
 * 友盟app数据统计
 */
#define UMENGDataStatistics @"557904a867e58ecd850013a7"

/**
 *  我的
 */
//#define UMENGDataStatistics @"552f0cb8fd98c5f2090021e2"

#endif /* ThirdConfig_h */

/*所有第三方的配置文件*/