//
//  UmengEventDefinition.h
//  MolBaseApp
//
//  Created by molbase on 16/1/12.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#ifndef UmengEventDefinition_h
#define UmengEventDefinition_h

#pragma mark - 公共事件部分
/*公共事件部分*/
///返回
#define UmengEvent_back                 @"back"


#pragma mark - 底部按钮
/*底部按钮*/
#define UmengEvent_main                 @"main"

///首页按钮
#define UmengEvent_main_index           @"index"
///百科按钮
#define UmengEvent_main_wiki            @"wiki"
///需求按钮
#define UmengEvent_main_demand          @"demand"
///我的按钮
#define UmengEvent_main_mine            @"mine"



#pragma mark - 首页
/*index 首页*/
#define UmengEvent_page_index           @"index"

///搜索
#define UmengEvent_index_search         @"search"
///消息按钮
#define UmengEvent_index_msg            @"msg"
///banner
#define UmengEvent_index_banner         @"banner"
///客户需求
#define UmengEvent_index_demand         @"demand"
///我的询盘
#define UmengEvent_index_mineinquiry    @"mineinquiry"
///我的收藏
#define UmengEvent_index_minecollect    @"minecollect"
///我要求购
#define UmengEvent_index_postdemand     @"postdemand"
///热门产品
#define UmengEvent_index_hotproduct     @"hotproduct"
///最近浏览
#define UmengEvent_index_browse         @"browse"
///热门推荐
#define UmengEvent_index_recomment      @"recomment"




#pragma mark - 百科
/*wiki 百科*/
#define UmenEvent_page_wiki             @"wiki"

///热门化学品
#define UmengEvent_wiki_hot             @"hot"
///热搜化学品
#define UmengEvent_wiki_searchproduct   @"searchproduct"
///搜索
#define UmengEvent_wiki_search          @"search"
///消息按钮
#define UmengEvent_wiki_msg             @"msg"




#pragma mark - 需求
/*demand 需求*/
#define UmengEvent_page_demand          @"demand"

///我要求购按钮
#define UmengEvent_demand_confirm       @"confirm"





#pragma mark - 搜索页
/*searchpage 搜索页*/
#define UmengEvent_page_searchpage      @"searchpage"

///热门搜索词
#define UmengEvent_searchpage_hotkey    @"hotkey"
///换一组
#define UmengEvent_searchpage_change    @"change"
///近期搜索词
#define UmengEvent_searchpage_history   @"history"
///清除搜索记录按钮
#define UmengEvent_searchpage_remove    @"remove"






#pragma mark - 关键词结果页
/*search 关键词结果页*/
#define UmengEvent_page_search          @"search"

///整体留存情况
#define UmengEvent_search_searchCode    @"searchCode"





#pragma mark - cas号聚合页
/*casresult cas号聚合页*/
#define UmengEvent_page_casresult       @"casresult"

///百科详情
#define UmengEvent_casresult_wiki       @"wiki"
///发布询盘按钮
#define UmengEvent_casresult_moreinquiry @"moreinquiry"
///查看所有供应商按钮
#define UmengEvent_casresult_allsupply  @"allsupply"
///供应商名片信息
#define UmengEvent_casresult_supplydetail @"supplydetail"
///立即询盘按钮
#define UmengEvent_casresult_oneinquiry @"oneinquiry"







#pragma mark - 百科页具体
/*dict 百科页具体*/
#define UmengEvent_page_dict            @"dict"

///基本信息
#define UmengEvent_dict_basic           @"basic"
///安全信息
#define UmengEvent_dict_security        @"security"
///物化性质
#define UmengEvent_dict_phy             @"phy"
///毒性
#define UmengEvent_dict_virulence       @"virulence"
///msds
#define UmengEvent_dict_msds            @"msds"
///上下游关系
#define UmengEvent_dict_updownstream    @"updownstream"
///海关数据
#define UmengEvent_dict_custom          @"custom"
///合成路线
#define UmengEvent_dict_route           @"route"
///图谱
#define UmengEvent_dict_nmr             @"nmr"




#pragma mark - 百科页基本
/*wikipage 百科页基本*/
#define UmengEvent_page_wikipage        @"wikipage"

///更多按钮
#define UmengEvent_wikipage_more        @"more"
///立即询盘按钮
#define UmengEvent_wikipage_inquiry     @"inquiry"
///收藏按钮
#define UmengEvent_wikipage_collect     @"collect"





#pragma mark - 查看供应商
/*querysupply 查看供应商*/
#define UmengEvent_page_querysupply     @"querysupply"

///查看供应商名片信息
#define UmengEvent_querysupply_card     @"card"
///立即询盘按钮
#define UmengEvent_querysupply_post     @"post"




#pragma mark - 选择供应商
/*selectsupply 选择供应商*/
#define UmengEvent_page_selectsupply    @"selectsupply"

///默认排序按钮
#define UmengEvent_selectsupply_default @"default"
///价格排序按钮
#define UmengEvent_selectsupply_price   @"price"
///纯度排序按钮
#define UmengEvent_selectsupply_purity  @"purity"
///下一步按钮
#define UmengEvent_selectsupply_next    @"next"

///----------筛选按钮
///供应商类型
#define UmengEvent_selectsupply_type    @"type"
///规格
#define UmengEvent_selectsupply_spec    @"spec"
///所在地
#define UmengEvent_selectsupply_location @"location"
///确定按钮
#define UmengEvent_selectsupply_confirm @"confirm"
///取消按钮
#define UmengEvent_selectsupply_cancle  @"cancle"





#pragma mark - 填写询盘信息
/*inputinquiry 填写询盘信息*/
#define UmengEvent_page_inputinquiry    @"inputinquiry"

///取消选择
#define UmengEvent_inputinquiry_cancle  @"cancle"
///提交询盘按钮
#define UmengEvent_inputinquiry_post    @"post"






#pragma mark - 我的
/*mine 我的*/
#define UmengEvent_page_mine            @"mine"

///头部缝纫资料按钮
#define UmengEvent_mine_personaldetail  @"personaldetail"
///客户需求
#define UmengEvent_mine_demand          @"demand"
///我的询盘
#define UmengEvent_mine_inquiry         @"inquiry"
///我的求购
#define UmengEvent_mine_manage          @"manage"
///我的收藏
#define UmengEvent_mine_collect         @"collect"
///我的消息
#define UmengEvent_mine_msg             @"msg"
///我的设置
#define UmengEvent_mine_set             @"set"





#pragma mark - 客户需求
/*cusdemand 客户需求*/
#define UmengEvent_page_cusdemand       @"cusdemand"

///待报价按钮
#define UmengEvent_cusdemand_wait       @"wait"
///已报价按钮
#define UmengEvent_cusdemand_offer      @"offer"
///已成功按钮
#define UmengEvent_cusdemand_success    @"success"
///已取消按钮
#define UmengEvent_cusdemand_cancle     @"cancle"
///列表数据点击项
#define UmengEvent_cusdemand_item       @"item"
///列表数据立即报价
#define UmengEvent_cusdemand_postoffer  @"postoffer"





#pragma mark - 客户需求详情
/*cusdemanddetail 客户需求详情*/
#define UmengEvent_page_cusdemanddetail @"cusdemanddetail"

///联系molbase
#define UmengEvent_cusdemanddetail_phone @"phone"
///无法供货
#define UmengEvent_cusdemanddetail_nogoods @"nogoods"
///立即报价
#define UmengEvent_cusdemanddetail_offer    @"offer"
///申诉按钮
#define UmengEvent_cusdemanddetail_complain @"complain"




#pragma mark - 询盘报价
/*inquiryoffer 询盘报价*/
#define UmengEvent_page_inquiryoffer    @"inquiryoffer"

///提交报价
#define UmengEvent_inquiryoffer_post    @"post"

///--------联系人信息
///编辑
#define UmengEvent_inquiryoffer_edit    @"edit"
///删除
#define UmengEvent_inquiryoffer_remove  @"remove"
///新建联系人
#define UmengEvent_inquiryoffer_add     @"add"





#pragma mark - 我的询盘
/*mineinquiry 我的询盘*/
#define UmengEvent_page_mineinquiry     @"mineinquiry"

///待报价按钮
#define UmengEvent_mineinquiry_wait     @"wait"
///已报价按钮
#define UmengEvent_mineinquiry_offer    @"offer"
///已成功按钮
#define UmengEvent_mineinquiry_success  @"success"
///已取消按钮
#define UmengEvent_mineinquiry_cancle   @"cancle"
///列表数据点击项
#define UmengEvent_mineinquiry_item     @"item"





#pragma mark - 我的询盘详情
/*mineinquirydetail 我的询盘详情*/
#define UmengEvent_page_mineinquirydetail @"mineinquirydetail"


///供应商列表点击
#define UmengEvent_mineinquirydetail_item @"item"
///联系molbase
#define UmengEvent_mineinquirydetail_phone @"phone"
///取消询盘
#define UmengEvent_mineinquirydetail_cancle @"cancle"


#pragma mark - 我的求购
/*minedemand 我的求购*/
#define UmengEvent_page_minedemand @"minedemand"


///待报价按钮
#define UmengEvent_minedemand_wait @"wait"
///已报价按钮
#define UmengEvent_minedemand_offer @"offer"
///已成功按钮
#define UmengEvent_minedemand_success @"success"
///已取消按钮
#define UmengEvent_minedemand_cancle @"cancle"
///列表数据点击项
#define UmengEvent_minedemand_item @"item"




#pragma mark - 我的求购详情
/*minedemanddetail 我的求购详情*/
#define UmengEvent_page_minedemanddetail @"minedemanddetail"


///供应商列表点击
#define UmengEvent_minedemanddetail_item @"item"
///联系MOLBASE
#define UmengEvent_minedemanddetail_phone @"phone"
///取消询盘
#define UmengEvent_minedemanddetail_cancle @"cancle"







#pragma mark - 报价详情
/*offerdetail 报价详情*/
#define UmengEvent_page_offerdetail     @"offerdetail"

///接受报价
#define UmengEvent_offerdetail_accept   @"accept"
///联系molbase
#define UmengEvent_offerdetail_phone    @"phone"
///联系供应商
#define UmengEvent_offerdetail_supply   @"supply"








#pragma mark - 我的收藏
/*minecollect 我的收藏*/
#define UmengEvent_page_minecollect     @"minecollect"

///编辑
#define UmengEvent_minecollect_edit     @"edit"
///立即询盘按钮
#define UmengEvent_minecollect_inquiry  @"inquiry"
///产品点击
#define UmengEvent_minecollect_click    @"click"
///完成
#define UmengEvent_minecollect_finish   @"finish"
///删除按钮
#define UmengEvent_minecollect_delete   @"delete"






#pragma mark - 我的消息
/*minemsg 我的消息*/
#define UmengEvent_page_minemsg         @"minemsg"

///资讯信息
#define UmengEvent_minemsg_news         @"news"
///询盘信息
#define UmengEvent_minemsg_inquiry      @"inquiry"




#pragma mark -消息设置
/*msgset 消息设置*/
#define UmengEvent_page_msgset          @"msgset"

///新消息设置开关
#define UmengEvent_msgset_newmsg        @"newmsg"
///询盘信息开关
#define UmengEvent_msgset_inquirymsg    @"inquirymsg"
///资讯信息开关
#define UmengEvent_msgset_newsmsg       @"newsmsg"
///接受时间
#define UmengEvent_msgset_timeset       @"timeset"





#pragma mark - 资讯信息
/*newsmsg 资讯信息*/
#define UmengEvent_page_newsmsg         @"newsmsg"

///数据点击
#define UmengEvent_newsmsg_item         @"item"
///设置按钮
#define UmengEvent_newsmsg_set          @"set"




#pragma mark - 询盘信息
/*inquirymsg 询盘信息*/
#define UmengEvent_page_inquirymsg      @"inquirymsg"

///数据点击
#define UmengEvent_inquirymsg_item      @"item"
///设置按钮
#define UmengEvent_inquirymsg_set       @"set"




#pragma mark - 我的设置
/*minesetting 我的设置*/
#define UmengEvent_page_minesetting     @"minesetting"

///个人资料
#define UmengEvent_minesetting_detail   @"detail"
///修改密码
#define UmengEvent_minesetting_chgpwd   @"chgpwd"
///关于我们
#define UmengEvent_minesetting_about    @"about"
///意见反馈
#define UmengEvent_minesetting_feedback @"feedback"
///客户热线
#define UmengEvent_minesetting_phone    @"phone"
///清楚缓存
#define UmengEvent_minesetting_remove   @"remove"
///退出当前账户
#define UmengEvent_minesetting_quit     @"quit"




#pragma mark - 个人资料
/*personaldetail 个人资料*/
#define UmengEvent_page_personaldetail  @"personaldetail"

///手机号点击
#define UmengEvent_personaldetail_phone @"phone"
///注册邮箱点击
#define UmengEvent_personaldetail_email @"email"




#pragma mark - 修改密码
/*changepwd 修改密码*/
#define UmengEvent_page_changepwd       @"changepwd"

///确认修改按钮
#define UmengEvent_changepwd_confirm    @"confirm"





#pragma mark - 意见反馈
/*feedback 意见反馈*/
#define UmengEvent_page_feedback        @"feedback"

///提交按钮
#define UmengEvent_feedback_confirm     @"confirm"




#pragma mark - 登录
/*login 登录*/
#define UmengEvent_page_login           @"login"

///登录按钮
#define UmengEvent_login_login          @"login"




#pragma mark - 忘记密码
/*account 忘记密码*/
#define UmengEvent_page_account         @"account"

///获取验证码
#define UmengEvent_account_getvercode   @"getvercode"
///确认修改按钮
#define UmengEvent_account_confirm      @"confirm"




#pragma mark - 注册
/*注册*/
#define UmengEvent_page_register        @"register"

///获取验证码
#define UmengEvent_register_getvercode  @"getvercode"
///服务协议
#define UmengEvent_register_protocol    @"protocol"
///立即注册按钮
#define UmengEvent_register_register    @"register"



#pragma mark - 分享
/*share 分享*/
#define UmengEvent_page_share       @"share"

///微信
#define UmengEvent_share_wx  @"wx"
///朋友圈
#define UmengEvent_share_pyq  @"pyq"
///QQ好友
#define UmengEvent_share_qq  @"qq"
///微博
#define UmengEvent_share_wb  @"wb"
///邮件
#define UmengEvent_share_yj  @"yj"
///短信
#define UmengEvent_share_dx  @"dx"







#endif /* UmengEventDefinition_h */
