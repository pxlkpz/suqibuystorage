//
//  ToolsConfig.h
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#ifndef ToolsConfig_h
#define ToolsConfig_h

#define DDLog(xx, ...)          NSLog(@"%s(%d): " xx, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#define VIEWBG_COLOR            @"#f0f0f0"

#define TOP_NAVBTN_COLOR        @"#b4b4b4"
#define ALL_LINE_COLOR              @"#e6e6e6"

#define NAV_BACK_IMG            @"sq_common_navi_left"
#define NAV_BACK_SELECT_IMG     @"sq_common_navi_left"

#define ALL_line_height         (1 / [UIScreen mainScreen].scale)

#define iPhone6Less               ([UIScreen mainScreen].bounds.size.width <= 320.0)
#define iPhone6_D                 ([UIScreen mainScreen].bounds.size.width == 375.0)
#define iPhone6P_D                ([UIScreen mainScreen].bounds.size.width == 414.0)
//当前系统版本
#define IOS_VERSION              ([[UIDevice currentDevice].systemVersion floatValue])

#define IsiPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

#define TABBAR_ONE      @"代购"
#define TABBAR_TWO      @"转运"
#define TABBAR_THREE    @"社区"
#define TABBAR_FOUR     @"我"
//导航条颜色
#define NAV_BAR_COLOR            @"#ffffff"

//屏幕大小尺寸
#define screen_width [UIScreen mainScreen].bounds.size.width

//判断空字符串
#define wal_is_null(obj) \
(obj == nil || [obj isKindOfClass:[NSNull class]])

//询盘提交后发通知
#define NSNotificationFromInputInquiryVC    @"NSNotificationFromInputInquiryVC"

//取view的坐标及长宽 lpf
#define W(view)    view.frame.size.width
#define H(view)    view.frame.size.height
#define X(view)    view.frame.origin.x
#define Y(view)    view.frame.origin.y
#define screen_height [UIScreen mainScreen].bounds.size.height

#ifndef IOS7_OR_LATER
#define IOS7_OR_LATER ( [[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending )
#endif

#endif /* ToolsConfig_h */

/*所有的工具宏定义放在这个地方 eg 图片名字 颜色值，一些数字 动画时间等*/

//组件距右间距
#define viewSpace 50

// 获取RGB颜色
#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]
#define RGB(r,g,b) RGBA(r,g,b,1.0f)

// 颜色
#define KColorRGBA(r, g, b, a) [UIColor colorWithRed:(r) / 255.0 green:(g) / 255.0 blue:(b) / 255.0 alpha:(a)]
#define KColorRGB(r, g, b) KColorRGBA(r, g, b, 1.f)


// 十六进制转颜色
#define KColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


// @interface
#define singleton_interface(className) \
+ (className *)shared##className;


// @implementation
#define singleton_implementation(className) \
static className *_instance; \
+ (id)allocWithZone:(NSZone *)zone \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [super allocWithZone:zone]; \
}); \
return _instance; \
} \
+ (className *)shared##className \
{ \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
_instance = [[self alloc] init]; \
}); \
return _instance; \
}

// NavBar高度
#define KNavigationBarHeight (44.0)
// 状态栏高度
#define KStatusBarHeight ([[UIApplication sharedApplication] statusBarFrame].size.height)


#define KScreenHeight ([UIScreen mainScreen].bounds.size.height)
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KTopHeight (KNavigationBarHeight + KStatusBarHeight)

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define SCREEN_MAX_LENGTH (MAX(KScreenWidth, KScreenHeight))
#define SCREEN_MIN_LENGTH (MIN(KScreenWidth, KScreenHeight))

#define IPHONE3_5INCH   ([[UIScreen mainScreen] bounds].size.height == 480)

#define IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH <= 568.0)
#define IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)


