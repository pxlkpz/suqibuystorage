//
//  SQNotification.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#ifndef SQNotification_h
#define SQNotification_h

///注册成功通知
#define SQ_REGISTER_SUCCESS_NOTIFICATION @"SQ_REGISTER_SUCCESS_NOTIFICATION"

///淘宝点按
#define SQ_PUSH_BUTTON_NOTIFICATION @"SQ_PUSH_BUTTON_NOTIFICATION"

#endif /* SQNotification_h */
