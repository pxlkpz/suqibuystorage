//
//  MBBageView.m
//  MolBaseApp
//
//  Created by MOLBASE on 15/11/12.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import "MBBageView.h"

@implementation MBBageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
        _numLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 25, 16)];
        _numLab.textAlignment = NSTextAlignmentCenter;
        _numLab.backgroundColor = [UIColor redColor];
        _numLab.textColor = [UIColor whiteColor];
        _numLab.font = [UIFont boldSystemFontOfSize:11.0f];
        _numLab.text = @"";
        _numLab.layer.cornerRadius = H(_numLab)/2;
        _numLab.layer.masksToBounds = YES;
        [self addSubview:_numLab];
        
    }
    return self;
}

-(void)setTitle:(NSString*)str
{
    if ([str isEqualToString:@"frist"]) {
        _numLab.frame = CGRectMake(0, 0, 16, 16);
        _numLab.text = @"";
        _numLab.hidden = YES;
    }else{
        _numLab.hidden = NO;
        if ([str intValue] < 10) {
            if ([str intValue]==0) {
                _numLab.hidden = YES;
            }
            _numLab.frame = CGRectMake(0, 0, 16, 16);
            _numLab.text = str;
        }else if ([str intValue] > 99){
            _numLab.frame = CGRectMake(0, 0, 25, 16);
            _numLab.text = @"99+";
        }else{
            _numLab.frame = CGRectMake(0, 0, 20, 16);
            _numLab.text = str;
        }
    }
}

@end
