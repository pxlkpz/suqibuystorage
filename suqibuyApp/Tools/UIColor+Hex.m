//
//  UIColor+Hex.m
//  XiGouApp
//
//  Created by Marbury_CC on 14-8-15.
//  Copyright (c) 2014年 Jelly. All rights reserved.
//

#import "UIColor+Hex.h"

@implementation NSString (Hex)

- (NSUInteger)integerValueFromHex
{
	int result = 0;
	sscanf([self UTF8String], "%x", &result);
	return result;
}

@end

@implementation UIColor (Hex)

+ (UIColor *)colorWithHexString:(NSString *)hex
{
	if ([hex hasPrefix:@"#"]){
		hex = [hex substringFromIndex:1];
	}
	
	if ([hex length]!=6 && [hex length]!=3){
		return nil;
	}
	
	NSUInteger digits = [hex length]/3;
	CGFloat maxValue = (digits==1)?15.0:255.0;
	
	CGFloat red = [[hex substringWithRange:NSMakeRange(0, digits)] integerValueFromHex]/maxValue;
	CGFloat green = [[hex substringWithRange:NSMakeRange(digits, digits)] integerValueFromHex]/maxValue;
	CGFloat blue = [[hex substringWithRange:NSMakeRange(2*digits, digits)] integerValueFromHex]/maxValue;
	
	return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

@end
