//
//  CommonWebViewController.m
//  yunbo2016
//
//  Created by apple on 15/12/1.
//  Copyright © 2015年 apple. All rights reserved.
//  共用webView

#import "CommonWebViewController.h"
//#import "WebViewJavascriptBridge.h"
//#import "ShareModel.h"
@interface CommonWebViewController ()<UIWebViewDelegate>

//@property WebViewJavascriptBridge* bridge;

@property (nonatomic ,copy) NSString *urlStr;
@property (nonatomic ,copy) NSString *NavTitle;
//@property (nonatomic , strong) ShareModel *model;

@end
@implementation CommonWebViewController

-(instancetype)initWithUrlStr:(NSString *)urlStr andNavTitle:(NSString *)navTitle


{
    if (self= [super init]) {
        _urlStr = urlStr;
        _NavTitle = navTitle;

        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.title = NSLocalizedString(_NavTitle, nil);
    _web = [[UIWebView alloc]init];
    _web.scrollView.bounces = NO;
    _web.delegate   = self;
    _web.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_web];
    [_web  mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    NSURLRequest *requset = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlStr]];
    [_web loadRequest:requset];
    
    
}

@end
