//
//  MBOnlyPromptTextHUD.h
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import "MBProgressHUD.h"

@interface MBOnlyPromptTextHUD : MBProgressHUD
<MBProgressHUDDelegate>
+(void)showDefaultPromptTextHUD:(NSString*)text afterDelay:(NSTimeInterval)delay;

@end
