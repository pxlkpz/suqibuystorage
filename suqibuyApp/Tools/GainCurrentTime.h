//
//  GainCurrentTime.h
//  MolBaseApp
//
//  Created by  Lilee on 15/12/9.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GainCurrentTime : NSObject
//获取当前时间所有的秒
+(NSString*)allSecond;

//根基服务器返回的秒返回正常年月日十分秒
+(NSString*)returnNormalTime:(NSString*)time;


/** 消息时间来源处理 */
+ (NSString *)createdAt:(NSString*)time;



/**获取当前小时时间*/
+ (NSInteger)getsTheCurrentHour;
@end
