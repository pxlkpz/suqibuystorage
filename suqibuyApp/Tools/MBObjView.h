//
//  MBObjView.h
//  MolBaseApp
//
//  Created by MOLBASE on 15/11/27.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBObjView : UIButton

@property (strong, nonatomic) id model;

@end
