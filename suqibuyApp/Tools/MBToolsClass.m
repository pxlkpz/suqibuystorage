//
//  MBToolsClass.m
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import "MBToolsClass.h"
#import "MBOnlyPromptTextHUD.h"
#import "AppDelegate.h"

#import <CommonCrypto/CommonDigest.h>

float kDefaultPromptTime = 2.0f;

@implementation MBToolsClass

#pragma mark - HUD
void showDefaultPromptTextHUD(NSString* text)
{
    [MBOnlyPromptTextHUD showDefaultPromptTextHUD:text afterDelay:kDefaultPromptTime];
}

//颜色转成图片
+ (UIImage*) createImageWithColor: (UIColor*) color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

void showModelView(UIViewController* viewCtrl, BOOL animate)
{
    UINavigationController* _nav = [[UINavigationController alloc]initWithRootViewController:viewCtrl];
    _nav.navigationBar.barTintColor = [UIColor colorWithHexString:NAV_BAR_COLOR];
    //_nav.navigationBar.translucent = YES;
    _nav.interactivePopGestureRecognizer.enabled = YES;
    [MB_AppDelegate.window.rootViewController presentViewController:_nav animated:animate completion:nil];
}

+ (id)getNSUserDefaultsWithKey:(NSString*)key
{
    id value = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    return value;
}

+ (BOOL)saveNSUserDefaultsWithObject:(id)object Key:(NSString*)key
{
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    return [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)creatMD5StringWithString:(NSString *)string
{
    const char *original_str = [string UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(original_str, (CC_LONG)strlen(original_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++)
        [hash appendFormat:@"%02X", result[i]];
    [hash lowercaseString];
    return hash;
}

@end
