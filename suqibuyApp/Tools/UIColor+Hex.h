//
//  UIColor+Hex.h
//  XiGouApp
//
//  Created by Marbury_CC on 14-8-15.
//  Copyright (c) 2014年 Jelly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)
+ (UIColor *)colorWithHexString:(NSString *)hex;
@end
