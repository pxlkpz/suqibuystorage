//
//  GainCurrentTime.m
//  MolBaseApp
//
//  Created by  Lilee on 15/12/9.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import "GainCurrentTime.h"

@implementation GainCurrentTime
//获取当前时间所有的秒
//AllSecond
+(NSString*)allSecond
{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval a=[dat timeIntervalSince1970];
    
    NSString *timeString = [NSString stringWithFormat:@"%f", a];//转为字符型
    
    
    return timeString;

}


//根基服务器返回的秒返回正常年月日十分秒
+(NSString*)returnNormalTime:(NSString*)time
{
    
    
    NSInteger  secondInteger=[time integerValue];
    NSDate  *date = [NSDate dateWithTimeIntervalSince1970:secondInteger];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    //    NSDate *date = [NSDate date];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    
    return formattedDateString;
    
    
}



/**获取当前小时时间*/
+ (NSInteger)getsTheCurrentHour
{
    
     NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"HH"];
    

    //    NSDate *date = [NSDate date];
    NSString *formattedDateString = [dateFormatter stringFromDate:dat];
    NSLog(@"---========------------%@",formattedDateString);
    return [formattedDateString integerValue];
}

/** 消息时间来源处理 */
+ (NSString *)createdAt:(NSString*)time
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"EEE MMM d HH:mm:ss Z yyyy";
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    //    NSTimeInterval  delta=70;
    
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:[time integerValue]];
    //    NSDate *date = [formatter dateFromString:timeDate];
    
    NSDate *now = [NSDate date];
    NSLog(@"=============%@",now);
    //     NSLog(@"======date=======%@",time);
    NSLog(@"======date=======%@",timeDate);
    // 比较微博发送时间和当前时间
    NSTimeInterval delta = [now timeIntervalSinceDate:timeDate];
    
    
    if (delta < 60) { // 1分钟以内
        return @"刚刚";
    } else if (delta < 60 * 60) { // 60分钟以内
        return [NSString stringWithFormat:@"%.f分钟前", delta/60];
    } else if (delta < 60 * 60 * 24) { // 24小时内
        return [NSString stringWithFormat:@"%.f小时前", delta/(60 * 60)];
    } else {
        formatter.dateFormat = @"yyyy-MM-dd HH:mm";
        return [formatter stringFromDate:timeDate];
    }
}
@end
