//
//  MBToolsClass.h
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBToolsClass : NSObject

/**
 *  菊花
 *	@param	text	输入
 */
void showDefaultPromptTextHUD(NSString* text);

/**
 *  色值转图片
 *	@param	text	输入
 */
+ (UIImage*) createImageWithColor: (UIColor*) color;

/**
 *  根据传字符串多少计算lab的高度
 *	@param	text	输入
 */
void showModelView(UIViewController* viewCtrl, BOOL animate);

+ (id)getNSUserDefaultsWithKey:(NSString*)key;

+ (BOOL)saveNSUserDefaultsWithObject:(id)object Key:(NSString*)key;

/**
 *
 *	MD5加密
 */
+ (NSString *)creatMD5StringWithString:(NSString *)string;


@end
