//
//  MBOnlyPromptTextHUD.m
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import "MBOnlyPromptTextHUD.h"

static MBOnlyPromptTextHUD* onlyPromptTextHUD = nil;

@implementation MBOnlyPromptTextHUD

+(void)showDefaultPromptTextHUD:(NSString*)text afterDelay:(NSTimeInterval)delay{
    if (onlyPromptTextHUD==nil) {
        UIWindow* window = [[UIApplication sharedApplication].delegate window];
        onlyPromptTextHUD = [[MBOnlyPromptTextHUD alloc] initWithView:window];
        [window addSubview:onlyPromptTextHUD];
        [onlyPromptTextHUD show:YES];
        onlyPromptTextHUD.userInteractionEnabled = NO;
        onlyPromptTextHUD.dimBackground = NO;
        onlyPromptTextHUD.mode = MBProgressHUDModeText;
        onlyPromptTextHUD.removeFromSuperViewOnHide = YES;
        onlyPromptTextHUD.delegate = onlyPromptTextHUD;
    }
    onlyPromptTextHUD.detailsLabelText = text;
    [onlyPromptTextHUD hide:YES afterDelay:delay];
}

#pragma mark - MBProgressHUDDelegate
- (void)hudWasHidden:(MBProgressHUD *)hud{
    onlyPromptTextHUD.delegate = nil;
    onlyPromptTextHUD = nil;
}

@end
