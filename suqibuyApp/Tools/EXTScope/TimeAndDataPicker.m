//
//  TimePicker.m
//  CarService
//
//  Created by Apple on 11/13/14.
//  Copyright (c) 2014 fenglun. All rights reserved.
//

 /***   时间选择        */

#import "TimeAndDataPicker.h"
@interface TimeAndDataPicker()<UIPickerViewDelegate,UIPickerViewDataSource>

@end

@implementation TimeAndDataPicker {
    NSDate *minDate;
    NSDate *maxDate;
    UIPickerView* dataPicker1;
    
    NSArray *first ;
    NSArray *second;
}

- (id)initWithFrame:(CGRect)frame andType:(DatePickerSelectMode)mode
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setFrame:frame];
        selectMode = mode;
        [self loadView];
        
    }
    return self;
}

-(void)timePickerAnimations{
    [UIView beginAnimations:@"upAnimation" context:nil];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    CGRect frame = self.frame;
    frame.origin.y = 0;
    self.frame = frame;
    [UIView commitAnimations];
}

-(void)getDateWithPicerMode:(UIDatePickerMode) dataModel andFirstSelectDate:(NSString*)dateStr andFinsihBlock:(void(^)(NSString *resurltStr)) finsihBlock
{
    _dataModel  =dataModel;
    _FinishdBlock = [finsihBlock copy];
    _firstDateStr =dateStr;
    datePicker.datePickerMode = _dataModel;
    
    
    NSDate *select = [datePicker date];
    NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
    if (self.dataModel ==UIDatePickerModeDate) {
        
        [dateFormat2 setDateFormat:@"yyyy-MM-dd"];
        [datePicker setMinimumDate:[NSDate date]];
        
    }else if (self.dataModel == UIDatePickerModeTime){
        
        if (_firstDateStr) {
            [datePicker setMinimumDate:[dateFormat2 dateFromString:_firstDateStr]];
        }
        datePicker.minuteInterval = 15;
        [dateFormat2 setDateFormat:@"HH:mm"];
       
    }
    
    theDate = [dateFormat2 stringFromDate:select];
    
}

-(void)getDataWithArray:(NSArray*)aArray andFinsihBlock:(void (^)(NSString *, NSInteger, NSInteger))finsihBlock
{
    if (aArray.count < 1) {
        return;
    }
    pickerArray = aArray;
    _Block = [finsihBlock copy];
    if (selectMode == TimePickerDataTwoSelectMode) {
        first = aArray;
        NSDictionary *dic = first [0];
        second = dic[@"time"];
    }
    [dataPicker1 reloadAllComponents];

}


- (void) loadView
{
    [self setAlpha:1];
    
    //界面
    UIView *datePickerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 216 - 44, self.frame.size.width, 216+44)];
    [datePickerView setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:datePickerView];
    
    toolBar = [[UIToolbar alloc] init ];
    toolBar.barStyle = 0;
    
    UIBarButtonItem *titleButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target: nil action: nil];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleDone target: self action: @selector(done)];
    UIBarButtonItem *leftButton  = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleBordered target: self action: @selector(cancel)];
    UIBarButtonItem *fixedButton  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target: nil action: nil];
    NSArray *array = [[NSArray alloc] initWithObjects:fixedButton, leftButton,fixedButton,fixedButton,fixedButton,fixedButton,titleButton,fixedButton,fixedButton, fixedButton,fixedButton,rightButton,fixedButton, nil];
    [toolBar setItems:array];
    [datePickerView addSubview:toolBar];
    [toolBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(datePickerView);
        make.right.equalTo(datePickerView);
        make.top.equalTo(datePickerView);
        make.height.mas_equalTo(@44);

    }];
    
    if (selectMode == TimePickerTimeSelectMode) {
        datePicker = [[UIDatePicker alloc] init];
        
        [datePicker addTarget:self action:@selector(datePickerDateChanged:) forControlEvents:UIControlEventValueChanged];
        
        [datePickerView addSubview:datePicker];
        [datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(datePickerView);
            make.right.equalTo(datePickerView);
            make.top.equalTo(toolBar.mas_bottom);
            make.bottom.mas_equalTo(datePickerView);

        }];

        
    }else if(selectMode == TimePickerDataSelectMode || selectMode == TimePickerDataTwoSelectMode){
        dataPicker1 = [[UIPickerView alloc] init];
        dataPicker1.delegate = self;
        dataPicker1.dataSource = self;
        [datePickerView addSubview:dataPicker1];
        [dataPicker1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(datePickerView);
            make.right.equalTo(datePickerView);
            make.top.equalTo(toolBar.mas_bottom);
            make.bottom.mas_equalTo(datePickerView);

        }];
        
        
        [dataPicker1 selectRow:0 inComponent:0 animated:NO];

    }
    
}

-(void) datePickerDateChanged:(UIDatePicker *)paramDatePicker
{
    if ([paramDatePicker isEqual:datePicker])
    {
        NSDate *select = [paramDatePicker date];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//        MyLog(@"%@",dateFormat);
        if (self.dataModel ==UIDatePickerModeDate) {
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
        }else if (self.dataModel == UIDatePickerModeTime){
           
            [dateFormat setDateFormat:@"HH:mm"];
        }

        theDate = [dateFormat stringFromDate:select];
    }
}

- (void) done
{
    if (selectMode == TimePickerTimeSelectMode) {
        _FinishdBlock(theDate);
    }else if(selectMode == TimePickerDataSelectMode){
        NSInteger row = [dataPicker1 selectedRowInComponent:0];
        theDate = [pickerArray objectAtIndex:row];
        _Block(theDate,row,0);
    }else if(selectMode == TimePickerDataTwoSelectMode){
        
        NSInteger row = [dataPicker1 selectedRowInComponent:0];
        NSInteger row1 = [dataPicker1 selectedRowInComponent:1];
        NSString *firstStr = [first[row] objectForKey:@"day"];
        NSDictionary *dic = [second objectAtIndex:row1];
        NSString *start = [ [dic objectForKey:@"start"] substringWithRange:NSMakeRange(0, 5)];
        NSString *end = [ [dic objectForKey:@"end"] substringWithRange:NSMakeRange(0, 5)];
        
        NSString *secondStr = [NSString stringWithFormat:@"%@-%@", start, end];

        theDate = [NSString stringWithFormat:@"%@ %@", firstStr, secondStr];
        _Block(theDate,row, row1);
    }

    
    
    [self inAndOut];
}

- (void) cancel
{
    [self inAndOut];
}

- (void) inAndOut
{
    [self setFrame:CGRectMake(0, CGRectGetHeight(self.frame), self.frame.size.width, CGRectGetHeight(self.frame))];
    [self.window setAlpha:1.0];
}



//DateSelectMode

//返回显示的列数
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    if (selectMode == TimePickerDataTwoSelectMode) {
        return 2;
    }else{
        return 1;
    }

}
//返回当前列显示的行数
-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (selectMode == TimePickerDataTwoSelectMode) {
        if (component == 0) return [first count];
        return [second count];
    }else{
        return [pickerArray count];
    }
    return 0;
}
//返回当前行的内容,此处是将数组中数值添加到滚动的那个显示栏上

-(NSString*) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (selectMode == TimePickerDataTwoSelectMode) {
        if (component == 0)
        {
            NSDictionary *dic = [first objectAtIndex:row];
            NSString *str = [dic objectForKey:@"day"];
            return str;
        }
        
        else
            
        {
            NSDictionary *dic = [second objectAtIndex:row];
            NSString *start = [ [dic objectForKey:@"start"] substringWithRange:NSMakeRange(0, 5)];
              NSString *end = [ [dic objectForKey:@"end"] substringWithRange:NSMakeRange(0, 5)];

            NSString *str = [NSString stringWithFormat:@"%@-%@", start, end];
            return str;
        }
    }else{
        return [pickerArray objectAtIndex:row];
    }
    return 0;
}



//选择
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (selectMode == TimePickerDataTwoSelectMode) {
        if (component == 0) {//
            NSDictionary *seletedProvince = [first objectAtIndex:row];
            NSArray *array = [seletedProvince objectForKey:@"time"];
//            first = row;
            second = array;
            [pickerView  reloadComponent:1];
            [pickerView  selectRow:0 inComponent:1 animated:YES];
        }
//        if (component == 1){//
//            second = row;
//        }
 
    }
    
    
}



@end
