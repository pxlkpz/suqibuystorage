//
//  TimePicker.h
//  CarService
//
//  Created by Apple on 11/13/14.
//  Copyright (c) 2014 fenglun. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    
    TimePickerTimeSelectMode = 1,/**< 星期天选择  */
    TimePickerDataSelectMode,
    TimePickerDataTwoSelectMode,
    
}DatePickerSelectMode;

@protocol SElectHHMMTimeDelegate <NSObject>

- (void) selectedTime:(NSString *) time;

@end

@interface TimeAndDataPicker : UIView
{
    UIToolbar *toolBar;
    UIDatePicker *datePicker;
    NSString *theDate;
    void (^_FinishdBlock)(NSString  *resurltStr);
    
    void (^_Block)(NSString  *resurltStr, NSInteger rowOne, NSInteger  rowTwo);

    //自定义date
    NSArray *pickerArray;
    DatePickerSelectMode selectMode;
}

@property (assign,nonatomic) id<SElectHHMMTimeDelegate> delegate;
@property (nonatomic ,assign) UIDatePickerMode  dataModel;
@property (nonatomic ,copy)  NSString  *firstDateStr;

/**< 时间数据设置  */
-(void)getDateWithPicerMode:(UIDatePickerMode) dataModel andFirstSelectDate:(NSString*)dateStr andFinsihBlock:(void(^)(NSString *resurltStr)) finsihBlock;

/**< NSArray数据设置  */
-(void)getDataWithArray:(NSArray*)aArray andFinsihBlock:(void(^)(NSString  *resurltStr, NSInteger rowOne, NSInteger  rowTwo)) finsihBlock;


/**< 初始化  */
- (id)initWithFrame:(CGRect)frame andType:(DatePickerSelectMode)mode;

/**< 移动动画效果  */
-(void)timePickerAnimations;
- (void) cancel;

@end
