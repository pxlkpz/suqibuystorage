//
//  UIView+UIView_Frame.h
//  MolBaseApp
//
//  Created by MOLBASE on 16/1/6.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIView_Frame)

@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;
@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) CGPoint origin;
@property (nonatomic, assign) CGSize size;


@end
