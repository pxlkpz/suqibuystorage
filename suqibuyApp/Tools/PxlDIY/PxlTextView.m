//
//  PxlTextView.m
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "PxlTextView.h"
#import "MBZPKTextField.h"

@implementation PxlTextView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
//- (instancetype)initwithStr:(NSString*)string
//{
//    self = [super init];
//    if (self) {
//        
//    }
//    return self;
//}

//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super init];
//    if (self) {
//        
//    }
//    return self;
//}
//height 35
- (instancetype)initWithFrame:(CGRect)frame andString:(NSString*)str{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatTextfildString:str];
    }
    return self;
}

- (instancetype)initWithTwoFrame:(CGRect)frame andString:(NSString*)str{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatTextfildTwoString:str];
    }
    return self;
}

-(__kindof MBZPKTextField*)creatTextfildString:(NSString*)Str{
    _textfield = [PxlTextView createMBZPKTextField];
    [self addSubview:_textfield];
    [_textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-5);
        make.left.equalTo(self).offset(85);
        make.centerY.equalTo(self);
        make.height.equalTo(@35);
    }];
    
    _titleLabel = [PxlTextView createLabel];
    [self addSubview:_titleLabel];
    
    _titleLabel.text = Str;
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_textfield.mas_left).offset(-5);
        make.centerY.equalTo(_textfield);
        make.left.equalTo(self);
        //        make.left.greaterThanOrEqualTo(label.mas_left).with.priorityLow();
        
        //        make.width.greaterThanOrEqualTo(_textfield.mas_width).with.priorityHigh(40);
    }];
    
    return _textfield;
}


-(__kindof MBZPKTextField*)creatTextfildTwoString:(NSString*)Str{
    _textfield = [PxlTextView createMBZPKTextField];
    [self addSubview:_textfield];
    [_textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-5);
        make.left.equalTo(self).offset(110);
        make.centerY.equalTo(self);
        make.height.equalTo(@35);
    }];
    
    _titleLabel = [PxlTextView createLabel];
    [self addSubview:_titleLabel];
    
    _titleLabel.text = Str;
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_textfield.mas_left).offset(-5);
        make.centerY.equalTo(_textfield);
        make.left.equalTo(self);
    }];
    
    return _textfield;
}


+(__kindof MBZPKTextField*)createMBZPKTextField{
    MBZPKTextField *textfield1 = [[MBZPKTextField alloc] init];
    [textfield1 setFont:[UIFont systemFontOfSize:14]];
    textfield1.textColor = [UIColor blackColor];
    [textfield1 setTextAlignment:NSTextAlignmentLeft];
    //    textfield1.secureTextEntry = YES;
    //    [self.view addSubview:textfield1];
    
    return textfield1;
}

+(__kindof UILabel*)createLabel{
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    label1.textAlignment = NSTextAlignmentRight;
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [label1 sizeToFit];
    //    [self.view addSubview:label1];
    
    return label1;
}


+(__kindof UIButton*)createButton:(NSString *)titileStr{
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:titileStr forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    //    [button addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    //    [self.view addSubview:button];
    //    [button mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.right.equalTo(self.view.mas_right).offset(-5);
    //        make.left.equalTo(@5);
    //        make.height.equalTo(@40);
    //        make.top.equalTo(self.view).offset(170+num);
    //    }];
    
    return button;
}


+ (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}


@end
