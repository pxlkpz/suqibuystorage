//
//  UITabBar+PxlBadge.h
//  suqibuyApp
//
//  Created by apple on 16/6/20.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITabBar (PxlBadge)



- (void)showBadgeOnItemIndex:(int)index;   //显示小红点

- (void)hideBadgeOnItemIndex:(int)index; //隐藏小红点  @end

@end