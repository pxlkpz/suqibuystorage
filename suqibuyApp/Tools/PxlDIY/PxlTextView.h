//
//  PxlTextView.h
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MBZPKTextField.h"
@interface PxlTextView : UIView

- (instancetype)initWithFrame:(CGRect)frame andString:(NSString*)str;
- (instancetype)initWithTwoFrame:(CGRect)frame andString:(NSString*)str;

+(__kindof UILabel*)createLabel;
+(__kindof MBZPKTextField*)createMBZPKTextField;
+(__kindof UIButton*)createButton:(NSString *)titileStr;
//判断是不是数字
+ (BOOL)validateNumber:(NSString*)number;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) MBZPKTextField *textfield;

@end
