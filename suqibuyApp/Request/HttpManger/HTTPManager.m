//
//  HTTPManager.m
//  yunbo2016
//
//  Created by apple on 15/10/22.
//  Copyright © 2015年 apple. All rights reserved.
//

#import "HTTPManager.h"
#import "AFAppNetClient.h"
//#import "YhjRootViewController.h"

#define HTTPKEY       @"dbc7f0747d204257b3ea0f9338a7958d"
#define SHOWSERVICE   @"User.applyForInvoice"

@implementation HTTPManager

typedef NS_ENUM(NSUInteger, CPMethodType) {
    CPMethodTypeGET,
    CPMethodTypePOST,
    CPMethodTypeUpload,
    CPMethodTypeDownload
};

+ (NSURLSessionDataTask *)requestWithMethod:(CPMethodType)type getDataWithApiPre:(NSString *)apiPre GetParameters:(NSDictionary *)getParameters andPostParameters:(NSDictionary *)postParameters andBlocks:(void (^)(NSDictionary *result))block{
    
    switch (type) {
            case CPMethodTypeGET:
        {
            return [[AFAppNetClient sharedClient] GET:apiPre parameters:getParameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSInteger ret = [responseObject[@"ret"] integerValue];
                NSDictionary *resultdic = (NSDictionary *)responseObject;
                if (ret==200) {
                    if (block) {
                        block(resultdic[@"data"]);
                    }
                }else if (ret==401)
                {
                    
                }else
                {
                    block(nil);
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                if (error.code == -1001) {
                    //                    超时
                }
                
            }];
        }
            break;
            case CPMethodTypePOST:
        {
            return [[AFAppNetClient sharedClient] POST:apiPre parameters:postParameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSDictionary *resultdic = (NSDictionary *)responseObject;
                if([[postParameters objectForKey:@"service"]
                    isEqualToString:SHOWSERVICE]){
                    NSLog(@"%@",responseObject);
                }
                
                NSNumber *ret = [responseObject objectForKey:@"ret"];
                if ([ret isEqualToNumber: @200]) {
                    
                    if (block) {
                        block(resultdic[@"data"]);
                    }
                } else if ([ret isEqualToNumber:@401])
                {
                    
                }
                else
                {
                    block(nil);
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
                if (error.code == -1001) {
                    
                    //                    超时,请检查网络"];
                }
                block(nil);
            }];
        }
            break;
            case CPMethodTypeUpload:
        {
            return [[AFAppNetClient sharedClient] POST:apiPre parameters:postParameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                if ([getParameters[@"pics"] isKindOfClass:[NSArray class]]) {
                    NSArray *imageArr = getParameters[@"pics"];
                    for(int i= 0 ; i< imageArr.count; i++){
                        NSData *imageData = imageArr[i];
                        [formData appendPartWithFileData:imageData name:[NSString stringWithFormat:@"user_data[avatar]"] fileName:[NSString stringWithFormat:@"%d.jpg",i+1] mimeType:@"image/jpeg"];
                    }

                } else {
                    [formData appendPartWithFileData:getParameters[@"pics"] name:[NSString stringWithFormat:@"file"] fileName:[NSString stringWithFormat:@"%d.jpg",1] mimeType:@"image/jpeg"];

                }
            } progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                

                NSNumber *success = [responseObject objectForKey:@"success"];
                NSDictionary *resultdic = (NSDictionary *)responseObject;
                if ([success isEqualToNumber:@1]) {
                    block(resultdic);
                }else
                {
                    
                    block(nil);
                }
            } failure:^(NSURLSessionDataTask *task, NSError *error) {
                
                if (error.code == -1001) {
                    //                    [NTAlterView showAlertViewWithString:@"请求超时,请检查网络"];
                }
                
                block(nil);
            }];
            
        }
            break;
            case CPMethodTypeDownload:
        {
            
        }
            break;
    }
    return nil;
}


///: 数据请求
+ (NSURLSessionDataTask *)getDataWithUrl:(NSString *)urlPath andPostParameters:(NSDictionary *)postPatameters andBlocks:(void (^)(NSDictionary *result))block
{
    
    return [self requestWithMethod:CPMethodTypePOST getDataWithApiPre:[self configUrl:urlPath] GetParameters:nil andPostParameters:[self configCommonPamerDic:postPatameters] andBlocks:block];
}


///: 图片请求
+(NSURLSessionDataTask *) publishCarCirclWithUrl:(NSString *)urlPath andPostParameters:(NSDictionary *)postParamers andImageDic:(NSDictionary *)imagePatamer andBlocks:(void(^)(NSDictionary *result)) block
{
    return [self requestWithMethod:CPMethodTypeUpload getDataWithApiPre:[self configUrl:urlPath] GetParameters:imagePatamer andPostParameters:[self configCommonPamerDic:postParamers] andBlocks:block];
    
}

+(NSString *)configUrl:(NSString *)url{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",API_MAIN_URL,url];
    
    return urlStr;
}

+(NSDictionary *)configCommonPamerDic:(NSDictionary *)parameter
{
    
    NSMutableDictionary *parametersDic = [[NSMutableDictionary alloc]init];
    NSString *api_token = [self getApi_token];
    [parametersDic setObject:api_token forKey:@"api_token"];
    [parametersDic addEntriesFromDictionary:parameter];
    
    return parametersDic;
}
+(NSString *)getApi_token
{
    if ([UIScreen mainScreen].scale > 1)
    {
        if (screen_height == 480)
        {
            return @"IWantToGetAIphone6";
        }
        else if(screen_height == 568)
        {
            return @"IphoneRetinaEquipment";
        }
    }
    
    return @"IphoneOtherEquipment";
}

@end

