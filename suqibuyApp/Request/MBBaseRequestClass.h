//
//  MBBaseRequestClass.h
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import <Foundation/Foundation.h>

//定义返回请求数据的block类型
typedef void (^FinishBlock) (id returnValue);
typedef void (^ErrorBlock) (id errorCode);
typedef void (^FailedBlock)(void);
typedef void (^NetWorkBlock)(BOOL netConnetState);

@interface MBBaseRequestClass : NSObject

#pragma 监测网络的可链接性
//+ (BOOL) netWorkReachabilityWithURLString;

#pragma POST请求
+ (void) MBRequestPOSTWithRequestURL: (NSString *) requestURLString
                       WithParameter: (NSDictionary *) parameter
                     WithFinishBlock: (FinishBlock) finishBlock
                      WithErrorBlock: (ErrorBlock) errorBlock
                     WithFailedBlock: (FailedBlock) failedBlock;

///**
// *  检测更新调用
// *
// *  @param requestURLString
// *  @param parameter
// *  @param finishBlock   检测更新调用
// *  @param errorBlock
// *  @param failedBlock
// */
//+ (void)updateRequestPOSTWithRequestURL: (NSString *) requestURLString
//                          WithParameter: (NSDictionary *) parameter
//                        WithFinishBlock: (FinishBlock) finishBlock
//                         WithErrorBlock: (ErrorBlock) errorBlock
//                        WithFailedBlock: (FailedBlock) failedBlock;

@end
