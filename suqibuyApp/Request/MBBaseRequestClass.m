//
//  MBBaseRequestClass.m
//  MolBaseApp
//
//  Created by MOLBASE on 15/10/30.
//  Copyright © 2015年 MOLBASE. All rights reserved.
//

#import "MBBaseRequestClass.h"
#import "AFAppNetClient.h"
@implementation MBBaseRequestClass

#pragma 监测网络的可链接性
//+ (BOOL) netWorkReachabilityWithURLString
//{
//    __block BOOL netState = NO;
//
//    NSString *urlString = [NSString stringWithFormat:@"%@%@",API_MAIN_URL,API_SOFTHELP_NETWORK];
//    NSURL *baseURL = [NSURL URLWithString:urlString];
//
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
//    manager.requestSerializer.timeoutInterval = 30.0f;
//     [manager.requestSerializer setValue:httpHeader forHTTPHeaderField:@"User-Agent"];
//    NSOperationQueue *operationQueue = manager.operationQueue;
//
//    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
//        switch (status) {
//            case AFNetworkReachabilityStatusReachableViaWWAN:
//            case AFNetworkReachabilityStatusReachableViaWiFi:
//                [operationQueue setSuspended:NO];
//                netState = YES;
//                break;
//            case AFNetworkReachabilityStatusNotReachable:
//                netState = NO;
//            default:
//                [operationQueue setSuspended:YES];
//                break;
//        }
//    }];
//
//    [manager.reachabilityManager startMonitoring];
//
//    return netState;
//}

#pragma --mark POST请求方式

/***************************************
 在这做判断如果有dic里有errorCode
 调用errorBlock(dic)
 没有errorCode则调用block(dic
 ******************************/

+ (void) MBRequestPOSTWithRequestURL: (NSString *) requestURLString
                       WithParameter: (NSDictionary *) parameter
                     WithFinishBlock: (FinishBlock) finishBlock
                      WithErrorBlock: (ErrorBlock) errorBlock
                     WithFailedBlock: (FailedBlock) failedBlock
{
    NSMutableDictionary *parametersDic = [[NSMutableDictionary alloc]init];
    NSString *api_token = [self getApi_token];
    [parametersDic setObject:api_token forKey:@"api_token"];
    [parametersDic addEntriesFromDictionary:parameter];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",API_MAIN_URL,requestURLString];
    
    [[AFAppNetClient sharedClient] POST:urlStr parameters:parametersDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
        
        NSDictionary *dic = (NSDictionary *)responseObject;
        DDLog(@"%@", dic);
        finishBlock(dic);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failedBlock();
        
    }];
}



/**
 识别尺寸
 
 @return string
 */
+(NSString *)getApi_token
{
    if ([UIScreen mainScreen].scale > 1)
    {
        if (screen_height == 480)
        {
            return @"IWantToGetAIphone6";
        }
        else if(screen_height == 568)
        {
            return @"IphoneRetinaEquipment";
        }
    }
    return @"IphoneOtherEquipment";
}

+ (NSString *)md5TokenTime
{
    NSDate* dateTime = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval localeTimeInterval=[dateTime timeIntervalSince1970];
    NSString *localeTime = [NSString stringWithFormat:@"%.0lf", localeTimeInterval];
    
    int errorTime = [[MBToolsClass getNSUserDefaultsWithKey:@"index_time_diff"] intValue];
    int tokenTime = [localeTime intValue]+errorTime;
    
    NSLog(@"timeString=%@----%d----%d---",localeTime,errorTime,tokenTime);
    NSString *tokenTimeStr = [NSString stringWithFormat:@"%@%d",APP_MD5TOKEN_VALUE,tokenTime];
    NSString *md5Token = [NSString stringWithFormat:@"%@",[MBToolsClass creatMD5StringWithString:tokenTimeStr]];
    return [NSString stringWithFormat:@"%@%d",md5Token,tokenTime];
}

/**
 *  检测更新调用
 *
 *  @param requestURLString
 *  @param parameter
 *  @param finishBlock   检测更新调用
 *  @param errorBlock
 *  @param failedBlock
 */
//+ (void)updateRequestPOSTWithRequestURL: (NSString *) requestURLString
//                       WithParameter: (NSDictionary *) parameter
//                     WithFinishBlock: (FinishBlock) finishBlock
//                      WithErrorBlock: (ErrorBlock) errorBlock
//                     WithFailedBlock: (FailedBlock) failedBlock
//{
//    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
//    NSString *urlStr = [NSString stringWithFormat:@"%@",requestURLString];
//    manager.requestSerializer.timeoutInterval = 30.0f;
//    AFHTTPRequestOperation *op = [manager POST:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:nil];
//
//        DDLog(@"%@", dic);
//
//        finishBlock(dic);
//
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        failedBlock();
//    }];
//    op.responseSerializer = [AFHTTPResponseSerializer serializer];
//    [op start];
//}

@end

