//
//  SQRegisterVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQRegisterVC.h"
#import "MBZPKTextField.h"
#import "SQUser.h"
#import "MBSingleWebViewController.h"

@interface SQRegisterVC ()
{
    __weak IBOutlet MBZPKTextField *emailTextField;
    __weak IBOutlet MBZPKTextField *nameTextField;
    __weak IBOutlet MBZPKTextField *passwordTextField;
    __weak IBOutlet MBZPKTextField *repeatPasswordTextField;
    __weak IBOutlet MBZPKTextField *codeTextField;
    
    __weak IBOutlet UIButton *registerButton;
    __weak IBOutlet UIButton *lookButton;
    
    __weak IBOutlet NSLayoutConstraint *emailLabelTop;
    __weak IBOutlet NSLayoutConstraint *emailTextFieldTop;
    __weak IBOutlet NSLayoutConstraint *registerButtonTop;
    __weak IBOutlet NSLayoutConstraint *lookButtonTop;
}

@end

@implementation SQRegisterVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"免费注册";
    
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    registerButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
    registerButton.layer.cornerRadius = allCornerRadius;
    
    [lookButton setTitleColor:[UIColor colorWithHexString:button_blue] forState:UIControlStateNormal];
    
    if (screen_height <= 480)
    {
        emailLabelTop.constant = 20;
        emailTextFieldTop.constant = 20;
        registerButtonTop.constant = 20;
        lookButtonTop.constant = 20;
    }
}

#pragma mark - 注册新用户
- (IBAction)registerNew:(id)sender
{
    NSString *email = emailTextField.text;
    NSString *nickname = nameTextField.text;
    NSString *password = passwordTextField.text;
    NSString *password_confirmation = repeatPasswordTextField.text;
    //    NSString *online_pref = @"1";
    NSString *invitation_code = codeTextField.text;
    
    
    if (email.length == 0)
    {
        showDefaultPromptTextHUD(@"邮箱不能为空");
        return;
    }
    else
    {
        if (![MBZPKTools checkEmail:email])
        {
            showDefaultPromptTextHUD(@"邮箱格式不对");
            return;
        }
    }
    
    if (nickname.length == 0)
    {
        showDefaultPromptTextHUD(@"昵称不能为空");
        return;
    }
    else
    {
        //        if (nickname.length <6 || nickname.length > 18)
        //        {
        //            showDefaultPromptTextHUD(@"昵称长度应在6-18位");
        //            return;
        //        }
    }
    
    if (password.length == 0)
    {
        showDefaultPromptTextHUD(@"密码不能为空");
        return;
    }
    else
    {
        if (password.length <6 || password.length > 18)
        {
            showDefaultPromptTextHUD(@"密码长度应在6-18位");
            return;
        }
    }
    
    if (![password isEqualToString:password_confirmation])
    {
        showDefaultPromptTextHUD(@"确认密码与密码不一致");
        return;
    }
    
    NSDictionary *dic = nil;
    if (invitation_code.length == 0)
    {
        dic = @{@"email":email, @"nickname": nickname, @"password":password, @"password_confirmation":password_confirmation};
    }
    else
    {
        dic = @{@"email":email, @"nickname": nickname, @"password":password, @"password_confirmation":password_confirmation, @"invitation_code":invitation_code};
    }
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_USER_SIGNUP WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                showDefaultPromptTextHUD(@"注册成功");
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
//                    SQUserModel *model = [[SQUserModel alloc] initWithDic:result];
                    SQUserModel * model = [SQUserModel mj_objectWithKeyValues:result];
                    //                    [SQUser clear];
                    [SQUser save:model];
                    [[NSNotificationCenter defaultCenter] postNotificationName:SQ_REGISTER_SUCCESS_NOTIFICATION object:nil];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark - 查看用户协议
- (IBAction)look:(id)sender
{
    MBSingleWebViewController *controller = [[MBSingleWebViewController alloc] init];
    controller.type = 0;
    [self.navigationController pushViewController:controller animated:YES];
}

@end
