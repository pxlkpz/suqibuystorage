//
//  SQUser.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/22.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQUser.h"
#import "SQLoginVC.h"
@implementation SQUser

+(void)save:(SQUserModel *)model
{
    NSDictionary* userDic = model.mj_keyValues;
    [[NSUserDefaults standardUserDefaults] setObject:userDic forKey:@"SQUserModelDataDefaults"];
    [self saveDiction:userDic];
    ///保存前，先删除上个用户的
}

+(void)saveDiction:(NSDictionary *)userDic
{
    [[NSUserDefaults standardUserDefaults] setObject:userDic forKey:@"SQUserModelDataDefaults"];
    ///保存前，先删除上个用户的
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(SQUserModel *)loadmodel
{
    NSDictionary* dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"SQUserModelDataDefaults"];
    
    SQUserModel *model = [SQUserModel mj_objectWithKeyValues:dic];

    return model;
}

+(NSDictionary *)loadDictionary
{
    NSDictionary* dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"SQUserModelDataDefaults"];
    
    return dic;
}


+(void) clear
{
    SQUserModel *model = [[SQUserModel alloc] init];
    [model clearData];
    NSDictionary* userDic = model.mj_keyValues;
    [[NSUserDefaults standardUserDefaults] setObject:userDic forKey:@"SQUserModelDataDefaults"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(NSString *) testDic:(NSMutableDictionary *) dic key:(NSString *) key
{
    if ([dic objectForKey:key] == nil)
    {
        return @"";
    }
    else
    {
        return [dic objectForKey:key];
    }
}

/**
 判断是否登录

 @return yes:登录， no：未登录
 */
+(BOOL)isLogin {
    NSString *user_id = [self loadmodel].user_id;
    if (user_id.length == 0)
    {
        return NO;
    }
    
    return YES;
}

/**
 是否登录

 @param controll self
 @return yes 弹出登录页面 no 未弹出
 */
+(BOOL) LoginWitchController:(UIViewController *)controll{
    
    if (![self isLogin]) {
        SQLoginVC *log = [[SQLoginVC alloc]init];
        UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:log];
        navi.navigationBar.backgroundColor = [UIColor whiteColor];
        navi.navigationBar.translucent = NO;
        navi.interactivePopGestureRecognizer.enabled = YES;
        [controll presentViewController:navi animated:YES completion:nil];
        return YES;
    }

    return NO;
}

@end
