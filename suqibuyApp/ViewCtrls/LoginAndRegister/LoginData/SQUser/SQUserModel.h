//
//  SQModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/22.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "MBZPKBaseModel.h"

@interface SQUserModel : MBZPKBaseModel

@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *user_token;
@property (strong, nonatomic) NSString *displayname;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *mobile;
@property (strong, nonatomic) NSString *remain_cash;
@property (strong, nonatomic) NSString *freeze_amount;
@property (strong, nonatomic) NSString *group_name;
@property (strong, nonatomic) NSString *group_id;
@property (strong, nonatomic) NSString *credits_amount;
@property (strong, nonatomic) NSString *growth_score;

@property (strong, nonatomic) NSString *msg_count; // new_msg_count
@property (strong, nonatomic) NSString *avatar_url;
@property (strong, nonatomic) NSString *ad_image;
@property (strong, nonatomic) NSString *coupon_count;

@property (strong, nonatomic) NSString *signedToday;
@property (strong, nonatomic) NSString *sequentialSignedDays;
@property (strong, nonatomic) NSString *has_new_ver;
//@property (strong, nonatomic) NSString *new_verion;




//"user_id": "2",
//"user_token": "UID-5aba12ec9b5f4e9baa07b8f5e703090a2fd4b586",
//"displayname": "robinfu",
//"email": "robinfu@shtag.com",
//"mobile": "",
//"remain_cash": "¥95,963.15",
//"freeze_amount": "¥50.00",
//"group_name": "VIP1",
//"group_id": "5",
//"credits_amount": "2334472",
//"growth_score": "2344080",
//"new_msg_count": 157,
//"avatar_url": "http://t1.shtag.com/uploads/user/avatar/0326/37dba8bdd8831a2f747a11ae6e4eb6c6a9ec1888.jpg",
////"couponCount": "30",
//"signedToday": false,
//"sequentialSignedDays": 0,
//"has_new_ver": false,
//"new_verion": null,
//"url": null



-(void)clearData;
-(instancetype) initWithDic:(NSDictionary *) dic;
@end
