//
//  SQSwiftUserModel.swift
//  suqibuyApp
//
//  Created by pxl on 2018/3/16.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

import Foundation
import ObjectMapper
class SQSwiftUserModel: Mappable {

    var user_id : String?
    var user_token : String?
    var displayname : String?
    var email : String?
    var mobile : String?
    var remain_cash : String?
    var freeze_amount : String?
    var group_name : String?
    var group_id : String?
    var credits_amount : String?
    var growth_score : String?
    
    ///new_msg_count
    var msg_count : String?
    var avatar_url : String?
    var ad_image : String?
    var coupon_count : String?
    
    var signedToday : String?
    var sequentialSignedDays : String?
    var has_new_ver : String?


    
    
    
    func mapping(map: Map)
    {
        user_id <- map["user_id"]
        user_token <- map["user_token"]
        displayname <- map["displayname"]
        email <- map["email"]
        mobile <- map["mobile"]
        remain_cash <- map["remain_cash"]
        freeze_amount <- map["freeze_amount"]
        group_name <- map["group_name"]
        group_id <- map["group_id"]
        credits_amount <- map["credits_amount"]
        growth_score <- map["growth_score"]
        msg_count <- map["msg_count"]
        avatar_url <- map["avatar_url"]
        ad_image <- map["ad_image"]
        coupon_count <- map["coupon_count"]
        
        signedToday <- map["signedToday"]
        sequentialSignedDays <- map["sequentialSignedDays"]
        has_new_ver <- map["has_new_ver"]

    }
    
    
    required init?(map: Map) {
        
    }

}
