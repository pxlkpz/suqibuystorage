//
//  SQUser.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/22.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyChain.h"
#import "SQUserModel.h"

@interface SQUser : NSObject
+(void) save:(SQUserModel *) model;
+(void)saveDiction:(NSDictionary *)userDic;
+(void) clear;
+(BOOL)isLogin;
+(BOOL)LoginWitchController:(UIViewController *)controll;

+(SQUserModel *) loadmodel;
+(NSDictionary *)loadDictionary;


@end
