//
//  SQModel.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/22.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQUserModel.h"
#import "JPUSHService.h"
@implementation SQUserModel

-(instancetype)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self)
    {
        self.user_id = [self testNSStringInDic:dic string:@"user_id"];
        self.user_token = [self testNSStringInDic:dic string:@"user_token"];
        self.displayname = [self testNSStringInDic:dic string:@"displayname"];
        self.email = [self testNSStringInDic:dic string:@"email"];
        self.mobile = [self testNSStringInDic:dic string:@"mobile"];
        self.remain_cash = [self testNSStringInDic:dic string:@"remain_cash"];
        self.freeze_amount = [self testNSStringInDic:dic string:@"freeze_amount"];
        self.group_name = [self testNSStringInDic:dic string:@"group_name"];
        self.group_id = [self testNSStringInDic:dic string:@"group_id"];
        self.credits_amount = [self testNSStringInDic:dic string:@"credits_amount"];
        self.growth_score = [self testNSStringInDic:dic string:@"growth_score"];
        self.ad_image = [self testNSStringInDic:dic string:@"ad_image"];
        self.coupon_count = [self testNSStringInDic:dic string:@"coupon_count"];

        
        self.signedToday = [self testNSStringInDic:dic string:@"signedToday"];
        self.sequentialSignedDays = [self testNSStringInDic:dic string:@"sequentialSignedDays"];
        self.has_new_ver = [self testNSStringInDic:dic string:@"has_new_ver"];

        

        id msg_count = [dic objectForKey:@"new_msg_count"];
        if (msg_count != nil && msg_count != [NSNull null] && [msg_count isKindOfClass:[NSNumber class]])
        {
            self.msg_count = [NSString stringWithFormat:@"%ld",(long)[msg_count integerValue]];
        }
        else
        {
            self.msg_count = @"";
        }
        self.avatar_url = [self testNSStringInDic:dic string:@"avatar_url"];
        
        
        
//        __autoreleasing NSMutableSet *tags = [NSMutableSet set];
//
//
//
//        __autoreleasing NSString *alias = _user_id;
//
//
//        [JPUSHService setTags:tags
//                        alias:alias
//             callbackSelector:@selector(tagsAliasCallback:tags:alias:)
//                       object:nil];
        
        
    }
    return self;
}


- (void)tagsAliasCallback:(int)iResCode
                     tags:(NSSet *)tags
                    alias:(NSString *)alias {
    NSString *callbackString =
    [NSString stringWithFormat:@"%d, \ntags: %@, \nalias: %@\n", iResCode,
     [self logSet:tags], alias];
    //    if ([_callBackTextView.text isEqualToString:@"服务器返回结果"]) {
    //        _callBackTextView.text = callbackString;
    //    } else {
    //        _callBackTextView.text = [NSString
    //                                  stringWithFormat:@"%@\n%@", callbackString, _callBackTextView.text];
    //    }
    NSLog(@"TagsAlias回调:%@", callbackString);
}

- (NSString *)logSet:(NSSet *)dic {
    if (![dic count]) {
        return nil;
    }
    NSString *tempStr1 =
    [[dic description] stringByReplacingOccurrencesOfString:@"\\u"
                                                 withString:@"\\U"];
    NSString *tempStr2 =
    [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 =
    [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    //NSString *str =
    //[NSPropertyListSerialization propertyListFromData:tempData
    //                                 mutabilityOption:NSPropertyListImmutable
   //                                            format:NULL
   //                                  errorDescription:NULL];
    
    NSString *str = [NSPropertyListSerialization propertyListWithData:tempData
                                options:NSPropertyListImmutable
                                               format:NULL
                                                error: nil
                                        ];
    
    return str;
}


+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"msg_count":@"new_msg_count"};
}

-(void)clearData{
    self.user_id = @"";
    self.user_token = @"";
    self.displayname = @"";
    self.email = @"";
    self.mobile = @"";
    self.remain_cash =@"";
    self.freeze_amount = @"";
    self.group_name = @"";
    self.group_id = @"";
    self.credits_amount = @"";
    self.growth_score = @"";
    
    self. msg_count = @"";
    self.avatar_url = @"";
    
    self.coupon_count = @"";

    self.signedToday = @"";
    self.sequentialSignedDays = @"";
    self.has_new_ver = @"";

    
}
@end
