//
//  SQLoginVC.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/22.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"
#import "AppDelegate.h"

@interface SQLoginVC : SQBaseViewController

@property (weak, nonatomic) AppDelegate *appDelegate;
@end
