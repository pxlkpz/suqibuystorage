//
//  SQLoginVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/22.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQLoginVC.h"
#import "MBZPKTextField.h"
#import "SQUserModel.h"
#import "SQRetrievePasswordVC.h"
#import "SQRegisterVC.h"


@interface SQLoginVC ()<UITextFieldDelegate>
{
    __weak IBOutlet MBZPKTextField *accountTextField;
    __weak IBOutlet MBZPKTextField *passwordTextField;
    
    __weak IBOutlet UIButton *forgetButton;
    __weak IBOutlet UIButton *loginButton;
    __weak IBOutlet UIButton *registerButton;
    
    __weak IBOutlet NSLayoutConstraint *loginButtonTop;
}

@end

@implementation SQLoginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"登录";
    
    accountTextField.placeholder = @"注册邮箱地址";
    accountTextField.layer.borderWidth = 1;
    accountTextField.layer.borderColor = [UIColor colorWithHexString:text_gray].CGColor;
    accountTextField.layer.cornerRadius = allCornerRadius;
    accountTextField.delegate = self;
        
    passwordTextField.placeholder = @"密码";
    passwordTextField.layer.borderWidth = 1;
    passwordTextField.layer.borderColor = [UIColor colorWithHexString:text_gray].CGColor;
    passwordTextField.layer.cornerRadius = allCornerRadius;
    passwordTextField.secureTextEntry = YES;
    passwordTextField.delegate = self;
    
    [forgetButton setTitleColor:[UIColor colorWithHexString:text_blue] forState:UIControlStateNormal];
    
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    loginButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
    loginButton.layer.cornerRadius = allCornerRadius;
    
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    registerButton.backgroundColor = [UIColor colorWithHexString:button_blue];
    registerButton.layer.cornerRadius = allCornerRadius;
    
    if (screen_height <= 480)
    {
        loginButtonTop.constant = 55;
    }
    
   
    [self setLeftNavigationItemWithCustomView: [self createBackButton]];
    
}

- (void)onBack {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) loginSuccess
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"SQwoVCIsRequst" object:nil];

    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)forgetPassword:(id)sender
{
    SQRetrievePasswordVC *controller = [[SQRetrievePasswordVC alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)login:(id)sender
{
    NSString *username = accountTextField.text;
    NSString *password = passwordTextField.text;
    
    if (username.length == 0)
    {
        showDefaultPromptTextHUD(@"账户名不能为空");
        return;
    }
    
    if (password.length == 0)
    {
        showDefaultPromptTextHUD(@"密码不能为空");
    }
    
    NSDictionary *dic = @{@"username":username, @"password": password};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_USER_LOGIN WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    SQUserModel * model = [SQUserModel mj_objectWithKeyValues:result];

                    [SQUser save:model];
                    [self loginSuccess];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

- (IBAction)registerNew:(id)sender
{
    SQRegisterVC *controller = [[SQRegisterVC alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - textfield代理
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self login:nil];
    [textField resignFirstResponder];
    return YES;
}

@end
