//
//  SQRetrievePasswordVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQRetrievePasswordVC.h"
#import "MBZPKTextField.h"

@interface SQRetrievePasswordVC ()
{
    __weak IBOutlet UILabel *reminderLabel;
    __weak IBOutlet UILabel *emailLabel;
    __weak IBOutlet MBZPKTextField *emailTextField;
    __weak IBOutlet UILabel *redReminderLabel;
    __weak IBOutlet UIButton *resetButton;
    __weak IBOutlet NSLayoutConstraint *redLabelTop;
}

@end

@implementation SQRetrievePasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"找回密码";
    
    reminderLabel.textColor = [UIColor colorWithHexString:text_color];
    reminderLabel.font = [UIFont systemFontOfSize:14];
    reminderLabel.text = @"请在下面输入您注册时所使用的邮箱地址。\n\n系统将发送一个可以重设密码的链接到您邮箱，你可以点击该链接重设新的密码。\n\n这个链接很重要，请不要告诉其它任何人。";
    
    emailLabel.textColor = [UIColor colorWithHexString:text_color];
    emailLabel.font = [UIFont systemFontOfSize:14];
    
    emailTextField.placeholder = @"请输入邮箱";
    emailTextField.font = [UIFont systemFontOfSize:14];
    emailTextField.layer.borderWidth = 1;
    emailTextField.layer.borderColor = [UIColor colorWithHexString:text_gray].CGColor;
    emailTextField.layer.cornerRadius = allCornerRadius;
    
    redReminderLabel.textColor = [UIColor colorWithHexString:text_red];
    redReminderLabel.font = [UIFont systemFontOfSize:14];
    redReminderLabel.text = @"重设密码邮件已成功发送至上方的邮箱。\n\n请重送你的邮箱接收重新密码邮件，并根据邮箱中的提示完成重设密码。\n\n如果你在 5分钟后 还没有收到邮件，请检查您的邮箱中的“垃圾收件箱”文件夹。";
    redReminderLabel.hidden = YES;
    
    [resetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    resetButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
    resetButton.layer.cornerRadius = allCornerRadius;
    
    if (screen_height <= 480)
    {
        redLabelTop.constant = 35;
    }
}

- (IBAction)resetPassword:(id)sender
{
    NSString *email = emailTextField.text;
    
    if (email.length == 0)
    {
        showDefaultPromptTextHUD(@"邮箱不能为空");
        return;
    }
    else
    {
        if (![MBZPKTools checkEmail:email])
        {
            showDefaultPromptTextHUD(@"邮箱格式不对");
            return;
        }
    }
    
    NSDictionary *dic = @{@"email":email};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_USER_FORGOTTENPWD WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && result != [NSNull null] && [result isKindOfClass:[NSDictionary class]])
                {
                    id message = [result objectForKey:@"message"];
                    if (message != nil && [message isKindOfClass:[NSString class]])
                    {
                        showDefaultPromptTextHUD(message);
                    }
                }
                
                resetButton.hidden = YES;
                redReminderLabel.hidden = NO;
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

@end
