//
//  SQzhuanyunListVC.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunListVC.h"
#import "SQzhuanyunListModel.h"
//#import "SQFillShopListCell.h"
#import "SQzhuanyunListOrderDetails.h"
#import <objc/runtime.h>
#import "SQzhuanyunListCell.h"
static NSString *identifier = @"SQzhuanyunListCell";

@interface SQzhuanyunListVC ()<UITableViewDataSource,UITableViewDelegate>
{
    
    SQzhuanyunListModel*_listModel;
    
}
@property(nonatomic, strong) UITableView *listTableView;
@property(nonatomic, assign) NSInteger currentPage;
@end

@implementation SQzhuanyunListVC


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发货订单列表";
    
    [self buildclearView];
    
    [self buildTableView];
    
    self.currentPage = 1;
    [self requestData:1];
    
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

#pragma mark - ---- event response 🍐🌛
#pragma mark -进入订单详情
-(void) orderInformation:(UITapGestureRecognizer *) tap
{
    
    SQzhuanyunLIstItemsModel *model = _listModel.items[tap.view.tag];
    
    SQzhuanyunListOrderDetails *controller = [[SQzhuanyunListOrderDetails alloc] init];
    controller.model = model;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - ---- private methods 🍊🌜

-(void)buildclearView{
    
    UILabel* noItemLabel = [[UILabel alloc] init];
    noItemLabel.text = @"当前列表为空！";
    noItemLabel.textColor = [UIColor colorWithHexString:text_gray];
    noItemLabel.textAlignment = NSTextAlignmentCenter;
    noItemLabel.font = [UIFont systemFontOfSize:14];
    noItemLabel.numberOfLines = 2;
    [self.view addSubview:noItemLabel];
    [noItemLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
    }];
    
}

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    _listTableView.hidden = YES;
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 80.0f;
    //[_listTableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(@0);
    }];
    [_listTableView setTableFooterView:[ [UIView alloc]init]];
    UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
    [_listTableView registerNib:nib forCellReuseIdentifier:identifier];
    
    @weakify(self);
    
    
    
    
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:1];
        self.currentPage = 1;
    }];
    
    _listTableView.mj_footer  = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:self.currentPage+1];
    }];
//    _listTableView.mj_footer.hidden = YES;
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}

#pragma mark -请求列表数据
-(void) requestData:(NSInteger ) page
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSString *pageString = [NSString stringWithFormat:@"%ld",(long)page];
    NSDictionary *dic = @{@"user_token":user_token,@"page":pageString};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_transship_orderlist WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if (page == 1)
                        {
                            _listModel = [SQzhuanyunListModel mj_objectWithKeyValues:result];
                            [_listTableView.mj_header endRefreshing];
                            [_listTableView.mj_footer endRefreshing];
                            
                            
                            
                            [_listTableView reloadData];
                            
                            if (_listModel.items.count != 0) {
                                _listTableView.hidden = NO;
//                                _listTableView.mj_footer.hidden = NO;
                                
                            }
                            
                            return ;
                        }
                        else
                        {
                            if ([items count] == 0)
                            {
                                showDefaultPromptTextHUD(@"没有更多内容了");
                                [self.listTableView.mj_footer endRefreshingWithNoMoreData];
                                
                                return ;

                            }
                            else
                            {
                                self.currentPage++;
                            }
                        }
                        NSArray* array = [SQzhuanyunLIstItemsModel mj_objectArrayWithKeyValuesArray:items];
                        [_listModel.items addObjectsFromArray:array];
                        
                        [_listTableView reloadData];
//                        _listTableView.mj_footer.hidden = NO;
                        
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark - ---- delegate 🍎🌝
#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark- tableviewdelegate
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return _listModel.items.count;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SQzhuanyunLIstItemsModel *model = _listModel.items[section];
    return model.goods.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SQzhuanyunListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    SQzhuanyunLIstItemsModel *model = _listModel.items[indexPath.section];
    SQzhuanyunListGoodsModel *good = model.goods[indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell setData:good];
    
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SQzhuanyunLIstItemsModel *model = _listModel.items[section];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 45)];
    view.backgroundColor = [UIColor whiteColor];
    view.tag = section;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(orderInformation:)];
    
    [view addGestureRecognizer:tap];
    
    
    UILabel *label= [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@" 订单号:%@ ",model.package_no];
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view addSubview:label];
    [label sizeToFit];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(@15);
        make.height.equalTo(@30);
    }];
    
    UILabel *label2= [[UILabel alloc] init];
    label2.text =  model.status_label ;
    label2.font = [UIFont systemFontOfSize:13];
    label2.textColor =  [UIColor colorWithHexString:order_back_color];
    
    [view addSubview:label2];
    [label2 sizeToFit];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@15);
        make.right.equalTo(view).offset(-5);
        make.height.equalTo(@30);
    }];
    
    
    label2.textColor = [self orderStatusBackground:model];
    
    
    
    UIView *view1 = [[UIView alloc]init];
    [view addSubview:view1];
    view1.alpha = 0.6;
    view1.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    
    
    
    return view;
}



-(UIColor*)orderStatusBackground:(SQzhuanyunLIstItemsModel*)model{
    
    
    UIColor *color = [UIColor blackColor];
    if ([model.status isEqualToString:@"pendding"]) {
        color = [UIColor colorWithHexString:@"#83d79b"];
    }
    else if ([model.status isEqualToString:@"daifukuan"]) {
        color = [UIColor colorWithHexString:@"#FF4081"];
        
    }
    else if ([model.status isEqualToString:@"waiting_payment_confirm"]
             ||[model.status isEqualToString:@"confirm_weightsize"]
             ||[model.status isEqualToString:@"recalculate_total"]
             ||[model.status isEqualToString:@"holded"]
             ||[model.status isEqualToString:@"paid_less"]
             ||[model.status isEqualToString:@"waiting_paid"]
             ||[model.status isEqualToString:@"waiting_paid_confirmed"]
             ||[model.status isEqualToString:@"paid_confirmed"]
             ||[model.status isEqualToString:@"shipping"]
             ) {
        color = [UIColor colorWithHexString:@"#008000"];
        //        color = KColorFromRGB(0xe49a72);
    }
    else if ([model.status isEqualToString:@"complete"]) {
        //        color = KColorFromRGB(0xe49a72);
        color = [UIColor colorWithHexString:@"#83d79b"];
        
        
    }
    else if ([model.status isEqualToString:@"canceled"]) {
        color = [UIColor colorWithHexString:@"#d9dada"];
        
        
        
    }
    
    return color;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];
    view.tag = section;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(orderInformation:)];
    [view addGestureRecognizer:tap];
    
    
    UILabel *statusLabel= [[UILabel alloc] init];
    //                           initWithFrame:CGRectMake(0, 0, screen_width, 40)];
    statusLabel.text = @"查看订单详情";
    statusLabel.textAlignment = NSTextAlignmentRight;
    statusLabel.font = [UIFont systemFontOfSize:13];
    [statusLabel sizeToFit];
    statusLabel.textColor = [UIColor colorWithHexString:button_blue];
    [view addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-5);
        make.bottom.equalTo(view).offset(-5);
    }];
    
    
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    return view;
}
-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 30;
}


-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

#pragma mark - ---- getters and setters 🍋🌞



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
