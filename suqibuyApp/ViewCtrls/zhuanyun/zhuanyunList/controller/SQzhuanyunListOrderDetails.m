
//
//  SQzhuanyunListOrderDetails.m
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunListOrderDetails.h"
#import "SQzhuanyunListOrderDetailCell.h"
#import "SQZhuanyunQuoteVCCell.h"
#import "SQzhuanyunListCell.h"
#import "UIAlertView+BlocksKit.h"
#import "SQzhuanyunPay.h"
static NSString *identifier = @"SQzhuanyunListOrderDetailCell";

@interface SQzhuanyunListOrderDetails ()<UITableViewDelegate,UITableViewDataSource>{
    UIView* _bottomView;
    UIButton* _cancelButton;
    UIButton* _sureButton;
}
@property (nonatomic, strong) UITableView *listTableView;

@end

#pragma mark - ---- lefe cycle 🍏🌚
@implementation SQzhuanyunListOrderDetails

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.modelDic) {
        self.model = [SQzhuanyunLIstItemsModel mj_objectWithKeyValues:self.modelDic];
    }
    
    self.title = [NSString stringWithFormat:@"%@发货订单详情", _model.package_no];
    
    [self buildTableView];
    
    [self buildBottomView];
    
    [self BottomViewButtonChange];
    
    [self requestGetOrderInfo:self.model.package_no];

    
    
}


#pragma mark - ---- event response 🍐🌛
-(void)cencelButtonAction:(UIButton*)sender{
    [UIAlertView bk_showAlertViewWithTitle:nil message:@"您确定要取消该订单吗?\n取消后不可以恢复,不过可以重新下单。" cancelButtonTitle:@"否" otherButtonTitles:@[@"是"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            [self requestDataCancelOrder:_model.package_no];
        }
    }];
}
-(void)sureButtonAction:(UIButton*)sender{
    SQzhuanyunPay *vc = [[SQzhuanyunPay alloc] init];
    vc.model = _model;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - ---- private methods 🍊🌜

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.tag = 5;
    _listTableView.delegate = self;
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 44.0f;
    UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
    [_listTableView registerNib:nib forCellReuseIdentifier:identifier];
    
    [_listTableView registerClass:[SQZhuanyunQuoteVCCell class] forCellReuseIdentifier:@"SQZhuanyunQuoteVCCell"];
    
    
    UINib *nib2 = [UINib nibWithNibName:@"SQzhuanyunListCell" bundle:nil];
    [_listTableView registerNib:nib2 forCellReuseIdentifier:@"SQzhuanyunListCell"];
    
    [_listTableView setTableFooterView:[[UIView alloc] init]];
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    @weakify(self);
    
    
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestGetOrderInfo:self.model.package_no];

    }];
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}
-(void)buildBottomView{
    _bottomView = [[UIView alloc] init];
    _bottomView.backgroundColor = KColorFromRGB(0xE2E2E2);
    [self.view addSubview:_bottomView];
    _bottomView.hidden = YES;
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        
        if(IsiPhoneX)
        {
            make.bottom.equalTo(self.view).offset(-24);
        }
        else
        {
            make.bottom.equalTo(self.view).offset(0);
        }
        
        
        make.height.equalTo(@50);
    }];
    
    UIView* leftView = [[UIView alloc] init];
    [_bottomView addSubview:leftView];
    [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(_bottomView);
        make.width.equalTo(_bottomView.mas_width).multipliedBy(0.5);
    }];
    
    
    
    _cancelButton = [[UIButton alloc] init];
    [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_cancelButton setTitle:@"取消订单" forState:UIControlStateNormal];
    [_cancelButton setBackgroundColor:KColorFromRGB(0xE2E2E2)];
    [_cancelButton addTarget:self action:@selector(cencelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:_cancelButton];
    [_cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(_bottomView);
        make.width.equalTo(_bottomView.mas_width).multipliedBy(0.5);
    }];
    
    _sureButton = [[UIButton alloc] init];
    [_sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_sureButton setTitle:@"支付订单" forState:UIControlStateNormal];
    [_sureButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    [_sureButton addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:_sureButton];
    [_sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(_bottomView);
        make.width.equalTo(_bottomView.mas_width).multipliedBy(0.5);
    }];
    
    [_sureButton layoutIfNeeded];
    [_cancelButton layoutIfNeeded];
}


-(void)BottomViewButtonChange{
    _bottomView.hidden = NO;
    [_listTableView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-50);
    }];
    
    if ([_model.can_cancel boolValue] && [_model.can_payment boolValue]) {
        [_cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.equalTo(_bottomView);
            make.width.equalTo(_bottomView.mas_width).multipliedBy(0.5);
        }];
        [_sureButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.equalTo(_bottomView);
            make.width.equalTo(_bottomView.mas_width).multipliedBy(0.5);
        }];
        
    }else if (![_model.can_cancel boolValue] && [_model.can_payment boolValue]){
        [_cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.equalTo(_bottomView);
            make.width.equalTo(@0);
        }];
        [_sureButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.equalTo(_bottomView);
            make.width.equalTo(_bottomView.mas_width);
        }];
        
    }else if ([_model.can_cancel boolValue] && ![_model.can_payment boolValue]){
        [_cancelButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.top.bottom.equalTo(_bottomView);
            make.width.equalTo(_bottomView.mas_width).multipliedBy(1);
        }];
        [_sureButton mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.equalTo(_bottomView);
            make.width.equalTo(@0);
        }];
        
    }else if (![_model.can_cancel boolValue] && ![_model.can_payment boolValue]){
        _bottomView.hidden = YES;
        [_listTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view).offset(0);
        }];
        
        
    }
}


-(void) requestDataCancelOrder:(NSString*)order_no
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"order_no":order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_transship_cancel WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void) requestGetOrderInfo:(NSString*)order_no
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"order_no":order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_transship_detail WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _model = [SQzhuanyunLIstItemsModel mj_objectWithKeyValues:result];
                    [_listTableView reloadData];
                    [self BottomViewButtonChange];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        [_listTableView.mj_header endRefreshing];
    } WithErrorBlock:^(id errorCode) {
        [_listTableView.mj_header endRefreshing];
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [_listTableView.mj_header endRefreshing];
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


#pragma mark - ---- delegate 🍎🌝

#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 4;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 12;
    }else if (section == 1){
        return 4;
    }else if (section == 2){
        return _model.goods.count;
    }
    else if (section == 3){
        return 8;
    }
    return 0;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        SQzhuanyunListOrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        [cell setdata:_model andindexRow:indexPath.row];
        
        return cell;
    }else if (indexPath.section == 1){
        SQzhuanyunListOrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        [cell setdataInTwo:_model andindexRow:indexPath.row];
        
        return cell;
        
    }
    else if (indexPath.section == 2) {
        NSString *identf;
        identf = @"SQzhuanyunListCell";
        
        SQzhuanyunListCell *cell = [tableView dequeueReusableCellWithIdentifier:identf];
        SQzhuanyunListGoodsModel *good = _model.goods[indexPath.row];
        
        [cell setDataInListOrderDetail:good];
        
        return cell;
        
    }
    else if (indexPath.section == 3) {
        NSString *identf;
        identf = @"SQZhuanyunQuoteVCCell";
        SQZhuanyunQuoteVCCell *cell = [tableView dequeueReusableCellWithIdentifier:identf];
        if (nil == cell) {
            cell = [[SQZhuanyunQuoteVCCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identf];
        }
        [cell configInOrderDetail:_model andIndexRow:indexPath.row];

        [cell.rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(cell.contentView).offset(-5);
            make.centerY.equalTo(cell.contentView).offset(0);
        }];
        return cell;
        
    }
    return nil;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (section == 2) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 20)];
        view.backgroundColor = [UIColor colorWithHexString:bg_gray];
        
        
        UILabel *label= [[UILabel alloc] init];
        label.text = @"     商品清单";
        label.font = [UIFont systemFontOfSize:15];
        label.textColor = [UIColor colorWithHexString:text_color_gray_black];
        label.backgroundColor = [UIColor whiteColor];
        [view addSubview:label];
        [label sizeToFit];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.top.equalTo(view).offset(15);
            make.height.equalTo(@30);
            make.right.equalTo(view);
        }];
        
        UIView *view2 = [[UIView alloc]init];
        [view addSubview:view2];
        view2.alpha = 0.6;
        view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
        [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(view);
            make.height.mas_equalTo(0.7);
            make.right.mas_equalTo(0);
            make.left.mas_equalTo(15);
        }];
        
        
        return view;
        
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 20)];
    view.backgroundColor = [UIColor colorWithHexString:bg_gray];
    return view;
    
    
    return view;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (section == 2) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 20)];
        view.backgroundColor = [UIColor whiteColor];
        
        UILabel *label= [[UILabel alloc] init];
        
        label.text = [NSString stringWithFormat:@"共 %lu 件商品",(unsigned long)_model.goods.count];
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor colorWithHexString:text_color_gray_black];
        [view addSubview:label];
        [label sizeToFit];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-5);
            make.centerY.equalTo(view);
        }];
        
        
        UIView *view2 = [[UIView alloc]init];
        [view addSubview:view2];
        view2.alpha = 0.6;
        view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
        [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view);
            make.height.mas_equalTo(0.7);
            make.right.mas_equalTo(0);
            make.left.mas_equalTo(15);
        }];
        
        
        return view;
        
    }
    return nil;
}// custom view for footer. will be adjusted to default or specified footer height



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 45;
    }
    return 15;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return 30;
    }
    return 0;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < 2) {
        return UITableViewAutomaticDimension;
    }else if (indexPath.section == 2){
        return UITableViewAutomaticDimension;
    }
    return 44;
}
#pragma mark - ---- getters and setters 🍋🌞
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

@end
