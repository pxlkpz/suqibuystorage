//
//  SQzhuanyunListOrderDetails.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"
#import "SQzhuanyunLIstItemsModel.h"
@interface SQzhuanyunListOrderDetails : SQBaseViewController

@property (nonatomic, strong) NSDictionary*modelDic;

@property (nonatomic, strong) SQzhuanyunLIstItemsModel*model;

@end
