//
//  SQzhuanyunPayMoneyCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunPayMoneyCell.h"
#import "SQzhuanyunLIstItemsModel.h"
#import "SQWoChongzhiVC.h"
#import <IQKeyboardManager/IQUIView+Hierarchy.h>
#import "SQdaishouListItemsModel.h"

@implementation SQzhuanyunPayMoneyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.rightButton.layer.cornerRadius = allCornerRadius;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setdata:(id)dataModel andindexRow:(NSInteger)row{
    if ([dataModel isKindOfClass:[SQzhuanyunLIstItemsModel class]]) {
        SQzhuanyunLIstItemsModel *data1 = dataModel;
        self.rightMoneyLabel.text = [data1.user_remain_cash gtm_stringByUnescapingFromHTML];
        if ([data1.need_incharge boolValue]) {
            self.rightButton.hidden = NO;
        }else{
            self.rightButton.hidden = YES;
        }
    }else if ([dataModel isKindOfClass:[SQdaishouListItemsModel class]]){
        SQdaishouListItemsModel *data1 = dataModel;
        self.rightMoneyLabel.text = [data1.user_remain_cash gtm_stringByUnescapingFromHTML];
        if ([data1.need_incharge boolValue]) {
            self.rightButton.hidden = NO;
        }else{
            self.rightButton.hidden = YES;
        }

    }
    
    
}




- (IBAction)rightButtonAction:(UIButton *)sender {
    SQWoChongzhiVC*VC = [[SQWoChongzhiVC alloc] init];
    [self.viewController.navigationController pushViewController:VC animated:YES];
}
@end
