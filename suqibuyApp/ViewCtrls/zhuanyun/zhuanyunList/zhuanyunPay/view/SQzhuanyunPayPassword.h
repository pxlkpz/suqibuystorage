//
//  SQzhuanyunPayPassword.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQzhuanyunPayPassword : UITableViewCell
-(void)setdata:(id)dataModel andindexRow:(NSInteger)row;
@property (weak, nonatomic) IBOutlet UIButton *sureButton;
- (IBAction)sureButtonAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *textField;

- (BOOL)textFieldShouldReturn:(UITextField *)_textField;
- (IBAction)textFiledReturnEditing:(id)sender;

@end
