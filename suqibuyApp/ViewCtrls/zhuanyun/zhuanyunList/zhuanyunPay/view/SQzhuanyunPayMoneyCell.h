//
//  SQzhuanyunPayMoneyCell.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQzhuanyunPayMoneyCell : UITableViewCell
-(void)setdata:(id)dataModel andindexRow:(NSInteger)row;
@property (weak, nonatomic) IBOutlet UILabel *rightBottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightMoneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
- (IBAction)rightButtonAction:(UIButton *)sender;

@end
