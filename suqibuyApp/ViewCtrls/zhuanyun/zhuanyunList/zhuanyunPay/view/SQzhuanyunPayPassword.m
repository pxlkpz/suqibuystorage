//
//  SQzhuanyunPayPassword.m
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunPayPassword.h"
#import "SQzhuanyunLIstItemsModel.h"

@implementation SQzhuanyunPayPassword

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //    UIButton *sureButton = [[UIButton alloc] init];
    //    [sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [sureButton setTitle:@"退出当前账号" forState:UIControlStateNormal];
    //    sureButton.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    //    [sureButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    //    sureButton.layer.cornerRadius = allCornerRadius;
    //    [sureButton addTarget:self action:@selector(sureButtonAction) forControlEvents:UIControlEventTouchUpInside];
    //    [bottomView addSubview:sureButton];
    //    [sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.bottom.equalTo(bottomView).offset(-5);
    //        make.right.equalTo(bottomView.mas_right).offset(-20);
    //        make.left.equalTo(@20);
    //        make.height.equalTo(@40);
    //    }];
    
    self.sureButton.layer.cornerRadius = allCornerRadius;
    self.textField.secureTextEntry = YES;
    
    //    [self.textField becomeFirstResponder];
    
    self.textField.returnKeyType = UIReturnKeyDone;
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)_textField {
    
    [self.textField resignFirstResponder];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SQzhuanyunPayNoti" object:_textField.text];
    
    return YES;
}

-(IBAction)textFiledReturnEditing:(id)sender {
    [sender resignFirstResponder];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SQzhuanyunPayNoti" object:_textField.text];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setdata:(SQzhuanyunLIstItemsModel*)dataModel andindexRow:(NSInteger)row{
    
}
- (IBAction)sureButtonAction:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SQzhuanyunPayNoti" object:_textField.text];
}
@end
