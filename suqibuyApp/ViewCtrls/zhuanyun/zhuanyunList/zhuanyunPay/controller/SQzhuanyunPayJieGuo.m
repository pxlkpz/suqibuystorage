//
//  SQzhuanyunPayJieGuo.m
//  suqibuyApp
//
//  Created by apple on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunPayJieGuo.h"
#import "PxlTextView.h"
#import "SQzhuanyunListOrderDetails.h"

@interface SQzhuanyunPayJieGuo ()

@end

@implementation SQzhuanyunPayJieGuo


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"%@订单支付结果", _model.package_no];
    
    [self buildUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ---- event response 🍐🌛
-(void)sureButtonAction:(UIButton*)sender{
    
    //    self.navigationController.viewControllers
    SQzhuanyunListOrderDetails*vc = [[SQzhuanyunListOrderDetails alloc] init];
    vc.model = _model;
    
    [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0], self.navigationController.viewControllers[1], vc]];
}
#pragma mark - ---- private methods 🍊🌜
-(void)buildUI{
    const NSInteger num = 0;
    UILabel * label =[self createCell:20 + num andStr1:@"订单号:" andStr2:_model.package_no];
    UIView *view2 = [[UIView alloc]init];
    [self.view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label.mas_bottom).offset(7);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(-15);
        make.left.mas_equalTo(15);
    }];
    
    
    
    UILabel*label1 = [self createCell:60 + num andStr1:@"付款金额:" andStr2:[_model.paid_total gtm_stringByUnescapingFromHTML]];
    label1.textColor = [UIColor colorWithHexString:pink_color];
    
    UILabel*label2 =  [self createCell:85 + num andStr1:@"订单状态:" andStr2:_model.status_label];
    label2.textColor = [UIColor colorWithHexString:order_status_green];
    
    
    
    
    UILabel *labelBottom= [PxlTextView createLabel];
    labelBottom.text = @"我们的操作人员将尽快确认并处理您的订单!";
    labelBottom.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [self.view addSubview:labelBottom];
    [labelBottom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label2.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(13);
    }];
    
    UIButton* button = [PxlTextView createButton:@"查看订单详情以及更多操作"];
    [self.view addSubview:button];
    [button addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelBottom.mas_bottom).offset(35);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.equalTo(@40);
    }];
    
}

-(UILabel*)createCell:(NSInteger)num andStr1:(NSString*)str1 andStr2:(NSString*)str2{
    UILabel *label1= [PxlTextView createLabel];
    label1.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [self.view addSubview:label1];
    label1.textAlignment = NSTextAlignmentLeft;
    
    label1.text = str1;
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(15);
        make.top.equalTo(self.view).offset(num);
        make.size.mas_equalTo(CGSizeMake(70, 21));
    }];
    
    UILabel *label2= [PxlTextView createLabel];
    label2.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:label2];
    label2.textColor = [UIColor colorWithHexString:text_shallow_gray];
    label2.text = str2;
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label1.mas_right).offset(5);
        make.centerY.equalTo(label1);
        make.right.equalTo(self.view).offset(10);
        //        make.size.mas_equalTo(CGSizeMake(70, 21));
    }];
    
    
    
    
    return label2;
    
}
#pragma mark - ---- delegate 🍎🌝

#pragma mark - ---- getters and setters 🍋🌞


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
