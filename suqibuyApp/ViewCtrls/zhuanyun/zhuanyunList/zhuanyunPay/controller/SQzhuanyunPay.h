//
//  SQzhuanyunPay.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"
#import "SQzhuanyunLIstItemsModel.h"

@interface SQzhuanyunPay : SQBaseViewController
@property (nonatomic, strong) SQzhuanyunLIstItemsModel* model;
@property (nonatomic, strong) NSDictionary* dataDic;

@end
