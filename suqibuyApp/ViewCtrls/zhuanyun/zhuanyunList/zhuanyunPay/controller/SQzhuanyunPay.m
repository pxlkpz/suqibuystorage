//
//  SQzhuanyunPay.m
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunPay.h"
#import "SQZhuanyunQuoteVCCell.h"
#import "SQzhuanyunListOrderDetailCell.h"
#import "SQzhuanyunPayMoneyCell.h"
#import "SQzhuanyunPayPassword.h"
#import "SQzhuanyunPayJieGuo.h"
@interface SQzhuanyunPay ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQzhuanyunPay


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.model) {
        self.model = [SQzhuanyunLIstItemsModel mj_objectWithKeyValues:self.dataDic];
    }
    self.title = [NSString stringWithFormat:@"%@发货订单支付",_model.package_no];
    [self buildTableView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationAction:) name:@"SQzhuanyunPayNoti" object:nil];
    
    [self requestGetOrderInfo:self.model.package_no];

    
    
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



#pragma mark - ---- event response 🍐🌛
-(void)notificationAction:(NSNotification*)noti{
    NSString* password = [noti object];
    if ([password isEqualToString:@""] || !password) {
        showDefaultPromptTextHUD(@"请输入您的帐号登录密码");
        return;
    }
    [self requestDataPassword:password andorderNo:_model.package_no];
}
#pragma mark - ---- private methods 🍊🌜

-(void) requestGetOrderInfo:(NSString*)order_no
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"order_no":order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_transship_detail WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _model = [SQzhuanyunLIstItemsModel mj_objectWithKeyValues:result];
                    [_listTableView reloadData];

                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        [_listTableView.mj_header endRefreshing];
    } WithErrorBlock:^(id errorCode) {
        [_listTableView.mj_header endRefreshing];
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [_listTableView.mj_header endRefreshing];
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}



-(void) requestDataPassword:(NSString*)confirm_password andorderNo:(NSString*)order_no
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"confirm_password":confirm_password, @"order_no":order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_transship_paymentpost WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    SQzhuanyunLIstItemsModel *model = [SQzhuanyunLIstItemsModel mj_objectWithKeyValues:result];
                    SQzhuanyunPayJieGuo *vc = [[SQzhuanyunPayJieGuo alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    //[_listTableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    _listTableView.dataSource = self;
    _listTableView.tag = 5;
    _listTableView.delegate = self;
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 44.0f;
    
    UINib *nib = [UINib nibWithNibName:@"SQzhuanyunPayMoneyCell" bundle:nil];
    [_listTableView registerNib:nib forCellReuseIdentifier:@"SQzhuanyunPayMoneyCell"];
    
    UINib *nib2 = [UINib nibWithNibName:@"SQzhuanyunListOrderDetailCell" bundle:nil];
    [_listTableView registerNib:nib2 forCellReuseIdentifier:@"SQzhuanyunListOrderDetailCell"];
    
    UINib *nib3 = [UINib nibWithNibName:@"SQzhuanyunPayPassword" bundle:nil];
    [_listTableView registerNib:nib3 forCellReuseIdentifier:@"SQzhuanyunPayPassword"];
    
    
    [_listTableView registerClass:[SQZhuanyunQuoteVCCell class] forCellReuseIdentifier:@"SQZhuanyunQuoteVCCell"];
    
    
    
    [_listTableView setTableFooterView:[[UIView alloc] init]];
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}

#pragma mark - ---- delegate 🍎🌝
#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 2;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 8;
    }else if (section == 1){
        return 6;
    }
    return 0;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        NSString *identf;
        identf = @"SQZhuanyunQuoteVCCell";
        SQZhuanyunQuoteVCCell *cell = [tableView dequeueReusableCellWithIdentifier:identf];
        if (nil == cell) {
            cell = [[SQZhuanyunQuoteVCCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identf];
        }
        
        [cell configInOrderDetail:_model andIndexRow:indexPath.row];
        
        return cell;
    }else if (indexPath.section == 1){
        if (indexPath.row<4) {
            NSString *identifier = @"SQzhuanyunListOrderDetailCell";
            SQzhuanyunListOrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            [cell setdataInzhuanyunPay:_model andindexRow:indexPath.row];
            
            return cell;
        }else if (indexPath.row == 4){
            NSString *identifier = @"SQzhuanyunPayMoneyCell";
            SQzhuanyunPayMoneyCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            //            [cell setdataInTwo:_model andindexRow:indexPath.row];
            [cell setdata:_model andindexRow:indexPath.row];
            return cell;
            
        }else if (indexPath.row == 5){
            NSString *identifier = @"SQzhuanyunPayPassword";
            SQzhuanyunPayPassword *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            //            [cell setdataInTwo:_model andindexRow:indexPath.row];
            [cell setdata:_model andindexRow:indexPath.row];
            return cell;
            
        }
    }
    return nil;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (section == 2) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 20)];
        view.backgroundColor = [UIColor colorWithHexString:bg_gray];
        
        
        UILabel *label= [[UILabel alloc] init];
        label.text = @"     商品清单";
        label.font = [UIFont systemFontOfSize:15];
        label.textColor = [UIColor colorWithHexString:gray];
        label.backgroundColor = [UIColor whiteColor];
        [view addSubview:label];
        [label sizeToFit];
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(0);
            make.top.equalTo(view).offset(15);
            make.height.equalTo(@30);
            make.right.equalTo(view);
            //            make.height.equalTo(@30);
        }];
        
        return view;
        
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 20)];
    view.backgroundColor = [UIColor colorWithHexString:bg_gray];
    return view;
    
    
    return view;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 45;
    }
    return 15;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 44;
        
    }else if (indexPath.section == 1 && indexPath.row < 4){
        
        return UITableViewAutomaticDimension;
    }else if (indexPath.row == 5){
        return 160;
    }
    
    return UITableViewAutomaticDimension;
    
    
    return 44;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

#pragma mark - ---- getters and setters 🍋🌞



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
