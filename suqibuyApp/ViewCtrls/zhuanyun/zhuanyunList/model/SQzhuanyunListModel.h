//
//  SQzhuanyunListModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQzhuanyunLIstItemsModel.h"
@interface SQzhuanyunListModel : NSObject
//"total_pages": 2,
//"page": "1",
//"page_num": 10,
//"total_results": 20,
//"current_page": 1,
//"items": [
@property (nonatomic, copy) NSString* total_pages;
@property (nonatomic, copy) NSString* page;

@property (nonatomic, copy) NSString* page_num;
@property (nonatomic, copy) NSString* total_results;
@property (nonatomic, copy) NSString* current_page;
@property (nonatomic, strong) NSMutableArray* items;

@end
