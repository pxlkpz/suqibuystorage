//
//  SQzhuanyunListGoodsModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQzhuanyunListGoodsModel : NSObject
 
//"id": "26",
//"thumbnail": "https://t1.shtag.com/images/default/package_icon.png",
//"item_code": "234",
//"item_name": "234",
//"qty": "1",
//"weight": "1.00",
//"size": "1X1X1",
//"item_params": "242424",
//"detail": "[234]"


 @property (nonatomic, copy) NSString* thumbnail;
@property (nonatomic, copy) NSString* item_code;
@property (nonatomic, copy) NSString* item_name;
@property (nonatomic, copy) NSString* qty;
@property (nonatomic, copy) NSString* weight;
@property (nonatomic, copy) NSString* size;
@property (nonatomic, copy) NSString* item_params;
@property (nonatomic, copy) NSString* detail;
@property (nonatomic, copy) NSString* my_id;

@end


