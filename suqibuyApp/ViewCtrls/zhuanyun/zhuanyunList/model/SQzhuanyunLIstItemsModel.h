//
//  SQzhuanyunLIstItemsModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQzhuanyunListGoodsModel.h"
@interface SQzhuanyunLIstItemsModel : NSObject
/*
 {
 {
 "id": "21",
 "package_no": "TO10000028",
 "shipping_address": "12312 312323 213132 123123123 America 美国",
 "shipping_method": "EMS (约5-8工作日，时效不稳定)",
 "weight": "0.03kg",
 "total_qty": "13",
 "status": "daifukuan",
 "status_label": "待付款",
 "customer_note": "",
 "sub_total": "¥173.00",
 "agency_fee": "¥8.65",
 "insurance_amount": "4500.00",
 "insurance_fee": "¥90.00",
 "usergroup_discount_amount": "- ¥0.02",
 "coupon_code": "",
 "coupon_discount_amount": "¥0.00",
 "credits_point_total": "10499",
 "credits_amount": "- ¥104.99",
 "grand_total": "¥166.64",
 "created_at": "2016-06-11",
 "can_payment": true,
 "can_cancel": true,
 "waiting_payment_total": "¥166.64",
 "need_incharge": true,
 "paid_total": "¥0.00",
 "paid_at": null,
 "shipping_no": null,
 "shipping_at": null,
 "transfer_no": null,
 "complete_at": null,
 "user_remain_cash": "¥29.02",
 "goods": [
 
 */


/*
 "agency_fee" = "&yen;44.28";
 "can_cancel" = 1;
 "can_payment" = 1;
 "complete_at" = "<null>";
 "coupon_code" = "";
 "coupon_discount_amount" = "&yen;0.00";
 "created_at" = "2016-06-19";
 "credits_amount" = "&yen;0.00";
 "credits_point_total" = 0;
 "customer_note" = "";
 goods =         (
 {
 }
 );
 "grand_total" = "&yen;929.69";
 id = 40;
 "insurance_amount" = "0.00";
 "insurance_fee" = "&yen;0.00";
 "need_incharge" = 0;
 "package_no" = TO10000047;
 "paid_at" = "<null>";
 "paid_total" = "&yen;0.00";
 "shipping_address" = "We We Weww Www   America \U7f8e\U56fd";
 "shipping_at" = "<null>";
 "shipping_method" = "EMS (\U7ea65-8\U5de5\U4f5c\U65e5\Uff0c\U65f6\U6548\U4e0d\U7a33\U5b9a)";
 "shipping_no" = "<null>";
 status = daifukuan;
 "status_label" = "\U5f85\U4ed8\U6b3e";
 "sub_total" = "&yen;885.50";
 "total_qty" = 2;
 "transfer_no" = "<null>";
 "user_remain_cash" = "&yen;94,256.39";
 "usergroup_discount_amount" = "- &yen;0.09";
 "waiting_payment_total" = "&yen;929.69";
 weight = 10kg;

 */

@property (nonatomic, copy) NSString* my_id;
@property (nonatomic, copy) NSString* package_no;
@property (nonatomic, copy) NSString* shipping_address;
@property (nonatomic, copy) NSString* shipping_method;
@property (nonatomic, copy) NSString* weight;
@property (nonatomic, copy) NSString* total_qty;
@property (nonatomic, copy) NSString* status;
@property (nonatomic, copy) NSString* status_label;
@property (nonatomic, copy) NSString* customer_note;
@property (nonatomic, copy) NSString* sub_total;
@property (nonatomic, copy) NSString* agency_fee;
@property (nonatomic, copy) NSString* insurance_amount;
@property (nonatomic, copy) NSString* insurance_fee;
@property (nonatomic, copy) NSString* usergroup_discount_amount;
@property (nonatomic, copy) NSString* coupon_code;
@property (nonatomic, copy) NSString* coupon_discount_amount;
@property (nonatomic, copy) NSString* credits_point_total;
@property (nonatomic, copy) NSString* credits_amount;
@property (nonatomic, copy) NSString* created_at;
@property (nonatomic, copy) NSString* grand_total;
@property (nonatomic, copy) NSString* can_payment;
@property (nonatomic, copy) NSString* can_cancel;
@property (nonatomic, copy) NSString* waiting_payment_total;
@property (nonatomic, copy) NSString* need_incharge;
@property (nonatomic, copy) NSString* paid_total;
@property (nonatomic, copy) NSString* paid_at;
@property (nonatomic, copy) NSString* shipping_no;
@property (nonatomic, copy) NSString* shipping_at;
@property (nonatomic, copy) NSString* transfer_no;
@property (nonatomic, copy) NSString* complete_at;
@property (nonatomic, copy) NSString* user_remain_cash;
@property (nonatomic, copy) NSArray* goods;
@property (nonatomic, copy) NSString* size_total;

@property (nonatomic, copy) NSString* apply_value;
@property (nonatomic, copy) NSString* warehouse_fee;


@end
