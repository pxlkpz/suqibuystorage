//
//  SQzhuanyunLIstItemsModel.m
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunLIstItemsModel.h"

@implementation SQzhuanyunLIstItemsModel
+ (NSDictionary *)objectClassInArray{
    return @{
             @"goods" : @"SQzhuanyunListGoodsModel",
             };
}

+(NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"id":@"my_id"};
}

@end
