//
//  SQzhuanyunListCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/18.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunListCell.h"
#import "SQzhuanyunListGoodsModel.h"
@implementation SQzhuanyunListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width - 96; // 44 = avatar宽度，4 * 3为padding
    
    self.leftBottomLabel.preferredMaxLayoutWidth = preferredMaxWidth; // 多行时必须设置
    
    [self.leftBottomLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(void)setData:(SQzhuanyunListGoodsModel*)modeldata{
    NSURL *url = [NSURL URLWithString:modeldata.thumbnail];
    [self.headImageView sd_setImageWithURL:url placeholderImage:nil];
    
    self.leftTopLabel.text = modeldata.item_name;
    
    self.rightLabel.text = [NSString stringWithFormat:@"x%@",modeldata.qty];
    //    cell.itemPriceLabel.textColor = [UIColor colorWithHexString:text_shallow_gray];
    
    self.leftBottomLabel.text = @"";
    
    self.leftBottomLabel.text = modeldata.item_params;
    
}

-(void)setDataInListOrderDetail:(SQzhuanyunListGoodsModel*)modeldata{
    NSURL *url = [NSURL URLWithString:modeldata.thumbnail];
    [self.headImageView sd_setImageWithURL:url placeholderImage:nil];
    
    self.leftTopLabel.text = modeldata.item_name;
    
    self.rightLabel.text = [NSString stringWithFormat:@"x%@",modeldata.qty];
    
    self.leftBottomLabel.text = @"";
    
    self.leftBottomLabel.text = modeldata.item_params;
    
    self.rightLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    self.leftBottomLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    
}
@end
