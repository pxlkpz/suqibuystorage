//
//  SQzhuanyunListCell.h
//  suqibuyApp
//
//  Created by apple on 16/6/18.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQzhuanyunListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *leftTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftBottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
-(void)setData:(id)modeldata;

-(void)setDataInListOrderDetail:(id)modeldata;

@end
