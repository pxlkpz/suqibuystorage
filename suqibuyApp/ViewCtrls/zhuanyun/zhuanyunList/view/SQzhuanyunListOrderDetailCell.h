//
//  SQzhuanyunListOrderDetailCell.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQzhuanyunListOrderDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
- (IBAction)rightButtonAcition:(id)sender;

-(void)setdata:(id)dataModel andindexRow:(NSInteger)row;
-(void)setdataInTwo:(id)dataModel andindexRow:(NSInteger)row;
-(void)setdataInzhuanyunPay:(id)dataModel andindexRow:(NSInteger)row;
-(void)setdataInzhuanyunADPay:(id)dataModel andindexRow:(NSInteger)row;
@end
