//
//  SQzhuanyunListOrderDetailCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunListOrderDetailCell.h"
#import "SQzhuanyunLIstItemsModel.h"
#import "CommonWebViewController.h"
#import "SQdaishouListItemsModel.h"
#import <IQKeyboardManager/IQUIView+Hierarchy.h>
@implementation SQzhuanyunListOrderDetailCell
{
    SQzhuanyunLIstItemsModel* _listItemsdata;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.rightButton.layer.cornerRadius = allCornerRadius;
    self.selectionStyle  = UITableViewCellSelectionStyleNone;
}

-(void)setdataInzhuanyunADPay:(SQdaishouListItemsModel*)dataModel andindexRow:(NSInteger)row{
    switch (row) {
        case 0:
        {
            self.leftLabel.text = @"快递公司:";
            self.rightLabel.text = dataModel.company_name;
        }
            break;
        case 1:
        {
            self.leftLabel.text = @"快递单号:";
            self.rightLabel.text = dataModel.package_express_no;
        }
            break;

        case 2:
        {
            self.leftLabel.text = @"状态:";
            self.rightLabel.text = dataModel.status_label;
            
            self.rightLabel.textColor = [self orderStatusBackground:dataModel.status];
            
        }
            break;
        case 3:
        {
            self.leftLabel.text = @"已经支付:";
            self.rightLabel.text = [dataModel.paid_total gtm_stringByUnescapingFromHTML];
        }
            break;
        case 4:
        {//等支付
            self.leftLabel.text = @"需补款:";
            self.rightLabel.text = [dataModel.waiting_payment_total gtm_stringByUnescapingFromHTML];
            self.rightLabel.textColor = [UIColor colorWithHexString:text_yellow];
        }
            break;
            
        default:
            break;
    }

}

-(void)setdataInzhuanyunPay:(SQzhuanyunLIstItemsModel*)dataModel andindexRow:(NSInteger)row{
    switch (row) {
        case 0:
        {
            self.leftLabel.text = @"订单号:";
            self.rightLabel.text = dataModel.package_no;
        }
            break;
        case 1:
        {
            self.leftLabel.text = @"订单状态:";
            self.rightLabel.text = dataModel.status_label;
            
            self.rightLabel.textColor = [self orderStatusBackground:dataModel.status];
            
        }
            break;
        case 2:
        {
            self.leftLabel.text = @"已经支付:";
            self.rightLabel.text = [dataModel.paid_total gtm_stringByUnescapingFromHTML];
        }
            break;
        case 3:
        {//等支付
            self.leftLabel.text = @"需补款:";
            self.rightLabel.text = [dataModel.waiting_payment_total gtm_stringByUnescapingFromHTML];
            self.rightLabel.textColor = [UIColor colorWithHexString:text_yellow];
        }
            break;
            
        default:
            break;
    }
}

-(void)setdataInTwo:(SQzhuanyunLIstItemsModel*)dataModel andindexRow:(NSInteger)row{
    NSString* string;
    switch (row) {
        case 0://订单号
        {
            self.leftLabel.text = @"运单号:";
            self.rightLabel.text = dataModel.shipping_no;
            _listItemsdata = dataModel;
            string = dataModel.shipping_no;
            if (![dataModel.shipping_no isEqualToString:@""] && dataModel.shipping_no) {
                self.rightButton.hidden = NO;
                self.rightButton.tag = 10;
            }else{
                self.rightButton.hidden = YES;
            }
            
        }
            break;
        case 1:
        {
            self.leftLabel.text = @"转单号:";
            self.rightLabel.text = dataModel.transfer_no;
            string = dataModel.transfer_no;
            
        }
            break;
        case 2:
        {
            self.leftLabel.text = @"转运日期:";
            self.rightLabel.text = dataModel.shipping_at;
            string = dataModel.shipping_at;
            
        }
            break;
        case 3:
        {
            self.leftLabel.text = @"完成日期:";
            self.rightLabel.text = dataModel.complete_at;
            string = dataModel.complete_at;
            
        }
            break;
            
        default:
            break;
    }
    if ([string isEqualToString:@""] || !string) {
        self.rightLabel.text = @"  ";
    }
    
}

/**
 *  订单号
 *
 *  @param sender
 */
- (IBAction)rightButtonAcition:(UIButton*)sender {
    if (sender.tag == 10) {
        NSString *url = [NSString stringWithFormat:@"%@tracking/transorder?order_no=%@",API_MAIN_URL_HTML,_listItemsdata.package_no];
        
        CommonWebViewController *vc = [[CommonWebViewController alloc] initWithUrlStr:url andNavTitle:@"发货订单追踪查询"];
        
        [self.viewController.navigationController pushViewController:vc animated:YES];
    }
    
}

-(void)setdata:(SQzhuanyunLIstItemsModel*)dataModel andindexRow:(NSInteger)row{
    switch (row) {
        case 0:
        {
            self.leftLabel.text = @"订单状态:";
            self.rightLabel.text = dataModel.status_label;
            
            self.rightLabel.textColor = [self orderStatusBackground:dataModel.status];
            
            
        }
            break;
        case 1:
        {
            self.leftLabel.text = @"订单号:";
            self.rightLabel.text = dataModel.package_no;
        }
            break;
        case 2:
        {
            self.leftLabel.text = @"转运地址:";
            self.rightLabel.text = dataModel.shipping_address;
        }
            break;
        case 3:
        {
            self.leftLabel.text = @"转运方式:";
            self.rightLabel.text = dataModel.shipping_method;
        }
            break;
        case 4:
        {
            self.leftLabel.text = @"订单总金额:";
            self.rightLabel.text = [dataModel.waiting_payment_total gtm_stringByUnescapingFromHTML];
        }
            break;
            
        case 5:
        {
            self.leftLabel.text = @"重量:";
            self.rightLabel.text = dataModel.weight;
        }
            break;

            
        case 6:
        {
            self.leftLabel.text = @"体积重量:";
            self.rightLabel.text = dataModel.size_total;
        }
            break;

            
        case 7:
        {
            self.leftLabel.text = @"下单时间:";
            self.rightLabel.text = dataModel.created_at;
        }
            break;
        case 8:
        {
            self.leftLabel.text = @"申报价值:";
            self.rightLabel.text = dataModel.apply_value;
            if ([dataModel.apply_value isEqualToString:@""] || !dataModel.customer_note) {
                self.rightLabel.text = @"  ";
            }

        }
            break;
        case 9:
        {
            self.leftLabel.text = @"订单备注:";
            self.rightLabel.text = dataModel.customer_note;
            if ([dataModel.customer_note isEqualToString:@""] || !dataModel.customer_note) {
                self.rightLabel.text = @"  ";
            }
        }
            break;
        case 10:
        {
            self.leftLabel.text = @"已付款:";
            self.rightLabel.text = [dataModel.paid_total gtm_stringByUnescapingFromHTML];
            self.rightLabel.textColor = [UIColor colorWithHexString:text_blue];
        }
            break;
        case 11:
        {
            self.leftLabel.text = @"付款日期:";
            self.rightLabel.text = dataModel.paid_at;
            if ([dataModel.paid_at isEqualToString:@""] || !dataModel.paid_at) {
                self.rightLabel.text = @"  ";
            }
            
        }
            break;
            
        default:
            break;
    }
}

-(UIColor*)orderStatusBackground:(NSString*)status{
    
    
    UIColor *color = [UIColor blackColor];
    if ([status isEqualToString:@"pendding"]) {
        color = [UIColor colorWithHexString:@"#83d79b"];
    }
    else if ([status isEqualToString:@"daifukuan"]) {
        color = [UIColor colorWithHexString:@"#FF4081"];
        
    }
    else if ([status isEqualToString:@"waiting_payment_confirm"]
             ||[status isEqualToString:@"confirm_weightsize"]
             ||[status isEqualToString:@"recalculate_total"]
             ||[status isEqualToString:@"holded"]
             ||[status isEqualToString:@"paid_less"]
             ||[status isEqualToString:@"waiting_paid"]
             ||[status isEqualToString:@"waiting_paid_confirmed"]
             ||[status isEqualToString:@"paid_confirmed"]
             ||[status isEqualToString:@"shipping"]
             ) {
        color = [UIColor colorWithHexString:@"#008000"];
        //        color = KColorFromRGB(0xe49a72);
    }
    else if ([status isEqualToString:@"complete"]) {
        //        color = KColorFromRGB(0xe49a72);
        color = [UIColor colorWithHexString:@"#83d79b"];
        
        
    }
    else if ([status isEqualToString:@"canceled"]) {
        color = [UIColor colorWithHexString:@"#d9dada"];
        
        
        
    }
    
    return color;
}

@end
