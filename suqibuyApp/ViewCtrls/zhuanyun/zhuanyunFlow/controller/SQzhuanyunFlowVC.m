//
//  SQzhuanyunFlowVC.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunFlowVC.h"
#import "SQzhuanyunAddNewVC.h"
@interface SQzhuanyunFlowVC ()

@end

@implementation SQzhuanyunFlowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发货流程";
    
    [self buildui];
}
#pragma mark - ---- event response 🍐🌛
-(void)buttonAction{
    SQzhuanyunAddNewVC *controller = [[SQzhuanyunAddNewVC alloc] init];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0], controller]];
}
#pragma mark - ---- private methods 🍊🌜


-(void)buildui{
    
    UIView* bgview = [UIView new];

    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"fahuo_steps"];
    CGSize size1 = imageView.image.size;
    CGFloat r = (screen_width/size1.width);
    bgview.frame = CGRectMake(0, 0, KScreenWidth, r*size1.height +100);

    
    [bgview addSubview:imageView];
   
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bgview);
        make.top.equalTo(bgview).offset(20);
        
        make.size.mas_equalTo(CGSizeMake(screen_width, r*size1.height ));
    }];
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor colorWithHexString:text_yellow] forState:UIControlStateNormal];
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor colorWithHexString:text_yellow].CGColor;
    [button setTitle:@"立刻发货" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor whiteColor]];
    
    button.layer.cornerRadius = allCornerRadius;
    
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [bgview addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_bottom).offset(20);
        make.right.equalTo(bgview.mas_right).offset(-20);
        make.left.equalTo(@20);
        make.height.equalTo(@40);
    }];
    
    
    UITableView *tabel1 = [[UITableView alloc] init];
    [self.view addSubview:tabel1];
    [tabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.view);
    }];
    
    [tabel1 setTableHeaderView:bgview];}



-(void)buildUI{
    
    NSInteger Topinter = 0;
    
    if (IPHONE_5)
    {
        Topinter = 20;
    }else if(IPHONE3_5INCH){
        Topinter = 40;
    }
    
    
    
    UILabel *label1 = [self creatLabel:@"在可发货物品中选择本次想要发货的物品"];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@50);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.left.equalTo(@20);
        make.height.equalTo(@50);
    }];
    
    
    UILabel *label2 = [self creatLabel:@"填写转运订单，创建或者创建地址，使用积分、优惠券"];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label1.mas_bottom).offset(60-Topinter);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.left.equalTo(@20);
        make.height.equalTo(@50);
    }];
    
    UILabel *label3 = [self creatLabel:@"保存转动订单，如需支付，可使用余额支付，如果余额不够，可先充值后再完成支付。"];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label2.mas_bottom).offset(60-Topinter);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.left.equalTo(@20);
        make.height.equalTo(@50);
    }];
    
    UILabel *label4 = [self creatLabel:@"等待审核并确认重量、尺寸，以及最后的价格"];
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label3.mas_bottom).offset(60-Topinter);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.left.equalTo(@20);
        make.height.equalTo(@50);
    }];
    
    
    
    NSInteger jianju = 110 -Topinter;
    
    UIView *line1 = [[UIView alloc] init];
    line1.backgroundColor = KColorFromRGB(0xDFDEDE);
    [self.view addSubview:line1];
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.equalTo(@1);
        make.top.equalTo(@105);
        make.height.mas_equalTo(50-Topinter);
    }];
    
    UIView *line2 = [[UIView alloc] init];
    line2.backgroundColor = KColorFromRGB(0xDFDEDE);
    [self.view addSubview:line2];
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.equalTo(@1);
        make.top.mas_equalTo(jianju + 105);
        make.height.mas_equalTo(50-Topinter);
    }];
    
    UIView *line3 = [[UIView alloc] init];
    line3.backgroundColor = KColorFromRGB(0xDFDEDE);
    [self.view addSubview:line3];
    [line3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.equalTo(@1);
        make.top.mas_equalTo(jianju * 2 + 105);
        make.height.mas_equalTo(50-Topinter);
    }];
    
    
    
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"马上开启您的发货之旅吧！" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label4.mas_bottom).offset(50-Topinter);
        make.right.equalTo(self.view.mas_right).offset(-20);
        make.left.equalTo(@20);
        make.height.equalTo(@40);
    }];
    
}

-(__kindof UILabel*) creatLabel:(NSString*)str{
    UILabel *label1 = [[UILabel alloc] init];
    label1.backgroundColor = KColorFromRGB(0xF7F7F7);
    label1.layer.borderColor = [UIColor colorWithHexString:border_color].CGColor;
    label1.numberOfLines = 0;
    label1.textColor = KColorFromRGB(0x717171);
    label1.text = str;
    label1.layer.borderWidth = 1;
    label1.textAlignment = NSTextAlignmentCenter;
    label1.layer.cornerRadius = allCornerRadius;
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [self.view addSubview:label1];
    return label1;
}




#pragma mark - ---- delegate 🍎🌝

#pragma mark - ---- getters and setters 🍋🌞



@end
