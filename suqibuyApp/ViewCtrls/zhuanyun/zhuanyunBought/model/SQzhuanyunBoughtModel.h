//
//  SQzhuanyunBoughtModel.h
//  suqibuyApp
//
//  Created by apple on 16/5/30.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQzhuanyunBoughtModel : NSObject

//"cat_name" = "\U56db\U7c7b\U7269\U54c1";
//"express_no" = ES9234242;
//"expresss_company_name" = "\U4e2d\U901a\U5feb\U9012";
//id = 40;
//"is_added" = 0;
//"item_description" = "\U7535\U8111\Uff0c\U6709\U5305\U88c5\U989c\U8272 \Uff1a\U94f6; \U5c3a\U5bf8 \Uff1a15\U5bf8; \U6570\U91cf \Uff1a2";
//"item_name" = "\U82f9\U679c\U7b14\U8bb0\U672c\U7535\U8111\U3010\U56db\U7c7b\U7269\U54c1\U3011";
//qty = 1;
//size = 1X1X1;
//weight = "11.00";

@property (nonatomic, copy) NSString* daishou_package_id;
/** 物品类型 */
@property (nonatomic, copy) NSString* cat_name;
/** 快递号 */
@property (nonatomic, copy) NSString* express_no;
/** 快递名称 */
@property (nonatomic, copy) NSString* expresss_company_name;
/** Id */
@property (nonatomic, copy) NSString* my_id;
/** 是否添加 */
@property (nonatomic, copy) NSString* is_added;
/** 描述 */
@property (nonatomic, copy) NSString* item_description;
/** 商品名字 */
@property (nonatomic, copy) NSString* item_name;
/** 数量 */
@property (nonatomic, copy) NSString* qty;
/** 大小 */
@property (nonatomic, copy) NSString* size;
/** 重量 */
@property (nonatomic, copy) NSString* weight;

@end
