//
//  SQzhuanyunBoughtCell.h
//  suqibuyApp
//
//  Created by apple on 16/5/30.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SQzhuanyunBoughtModel;
@interface SQzhuanyunBoughtCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImagView;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UIButton *clickButton;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel2;
-(void)setData:(SQzhuanyunBoughtModel*)model;

- (IBAction)addCatButtonAction:(UIButton *)sender;

@end
