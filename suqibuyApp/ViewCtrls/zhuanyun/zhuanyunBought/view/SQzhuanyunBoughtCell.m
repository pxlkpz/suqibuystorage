//
//  SQzhuanyunBoughtCell.m
//  suqibuyApp
//
//  Created by apple on 16/5/30.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunBoughtCell.h"
#import "SQzhuanyunBoughtModel.h"

@implementation SQzhuanyunBoughtCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    self.clickButton.layer.cornerRadius = allCornerRadius;
    self.clickButton.titleLabel.font = [UIFont systemFontOfSize: 14.0];
    
    
    // Configure the view for the selected state
}

-(void)setData:(SQzhuanyunBoughtModel*)model{
    self.topLabel.text = [NSString stringWithFormat:@"%@", model.item_name];
    self.bottomLabel.text = [NSString stringWithFormat:@"商品描述: %@",model.item_description];
    self.clickButton.tag = [model.my_id integerValue];
    self.bottomLabel2.text = [NSString stringWithFormat:@"重量: %@ g",model.weight];
    if ([model.is_added isEqualToString:@"1"]) {
        [self.clickButton setBackgroundColor:[UIColor colorWithHexString:gray]];
        self.clickButton.enabled = NO;
        [self.clickButton setTitle:@"已添加" forState:UIControlStateNormal];
    }else{
        [self.clickButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
        self.clickButton.enabled = YES;
        [self.clickButton setTitle:@"未添加" forState:UIControlStateNormal];
    }
}

- (IBAction)addCatButtonAction:(UIButton *)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"cellButtonNoti" object:sender];
}

//@property (weak, nonatomic) IBOutlet UIImageView *headImagView;
//@property (weak, nonatomic) IBOutlet UILabel *topLabel;
//@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
//@property (weak, nonatomic) IBOutlet UIButton *clickButton;


@end
