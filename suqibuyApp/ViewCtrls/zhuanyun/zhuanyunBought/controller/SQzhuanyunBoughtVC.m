//
//  SQzhuanyunBoughtVC.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunBoughtVC.h"
#import "SQzhuanyunBoughtCell.h"
#import "SQzhuanyunBoughtModel.h"
@interface SQzhuanyunBoughtVC ()<UITableViewDataSource,UITableViewDelegate>
{
    
    
    NSMutableArray *dataArray;
    UILabel *_headLabel;
    
    NSString *_last_daishou_package_id;
    UIButton *_addAllButton;
    
    UIView *_navigationBgview;
}

@property(nonatomic, strong) UITableView *listTableView;
@property(nonatomic, assign) NSInteger currentPage;

@end

@implementation SQzhuanyunBoughtVC
//@synthesize _listTableView;


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self buildclearView];
    
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    _listTableView.hidden = YES;
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(35);
    }];
    [_listTableView setTableFooterView:[ [UIView alloc]init]];
    
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    
    _listTableView.estimatedRowHeight = 80.0f;
    
    
    @weakify(self);
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        self.currentPage = 1;
        [self requestData:1];
    }];
    
    _listTableView.mj_footer  = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:self.currentPage+1];
    }];
    
    dataArray = [[NSMutableArray alloc] init];
    
    self.currentPage = 1;
    [self requestData:1];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cellNoti:) name:@"cellButtonNoti" object:nil];
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [_navigationBgview removeFromSuperview];
    
    
    NSLog(@"修改前");
    [self printViewHierarchy:self.navigationController.navigationBar];
    
    //修改NavigaionBar的高度
    self.navigationController.navigationBar.frame = CGRectMake(0, 20, KScreenWidth, 44);
    
    NSLog(@"\n修改后");
    [self printViewHierarchy:self.navigationController.navigationBar];
    
    [super viewWillDisappear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSLog(@"修改前");
    [self printViewHierarchy:self.navigationController.navigationBar];
    
    //修改NavigaionBar的高度
    self.navigationController.navigationBar.frame = CGRectMake(0, 20, KScreenWidth, 79);
    
    NSLog(@"\n修改后");
    [self printViewHierarchy:self.navigationController.navigationBar];
}


-(void)viewWillAppear:(BOOL)animated{
    [self buildHeadView];
    
    [super viewWillAppear:animated];
    
}
#pragma mark - ---- event response 🍐🌛

-(void)AddAllButtonAction:(UIButton*)sender{
    [self requestAddAll];
}

-(void)cellNoti:(NSNotification*)noti{
    UIButton *btn = [noti object];
    [self requestAddOne:[NSString stringWithFormat: @"%ld", (long)btn.tag]];
    
}
#pragma mark - ---- private methods 🍊🌜

-(void)addAllDataChange{
    for (NSArray*arr in dataArray) {
        for (SQzhuanyunBoughtModel *model in arr) {
            model.is_added = @"1";
        }
    }
    [self.listTableView reloadData];
}

-(void)addOneDataChange:(NSString*)my_id{
    for (NSArray*arr in dataArray) {
        for (SQzhuanyunBoughtModel *model in arr) {
            if ([model.my_id isEqualToString:my_id]) {
                model.is_added = @"1";
                [self.listTableView reloadData];
                
                return;
            }
        }
    }
}

-(void)buildclearView{
    
    UILabel* noItemLabel = [[UILabel alloc] init];
    noItemLabel.text = @"当前列表为空！";
    noItemLabel.textColor = [UIColor colorWithHexString:text_gray];
    noItemLabel.textAlignment = NSTextAlignmentCenter;
    noItemLabel.font = [UIFont systemFontOfSize:14];
    noItemLabel.numberOfLines = 2;
    [self.view addSubview:noItemLabel];
    [noItemLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
    }];
    
}
- (void)printViewHierarchy:(UIView *)superView
{
    static uint level = 0;
    for(uint i = 0; i < level; i++){
        printf("\t");
    }
    
    const char *className = NSStringFromClass([superView class]).UTF8String;
    const char *frame = NSStringFromCGRect(superView.frame).UTF8String;
    printf("%s:%s\n", className, frame);
    
    ++level;
    for(UIView *view in superView.subviews){
        [self printViewHierarchy:view];
    }
    --level;
}

-(void)buildHeadView{
    
    
    _navigationBgview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 79)];
    _navigationBgview.backgroundColor = KColorRGB(250, 60, 6);
    [self.navigationController.navigationBar addSubview:_navigationBgview];
    
    UIView* bgView0 = [[UIView alloc] init];
    [_navigationBgview addSubview:bgView0];
    bgView0.backgroundColor = [UIColor clearColor];
    //    KColorRGB(58, 128, 132);
    [bgView0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(_navigationBgview);
        make.height.equalTo(@44);
        //        make.top.equalTo(_navigationBgview).offset(48);
    }];
    
    UIButton* backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:NAV_BACK_IMG] forState:UIControlStateNormal];
    [backButton setFrame:CGRectMake(0, 0, 40 ,45)];
    [backButton addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, -8, 0, 0)];
    [bgView0 addSubview:backButton];
    
    
    UILabel *label = [[UILabel alloc] init];
    //                      WithFrame:CGRectZero];
    //    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:18.0];
    //    label.textAlignment = NSTextAlignmentLeft;
    label.textColor=[UIColor whiteColor];
    label.shadowOffset = CGSizeMake(1, 1);
    //    self.navigationItem.titleView = label;
    label.text = @"可发货物品";
    [label sizeToFit];
    [bgView0 addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(bgView0);
    }];
    
    
    
    UIView* bgView = [[UIView alloc] init];
    [_navigationBgview addSubview:bgView];
    bgView.backgroundColor = [UIColor clearColor];
    //    KColorRGB(58, 128, 132);
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(_navigationBgview);
        make.height.equalTo(@35);
        //        make.top.equalTo(_navigationBgview).offset(48);
    }];
    
    _headLabel = [[UILabel alloc] init];
    _headLabel.userInteractionEnabled = YES;
    _headLabel.textColor = [UIColor whiteColor];
    _headLabel.textAlignment = NSTextAlignmentCenter;
    [_headLabel setFont:[UIFont systemFontOfSize:13]];
    [_headLabel sizeToFit];
    [bgView addSubview:_headLabel];
    [_headLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView);
        make.left.equalTo(@10);
    }];
    
    _addAllButton = [[UIButton alloc] init];
    _addAllButton.userInteractionEnabled = YES;
    _addAllButton.enabled = YES;
    [_addAllButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_addAllButton setTitle:@"添加所有" forState:UIControlStateNormal];
    _addAllButton.titleLabel.font = [UIFont systemFontOfSize: 14.0];
    [_addAllButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    _addAllButton.layer.cornerRadius = allCornerRadius;
    [_addAllButton addTarget:self action:@selector(AddAllButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [bgView addSubview:_addAllButton];
    [_addAllButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgView.mas_right).offset(-5);
        make.size.mas_equalTo(CGSizeMake(75, 28));
        make.centerY.equalTo(bgView);
    }];
    
}

-(void) requestData:(NSInteger ) page
{
    if(page == 1){
        _last_daishou_package_id = @"";
    }
    NSString *user_token = [SQUser loadmodel].user_token;
    NSString *pageString = [NSString stringWithFormat:@"%ld",(long)page];
    NSDictionary *dic = @{@"user_token":user_token,@"page":pageString, @"last_daishou_package_id":_last_daishou_package_id};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_TransshipExpressitems WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                _headLabel.text = [NSString stringWithFormat:@"未发货物品总重量 : %@",result[@"waiting_transhipp_weight_total"]];
                _last_daishou_package_id = result[@"last_daishou_package_id"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if (page == 1)
                        {
                            [dataArray removeAllObjects];
                            if ([items count] == 0)
                            {
                                [_addAllButton setBackgroundColor:[UIColor colorWithHexString:gray]];
                                _addAllButton.enabled = NO;
                                //                                [self.clickButton setTitle:@"已添加" forState:UIControlStateNormal];
                                
                            }else{
                                _listTableView.hidden = NO;
                            }
                        }
                        else
                        {
                            if ([items count] == 0)
                            {
                                showDefaultPromptTextHUD(@"没有更多内容了");
                                [self.listTableView.mj_footer endRefreshingWithNoMoreData];
                                
                                return ;
                                
                            }
                            else
                            {
                                self.currentPage++;
                            }
                        }
                        
                        NSArray* array = [SQzhuanyunBoughtModel mj_objectArrayWithKeyValuesArray:items];
                        NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
                        
                        NSString *bgId = @"";
                        for (NSInteger i = 0; i < array.count; i++)
                        {
                            SQzhuanyunBoughtModel *model = array[i];
                            if ([model.daishou_package_id isEqualToString:bgId] && (![bgId isEqualToString:@""])) {
                                [[mutableArray lastObject] addObject:model];
                            }else{
                                NSMutableArray *array = [NSMutableArray arrayWithObjects:model, nil];
                                [mutableArray addObject:array];
                            }
                            bgId = model.daishou_package_id;
                        }
                        [dataArray addObjectsFromArray:mutableArray];
                        //                        _listTableView.mj_footer.hidden = NO;
                        
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        [_listTableView reloadData];
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void) requestAddAll
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANGYUN_Transship_Addall WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                [self addAllDataChange];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


-(void) requestAddOne:(NSString*)my_id
{
    NSString *user_token = [SQUser loadmodel].user_token;
    //    NSString *pageString = [NSString stringWithFormat:@"%ld",(long)page];
    NSDictionary *dic = @{@"user_token":user_token,@"id":my_id};
    
    [self showLoadingHUD];
    @weakify(self);
    
    
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANGYUN_Transship_Addtocart WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        @strongify(self);
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                [self addOneDataChange:my_id];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}



#pragma mark - ---- delegate 🍎🌝
#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark- tableviewdelegate
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataArray count];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = dataArray[section];
    return [array count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"SQzhuanyunBoughtCell";
    
    SQzhuanyunBoughtCell *cell = (SQzhuanyunBoughtCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQzhuanyunBoughtCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQzhuanyunBoughtCell class]])
            {
                cell = (SQzhuanyunBoughtCell*)nib;
                break;
            }
        }
    }
    
    NSArray* sectionArray = dataArray[indexPath.section];
    SQzhuanyunBoughtModel *listModel = sectionArray[indexPath.row];
    [cell setData:listModel];
    //    NSURL *url = [NSURL URLWithString:listModel.item_thumb];
    //    [cell.headImageView sd_setImageWithURL:url placeholderImage:nil];
    
    return cell;
}




-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SQzhuanyunBoughtModel *model = dataArray[section][0];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 45)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *label= [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@" 来自:%@  %@ ",model.expresss_company_name,model.express_no];
    label.font = [UIFont systemFontOfSize:16];
    label.textColor = [UIColor blackColor];
    //label.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view addSubview:label];
    [label sizeToFit];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(@15);
        make.height.equalTo(@30);
    }];
    UIView *view1 = [[UIView alloc]init];
    [view addSubview:view1];
    view1.alpha = 0.6;
    view1.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    
    
    return view;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}
#pragma mark - ---- getters and setters 🍋🌞




@end
