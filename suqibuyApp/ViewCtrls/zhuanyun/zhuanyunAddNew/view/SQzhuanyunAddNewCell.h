//
//  SQzhuanyunAddNewCell.h
//  suqibuyApp
//
//  Created by apple on 16/5/31.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SQzhuanyunAddNewModel;
@interface SQzhuanyunAddNewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;

@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@property (weak, nonatomic) IBOutlet UILabel *centerLaebl;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
-(void)setData:(SQzhuanyunAddNewModel*)model;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageLeftConstraint;

@end
