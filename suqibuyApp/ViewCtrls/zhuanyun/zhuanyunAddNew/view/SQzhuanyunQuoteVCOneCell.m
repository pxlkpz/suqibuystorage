//
//  SQzhuanyunQuoteVCOneCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/30.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunQuoteVCOneCell.h"
#import "PxlTextView.h"

@implementation SQzhuanyunQuoteVCOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _centerLabel = [PxlTextView createLabel];
        [self.contentView addSubview:_centerLabel];
        _centerLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
        _centerLabel.textAlignment = NSTextAlignmentLeft;
        [_centerLabel setFont:[UIFont systemFontOfSize:15]];
        _centerLabel.numberOfLines = 0;
        [_centerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(15);
            make.top.equalTo(self.contentView).offset(10);
            make.bottom.equalTo(self.contentView).offset(-10);
            
            self.leftconstraint = make.left;
            
        }];
        
        
        
        CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width - 4 * 3-30; // 44 = avatar宽度，4 * 3为padding
        self.centerLabel.preferredMaxLayoutWidth = preferredMaxWidth;
        [self.centerLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
