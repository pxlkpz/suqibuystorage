

//
//  SQzhuanyunAddNewCell.m
//  suqibuyApp
//
//  Created by apple on 16/5/31.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunAddNewCell.h"
#import "SQzhuanyunAddNewModel.h"

@implementation SQzhuanyunAddNewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//{
//    "cart_id" = 1;
//    "cat_name" = "\U56db\U7c7b\U7269\U54c1";
//    "daishou_package_id" = 45;
//    "express_no" = 2314512351sad;
//    "expresss_company_name" = "\U901f\U5c14\U5feb\U9012";
//    "good_id" = 51;
//    "item_description" = "\U5546\U54c1\U63cf\U8ff0\U989c\U8272 \Uff1a\U7ea2\U82721";
//    "item_name" = "\U5546\U54c1\U540d\U79f0\U5546\U54c1\U540d\U79f0\U5546\U54c1\U540d\U79f01";
//    qty = 1;
//    size = 1X1X1;
//    weight = "1.00";
//}

-(void)setData:(SQzhuanyunAddNewModel*)model{
//    @property (weak, nonatomic) IBOutlet UIImageView *headImageView;
//
//    @property (weak, nonatomic) IBOutlet UILabel *topLabel;
//    
//    @property (weak, nonatomic) IBOutlet UILabel *centerLaebl;
//    @property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
    self.topLabel.text = model.item_name;
    self.centerLaebl.text = model.item_description;
    self.bottomLabel.text = [NSString stringWithFormat:@"%@g",model.weight];    
    
    
}
@end
