//
//  SQZhuanyunQuoteVCCell.h
//  suqibuyApp
//
//  Created by apple on 16/6/10.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBZPKTextField.h"

@interface SQZhuanyunQuoteVCCell : UITableViewCell
-(void)configMoneyCellWithDic:(id)model andIndexRow:(NSInteger )inter;


-(void)configInOrderDetail:(id)model andIndexRow:(NSInteger )row;

-(void)configApplyValueCellWithDic:(id)model;


@property (strong, nonatomic)  UILabel *leftTopLabel;
@property (strong, nonatomic)  UILabel *leftBottomLabel;

@property (strong, nonatomic)  UILabel *rightLabel;

@property (strong, nonatomic)  UILabel *leftcenterLabel;


@property (strong, nonatomic)  MBZPKTextField* textfield;


@property (strong, nonatomic) MASConstraint *leftBottomconstraint;
@property (strong, nonatomic) MASConstraint *leftTopconstraint;
@property (strong, nonatomic) MASConstraint *leftcenterconstraint;
@property (strong, nonatomic) MASConstraint *rightenterconstraint;



@end
