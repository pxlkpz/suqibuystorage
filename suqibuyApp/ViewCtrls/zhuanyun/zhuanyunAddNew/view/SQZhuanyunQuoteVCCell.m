//
//  SQZhuanyunQuoteVCCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/10.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQZhuanyunQuoteVCCell.h"
#import "PxlTextView.h"
#import "SQzhuanyunQuoteModel.h"
#import "SQzhuanyunLIstItemsModel.h"

#define notificationN @"SQzhuanyunQuoteVCNoti"

@interface SQZhuanyunQuoteVCCell()<UITextFieldDelegate>


@end
@implementation SQZhuanyunQuoteVCCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _leftTopLabel = [PxlTextView createLabel];
        _leftTopLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
        _leftTopLabel.textAlignment = NSTextAlignmentLeft;
        [_leftTopLabel setFont:[UIFont systemFontOfSize:15]];
        
        [self.contentView addSubview:_leftTopLabel];
        [_leftTopLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            self.leftTopconstraint = make.left.equalTo(self.contentView).offset(15);
            make.top.equalTo(self.contentView).offset(8);
            
        }];
        
        
        _leftcenterLabel = [PxlTextView createLabel];
        [self.contentView addSubview:_leftcenterLabel];
        _leftcenterLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
        _leftcenterLabel.textAlignment = NSTextAlignmentLeft;
        [_leftcenterLabel setFont:[UIFont systemFontOfSize:15]];
        
        [_leftcenterLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            self.leftcenterconstraint =  make.left.equalTo(self.contentView).offset(15);
            
            make.centerY.equalTo(self.contentView);
        }];
        
        
        _rightLabel = [PxlTextView createLabel];
        [self.contentView addSubview:_rightLabel];
        _rightLabel.textColor = [UIColor colorWithHexString:text_yellow];
        
        _textfield = [PxlTextView createMBZPKTextField];
        _textfield.delegate = self;
        [self.contentView addSubview:_textfield];
        _textfield.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [_rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            self.rightenterconstraint =  make.right.equalTo(_textfield.mas_left).offset(-2);
            make.centerY.equalTo(self.contentView).offset(0);
        }];
        
        [_textfield mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView).offset(0);
            make.right.equalTo(self.contentView).offset(-5);
            
            make.height.equalTo(@25);
            make.width.equalTo(@0);
        }];
        _textfield.hidden = YES;
        
        
        _leftBottomLabel = [PxlTextView createLabel];
        [self.contentView addSubview:_leftBottomLabel];
        [_leftBottomLabel setFont:[UIFont systemFontOfSize:13]];
        _leftBottomLabel.textColor = [UIColor colorWithHexString:text_shallow_gray];
        _leftBottomLabel.textAlignment = NSTextAlignmentLeft;
        _leftBottomLabel.numberOfLines = 0;
        [_leftBottomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            
            self.leftBottomconstraint = make.left.equalTo(self.contentView).offset(15);
            make.top.equalTo(_leftTopLabel.mas_bottom).offset(5);
            make.bottom.equalTo(self.contentView).offset(-5);
        }];
        
        CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width - 4 * 3-130; // 44 = avatar宽度，4 * 3为padding
        self.leftBottomLabel.preferredMaxLayoutWidth = preferredMaxWidth;
        [self.leftBottomLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        
        
    }
    return self;
}

-(void)configInOrderDetail:(SQzhuanyunLIstItemsModel*)model andIndexRow:(NSInteger)row{
    self.leftcenterLabel.text = @"";
    self.leftTopLabel.text = @"";
    self.leftBottomLabel.text = @"";
    self.rightLabel.text = @"";
    switch (row) {
        case 0:
        {
            self.leftcenterLabel.text = @"转运费:";
            self.rightLabel.text = [model.sub_total gtm_stringByUnescapingFromHTML];
        }
            break;
        case 1:
        {
            self.leftcenterLabel.text = @"转运操作费:";
            self.rightLabel.text = [model.agency_fee gtm_stringByUnescapingFromHTML];
        }
            break;
        case 2:
        {
            self.leftcenterLabel.text = [NSString stringWithFormat:@"购买保险[保额:%@]",model.insurance_amount];
            self.rightLabel.text = [model.insurance_fee gtm_stringByUnescapingFromHTML];
        }
            break;
        case 3:
        {
            self.leftcenterLabel.text = @"优惠券抵扣";
            self.rightLabel.text = [model.coupon_discount_amount gtm_stringByUnescapingFromHTML];
        }
            break;
        case 4:
        {
            self.leftcenterLabel.text = @"会员等级专享:";
            self.rightLabel.text = [model.usergroup_discount_amount gtm_stringByUnescapingFromHTML];
        }
            break;
        case 5:
        {
            self.leftcenterLabel.text = [NSString stringWithFormat:@"积分抵扣 [ 共使用%@个积分 ]",model.credits_point_total];
            self.rightLabel.text = [model.credits_amount gtm_stringByUnescapingFromHTML];
        }
            break;
        case 6:
        {
            self.leftcenterLabel.text = @"仓储费用";
            self.rightLabel.text = [model.warehouse_fee gtm_stringByUnescapingFromHTML];
        }
            break;
        case 7:
        {
            self.leftcenterLabel.text = @"";
            self.rightLabel.text = [model.grand_total gtm_stringByUnescapingFromHTML];
        }
            break;
            
            
        default:
            break;
    }
}
-(void)configApplyValueCellWithDic:(SQzhuanyunQuoteModel *)model{
    self.leftcenterLabel.text = @"";
    self.leftTopLabel.text = @"";
    self.leftBottomLabel.text = @"";
    self.rightLabel.text = @"";
    self.textfield.hidden = YES;
//    self.textfield.tag = inter;
    self.accessoryType = UITableViewCellAccessoryNone;

    
    
    self.leftTopLabel.text = @"申请价值(USD)";
    self.textfield.hidden = NO;
    self.leftBottomLabel.text = @"此是向目的国海关申报,关税到货物关税与货物清关,如不填写视为我司代为申报,产生问题我司不承担任何责任。";
    _rightLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    _rightLabel.text = @"USD";
    [self.textfield mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@70);
        make.right.equalTo(self.contentView).offset(-40);

    }];
    [_rightLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-5);
        make.centerY.equalTo(self.contentView).offset(0);
    }];


}

-(void)configMoneyCellWithDic:(SQzhuanyunQuoteModel *)model andIndexRow:(NSInteger )inter{
    self.leftcenterLabel.text = @"";
    self.leftTopLabel.text = @"";
    self.leftBottomLabel.text = @"";
    self.rightLabel.text = @"";
    self.textfield.hidden = YES;
    self.textfield.tag = inter;
    self.accessoryType = UITableViewCellAccessoryNone;
    
    switch (inter) {
        case 0:
        {
            self.leftTopLabel.text = @"转运运费";
            self.rightLabel.text = [model.sub_total gtm_stringByUnescapingFromHTML];
        }
            break;
        case 1:
        {
            self.leftTopLabel.text = @"转运操作费";
            self.rightLabel.text = [model.agency_fee gtm_stringByUnescapingFromHTML];
            self.leftBottomLabel.text = model.agency_fee_msg;
        }
            break;
        case 2:
        {
            self.leftTopLabel.text = @"购买保险(自愿购买)";
            self.textfield.hidden = NO;
            if (model.insurance_msg && ![model.insurance_msg isEqualToString:@""]) {
                self.leftBottomLabel.text = model.insurance_msg;
            }
            if (model.insurance_fee && ![model.insurance_fee isEqualToString:@""]) {
                self.rightLabel.text = [model.insurance_fee gtm_stringByUnescapingFromHTML];
            }
            
            [self.textfield mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@70);
            }];
            
        }
            break;
        case 3:
        {
            self.leftTopLabel.text = @"使用优惠券";
            if (model.coupon_code && ![model.coupon_code isEqualToString:@""]) {
                self.leftTopLabel.text = [NSString stringWithFormat:@"优惠券[%@]",model.coupon_code];
                
            }
            self.rightLabel.text = [model.coupon_discount_amount gtm_stringByUnescapingFromHTML];
            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            
        }
            break;
        case 4:
        {
            self.leftTopLabel.text = @"会员专享";
            self.rightLabel.text = [model.usergroup_discout_amount gtm_stringByUnescapingFromHTML];
            self.leftBottomLabel.text = model.usergroup_discount_msg;
            
        }
            break;
        case 5:
        {
            self.leftTopLabel.text = @"使用积分";
            self.textfield.hidden = NO;
            if (model.credits_msg && ![model.credits_msg isEqualToString:@""]) {
                self.leftBottomLabel.text = model.credits_msg;
            }
            if (model.credits_amount && ![model.credits_amount isEqualToString:@""]) {
                self.rightLabel.text = [model.credits_amount gtm_stringByUnescapingFromHTML];
            }
            
            [self.textfield mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@70);
                
            }];
            
        }
            break;
       
        default:
            break;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    NSArray *array = @[textField.text, [NSString stringWithFormat:@"%ld",(long)textField.tag]];
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationN object:array];
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
// may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return [self validateNumber:string];
}

- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}


@end
