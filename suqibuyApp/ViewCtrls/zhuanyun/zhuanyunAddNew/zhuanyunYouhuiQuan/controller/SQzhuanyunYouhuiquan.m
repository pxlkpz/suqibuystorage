//
//  SQzhuanyunYouhuiquan.m
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunYouhuiquan.h"

#import "SQzhuanyunFangshiModel.h"
#import "SQzhuanyunYouhuiquanCell.h"
#import "MBZPKTextField.h"
#import "SQzhuanyunYouHuiQuanModel.h"

@interface SQzhuanyunYouhuiquan ()<UITableViewDelegate,UITableViewDataSource>{
    //    SQzhuanyunFangshiModel *_model;
    NSArray* dataArray;
    MBZPKTextField *_textfield;
}
@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQzhuanyunYouhuiquan


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (_fromController == FromControllerNitao) {
        self.title = @"代购结算可用优惠券";
        
    }else{
        self.title = @"发货结算可用优惠券";
        
    }
    
    
    dataArray = [NSArray array];
    
    [self buildTabelHeadView];
    
    [self buildTableView];
    
    
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ---- event response 🍐🌛
-(void)sureButtonAction:(UIButton*)sender{
    if ([_textfield.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入优惠券券码");
        return;
    }
    [self requestData:_textfield.text];
}
#pragma mark - ---- private methods 🍊🌜
-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 80.0f;
    [_listTableView setTableFooterView:[[UIView alloc]init ]];
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        if (IPHONE_4_OR_LESS) {
            make.top.equalTo(@115);
            
        }else{
            make.top.equalTo(@100);
        }
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}

-(__kindof UIView*)buildTabelHeadView{
    UIView *tabelHeadView = [[UIView alloc] init];
    tabelHeadView.frame = CGRectMake(0, 0, screen_width, 100);
    [self.view addSubview:tabelHeadView];
    if (IPHONE_4_OR_LESS) {
        tabelHeadView.frame = CGRectMake(0, 0, screen_width, 115);
    }
    
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor colorWithHexString:bg_gray];
    [tabelHeadView addSubview:topView];
    topView.frame = CGRectMake(0, 0, screen_width, 10);
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor colorWithHexString:bg_gray];
    [tabelHeadView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(tabelHeadView);
        make.left.right.equalTo(tabelHeadView);
        make.height.equalTo(@10);
    }];
    
    
    
    UILabel * myLabel = [[UILabel alloc] init];
    myLabel.numberOfLines = 0;
    myLabel.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [myLabel setFont:[UIFont systemFontOfSize:15]];
    myLabel.text = @"请输入优惠券代码或者直接选择可用优惠券:";
    [tabelHeadView addSubview:myLabel];
    [myLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(topView.mas_bottom).offset(10);
        make.left.equalTo(tabelHeadView).offset(10);
        make.right.equalTo(tabelHeadView).offset(-10);
        
    }];
    
    
    _textfield = [[MBZPKTextField alloc] init];
    [_textfield setFont:[UIFont systemFontOfSize:14]];
    _textfield.textColor = [UIColor blackColor];
    [_textfield setTextAlignment:NSTextAlignmentLeft];
    _textfield.keyboardType = UIKeyboardTypeASCIICapable;
    [self.view addSubview:_textfield];
    //    _textfield.enabled = NO;
    [_textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-70);
        make.left.equalTo(self.view).offset(10);
        make.top.equalTo(myLabel.mas_bottom).offset(10);
        make.height.equalTo(@35);
    }];
    
    
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"确定" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    [button addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    //    button.tag = num;
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_textfield);
        make.right.equalTo(self.view.mas_right).offset(-5);
        make.left.equalTo(_textfield.mas_right).offset(5);
        make.bottom.equalTo(_textfield.mas_bottom);
    }];
    
    
    
    
    
    
    
    
    return  tabelHeadView;
    
    
}

-(void) requestData
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token};
    NSString*url = @"";
    if (_fromController == FromControllerNitao) {
        url = API_DAIGOU_MYCOUPON;
    }else{
        url = API_ZHUANYUN_transship_mycoupons;
    }
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:url WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    //                    _model = [SQzhuanyunFangshiModel mj_objectWithKeyValues:result];
                    
                    //                    [_listTableView setTableHeaderView:[self buildTabelHeadView]];
                    dataArray = [SQzhuanyunYouHuiQuanModel mj_objectArrayWithKeyValuesArray:result[@"items"]];
                    [self.listTableView reloadData];                    
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


-(void) requestData:(NSString*)code
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSString *url = @"";
    NSDictionary *dic = @{};
    if (_fromController == FromControllerNitao) {
        url = API_DAIGOU_USERCOUPON;
        dic = @{@"user_token":user_token,@"coupon":code};
        
    }else{
        url = API_ZHUANYUN_transship_usecoupon;
        dic = @{@"user_token":user_token,@"coupon_code":code};
        
    }
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:url WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}
//#pragma mark - ---- delegate 🍎🌝
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //    if (_model) {
    //        return _model.items.count;
    //    }
    return dataArray.count;
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SQzhuanyunYouhuiquanCell";
    
    SQzhuanyunYouhuiquanCell *cell = (SQzhuanyunYouhuiquanCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQzhuanyunYouhuiquanCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQzhuanyunYouhuiquanCell class]])
            {
                cell = (SQzhuanyunYouhuiquanCell*)nib;
                break;
            }
        }
    }
    
    [cell setDataInYouHuiQuan:dataArray[indexPath.row]];
    
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 50)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *label= [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@"我的优惠券:("];
    label.font = [UIFont systemFontOfSize:15];
    label.textColor =[UIColor colorWithHexString:text_color_gray_black];
    label.backgroundColor = [UIColor whiteColor];
    [view addSubview:label];
    [label sizeToFit];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.centerY.equalTo(view);
        make.height.equalTo(@50);
    }];
    
    UILabel *label2= [[UILabel alloc] init];
    label2.text = [NSString stringWithFormat:@"%lu",(unsigned long)dataArray.count];
    label2.font = [UIFont systemFontOfSize:14];
    label2.textColor =[UIColor colorWithHexString:text_yellow];
    label2.backgroundColor = [UIColor whiteColor];
    [view addSubview:label2];
    [label2 sizeToFit];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label.mas_right).offset(1);
        make.centerY.equalTo(view);
        
        make.height.equalTo(@50);
    }];
    
    
    UILabel *label3= [[UILabel alloc] init];
    label3.text = [NSString stringWithFormat:@")"];
    label3.font = [UIFont systemFontOfSize:14];
    label3.textColor =[UIColor colorWithHexString:text_color_gray_black];
    label3.backgroundColor = [UIColor whiteColor];
    [view addSubview:label3];
    [label3 sizeToFit];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label2.mas_right).offset(1);
        make.centerY.equalTo(view);
        
        make.height.equalTo(@50);
    }];
    
    UIView *view1 = [[UIView alloc]init];
    [view addSubview:view1];
    view1.alpha = 0.6;
    view1.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    
    
    return view;
}




-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SQzhuanyunYouHuiQuanModel*model =  dataArray[indexPath.row];
    [self requestData:model.code];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

#pragma mark - ---- getters and setters 🍋🌞


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
