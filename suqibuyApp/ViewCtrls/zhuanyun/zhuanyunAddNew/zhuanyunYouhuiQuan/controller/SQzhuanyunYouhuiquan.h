//
//  SQzhuanyunYouhuiquan.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"

typedef enum : NSUInteger {
    FromControllerNitao,
    FromControllerFahuo,
} FromController;

@interface SQzhuanyunYouhuiquan : SQBaseViewController

@property (nonatomic, assign) FromController fromController;


@end
