//
//  SQzhuanyunYouHuiQuanModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQzhuanyunYouHuiQuanModel : NSObject
//code = c48v38p7;
//description = "<null>";
//"discount_amount" = "&yen;100.00";
//"from_date" = "2016-06-01 07:37:30";
//name = "\U901f\U5947\U5377";
//status = "\U6709\U6548";
//"to_date" = "2016-07-30 07:37:33";
//"validate_desc" = "2016-06-01 07:37:30 \U81f3 2016-07-30 07:37:33; <br/>\U91d1\U989d\U5927\U4e8e2000.00\U53ef\U7528;";

@property (nonatomic, copy) NSString* code;
@property (nonatomic, copy) NSString* my_description;
@property (nonatomic, copy) NSString* discount_amount;
@property (nonatomic, copy) NSString* from_date;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSString* status;
@property (nonatomic, copy) NSString* to_date;
@property (nonatomic, copy) NSString* validate_desc;
@end
