//
//  SQzhuanyunYouhuiquanCell.h
//  suqibuyApp
//
//  Created by apple on 16/6/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQzhuanyunYouhuiquanCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *leftCenterLabel;
@property (weak, nonatomic) IBOutlet UILabel *centerLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightOneLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightTwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightThreeLabel;
//@property (weak, nonatomic) IBOutlet UILabel *rightFourLabel;
//-(void)setData:(id)data;
-(void)setDataInYouHuiQuan:(id)data;


@end
