//
//  SQzhuanyunYouhuiquanCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunYouhuiquanCell.h"
#import "SQzhuanyunYouHuiQuanModel.h"
@implementation SQzhuanyunYouhuiquanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width/2; // 44 = avatar宽度，4 * 3为padding
    
//    self.rightFourLabel.preferredMaxLayoutWidth = preferredMaxWidth; // 多行时必须设置
//    
//    [self.rightFourLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    
    self.rightThreeLabel.preferredMaxLayoutWidth = preferredMaxWidth; // 多行时必须设置
    
    [self.rightThreeLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    
//    self.rightOneLabel.font = [UIFont boldSystemFontOfSize:13];
    
    self.centerLabel.adjustsFontSizeToFitWidth = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)setDataInYouHuiQuan:(SQzhuanyunYouHuiQuanModel*)data{
    self.leftCenterLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    self.rightOneLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    self.rightTwoLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    self.rightThreeLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    
    NSString* desc = [data.validate_desc stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    
    self.leftCenterLabel.text = data.code;
    self.centerLabel.text = [data.discount_amount gtm_stringByUnescapingFromHTML];
    self.rightOneLabel.text = data.name;
    self.rightTwoLabel.text = data.status;
    self.rightThreeLabel.text = desc;
//    self.rightFourLabel.hidden = YES;
    
}
@end
