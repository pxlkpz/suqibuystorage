//
//  SQzhuanyunQuoteModel.h
//  suqibuyApp
//
//  Created by apple on 16/5/31.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQzhuanyunQuoteModel : NSObject

@property (nonatomic, copy) NSString* total_qty;
@property (nonatomic, copy) NSString* address_id;
@property (nonatomic, copy) NSString* shipping_method_id;
@property (nonatomic, copy) NSString* address_info;
@property (nonatomic, copy) NSString* has_error;
@property (nonatomic, copy) NSString* notice;
@property (nonatomic, copy) NSString* weight;
@property (nonatomic, copy) NSString* size;
@property (nonatomic, copy) NSString* max_side_length;
@property (nonatomic, copy) NSString* calculated_weight;
@property (nonatomic, copy) NSString* base_size_weight;
@property (nonatomic, copy) NSString* size_rate;
@property (nonatomic, copy) NSString* sub_total;
@property (nonatomic, copy) NSString* shipping_info;
@property (nonatomic, copy) NSString* coupon_code;
@property (nonatomic, copy) NSString* credits_point_total;
@property (nonatomic, copy) NSString* insurance_amount;
@property (nonatomic, copy) NSString* agency_fee_msg;
@property (nonatomic, copy) NSString* usergroup_discount_msg;
@property (nonatomic, copy) NSString* error_coupon;
@property (nonatomic, copy) NSString* error_credit;
@property (nonatomic, copy) NSString* error_insurance;
@property (nonatomic, copy) NSString* agency_fee;
@property (nonatomic, copy) NSString* insurance_fee;
@property (nonatomic, copy) NSString* usergroup_discout_amount;
@property (nonatomic, copy) NSString* coupon_discount_amount;
@property (nonatomic, copy) NSString* credits_amount;
@property (nonatomic, copy) NSString* grand_total;
@property (nonatomic, copy) NSString* credits_msg;
@property (nonatomic, copy) NSString* usergroup_discount_rate;
@property (nonatomic, copy) NSString* insurance_msg;

@property (nonatomic, strong) NSArray* items;

//
//"result": {
//    "items": [],
//    "total_qty": 13,
//    "address_id": "12",
//    "shipping_method_id": "5",
//    "address_info": "123123123 America 美国 213132, 312323",
//    "has_error": false,
//    "notice": null,
//    "weight": 33,
//    "size": 23,
//    "max_side_length": "11",
//    "calculated_weight": "33.00",
//    "base_size_weight": false,
//    "size_rate": null,
//    "sub_total": "¥173.00",
//    "shipping_info": "EMS[约5-8工作日，时效不稳定]",
//    "coupon_code": "",
//    "credits_point_total": "18163",
//    "insurance_amount": "4500.00",
//    "agency_fee_msg": "VIP1 用户的转运操作费率为：5%",
//    "usergroup_discount_msg": "VIP1 会员等级专享运费优惠：0.01%",
//    "error_coupon": "",
//    "error_credit": "",
//    "error_insurance": "",
//    "agency_fee": "¥8.65",
//    "insurance_fee": "¥90.00",
//    "usergroup_discout_amount": "- ¥0.02",
//    "coupon_discount_amount": "- ¥0.00",
//    "credits_amount": "- ¥181.63",
//    "grand_total": "¥90.00",
//    "credits_msg": "最多可用 27163 个积分",
//    "usergroup_discount_rate": 0,
//    "insurance_msg": "最高保险金额4500元，保费2%，保丢不保损坏"
//},
@end
