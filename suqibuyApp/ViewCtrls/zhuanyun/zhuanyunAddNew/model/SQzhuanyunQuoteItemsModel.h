//
//  SQzhuanyunQuoteItemsModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/8.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQzhuanyunQuoteItemsModel : NSObject
//"cart_id": "16",
//"good_id": "49",
//"daishou_package_id": "44",
//"expresss_company_name": "EMS快递",
//"express_no": "A234234234",
//"cat_name": "四类物品",
//"item_name": "商品名称商品名称商品名称1",
//"item_description": "商品描述颜色 ：红色1",
//"qty": "1",
//"weight": "1.00",
//"size": "1X1X1"

@property (nonatomic, copy) NSString* cart_id;
@property (nonatomic, copy) NSString* good_id;

@property (nonatomic, copy) NSString* daishou_package_id;

@property (nonatomic, copy) NSString* expresss_company_name;

@property (nonatomic, copy) NSString* express_no;

@property (nonatomic, copy) NSString* cat_name;

@property (nonatomic, copy) NSString* item_name;
@property (nonatomic, copy) NSString* item_description;
@property (nonatomic, copy) NSString* qty;
@property (nonatomic, copy) NSString* weight;
@property (nonatomic, copy) NSString* size;
@end
