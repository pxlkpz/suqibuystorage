//
//  SQzhuanyunAddNewModel.h
//  suqibuyApp
//
//  Created by apple on 16/5/31.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SQzhuanyunAddNewModel : NSObject
/*
 {
 "cart_id" = 15;
 "cat_name" = "\U56db\U7c7b\U7269\U54c1";
 "daishou_package_id" = 44;
 "express_no" = A234234234;
 "expresss_company_name" = "EMS\U5feb\U9012";
 "good_id" = 49;
 "item_description" = "\U5546\U54c1\U63cf\U8ff0\U989c\U8272 \Uff1a\U7ea2\U82721";
 "item_name" = "\U5546\U54c1\U540d\U79f0\U5546\U54c1\U540d\U79f0\U5546\U54c1\U540d\U79f01";
 qty = 1;
 size = 1X1X1;
 weight = "1.00";
 },

 */
/** 购物车中的ID */
@property (nonatomic, copy) NSString* cart_id;
/** 物品分类 */
@property (nonatomic, copy) NSString* cat_name;
/** 包裹id */
@property (nonatomic, copy) NSString* daishou_package_id;
/** 快递单号 */
@property (nonatomic, copy) NSString* express_no;
/** 快递公司 */
@property (nonatomic, copy) NSString* expresss_company_name;
/** 物品ID */
@property (nonatomic, copy) NSString* good_id;
/** 物品描述 */
@property (nonatomic, copy) NSString* item_description;
/** 物品名称 */
@property (nonatomic, copy) NSString* item_name;
/** 数量 */
@property (nonatomic, copy) NSString* qty;
/** 大小 */
@property (nonatomic, copy) NSString* size;
/** 重量 */
@property (nonatomic, copy) NSString* weight;

@end
