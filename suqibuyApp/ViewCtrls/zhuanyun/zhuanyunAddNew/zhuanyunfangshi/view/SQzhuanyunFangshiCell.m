//
//  SQzhuanyunFangshiCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunFangshiCell.h"
#import "SQzhuanyunFangshiItemsModel.h"
#import "SQzhuanyunYouHuiQuanModel.h"

@implementation SQzhuanyunFangshiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    //    self.rightFourLabel
    CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width/2; // 44 = avatar宽度，4 * 3为padding
    
    self.rightFourLabel.preferredMaxLayoutWidth = preferredMaxWidth; // 多行时必须设置
    
    [self.rightFourLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    
    self.centerLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)setData:(SQzhuanyunFangshiItemsModel*)data{
    //    @property (weak, nonatomic) IBOutlet UILabel *leftCenterLabel;
    //    @property (weak, nonatomic) IBOutlet UILabel *centerLabel;
    //    @property (weak, nonatomic) IBOutlet UILabel *rightOneLabel;
    //    @property (weak, nonatomic) IBOutlet UILabel *rightTwoLabel;
    //    @property (weak, nonatomic) IBOutlet UILabel *rightThreeLabel;
    //    @property (weak, nonatomic) IBOutlet UILabel *rightFourLabel;
    //"description": "约5-8工作日，时效不稳定",
    //"limit_weight": "限重：30.00 克(g)",
    //"limit_height": "限高：1.00 米",
    //"base_size_weight": "是否按体积计费：否"
    
    self.leftCenterLabel.text = data.shipping_method;
    self.centerLabel.text = [data.calculate_shipping_fee gtm_stringByUnescapingFromHTML];
    self.rightOneLabel.text = data.base_size_weight;
    self.rightTwoLabel.text = data.limit_weight;
    self.rightThreeLabel.text = data.limit_height;
    self.rightFourLabel.text = data.my_description;
}

-(void)setDataInYouHuiQuan:(SQzhuanyunYouHuiQuanModel*)data{
    self.leftCenterLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    self.rightOneLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    self.rightTwoLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    self.rightThreeLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    
    NSString* desc = [data.validate_desc stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    
    self.leftCenterLabel.text = data.code;
    self.centerLabel.text = [data.discount_amount gtm_stringByUnescapingFromHTML];
    self.rightOneLabel.text = data.name;
    self.rightTwoLabel.text = data.status;
    self.rightThreeLabel.text = desc;
    self.rightFourLabel.hidden = YES;
    
}

@end
