//
//  SQzhuanyunFangshiModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQzhuanyunFangshiModel : NSObject
//"command": "transship.shippingmethods",
//"request_code": "",
//"result": {
//    "items": [
//    
//    ],
//    "notice": "(* 提示：此处查出运费为大约费用，实际费用因包裹打包后重量不同，有误差，请以最终费用为准)",
//    "shipping_count": 4
//},

@property (nonatomic, copy) NSString* notice;
@property (nonatomic, copy) NSArray* items;

@property (nonatomic, copy) NSString* shipping_count;

@end
