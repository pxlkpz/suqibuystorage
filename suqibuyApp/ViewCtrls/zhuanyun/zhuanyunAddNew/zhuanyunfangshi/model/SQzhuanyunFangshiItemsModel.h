//
//  SQzhuanyunFangshiItemsModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQzhuanyunFangshiItemsModel : NSObject

//"id": "5",
//"is_selected": false,
//"shipping_method": "EMS",
//"calculate_shipping_fee": "¥173.00",
//"description": "约5-8工作日，时效不稳定",
//"limit_weight": "限重：30.00 克(g)",
//"limit_height": "限高：1.00 米",
//"base_size_weight": "是否按体积计费：否"
@property (nonatomic, copy) NSString* my_id;
@property (nonatomic, copy) NSString* is_selected;
@property (nonatomic, copy) NSString* shipping_method;
@property (nonatomic, copy) NSString* calculate_shipping_fee;
@property (nonatomic, copy) NSString* my_description;
 @property (nonatomic, copy) NSString* limit_height;
@property (nonatomic, copy) NSString* limit_weight;
@property (nonatomic, copy) NSString* base_size_weight;


@end
