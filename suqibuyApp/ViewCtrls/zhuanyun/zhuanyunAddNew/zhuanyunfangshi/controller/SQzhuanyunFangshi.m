//
//  SQzhuanyunFangshi.m
//  suqibuyApp
//
//  Created by apple on 16/6/11.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunFangshi.h"
#import "SQzhuanyunFangshiModel.h"
#import "SQzhuanyunFangshiCell.h"
#import "SQzhuanyunFangshiItemsModel.h"

@interface SQzhuanyunFangshi ()<UITableViewDelegate,UITableViewDataSource>{
    SQzhuanyunFangshiModel *_model;
}
@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQzhuanyunFangshi

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"选择发货方式";
    [self buildTableView];
    
    [self requestData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ---- lefe cycle 🍏🌚

#pragma mark - ---- event response 🍐🌛

#pragma mark - ---- private methods 🍊🌜
-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    [_listTableView setTableFooterView:[[UIView alloc]init ]];
    
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    //[_listTableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    _listTableView.estimatedRowHeight = 80.0f;
    
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    //    [_listTableView layoutIfNeeded];
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}

-(__kindof UIView*)buildTabelHeadView{
    UIView *tabelHeadView = [[UIView alloc] init];
    tabelHeadView.frame = CGRectMake(0, 0, screen_width, 80);
    
    
    UIView* bgView = [[UIView alloc] init];
    [tabelHeadView addSubview:bgView];
    bgView.backgroundColor = KColorFromRGB(0xf7f7f7);
    //    [UIColor colorWithHexString:seperator_color_gray];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(tabelHeadView).offset(0);
        make.left.equalTo(tabelHeadView).offset(10);
        make.right.equalTo(tabelHeadView).offset(-10);
        make.height.equalTo(@70);
    }];
    bgView.layer.borderWidth = 1;
    bgView.layer.borderColor = [UIColor colorWithHexString:border_color].CGColor;
    bgView.layer.cornerRadius = allCornerRadius;
    
    
    UIImageView *imageview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"notice_icon"]];
    [bgView addSubview:imageview];
    [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView);
        make.left.equalTo(@5);
        make.size.mas_equalTo(CGSizeMake(21, 21));
    }];
    
    NSString * htmlString = _model.notice;
    UILabel * myLabel = [[UILabel alloc] init];
    myLabel.numberOfLines = 0;
    myLabel.textColor = [UIColor colorWithHexString:text_shallow_gray];
    //    myLabel.textColor = [UIColor whiteColor];
    [myLabel setFont:[UIFont systemFontOfSize:14]];
    myLabel.text = htmlString;
    [bgView addSubview:myLabel];
    [myLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageview.mas_right).offset(5);
        make.top.equalTo(@5);
        make.right.equalTo(bgView).offset(-5);
        make.bottom.equalTo(bgView).offset(-5);
    }];
    
    
    return  tabelHeadView;
    
    
}



-(void) requestData:(NSString*)ItemsId
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"shipping_method_id":ItemsId};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_transship_selectshippingmethod WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void) requestData
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_transship_shippingmethods WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _model = [SQzhuanyunFangshiModel mj_objectWithKeyValues:result];
                    
                    [_listTableView setTableHeaderView:[self buildTabelHeadView]];
                    
                    [self.listTableView reloadData];
                    
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark - ---- delegate 🍎🌝
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_model) {
        return _model.items.count;
    }
    return 0;
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SQzhuanyunFangshiCell";
    
    SQzhuanyunFangshiCell *cell = (SQzhuanyunFangshiCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQzhuanyunFangshiCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQzhuanyunFangshiCell class]])
            {
                cell = (SQzhuanyunFangshiCell*)nib;
                break;
            }
        }
    }
    
    [cell setData:_model.items[indexPath.row]];
    
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 30)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *label= [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@"可转运方式:("];
    label.font = [UIFont systemFontOfSize:15];
    label.textColor =[UIColor blackColor];
    label.backgroundColor = [UIColor whiteColor];
    [view addSubview:label];
    [label sizeToFit];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@15);
        make.centerY.equalTo(view);
        make.height.equalTo(@30);
    }];
    
    UILabel *label2= [[UILabel alloc] init];
    if (_model.shipping_count && ![_model.shipping_count isEqualToString:@""]) {
        label2.text = [NSString stringWithFormat:@"%@",_model.shipping_count];
    }else{
        label2.text = [NSString stringWithFormat:@"0"];
        
    }
    label2.font = [UIFont systemFontOfSize:14];
    label2.textColor =[UIColor colorWithHexString:text_yellow];
    label2.backgroundColor = [UIColor whiteColor];
    [view addSubview:label2];
    [label2 sizeToFit];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label.mas_right).offset(1);
        make.centerY.equalTo(view);
        
        make.height.equalTo(@30);
    }];
    
    
    UILabel *label3= [[UILabel alloc] init];
    label3.text = [NSString stringWithFormat:@")"];
    label3.font = [UIFont systemFontOfSize:15];
    label3.textColor =[UIColor blackColor];
    label3.backgroundColor = [UIColor whiteColor];
    [view addSubview:label3];
    [label3 sizeToFit];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label2.mas_right).offset(1);
        make.centerY.equalTo(view);
        
        make.height.equalTo(@30);
    }];
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    
    return view;
}




-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SQzhuanyunFangshiItemsModel*model =  _model.items[indexPath.row];
    [self requestData:model.my_id];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}


#pragma mark - ---- getters and setters 🍋🌞



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
