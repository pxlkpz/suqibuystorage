//
//  SQzhuanyunAddNewVC.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunAddNewVC.h"
#import "SQdaishouListCell.h"
#import "SQzhuanyunAddNewModel.h"
#import "SQzhuanyunAddNewCell.h"
#import "UIAlertView+BlocksKit.h"
#import "SQzhuanyunQuoteVC.h"
#import "SQzhuanyunBoughtVC.h"

@interface SQzhuanyunAddNewVC ()<UITableViewDelegate,UITableViewDataSource>
{    
    
    UIButton *addItemButton;
    
    NSMutableArray *dataArray;
    
    UIView *_clearView;
    
    UIButton* _closeButton;
    
    UILabel *_cart_totalLabel;
    UILabel *_headLabel;
    UIButton*_starButton;
    
    UIView *_navigationBgview;
    
}

@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQzhuanyunAddNewVC
//@synthesize listTableView;
#pragma mark - ---- lefe cycle 🍏🌚

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"待发货清单";
    
    
    dataArray = [[NSMutableArray alloc] init];
    
    //    [self buildHeadView];
    
    [self buildTableView];
    
    [self buildclearView];
    
    
    [self buildBottomView];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self buildHeadView];
    
    [self requestData];
    
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [_navigationBgview removeFromSuperview];
}
#pragma mark - ---- event response 🍐🌛
/**
 *  添加可发货物品
 */
-(void)addItem{
    SQzhuanyunBoughtVC *controller = [[SQzhuanyunBoughtVC alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)closeButtonAction:(UIButton*)sender{
    SQzhuanyunQuoteVC *controller = [[SQzhuanyunQuoteVC alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - ---- private methods 🍊🌜
///transship/delitem
-(void) requestDelitem:(NSString*)my_id
{
    //id ->cart_id
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"id":my_id};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_TransshipDelitem WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _cart_totalLabel.text = [NSString stringWithFormat:@"%@",result[@"cart_total"]];
                    _headLabel.text = [NSString stringWithFormat:@"当前购物车总重量 : %@g",result[@"total_weight"]];
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if ([items count] == 0)
                        {
                            _clearView.hidden = NO;
                            _listTableView.hidden = YES;
                            
                            _starButton.enabled = NO;
                            _starButton.backgroundColor = KColorRGB(115, 115, 115);
                            //                            showDefaultPromptTextHUD(@"没有更多内容了");
                            return ;
                        }
                        
                        _starButton.enabled = YES;
                        _starButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
                        
                        [dataArray removeAllObjects];
                        _clearView.hidden = YES;
                        
                        NSArray* array = [SQzhuanyunAddNewModel mj_objectArrayWithKeyValuesArray:items];
                        NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
                        
                        NSString *bgId = @"";
                        for (NSInteger i = 0; i < array.count; i++)
                        {
                            SQzhuanyunAddNewModel *model = array[i];
                            if ([model.daishou_package_id isEqualToString:bgId] && (![bgId isEqualToString:@""])) {
                                [[mutableArray lastObject] addObject:model];
                            }else{
                                NSMutableArray *array = [NSMutableArray arrayWithObjects:model, nil];
                                [mutableArray addObject:array];
                            }
                            bgId = model.daishou_package_id;
                        }
                        [dataArray addObjectsFromArray:mutableArray];
                        _listTableView.hidden = NO;
                        [_listTableView reloadData];
                    }
                }
            }
            else
            {
                _listTableView.hidden = YES;
                
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        _listTableView.hidden = YES;
        
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        _listTableView.hidden = YES;
        
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void) requestData
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_TransshipCartitems WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _cart_totalLabel.text = [NSString stringWithFormat:@"%@",result[@"cart_total"]];
                    _headLabel.text = [NSString stringWithFormat:@"当前购物车总重量 : %@g",result[@"total_weight"]];
                    
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if ([items count] == 0)
                        {
                            _clearView.hidden = NO;
                            _starButton.enabled = NO;
                            _starButton.backgroundColor = KColorRGB(115, 115, 115);
                            _listTableView.hidden = YES;
                            return ;
                        }
                        
                        _starButton.enabled = YES;
                        _starButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
                        [dataArray removeAllObjects];
                        _clearView.hidden = YES;
                        
                        NSArray* array = [SQzhuanyunAddNewModel mj_objectArrayWithKeyValuesArray:items];
                        NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
                        
                        NSString *bgId = @"";
                        for (NSInteger i = 0; i < array.count; i++)
                        {
                            SQzhuanyunAddNewModel *model = array[i];
                            if ([model.daishou_package_id isEqualToString:bgId] && (![bgId isEqualToString:@""])) {
                                [[mutableArray lastObject] addObject:model];
                            }else{
                                NSMutableArray *array = [NSMutableArray arrayWithObjects:model, nil];
                                [mutableArray addObject:array];
                            }
                            bgId = model.daishou_package_id;
                        }
                        [dataArray addObjectsFromArray:mutableArray];
                        _listTableView.hidden = NO;
                        [_listTableView reloadData];
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [_listTableView.mj_header endRefreshing];
    } WithErrorBlock:^(id errorCode) {
        [_listTableView.mj_header endRefreshing];
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [_listTableView.mj_header endRefreshing];
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}
-(void)buildHeadView{
    _navigationBgview = [[UIView alloc]initWithFrame:CGRectMake(0, 44, KScreenWidth, 30)];
    _navigationBgview.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar addSubview:_navigationBgview];
    
    
    UIView* bgView = [[UIView alloc] init];
    [_navigationBgview addSubview:bgView];
    bgView.backgroundColor = KColorRGB(250, 60, 6);
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(_navigationBgview);
        make.height.equalTo(@30);
        //        make.top.equalTo(_navigationBgview).offset(48);
    }];
    
    _headLabel = [[UILabel alloc] init];
    _headLabel.text = @"未发货物品总重量 : ";
    _headLabel.textColor = [UIColor whiteColor];
    _headLabel.textAlignment = NSTextAlignmentRight;
    [_headLabel setFont:[UIFont systemFontOfSize:13]];
    [_headLabel sizeToFit];
    [bgView addSubview:_headLabel];
    [_headLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView);
        make.right.mas_equalTo(-5);
    }];
    
    
}

-(void)buildclearView{
    _clearView = [[UIView alloc] init];
    [self.view addSubview:_clearView];
    _clearView.hidden = YES;
    _clearView.frame = CGRectMake(0, 0, screen_width, 300);
    
    UILabel* noItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, screen_width, 200)];
    noItemLabel.text = @"您尚未添加任何内容！";
    noItemLabel.textColor = [UIColor colorWithHexString:text_gray];
    noItemLabel.textAlignment = NSTextAlignmentCenter;
    noItemLabel.font = [UIFont systemFontOfSize:14];
    noItemLabel.numberOfLines = 2;
    [_clearView addSubview:noItemLabel];
    
    
    addItemButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 200, screen_width - 20, 40)];
    addItemButton.layer.cornerRadius = allCornerRadius;
    [addItemButton setTitle:@"添加可发货物品" forState:UIControlStateNormal];
    addItemButton.titleLabel.font = [UIFont systemFontOfSize:15];
    addItemButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
    [addItemButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addItemButton addTarget:self action:@selector(addItem) forControlEvents:UIControlEventTouchUpInside];
    
    [_clearView addSubview:addItemButton];
}

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    //                      WithFrame:CGRectMake(0,0,0,0) style:UITableViewStyleGrouped];
    _listTableView.hidden = YES;
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 80.0f;
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    [_listTableView setTableFooterView:[self buildFooterView]];
    
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@30);
        make.bottom.equalTo(self.view).offset(-50);
    }];
    
    @weakify(self);
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestData];
    }];
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}

-(void)buildBottomView{
    UIView* bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = KColorFromRGB(0xE2E2E2);
    [self.view addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        
        if(IsiPhoneX)
        {
            make.bottom.equalTo(self.view).offset(-24);
        }
        else
        {
            make.bottom.equalTo(self.view);
        }
       
        
        make.height.equalTo(@50);
    }];
    
    UIView* leftView = [[UIView alloc] init];
    [bottomView addSubview:leftView];
    [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(bottomView);
        make.width.equalTo(bottomView.mas_width).multipliedBy(0.5);
    }];
    
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor blackColor];
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont systemFontOfSize:15]];
    [label1 sizeToFit];
    label1.text = @"已选择";
    [leftView addSubview:label1];
    
    _cart_totalLabel = [[UILabel alloc] init];
    _cart_totalLabel.textColor = [UIColor colorWithHexString:text_yellow];
    _cart_totalLabel.textAlignment = NSTextAlignmentCenter;
    [_cart_totalLabel setFont:[UIFont systemFontOfSize:15]];
    [_cart_totalLabel sizeToFit];
    _cart_totalLabel.text = @"0";
    [leftView addSubview:_cart_totalLabel];
    
    UILabel *label3 = [[UILabel alloc] init];
    label3.textColor = [UIColor blackColor];
    label3.textAlignment = NSTextAlignmentCenter;
    [label3 setFont:[UIFont systemFontOfSize:15]];
    [label3 sizeToFit];
    label3.text = @"件商品";
    [leftView addSubview:label3];
    
    [_cart_totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(leftView);
    }];
    
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_cart_totalLabel.mas_right).offset(5);
        make.centerY.equalTo(leftView);
    }];
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_cart_totalLabel.mas_left).offset(-5);
        make.centerY.equalTo(leftView);
    }];
    
    
    
    _starButton = [[UIButton alloc] init];
    [_starButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_starButton setTitle:@"开始发货" forState:UIControlStateNormal];
    [_starButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    [_starButton addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:_starButton];
    [_starButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(bottomView);
        make.width.equalTo(bottomView.mas_width).multipliedBy(0.5);
    }];
    
    
    
}

-(__kindof UIView*)buildFooterView{
    
    UIView* _bgView = [[UIView alloc] init];
    _bgView.frame = CGRectMake(0, 0, screen_width, 50);
    
    UIButton* AddItemButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 5, screen_width - 20, 40)];
    AddItemButton.layer.cornerRadius = allCornerRadius;
    [AddItemButton setTitle:@"添加可发货物品" forState:UIControlStateNormal];
    AddItemButton.titleLabel.font = [UIFont systemFontOfSize:15];
    AddItemButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
    [AddItemButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [AddItemButton addTarget:self action:@selector(addItem) forControlEvents:UIControlEventTouchUpInside];
    [_bgView addSubview:AddItemButton];
    
    return _bgView;
}

#pragma mark - ---- delegate 🍎🌝
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [dataArray count];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray[section] count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SQzhuanyunAddNewCell";
    
    SQzhuanyunAddNewCell *cell = (SQzhuanyunAddNewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQzhuanyunAddNewCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQzhuanyunAddNewCell class]])
            {
                cell = (SQzhuanyunAddNewCell*)nib;
                break;
            }
        }
    }
    
    [cell setData:dataArray[indexPath.section][indexPath.row]];
    
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SQzhuanyunAddNewModel *model = dataArray[section][0];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 45)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *label= [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@" 来自:%@  %@ ",model.expresss_company_name,model.express_no];
    label.font = [UIFont systemFontOfSize:16];
    label.textColor = [UIColor whiteColor];
    //label.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view addSubview:label];
    [label sizeToFit];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(@15);
        make.height.equalTo(@30);
    }];
    UIView *view1 = [[UIView alloc]init];
    [view addSubview:view1];
    view1.alpha = 0.6;
    view1.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    return view;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        //        @weakify(self);
        //        [UIAlertView bk_showAlertViewWithTitle:@"提醒" message:@"您真的要删除吗？\n删除后不可恢复，不过您可以再次添加。" cancelButtonTitle:@"否" otherButtonTitles:@[@"是"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        //            @strongify(self);
        //            if (buttonIndex == 1) {
        //            }
        //        }];
        
        SQzhuanyunAddNewModel* model = dataArray[indexPath.section][indexPath.row];
        [self requestDelitem:model.cart_id];
    }
    
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}


#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}
#pragma mark - ---- getters and setters 🍋🌞


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
