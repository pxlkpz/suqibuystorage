//
//  SQzhuanyunQuoteVC.m
//  suqibuyApp
//
//  Created by apple on 16/5/31.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunQuoteVC.h"
#import "SQzhuanyunAddNewCell.h"
#import "SQzhuanyunAddNewModel.h"
#import "SQzhuanyunQuoteModel.h"
#import "SQZhuanyunQuoteVCCell.h"
#import "SQwoUserAddress.h"
#import "SQzhuanyunFangshi.h"
#import "MBZPKTextView.h"
#import "SQzhuanyunYouhuiquan.h"
#import "SQzhuanyunPay.h"
#import "SQzhuanyunQuoteVCOneCell.h"

#define showString @"若您对订单有特殊需求,请在这里备注"

@interface SQzhuanyunQuoteVC ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
{
    NSMutableArray *dataArray;
    SQzhuanyunQuoteModel* _quoteModel;
    UILabel* _cart_totalLabel;
    UIView *bgTableFooterView;
    MBZPKTextView *_beizutextView;
    
    
    UIButton* _sureButton;
}
@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQzhuanyunQuoteVC


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"创建发货订单";
    
    dataArray = [[NSMutableArray alloc] init];
    
    [self buildTableView];
    
    [self buildBottomView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifictionAction:) name:@"SQzhuanyunQuoteVCNoti" object:nil];
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self requestData];
    
    
}
#pragma mark - ---- event response 🍐🌛

-(void)notifictionAction:(NSNotification*)noti{
    NSArray *arr = [noti object];
    NSInteger row = [arr[1] integerValue];
    if (row == 2) {
        [self requestDataInsuranceAmount:arr[0]];
    }else if(row == 5){
        [self requestDataCredits_point_total:arr[0]];
    }
}

/**
 结算 --

 @param sender button
 */
-(void)closeButtonAction:(UIButton*)sender{
    if ([_beizutextView.text isEqualToString:showString]) {
        [self requestDataSaveOrder:@""];
    }
    else{
        [self requestDataSaveOrder:_beizutextView.text];
    }
}
#pragma mark - ---- private methods 🍊🌜
-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.hidden = YES;
    _listTableView.dataSource = self;
    _listTableView.tag = 5;
    _listTableView.delegate = self;
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 80.0f;
    //[_listTableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    [_listTableView registerClass:[SQZhuanyunQuoteVCCell class] forCellReuseIdentifier:@"SQZhuanyunQuoteVCCell"];
    [_listTableView registerClass:[SQzhuanyunQuoteVCOneCell class] forCellReuseIdentifier:@"SQzhuanyunQuoteVCOneCell"];
    
    
    
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(-50);
    }];
    [_listTableView setTableFooterView:[self buildTableFooterView]];
    
    
    if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
        
    }
    if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
        [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
    }
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}

-(__kindof UIView*)buildTableFooterView{
    bgTableFooterView = [[UIView alloc] init];
    bgTableFooterView.frame = CGRectMake(0, 0, screen_width, 120);
    
    _beizutextView = [[MBZPKTextView alloc] init];
    [_beizutextView setFont:[UIFont systemFontOfSize:14]];
    _beizutextView.delegate =self;
    _beizutextView.text = showString;
    //    _beizutextView.textColor = [UIColor blackColor];
    _beizutextView.textColor = [UIColor colorWithHexString:text_shallow_gray];
    
    [_beizutextView setTextAlignment:NSTextAlignmentLeft];
    [bgTableFooterView addSubview:_beizutextView];
    [_beizutextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgTableFooterView).offset(-5);
        make.left.equalTo(bgTableFooterView).offset(5);
        make.top.equalTo(bgTableFooterView).offset(10);
        make.bottom.equalTo(bgTableFooterView).offset(-40);
    }];
    
    UILabel *label= [[UILabel alloc] init];
    label.text = @"(打包标准:去包装,球鞋,吊牌,发票等,如有不需要去掉或者其他要求请备注)";
    label.font = [UIFont systemFontOfSize:13];
    label.textColor = [UIColor colorWithHexString:text_color_gray_black];
    
    [bgTableFooterView addSubview:label];
    label.numberOfLines = 0;
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgTableFooterView).offset(-5);
        make.left.equalTo(bgTableFooterView).offset(5);
        make.top.equalTo(_beizutextView.mas_bottom).offset(5);
        make.bottom.equalTo(bgTableFooterView).offset(0);
    }];
    
    
    
    
    
    return bgTableFooterView;
}

-(void)buildBottomView{
    UIView* bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = KColorFromRGB(0xE2E2E2);
    [self.view addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        
        if(IsiPhoneX)
        {
            make.bottom.equalTo(self.view).offset(-24);
        }
        else
        {
            make.bottom.equalTo(self.view);
        }
        
        make.height.equalTo(@50);
    }];
    
    UIView* leftView = [[UIView alloc] init];
    [bottomView addSubview:leftView];
    [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(bottomView);
        make.width.equalTo(bottomView.mas_width).multipliedBy(0.7);
    }];
    
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor blackColor];
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont systemFontOfSize:16]];
    [label1 sizeToFit];
    label1.text = @"总金额:";
    [leftView addSubview:label1];
    
    _cart_totalLabel = [[UILabel alloc] init];
    _cart_totalLabel.textColor = [UIColor colorWithHexString:simple_button_yellow];
    _cart_totalLabel.textAlignment = NSTextAlignmentCenter;
    [_cart_totalLabel setFont:[UIFont systemFontOfSize:16]];
    [_cart_totalLabel sizeToFit];
    //    _cart_totalLabel.text = @"2";
    [leftView addSubview:_cart_totalLabel];
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(leftView).offset(10);
        make.centerY.equalTo(leftView);
    }];
    
    [_cart_totalLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(leftView);
        make.left.equalTo(label1.mas_right).offset(5);
    }];
    
    _sureButton = [[UIButton alloc] init];
    [_sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_sureButton setTitle:@"结算" forState:UIControlStateNormal];
    [_sureButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    [_sureButton addTarget:self action:@selector(closeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:_sureButton];
    [_sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(bottomView);
        make.width.equalTo(bottomView.mas_width).multipliedBy(0.3);
    }];
    
    
    
    
    
}


-(void) requestData
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_TransshipQuote WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _quoteModel = [SQzhuanyunQuoteModel mj_objectWithKeyValues:result];
                    _cart_totalLabel.text = [_quoteModel.grand_total gtm_stringByUnescapingFromHTML];
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if ([items count] == 0)
                        {
                            showDefaultPromptTextHUD(@"没有更多内容了");
                            return ;
                        }
                        
                        [dataArray removeAllObjects];
                        
                        NSArray* array = [SQzhuanyunAddNewModel mj_objectArrayWithKeyValuesArray:items];
                        NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
                        
                        NSString *bgId = @"";
                        for (NSInteger i = 0; i < array.count; i++)
                        {
                            SQzhuanyunAddNewModel *model = array[i];
                            if ([model.daishou_package_id isEqualToString:bgId] && (![bgId isEqualToString:@""])) {
                                [[mutableArray lastObject] addObject:model];
                            }else{
                                NSMutableArray *array = [NSMutableArray arrayWithObjects:model, nil];
                                [mutableArray addObject:array];
                            }
                            bgId = model.daishou_package_id;
                        }
                        [dataArray addObjectsFromArray:mutableArray];
                        _listTableView.hidden = NO;
                        
                        [_listTableView reloadData];
                        
                        if ([_quoteModel.shipping_method_id isEqualToString:@""] || !_quoteModel.shipping_method_id || [_quoteModel.address_id isEqualToString:@""] || !_quoteModel.address_id) {
                            _sureButton.enabled =NO;
                            [_sureButton setBackgroundColor:[UIColor colorWithHexString:text_color_gray_black]];
                            
                        }else{
                            [_sureButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
                            _sureButton.enabled =YES;
                            
                        }
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void) requestDataInsuranceAmount:(NSString*)insuranceAmoun

{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"insurance_amount":insuranceAmoun};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_transship_insurancepost WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _quoteModel = [SQzhuanyunQuoteModel mj_objectWithKeyValues:result];
                    _cart_totalLabel.text = [_quoteModel.grand_total gtm_stringByUnescapingFromHTML];
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if ([items count] == 0)
                        {
                            showDefaultPromptTextHUD(@"没有更多内容了");
                            return ;
                        }
                        
                        [dataArray removeAllObjects];
                        
                        NSArray* array = [SQzhuanyunAddNewModel mj_objectArrayWithKeyValuesArray:items];
                        NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
                        
                        NSString *bgId = @"";
                        for (NSInteger i = 0; i < array.count; i++)
                        {
                            SQzhuanyunAddNewModel *model = array[i];
                            if ([model.daishou_package_id isEqualToString:bgId] && (![bgId isEqualToString:@""])) {
                                [[mutableArray lastObject] addObject:model];
                            }else{
                                NSMutableArray *array = [NSMutableArray arrayWithObjects:model, nil];
                                [mutableArray addObject:array];
                            }
                            bgId = model.daishou_package_id;
                        }
                        [dataArray addObjectsFromArray:mutableArray];
                        _listTableView.hidden = NO;
                        
                        [_listTableView reloadData];
                        
                        if ([_quoteModel.shipping_method_id isEqualToString:@""] || !_quoteModel.shipping_method_id || [_quoteModel.address_id isEqualToString:@""] || !_quoteModel.address_id) {
                            _sureButton.enabled =NO;
                            [_sureButton setBackgroundColor:[UIColor colorWithHexString:text_color_gray_black]];
                            
                        }else{
                            [_sureButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
                            _sureButton.enabled =YES;
                            
                        }
                        
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


-(void) requestDataCredits_point_total:(NSString*)credits_point_total

{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"credits_point_total":credits_point_total};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_transship_usecredit WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _quoteModel = [SQzhuanyunQuoteModel mj_objectWithKeyValues:result];
                    _cart_totalLabel.text = [_quoteModel.grand_total gtm_stringByUnescapingFromHTML];
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if ([items count] == 0)
                        {
                            showDefaultPromptTextHUD(@"没有更多内容了");
                            return ;
                        }
                        
                        [dataArray removeAllObjects];
                        
                        NSArray* array = [SQzhuanyunAddNewModel mj_objectArrayWithKeyValuesArray:items];
                        NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
                        
                        NSString *bgId = @"";
                        for (NSInteger i = 0; i < array.count; i++)
                        {
                            SQzhuanyunAddNewModel *model = array[i];
                            if ([model.daishou_package_id isEqualToString:bgId] && (![bgId isEqualToString:@""])) {
                                [[mutableArray lastObject] addObject:model];
                            }else{
                                NSMutableArray *array = [NSMutableArray arrayWithObjects:model, nil];
                                [mutableArray addObject:array];
                            }
                            bgId = model.daishou_package_id;
                        }
                        [dataArray addObjectsFromArray:mutableArray];
                        _listTableView.hidden = NO;
                        
                        [_listTableView reloadData];
                        
                        if ([_quoteModel.shipping_method_id isEqualToString:@""] || !_quoteModel.shipping_method_id || [_quoteModel.address_id isEqualToString:@""] || !_quoteModel.address_id) {
                            _sureButton.enabled =NO;
                            [_sureButton setBackgroundColor:[UIColor colorWithHexString:text_color_gray_black]];
                            
                        }else{
                            [_sureButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
                            _sureButton.enabled =YES;
                            
                        }
                        
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void)requestDataSaveOrder:(NSString*)custom_remark
{
    NSString *apply_value = @"";
    SQZhuanyunQuoteVCCell *cell = [_listTableView  cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:[dataArray count]+2]];
    apply_value = cell.textfield.text;
    if ([cell.textfield.text isEqualToString:@""] || cell.textfield.text == nil) {
        apply_value = @"";
    }
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"custom_remark":custom_remark, @"apply_value": apply_value};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_transship_saveorder WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    SQzhuanyunLIstItemsModel* model1 = [SQzhuanyunLIstItemsModel mj_objectWithKeyValues:result];
                    SQzhuanyunPay *vc = [[SQzhuanyunPay alloc] init];
                    vc.model = model1;
                    [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0],self.navigationController.viewControllers[1], vc]];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}



#pragma mark - ---- delegate 🍎🌝
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataArray count] + 5;
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section < 2) {
        return 1;
    }else if (section == [dataArray count]+2){
        return 1;
    }else if (section == [dataArray count]+3){
        return 1;
    }else if (section == [dataArray count]+4){
        return 6;
    }
    
    return [dataArray[section-2] count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        
        NSString *identf1;
        identf1 = @"SQzhuanyunQuoteVCOneCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identf1];
        if (nil == cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identf1];
        }
        SQzhuanyunQuoteVCOneCell*zhuanyunOnecell = (SQzhuanyunQuoteVCOneCell*)cell;
        zhuanyunOnecell.leftconstraint.equalTo(@5);
        
        zhuanyunOnecell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        zhuanyunOnecell.centerLabel.text = _quoteModel.address_info;
        
        if (!_quoteModel.address_info && _quoteModel.notice) {
            zhuanyunOnecell.centerLabel.text = _quoteModel.notice;
        }
        return zhuanyunOnecell;
    }
    
    if (indexPath.section == 1 || indexPath.section > [dataArray count]+1) {// 1 && X.next
        NSString *identf;
        identf = @"SQZhuanyunQuoteVCCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identf];
        if (nil == cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identf];
        }
        
        SQZhuanyunQuoteVCCell*zhuanyuncell = (SQZhuanyunQuoteVCCell*)cell;
        zhuanyuncell.leftBottomconstraint.equalTo(@5);
        zhuanyuncell.leftcenterconstraint.equalTo(@5);
        zhuanyuncell.leftTopconstraint.equalTo(@5);
        
        zhuanyuncell.leftcenterLabel.text = @"";
        zhuanyuncell.leftTopLabel.text = @"";
        zhuanyuncell.leftBottomLabel.text = @"";
        zhuanyuncell.rightLabel.text = @"";
        zhuanyuncell.textfield.hidden = YES;
        [zhuanyuncell.textfield mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@0);
        }];
        
        if (indexPath.section == 1) {
            
            zhuanyuncell.leftcenterLabel.text = @"商品清单";
            zhuanyuncell.accessoryType = UITableViewCellAccessoryNone;
            
        }else if (indexPath.section == [dataArray count]+2){
            //--- 转运
            [zhuanyuncell configApplyValueCellWithDic:_quoteModel];
            
        }else if (indexPath.section == [dataArray count]+3){
            //--- 转运
            zhuanyuncell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            zhuanyuncell.leftcenterLabel.text = @"请选择转运方式";
            if(_quoteModel.shipping_info){
                zhuanyuncell.leftcenterLabel.text = _quoteModel.shipping_info;
                
            }
            
        }else if (indexPath.section == [dataArray count]+4 && _quoteModel) {
            
            [zhuanyuncell configMoneyCellWithDic:_quoteModel andIndexRow:indexPath.row];
            
        }
        
        
        return zhuanyuncell;
    }
    
    
    //----
    static NSString *CellIdentifier = @"SQzhuanyunAddNewCell";
    
    SQzhuanyunAddNewCell *cell = (SQzhuanyunAddNewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQzhuanyunAddNewCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQzhuanyunAddNewCell class]])
            {
                cell = (SQzhuanyunAddNewCell*)nib;
                break;
            }
        }
    }
    cell.imageLeftConstraint.constant = 5;
    [cell setData:dataArray[indexPath.section-2][indexPath.row]];
    
    return cell;
    
    
    
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section < 2 || section > [dataArray count]+1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 45)];
        view.backgroundColor = [UIColor colorWithHexString:bg_gray];
        return view;
    }
    
    SQzhuanyunAddNewModel *model = dataArray[section-2][0];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 45)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *label= [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@" 来自:%@  %@ ",model.expresss_company_name,model.express_no];
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view addSubview:label];
    [label sizeToFit];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(@15);
        make.left.mas_equalTo(0);
        make.height.equalTo(@30);
    }];
    UIView *view1 = [[UIView alloc]init];
    [view addSubview:view1];
    view1.alpha = 0.6;
    view1.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    
    return view;
    
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        SQwoUserAddress *cv = [[SQwoUserAddress alloc] init];
        cv.type = 10;
        [self.navigationController pushViewController:cv animated:YES];
    }else if (indexPath.section == [dataArray count]+3){
        
        SQzhuanyunFangshi *cv = [[SQzhuanyunFangshi alloc] init];
        [self.navigationController pushViewController:cv animated:YES];
        
    }else if(indexPath.section == [dataArray count]+4){
        if (indexPath.row == 3) {
            SQzhuanyunYouhuiquan *vc = [[SQzhuanyunYouhuiquan alloc] init];
            vc.fromController = FromControllerFahuo;
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 30)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *statusLabel1= [[UILabel alloc] init];
    statusLabel1.text = @"件物品";
    statusLabel1.textAlignment = NSTextAlignmentRight;
    statusLabel1.font = [UIFont systemFontOfSize:13];
    [statusLabel1 sizeToFit];
    statusLabel1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    [view addSubview:statusLabel1];
    [statusLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-10);
        make.centerY.equalTo(view);
    }];
    
    
    UILabel *LabelNumber= [[UILabel alloc] init];
    LabelNumber.text = _quoteModel.total_qty;
    LabelNumber.textAlignment = NSTextAlignmentRight;
    LabelNumber.font = [UIFont systemFontOfSize:13];
    [LabelNumber sizeToFit];
    LabelNumber.textColor = [UIColor colorWithHexString:simple_button_yellow];
    [view addSubview:LabelNumber];
    [LabelNumber mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(statusLabel1.mas_left).offset(-4);
        make.centerY.equalTo(view);
    }];
    
    UILabel *statusLabel= [[UILabel alloc] init];
    statusLabel.text = @"共";
    statusLabel.textAlignment = NSTextAlignmentRight;
    statusLabel.font = [UIFont systemFontOfSize:13];
    [statusLabel sizeToFit];
    statusLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    [view addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(LabelNumber.mas_left).offset(-4);
        make.centerY.equalTo(view);
    }];
    
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    
    return view;
}



-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (section < 2 || section > [dataArray count]+1) {
        return 10;
    }
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == dataArray.count + 1) {
        return 30;
    }
    
    return 0;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return UITableViewAutomaticDimension;
    }
    else if (indexPath.section == 1) {
        return 30;
    }
    else if (indexPath.section == [dataArray count]+2) {//--申报价值
        return UITableViewAutomaticDimension;
    }else if (indexPath.section == [dataArray count]+3  ) {
        return 40;
    }
    return UITableViewAutomaticDimension;
}
#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}


- (void)textViewDidBeginEditing:(UITextView *)textView {
    if ([textView.text isEqualToString:showString]) {
        textView.text = @"";
        _beizutextView.textColor = [UIColor colorWithHexString:text_color_gray_black];
        
    }
}
//3.在结束编辑的代理方法中进行如下操作
- (void)textViewDidEndEditing:(UITextView *)textView {
    if (textView.text.length<1) {
        textView.text = showString;
        _beizutextView.textColor = [UIColor colorWithHexString:text_shallow_gray];
        
    }
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
}

#pragma mark - ---- getters and setters 🍋🌞



@end
