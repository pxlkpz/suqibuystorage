//
//  SQzhuanyunFeeRequstModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/13.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQzhuanyunFeeRequstModel : NSObject
///costestimate/result?country_id=1&weight=500&length=10&width=10&high=10&daigouamount=0&group_id=5&api_token=IWantToGetAIphone6&thing_cat_str=2

@property (nonatomic, copy) NSString* country_id;
@property (nonatomic, copy) NSString* thing_cat_str;
@property (nonatomic, copy) NSString* weight;
@property (nonatomic, copy) NSString* width;
@property (nonatomic, copy) NSString* length;
@property (nonatomic, copy) NSString* high;
@property (nonatomic, copy) NSString* daigouamount;
@property (nonatomic, copy) NSString* group_id;
@property (nonatomic, copy) NSString* api_token;

@end
