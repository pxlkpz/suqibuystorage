//
//  SQzhuanyunFeeVC.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunFeeVC.h"
#import "MBZPKTextField.h"
#import "SQAddressSearchViewCtrl.h"
#import "PxlTextView.h"
#import "CommonWebViewController.h"
#import "SQzhuanyunFeeRequstModel.h"
#import "MBSingleWebViewController.h"
#import "SQzhuanyunFeeClassTypeView.h"
#import "suqibuyApp-Swift.h"

@interface SQzhuanyunFeeVC ()<SQAddressSearchViewDelegate, UITextFieldDelegate>{
    MBZPKTextField* _textField_country;
    UIButton* _changeButton;
    
    
    
    MBZPKTextField* _textField_weight;
    MBZPKTextField* _textField_size1;
    MBZPKTextField* _textField_size2;
    MBZPKTextField* _textField_size3;
    MBZPKTextField* _textField_money;
    
}
@property(nonatomic, strong)UITableView *listTableView;
@property(nonatomic, strong) UIView *myView;
@property(nonatomic, strong) SQzhuanyunFeeRequstModel* requstModel;

@end

@implementation SQzhuanyunFeeVC



#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"费用估算";
    _requstModel = [[SQzhuanyunFeeRequstModel alloc] init];
    
    [self buildTableView];
}


#pragma mark - ---- event response 🍐🌛
-(void)buttonAction:(UIButton*)sender{
    SQAddressSearchViewCtrl *controller = [[SQAddressSearchViewCtrl alloc] init];
    controller.delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
}
/**
 *  选择物品分类
 *
 *  @param sender uibutton
 */
-(void)promptButtonAction:(UIButton*)sender{
    
    if (_changeButton) {
        _changeButton.selected = NO;
    }
    _changeButton = sender;
    
    sender.selected = YES;
    
    _requstModel.thing_cat_str = [NSString stringWithFormat:@"%ld",(long)sender.tag];
}
/**
 *  马上估值
 *
 *  @param sender uibutton
 */
-(void)SurebuttonAction:(UIButton*)sender{
    
    _requstModel.high = _textField_size3.text;
    _requstModel.width = _textField_size2.text;
    _requstModel.length = _textField_size1.text;
    
    _requstModel.weight = _textField_weight.text;
    _requstModel.daigouamount = _textField_money.text;
    
    if ([_requstModel.country_id isEqualToString:@""] || !_requstModel.country_id) {
        showDefaultPromptTextHUD(@"请选择国家");
        return;
    }else if ([_requstModel.thing_cat_str isEqualToString:@""] || !_requstModel.thing_cat_str) {
        showDefaultPromptTextHUD(@"请选择货物分类");
        return;
    }else if ([_requstModel.weight isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入货物重量");
        return;
    }
    
    
    NSString *url = [NSString stringWithFormat:@"%@costestimate/result?country_id=%@&weight=%@&length=%@&width=%@&high=%@&daigouamount=%@&thing_cat_str=%@&api_token=%@&group_id=%@",API_MAIN_URL_HTML,_requstModel.country_id, _requstModel.weight, _requstModel.length, _requstModel.width, _requstModel.high,_requstModel.daigouamount, _requstModel.thing_cat_str,[self  getApi_token],[SQUser loadmodel].group_id];
    
    CommonWebViewController *vc = [[CommonWebViewController alloc] initWithUrlStr:url andNavTitle:@"邮寄费用查询"];
    [self.navigationController pushViewController:vc animated:YES];
}


-(NSString *)getApi_token
{
    if ([UIScreen mainScreen].scale > 1)
    {
        if (screen_height == 480)
        {
            return @"IWantToGetAIphone6";
        }
        else if(screen_height == 568)
        {
            return @"IphoneRetinaEquipment";
        }
    }
    
    return @"IphoneOtherEquipment";
}

#pragma mark - ---- private methods 🍊🌜
-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.tag = 5;

    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    [_listTableView setTableFooterView:[[UIView alloc] init]];
    [_listTableView layoutIfNeeded];
    
    [_listTableView setTableHeaderView:[self buildTableHeaderView]];
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
    
}
-(UIView*)buildTableHeaderView{
    _myView = [[UIView alloc] init];
    _myView.frame = CGRectMake(0, 0, screen_width, 565);
    
    
    const NSInteger num = 0;
    
    _textField_country = [self creatCellUI:@"国家(*)" andTopNumber:30+num];
    _textField_country.enabled = NO;
    
    //物品分类
    [self createCargoClass];
    
    
    [self createMoneyNumber];
    
    [self createWeightNumber];

    
    [self createFiveCellNumber:368.5];
    
    return _myView;
    
}


-(__kindof MBZPKTextField*)creatCellUI:(NSString*)Str andTopNumber:(NSInteger)num{
    
    UILabel *label1 = [self createLeftTitleLabel];
    label1.text = Str;
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_myView).offset(16);
        make.top.equalTo(_myView).offset(36);
    }];

    
    MBZPKTextField *textfield = [self createMBZPKTextField];
    textfield.layer.borderWidth = 0;
    textfield.backgroundColor = [UIColor colorWithHexString:@"#F4F4F4"];
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView).offset(-55.7);
        make.left.equalTo(label1.mas_right).offset(10);
        make.height.equalTo(@35);
        make.centerY.equalTo(label1);

    }];
    
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:@"sq_daigou_gusuan_add"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];

    [_myView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(textfield);
        make.right.equalTo(_myView.mas_right).offset(-16);
        make.size.mas_equalTo(CGSizeMake(29, 30));
    }];
    
    return textfield;
    
}


-(void)createCargoClass{
    
    SQzhuanyunFeeClassTypeView *feeClassView = [[SQzhuanyunFeeClassTypeView alloc] initWithNibName:@"SQzhuanyunFeeClassTypeView"];
    [_myView addSubview:feeClassView];
    [feeClassView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_myView);
        make.height.equalTo(@149);
        make.top.equalTo(_myView).offset(84);
    }];
    
    @weakify(self);
    feeClassView.viewBlock = ^(NSUInteger Type, id returnValue) {
        @strongify(self);
        if (Type == 0) {
            SQConfineController *controller = [[SQConfineController alloc] init];
            controller.title = @"转运物品限制";
            [self.navigationController pushViewController:controller animated:YES];
        } else if (Type == 1) {
            self.requstModel.thing_cat_str = [NSString stringWithFormat:@"%@",returnValue];
        }
    };
}

-(__kindof MBZPKTextField*)createMoneyNumber{
    
    _textField_money = [self createMBZPKTextField];
    _textField_money.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    _textField_money.delegate = self;
    [_textField_money mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView).offset(-40);
        make.left.equalTo(_myView).offset(102.5);
        make.top.equalTo(_myView).offset(258.5);
        make.height.equalTo(@30);
    }];
    
    UILabel *label1 = [self createLeftTitleLabel];
    label1.text = @"代购金额(*)";
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.centerY.equalTo(_textField_money);
        make.right.equalTo(_textField_money.mas_left).offset(-10);
    }];
    
    UILabel *label2 = [self createLabel];
    label2.text = @"元";
    
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_textField_money.mas_right).offset(13);
        make.centerY.equalTo(_textField_money);
    }];
    return _textField_money;
}

-(__kindof MBZPKTextField*)createWeightNumber{
    _textField_weight = [self createMBZPKTextField];
    _textField_weight.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    
    [_textField_weight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView).offset(-40);
        make.left.equalTo(_myView).offset(102.5);
        make.top.mas_equalTo(_textField_money.mas_bottom).offset(25);
        make.height.equalTo(@30);
    }];
    
    UILabel *label1 = [self createLeftTitleLabel];
    label1.text = @"总重量(*)";
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_textField_weight);
        make.right.equalTo(_textField_weight.mas_left).offset(-10);
        
    }];
    
    UILabel *label2 = [self createLabel];
    label2.text = @"克";
    
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_textField_weight.mas_right).offset(13);
        make.centerY.equalTo(_textField_weight);
    }];

    return _textField_weight;
}

-(__kindof MBZPKTextField*)createFiveCellNumber:(NSInteger)num{
    
    _textField_size1 = [self createMBZPKTextField];
    _textField_size1.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    
    _textField_size2= [self createMBZPKTextField];
    _textField_size2.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    
    _textField_size3 = [self createMBZPKTextField];
    _textField_size3.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    
    UILabel *label1 = [self createLeftTitleLabel];
    
    UILabel *label2 = [self createLabel];
    [label2 setFont:[UIFont systemFontOfSize:15]];
    
    label2.textColor = [UIColor colorWithHexString:@"#333333"];
    
    UILabel *label3 = [self createLabel];
    [label3 setFont:[UIFont systemFontOfSize:13]];
    
    label3.textColor = [UIColor colorWithHexString:@"#333333"];
    
    UILabel *label4 = [self createLabel];
    
    _textField_size1.placeholder = @"长";
    _textField_size1.delegate = self;
    [_textField_size1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@50);
        make.left.equalTo(_myView).offset(102.5);
        make.top.mas_equalTo(num);
        make.height.equalTo(@30);
    }];
    
    
    _textField_size2.placeholder = @"宽";
    _textField_size2.delegate = self;
    [_textField_size2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@50);
        make.left.equalTo(label2.mas_right).offset(10);
        make.top.mas_equalTo(num);
        make.height.equalTo(@30);
    }];
    
    
    _textField_size3.placeholder = @"高";
    _textField_size3.delegate = self;
    [_textField_size3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@50);
        make.left.equalTo(label3.mas_right).offset(10);
        make.top.mas_equalTo(num);
        make.height.equalTo(@30);
    }];
    
    label1.text = @"体积";
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_textField_size1.mas_left).offset(-10);
        make.centerY.equalTo(_textField_size1);
    }];
    
    
    label2.text = @"×";
    
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_textField_size1.mas_right).offset(10);
        make.centerY.equalTo(_textField_size1);
    }];
    
    
    label3.text = @"×";
    
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_textField_size2.mas_right).offset(10);
        make.centerY.equalTo(_textField_size1);
    }];
    
    label4.text = @"cm";
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_textField_size3.mas_right).offset(8.2);
        make.centerY.equalTo(_textField_size1);
    }];
    

    
    UIView *lineView = [UIView new];
    [_myView addSubview:lineView];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_myView);
        make.height.mas_equalTo(0.5);
        make.top.equalTo(_textField_size1.mas_bottom).offset(68);
    }];
    
    UIButton *button = [[UIButton alloc] init];
    [button setImage:[UIImage imageNamed:@"sq_daigou_gusuan_button_sure"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(SurebuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_myView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineView.mas_bottom).offset(51);
        make.right.equalTo(_myView.mas_right).offset(-16);
        make.left.equalTo(_myView).offset(16);
        make.height.equalTo(@40);
    }];

    return _textField_size1;
}




-(__kindof UIButton*)createButton:(NSString *)titileStr{
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitle:titileStr forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setImage:[UIImage imageNamed:@"unselect"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
    
    [button addTarget:self action:@selector(promptButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    return button;
}


-(__kindof MBZPKTextField*)createMBZPKTextField{
    MBZPKTextField *textfield1 = [[MBZPKTextField alloc] init];
    [textfield1 setFont:[UIFont systemFontOfSize:14]];
    textfield1.textColor = [UIColor blackColor];
    [textfield1 setTextAlignment:NSTextAlignmentLeft];
    [_myView addSubview:textfield1];
    
    return textfield1;
}

-(__kindof UILabel*)createLabel{
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:@"#999999"];
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont systemFontOfSize:12]];
    [label1 sizeToFit];
    [_myView addSubview:label1];
    
    return label1;
}

-(__kindof UILabel*)createLeftTitleLabel{
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:@"#333333"];
    [label1 setFont:[UIFont systemFontOfSize:15]];
    [_myView addSubview:label1];
    
    return label1;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian = YES;
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            
            //首字母不能为0和小数点
            if([textField.text length] == 0){
                if(single == '.') {

                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
                
            }
            
            if ([textField.text isEqualToString:@"0"]) {
                if([textField.text length] == 1){
                    if(single != '.') {

                        [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        return NO;
                    }else{
                        isHaveDian = YES;
                        return YES;
                        
                    }
                    
                }
                
            }
            
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian = YES;
                    return YES;
                    
                }else{
                    //                    [self showError:@"亲，您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                
                if (isHaveDian) {//存在小数点
                    
                    //判断小数点的位数
                    return YES;
                    
                }else{
                    return YES;
                }
            }
            
            
        }else{//输入的数据格式不正确
            
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
}

#pragma mark - ---- delegate 🍎🌝
- (void)addressSearchViewDelegate:(NSDictionary *)data{
    _textField_country.text = data[@"name"];
    _requstModel.country_id = data[@"id"];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}
#pragma mark - ---- getters and setters 🍋🌞



@end

