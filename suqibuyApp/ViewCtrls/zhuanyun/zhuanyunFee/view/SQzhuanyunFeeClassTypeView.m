//
//  SQzhuanyunFeeClassTypeView.m
//  suqibuyApp
//
//  Created by pxl on 2018/2/4.
//  Copyright © 2018年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunFeeClassTypeView.h"
@interface SQzhuanyunFeeClassTypeView ()
@property (nonatomic, strong) UIButton *changeButton;

@end
@implementation SQzhuanyunFeeClassTypeView

- (IBAction)detailAction:(id)sender {
    if (self.viewBlock) {
        self.viewBlock(0, nil);
    }
    
}
- (IBAction)selectAction:(UIButton *)sender {
    
    if (_changeButton) {
        _changeButton.selected = NO;
    }
    _changeButton = sender;
    
    sender.selected = YES;
    if (self.viewBlock) {
        self.viewBlock(1, [NSString stringWithFormat:@"%ld",(long)sender.tag]);
    }
}

@end
