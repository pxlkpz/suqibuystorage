//
//  SQzhuanyunVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunVC.h"
#import "PxlCommonViewCreate.h"
@interface SQzhuanyunVC ()<PxlCommonViewCreateDelegate>
@property (nonatomic, strong) PxlCommonViewCreate *pxlTool;
@end

@implementation SQzhuanyunVC


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"发货中心";

    _pxlTool = [[PxlCommonViewCreate alloc] init];
    _pxlTool.delegate = self;
    [_pxlTool pxl_buildUI:[PxlCommonViewCreate pxl_getPlistData:@"SQzhuanyunUIData"] andSuperView:self.view];
    
}

#pragma mark - ---- event response 🍐🌛



#pragma mark - ---- delegate 🍎🌝
-(void)pxl_CommonViewCreateDelegate:(NSString *)pushString{
    UIViewController *controller = [[NSClassFromString(pushString) alloc] init];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];

}
#pragma mark - ---- getters and setters 🍋🌞


@end
