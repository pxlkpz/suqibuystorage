//
//  SQTaoBaoAttributesModel.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/28.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQTaoBaoAttributesModel.h"

@implementation SQTaoBaoAttributesModel

+(NSDictionary *)mj_objectClassInArray
{
    return @{@"items":@"SQTaoBaoItemsModel"};
}
@end
