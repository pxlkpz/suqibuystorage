//
//  SQTaoBaoDaigouModel.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/28.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQTaoBaoDaigouModel.h"

@implementation SQTaoBaoDaigouModel

+(NSDictionary *)mj_objectClassInArray
{
    return @{@"skus":@"SQTaoBaoSkusModel",
             @"attributes":@"SQTaoBaoAttributesModel"};
}
@end
