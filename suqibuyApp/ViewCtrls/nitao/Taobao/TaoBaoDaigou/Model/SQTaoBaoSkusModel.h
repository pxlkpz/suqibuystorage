//
//  SQTaoBaoSkusModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQTaoBaoSkusModel : NSObject

@property (copy, nonatomic) NSString *key;
@property (copy, nonatomic) NSString *price;
@property (copy, nonatomic) NSString *skuId;
@property (copy, nonatomic) NSString *stock;


@end
