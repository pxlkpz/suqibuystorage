//
//  SQTaoBaoItemsModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQTaoBaoItemsModel : NSObject

@property (copy, nonatomic) NSString *key;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *icon;
@property (assign, nonatomic) BOOL is_selected;

@end
