//
//  SQTaoBaoAttributesModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/28.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQTaoBaoItemsModel.h"

@interface SQTaoBaoAttributesModel : NSObject

@property (copy, nonatomic) NSString *label;
@property (strong, nonatomic) NSMutableArray * items;

@end


