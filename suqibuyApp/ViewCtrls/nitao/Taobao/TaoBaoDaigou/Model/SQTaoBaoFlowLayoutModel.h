//
//  SQTaoBaoFlowLayoutModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQTaoBaoFlowLayoutModel : NSObject

@property (assign, nonatomic) CGFloat worksWidth;
@property (assign, nonatomic) CGFloat worksHeight;

@end
