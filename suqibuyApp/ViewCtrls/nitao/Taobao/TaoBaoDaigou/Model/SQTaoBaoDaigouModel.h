//
//  SQTaoBaoDaigouModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/28.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQTaoBaoAttributesModel.h"
#import "SQTaoBaoSkusModel.h"

@interface SQTaoBaoDaigouModel : NSObject

@property (copy, nonatomic) NSString *web;
@property (copy, nonatomic) NSString *shipping_fee;
@property (copy, nonatomic) NSString *pid;
@property (copy, nonatomic) NSString *shopid;
@property (copy, nonatomic) NSString *shop_name;
@property (copy, nonatomic) NSString *seller_id;
@property (copy, nonatomic) NSString *seller_nick;
@property (copy, nonatomic) NSString *ptitle;
@property (copy, nonatomic) NSString *selected_price;
@property (copy, nonatomic) NSString *mainpic;
@property (copy, nonatomic) NSString *selected_sku;
@property (copy, nonatomic) NSString *selected_stock;
@property (copy, nonatomic) NSString *count;
@property (copy, nonatomic) NSString *url;

@property (strong, nonatomic) NSMutableArray *skus;
@property (strong, nonatomic) NSMutableArray *attributes;

@end


