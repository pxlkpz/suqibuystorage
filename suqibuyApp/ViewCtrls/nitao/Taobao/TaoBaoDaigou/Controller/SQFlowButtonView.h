//
//  SQFlowButtonView.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/6/13.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQTaoBaoAttributesModel.h"

@interface SQFlowButtonView : UIView <UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) SQTaoBaoAttributesModel *model;
@property (assign, nonatomic) CGFloat height;

@property (strong, nonatomic) SQTaoBaoItemsModel *selectItem;

-(instancetype) initWithModel:(SQTaoBaoAttributesModel *) model;
-(void) initFlow;
@end
