//
//  SQTaobaoFlowLayout.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQTaoBaoFlowLayoutModel.h"

@interface SQTaobaoFlowLayout : UICollectionViewFlowLayout


// 总行数
@property (nonatomic, assign) NSInteger rowCount;
// 所有的item宽高
@property (nonatomic, strong) NSMutableArray *modelList;
@end
