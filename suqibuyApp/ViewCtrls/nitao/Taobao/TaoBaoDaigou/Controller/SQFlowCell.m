//
//  SQFlowCell.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/6/14.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQFlowCell.h"

@implementation SQFlowCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.label.textColor = [UIColor colorWithHexString:text_color];
    self.label.font = [UIFont systemFontOfSize:14];
    self.label.textAlignment= NSTextAlignmentCenter;
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor colorWithHexString:border_color].CGColor;
    self.layer.cornerRadius = allCornerRadius;
}

-(void) setSelected:(BOOL)selected
{
    if (selected)
    {
        self.backgroundColor = [UIColor colorWithHexString:border_color];
    }
    else
    {
        self.backgroundColor = [UIColor whiteColor];
    }
}

@end
