//
//  SQTaoBaoDaigouVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/28.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQTaoBaoDaigouVC.h"
#import "GTMBase64.h"
#import "SQTaoBaoDaigouModel.h"
#import "MBZPKButton.h"
#import "SQFillShopList.h"
#import "MBZPKBaseView.h"
#import "MBZPKTextField.h"
#import "SQFlowButtonView.h"
#import "suqibuyApp-Swift.h"
@interface SQTaoBaoDaigouVC ()<UIAlertViewDelegate>
{
    SQTaoBaoDaigouModel *daigouModel;
    
    //代购按钮
    UIButton *button;
    
    //点桉数
    NSMutableArray *touchArray;
    
    //skuid label
    UILabel *skuIdLabel;
    
    //属性 label
    UILabel *attributeLabel;
    
    //存放瀑布流数组
    NSMutableArray *flowArray;
    
    //最上面的view
    MBZPKBaseView *priceView;
    
    //物品价格显示label
    UILabel *priceSignLabel;
    
    //修改物品价格按钮
    UIButton *editButton;
    
    //确认修改物品价格按钮
    UIButton *confirmButton;
    
    //物品修改价格文本框
    MBZPKTextField *priceTextField;
    
    //数量文本框
    UITextField *quantityTextField;
    
    //运输费用文本框
    MBZPKTextField *shipTextField;
    
    //显示购物车个数
    UILabel *tagLabel;
    
    //获取数据失败的view
    UIView *failView;
}

@end

@implementation SQTaoBaoDaigouVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"suqibuy 代购";
    
    flowArray = [[NSMutableArray alloc] init];
    [self initView];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectCell) name:SQ_PUSH_BUTTON_NOTIFICATION object:nil];
    [self requestProduct];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SQ_PUSH_BUTTON_NOTIFICATION object:nil];
}
#pragma mark -请求数据
// 0: taobao tianmao 1: jingdong tiantian
-(void) requestProduct {
    if ([self.url containsString:@"taobao.com"] || [self.url containsString:@"tmall.com"] ) {
        [self requestData];
    } else {
        [self requestDaigouAnalyze];
    }
}

- (void)showFailView {
    if (failView == nil)
    {
        failView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, screen_height)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, screen_width, 40)];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"分析产品数据失败，请点击下方的按钮重试";
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor colorWithHexString:gray];
        [failView addSubview:label];
        
        UIButton *databutton = [[UIButton alloc] initWithFrame:CGRectMake(screen_width/4, 150, screen_width/2, 40)];
        [databutton setTitle:@"再次分析产品信息" forState:UIControlStateNormal];
        databutton.titleLabel.font = [UIFont systemFontOfSize:14];
        [databutton setTitleColor:[UIColor colorWithHexString:text_color] forState:UIControlStateNormal];
        databutton.layer.cornerRadius = allCornerRadius;
        databutton.layer.borderColor = [UIColor colorWithHexString:gray].CGColor;
        databutton.layer.borderWidth = 1;
        [databutton addTarget:self action:@selector(requestProduct) forControlEvents:UIControlEventTouchUpInside];
        [failView addSubview:databutton];
    }
}

//test
-(void) requestDaigouAnalyze
{

    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"isBase64Encode":@"0", @"mobile_url":self.url};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_ANALYZE WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                [failView removeFromSuperview];
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    daigouModel = [SQTaoBaoDaigouModel mj_objectWithKeyValues:result];
                    
                    touchArray = [NSMutableArray arrayWithCapacity:[daigouModel.attributes count]];
                    for (NSInteger i = 0; i < [daigouModel.attributes count]; i++)
                    {
                        [touchArray addObject:@"0"];
                    }
                    [self initDataView];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                    [self showFailView];
                    [self.view addSubview:failView];
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}



-(void) requestData
{
    NSData* originData = [self.url dataUsingEncoding:NSUTF8StringEncoding];
    
    NSData* encodeData = [GTMBase64 encodeData:originData];
    
    NSString* mobile_url = [[NSString alloc] initWithData:encodeData encoding:NSUTF8StringEncoding];
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token,@"mobile_url":mobile_url};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_FETCHURL WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                [failView removeFromSuperview];
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    daigouModel = [SQTaoBaoDaigouModel mj_objectWithKeyValues:result];
                    
                    touchArray = [NSMutableArray arrayWithCapacity:[daigouModel.attributes count]];
                    for (NSInteger i = 0; i < [daigouModel.attributes count]; i++)
                    {
                        [touchArray addObject:@"0"];
                    }
                    [self initDataView];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                    [self showFailView];
                    [self.view addSubview:failView];
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark -初始化界面
-(void) initView
{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, screen_height - 50, screen_width, 50)];
    
    [self.view addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(0);
        make.right.equalTo(self.view.mas_right).with.offset(0);
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-24);
        make.height.mas_equalTo(50);
    }];
    
    button = [[UIButton alloc] init];
    [button setTitle:@"加入购物车" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.layer.cornerRadius = allCornerRadius;
    button.enabled = NO;
    button.backgroundColor = [UIColor colorWithHexString:button_disable];
    [view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(30);
        make.right.equalTo(view.mas_right).with.offset(-15);
        make.centerY.equalTo(view);
    }];
    [button addTarget:self action:@selector(addItem) forControlEvents:UIControlEventTouchUpInside];
    
    MBZPKButton *daigouButton = [[MBZPKButton alloc] init];
    UIImage *image = [UIImage imageNamed:@"cart_bar"];
    [daigouButton setImage:image forState:UIControlStateNormal];
    [view addSubview:daigouButton];
    [daigouButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(50);
        make.right.equalTo(button.mas_left).with.offset(-8);
        make.top.equalTo(view.mas_top).with.offset(0);
        make.bottom.equalTo(view.mas_bottom).with.offset(0);
    }];
    [daigouButton addTarget:self action:@selector(cart) forControlEvents:UIControlEventTouchUpInside];
    
    tagLabel = [[UILabel alloc] init];
    tagLabel.font = [UIFont systemFontOfSize:12];
    tagLabel.textAlignment = NSTextAlignmentCenter;
    tagLabel.text = @"0";
    tagLabel.textColor = [UIColor whiteColor];
    tagLabel.layer.cornerRadius = 10;
    tagLabel.layer.backgroundColor = [UIColor colorWithHexString:text_red].CGColor;
    [daigouButton addSubview:tagLabel];
    [tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(daigouButton.mas_left).with.offset(30);
        make.top.equalTo(daigouButton.mas_top).with.offset(10);
        make.width.mas_equalTo(20);
        make.height.mas_equalTo(20);
    }];
    
    //倒数第一个view
    MBZPKBaseView *shipView = [[MBZPKBaseView alloc] initWithFrame:CGRectZero];
    shipView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:shipView];
    [shipView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(0);
        make.right.equalTo(self.view.mas_right).with.offset(0);
        make.bottom.equalTo(view.mas_top).with.offset(0);
        make.height.mas_equalTo(43);
    }];
    
    UILabel *shipLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    shipLabel.font = [UIFont systemFontOfSize:14];
    shipLabel.text = @"国内运费：";
    shipLabel.textColor = [UIColor colorWithHexString:text_gray];
    [shipView addSubview:shipLabel];
    [shipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shipView.mas_left).with.offset(8);
        make.width.mas_equalTo(80);
        make.top.equalTo(shipView.mas_top).with.offset(0);
        make.bottom.equalTo(shipView.mas_bottom).with.offset(0);
    }];
    
    UILabel *shipSignLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    shipSignLabel.font = [UIFont systemFontOfSize:14];
    shipSignLabel.text = @"¥";
    shipSignLabel.textColor = [UIColor colorWithHexString:text_yellow];
    [shipView addSubview:shipSignLabel];
    [shipSignLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shipLabel.mas_right).with.offset(0);
        make.width.mas_equalTo(10);
        make.top.equalTo(shipView.mas_top).with.offset(0);
        make.bottom.equalTo(shipView.mas_bottom).with.offset(0);
    }];
    
    shipTextField = [[MBZPKTextField alloc] init];
    shipTextField.font = [UIFont systemFontOfSize:13];
    shipTextField.text = @"0.00";
    shipTextField.keyboardType = UIKeyboardTypeNumberPad;
    [shipView addSubview:shipTextField];
    [shipTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shipSignLabel.mas_right).with.offset(16);
        make.width.mas_equalTo(80);
        make.top.equalTo(shipView.mas_top).with.offset(8);
        make.bottom.equalTo(shipView.mas_bottom).with.offset(-8);
    }];
    
    UILabel *yuanLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    yuanLabel.font = [UIFont systemFontOfSize:14];
    yuanLabel.text = @"元";
    yuanLabel.textColor = [UIColor colorWithHexString:text_gray];
    [shipView addSubview:yuanLabel];
    [yuanLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shipTextField.mas_right).with.offset(8);
        make.width.mas_equalTo(20);
        make.top.equalTo(shipView.mas_top).with.offset(0);
        make.bottom.equalTo(shipView.mas_bottom).with.offset(0);
    }];
    
    //最上面的view
    priceView = [[MBZPKBaseView alloc] initWithFrame:CGRectZero];
    priceView.bottomLineHiden = YES;
    priceView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:priceView];
    [priceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(0);
        make.right.equalTo(self.view.mas_right).with.offset(0);
        make.bottom.equalTo(shipView.mas_top).with.offset(0);
        if (screen_width < 700)
        {
            make.height.mas_equalTo(70);
        }
        else
        {
            make.height.mas_equalTo(50);
        }
    }];
    
    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    priceLabel.font = [UIFont systemFontOfSize:14];
    priceLabel.text = @"商品价格：";
    priceLabel.textColor = [UIColor colorWithHexString:text_gray];
    [priceView addSubview:priceLabel];
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(priceView.mas_left).with.offset(8);
        make.top.equalTo(priceView.mas_top).with.offset(8);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
    
    priceSignLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    priceSignLabel.font = [UIFont systemFontOfSize:14];
    priceSignLabel.text = @"¥";
    priceSignLabel.textColor = [UIColor colorWithHexString:text_yellow];
    [priceView addSubview:priceSignLabel];
    [priceSignLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(shipLabel.mas_right).with.offset(0);
        make.top.equalTo(priceView.mas_top).with.offset(8);
        make.width.mas_equalTo(10);
        make.height.mas_equalTo(20);
    }];
    
    editButton = [[UIButton alloc] init];
    editButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [editButton setTitle:@"修改" forState:UIControlStateNormal];
    [editButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    editButton.backgroundColor = [UIColor colorWithHexString:button_disable];
    editButton.layer.cornerRadius = allCornerRadius;
    [editButton addTarget:self action:@selector(editPrice) forControlEvents:UIControlEventTouchUpInside];
    [priceView addSubview:editButton];
    [editButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(priceSignLabel.mas_right).with.offset(8);
        make.top.equalTo(priceView.mas_top).with.offset(8);
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(20);
    }];
    
    //确认输入修改价格文本框
    confirmButton = [[UIButton alloc] init];
    confirmButton.titleLabel.font = [UIFont systemFontOfSize:13];
    [confirmButton setTitle:@"确认" forState:UIControlStateNormal];
    [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    confirmButton.backgroundColor = [UIColor colorWithHexString:text_yellow];
    confirmButton.layer.cornerRadius = allCornerRadius;
    [confirmButton addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
    [priceView addSubview:confirmButton];
    [confirmButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(priceView.mas_right).with.offset(-8);
        make.top.equalTo(priceView.mas_top).with.offset(8);
        make.width.mas_equalTo(45);
        make.height.mas_equalTo(20);
    }];
    confirmButton.hidden = YES;
    
    //输入物品修修改价格文本框
    priceTextField = [[MBZPKTextField alloc] init];
    priceTextField.font = [UIFont systemFontOfSize:13];
    [priceView addSubview:priceTextField];
    [priceTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(confirmButton.mas_left).with.offset(-8);
        make.top.equalTo(priceView.mas_top).with.offset(8);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
    priceTextField.hidden = YES;
    
    UILabel *mindLabel = [[UILabel alloc] init];
    mindLabel.numberOfLines = 0;
    mindLabel.text = @"如果有优惠价，请输入优惠价格。实际价格不符，我们通知您补全差价。";
    mindLabel.textColor = [UIColor colorWithHexString:text_gray];
    mindLabel.font = [UIFont systemFontOfSize:12];
    [priceView addSubview:mindLabel];
    [mindLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(priceView.mas_left).with.offset(8);
        make.right.equalTo(priceView.mas_right).with.offset(-8);
        make.bottom.equalTo(priceView.mas_bottom).with.offset(0);
        make.top.equalTo(priceLabel.mas_bottom).with.offset(8);
    }];
}

#pragma mark - 修改价格
-(void) editPrice
{
    editButton.hidden = YES;
    confirmButton.hidden = NO;
    priceTextField.hidden = NO;
    
    CGFloat width = screen_width - 88 - 53 - 88 - 8;
    if (priceSignLabel.frame.size.width > width)
    {
        [priceSignLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
        }];
    }
}

#pragma mark - 确认价格
-(void) confirm
{
    priceSignLabel.text = [NSString stringWithFormat:@"¥%@",priceTextField.text];
    CGSize expect = [priceSignLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, priceSignLabel.frame.size.height)];
    CGFloat width = 0;
    if (expect.width > screen_width - 88 - 16 - 45)
    {
        width = screen_width - 88 - 16 - 45;
    }
    else
    {
        width = expect.width;
    }
    [priceSignLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(width);
    }];
    
    confirmButton.hidden = YES;
    priceTextField.text = @"";
    priceTextField.hidden = YES;
    editButton.hidden = NO;
}

#pragma mark - 初始化加载数据之后的界面
-(void) initDataView
{
    ///没属性可以点击加入购物车
    if ([daigouModel.attributes count] <=0)
    {
        button.enabled = YES;
        button.backgroundColor = [UIColor colorWithHexString:text_yellow];
    }
    
    if (daigouModel.selected_price > 0 && daigouModel.skus.count == 0) {
        priceSignLabel.text = [NSString stringWithFormat:@"¥%@", daigouModel.selected_price];
        CGSize expect = [priceSignLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, priceSignLabel.frame.size.height)];
        CGFloat width = 0;
        if (expect.width > screen_width - 88 - 16 - 45)
        {
            width = screen_width - 88 - 16 - 45;
        }
        else
        {
            width = expect.width;
        }
        [priceSignLabel mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.mas_equalTo(width);
        }];

        
    }

    UIScrollView *scroll = [[UIScrollView alloc] init];
    scroll.bounces = NO;
    [self.view addSubview:scroll];
    [scroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view.mas_left).with.offset(0);
        make.top.equalTo(self.view.mas_top).with.offset(0);
        make.bottom.equalTo(priceView.mas_top).with.offset(0);
        make.width.mas_equalTo(screen_width);
    }];
    
    UIView *topView = [[UIView alloc] init];
    [scroll addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(scroll.mas_left).with.offset(0);
        make.top.equalTo(scroll.mas_top).with.offset(0);
        make.width.mas_equalTo(screen_width);
        make.height.mas_equalTo(100);
    }];
    
    UIImageView *imageView = [[UIImageView alloc] init];
    [imageView sd_setImageWithURL:[NSURL URLWithString:daigouModel.mainpic] placeholderImage:nil];
    [topView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(topView.mas_left).with.offset(8);
        make.width.mas_equalTo(84);
        make.height.mas_equalTo(84);
        make.centerY.equalTo(topView);
    }];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = daigouModel.ptitle;
    titleLabel.textColor = [UIColor colorWithHexString:text_gray];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.numberOfLines = 0;
    [topView addSubview:titleLabel];
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView.mas_right).with.offset(8);
        make.top.equalTo(topView.mas_top).with.offset(8);
        make.right.equalTo(topView.mas_right).with.offset(-8);
        make.height.mas_equalTo(40);
    }];
    
    skuIdLabel = [[UILabel alloc] init];
    skuIdLabel.textColor = [UIColor colorWithHexString:text_gray];
    skuIdLabel.font = [UIFont systemFontOfSize:14];
    [topView addSubview:skuIdLabel];
    [skuIdLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView.mas_right).with.offset(8);
        make.top.equalTo(titleLabel.mas_bottom).with.offset(0);
        make.right.equalTo(topView.mas_right).with.offset(-8);
        make.height.mas_equalTo(14);
    }];
    
    attributeLabel = [[UILabel alloc] init];
    attributeLabel.numberOfLines = 0;
    attributeLabel.textColor = [UIColor colorWithHexString:text_gray];
    attributeLabel.font = [UIFont systemFontOfSize:14];
    [topView addSubview:attributeLabel];
    [attributeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageView.mas_right).with.offset(8);
        make.top.equalTo(skuIdLabel.mas_bottom).with.offset(0);
        make.right.equalTo(topView.mas_right).with.offset(-8);
        make.bottom.equalTo(topView.mas_bottom).with.offset(0);
    }];
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = [UIColor whiteColor];
    [scroll addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(scroll.mas_left).with.offset(0);
        make.top.equalTo(topView.mas_bottom).with.offset(0);
        make.width.mas_equalTo(screen_width);
        make.height.mas_equalTo(900);
    }];
    
    for (NSInteger i = 0; i < [daigouModel.attributes count]; i++)
    {
        SQFlowButtonView *flowView = [[SQFlowButtonView alloc] initWithModel:daigouModel.attributes[i]];
        flowView.backgroundColor = [UIColor whiteColor];
        [bottomView addSubview:flowView];
        [flowView mas_makeConstraints:^(MASConstraintMaker *make) {
            if (i == 0)
            {
                make.top.equalTo(bottomView.mas_top).with.offset(25);
            }
            else
            {
                SQFlowButtonView *lastFlowView = flowArray[i-1];
                make.top.equalTo(lastFlowView.mas_bottom).with.offset(25);
            }
            make.left.equalTo(bottomView.mas_left).with.offset(0);
            make.right.equalTo(bottomView.mas_right).with.offset(0);
            make.height.mas_equalTo(flowView.height);
            [flowArray addObject:flowView];
        }];
        
        [flowView initFlow];
    }
    
    UIView *quantityView = [[UIView alloc] init];
    quantityView.backgroundColor = [UIColor whiteColor];
    [bottomView addSubview:quantityView];
    [quantityView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bottomView.mas_left).with.offset(0);
        if ([daigouModel.attributes count] <1)
        {
            make.top.equalTo(bottomView.mas_top).with.offset(0);
        }
        else
        {
            SQFlowButtonView *flow = flowArray[[daigouModel.attributes count]-1];
            make.top.equalTo(flow.mas_bottom).with.offset(20);
        }
        make.right.equalTo(bottomView.mas_right).with.offset(0);
        make.height.mas_equalTo(45);
    }];
    
    UILabel *quantityLabel = [[UILabel alloc] init];
    quantityLabel.text = @"数量:";
    quantityLabel.textColor = [UIColor colorWithHexString:text_gray];
    quantityLabel.font = [UIFont systemFontOfSize:14];
    [quantityView addSubview:quantityLabel];
    [quantityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(quantityView.mas_left).with.offset(8);
        make.width.mas_equalTo(40);
        make.height.mas_equalTo(25);
        make.centerY.equalTo(quantityView);
    }];
    
    UIButton *subButton = [[UIButton alloc] init];
    subButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [subButton setTitle:@"-" forState:UIControlStateNormal];
    subButton.layer.borderWidth = 1;
    subButton.layer.cornerRadius = allCornerRadius;
    subButton.layer.borderColor = [UIColor colorWithHexString:border_color].CGColor;
    [subButton setTitleColor:[UIColor colorWithHexString:text_gray] forState:UIControlStateNormal];
    [subButton addTarget:self action:@selector(sub) forControlEvents:UIControlEventTouchUpInside];
    [quantityView addSubview:subButton];
    [subButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(quantityLabel.mas_right).with.offset(0);
        make.centerY.equalTo(quantityView);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(25);
    }];
    
    quantityTextField = [[UITextField alloc] init];
    quantityTextField.text = @"1";
    quantityTextField.font = [UIFont systemFontOfSize:13];
    quantityTextField.layer.borderWidth = 1;
    quantityTextField.layer.borderColor = [UIColor colorWithHexString:text_gray].CGColor;
    quantityTextField.layer.cornerRadius = allCornerRadius;
    quantityTextField.textAlignment = NSTextAlignmentCenter;
    quantityTextField.keyboardType = UIKeyboardTypeNumberPad;
    [quantityView addSubview:quantityTextField];
    [quantityTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(subButton.mas_right).with.offset(5);
        make.centerY.equalTo(quantityView);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(80);
    }];
    
    UIButton *addButton = [[UIButton alloc] init];
    addButton.titleLabel.font = [UIFont systemFontOfSize:12];
    [addButton setTitle:@"+" forState:UIControlStateNormal];
    addButton.layer.borderWidth = 1;
    addButton.layer.cornerRadius = allCornerRadius;
    addButton.layer.borderColor = [UIColor colorWithHexString:border_color].CGColor;
    [addButton setTitleColor:[UIColor colorWithHexString:text_gray] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(add) forControlEvents:UIControlEventTouchUpInside];
    [quantityView addSubview:addButton];
    [addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(quantityTextField.mas_right).with.offset(5);
        make.centerY.equalTo(quantityView);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(25);
    }];
    
    CGFloat height = 0;
    for (NSInteger i = 0; i < [flowArray count]; i++)
    {
        SQFlowButtonView *flow = flowArray[i];
        height += flow.height + 25;
    }
    
    [bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(height + 20 + 45);
    }];
    
    [scroll setContentOffset:CGPointMake(0, 0)];
    [scroll setContentSize:CGSizeMake(screen_width, 100 + height + 65)];
    
    if ([daigouModel.count integerValue] > 0)
    {
        tagLabel.text = daigouModel.count;
    }
}

#pragma mark - 点按collectionviewcell
-(void) selectCell
{
    for (NSInteger i = 0; i < [daigouModel.attributes count]; i++)
    {
        SQTaoBaoAttributesModel *model = daigouModel.attributes[i];
        for (NSInteger j = 0; j < [model.items count] ; j++)
        {
            SQTaoBaoItemsModel *item = model.items[j];
            if (item.is_selected == YES)
            {
                touchArray[i] = @"1";
                break;
            }
        }
    }
    
    BOOL allSelect = NO;
    for (NSInteger i = 0; i < [daigouModel.attributes count]; i++)
    {
        if ([touchArray[i] isEqualToString:@"0"])
        {
            break;
        }
        else
        {
            if (i == [daigouModel.attributes count] - 1)
            {
                allSelect = YES;
            }
        }
    }
    
    if (allSelect)
    {
        button.enabled = YES;
        button.backgroundColor = [UIColor colorWithHexString:text_yellow];
        
        [self setSkusID];
    }
    
    [self setAttributeLabel];
}

#pragma mark - 设置属性label
-(void) setAttributeLabel
{
    NSMutableString *attribute = [[NSMutableString alloc] init];
    for (NSInteger i = 0; i < [flowArray count]; i++)
    {
        SQFlowButtonView *flow = flowArray[i];
        SQTaoBaoItemsModel *model = flow.selectItem;
        if (model != nil)
        {
            [attribute appendString:[NSString stringWithFormat:@"%@:%@;",flow.model.label,model.name]];
        }
    }
    [attribute deleteCharactersInRange:NSMakeRange(attribute.length - 1, 1)];
    
    attributeLabel.text = attribute;
}

#pragma mark - 设置skusID
-(void) setSkusID
{
    NSMutableString *key = [[NSMutableString alloc] init];
    for (NSInteger i = 0; i < [flowArray count]; i++)
    {
        SQFlowButtonView *flow = flowArray[i];
        SQTaoBaoItemsModel *model = flow.selectItem;
        [key appendFormat:@";%@",model.key];
    }
    [key appendString:@";"];
    
    for (NSInteger i = 0; i < [daigouModel.skus count]; i++)
    {
        SQTaoBaoSkusModel *model = daigouModel.skus[i];
        if ([key isEqualToString:model.key])
        {
            skuIdLabel.text = model.skuId;
            priceSignLabel.text = [NSString stringWithFormat:@"¥%@",model.price];
            CGSize expect = [priceSignLabel sizeThatFits:CGSizeMake(CGFLOAT_MAX, priceSignLabel.frame.size.height)];
            CGFloat width = 0;
            if (expect.width > screen_width - 88 - 16 - 45)
            {
                width = screen_width - 88 - 16 - 45;
            }
            else
            {
                width = expect.width;
            }
            [priceSignLabel mas_updateConstraints:^(MASConstraintMaker *make) {
                make.width.mas_equalTo(width);
            }];
            break;
        }
    }
}

#pragma mark - sub减少数量
-(void) sub
{
    NSInteger count = [quantityTextField.text integerValue];
    if (count == 1)
    {
        count = 1;
    }
    else
    {
        quantityTextField.text = [NSString stringWithFormat:@"%d",count-1];
    }
}

#pragma mark - add增加数量
-(void) add
{
    NSInteger count = [quantityTextField.text integerValue];
    quantityTextField.text = [NSString stringWithFormat:@"%d",count+1];
    
}

#pragma mark - 进入购物车
-(void) cart
{
    SQDaigouShopController *controller = [[SQDaigouShopController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 添加物品到购物车
-(void) addItem
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    NSString *user_token = [SQUser loadmodel].user_token;
    [dic setObject:user_token forKey:@"user_token"];
    
    if (daigouModel.url.length != 0)
    {
        [dic setObject:daigouModel.url forKey:@"item_url"];
    }
    if (daigouModel.ptitle.length != 0)
    {
        [dic setObject:daigouModel.ptitle forKey:@"item_name"];
    }
    if (skuIdLabel.text.length != 0)
    {
        [dic setObject:skuIdLabel.text forKey:@"item_sku"];
    }
    if (daigouModel.shop_name.length != 0)
    {
        [dic setObject:daigouModel.shop_name forKey:@"item_store"];
    }
    
    if (daigouModel.mainpic.length != 0)
    {
        [dic setObject:daigouModel.mainpic forKey:@"item_image"];
    }
    
    
    if ([quantityTextField.text integerValue] > 0)
    {
        [dic setObject:quantityTextField.text forKey:@"item_qty"];
    }
    else
    {
        showDefaultPromptTextHUD(@"请输入大于0的数量");
    }
    
    NSMutableString *price = [[NSMutableString alloc] initWithString:priceSignLabel.text];
    [price deleteCharactersInRange:NSMakeRange(0, 1)];
    
    if (price.length != 0)
    {
        [dic setObject:price forKey:@"item_purchasing_price"];
    }
    
    if (shipTextField.text.length != 0)
    {
        [dic setObject:shipTextField.text forKey:@"item_shipping_price"];
    }
    
    NSString *attributes = attributeLabel.text;
    if (attributes.length != 0)
    {
        [dic setObject:attributes forKey:@"item_attributes"];
    }
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_ADDCART WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提醒" message:@"商品添加成功！\n您是否继续添加新的商品？" delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
                    [alert show];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark - alertviewdelegate
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        UINavigationController *navi = self.navigationController;
        SQDaigouShopController *controller = [[SQDaigouShopController alloc] init];
        [navi pushViewController:controller animated:YES];
        
        NSMutableArray *array = [NSMutableArray arrayWithArray:navi.childViewControllers];
        [array removeObjectAtIndex:[array count] - 2];
        [navi setViewControllers:array animated:NO];
    }
}
@end
