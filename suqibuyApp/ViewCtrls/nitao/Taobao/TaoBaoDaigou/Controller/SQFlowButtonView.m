//
//  SQFlowButtonView.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/6/13.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQFlowButtonView.h"
#import "SQFlowCell.h"
#import "SQTaobaoFlowLayout.h"

static NSString *identifier = @"SQFlowCell";

@interface SQFlowButtonView()
{
    SQTaobaoFlowLayout *flowLayout;
}

@end

@implementation SQFlowButtonView

-(instancetype) initWithModel:(SQTaoBaoAttributesModel *) model;
{
    self = [super init];
    if (self)
    {
        self.model = model;
        
        //初始化非选中模式
        for (NSInteger i = 0; i < [self.model.items count]; i++)
        {
            SQTaoBaoItemsModel *item = self.model.items[i];
            item.is_selected = NO;
        }
        
        flowLayout = [[SQTaobaoFlowLayout alloc] init];
        flowLayout.modelList = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < [self.model.items count]; i++)
        {
            SQTaoBaoItemsModel *item = self.model.items[i];
            
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
            label.text = [NSString stringWithFormat:@"%@:",item.name];
            label.font = [UIFont systemFontOfSize:14];
            label.numberOfLines = 1;
            CGSize expect = [label sizeThatFits:CGSizeMake(MAXFLOAT, 34)];
            
            SQTaoBaoFlowLayoutModel *model = [[SQTaoBaoFlowLayoutModel alloc] init];
            model.worksHeight = 50;
            if (item.icon.length == 0)
            {
                model.worksWidth = expect.width + 16;
            }
            else
            {
                CGFloat width = expect.width + 8 * 3 + 34;
                model.worksWidth = width;
            }
            
            if (model.worksWidth <= model.worksHeight)
            {
                model.worksWidth = model.worksHeight;
            }
            
            [flowLayout.modelList addObject:model];
        }
        
        NSInteger rowCount = 0;
        CGFloat collectWidth = screen_width - 88;
        CGFloat X = 0;
        for (NSInteger i = 0; i < [flowLayout.modelList count]; i++)
        {
            // 创建路径, 记录item 所在分区分区和位置
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
            // 根据路径创建对应的布局属性
            UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            SQTaoBaoFlowLayoutModel *model = flowLayout.modelList[i];
            if (X + model.worksWidth + 8 > collectWidth)
            {
                rowCount++;
                X = 0;
                attributes.frame = CGRectMake(X, rowCount * 58, model.worksWidth, 50);
            }
            else
            {
                attributes.frame = CGRectMake(X, rowCount * 58, model.worksWidth, 50);
            }
            X = X + model.worksWidth + 8;
        }
        
        self.height = (rowCount+1) * 50 + rowCount * 8;
    }
    return self;
}

-(void)initFlow
{
    UILabel *label = [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@"%@:",self.model.label];
    label.textColor = [UIColor colorWithHexString:text_gray];
    label.font = [UIFont systemFontOfSize:14];
    [self addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(8);
        make.top.equalTo(self).with.offset(8);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(25);
    }];
    
    UICollectionView *collect = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
    collect.backgroundColor = [UIColor whiteColor];
    collect.scrollEnabled = NO;
    UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
    [collect registerNib:nib forCellWithReuseIdentifier:identifier];
    collect.dataSource = self;
    collect.delegate = self;
    [self addSubview:collect];
    [collect mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label.mas_right).with.offset(0);
        make.right.equalTo(self).with.offset(0);
        make.top.equalTo(self).with.offset(0);
        make.bottom.equalTo(self).with.offset(0);
    }];
    
    [collect reloadData];
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.model.items count];
}

-(UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SQFlowCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    SQTaoBaoItemsModel *item = self.model.items[indexPath.row];
    
    cell.label.text = item.name;
    
    if (item.icon.length == 0)
    {
        cell.picImageView.hidden = YES;
        cell.imageWidth.constant = 0;
        cell.labelLEFT.constant = 0;
    }
    else
    {
        [cell.picImageView sd_setImageWithURL:[NSURL URLWithString:item.icon]placeholderImage:nil];
        cell.picImageView.hidden = NO;
        cell.imageWidth.constant = 34;
        cell.labelLEFT.constant = 8;
    }
    
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    for (NSInteger i = 0; i < [self.model.items count]; i++)
    {
        SQTaoBaoItemsModel *item = self.model.items[i];
        item.is_selected = NO;
    }
    
    SQTaoBaoItemsModel *item = self.model.items[indexPath.row];
    self.selectItem = item;
    item.is_selected = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SQ_PUSH_BUTTON_NOTIFICATION object:nil];
}
@end
