//
//  SQTaobaoFlowLayout.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQTaobaoFlowLayout.h"

#define SQFLOWLAYOUT_SPACE 8
#define SQFLOWLAYOUT_HEIGHT 50
@interface SQTaobaoFlowLayout ()

// 所有item的属性的数组
@property (nonatomic, strong) NSMutableArray *layoutAttributesArray;  // 定义布局属性的数组, 用来存放布局item 的属性

@end

@implementation SQTaobaoFlowLayout
/**
 *  布局准备方法 当collectionView的布局发生变化时 会被调用
 *  通常是做布局的准备工作 itemSize.....
 *  UICollectionView 的 contentSize 是根据 itemSize 动态计算出来的
 */

- (void)prepareLayout {
    [super prepareLayout];
    // 根据行数 计算item的高度 所有item 的高度是一样的
    [self computeAttributesWithItemWith:50];
}


/**
 根据itemHeight 计算布局属性
 */
- (void)computeAttributesWithItemWith:(CGFloat)itemHeight
{
    self.rowCount = 0;
    self.layoutAttributesArray = [[NSMutableArray alloc] init];
    CGFloat collectWidth = self.collectionView.frame.size.width;
    CGFloat X = 0;
    for (NSInteger i = 0; i < [self.modelList count]; i++)
    {
        // 创建路径, 记录item 所在分区分区和位置
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        // 根据路径创建对应的布局属性
        UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
        SQTaoBaoFlowLayoutModel *model = self.modelList[i];
        if (X + model.worksWidth + SQFLOWLAYOUT_SPACE > collectWidth)
        {
            self.rowCount++;
            X = 0;
            
        }
        attributes.frame = CGRectMake(X, self.rowCount * (SQFLOWLAYOUT_HEIGHT +SQFLOWLAYOUT_SPACE), model.worksWidth, SQFLOWLAYOUT_HEIGHT);
        X = X + model.worksWidth + SQFLOWLAYOUT_SPACE;
        [self.layoutAttributesArray addObject:attributes];
    }
}

/**
 *  跟踪效果：当到达要显示的区域时 会计算所有显示item的属性
 *           一旦计算完成 所有的属性会被缓存 不会再次计算
 *  @return 返回布局属性(UICollectionViewLayoutAttributes)数组
 */
- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    // 直接返回计算好的布局属性数组
    return self.layoutAttributesArray;
}

// 重写此方法, 改变collectionView的contentSize
- (CGSize )collectionViewContentSize{
    return CGSizeMake(self.rowCount * SQFLOWLAYOUT_HEIGHT, self.collectionView.bounds.size.height);
}

@end
