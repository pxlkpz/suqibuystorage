//
//  SQTaobaoVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//  淘宝 OR 天猫

#import "SQTaobaoVC.h"
#import "MBZPKButton.h"

#import "SQWoHelpVC.h"
#import "AppDelegate.h"
#import "SQFillShopList.h"
#import "SQTaoBaoDaigouVC.h"
#import "suqibuyApp-Swift.h"
@interface SQTaobaoVC ()<UIWebViewDelegate>
{
    __weak IBOutlet UIWebView *taobaoWebView;
    

    __weak IBOutlet UIButton *helpButton;
    
    __weak IBOutlet UIButton *cartButton;
    __weak IBOutlet UIButton *buyButton;
    
    //显示购物车个数
    UILabel *tagLabel;
}

@end

@implementation SQTaobaoVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title =  @"suqibuy 代购";
    
    NSURL *url = nil;
    if (self.type == 0)
    {
        url = [NSURL URLWithString:@"https://h5.m.taobao.com/"];
    }
    else if (self.type == 1)

    {
        url = [NSURL URLWithString:@"https://m.tmall.com"];
    }
    else if (self.type == 2)
        
    {
        url = [NSURL URLWithString:@"https://www.jd.com"];
    }else if (self.type == 3)
        
    {
        url = [NSURL URLWithString:@"http://www.dangdang.com"];
    }
    
    [buyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buyButton.backgroundColor = [UIColor colorWithHexString:button_disable];
    buyButton.layer.cornerRadius = allCornerRadius;
    buyButton.enabled = NO;
    [buyButton addTarget:self action:@selector(daigou) forControlEvents:UIControlEventTouchUpInside];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [taobaoWebView loadRequest:request];
    taobaoWebView.delegate = self;
    
    

    
    [helpButton addTarget:self action:@selector(help) forControlEvents:UIControlEventTouchUpInside];

    [cartButton addTarget:self action:@selector(cart) forControlEvents:UIControlEventTouchUpInside];
    
    tagLabel = [[UILabel alloc] init];
    tagLabel.font = [UIFont systemFontOfSize:8];
    tagLabel.textAlignment = NSTextAlignmentCenter;
    tagLabel.text = @"0";
    tagLabel.textColor = [UIColor whiteColor];
    tagLabel.layer.cornerRadius = 5;
    tagLabel.layer.backgroundColor = [UIColor colorWithHexString:@"FF6702"].CGColor;
    
    [cartButton addSubview:tagLabel];
    [tagLabel mas_makeConstraints:^(MASConstraintMaker *make) {
    
        make.left.equalTo(cartButton.mas_left).with.offset(cartButton.imageView.frame.origin.x+10);
        make.top.equalTo(cartButton.mas_top).with.offset(cartButton.imageView.frame.origin.y-5);
        make.width.mas_equalTo(10);
        make.height.mas_equalTo(10);
    }];
    
    [self requestData];
    
}

-(void) backToLastWebView
{
    [taobaoWebView goBack];
}

-(void) requestData
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_CARTCOUNT WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    id c = [result objectForKey:@"c"];
                    if (c != nil && c != [NSNull null] && [c isKindOfClass:[NSString class]])
                    {
                        if ([c integerValue] > 0)
                        {
                            tagLabel.text = [NSString stringWithFormat:@"%@",c];
                        }
                    }
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
    } WithFailedBlock:^{
        [self hideLoadingHUD];
    }];
}



#pragma mark - uiwebview 完成加载
-(void) webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"%@",webView.request.URL.description);
    NSString *urlString = webView.request.URL.description;
    if (urlString != nil && [urlString containsString:@"taobao.com"] && [urlString containsString:@"detail.htm"])
    {
        buyButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
        buyButton.enabled = YES;
    }
    else if(urlString != nil && [urlString containsString:@"tmall.com"] && [urlString containsString:@"id="])
    {
        buyButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
        buyButton.enabled = YES;
    }
    else if(urlString != nil && [urlString containsString:@"jd"] && ([urlString containsString:@"item"]))
    {
        buyButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
        buyButton.enabled = YES;
    }
    else if(urlString != nil && [urlString containsString:@"dangdang.com"] && [urlString containsString:@"product"])
    {
        buyButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
        buyButton.enabled = YES;
    }
    else
    {
        buyButton.backgroundColor = [UIColor colorWithHexString:button_disable];
        buyButton.enabled = NO;
    }
    
    //隐藏淘宝购物车
    if (urlString != nil && [urlString containsString:@"taobao.com"] && [urlString containsString:@"detail.htm"])
    {
        NSString *fun2 = @"javascript:function hideOther() {document.querySelector('.bottom_bar').style.display='none'; }";
        [webView stringByEvaluatingJavaScriptFromString:fun2];
        [webView stringByEvaluatingJavaScriptFromString:@"javascript:hideOther();"];
    }
    else if(urlString != nil && [urlString containsString:@"tmall.com"] && [urlString containsString:@"id="])
    {
        NSString *fun2 = @"javascript:function hideOther() {document.getElementById('s-actionBar-container').style.display='none'; }";
        [webView stringByEvaluatingJavaScriptFromString:fun2];
        [webView stringByEvaluatingJavaScriptFromString:@"javascript:hideOther();"];
    }else if(urlString != nil && [urlString containsString:@"jd"] && [urlString containsString:@"item"])
    {
        NSString *fun2 = @"javascript:function hideOther() {document.getElementById('cart1').style.display='none'; }";
        [webView stringByEvaluatingJavaScriptFromString:fun2];
        [webView stringByEvaluatingJavaScriptFromString:@"javascript:hideOther();"];
    }else if(urlString != nil && [urlString containsString:@"dangdang.com"] && [urlString containsString:@"product"])
    {
        NSString *fun2 = @"javascript:function hideOther() {document.querySelector('.shopping_cart').style.display='none'; }";
        [webView stringByEvaluatingJavaScriptFromString:fun2];
        [webView stringByEvaluatingJavaScriptFromString:@"javascript:hideOther();"];
    }

}

#pragma mark - 进入帮助中心
-(void) help
{
    SQCustomServiceController *controller = [[SQCustomServiceController alloc] init];
    controller.title = @"在线客服";
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 进入购物车
-(void) cart
{
    SQDaigouShopController *controller = [[SQDaigouShopController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 我要代购
-(void) daigou
{
    SQTaoBaoDaigouVC *controller = [[SQTaoBaoDaigouVC alloc] init];
    controller.url = taobaoWebView.request.URL.description;
    [self.navigationController pushViewController:controller animated:YES];
}
@end
