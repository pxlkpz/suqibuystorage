//
//  SQnitaoVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQnitaoVC.h"

#import "SQTaobaoVC.h"
#import "UITabBar+PxlBadge.h"
#import "suqibuyApp-Swift.h"

#import "PxlCommonViewCreate.h"

@interface SQnitaoVC ()<PxlCommonViewCreateDelegate>

@property (nonatomic, strong) PxlCommonViewCreate *pxlTool;

@end

@implementation SQnitaoVC



- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"代购";

    NSString *count = [SQUser loadmodel].msg_count;
    if (count.length != 0 && [count integerValue] > 0)
    {
        [self.tabBarController.tabBar showBadgeOnItemIndex:3];
    }
    _pxlTool = [[PxlCommonViewCreate alloc] init];
    [_pxlTool pxl_buildUI:[PxlCommonViewCreate pxl_getPlistData:@"SQnitaoUIData"] andSuperView:self.view];
    _pxlTool.delegate = self;
}


-(void)pxl_CommonViewCreateDelegate:(NSString *)pushString{
    if ([pushString isEqualToString:@"SQTaobaoVC0"]) {
        SQTaobaoVC *controller = [[SQTaobaoVC alloc] init];
        controller.type = 0;
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
        
    }else if([pushString isEqualToString:@"SQTaobaoVC1"]){
        SQTaobaoVC *controller = [[SQTaobaoVC alloc] init];
        controller.type = 1;
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
        
    }else if([pushString isEqualToString:@"MBSingleWebViewController"]){
        ShipguideViewController *controller = [[ShipguideViewController alloc] init];
//        controller.type = 1;
        controller.title = @"代购物品限制";
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
    
    else{
        UIViewController *controller = [[NSClassFromString(pushString) alloc] init];
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
    
}


@end
