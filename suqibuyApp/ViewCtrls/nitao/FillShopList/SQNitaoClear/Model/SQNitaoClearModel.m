//
//  SQNitaoClearModel.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/25.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQNitaoClearModel.h"

@implementation SQNitaoClearModel

-(instancetype)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self)
    {
        self.total_qty = [self testNSStringInDic:dic string:@"total_qty"];
        self.sub_total = [self testNSStringInDic:dic string:@"sub_total"];
        self.is_has_daigou_fee = [self testNSStringInDic:dic string:@"is_has_daigou_fee"];
        self.agency_fee = [self testNSStringInDic:dic string:@"agency_fee"];
        self.daigoufee_msg = [self testNSStringInDic:dic string:@"daigoufee_msg"];
        self.coupon_code = [self testNSStringInDic:dic string:@"coupon_code"];
        self.coupon_discount_amount = [self testNSStringInDic:dic string:@"coupon_discount_amount"];
        self.coupon_msg = [self testNSStringInDic:dic string:@"coupon_msg"];
        self.credits_point_total = [self testNSStringInDic:dic string:@"credits_point_total"];
        self.credits_amount = [self testNSStringInDic:dic string:@"credits_amount"];
        self.credit_msg = [self testNSStringInDic:dic string:@"credit_msg"];
        self.grand_total = [self testNSStringInDic:dic string:@"grand_total"];
        self.shipping_total = [self testNSStringInDic:dic string:@"shipping_total"];
    }
    return self;
}
@end
