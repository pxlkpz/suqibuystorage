//
//  SQNitaoClearModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/25.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "MBZPKBaseModel.h"

@interface SQNitaoClearModel : MBZPKBaseModel

///商品总件数
@property (strong, nonatomic) NSString *total_qty;

///代付金额
@property (strong, nonatomic) NSString *sub_total;
@property (strong, nonatomic) NSString *is_has_daigou_fee;

///操作费
@property (strong, nonatomic) NSString *agency_fee;
///操作费留言
@property (strong, nonatomic) NSString *daigoufee_msg;

///优惠券代码
@property (strong, nonatomic) NSString *coupon_code;

///优惠券减负
@property (strong, nonatomic) NSString *coupon_discount_amount;
@property (strong, nonatomic) NSString *coupon_msg;
@property (strong, nonatomic) NSString *credits_point_total;

///剩余积分
@property (strong, nonatomic) NSString *credits_amount;
///积分留言
@property (strong, nonatomic) NSString *credit_msg;

///总金额
@property (strong, nonatomic) NSString *grand_total;

///运输费
@property (strong, nonatomic) NSString *shipping_total;

-(instancetype) initWithDic:(NSDictionary *) dic;
@end
