//
//  SQNitaoClearVC.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/24.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"

@interface SQNitaoClearVC : SQBaseViewController

@property (nonatomic, strong) NSDictionary* requestDic;

@end
