//
//  SQNitaoClearVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/24.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQNitaoClearVC.h"
#import "SQNitaoClearModel.h"
#import "SQShopListItemModel.h"
#import "SQFillShopListCell.h"
#import "SQzhuanyunYouhuiquan.h"

#import "MBZPKBaseView.h"
#import "MBZPKTextView.h"

#import "SQPayOrderVC.h"
#import "MBZPKTextField.h"
#import "PxlTextView.h"

static NSString *identifier = @"SQFillShopListCell";
static NSString *feedbackPlaceholder = @"若您对订单有特殊需求，请在这里备注";

@interface SQNitaoClearVC ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UITextViewDelegate>
{
    __weak IBOutlet NSLayoutConstraint *viewWidth;
    __weak IBOutlet NSLayoutConstraint *viewHeight;
    __weak IBOutlet NSLayoutConstraint *listTableViewHeight;
    
    __weak IBOutlet UIScrollView *bigScroll;
    __weak IBOutlet UITableView *listTableView;
    __weak IBOutlet UILabel *total_qtyLabel;
    __weak IBOutlet UILabel *subtotalLabel;
    __weak IBOutlet UILabel *daigoufee_msgLabel;
    __weak IBOutlet UILabel *agency_feeLabel;
    __weak IBOutlet UILabel *coupon_codeLabel;
    __weak IBOutlet UILabel *coupon_discount_amountLabel;
    __weak IBOutlet UILabel *credit_msgLabel;
    __weak IBOutlet UILabel *credit_discountLabel;
    __weak IBOutlet MBZPKTextField *creditsTextField;
    __weak IBOutlet MBZPKTextView *remarkTextView;
    
    
    __weak IBOutlet UILabel *totalCostLabel;
    __weak IBOutlet UILabel *expressCostLabel;
    __weak IBOutlet UIButton *clearButton;
    __weak IBOutlet NSLayoutConstraint *clearButtonWidth;
    
    
    __weak IBOutlet MBZPKBaseView *daifuMoneyBaseView;
    __weak IBOutlet MBZPKBaseView *operayionFeeBaseView;
    __weak IBOutlet MBZPKBaseView *useCouponView;
    __weak IBOutlet MBZPKBaseView *useCreditsBaseView;
    
    SQNitaoClearModel *clearModel;
    NSMutableArray *itemArray;
    
    NSMutableArray *itemRowArray;
    NSMutableArray *itemHeaderArray;
}

@end

@implementation SQNitaoClearVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"代购订单结算";
    daifuMoneyBaseView.topLineHiden = YES;
    operayionFeeBaseView.topLineHiden =YES;
    useCouponView.topLineHiden = YES;
    useCreditsBaseView.topLineHiden = YES;
    
    bigScroll.bounces = NO;
    
    
    [clearButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [clearButton setBackgroundColor:[UIColor colorWithHexString:text_color]];
    clearButton.enabled = NO;
    
    clearButtonWidth.constant = screen_width/4;
    
    viewWidth.constant = screen_width;
    
    listTableView.scrollEnabled = NO;
    listTableView.dataSource = self;
    listTableView.delegate = self;
    listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIView *view =[ [UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [listTableView setTableFooterView:view];
    UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
    [listTableView registerNib:nib forCellReuseIdentifier:identifier];
    
    if (iPhone6P_D) {
        if ([listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
    
    
    
    creditsTextField.layer.borderWidth = 1;
    creditsTextField.layer.borderColor = [UIColor colorWithHexString:text_gray].CGColor;
    creditsTextField.layer.cornerRadius = allCornerRadius;
    creditsTextField.keyboardType =UIKeyboardTypeDecimalPad;
    creditsTextField.delegate = self;
    
    remarkTextView.delegate = self;
    remarkTextView.text = feedbackPlaceholder;
    remarkTextView.textColor = [UIColor colorWithHexString:text_gray];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(useCoupon)];
    [useCouponView addGestureRecognizer:tap];
    
    itemArray = [[NSMutableArray alloc] init];
    itemHeaderArray = [[NSMutableArray alloc] init];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self requestData];
    
}

#pragma mark - 私有方法

+(NSString *)stringTOjson:(id)temps   //把字典和数组转换成json字符串
{
    NSData* jsonData =[NSJSONSerialization dataWithJSONObject:temps
                                                      options:NSJSONWritingPrettyPrinted error:nil];
    NSString *strs=[[NSString alloc] initWithData:jsonData
                                         encoding:NSUTF8StringEncoding];
    return strs;
}
#pragma mark -请求数据
-(void) requestData
{
    NSString *user_token = [SQUser loadmodel].user_token;
//    [_requestDic setValue:user_token forKey:@"user_token"];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:_requestDic];
    [dic addEntriesFromDictionary:@{@"user_token":user_token}];
//    NSDictionary *dic = @{@"user_token":user_token, @"ids[]":[@[@8] mj_JSONString] };
//    NSArray*sss = @[@1];
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_QUOTE WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    //                    clearModel = [[SQNitaoClearModel alloc] initWithDic:result];
                    clearModel = [SQNitaoClearModel mj_objectWithKeyValues:result];
                    [itemArray removeAllObjects];
                    [itemHeaderArray removeAllObjects];
                    [itemRowArray removeAllObjects];
                    
                    id cart = [result objectForKey:@"cart"];
                    if (cart != nil && cart != [NSNull null] && [cart isKindOfClass:[NSDictionary class]])
                    {
                        id items =[cart objectForKey:@"items"];
                        if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                        {
                            NSArray *array = items;
                            NSInteger count = [array count];
                            for (NSInteger i = 0; i < count; i++)
                            {
                                if (array[i] != nil && array[i] != [NSNull null] && [array[i] isKindOfClass:[NSDictionary class]])
                                {
                                    SQShopListItemModel *model = [[SQShopListItemModel alloc] initWithDic:array[i]];
                                    if (model.item_store.length != 0)
                                    {
                                        NSString *string = [model.item_store mutableCopy];
                                        [itemHeaderArray addObject:string];
                                    }
                                    [itemArray addObject:model];
                                }
                            }
                            
                            ///算出row
                            itemRowArray = [NSMutableArray arrayWithCapacity:[itemHeaderArray count]];
                            for (NSInteger i = 0; i < [itemHeaderArray count]; i ++)
                            {
                                NSMutableArray *array =[[NSMutableArray alloc] init];
                                [itemRowArray addObject:array];
                            }
                            
                            NSInteger j = 0;
                            for (NSInteger i = 0; i < [itemArray count]; i++)
                            {
                                if (i == 0)
                                {
                                    [itemRowArray[0] addObject:itemArray[0]];
                                }
                                else
                                {
                                    SQShopListItemModel *model = itemArray[i];
                                    if (model.item_store.length != 0)
                                    {
                                        j++;
                                    }
                                    [itemRowArray[j] addObject:itemArray[i]];
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [self reloadView];
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark 使用积分
-(void) useCredits
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSString *credit = creditsTextField.text;
    if (credit.length == 0)
    {
        return;
    }
    NSDictionary *dic = @{@"user_token":user_token,@"credit":credit};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_USERCREDIT WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    clearModel = [[SQNitaoClearModel alloc] initWithDic:result];
                    [itemArray removeAllObjects];
                    [itemHeaderArray removeAllObjects];
                    [itemRowArray removeAllObjects];
                    
                    id cart = [result objectForKey:@"cart"];
                    if (cart != nil && cart != [NSNull null] && [cart isKindOfClass:[NSDictionary class]])
                    {
                        id items =[cart objectForKey:@"items"];
                        if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                        {
                            NSArray *array = items;
                            NSInteger count = [array count];
                            for (NSInteger i = 0; i < count; i++)
                            {
                                if (array[i] != nil && array[i] != [NSNull null] && [array[i] isKindOfClass:[NSDictionary class]])
                                {
                                    SQShopListItemModel *model = [[SQShopListItemModel alloc] initWithDic:array[i]];
                                    if (model.item_store.length != 0)
                                    {
                                        NSString *string = [model.item_store mutableCopy];
                                        [itemHeaderArray addObject:string];
                                    }
                                    [itemArray addObject:model];
                                }
                            }
                            
                            ///算出row
                            itemRowArray = [NSMutableArray arrayWithCapacity:[itemHeaderArray count]];
                            for (NSInteger i = 0; i < [itemHeaderArray count]; i ++)
                            {
                                NSMutableArray *array =[[NSMutableArray alloc] init];
                                [itemRowArray addObject:array];
                            }
                            
                            NSInteger j = 0;
                            for (NSInteger i = 0; i < [itemArray count]; i++)
                            {
                                if (i == 0)
                                {
                                    [itemRowArray[0] addObject:itemArray[0]];
                                }
                                else
                                {
                                    SQShopListItemModel *model = itemArray[i];
                                    if (model.item_store.length != 0)
                                    {
                                        j++;
                                    }
                                    [itemRowArray[j] addObject:itemArray[i]];
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [self reloadView];
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark -使用优惠券
-(void) useCoupon
{
    NSLog(@"使用优惠券");
    SQzhuanyunYouhuiquan *controller = [[SQzhuanyunYouhuiquan alloc] init];
    controller.fromController = FromControllerNitao;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -结算
- (IBAction)clear:(id)sender
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = nil;
    if ([remarkTextView.text isEqualToString:feedbackPlaceholder] || remarkTextView.text.length == 0)
    {
        dic = @{@"user_token":user_token};
        
    }
    else
    {
        NSString *custom_remark = remarkTextView.text;
        dic = @{@"user_token":user_token,@"custom_remark":custom_remark};
    }
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_SAVE WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    ///跳转到订单支付，并且返回的话 回到代购页面
                    SQorderInformationModel *model = [[SQorderInformationModel alloc] initWithDic:result];
                    SQPayOrderVC *controller = [[SQPayOrderVC alloc] init];
                    controller.model = model;
                    [self.navigationController pushViewController:controller animated:YES];
                    UINavigationController * navi = self.navigationController;
                    NSMutableArray *array = [NSMutableArray arrayWithArray:navi.childViewControllers];
                    [array removeObjectAtIndex:[array count] -2];
                    [navi setViewControllers:array animated:NO];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [listTableView reloadData];
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark -刷新UI
-(void) reloadView
{
    listTableViewHeight.constant = 75 * [itemArray count] + 30 * [itemHeaderArray count];
    viewHeight.constant = listTableViewHeight.constant + 360;
    [bigScroll setContentSize:CGSizeMake(screen_width, viewHeight.constant)];
    
    [listTableView reloadData];
    
    ///商品数量Label
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"共 %@ 件商品",clearModel.total_qty]];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:text_color] range:NSMakeRange(0, 1)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:simple_button_yellow] range:NSMakeRange(1, clearModel.total_qty.length+2)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:text_color] range:NSMakeRange(clearModel.total_qty.length+2 + 1, 3)];
    
    [string addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:NSMakeRange(0, string.length)];
    total_qtyLabel.text = nil;
    total_qtyLabel.attributedText = string;
    
    
    ///代付金额
    subtotalLabel.text = [clearModel.sub_total gtm_stringByUnescapingFromHTML];
    
    ///操作费留言
    daigoufee_msgLabel.text = clearModel.daigoufee_msg;
    
    ///操作费
    agency_feeLabel.text = [clearModel.agency_fee gtm_stringByUnescapingFromHTML];
    
    ///使用优惠券
    if (clearModel.coupon_code.length == 0)
    {
        coupon_codeLabel.attributedText = nil;
        coupon_codeLabel.text = @"使用优惠券";
        coupon_discount_amountLabel.hidden = YES;
    }
    else
    {
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"使用优惠券 [ %@ ]",clearModel.coupon_code]];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:text_color] range:NSMakeRange(0, 6)];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:simple_button_yellow] range:NSMakeRange(6, clearModel.coupon_code.length+2)];
        [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:text_color] range:NSMakeRange(clearModel.coupon_code.length+2 + 6, 1)];
        
        [string addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, string.length)];
        coupon_codeLabel.text = nil;
        coupon_codeLabel.attributedText = string;
        coupon_discount_amountLabel.hidden = NO;
        coupon_discount_amountLabel.text = [clearModel.coupon_discount_amount gtm_stringByUnescapingFromHTML];
    }
    
    //使用积分
    credit_msgLabel.text = clearModel.credit_msg;
    if (clearModel.credits_amount.length != 0)
    {
        credit_discountLabel.text = [clearModel.credits_amount gtm_stringByUnescapingFromHTML];
    }
    else
    {
        credit_discountLabel.text = nil;
    }
    
    totalCostLabel.text = [clearModel.grand_total gtm_stringByUnescapingFromHTML];
    expressCostLabel.text = [NSString stringWithFormat:@"(含国内代付运费%@)",[clearModel.shipping_total gtm_stringByUnescapingFromHTML]];
    
    if ([itemArray count] > 0)
    {
        clearButton.enabled = YES;
        clearButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
    }
    else
    {
        clearButton.enabled = NO;
        clearButton.backgroundColor = [UIColor colorWithHexString:text_color];
    }
}

#pragma mark - textfield delegate
-(void) textFieldDidEndEditing:(UITextField *)textField
{
    if (![textField.text isEqualToString:clearModel.credits_point_total])
    {
        [self useCredits];
    }
}

#pragma mark-textview代理
/***********************************************************************
 * 方法名称：-(void)textViewDidBeginEditing:(UITextView *)textView
 * 功能描述：开始编辑文本框时，改变其文本框的占位符。
 * 输入参数：系统自带
 * 输出参数：textView.text:文本框内容
 * 返 回 值：无
 * 其它说明：无
 ***********************************************************************/
#pragma mark-开始编辑
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:feedbackPlaceholder])
    {
        textView.text = @"";
        textView.textColor = [UIColor colorWithHexString:text_color];
    }
}
/***********************************************************************
 * 方法名称：-(void)textViewDidEndEditing:(UITextView *)textView
 * 功能描述：结束编辑文本框时，根据文本框的编辑内容设置文本框的属性和提交按钮的状态
 * 输入参数：系统自带
 * 输出参数：textView.text:文本框内容
 textView.textColor:文本颜色
 [self changeButtonState]:按钮状态改变
 * 返 回 值：
 * 其它说明：
 ***********************************************************************/
#pragma mark-结束编辑
-(void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.text.length == 0)
    {
        textView.textColor = [UIColor colorWithHexString:text_gray];
        textView.text = feedbackPlaceholder;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return [PxlTextView validateNumber:string];
}



#pragma mark - tableview delegate
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [itemHeaderArray count];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemRowArray[section] count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SQFillShopListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.bottomLineLeftDistance = 0;
    cell.bottomLineHiden = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    SQShopListItemModel *model = itemRowArray[indexPath.section][indexPath.row];
    
    NSURL *url = [NSURL URLWithString:model.item_image];
    [cell.itemImageView sd_setImageWithURL:url placeholderImage:nil];
    
    cell.itemNameLabel.text = model.item_name;
    
    cell.itemPriceLabel.text = [model.item_purchasing_price gtm_stringByUnescapingFromHTML];
    
    cell.itemNumberLabel.text = [NSString stringWithFormat:@"x%@",model.item_qty];
    
    cell.itemDescriptionLabel.text = model.attr;
    
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 30)];
    UILabel *label= [[UILabel alloc] initWithFrame:CGRectMake(15, 0, screen_width, 30)];
    label.text = itemHeaderArray[section];
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor colorWithHexString:text_color];
    [view addSubview:label];
    return view;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

@end
