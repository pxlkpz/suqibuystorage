//
//  SQFillShopListCell.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBZPKTableViewCell.h"

@interface SQFillShopListCell : MBZPKTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;

@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *itemPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *itemDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *itemNumberLabel;

@end
