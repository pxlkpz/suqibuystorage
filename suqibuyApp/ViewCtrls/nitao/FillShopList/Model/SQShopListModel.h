//
//  SQShopListModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "MBZPKBaseModel.h"

@interface SQShopListModel : MBZPKBaseModel

@property (strong, nonatomic) NSString *grand_total;
@property (strong, nonatomic) NSString *shipping_total;

-(instancetype) initWithDic:(NSDictionary *) dic;
@end
