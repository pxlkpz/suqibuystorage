//
//  SQShopListItemModel.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQShopListItemModel.h"

@implementation SQShopListItemModel

-(instancetype)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self)
    {
        self.item_id = [self testNSStringInDic:dic string:@"id"];
        self.item_url = [self testNSStringInDic:dic string:@"item_url"];
        self.item_store = [self testNSStringInDic:dic string:@"item_store"];
        self.item_name = [self testNSStringInDic:dic string:@"item_name"];
        self.item_image = [self testNSStringInDic:dic string:@"item_image"];
        self.item_qty = [self testNSStringInDic:dic string:@"item_qty"];
        self.item_purchasing_price = [self testNSStringInDic:dic string:@"item_purchasing_price"];
        self.item_shipping_price = [self testNSStringInDic:dic string:@"item_shipping_price"];
        self.item_row_total = [self testNSStringInDic:dic string:@"item_row_total"];
        self.item_color = [self testNSStringInDic:dic string:@"item_color"];
        self.item_size = [self testNSStringInDic:dic string:@"item_size"];
        self.item_capacity = [self testNSStringInDic:dic string:@"item_capacity"];
        self.item_version = [self testNSStringInDic:dic string:@"item_version"];
        self.item_other = [self testNSStringInDic:dic string:@"item_other"];
        self.item_description = [self testNSStringInDic:dic string:@"item_description"];
        self.attr = [self testNSStringInDic:dic string:@"attr"];
    }
    return self;
}
@end
