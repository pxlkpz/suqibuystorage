//
//  SQShopListModel.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQShopListModel.h"

@implementation SQShopListModel

- (instancetype)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self)
    {
        self.grand_total = [self testNSStringInDic:dic string:@"grand_total"];
        self.shipping_total = [self testNSStringInDic:dic string:@"shipping_total"];
    }
    return self;
}
@end
