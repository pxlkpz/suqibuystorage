//
//  SQShopListItemModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "MBZPKBaseModel.h"

@interface SQShopListItemModel : MBZPKBaseModel

@property (strong, nonatomic) NSString *item_id;
@property (strong, nonatomic) NSString *item_url;
@property (strong, nonatomic) NSString *item_store;
@property (strong, nonatomic) NSString *item_name;
@property (strong, nonatomic) NSString *item_image;
@property (strong, nonatomic) NSString *item_qty;
@property (strong, nonatomic) NSString *item_purchasing_price;
@property (strong, nonatomic) NSString *item_shipping_price;
@property (strong, nonatomic) NSString *item_row_total;
@property (strong, nonatomic) NSString *item_color;
@property (strong, nonatomic) NSString *item_size;
@property (strong, nonatomic) NSString *item_capacity;
@property (strong, nonatomic) NSString *item_version;
@property (strong, nonatomic) NSString *item_other;
@property (strong, nonatomic) NSString *item_description;
@property (strong, nonatomic) NSString *attr;

-(instancetype) initWithDic:(NSDictionary *) dic;
@end
