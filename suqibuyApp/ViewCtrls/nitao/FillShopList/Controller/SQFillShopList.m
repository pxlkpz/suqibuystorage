//
//  SQFillShopList.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQFillShopList.h"
#import "SQShopListModel.h"
#import "SQShopListItemModel.h"
#import "SQFillShopListCell.h"
#import "SQNitaoAddNewItemVC.h"
#import "SQNitaoClearVC.h"

static NSString *identifier = @"SQFillShopListCell";

@interface SQFillShopList ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    __weak IBOutlet UIScrollView *bigScroll;
    
    UITableView *listTableView;
    UIButton *addItemButton;
    UILabel *noItemLabel;
    
    __weak IBOutlet UILabel *totalCostLabel;
    __weak IBOutlet UILabel *expressCostLabel;
    __weak IBOutlet UIButton *clearButton;
    __weak IBOutlet NSLayoutConstraint *clearButtonWidth;
    
    SQShopListModel *shopListModel;
    NSMutableArray *itemArray;
    
    NSMutableArray *itemRowArray;
    NSMutableArray *itemHeaderArray;
    
    NSIndexPath *path;
}

@end

@implementation SQFillShopList

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"代购购物车";
    
    noItemLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, screen_width, 200)];
    noItemLabel.text = @"您尚未添加任何内容\n马上点击下面的按钮开始添加吧！";
    noItemLabel.textColor = [UIColor colorWithHexString:text_gray];
    noItemLabel.textAlignment = NSTextAlignmentCenter;
    noItemLabel.font = [UIFont systemFontOfSize:14];
    noItemLabel.numberOfLines = 2;
    
    listTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 200)];
    listTableView.scrollEnabled = NO;
    listTableView.dataSource = self;
    listTableView.delegate = self;
    UIView *view =[ [UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [listTableView setTableFooterView:view];
    UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
    [listTableView registerNib:nib forCellReuseIdentifier:identifier];
    
    [listTableView addSubview:noItemLabel];
    
    if (iPhone6P_D) {
        if ([listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
    
    [bigScroll addSubview:listTableView];
    
    addItemButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 200 +10, screen_width - 20, 40)];
    addItemButton.layer.cornerRadius = allCornerRadius;
    [addItemButton setTitle:@"添加代购物品" forState:UIControlStateNormal];
    addItemButton.titleLabel.font = [UIFont systemFontOfSize:15];
    addItemButton.backgroundColor = [UIColor colorWithHexString:text_shallow_gray];
    [addItemButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addItemButton addTarget:self action:@selector(addItem) forControlEvents:UIControlEventTouchUpInside];
    
    [bigScroll addSubview:addItemButton];
    
    [clearButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [clearButton setBackgroundColor:[UIColor colorWithHexString:text_color]];
    clearButton.enabled = NO;
    
    clearButtonWidth.constant = screen_width/4;
    
    itemArray = [[NSMutableArray alloc] init];
    itemHeaderArray = [[NSMutableArray alloc] init];
    
    @weakify(self);
    bigScroll.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData];
    }];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self requestData];
}

#pragma mark - 私有方法
#pragma mark -请求数据
-(void) requestData
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_CARTITEMS WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    shopListModel = [[SQShopListModel alloc] initWithDic:result];
                    [itemArray removeAllObjects];
                    [itemHeaderArray removeAllObjects];
                    [itemRowArray removeAllObjects];
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        NSArray *array = items;
                        NSInteger count = [array count];
                        for (NSInteger i = 0; i < count; i++)
                        {
                            if (array[i] != nil && array[i] != [NSNull null] && [array[i] isKindOfClass:[NSDictionary class]])
                            {
                                SQShopListItemModel *model = [[SQShopListItemModel alloc] initWithDic:array[i]];
                                if (model.item_store.length != 0)
                                {
                                    NSString *string = [model.item_store mutableCopy];
                                    [itemHeaderArray addObject:string];
                                }
                                [itemArray addObject:model];
                            }
                        }
                        
                        ///算出row
                        itemRowArray = [NSMutableArray arrayWithCapacity:[itemHeaderArray count]];
                        for (NSInteger i = 0; i < [itemHeaderArray count]; i ++)
                        {
                            NSMutableArray *array =[[NSMutableArray alloc] init];
                            [itemRowArray addObject:array];
                        }
                        
                        NSInteger j = 0;
                        for (NSInteger i = 0; i < [itemArray count]; i++)
                        {
                            if (i == 0)
                            {
                                [itemRowArray[0] addObject:itemArray[0]];
                            }
                            else
                            {
                                SQShopListItemModel *model = itemArray[i];
                                if (model.item_store.length != 0)
                                {
                                    j++;
                                }
                                [itemRowArray[j] addObject:itemArray[i]];
                            }
                        }
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        if ([itemArray count] > 0)
        {
            [noItemLabel removeFromSuperview];
            
            CGFloat tableHeight = 75 * [itemArray count] + 30 * [itemHeaderArray count];
            [listTableView setFrame:CGRectMake(0, 0, screen_width, tableHeight)];
            [addItemButton setFrame:CGRectMake(10, tableHeight+10, screen_width - 20, 40)];
            [bigScroll setContentSize:CGSizeMake(screen_width, tableHeight+10 + 50)];
            
            [listTableView reloadData];
            NSString *grand_total = [shopListModel.grand_total gtm_stringByUnescapingFromHTML];
            totalCostLabel.text = grand_total;
            
            NSString *shipping_total = [shopListModel.shipping_total gtm_stringByUnescapingFromHTML];
            expressCostLabel.text = [NSString stringWithFormat:@"(含国内代付运费%@)",shipping_total];
        }
        else
        {
            [listTableView addSubview:noItemLabel];
            CGFloat tableHeight = 200;
            [listTableView setFrame:CGRectMake(0, 0, screen_width, tableHeight)];
            [addItemButton setFrame:CGRectMake(10, tableHeight+10, screen_width - 20, 40)];
            [bigScroll setContentSize:CGSizeMake(screen_width, tableHeight+10 + 50)];
            [listTableView reloadData];
        }
        
        if ([itemArray count] > 0)
        {
            clearButton.enabled = YES;
            clearButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
        }
        else
        {
            clearButton.enabled = NO;
            clearButton.backgroundColor = [UIColor colorWithHexString:text_color];
        }
        [bigScroll.mj_header endRefreshing];
    } WithErrorBlock:^(id errorCode) {
        [bigScroll.mj_header endRefreshing];
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [bigScroll.mj_header endRefreshing];
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark -添加代购物品
-(void) addItem
{
    SQNitaoAddNewItemVC *controller = [[SQNitaoAddNewItemVC alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -结算
- (IBAction)clear:(id)sender
{
    SQNitaoClearVC *controller = [[SQNitaoClearVC alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark - tableview delegate
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [itemHeaderArray count];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemRowArray[section] count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SQFillShopListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    SQShopListItemModel *model = itemRowArray[indexPath.section][indexPath.row];
    
    NSURL *url = [NSURL URLWithString:model.item_image];
    [cell.itemImageView sd_setImageWithURL:url placeholderImage:nil];
    
    cell.itemNameLabel.text = model.item_name;
    
    cell.itemPriceLabel.text = [model.item_purchasing_price gtm_stringByUnescapingFromHTML];
    
    cell.itemNumberLabel.text = [NSString stringWithFormat:@"x%@",model.item_qty];
    
    
    cell.itemDescriptionLabel.text = model.attr;
    
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 40)];
    UILabel *label= [[UILabel alloc] initWithFrame:CGRectMake(15, 0, screen_width, 40)];
    label.text = itemHeaderArray[section];
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor colorWithHexString:text_color];
    [view addSubview:label];
    return view;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        path = indexPath;
        [self requstDelete];
    }
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}


-(void)requstDelete{
    NSString *user_token = [SQUser loadmodel].user_token;
    SQShopListItemModel *model = itemRowArray[path.section][path.row];
    NSString *item_id = model.item_id;
    NSDictionary *dic = @{@"user_token":user_token,@"id":item_id};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_DELITEM WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                showDefaultPromptTextHUD(@"删除成功");
                [self requestData];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

@end
