//
//  SQNitaoAddNewItemVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/24.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQNitaoAddNewItemVC.h"
#import "MBZPKTextField.h"
#import "MBZPKTextView.h"
#import "PxlTextView.h"

@interface SQNitaoAddNewItemVC ()<UIAlertViewDelegate,UITextFieldDelegate>
{
    __weak IBOutlet NSLayoutConstraint *viewWidth;
    __weak IBOutlet NSLayoutConstraint *colorTextFieldWidth;
    __weak IBOutlet NSLayoutConstraint *versionTextFieldWidth;
    __weak IBOutlet NSLayoutConstraint *capacityTextFieldWidth;
    
    //必填
    __weak IBOutlet MBZPKTextField *urlTextField;
    __weak IBOutlet MBZPKTextField *nameTextField;
    __weak IBOutlet MBZPKTextField *quantityTextField;
    __weak IBOutlet MBZPKTextField *priceTextField;
    __weak IBOutlet MBZPKTextField *shipTextField;
    
    //非必填
    __weak IBOutlet MBZPKTextField *colorTextField;
    __weak IBOutlet MBZPKTextField *versionTextField;
    __weak IBOutlet MBZPKTextField *otherTextField;
    __weak IBOutlet MBZPKTextField *capacityTextField;
    __weak IBOutlet MBZPKTextField *sizeTextField;
    __weak IBOutlet MBZPKTextView *descriptionTextView;
    
    __weak IBOutlet UIButton *submitButton;
}

@end

@implementation SQNitaoAddNewItemVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"为代购订单添加商品";
    
    viewWidth.constant = screen_width;
    
    CGFloat width = (screen_width - 90 - 10 - 5)/2;
    colorTextFieldWidth.constant = width;
    versionTextFieldWidth.constant = width;
    capacityTextFieldWidth.constant = width;
    
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    submitButton.layer.cornerRadius = allCornerRadius;
    
    
    quantityTextField.keyboardType = UIKeyboardTypeDecimalPad;
    priceTextField.keyboardType = UIKeyboardTypeDecimalPad;
    shipTextField.keyboardType = UIKeyboardTypeDecimalPad;
    
}

#pragma mark -确认保存
- (IBAction)submit:(id)sender
{
    [self.view endEditing:YES];
    
    NSDictionary *dic = [[NSMutableDictionary alloc] init];
    
    NSString *user_token = [SQUser loadmodel].user_token;
    [dic setValue:user_token forKey:@"user_token"];
    
    //必填
    NSString *item_url = urlTextField.text;
    if (item_url.length == 0)
    {
        showDefaultPromptTextHUD(@"商品链接不能为空");
        return;
    }
    else
    {
        [dic setValue:item_url forKey:@"item_url"];
    }
    
    NSString *item_name = nameTextField.text;
    if (item_name.length == 0)
    {
        showDefaultPromptTextHUD(@"商品名称不能为空");
        return;
    }
    else
    {
        [dic setValue:item_name forKey:@"item_name"];
    }
    
    NSString *item_qty = quantityTextField.text;
    if (item_qty.length == 0)
    {
        showDefaultPromptTextHUD(@"商品数量不能为空");
        return;
    }
    else
    {
        [dic setValue:item_qty forKey:@"item_qty"];
    }
    
    NSString *item_purchasing_price = priceTextField.text;
    if (item_purchasing_price.length == 0)
    {
        showDefaultPromptTextHUD(@"商品价格不能为空");
        return;
    }
    else
    {
        [dic setValue:item_purchasing_price forKey:@"item_purchasing_price"];
    }
    
    NSString *item_shipping_price = shipTextField.text;
    if (item_shipping_price.length == 0)
    {
        showDefaultPromptTextHUD(@"国内运费不能为空");
        return;
    }
    else
    {
        [dic setValue:item_shipping_price forKey:@"item_shipping_price"];
    }
    
    NSString *item_color = colorTextField.text;
    if (item_color.length != 0)
    {
        [dic setValue:item_color forKey:@"item_color"];
    }
    
    NSString *item_size = sizeTextField.text;
    if (item_size.length != 0)
    {
        [dic setValue:item_size forKey:@"item_size"];
    }
    
    NSString *item_capacity = capacityTextField.text;
    if (item_capacity.length != 0)
    {
        [dic setValue:item_capacity forKey:@"item_capacity"];
    }
    
    NSString *item_version = versionTextField.text;
    if (item_version.length != 0)
    {
        [dic setValue:item_version forKey:@"item_version"];
    }
    
    NSString *item_other = otherTextField.text;
    if (item_other.length != 0)
    {
        [dic setValue:item_other forKey:@"item_other"];
    }
    
    NSString *item_description = descriptionTextView.text;
    if (item_description.length != 0)
    {
        [dic setValue:item_description forKey:@"item_description"];
    }
    
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_ADDCART WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提醒" message:@"商品添加成功！\n您是否继续添加新的商品" delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
                [alert show];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}

#pragma mark - alertview delegate
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        urlTextField.text = nil;
        nameTextField.text = nil;
        quantityTextField.text = nil;
        priceTextField.text = nil;
        shipTextField.text = nil;
        
        colorTextField.text = nil;
        versionTextField.text = nil;
        otherTextField.text = nil;
        capacityTextField.text = nil;
        sizeTextField.text = nil;
        descriptionTextView.text = nil;
    }
}

//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    return [PxlTextView validateNumber:string];
//}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag == 5) {
        return [PxlTextView validateNumber:string];
    }
    
    BOOL isHaveDian = YES;
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            
            //首字母不能为0和小数点
            if([textField.text length] == 0){
                if(single == '.') {
                    //                    [self showError:@"亲，第一个数字不能为小数点"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
                
            }
            
            if ([textField.text isEqualToString:@"0"]) {
                if([textField.text length] == 1){
                    if(single != '.') {
                        //                    [self showError:@"亲，第一个数字不能为小数点"];
                        [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        return NO;
                    }else{
                        isHaveDian = YES;
                        return YES;
                        
                    }
                    
                }
                
            }
            
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian = YES;
                    return YES;
                    
                }else{
                    //                    [self showError:@"亲，您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                
                if (isHaveDian) {//存在小数点
                    
                    //判断小数点的位数
                    //                    NSRange ran = [textField.text rangeOfString:@"."];
                    return YES;
                    //                    if (range.location - ran.location <= 2) {
                    //                        return YES;
                    //                    }else{
                    //                        //                        [self showError:@"亲，您最多输入两位小数"];
                    //                        return NO;
                    //
                }else{
                    return YES;
                }
            }
            
            
        }else{//输入的数据格式不正确
            //            [self showError:@"亲，您输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
}


@end
