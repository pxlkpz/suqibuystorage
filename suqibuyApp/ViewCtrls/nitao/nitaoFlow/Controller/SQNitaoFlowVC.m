//
//  SQNitaoFlowVC.m
//  suqibuyApp
//
//  Created by apple on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQNitaoFlowVC.h"
#import "SQFillShopList.h"
#import "suqibuyApp-Swift.h"

@interface SQNitaoFlowVC ()

@end

@implementation SQNitaoFlowVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"代购流程";
    
    [self buildui];
    
    
    
}

#pragma mark - ---- event response 🍐🌛
-(void)buttonAction{
    SQDaigouShopController *controller = [[SQDaigouShopController alloc] init];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0], controller]];
}
#pragma mark - ---- private methods 🍊🌜

-(void)buildui{
    UIView* bgview = [UIView new];
    bgview.backgroundColor = KColorFromRGB(0xEFEFEF);//efefef
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"nitao_step"];
    CGSize size1 = imageView.image.size;
    CGFloat r = (screen_width/size1.width);
    
    bgview.frame = CGRectMake(0, 0, KScreenWidth, r*size1.height +100);
    
    [bgview addSubview:imageView];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bgview);
        make.top.equalTo(bgview);
        
        make.size.mas_equalTo(CGSizeMake(screen_width, r*size1.height ));
    }];
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor colorWithHexString:text_yellow] forState:UIControlStateNormal];
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor colorWithHexString:text_yellow].CGColor;
    [button setTitle:@"立刻代购" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor whiteColor]];
    
    button.layer.cornerRadius = allCornerRadius;
    
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [bgview addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_bottom).offset(20);
        make.right.equalTo(bgview.mas_right).offset(-20);
        make.left.equalTo(@20);
        make.height.equalTo(@40);
    }];
    
    
    UITableView *tabel1 = [[UITableView alloc] init];
    [self.view addSubview:tabel1];
    [tabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.view);
    }];
    
    [tabel1 setTableHeaderView:bgview];
    
}

@end
