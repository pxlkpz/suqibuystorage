//
//  SQGoodsModel.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/25.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQGoodsModel.h"

@implementation SQGoodsModel

-(instancetype)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self)
    {
        self.item_thumb = [self testNSStringInDic:dic string:@"item_thumb"];
        self.item_name = [self testNSStringInDic:dic string:@"item_name"];
        self.item_params = [self testNSStringInDic:dic string:@"item_params"];
        self.item_purchasing_price = [self testNSStringInDic:dic string:@"item_purchasing_price"];
        self.item_qty = [self testNSStringInDic:dic string:@"item_qty"];
    }
    return self;
}
@end


