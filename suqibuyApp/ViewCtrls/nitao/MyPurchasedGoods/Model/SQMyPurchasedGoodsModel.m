//
//  SQMyPurchasedGoodsModel.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/25.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQMyPurchasedGoodsModel.h"

@implementation SQMyPurchasedGoodsModel

-(instancetype)initWidthDic:(NSDictionary *)dic
{
    self = [super init];
    if (self)
    {
        self.daigou_order_no = [self testNSStringInDic:dic string:@"daigou_order_no"];
        self.status_label = [self testNSStringInDic:dic string:@"status_label"];
        id goods = [dic objectForKey:@"goods"];
        if (goods != nil && goods != [NSNull null] && [goods isKindOfClass:[NSArray class]])
        {
            NSArray *array = goods;
            self.goods = [NSMutableArray arrayWithCapacity:[array count]];
            for (NSInteger i = 0; i < [array count]; i++)
            {
                SQGoodsModel *model =[[SQGoodsModel alloc] initWithDic:array[i]];
                [self.goods addObject:model];
            }
        }
    }
    return self;
}
@end
