//
//  SQGoodsModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/25.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "MBZPKBaseModel.h"

@interface SQGoodsModel : MBZPKBaseModel

@property (strong, nonatomic) NSString *item_thumb;
@property (strong, nonatomic) NSString *item_name;
@property (strong, nonatomic) NSString *item_params;
@property (strong, nonatomic) NSString *item_purchasing_price;
@property (strong, nonatomic) NSString *item_qty;

-(instancetype) initWithDic:(NSDictionary *) dic;

@end


//"item_link": "https://item.taobao.com/item.htm?id=527504843973",
//"item_thumb": "https://t1.shtag.com/images/default/package_icon.png",
//"item_name": "夏季薄款无痕冰丝内衣无钢圈舒适美背性感文胸瑜伽运动聚拢内衣女",
//"item_sku": "3139534724155",
//"status": "dealing",
//"status_label": "处理中",
//"item_params": "颜色分类: 肤色; 尺码: L",
//"item_purchasing_price": "¥23.00",
//"item_shipping_price": "0.00",
//"item_row_totals": "¥23.00",
//"item_qty": "1",
//"detail": null,
//"category_name": ""