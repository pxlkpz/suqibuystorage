//
//  SQMyPurchasedGoodsModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/25.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "MBZPKBaseModel.h"
#import "SQGoodsModel.h"

@interface SQMyPurchasedGoodsModel : MBZPKBaseModel

@property (strong, nonatomic) NSString *order_id;

///订单号
@property (strong, nonatomic) NSString *daigou_order_no;
@property (strong, nonatomic) NSString *status;

///订单状态
@property (strong, nonatomic) NSString *status_label;
@property (strong, nonatomic) NSString *is_waiting_payment;
@property (strong, nonatomic) NSString *can_cancel;
@property (strong, nonatomic) NSString *created_at;
@property (strong, nonatomic) NSString *total_qty;
@property (strong, nonatomic) NSString *sub_total;
@property (strong, nonatomic) NSString *agency_fee;
@property (strong, nonatomic) NSString *coupon_code;
@property (strong, nonatomic) NSString *coupon_discount_amount;
@property (strong, nonatomic) NSString *credits_point_total;
@property (strong, nonatomic) NSString *credits_amount;
@property (strong, nonatomic) NSString *grand_total;
@property (strong, nonatomic) NSString *paid_total;
@property (strong, nonatomic) NSString *paid_at;
@property (strong, nonatomic) NSString *waiting_pay_total;
@property (strong, nonatomic) NSString *custom_remark;

@property (strong, nonatomic) NSMutableArray *goods;

-(instancetype) initWidthDic:(NSDictionary *) dic;

@end
