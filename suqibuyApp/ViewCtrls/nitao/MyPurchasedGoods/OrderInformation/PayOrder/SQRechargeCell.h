//
//  SQRechargeCell.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SQRechargeCellDelegate <NSObject>

-(void) recharge;

@end

@interface SQRechargeCell : UITableViewCell
@property (weak, nonatomic) id<SQRechargeCellDelegate> cellDelegate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *remainingLabelWidth;


@property (weak, nonatomic) IBOutlet UILabel *remainingLabel;
@property (weak, nonatomic) IBOutlet UIButton *rechargeButton;

@end
