//
//  SQPayOrderVC.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"
#import "SQorderInformationModel.h"

@interface SQPayOrderVC : SQBaseViewController

@property (strong, nonatomic) SQorderInformationModel *model;
@property (strong, nonatomic) NSDictionary *dataModel;

- (IBAction)textFiledReturnEditing:(id)sender;

@end
