//
//  SQRechargeCell.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQRechargeCell.h"

@implementation SQRechargeCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.remainingLabel.textColor = [UIColor colorWithHexString:button_blue];
    
    [self.rechargeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.rechargeButton.backgroundColor = [UIColor colorWithHexString:button_blue];
    self.rechargeButton.layer.cornerRadius = allCornerRadius;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)recharge:(id)sender
{
    [self.cellDelegate recharge];
}



@end
