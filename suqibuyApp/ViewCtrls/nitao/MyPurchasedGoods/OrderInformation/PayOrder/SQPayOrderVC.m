//
//  SQPayOrderVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQPayOrderVC.h"
#import "MBZPKTextField.h"

#import "SQorderInformationTopCell.h"
#import "SQorderInformationBottomCell.h"
#import "SQRechargeCell.h"
#import "SQWoChongzhiVC.h"

static NSString *firstCellIdentifier = @"SQorderInformationBottomCell";
static NSString *secondCellIdentifier = @"SQorderInformationTopCell";
static NSString *rechargeCell = @"SQRechargeCell";

@interface SQPayOrderVC ()<UITableViewDelegate,UITableViewDataSource,SQRechargeCellDelegate>
{
    __weak IBOutlet NSLayoutConstraint *viewWidth;
    __weak IBOutlet NSLayoutConstraint *viewHeight;
    __weak IBOutlet NSLayoutConstraint *secondTableViewHeight;
    
    __weak IBOutlet UIScrollView *bigScroll;
    __weak IBOutlet UITableView *firstTableView;
    __weak IBOutlet UITableView *secondTableView;
    __weak IBOutlet MBZPKTextField *passwordTextField;
    __weak IBOutlet UIButton *submitButton;
    
    
}

@end

@implementation SQPayOrderVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.model == nil) {
        self.model = [SQorderInformationModel mj_objectWithKeyValues:self.dataModel];
    }
    
    self.title = [NSString stringWithFormat:@"%@代购订单支付",self.model.daigou_order_no];
    
    
    
    firstTableView.scrollEnabled = NO;
    firstTableView.dataSource = self;
    firstTableView.delegate = self;
    firstTableView.tag = 0;
    UINib *nib = [UINib nibWithNibName:firstCellIdentifier bundle:nil];
    [firstTableView registerNib:nib forCellReuseIdentifier:firstCellIdentifier];
    
    secondTableView.scrollEnabled = NO;
    secondTableView.dataSource = self;
    secondTableView.delegate = self;
    secondTableView.tag = 1;
    UINib *nib1 = [UINib nibWithNibName:secondCellIdentifier bundle:nil];
    [secondTableView registerNib:nib1 forCellReuseIdentifier:secondCellIdentifier];
    UINib *nib2 = [UINib nibWithNibName:rechargeCell bundle:nil];
    [secondTableView registerNib:nib2 forCellReuseIdentifier:rechargeCell];
    
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submitButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
    submitButton.layer.cornerRadius = allCornerRadius;
    [submitButton addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    
    viewWidth.constant = screen_width;
    if ([self.model.show_chongzhi_link integerValue] == 1)
    {
        secondTableViewHeight.constant = 180 - 36 + 120;
    }
    else
    {
        secondTableViewHeight.constant = 180;
    }
    viewHeight.constant = 441 + secondTableViewHeight.constant;
    
    
    passwordTextField.returnKeyType = UIReturnKeyDone;
    
    @weakify(self);
    
    
    bigScroll.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData];
    }];
    
    
}

-(IBAction)textFiledReturnEditing:(id)sender {
    [sender resignFirstResponder];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [passwordTextField resignFirstResponder];
}

#pragma mark - 确认付款
-(void) submit
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSString *confirm_password = passwordTextField.text;
    if (confirm_password.length == 0)
    {
        showDefaultPromptTextHUD(@"请输入您帐号的登录密码");
        return;
    }
    NSDictionary *dic =@{@"user_token":user_token,@"order_no":self.model.daigou_order_no,@"confirm_password":confirm_password};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_PAYMENT WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                showDefaultPromptTextHUD(@"付款成功");
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark - 请求数据
-(void) requestData
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token,@"order_no":self.model.daigou_order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_DETAIL WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    self.model = [[SQorderInformationModel alloc] initWithDic:result];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [bigScroll.mj_header endRefreshing];
        
        if ([self.model.show_chongzhi_link integerValue] == 1)
        {
            secondTableViewHeight.constant = 180 - 36 + 120;
        }
        else
        {
            secondTableViewHeight.constant = 180;
        }
        viewHeight.constant = 441 + secondTableViewHeight.constant;
        
        [firstTableView reloadData];
        [secondTableView reloadData];
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [bigScroll.mj_header endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [bigScroll.mj_header endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark - tableview delegate
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 0)
    {
        return 5;
    }
    else
    {
        return 5;
    }
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0)
    {
        SQorderInformationBottomCell *cell = [tableView dequeueReusableCellWithIdentifier:firstCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0)
        {
            cell.cellTitleLabel.text = @"购物代付总金额:";
            cell.cellPriceLabel.text = [self.model.sub_total gtm_stringByUnescapingFromHTML];
        }
        else if (indexPath.row == 1)
        {
            cell.cellTitleLabel.text = @"代购操作费:";
            cell.cellPriceLabel.text = [self.model.agency_fee gtm_stringByUnescapingFromHTML];
        }
        else if (indexPath.row == 2)
        {
            cell.cellTitleLabel.text = [NSString stringWithFormat:@"优惠券抵扣 [ %@ ]",self.model.coupon_code];
            cell.cellPriceLabel.text = [self.model.coupon_discount_amount gtm_stringByUnescapingFromHTML];
        }
        else if (indexPath.row == 3)
        {
            cell.cellTitleLabel.text = [NSString stringWithFormat:@"积分抵扣 [ 共使用了%@个积分 ]",self.model.credits_point_total];
            cell.cellPriceLabel.text = [self.model.credits_amount gtm_stringByUnescapingFromHTML];
        }
        else
        {
            cell.cellTitleLabel.text = nil;
            cell.cellPriceLabel.text = [self.model.waiting_pay_total gtm_stringByUnescapingFromHTML];
        }
        return cell;
    }
    
    
    else
    {
        
        SQorderInformationTopCell *cell = [tableView dequeueReusableCellWithIdentifier:secondCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0)
        {
            cell.cellTitleLabel.text = @"订单号:";
            cell.cellTextLabel.text = self.model.daigou_order_no;
            cell.cellTextLabel.textColor = [UIColor colorWithHexString:text_color];
        }
        else if (indexPath.row == 1)
        {
            cell.cellTitleLabel.text = @"订单状态:";
            cell.cellTextLabel.text = self.model.status_label;
            if ([self.model.status_label isEqualToString:@"待付款"])
            {
                cell.cellTextLabel.textColor = [UIColor colorWithHexString:pink_color];
            }
            else if([self.model.status_label isEqualToString:@"已取消"])
            {
                cell.cellTextLabel.textColor = [UIColor colorWithHexString:gray];
            }
            else
            {
                cell.cellTextLabel.textColor = [UIColor colorWithHexString:order_status_green];
                
            }
        }
        else if (indexPath.row == 2)
        {
            cell.cellTitleLabel.text = @"已付款:";
            cell.cellTextLabel.text = [self.model.paid_total gtm_stringByUnescapingFromHTML];
            cell.cellTextLabel.textColor = [UIColor colorWithHexString:text_color];
        }
        else if (indexPath.row == 3)
        {
            cell.cellTitleLabel.text = @"待支付:";
            cell.cellTextLabel.text = [self.model.waiting_pay_total gtm_stringByUnescapingFromHTML];
            cell.cellTextLabel.textColor = [UIColor colorWithHexString:simple_button_yellow];
        }
        else
        {
            if ([self.model.show_chongzhi_link integerValue] == 1)
            {
                SQRechargeCell *recharge = [tableView dequeueReusableCellWithIdentifier:rechargeCell];
                
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGFLOAT_MAX, 30)];
                label.font = [UIFont systemFontOfSize:14];
                label.numberOfLines = 1;
                label.text = [self.model.user_cash gtm_stringByUnescapingFromHTML];
                CGSize expect = [label sizeThatFits:CGSizeMake(CGFLOAT_MAX, 30)];
                recharge.remainingLabelWidth.constant = expect.width;
                
                recharge.remainingLabel.text = [self.model.user_cash gtm_stringByUnescapingFromHTML];
                recharge.cellDelegate = self;
                
                return recharge;
            }
            else
            {
                cell.cellTitleLabel.text = @"账户余额:";
                cell.cellTextLabel.text = [self.model.user_cash gtm_stringByUnescapingFromHTML];
                cell.cellTextLabel.textColor = [UIColor colorWithHexString:button_blue];
            }
        }
        return cell;
    }
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0)
    {
        return 39;
    }
    else
    {
        if ([self.model.show_chongzhi_link integerValue] == 1)
        {
            if (indexPath.row == 4)
            {
                return 120;
            }
            else
            {
                return 36;
            }
        }
        else
        {
            return 36;
        }
    }
}

#pragma mark -充值
-(void) recharge
{
    SQWoChongzhiVC *vc = [[SQWoChongzhiVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}
@end
