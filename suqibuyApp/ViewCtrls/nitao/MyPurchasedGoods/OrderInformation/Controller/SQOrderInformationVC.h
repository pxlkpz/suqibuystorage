//
//  SQOrderInformationVC.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"

@interface SQOrderInformationVC : SQBaseViewController

@property (strong, nonatomic) NSString *order_no;
@end
