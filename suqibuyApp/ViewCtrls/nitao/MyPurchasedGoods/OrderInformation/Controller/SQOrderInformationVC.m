//
//  SQOrderInformationVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQOrderInformationVC.h"

#import "SQorderInformationTopCell.h"
#import "SQorderInformationBottomCell.h"
#import "SQorderInformationLabelCell.h"
#import "SQFillShopListCell.h"

#import "SQorderInformationModel.h"
#import "SQPayOrderVC.h"

static NSString *firstCellIdentifier = @"SQorderInformationTopCell";

static NSString *secondCellIdentifier1 = @"SQorderInformationLabelCell";
static NSString *secondCellIdentifier2 = @"SQFillShopListCell";

static NSString *thirdCellIdentifier = @"SQorderInformationBottomCell";

@interface SQOrderInformationVC ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    __weak IBOutlet NSLayoutConstraint *viewWidth;
    __weak IBOutlet NSLayoutConstraint *viewHeight;
    __weak IBOutlet NSLayoutConstraint *secondTableViewHeight;
    
    __weak IBOutlet NSLayoutConstraint *cancleButtonWidth;
    __weak IBOutlet NSLayoutConstraint *cancleButtonHeight;
    __weak IBOutlet NSLayoutConstraint *orderButtonHeight;
    
    
    __weak IBOutlet UIScrollView *bigScroll;
    __weak IBOutlet UITableView *firstTableView;
    __weak IBOutlet UITableView *secondTableView;
    __weak IBOutlet UITableView *thirdTableView;
    __weak IBOutlet UIButton *cancelButton;
    __weak IBOutlet UIButton *orderButton;
    
    SQorderInformationModel *model;
}

@end

@implementation SQOrderInformationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [NSString stringWithFormat:@"%@代购订单详情",self.order_no];
    
    firstTableView.scrollEnabled = NO;
    firstTableView.dataSource = self;
    firstTableView.delegate = self;
    firstTableView.tag = 0;
    [firstTableView setTableFooterView:[[UIView alloc]init]];
    UINib *nib = [UINib nibWithNibName:firstCellIdentifier bundle:nil];
    [firstTableView registerNib:nib forCellReuseIdentifier:firstCellIdentifier];
    
    secondTableView.scrollEnabled = NO;
    secondTableView.dataSource = self;
    secondTableView.delegate = self;
    secondTableView.tag = 1;
    UINib *nib1 = [UINib nibWithNibName:secondCellIdentifier1 bundle:nil];
    [secondTableView registerNib:nib1 forCellReuseIdentifier:secondCellIdentifier1];
    UINib *nib2 = [UINib nibWithNibName:secondCellIdentifier2 bundle:nil];
    [secondTableView registerNib:nib2 forCellReuseIdentifier:secondCellIdentifier2];
    
    thirdTableView.scrollEnabled = NO;
    thirdTableView.dataSource = self;
    thirdTableView.delegate = self;
    thirdTableView.tag = 2;
    UINib *nib4 = [UINib nibWithNibName:thirdCellIdentifier bundle:nil];
    [thirdTableView registerNib:nib4 forCellReuseIdentifier:thirdCellIdentifier];
    
    viewWidth.constant = screen_width;
    
    cancelButton.backgroundColor = [UIColor colorWithHexString:gray];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelOrder) forControlEvents:UIControlEventTouchUpInside];
    
    orderButton.backgroundColor = [UIColor colorWithHexString:simple_button_yellow];
    [orderButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [orderButton addTarget:self action:@selector(payOrder) forControlEvents:UIControlEventTouchUpInside];
    
    @weakify(self)
    bigScroll.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData];
    }];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self requestData];
}

#pragma mark - 请求数据
-(void) requestData
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token,@"order_no":self.order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_DETAIL WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    model = [[SQorderInformationModel alloc] initWithDic:result];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [bigScroll.mj_header endRefreshing];
        
        secondTableViewHeight.constant = 35 * 2 + [model.goods count] * 70;
        viewHeight.constant = secondTableViewHeight.constant + 477;
        
        if ([model.is_waiting_payment integerValue] == 1 && [model.can_cancel integerValue] == 1)
        {
            cancleButtonWidth.constant = screen_width/2;
            cancleButtonHeight.constant = 50;
            orderButtonHeight.constant = 50;
        }
        else if ([model.is_waiting_payment integerValue] == 1 && [model.can_cancel integerValue] == 0)
        {
            cancleButtonWidth.constant = 0;
            cancleButtonHeight.constant = 50;
            orderButtonHeight.constant = 50;
        }
        else if ([model.is_waiting_payment integerValue] == 0 && [model.can_cancel integerValue] == 1)
        {
            cancleButtonWidth.constant = screen_width;
            cancleButtonHeight.constant = 50;
            orderButtonHeight.constant = 50;
        }
        else
        {
            cancleButtonHeight.constant = 0;
            orderButtonHeight.constant = 0;
        }
        
        [firstTableView reloadData];
        [secondTableView reloadData];
        [thirdTableView reloadData];
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [bigScroll.mj_header endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [bigScroll.mj_header endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark -取消订单
-(void) cancelOrder
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提醒" message:@"您确认要取消该订单吗?\n取消后不可恢复，不过可以重新再下单" delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
    [alert show];
}

#pragma mark -支付订单
-(void) payOrder
{
    SQPayOrderVC *controller = [[SQPayOrderVC alloc] init];
    controller.model = model;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - alertview delegate
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSString *user_token = [SQUser loadmodel].user_token;
        NSDictionary *dic = @{@"user_token":user_token,@"order_no":self.order_no};
        
        [self showLoadingHUD];
        [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_CANCEL WithParameter:dic WithFinishBlock:^(id returnValue) {
            [self hideLoadingHUD];
            if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
            {
                NSDictionary * dic = returnValue;
                id success = [dic objectForKey:@"success"];
                if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
                {
                    showDefaultPromptTextHUD(@"取消订单成功");
                    id result = [dic objectForKey:@"result"];
                    if (result != nil && [result isKindOfClass:[NSDictionary class]])
                    {
                        model = [[SQorderInformationModel alloc] initWithDic:result];
                    }
                }
                else
                {
                    id error_msg = [dic objectForKey:@"error_msg"];
                    if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                    {
                        showDefaultPromptTextHUD(error_msg);
                    }
                    else
                    {
                        showDefaultPromptTextHUD(@"网络异常");
                    }
                }
            }
            
            [bigScroll.mj_header endRefreshing];
            
            secondTableViewHeight.constant = 35 * 2 + [model.goods count] * 70;
            viewHeight.constant = secondTableViewHeight.constant + 477;
            
            if ([model.is_waiting_payment integerValue] == 1 && [model.can_cancel integerValue] == 1)
            {
                cancleButtonWidth.constant = screen_width/2;
                cancleButtonHeight.constant = 50;
                orderButtonHeight.constant = 50;
            }
            else if ([model.is_waiting_payment integerValue] == 1 && [model.can_cancel integerValue] == 0)
            {
                cancleButtonWidth.constant = 0;
                cancleButtonHeight.constant = 50;
                orderButtonHeight.constant = 50;
            }
            else if ([model.is_waiting_payment integerValue] == 0 && [model.can_cancel integerValue] == 1)
            {
                cancleButtonWidth.constant = screen_width;
                cancleButtonHeight.constant = 50;
                orderButtonHeight.constant = 50;
            }
            else
            {
                cancleButtonHeight.constant = 0;
                orderButtonHeight.constant = 0;
            }
            
            [firstTableView reloadData];
            [secondTableView reloadData];
            [thirdTableView reloadData];
        } WithErrorBlock:^(id errorCode) {
            [self hideLoadingHUD];
            [bigScroll.mj_header endRefreshing];
            showDefaultPromptTextHUD(@"网络异常");
        } WithFailedBlock:^{
            [self hideLoadingHUD];
            [bigScroll.mj_header endRefreshing];
            showDefaultPromptTextHUD(@"网络异常");
        }];
    }
}

#pragma mark - tableview delegate
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 0)
    {
        return 7;
    }
    else if (tableView.tag == 1)
    {
        return 2 + [model.goods count];
    }
    else
    {
        return 5;
    }
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0)
    {
        SQorderInformationTopCell *cell = [tableView dequeueReusableCellWithIdentifier:firstCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0)
        {
            cell.cellTitleLabel.text = @"订单状态:";
            cell.cellTextLabel.text = model.status_label;
            if ([model.status_label isEqualToString:@"待付款"])
            {
                cell.cellTextLabel.textColor = [UIColor colorWithHexString:pink_color];
            }
            else if([model.status_label isEqualToString:@"已取消"])
            {
                cell.cellTextLabel.textColor = [UIColor colorWithHexString:gray];
            }
            else
            {
                cell.cellTextLabel.textColor = [UIColor colorWithHexString:order_status_green];
            }
        }
        else if (indexPath.row == 1)
        {
            cell.cellTitleLabel.text = @"订单号:";
            cell.cellTextLabel.text = model.daigou_order_no;
            cell.cellTextLabel.textColor = [UIColor colorWithHexString:text_color];
        }
        else if (indexPath.row == 2)
        {
            cell.cellTitleLabel.text = @"订单总金额:";
            cell.cellTextLabel.text = [model.grand_total gtm_stringByUnescapingFromHTML];
            cell.cellTextLabel.textColor = [UIColor colorWithHexString:text_color];
        }
        else if (indexPath.row == 3)
        {
            cell.cellTitleLabel.text = @"订单备注:";
            cell.cellTextLabel.text = model.custom_remark;
            cell.cellTextLabel.textColor = [UIColor colorWithHexString:text_color];
        }
        else if (indexPath.row == 4)
        {
            cell.cellTitleLabel.text = @"下单时间:";
            cell.cellTextLabel.text = model.created_at;
            cell.cellTextLabel.textColor = [UIColor colorWithHexString:text_color];
        }
        else if (indexPath.row == 5)
        {
            cell.cellTitleLabel.text = @"已付款:";
            cell.cellTextLabel.text = [model.paid_total gtm_stringByUnescapingFromHTML];
            cell.cellTextLabel.textColor = [UIColor colorWithHexString:button_blue];
        }
        else
        {
            cell.cellTitleLabel.text = @"付款日期:";
            cell.cellTextLabel.text = model.paid_at;
            cell.cellTextLabel.textColor = [UIColor colorWithHexString:text_color];
//            cell.
        }
        return cell;
    }
    
    
    else if (tableView.tag == 1)
    {
        if (indexPath.row == 0)
        {
            SQorderInformationLabelCell *cell = [tableView dequeueReusableCellWithIdentifier:secondCellIdentifier1];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.cellLabel.text = @"商品清单";
            cell.cellLabel.textAlignment = NSTextAlignmentLeft;
            return cell;
        }
        else if (indexPath.row == [model.goods count] + 1)
        {
            SQorderInformationLabelCell *cell = [tableView dequeueReusableCellWithIdentifier:secondCellIdentifier1];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.cellLabel.text = [NSString stringWithFormat:@"共 %@ 件商品",model.total_qty];
            cell.cellLabel.font = [UIFont systemFontOfSize:13];
            cell.cellLabel.textAlignment = NSTextAlignmentRight;
            return cell;
        }
        else
        {
            SQFillShopListCell *cell =[tableView dequeueReusableCellWithIdentifier:secondCellIdentifier2];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            SQGoodsModel *goods = model.goods[indexPath.row - 1];
            
            NSURL *url = [NSURL URLWithString:goods.item_thumb];
            [cell.itemImageView sd_setImageWithURL:url placeholderImage:nil];
            
            cell.itemNameLabel.text = goods.item_name;
            
            cell.itemPriceLabel.text = [goods.item_purchasing_price gtm_stringByUnescapingFromHTML];
            
            cell.itemNumberLabel.text = [NSString stringWithFormat:@"x%@",goods.item_qty];
            
            cell.itemDescriptionLabel.text = goods.item_params;
            
            return cell;
        }
    }
    
    else
    {
        SQorderInformationBottomCell *cell = [tableView dequeueReusableCellWithIdentifier:thirdCellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        if (indexPath.row == 0)
        {
            cell.cellTitleLabel.text = @"购物代付总金额:";
            cell.cellPriceLabel.text = [model.sub_total gtm_stringByUnescapingFromHTML];
        }
        else if (indexPath.row == 1)
        {
            cell.cellTitleLabel.text = @"代购操作费:";
            cell.cellPriceLabel.text = [model.agency_fee gtm_stringByUnescapingFromHTML];
        }
        else if (indexPath.row == 2)
        {
            cell.cellTitleLabel.text = [NSString stringWithFormat:@"优惠券抵扣 [ %@ ]",model.coupon_code];
            cell.cellPriceLabel.text = [model.coupon_discount_amount gtm_stringByUnescapingFromHTML];
        }
        else if (indexPath.row == 3)
        {
            cell.cellTitleLabel.text = [NSString stringWithFormat:@"积分抵扣 [ 共使用了%@个积分 ]",model.credits_point_total];
            cell.cellPriceLabel.text = [model.credits_amount gtm_stringByUnescapingFromHTML];
        }
        else
        {
            cell.cellTitleLabel.text = nil;
            cell.cellPriceLabel.text = [model.waiting_pay_total gtm_stringByUnescapingFromHTML];
        }
        return cell;
    }
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0)
    {
        return 36;
    }
    else if (tableView.tag == 1)
    {
        if (indexPath.row == 0)
        {
            return 35;
        }
        else if (indexPath.row == [model.goods count] + 1)
        {
            return 35;
        }
        else
        {
            return 70;
        }
    }
    else
    {
        return 39;
    }
}
@end
