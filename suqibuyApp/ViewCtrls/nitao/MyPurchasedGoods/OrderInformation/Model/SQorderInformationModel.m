//
//  SQorderInformationModel.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQorderInformationModel.h"

@implementation SQorderInformationModel

-(instancetype)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self)
    {
        self.status_label = [self testNSStringInDic:dic string:@"status_label"];
        self.daigou_order_no = [self testNSStringInDic:dic string:@"daigou_order_no"];
        self.grand_total = [self testNSStringInDic:dic string:@"grand_total"];
        self.custom_remark = [self testNSStringInDic:dic string:@"custom_remark"];
        self.created_at = [self testNSStringInDic:dic string:@"created_at"];
        self.paid_total = [self testNSStringInDic:dic string:@"paid_total"];
        self.paid_at = [self testNSStringInDic:dic string:@"paid_at"];
        
        self.total_qty = [self testNSStringInDic:dic string:@"total_qty"];
        id goods = [dic objectForKey:@"goods"];
        if (goods != nil && goods != [NSNull null] && [goods isKindOfClass:[NSArray class]])
        {
            NSArray *array = goods;
            self.goods = [NSMutableArray arrayWithCapacity:[array count]];
            for (NSInteger i = 0; i < [array count]; i++)
            {
                SQGoodsModel *model =[[SQGoodsModel alloc] initWithDic:array[i]];
                [self.goods addObject:model];
            }
        }

        self.sub_total = [self testNSStringInDic:dic string:@"sub_total"];
        self.agency_fee = [self testNSStringInDic:dic string:@"agency_fee"];
        self.coupon_code = [self testNSStringInDic:dic string:@"coupon_code"];
        self.coupon_discount_amount = [self testNSStringInDic:dic string:@"coupon_discount_amount"];
        self.credits_point_total = [self testNSStringInDic:dic string:@"credits_point_total"];
        self.credits_amount = [self testNSStringInDic:dic string:@"credits_amount"];
        self.waiting_pay_total = [self testNSStringInDic:dic string:@"waiting_pay_total"];
        
        self.is_waiting_payment = [self testNSNumberInDic:dic string:@"is_waiting_payment"];
        self.can_cancel = [self testNSNumberInDic:dic string:@"can_cancel"];
        
        self.user_cash = [self testNSStringInDic:dic string:@"user_cash"];
        self.show_chongzhi_link = [self testNSNumberInDic:dic string:@"show_chongzhi_link"];
    }
    return self;
}
@end
