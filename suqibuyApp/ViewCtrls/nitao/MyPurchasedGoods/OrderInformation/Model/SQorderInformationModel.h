//
//  SQorderInformationModel.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "MBZPKBaseModel.h"
#import "SQGoodsModel.h"

@interface SQorderInformationModel : MBZPKBaseModel

///第一tableview
@property (strong, nonatomic) NSString *status_label;
@property (strong, nonatomic) NSString *daigou_order_no;
@property (strong, nonatomic) NSString *grand_total;
@property (strong, nonatomic) NSString *custom_remark;
@property (strong, nonatomic) NSString *created_at;
@property (strong, nonatomic) NSString *paid_total;
@property (strong, nonatomic) NSString *paid_at;


///第二tableview
@property (strong, nonatomic) NSString *total_qty;
@property (strong, nonatomic) NSMutableArray *goods;


///第三tableview
@property (strong, nonatomic) NSString *sub_total;
@property (strong, nonatomic) NSString *agency_fee;
@property (strong, nonatomic) NSString *coupon_code;
@property (strong, nonatomic) NSString *coupon_discount_amount;
@property (strong, nonatomic) NSString *credits_point_total;
@property (strong, nonatomic) NSString *credits_amount;
@property (strong, nonatomic) NSString *waiting_pay_total;

///按钮
@property (strong, nonatomic) NSNumber *is_waiting_payment;
@property (strong, nonatomic) NSNumber *can_cancel;

//订单支付界面所需
@property (strong, nonatomic) NSString *user_cash;
@property (strong, nonatomic) NSNumber *show_chongzhi_link;

-(instancetype) initWithDic:(NSDictionary *) dic;

@end

//"id": "33",
//"daigou_order_no": "DG100033",
//"status": "waiting_payment",
//"status_label": "待付款",
//"is_waiting_payment": true,
//"can_cancel": true,
//"created_at": "2016-05-25",
//"total_qty": "1",
//"sub_total": "¥1.00",
//"agency_fee": "¥0.00",
//"coupon_code": "",
//"coupon_discount_amount": "¥0.00",
//"credits_point_total": "0",
//"credits_amount": "¥0.00",
//"grand_total": "¥1.00",
//"paid_total": "¥0.00",
//"paid_at": null,
//"waiting_pay_total": "¥1.00",
//"custom_remark": "",