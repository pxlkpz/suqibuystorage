//
//  SQorderInformationLabelCell.h
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQorderInformationLabelCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellLabel;

@end
