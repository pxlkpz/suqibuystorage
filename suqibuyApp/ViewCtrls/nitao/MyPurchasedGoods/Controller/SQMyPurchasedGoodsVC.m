//
//  SQMyPurchasedGoodsVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/25.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQMyPurchasedGoodsVC.h"
#import "SQMyPurchasedGoodsModel.h"

#import "SQFillShopListCell.h"

#import "SQOrderInformationVC.h"


static NSString *identifier = @"SQFillShopListCell";

@interface SQMyPurchasedGoodsVC ()<UITableViewDataSource,UITableViewDelegate>
{
    __weak IBOutlet UITableView *listTableView;
    NSInteger currentPage;
    
    NSMutableArray *dataArray;
    
    UILabel* noItemLabel;
}

@end

@implementation SQMyPurchasedGoodsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"代购订单列表";
    
    [self buildclearView];
    
    listTableView.dataSource = self;
    listTableView.delegate = self;
    listTableView.hidden = YES;
    listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIView *view =[ [UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [listTableView setTableFooterView:view];
    UINib *nib = [UINib nibWithNibName:identifier bundle:nil];
    [listTableView registerNib:nib forCellReuseIdentifier:identifier];
    
    if (iPhone6P_D) {
        if ([listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
    @weakify(self);
    
    
    listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:1];
    }];
    
    listTableView.mj_footer  = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:currentPage+1];
    }];
    listTableView.mj_footer.hidden = YES;
    
    
    
    dataArray = [[NSMutableArray alloc] init];
    
    currentPage = 1;
    [self requestData:1];
}

-(void)buildclearView{
    
    noItemLabel = [[UILabel alloc] init];
    noItemLabel.text = @"当前列表为空\n马上开始创建吧！";
    noItemLabel.hidden = NO;
    noItemLabel.textColor = [UIColor colorWithHexString:text_gray];
    noItemLabel.textAlignment = NSTextAlignmentCenter;
    noItemLabel.font = [UIFont systemFontOfSize:14];
    noItemLabel.numberOfLines = 2;
    [self.view addSubview:noItemLabel];
    [noItemLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
    }];
    
}

#pragma mark -请求列表数据
-(void) requestData:(NSInteger ) page
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSString *pageString = [NSString stringWithFormat:@"%ld",(long)page];
    NSDictionary *dic = @{@"user_token":user_token,@"page":pageString};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAIGOU_LIST WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if (page == 1)
                        {
                            [dataArray removeAllObjects];
                            if ([items count] != 0)
                            {
                                listTableView.hidden =NO;
                                noItemLabel.hidden = YES;
                                
                            }
                            
                        }
                        else
                        {
                            if ([items count] == 0)
                            {
                                showDefaultPromptTextHUD(@"没有更多内容了");
                                [listTableView.mj_footer endRefreshingWithNoMoreData];
                                
                                return ;
                                
                            }
                            else
                            {
                                currentPage++;
                            }
                        }
                        NSArray *array = items;
                        NSInteger count = [array count];
                        
                        for (NSInteger i = 0; i < count; i++)
                        {
                            if (array[i] != nil && array[i] != [NSNull null] && [array[i] isKindOfClass:[NSDictionary class]])
                            {
                                SQMyPurchasedGoodsModel *model = [[SQMyPurchasedGoodsModel alloc] initWidthDic:array[i]];
                                [dataArray addObject:model];
                            }
                        }
                        
                        listTableView.mj_footer.hidden = NO;
                        
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [listTableView.mj_header endRefreshing];
        [listTableView.mj_footer endRefreshing];
        [listTableView reloadData];
        if (page == 1)
        {
            [listTableView setContentOffset:CGPointMake(0, 1) animated:YES];
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [listTableView.mj_header endRefreshing];
        [listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [listTableView.mj_header endRefreshing];
        [listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark -进入订单详情
-(void) orderInformation:(UITapGestureRecognizer *) tap
{
    SQMyPurchasedGoodsModel *model = dataArray[tap.view.tag];
    SQOrderInformationVC *controller = [[SQOrderInformationVC alloc] init];
    controller.order_no = model.daigou_order_no;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}


#pragma mark- tableviewdelegate
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataArray count];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SQMyPurchasedGoodsModel *model = dataArray[section];
    return [model.goods count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SQFillShopListCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.bottomLineLeftDistance = 15;
    cell.bottomLine.hidden = NO;
    SQMyPurchasedGoodsModel *model = dataArray[indexPath.section];
    SQGoodsModel *goods = model.goods[indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSURL *url = [NSURL URLWithString:goods.item_thumb];
    [cell.itemImageView sd_setImageWithURL:url placeholderImage:nil];
    
    cell.itemNameLabel.text = goods.item_name;
    
    cell.itemPriceLabel.text = [goods.item_purchasing_price gtm_stringByUnescapingFromHTML];
    
    cell.itemNumberLabel.text = [NSString stringWithFormat:@"x%@",goods.item_qty];
    
    cell.itemDescriptionLabel.text = goods.item_params;
    
    if (indexPath.row == [model.goods count] - 1)
    {
        cell.bottomLine.hidden = YES;
    }
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SQMyPurchasedGoodsModel *model = dataArray[section];
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];
    view.tag = section;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(orderInformation:)];
    [view addGestureRecognizer:tap];
    
    UILabel *label= [[UILabel alloc] init ];
    label.text = [NSString stringWithFormat:@" 订单号:%@   ",model.daigou_order_no];
    label.font = [UIFont systemFontOfSize:16];
    
    label.textColor = [UIColor blackColor];
    
    [label sizeToFit];
    //label.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(@15);
        make.height.equalTo(@30);
    }];
    
    UILabel *statusLabel= [[UILabel alloc] init];
    statusLabel.text = model.status_label;
    [statusLabel sizeToFit];
    statusLabel.font = [UIFont systemFontOfSize:13];
    if ([model.status_label isEqualToString:@"待付款"])
    {
        statusLabel.textColor = [UIColor colorWithHexString:pink_color];
    }
    else if([model.status_label isEqualToString:@"已取消"])
    {
        statusLabel.textColor = [UIColor colorWithHexString:gray];
    }
    else
    {
        statusLabel.textColor = [UIColor colorWithHexString:order_status_green];
        
    }
    
    
    [view addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-5);
        make.centerY.equalTo(label);
    }];
    
    
    UIView *view1 = [[UIView alloc]init];
    [view addSubview:view1];
    view1.alpha = 0.6;
    view1.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view);
        make.height.mas_equalTo(1/[UIScreen mainScreen].scale);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    return view;
}

-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];
    view.tag = section;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(orderInformation:)];
    [view addGestureRecognizer:tap];
    
    UILabel *statusLabel= [[UILabel alloc] init];
    //                           WithFrame:CGRectMake(0, 0, screen_width, 40)];
    statusLabel.text = @"查看订单详情";
    [statusLabel sizeToFit];
    statusLabel.textAlignment = NSTextAlignmentRight;
    statusLabel.font = [UIFont systemFontOfSize:13];
    statusLabel.textColor = [UIColor colorWithHexString:button_blue];
    [view addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-5);
        make.centerY.equalTo(view);
    }];
    
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view);
        make.height.mas_equalTo(1/[UIScreen mainScreen].scale);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    return view;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 30;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

@end
