//
//  MBZPKTools.m
//  MBPeopleBaseApp
//
//  Created by molbase on 16/4/14.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import "MBZPKTools.h"
#import "MBProgressHUD.h"

@implementation MBZPKTools

#pragma mark - 测试手机号码的合法性
+ (BOOL)checkPhoneNumber:(NSString *)number
{
    if (number.length != 11)
    {
        return NO;
    }
    /**
     * 手机号码:
     * 13[0-9], 14[5,7], 15[0, 1, 2, 3, 5, 6, 7, 8, 9], 17[6, 7, 8], 18[0-9], 170[0-9]
     * 移动号段: 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
     * 联通号段: 130,131,132,155,156,185,186,145,176,1709
     * 电信号段: 133,153,180,181,189,177,1700
     */
    NSString *MOBILE = @"^1(3[0-9]|4[57]|5[0-35-9]|8[0-9]|70)\\d{8}$";
    /**
     * 中国移动：China Mobile
     * 134,135,136,137,138,139,150,151,152,157,158,159,182,183,184,187,188,147,178,1705
     */
    NSString *CM = @"(^1(3[4-9]|4[7]|5[0-27-9]|7[8]|8[2-478])\\d{8}$)|(^1705\\d{7}$)";
    /**
     * 中国联通：China Unicom
     * 130,131,132,155,156,185,186,145,176,1709
     */
    NSString *CU = @"(^1(3[0-2]|4[5]|5[56]|7[6]|8[56])\\d{8}$)|(^1709\\d{7}$)";
    /**
     * 中国电信：China Telecom
     * 133,153,180,181,189,177,1700
     */
    NSString *CT = @"(^1(33|53|77|8[019])\\d{8}$)|(^1700\\d{7}$)";
    
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:number] == YES)
        || ([regextestcm evaluateWithObject:number] == YES)
        || ([regextestct evaluateWithObject:number] == YES)
        || ([regextestcu evaluateWithObject:number] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

#pragma mark - 测试密码的合法性
+ (BOOL)checkPassword:(NSString *) password
{
    NSString *pattern = @"[a-zA-Z0-9|-|`|=|\\|\\[|\\]|;|'|,|.|/|~|!|@|#|$|%|^|&|*|(|)|_|+|\\||{|}|:|\"|<|>|?]{6,20}";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:password];
    return isMatch;
    //    [-`=\\\[\];',./~!@#$%^&*()_+|{}:\"<>?]
}

#pragma mark - 测试真实姓名的合法性(只有中英文)
+ (BOOL)checkRealname:(NSString *) realname
{
    NSString *regex = @"^[\u4e00-\u9fa5a-zA-Z]+$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    BOOL isMatch = [predicate evaluateWithObject:realname];
    return isMatch;
}

#pragma mark - 测试邮箱合法性
+ (BOOL) checkEmail:(NSString *)email
{
    NSString *regex = @"^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regex];
    BOOL isMatch = [predicate evaluateWithObject:email];
    return isMatch;
}

#pragma mark - html转NSString
//Unicode转UTF-8

+ (NSString *)encodeToPercentEscapeString: (NSString *) input

{
    
    // Encode all the reserved characters, per RFC 3986
    
    // (<http://www.ietf.org/rfc/rfc3986.txt>)
    
    NSString *outputStr = (NSString *)
    
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              
                                                              (CFStringRef)input,
                                                              
                                                              NULL,
                                                              
                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              
                                                              kCFStringEncodingUTF8));  
    
    return outputStr;  
    
}
@end
