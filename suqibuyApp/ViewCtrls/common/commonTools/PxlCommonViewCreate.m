//
//  PxlCommonViewCreate.m
//  suqibuyApp
//
//  Created by pxl on 16/9/20.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "PxlCommonViewCreate.h"
#import <objc/message.h>

@implementation PxlCommonViewCreate


-(void)pxl_buildUI:(NSArray*)array andSuperView:(UIView*)superView{
    
    CGFloat width = (screen_width - 2)/3;
    CGFloat height = (screen_width - 2)/3+20;
    
    for (NSInteger i = 0; i < array.count; i++) {
        
        [self pxl_creatButtonWithData:[array objectAtIndex:i] andNumber:i andSuperView:superView];
    }
    NSInteger count1;
    if (array.count%3 == 0) {
        count1 = array.count/3;
    }else{
        count1 = array.count/3  + 1;
    }
    
    for (NSInteger num=0; num < count1; num++) {
        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, height * (num+1), screen_width, 1)];
        line1.backgroundColor = [UIColor colorWithHexString:seperator_color];
        [superView addSubview:line1];
        
     }
    
    for (NSInteger num1 = 0; num1 < 2 ; num1++) {
        UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake( width * (num1+1) , 0, 1, height*count1)];
        line2.backgroundColor = [UIColor colorWithHexString:seperator_color];
        [superView addSubview:line2];
    }
}

-(void)pxl_creatButtonWithData:(NSDictionary*)dictionary andNumber:(NSInteger)num andSuperView:(UIView*)superView{
    
    CGFloat width = (screen_width)/3;
    CGFloat height = (screen_width)/3+20;
    
    CGFloat distance = width/4;
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake((num%3) * width , num/3 * (height), width, height)];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    button.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.numberOfLines = 0;
    [button setTitleColor:[UIColor colorWithHexString:text_color] forState:UIControlStateNormal];
    
    [button setTitle:dictionary[@"title"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:dictionary[@"imageName"]] forState:UIControlStateNormal];
    objc_setAssociatedObject(button, @"buttonKey", dictionary[@"buttonId"], OBJC_ASSOCIATION_COPY_NONATOMIC);
    [button addTarget:self action:@selector(buttonAciton:) forControlEvents:UIControlEventTouchUpInside];
    
    
     CGFloat imageWidth = button.imageView.frame.size.width;
    
    if (IPHONE_6 || IPHONE_6P) {
        [button setImageEdgeInsets:UIEdgeInsetsMake(distance+8, distance * 2 - imageWidth/2, 0, 0)];
    }else{
        [button setImageEdgeInsets:UIEdgeInsetsMake(distance+5, distance * 2 - imageWidth/2, 0, 0)];
    }
    [button setTitleEdgeInsets:UIEdgeInsetsMake(distance + width/2+20, - imageWidth, distance/2, 0)];
    
    [button setNeedsLayout];
    [button layoutIfNeeded];
    CGFloat titleLabelWidth = (width - button.titleLabel.frame.size.width)/2;
    CGFloat titleLabelHeight = button.titleLabel.frame.size.height/2;
    
    [button setTitleEdgeInsets:UIEdgeInsetsMake(distance + width/2+22 -titleLabelHeight ,-imageWidth +titleLabelWidth, distance/2 + 10, 0)];
    
    [superView addSubview:button];
    
}

-(void)buttonAciton:(UIButton*)sender{
    
    NSString *buttonId = objc_getAssociatedObject(sender, @"buttonKey");
    if ([self.delegate respondsToSelector:@selector(pxl_CommonViewCreateDelegate:)]) {
        [self.delegate pxl_CommonViewCreateDelegate:buttonId];
    }
 }

+(NSArray*)pxl_getPlistData:(NSString*)resourceName{
    NSString * zhuanyunUIDataPath = [[NSBundle mainBundle] pathForResource:resourceName ofType:@"plist"];
    NSArray * dataArray = [[NSArray alloc] initWithContentsOfFile:zhuanyunUIDataPath];

    return dataArray;
}


@end
