//
//  PxlCommonViewCreate.h
//  suqibuyApp
//
//  Created by pxl on 16/9/20.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PxlCommonViewCreateDelegate<NSObject>

@optional
- (void)pxl_CommonViewCreateDelegate:(NSString*)pushString;

@end

@interface PxlCommonViewCreate : NSObject

-(void)pxl_buildUI:(NSArray*)array andSuperView:(UIView*)superView;

@property (weak, nonatomic) id<PxlCommonViewCreateDelegate> delegate;

+(NSArray*)pxl_getPlistData:(NSString*)resourceName;

@end
