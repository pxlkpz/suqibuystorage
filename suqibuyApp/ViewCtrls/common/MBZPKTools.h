//
//  MBZPKTools.h
//  MBPeopleBaseApp
//
//  Created by molbase on 16/4/14.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBZPKTools : NSObject

#pragma mark - 测试电话号码
+(BOOL) checkPhoneNumber:(NSString *) number;

#pragma mark - 测试密码的合法性
+ (BOOL) checkPassword:(NSString *) password;

#pragma mark - 测试真实姓名的合法性(只有中英文)
+ (BOOL) checkRealname:(NSString *) realname;

#pragma mark - 测试邮箱合法性
+(BOOL) checkEmail:(NSString *) email;

#pragma mark - html转NSString
+ (NSString *)encodeToPercentEscapeString: (NSString *) input;
@end
