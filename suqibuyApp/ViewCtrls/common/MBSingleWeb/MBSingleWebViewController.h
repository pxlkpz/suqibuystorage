//
//  MBSingleWebViewController.h
//  MBPeopleBaseApp
//
//  Created by molbase on 16/3/11.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import "SQBaseViewController.h"

@interface MBSingleWebViewController : SQBaseViewController

@property (assign, nonatomic) NSInteger type;

@end
