//
//  MBSingleWebViewController.m
//  MBPeopleBaseApp
//
//  Created by molbase on 16/3/11.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import "MBSingleWebViewController.h"

@interface MBSingleWebViewController ()

@end

@implementation MBSingleWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIWebView *webview = [[UIWebView alloc] init];
//                          WithFrame:CGRectMake(0, 0, screen_width, screen_height)];
    [self.view addSubview:webview];
    [webview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    webview.backgroundColor = [UIColor whiteColor];
    
    switch (self.type) {
        case 0:
        {
            self.title = @"用户协议";
            NSString *path = [[NSBundle mainBundle] pathForResource:@"protocal.txt" ofType:nil];
            NSData *data = [NSData dataWithContentsOfFile:path];
            [webview loadData:data MIMEType:@"text/plain" textEncodingName:@"UTF-8" baseURL:nil];
            break;
        }
        
        case 1:
        {
            self.title = @"代购物品限制";
            NSString *path = [[NSBundle mainBundle] pathForResource:@"limit.txt" ofType:nil];
            NSData *data = [NSData dataWithContentsOfFile:path];
            [webview loadData:data MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:nil];
            break;
        }
            
        case 2:
        {
            self.title = @"转运物品限制";
            NSString *path = [[NSBundle mainBundle] pathForResource:@"limit.txt" ofType:nil];
            NSData *data = [NSData dataWithContentsOfFile:path];
            [webview loadData:data MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:nil];
            break;
        }

        
        default:
            break;
    }
}

@end
