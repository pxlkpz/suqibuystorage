//
//  MBZPKTableViewCell.h
//  MBPeopleBaseApp
//
//  Created by molbase on 16/3/2.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBZPKTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView *bottomLine;
@property (nonatomic, assign) CGFloat bottomLineLeftDistance;
@property (nonatomic, assign) BOOL bottomLineHiden;

@end
