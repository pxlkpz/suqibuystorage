//
//  MBZPKTableViewCell.m
//  MBPeopleBaseApp
//
//  Created by molbase on 16/3/2.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import "MBZPKTableViewCell.h"

@implementation MBZPKTableViewCell

-(void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (self.bottomLine == nil)
    {
        self.bottomLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, rect.size.height, rect.size.width, 1/[UIScreen mainScreen].scale)];
        UIImage *image = [UIImage imageNamed:@"line_hor_icon"];
        self.bottomLine.image = image;
        self.bottomLine.backgroundColor = [UIColor colorWithHexString:seperator_color];
        [self.bottomLine setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:self.bottomLine];
        //左边约束
        NSLayoutConstraint *bottomLineLeft = [NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:self.bottomLineLeftDistance];
        //右边约束
        NSLayoutConstraint *bottomLineRight = [NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0];
        //下面约束
        NSLayoutConstraint *bottomLineBottom = [NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        //高度约束
        NSLayoutConstraint *bottomLineHeight = [NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0 constant:1/[UIScreen mainScreen].scale];
        [self addConstraint:bottomLineLeft];
        [self addConstraint:bottomLineRight];
        [self addConstraint:bottomLineBottom];
        [self addConstraint:bottomLineHeight];
        self.bottomLine.hidden = self.bottomLineHiden;
    }
}

- (void)awakeFromNib
{

}


@end
