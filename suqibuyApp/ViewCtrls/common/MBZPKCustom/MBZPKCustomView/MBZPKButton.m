//
//  MBZPKButton.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/26.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "MBZPKButton.h"

@implementation MBZPKButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(CGRect) imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(10, 10, 30, 30);
}

@end
