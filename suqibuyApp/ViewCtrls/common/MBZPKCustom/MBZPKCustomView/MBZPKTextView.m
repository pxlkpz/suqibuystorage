//
//  MBZPKTextView.m
//  MBPeopleBaseApp
//
//  Created by molbase on 16/3/10.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import "MBZPKTextView.h"

@implementation MBZPKTextView

/***********************************************************************
 * 方法名称： - (void)drawRect:(CGRect)rect
 * 功能描述： 初始化FeedbackTextView
 * 输入参数： (CGRect)rect：文本框尺寸
 * 输出参数： textContainerInset:文本框的文字位置布局 text:文本 textColor:文本颜色
 * 返 回 值： 无
 * 其它说明： 无
 ***********************************************************************/
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    self.textAlignment = NSTextAlignmentLeft;
    self.textContainerInset = UIEdgeInsetsMake(10, 10, 10, 10);
//    self.textColor = [UIColor colorWithHexString:MBgray2];
    self.textContainer.lineFragmentPadding = 0;
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor colorWithHexString:text_gray].CGColor;
    self.layer.cornerRadius = allCornerRadius;
}

@end
