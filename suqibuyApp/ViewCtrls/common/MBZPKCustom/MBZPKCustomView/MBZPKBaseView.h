//
//  MBBaseView.h
//  appContactLogin
//
//  Created by molbase on 16/1/29.
//  Copyright © 2016年 molbase. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBZPKBaseView : UIView

@property (nonatomic, strong) UIView *topLine;
@property (nonatomic, assign) CGFloat topLineLeftDistance;
@property (nonatomic, assign) BOOL topLineHiden;

@property (nonatomic, strong) UIView *bottomLine;
@property (nonatomic, assign) CGFloat bottomLineLeftDistance;
@property (nonatomic, assign) BOOL bottomLineHiden;
@end
