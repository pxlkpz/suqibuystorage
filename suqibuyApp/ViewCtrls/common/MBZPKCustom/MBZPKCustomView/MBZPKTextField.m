//
//  MBZPKTextField.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/22.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "MBZPKTextField.h"

@implementation MBZPKTextField

-(void) awakeFromNib
{
    [super awakeFromNib];
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor colorWithHexString:text_gray_666666].CGColor;
    self.layer.cornerRadius = allCornerRadius;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.layer.borderWidth = 1;
        self.layer.borderColor = [UIColor colorWithHexString:text_gray_666666].CGColor;
        self.layer.cornerRadius = allCornerRadius;

    }
    return self;
}

//控制 placeHolder 的位置，左右缩 10
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 0 );
    // CGRect inset = CGRectMake(bounds.origin.x + 10, bounds.origin.y, bounds.size.width - 10, bounds.size.height); //更好理解些
    // return inset;
}

// 控制文本的位置，左右缩 10
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 0 );
    //CGRect inset = CGRectMake(bounds.origin.x + 10, bounds.origin.y, bounds.size.width - 10, bounds.size.height);
    //  return inset;
}

@end
