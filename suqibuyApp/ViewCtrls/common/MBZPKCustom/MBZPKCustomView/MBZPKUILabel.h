//
//  MBZPKUILabel.h
//  MBPeopleBaseApp
//
//  Created by molbase on 16/2/25.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    VerticalAlignmentTop = 0, // default
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;

@interface MBZPKUILabel : UILabel

@property (nonatomic) VerticalAlignment verticalAlignment;


-(void) setAttributedString:(NSArray *) attrString  color:(NSArray *) attrColor font:(NSArray *) attrFont;
@end
