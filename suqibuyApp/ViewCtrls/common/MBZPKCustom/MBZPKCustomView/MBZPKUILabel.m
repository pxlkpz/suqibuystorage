//
//  MBZPKUILabel.m
//  MBPeopleBaseApp
//
//  Created by molbase on 16/2/25.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import "MBZPKUILabel.h"

@implementation MBZPKUILabel


#pragma mark - 设置文字排版
- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.verticalAlignment = VerticalAlignmentMiddle;
    }
    return self;
}

- (void)setVerticalAlignment:(VerticalAlignment)verticalAlignment {
    _verticalAlignment = verticalAlignment;
    [self setNeedsDisplay];
}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines {
    CGRect textRect = [super textRectForBounds:bounds limitedToNumberOfLines:numberOfLines];
    switch (self.verticalAlignment) {
        case VerticalAlignmentTop:
            textRect.origin.y = bounds.origin.y;
            break;
        case VerticalAlignmentBottom:
            textRect.origin.y = bounds.origin.y + bounds.size.height - textRect.size.height;
            break;
        case VerticalAlignmentMiddle:
            // Fall through.
        default:
            textRect.origin.y = bounds.origin.y + (bounds.size.height - textRect.size.height) / 2.0;
    }
    return textRect;
}

//-(void)drawTextInRect:(CGRect)requestedRect {
////    CGRect actualRect = [self textRectForBounds:requestedRect limitedToNumberOfLines:self.numberOfLines];
////    [super drawTextInRect:actualRect];
//    [super drawTextInRect:requestedRect];
//}




#pragma mark - 设置富文本文字
-(void) setAttributedString:(NSArray *)attrString color:(NSArray *)attrColor font:(NSArray *)attrFont
{
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:@""];
    NSInteger stringCount = [attrString count];
//    NSInteger colorCount = [attrColor count];
//    NSInteger fontCount = [attrFont count];
    
    for (NSInteger i = 0; i < stringCount; i++)
    {
        NSString *string = attrString[i];
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:string];
        [attributeString addAttribute:NSForegroundColorAttributeName value:attrColor[i] range:NSMakeRange(0, string.length)];
        [attributeString addAttribute:NSFontAttributeName value:attrFont[i] range:NSMakeRange(0, string.length)];
        [mutableAttributedString appendAttributedString:attributeString];
    }
    
    self.attributedText =mutableAttributedString;
}

@end
