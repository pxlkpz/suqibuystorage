//
//  MBBaseView.m
//  appContactLogin
//
//  Created by molbase on 16/1/29.
//  Copyright © 2016年 molbase. All rights reserved.
//

#import "MBZPKBaseView.h"


@implementation MBZPKBaseView

-(void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (self.topLine == nil)
    {
        self.topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 1/[UIScreen mainScreen].scale)];
        self.topLine.backgroundColor = [UIColor colorWithHexString:seperator_color];
        [self.topLine setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:self.topLine];
        //左边约束
        NSLayoutConstraint *topLineLeft = [NSLayoutConstraint constraintWithItem:self.topLine attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:self.topLineLeftDistance];
        //右边约束
        NSLayoutConstraint *topLineRight = [NSLayoutConstraint constraintWithItem:self.topLine attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0];
        //上面约束
        NSLayoutConstraint *topLineTop = [NSLayoutConstraint constraintWithItem:self.topLine attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        //高度约束
        NSLayoutConstraint *topLineHeight = [NSLayoutConstraint constraintWithItem:self.topLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0 constant:1/[UIScreen mainScreen].scale];
        [self addConstraint:topLineLeft];
        [self addConstraint:topLineRight];
        [self addConstraint:topLineTop];
        [self addConstraint:topLineHeight];
        self.topLine.hidden = self.topLineHiden;
    }
    
    
    
    if (self.bottomLine == nil)
    {
        self.bottomLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, rect.size.height - 1/[UIScreen mainScreen].scale, rect.size.width, 1/[UIScreen mainScreen].scale)];
        self.bottomLine.backgroundColor = [UIColor colorWithHexString:seperator_color];
        [self.bottomLine setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self addSubview:self.bottomLine];
        //左边约束
        NSLayoutConstraint *bottomLineLeft = [NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:self.bottomLineLeftDistance];
        //右边约束
        NSLayoutConstraint *bottomLineRight = [NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1 constant:0];
        //下面约束
        NSLayoutConstraint *bottomLineBottom = [NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        //高度约束
        NSLayoutConstraint *bottomLineHeight = [NSLayoutConstraint constraintWithItem:self.bottomLine attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0 constant:1/[UIScreen mainScreen].scale];
        [self addConstraint:bottomLineLeft];
        [self addConstraint:bottomLineRight];
        [self addConstraint:bottomLineBottom];
        [self addConstraint:bottomLineHeight];
        self.bottomLine.hidden = self.bottomLineHiden;
    }
}

@end
