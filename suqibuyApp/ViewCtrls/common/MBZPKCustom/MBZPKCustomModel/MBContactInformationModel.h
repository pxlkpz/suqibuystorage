//
//  MBContactInformation.h
//  MBPeopleBaseApp
//
//  Created by molbase on 16/3/3.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBContactInformationModel : NSObject

@property (strong, nonatomic) NSString *name;
@property (assign, nonatomic) NSInteger identity;
@property (strong, nonatomic) NSString *xiao;
@property (strong, nonatomic) NSString *cai;

@property (assign, nonatomic) BOOL select;

@property (assign, nonatomic, readonly) CGFloat xiaoHeight;
@property (assign, nonatomic, readonly) CGFloat caiHeight;
@property (assign, nonatomic, readonly) CGFloat cellHeight;

-(instancetype) init:(NSDictionary *)dic;

@end
