//
//  MBZPKBaseModel.h
//  MBPeopleBaseApp
//
//  Created by molbase on 16/3/8.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MBZPKBaseModel : NSObject

///测试字典里是否存在此字符串
-(NSString *) testNSStringInDic:(NSDictionary *)dic string:(NSString *) string;

#pragma mark -测试字典里是否存在此数字
-(NSNumber *)testNSNumberInDic:(NSDictionary *)dic string:(NSString *)string;

///测试是否是字符串
-(NSString *) testNSString:(id)test;

///测试是否是字典
-(NSDictionary *) testNSDictionary:(id)test;

#pragma mark -测试数据是否是数字 包含bool
-(NSNumber *) testNSNumber:(id)test;

///测试是否是数组
-(NSArray *) testNSArray:(id)test;
@end
