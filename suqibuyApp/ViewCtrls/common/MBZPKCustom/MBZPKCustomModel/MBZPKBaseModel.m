//
//  MBZPKBaseModel.m
//  MBPeopleBaseApp
//
//  Created by molbase on 16/3/8.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import "MBZPKBaseModel.h"

@implementation MBZPKBaseModel

#pragma mark -测试字典里是否存在此字符串
-(NSString *)testNSStringInDic:(NSDictionary *)dic string:(NSString *)string
{
    id test = [dic objectForKey:string];
    NSString *result = [self testNSString:test];
    return result;
}

#pragma mark -测试字典里是否存在此数字
-(NSNumber *)testNSNumberInDic:(NSDictionary *)dic string:(NSString *)string
{
    id test = [dic objectForKey:string];
    NSNumber *result = [self testNSNumber:test];
    return result;
}

#pragma mark -测试数据是否是字符串
-(NSString *) testNSString:(id) test
{
    if (test != nil && test != [NSNull null] && [test isKindOfClass:[NSString class]])
    {
        return (NSString *) test;
    }
    else
    {
        return @"";
    }
}

#pragma mark -测试数据是否是字典
-(NSDictionary *) testNSDictionary:(id)test
{
    if (test != nil && test != [NSNull null] && [test isKindOfClass:[NSDictionary class]])
    {
        return (NSDictionary *) test;
    }
    else
    {
        return nil;
    }
}

#pragma mark -测试数据是否是数字 包含bool
-(NSNumber *) testNSNumber:(id)test
{
    if (test != nil && test != [NSNull null] && [test isKindOfClass:[NSNumber class]])
    {
        return (NSNumber *) test;
    }
    else
    {
        return nil;
    }
}

#pragma mark -测试数据是否是数组
-(NSArray *) testNSArray:(id)test
{
    if (test != nil && test != [NSNull null] && [test isKindOfClass:[NSArray class]])
    {
        return (NSArray *) test;
    }
    else
    {
        return nil;
    }
}

@end
