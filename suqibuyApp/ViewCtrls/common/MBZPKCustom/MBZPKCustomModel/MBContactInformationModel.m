//
//  MBContactInformation.m
//  MBPeopleBaseApp
//
//  Created by molbase on 16/3/3.
//  Copyright © 2016年 MOLBASE. All rights reserved.
//

#import "MBContactInformationModel.h"
#import "MBZPKUILabel.h"

@interface MBContactInformationModel()

@property (readwrite, nonatomic) CGFloat xiaoHeight;
@property (readwrite, nonatomic) CGFloat caiHeight;
@property (readwrite, nonatomic) CGFloat cellHeight;

@end

@implementation MBContactInformationModel


-(instancetype) init:(NSDictionary *)dic
{
    self = [super init];
    if (self)
    {
        if ([dic isKindOfClass:[NSDictionary class]])
        {
            id name = [dic objectForKey:@"name"];
            id identity = [dic objectForKey:@"identity"];
            id xiao = [dic objectForKey:@"xiao"];
            id cai  = [dic objectForKey:@"cai"];
            
            self.name = [self testNSString:name];
            
            if ([self testNSString:identity] != nil)
            {
                self.identity = [[self testNSString:identity] integerValue];
            }
            else
            {
                self.identity = 0;
            }
            self.xiao = [self testNSString:xiao];
            self.cai  = [self testNSString:cai];
            self.select = NO;
            
            self.xiaoHeight = [self xiaoHeight];
            self.caiHeight  = [self caiHeight];
            self.cellHeight = [self cellHeight];
        }
    }
    return self;
}

-(CGFloat) xiaoHeight
{
    MBZPKUILabel *label = [[MBZPKUILabel alloc] initWithFrame:CGRectMake( 65 + 13 + 10, 65, screen_width - 15 - 88, CGFLOAT_MAX)];
    label.text = _xiao;
    label.textColor = [UIColor colorWithHexString:MBgray1];
    label.font = [UIFont systemFontOfSize:12];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 2;
    CGSize size = CGSizeMake(screen_width - 15 - 88, CGFLOAT_MAX);
    CGSize expect = [label sizeThatFits:size];
    return expect.height;
}

-(CGFloat) caiHeight
{
    CGFloat y = 65 + 10 + self.xiaoHeight;
    MBZPKUILabel *label = [[MBZPKUILabel alloc] initWithFrame:CGRectMake( 65 + 13 + 10, y, screen_width - 15 - 88, CGFLOAT_MAX)];
    label.text = _cai;
    label.textColor = [UIColor colorWithHexString:MBgray1];
    label.font = [UIFont systemFontOfSize:12];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 2;
    CGSize size = CGSizeMake(screen_width - 15 - 88, CGFLOAT_MAX);
    CGSize expect = [label sizeThatFits:size];
    return expect.height;
}

-(CGFloat) cellHeight
{
    return self.caiHeight + self.xiaoHeight + 65 + 10 + 15;
}

#pragma mark - 私有方法
#pragma mark -测试数据是否合法
-(NSString *) testNSString:(id) test
{
    if (test != nil && test != [NSNull null] && [test isKindOfClass:[NSString class]])
    {
        return (NSString *) test;
    }
    else
    {
        return @"";
    }
}

@end
