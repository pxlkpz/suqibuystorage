//
//  ShipguideViewController.swift
//  suqibuyApp
//
//  Created by pxl on 2017/3/25.
//  Copyright © 2017年 Peike Zhang. All rights reserved.
//

import UIKit

class ShipguideViewController: SQBaseViewController {
    @IBOutlet weak var shipimageView: UIImageView!

    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        let size1:CGSize = #imageLiteral(resourceName: "shipguide").size
        let r = kScreenW/size1.width
        scrollViewHeight.constant = r * size1.height
        self.title = "转运物品限制";

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}
