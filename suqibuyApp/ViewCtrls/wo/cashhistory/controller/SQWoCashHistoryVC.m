//
//  SQWoCashHistoryVC.m
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoCashHistoryVC.h"
#import "SQWoCashHistoryModel.h"
#import "SQWoCashHistoryCell.h"

@interface SQWoCashHistoryVC ()<UITableViewDataSource,UITableViewDelegate>{
    
    NSMutableArray *dataArray;
    UIButton    *rightButton;
}
@property(nonatomic, strong) UITableView *listTableView;
@property (nonatomic, assign) NSInteger currentPage;

@end

@implementation SQWoCashHistoryVC



#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"余额历史";
    
    rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    rightButton.titleLabel.font = [UIFont boldSystemFontOfSize:15.0];
    CGSize size = [@"我的余额" sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:16.0]}];
    [rightButton setFrame:CGRectMake(0, 0, MAX((size.width+30),40), MAX(30,size.height))];
    [rightButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 25, 0, -12)];
    
    [rightButton setTitle:@"我的余额" forState:UIControlStateNormal];
    UIBarButtonItem *m_buttonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    self.navigationItem.rightBarButtonItem = m_buttonItem;
    

    rightButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    rightButton.enabled = NO;
    [rightButton sizeToFit];
    
    dataArray = [NSMutableArray array];
    [self buildclearView];
    
    [self buildTableView];
    
    self.currentPage = 1;
    [self requestData:1];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
}
#pragma mark - ---- event response 🍐🌛

#pragma mark - ---- private methods 🍊🌜

-(void)buildclearView{
    
    UILabel* noItemLabel = [[UILabel alloc] init];
    noItemLabel.text = @"当前列表为空\n马上开始创建吧！";
    noItemLabel.textColor = [UIColor colorWithHexString:text_gray];
    noItemLabel.textAlignment = NSTextAlignmentCenter;
    noItemLabel.font = [UIFont systemFontOfSize:14];
    noItemLabel.numberOfLines = 2;
    [self.view addSubview:noItemLabel];
    [noItemLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
    }];
    
}
-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.hidden = YES;
    _listTableView.delegate = self;
    if ([UIDevice currentDevice].systemVersion.integerValue > 7) {
        _listTableView.rowHeight = UITableViewAutomaticDimension;
    }
    _listTableView.estimatedRowHeight = 80.0f;
    //[_listTableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.view);
    }];
    [_listTableView setTableFooterView:[ [UIView alloc]init]];
    
    @weakify(self);
    
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        self.currentPage = 1;
        [self requestData:1];
    }];
    
    _listTableView.mj_footer  = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:self.currentPage+1];
    }];
    _listTableView.mj_footer.hidden = YES;
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}
#pragma mark -请求列表数据
-(void) requestData:(NSInteger ) page
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSString *pageString = [NSString stringWithFormat:@"%ld",(long)page];
    NSDictionary *dic = @{@"user_token":user_token,@"page":pageString};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_cashhistory_list WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    NSString *remain_cash = [NSString stringWithFormat:@"余额:%@",[[result objectForKey:@"remain_cash"] gtm_stringByUnescapingFromHTML]];
                    [rightButton setTitle:remain_cash forState:UIControlStateNormal];
                    [rightButton sizeToFit];
                    
                    
                    
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if (page == 1)
                        {
                            [dataArray removeAllObjects];
                        }
                        else
                        {
                            if ([items count] == 0)
                            {
                                showDefaultPromptTextHUD(@"没有更多内容了");
                            }
                            else
                            {
                                self.currentPage ++;
                            }
                        }
                        
                        _listTableView.hidden = NO;
                        
                        
                        [dataArray addObjectsFromArray:[SQWoCashHistoryModel mj_objectArrayWithKeyValuesArray:items]];
                        [_listTableView reloadData];
                        _listTableView.mj_footer.hidden = NO;
                        
                    }else{
                        if ([result[@"total_results"] integerValue] != 0) {
                            showDefaultPromptTextHUD(@"没有更多内容了");
                            [self.listTableView.mj_footer endRefreshingWithNoMoreData];
                            
                            return ;
                            
                        }
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark - ---- delegate 🍎🌝
#pragma mark- tableviewdelegate
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"SQWoCashHistoryCell";
    
    SQWoCashHistoryCell *cell = (SQWoCashHistoryCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQWoCashHistoryCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQWoCashHistoryCell class]])
            {
                cell = (SQWoCashHistoryCell*)nib;
                break;
            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setupData:dataArray[indexPath.row]];
    
    return cell;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // iOS 8 的Self-sizing特性
    return UITableViewAutomaticDimension;
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

#pragma mark - ---- getters and setters 🍋🌞


@end
