//
//  SQWoCashHistoryModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWoCashHistoryModel : NSObject
//"": "23",
//"changetype": "代购订单退款",
//"type": "nochange",
//"amount": "¥0.00",
//"note": "[代购订单退款] : 代购订单取消返还余额。",
//"created_at": "2016-05-26 13:17:06"


@property (nonatomic, copy) NSString* my_id;
@property (nonatomic, copy) NSString* changetype;
@property (nonatomic, copy) NSString* type;
@property (nonatomic, copy) NSString* amount;
@property (nonatomic, copy) NSString* note;
@property (nonatomic, copy) NSString* created_at;

@end
