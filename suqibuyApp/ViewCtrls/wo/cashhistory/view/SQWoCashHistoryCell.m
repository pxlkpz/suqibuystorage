//
//  SQWoCashHistoryCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoCashHistoryCell.h"
#import "SQWoCashHistoryModel.h"
@interface SQWoCashHistoryCell()
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

@end
@implementation SQWoCashHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width - 4 * 3; // 44 = avatar宽度，4 * 3为padding
    self.bottomLabel.preferredMaxLayoutWidth = preferredMaxWidth;
    [self.bottomLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    self.stateLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(SQWoCashHistoryModel *)dataEntity {
    self.topLabel.text = dataEntity.created_at;
    self.bottomLabel.text = dataEntity.note;
    self.stateLabel.text = [dataEntity.amount gtm_stringByUnescapingFromHTML];
    if ([dataEntity.type isEqualToString:@"decrease"]) {
        self.stateLabel.backgroundColor = KColorFromRGB(0x0caa17);
    }else if ([dataEntity.type isEqualToString:@"increase"]){
        self.stateLabel.backgroundColor = KColorFromRGB(0xff0000);
    }else if ([dataEntity.type isEqualToString:@"nochange"]){
        self.stateLabel.backgroundColor = KColorFromRGB(0xd9dada);
    }
    
    self.stateLabel.highlightedTextColor = [UIColor blueColor];
//    <color name="item_text_status_canceled_color">#d9dada</color>
//    <color name="type_decrease">#0caa17</color>
//    <color name="type_increase">#ff0000</color>
    
    
}

@end
