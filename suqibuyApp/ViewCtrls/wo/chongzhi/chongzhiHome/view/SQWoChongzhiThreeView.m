//
//  SQWoChongzhiThreeView.m
//  suqibuyApp
//
//  Created by apple on 16/6/13.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoChongzhiThreeView.h"
#import "MBZPKTextField.h"
#import "SQWoChongzhiThreeModel.h"
#import "SQWoChongzhiPayidModel.h"

//支付宝
#import "Order.h"
#import "DataSigner.h"
#import <AlipaySDK/AlipaySDK.h>
#import "PxlTextView.h"
//微信
#import "WXApi.h"



@interface SQWoChongzhiThreeView ()<UITextFieldDelegate, UIPopoverControllerDelegate>
{
    UILabel *_changAfterMoneyLabel;
    MBZPKTextField *_textfield;
    SQWoChongzhiPayidModel* globalModel;
    UIButton *_sureButton;
    UIView *_bottomView;
    UIButton* _againButton;
    UILabel *_bottomViewLabel1;
}

@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;
//@property(nonatomic, strong, readwrite) NSString *environment;
//@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;

@end

@implementation SQWoChongzhiThreeView
#pragma mark - ---- lefe cycle 🍏🌚

- (BOOL)acceptCreditCards {
    return self.payPalConfig.acceptCreditCards;
}

- (void)setAcceptCreditCards:(BOOL)acceptCreditCards {
    self.payPalConfig.acceptCreditCards = acceptCreditCards;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"%@充值",_model.payment_method];
    
    //    PayPalConfiguration.ENVIRONMENT_SANDBOX
    //    PayPalConfiguration
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PayResult:) name:@"WEICHATPAYRESULT" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(AlipayPayResult:) name:@"AlipayWEICHATPAYRESULT" object:nil];
    
    
    [self buildTopView];
    
    //    [self createThreeCellNumber:250];
    
    // Set up payPalConfig
    _payPalConfig = [[PayPalConfiguration alloc] init];
#if HAS_CARDIO
    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
    // for more details.
    _payPalConfig.acceptCreditCards = YES;
#else
    _payPalConfig.acceptCreditCards = NO;
#endif
    _payPalConfig.merchantName = @"Awesome Shirts, Inc.";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    
    // Setting the languageOrLocale property is optional.
    //
    // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
    // its user interface according to the device's current language setting.
    //
    // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
    // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
    // to use that language/locale.
    //
    // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
    
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    
    // Setting the payPalShippingAddressOption property is optional.
    //
    // See PayPalConfiguration.h for details.
    
    _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //    self.successView.hidden = YES;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    // Preconnect to PayPal early
    [self setPayPalEnvironment:self.environment];
}

- (void)setPayPalEnvironment:(NSString *)environment {
    self.environment = environment;
    [PayPalMobile preconnectWithEnvironment:environment];
}

#pragma mark - ---- event response 🍐🌛


-(void)againButtonAction{
    
    if ([_model.payment_code isEqualToString:@"payal_normal"]) {
        
        [self requestGetpayment];
    }else{
        [self requstIsPayconfirm:globalModel.transaction_id];
    }
    
}
/**
 *  充值按钮
 *
 *  @param sender uibutton
 */
-(void)SurebuttonAction:(UIButton*)sender{
    
    if ([_textfield.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入充值金额");
        return;
    }
    else if ([_textfield.text doubleValue] <= 0) {
        showDefaultPromptTextHUD(@"充值的实际到账金额不能少于0");
        return;
    }
    [self requestData];
    
}

-(void)valueChanged:(UITextField*)textfield{
    NSLog(@"%@",textfield.text);
    if ([_model.payment_code isEqualToString:@"payal_normal"]) {
        _changAfterMoneyLabel.text =[NSString stringWithFormat:@"%@ 元", [self isNormalChange:textfield.text]];
    }else{
        if ([textfield.text isEqualToString:@""] ||[textfield.text floatValue] < 0) {
            _changAfterMoneyLabel.text =[NSString stringWithFormat:@"0 元"];
            
        }else{
            _changAfterMoneyLabel.text =[NSString stringWithFormat:@"%@ 元", textfield.text];
        }
    }
}
#pragma mark - ---- private methods 🍊🌜


-(void) requestData
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"payment_method":_model.payment_code,@"money":_textfield.text};
    
    //    _textfield.text
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_recharge_prepaid WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    globalModel = [SQWoChongzhiPayidModel mj_objectWithKeyValues:result];
                    if ([globalModel.payment_code isEqualToString:@"alipay_mormal"]) {
                        [self alipay:globalModel];
                    }else if ([globalModel.payment_code isEqualToString:@"payal_normal"]){
                        [self paypal:globalModel];
                    }else{
                        [self requestDataWithWX:globalModel.transaction_id];
                        //                        [self sendPayWithparameter:model];
                    }
                    
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


-(void) requestDataWithWX:(NSString*)orderNo
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"order_no":orderNo};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_weixinpayment_pay WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    [self sendPayWithparameter:result];
                    
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}



/**
 *  印证是否充值成功
 */
-(void) requstIsPayconfirm:(NSString *)order_no
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"order_no":order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_recharge_verify WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    showDefaultPromptTextHUD(result[@"info"]);
                    if ([result[@"state"] isEqualToString:@"approved"]) {
                        _bottomViewLabel1.text = result[@"info"];
                        _sureButton.hidden = YES;
                        _againButton.hidden = YES;
                        _bottomView.hidden = NO;
                        
                    }else{
                        _sureButton.hidden = YES;
                        _againButton.hidden = NO;
                        _bottomView.hidden = YES;
                        
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void) requestGetpayment
{
    
    if (!globalModel.pay_id) {
        globalModel.pay_id = @"";
    }
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"transaction_id":globalModel.transaction_id, @"pay_id":globalModel.pay_id};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_paypalwebhook_getpayment WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    showDefaultPromptTextHUD(result[@"info"]);
                    if ([result[@"state"] isEqualToString:@"approved"]) {
                        _bottomViewLabel1.text = result[@"info"];
                        
                        _sureButton.hidden = YES;
                        _againButton.hidden = YES;
                        _bottomView.hidden = NO;
                        
                    }else{
                        _sureButton.hidden = YES;
                        _againButton.hidden = NO;
                        _bottomView.hidden = YES;
                        
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}




-(void)buildTopView{
    
    UIImageView*imageView = [[UIImageView alloc] init ];
    [self.view addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(10);
        make.top.equalTo(self.view).offset(10);
        make.size.mas_equalTo(CGSizeMake(82, 28));
    }];
    
    if ([_model.payment_code isEqualToString:@"alipay_mormal"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_alipay_icon"];
        
    }else if ([_model.payment_code isEqualToString:@"weixin"]){
        imageView.image = [UIImage imageNamed:@"chong_zhi_weixin_icon"];
        
    }else{
        imageView.image = [UIImage imageNamed:@"chong_zhi_paypal_icon"];
        
    }
    
    UILabel *label1 = [UILabel new];
    [label1 sizeToFit];
    [label1 setFont:[UIFont systemFontOfSize:13]];
    label1.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [self.view addSubview:label1];
    label1.numberOfLines = 0;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[_model.payment_description stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"]];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:10];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [[_model.payment_description stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"] length])];
    label1.attributedText = attributedString;
    
    //    @"账户余额充值订单详情";
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
    }];
    
    MASViewAttribute* boom = label1.mas_bottom;
    
    _textfield = [self createThreeCellNumber:boom];
    _textfield.keyboardType = UIKeyboardTypeDecimalPad;
    
    //***隐藏部分
    
    [self buildBottom];
    
    
}


-(void)buildBottom{
    
    _againButton = [[UIButton alloc] init];
    _againButton.hidden = YES;
    [_againButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_againButton setTitle:@"再次检查充值结果" forState:UIControlStateNormal];
    _againButton.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [_againButton setBackgroundColor:KColorRGB(3, 132, 221)];
    _againButton.layer.cornerRadius = allCornerRadius;
    [_againButton addTarget:self action:@selector(againButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_againButton];
    [_againButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_changAfterMoneyLabel.mas_bottom).offset(20);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.left.equalTo(self.view).offset(10);
        make.height.equalTo(@40);
    }];
    
    
    _bottomView = [[UIView alloc] init];
    _bottomView.backgroundColor = KColorFromRGB(0xF7F7F7);
    _bottomView.layer.borderColor = [UIColor colorWithHexString:border_color].CGColor;
    _bottomView.layer.borderWidth = 1;
    _bottomView.layer.cornerRadius = allCornerRadius;
    [self.view addSubview:_bottomView];
    _bottomView.hidden = YES;
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.top.equalTo(_changAfterMoneyLabel.mas_bottom).offset(20);
        make.height.equalTo(@40);
    }];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"daishou_notice_icon"]];
    [_bottomView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@5);
        make.centerY.equalTo(_bottomView);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    
    //    如果无需验货，无需填写。分配人名张三，李四等每个客户不一样，不填写也可以入库
    _bottomViewLabel1 = [[UILabel alloc] init];
    _bottomViewLabel1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    _bottomViewLabel1.text = @"充值成功";
    _bottomViewLabel1.numberOfLines = 0;
    _bottomViewLabel1.textAlignment = NSTextAlignmentLeft;
    [_bottomViewLabel1 setFont:[UIFont systemFontOfSize:14]];
    [_bottomView addSubview:_bottomViewLabel1];
    [_bottomViewLabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_bottomView);
        make.left.equalTo(imageView.mas_right).offset(5);
        make.right.equalTo(_bottomView).offset(-5);
    }];
    
    
    
    
}

-(__kindof MBZPKTextField*)createThreeCellNumber:(MASViewAttribute*)num{
    MBZPKTextField *textfield = [self createMBZPKTextField];
    [textfield addTarget:self  action:@selector(valueChanged:)  forControlEvents:UIControlEventAllEditingEvents];
    textfield.delegate = self;
    
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-60);
        make.left.equalTo(self.view).offset(85);
        make.top.equalTo(num).offset(25);
        make.height.equalTo(@35);
    }];
    
    UILabel *label1 = [self createLabel];
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-5);
        make.centerY.equalTo(textfield);
    }];
    
    
    UILabel *label2 = [self createLabel];
    
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(textfield.mas_right).offset(5);
        make.centerY.equalTo(textfield);
    }];
    
    
    UILabel *label3 = [self createLabel];
    [label3 setFont:[UIFont systemFontOfSize:13]];
    
    label3.textAlignment = NSTextAlignmentLeft;
    label3.numberOfLines = 0;
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(textfield.mas_left).offset(0);
        make.right.equalTo(self.view).offset(-5);
        make.top.equalTo(textfield.mas_bottom).offset(5);
    }];
    
    
    label1.text = @"充值金额(*)";
    
    
    label2.text = _model.amount_label;
    //    @"美元";
    
    label3.text = _model.payment_note;
    
    
    
    _changAfterMoneyLabel = [self createLabel];
    _changAfterMoneyLabel.textAlignment = NSTextAlignmentLeft;
    _changAfterMoneyLabel.textColor = [UIColor colorWithHexString:text_yellow];
    [_changAfterMoneyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-60);
        make.left.equalTo(self.view).offset(90);
        make.top.equalTo(label3.mas_bottom).offset(20);
        make.height.equalTo(@35);
    }];
    
    
    UILabel *label5 = [self createLabel];
    label5.text = @"实际到账";
    [label5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_changAfterMoneyLabel.mas_left).offset(-10);
        make.centerY.equalTo(_changAfterMoneyLabel);
    }];
    
    _sureButton = [[UIButton alloc] init];
    [_sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_sureButton setTitle:@"确定充值" forState:UIControlStateNormal];
    _sureButton.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [_sureButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    _sureButton.layer.cornerRadius = allCornerRadius;
    [_sureButton addTarget:self action:@selector(SurebuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sureButton];
    [_sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label5.mas_bottom).offset(25);
        make.right.equalTo(self.view.mas_right).offset(-10);
        make.left.equalTo(self.view).offset(10);
        make.height.equalTo(@40);
    }];
    
    
    
    return textfield;
}

-(__kindof MBZPKTextField*)createMBZPKTextField{
    MBZPKTextField *textfield1 = [[MBZPKTextField alloc] init];
    [textfield1 setFont:[UIFont systemFontOfSize:14]];
    textfield1.textColor = [UIColor blackColor];
    [textfield1 setTextAlignment:NSTextAlignmentLeft];
    [self.view addSubview:textfield1];
    
    return textfield1;
}

-(__kindof UILabel*)createLabel{
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [label1 sizeToFit];
    [self.view addSubview:label1];
    
    return label1;
}

-(NSString*)isNormalChange:(NSString*)amount{
    if ([amount isEqualToString:@""]) {
        return @"0";
    }
    double amountF = [amount doubleValue];
    
    double amount2 = amountF -(amountF * [_model.fixed_rate doubleValue] + [_model.fixed_amount doubleValue]);
    double changeAmount = amount2 * [_model.transfer_rate doubleValue];
    
    NSString *stringAmount = [NSString stringWithFormat:@"%.2f",changeAmount];
    if ([stringAmount doubleValue]< 0) {
        return @"0";
    }
    return stringAmount;
}

#pragma mark //支付宝 支付
-(void)alipay:(SQWoChongzhiPayidModel*)model{
    /*
     *点击获取prodcut实例并初始化订单信息
     */
    //    Product *product = [self.productList objectAtIndex:indexPath.row];
    
    /*
     *商户的唯一的parnter和seller。
     *签约后，支付宝会为每个商户分配一个唯一的 parnter 和 seller。
     */
    
    /*============================================================================*/
    /*=======================需要填写商户app申请的===================================*/
    /*============================================================================*/
    NSString *partner = @"2088611540766792";
    NSString *seller = @"ybdlliwei@163.com";
    NSString *privateKey = @"MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMq4vOtwVpxje8xpaOQR8KYk0GvnfkPljectgrWi4lYdHeVmMkWjojoST5RO5rPaJM2FaYcltTaTjtEAeMeWQpxO+54PDYfoODqEUwXgHyfuDQ2YeLpQwuPoQIQ1gz5iBofoMArNnzlvKqZmjyelu4ue/xmJS6lncRAvrfZ0AlhRAgMBAAECgYA5bdhnQm3m4/AAWqRSaBqqlqdMyknk5jUrFqN+3WQNTTZY9HitbcvZc7KDq06ECPqAFplspewOSAMfhmiw8gpwSF/knrOOHieXr92flQ4KAAJmty3pLh4OieTweWSeV4v7wHARi3EsuD2h7y2D4aibNYcoJT6qUaOAWsElSRkbgQJBAO7ZhBLHRbgBXsKcfcQ4NXG8i9WR+qTbmq5bminemdcIbYdY85VqbWliNUnUMXG5HQsDB/iuRt90XFroJ3Ey0rkCQQDZRx/EuzrpdHfhE/GOIeulUP5HUaIixakHxBCJ7pvThDGJkwe6VWQekfJbFxU2IFBS7L156FGDBpNCQu0fPMZZAkB3BamM+gRCB1zDOdxqNWBchwpfqNjv4gcYr8kI6EZdI3QBKFBpupk/FJNdcQIam5ZfcGk+gQoMPB3xpxJQlHTxAkEApbo+XQyCjvcrSR6xLLxqxRnWGggGJ4ekuiuugcbiTJ+UOV9Qkav4RekEdr5Qi4xyOA4jKA0vv0UQpYXu39i3YQJABO7d0cz8boXhtKpYCB+xOB4X6GVb7hiaJ+7sbgGq7wJG8ZD1At+RTl8La6mDFMimbLgmw92daMcDL8kPI6j+PA==";
    /*============================================================================*/
    /*============================================================================*/
    /*============================================================================*/
    
    //partner和seller获取失败,提示
    if ([partner length] == 0 ||
        [seller length] == 0 ||
        [privateKey length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示"
                                                        message:@"缺少partner或者seller或者私钥。"
                                                       delegate:self
                                              cancelButtonTitle:@"确定"
                                              otherButtonTitles:nil];
        [alert show];
        //        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    }
    
    /*
     *生成订单信息及签名
     */
    //将商品信息赋予AlixPayOrder的成员变量
    Order *order = [[Order alloc] init];
    order.partner = partner;
    order.sellerID = seller;
    
    order.outTradeNO =model.transaction_id; //订单ID（由商家自行制定）
    order.subject =[NSString stringWithFormat:@"suqibuy 账户充值订单: %@", model.transaction_id]; //商品标题
    order.body = [NSString stringWithFormat:@"suqibuy iphoneAPP内的账户充值订单:%@", model.transaction_id]; //商品描述
    order.totalFee = [NSString stringWithFormat:@"%.2f",[model.amount doubleValue]]; //商品价格
    order.notifyURL = [NSString stringWithFormat:@"%@/alipaypayment/notify", API_MAIN_URL];
    //    @"m.alipay.com"; //回调URL
    
    
    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showURL = @"m.alipay.com";
    
    //应用注册scheme,在AlixPayDemo-Info.plist定义URL types
    NSString *appScheme = @"suqibuy";
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
    NSLog(@"orderSpec = %@",orderSpec);
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(privateKey);
    NSString *signedString = [signer signString:orderSpec];
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            
            
        }];
    }
    
}

#pragma mark //微信
//*********微信**************
-(void)sendPayWithparameter:(NSDictionary*)dict
{
    
    //调起微信支付
    PayReq* req              = [[PayReq alloc] init];
    
    NSMutableString *stamp  = [dict objectForKey:@"timestamp"];
    req.timeStamp           = stamp.intValue;
    req.openID              = [dict objectForKey:@"appid"];
    req.nonceStr            = [dict objectForKey:@"noncestr"];
    req.sign                = [dict objectForKey:@"sign"];
    req.partnerId          = [dict objectForKey:@"partnerid"];
    req.prepayId            = [dict objectForKey:@"prepayid"];
    
    req.package             = [dict objectForKey:@"mpackage"];
    
    [WXApi sendReq:req];
    
    
}


//NotiPay

-(void)AlipayPayResult:(NSNotification *)noti
{
    NSDictionary* result = [noti object];
    if (result) {
        NSString *resultStatus = result[@"resultStatus"];
        if ([resultStatus isEqualToString:@"9000"]) {
            [self requstIsPayconfirm:globalModel.transaction_id];
        }else if ([resultStatus isEqualToString:@"8000"]){
            showDefaultPromptTextHUD(@"支付结果确认中");
        }else if ([resultStatus isEqualToString:@"4000"]){
            showDefaultPromptTextHUD(@"支付失败, 请确认手机是否有安装支付宝钱包APP");
        }else if ([resultStatus isEqualToString:@"6001"]){
            showDefaultPromptTextHUD(@"用户中途取消");
        }else if ([resultStatus isEqualToString:@"6002"]){
            showDefaultPromptTextHUD(@"网络连接出错,请重试");
        }else {
            showDefaultPromptTextHUD(@"支付失败");
        }
        
    }
}
-(void)PayResult:(NSNotification *)noti
{
    NSLog(@"noti %@", [noti object]);
    BaseResp* resp = (BaseResp*)[noti object];
    if([resp isKindOfClass:[PayResp class]]){
        if (resp.errCode == WXSuccess) {
            [self requstIsPayconfirm:globalModel.transaction_id];
        }else{
            showDefaultPromptTextHUD(@"支付失败");
        }
        //        myBlock = nil;
    }
    
}


#pragma mark //paypal
- (void)paypal :(SQWoChongzhiPayidModel*)model{
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    
    payment.amount = [[NSDecimalNumber alloc] initWithString:model.amount];;
    payment.currencyCode = model.amount_symbol;
    payment.shortDescription =[NSString stringWithFormat:@"suqibuy 账户充值订单:%@ ",model.transaction_id];
    payment.items = nil;  // if not including multiple items, then leave payment.items as nil
    payment.paymentDetails = nil; // if not including payment details, then leave payment.paymentDetails as nil
    
    if (!payment.processable) {
        return;
    }
    
    // Update payPalConfig re accepting credit cards.
    self.payPalConfig.acceptCreditCards = self.acceptCreditCards;
    
    PayPalPaymentViewController *paymentViewController = [[PayPalPaymentViewController alloc] initWithPayment:payment
                                                                                                configuration:self.payPalConfig
                                                                                                     delegate:self];
    [self presentViewController:paymentViewController animated:YES completion:nil];
}





#pragma mark - ---- delegate 🍎🌝
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian = YES;
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            
            //首字母不能为0和小数点
            if([textField.text length] == 0){
                if(single == '.') {
                    //                    [self showError:@"亲，第一个数字不能为小数点"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
                
            }
            
            if ([textField.text isEqualToString:@"0"]) {
                if([textField.text length] == 1){
                    if(single != '.') {
                        //                    [self showError:@"亲，第一个数字不能为小数点"];
                        [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        return NO;
                    }else{
                        isHaveDian = YES;
                        return YES;
                        
                    }
                    
                }
                
            }
            
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian = YES;
                    return YES;
                    
                }else{
                    //                    [self showError:@"亲，您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                
                if (isHaveDian) {//存在小数点
                    
                    //判断小数点的位数
                    //                    NSRange ran = [textField.text rangeOfString:@"."];
                    return YES;
                    //                    if (range.location - ran.location <= 2) {
                    //                        return YES;
                    //                    }else{
                    //                        //                        [self showError:@"亲，您最多输入两位小数"];
                    //                        return NO;
                    //
                }else{
                    return YES;
                }
            }
            
            
        }else{//输入的数据格式不正确
            //            [self showError:@"亲，您输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
    
}


#pragma mark PayPalPaymentDelegate methods

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment {
    NSLog(@"PayPal Payment Success!");
    //        self.resultText = [completedPayment description];
    //        [self showSuccess];
    
    
    [self sendCompletedPaymentToServer:completedPayment]; // Payment was processed successfully; send to server for verification and fulfillment
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController {
    NSLog(@"PayPal Payment Canceled");
    //    self.resultText = nil;
    //    self.successView.hidden = YES;
    //    showDefaultPromptTextHUD(@"取消充值");
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Proof of payment validation

- (void)sendCompletedPaymentToServer:(PayPalPayment *)completedPayment {
    // TODO: Send completedPayment.confirmation to server
    //    NSLog(@"Here is your proof of payment:\n\n%@\n\nSend this to your server for confirmation and fulfillment.", completedPayment.confirmation);
    
    //    {
    //        client =     {
    //            environment = sandbox;
    //            "paypal_sdk_version" = "2.14.2";
    //            platform = iOS;
    //            "product_name" = "PayPal iOS SDK";
    //        };
    //        response =     {
    //            "create_time" = "2016-06-29T04:48:13Z";
    //            id = "PAY-83895236HY263742KK5ZVGBA";
    //            intent = sale;
    //            state = approved;
    //        };
    //        "response_type" = payment;
    //    }
    
    bool isContain = [[completedPayment.confirmation allKeys] containsObject:@"response"];
    if (isContain) {
        NSString *pay_id = [completedPayment.confirmation objectForKey:@"response"] [@"id"];
        globalModel.pay_id = pay_id;
        [self requestGetpayment];
    }
    //    NSData *confirmation = [NSJSONSerialization dataWithJSONObject:completedPayment.confirmation
    //                                                           options:0
    //                                                             error:nil];
    //    
    //    NSDictionary *dic1 = [NSJSONSerialization JSONObjectWithData:confirmation
    //                     
    //                                                    options:NSJSONReadingMutableContainers
    //                     
    //                                                      error:nil];
}



#pragma mark - ---- getters and setters 🍋🌞


@end
