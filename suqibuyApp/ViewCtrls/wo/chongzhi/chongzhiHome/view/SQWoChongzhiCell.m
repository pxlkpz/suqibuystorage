//
//  SQWoChongzhiCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoChongzhiCell.h"
@interface SQWoChongzhiCell()

@property (weak, nonatomic) IBOutlet UILabel *OneLabel;

@property (weak, nonatomic) IBOutlet UIImageView *OneImageView;
@end

@implementation SQWoChongzhiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dataEntity {
    self.OneImageView.image = [UIImage imageNamed:dataEntity[@"imageName"]];
    self.OneLabel.text = dataEntity[@"title"];
}


@end
