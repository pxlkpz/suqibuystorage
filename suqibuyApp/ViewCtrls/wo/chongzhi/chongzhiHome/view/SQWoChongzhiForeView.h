//
//  SQWoChongzhiForeView.h
//  suqibuyApp
//
//  Created by apple on 16/6/13.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"
#import "SQWoChongzhiForeModel.h"
@interface SQWoChongzhiForeView : SQBaseViewController

@property(nonnull, strong) SQWoChongzhiForeModel*chongzhiModel;

@end
