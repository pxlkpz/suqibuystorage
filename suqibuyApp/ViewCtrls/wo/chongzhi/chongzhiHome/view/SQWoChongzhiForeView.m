//
//  SQWoChongzhiForeView.m
//  suqibuyApp
//
//  Created by apple on 16/6/13.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoChongzhiForeView.h"

#import "SQzhuanyunFangshiItemsModel.h"
#import "PxlTextView.h"
#import "TimeAndDataPicker.h"
#import "SQWochongzhiForeRequstModel.h"

@interface SQWoChongzhiForeView ()<UITextFieldDelegate>{
    //    SQzhuanyunFangshiModel *_model;
    
    MBZPKTextField *_textField_Money;
    MBZPKTextField *_textField_BankName;
    MBZPKTextField *_textField_Email;
    MBZPKTextField *_textField_PhoneNumber;
    
    MBZPKTextField *_textFieldName1;
    MBZPKTextField *_textFieldName2;
    
    TimeAndDataPicker* _dataPicker;
    
    SQWochongzhiForeRequstModel* _resqustModel;
}
@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQWoChongzhiForeView


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"国家银行转账充值";
    [self buildTableView];
    
    _resqustModel = [[SQWochongzhiForeRequstModel alloc] init];
    
}

#pragma mark - ---- event response 🍐🌛
-(void)SurebuttonAction:(UIButton*)sender{
    //    _resqustModel.cny_type =
    _resqustModel.cny_amount = _textField_Money.text;
    _resqustModel.cny_bankname = _textField_BankName.text;
    _resqustModel.cny_firstname = _textFieldName1.text;
    _resqustModel.cny_lastname = _textFieldName2.text;
    
    _resqustModel.cny_email = _textField_Email.text;
    _resqustModel.cny_mobile = _textField_PhoneNumber.text;
    if ([_resqustModel.cny_type isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请选择汇款币种");
        return;
    }
    else if ([_resqustModel.cny_amount isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入汇款金额");
        return;
    }else if ([_resqustModel.cny_bankname isEqualToString:@""]){
        showDefaultPromptTextHUD(@"请输入汇款银行名称");
        return;
    }else if ([_resqustModel.cny_firstname isEqualToString:@""]){
        showDefaultPromptTextHUD(@"请输入汇款帐号名称");
        return;
    }else if ([_resqustModel.cny_lastname isEqualToString:@""]){
        showDefaultPromptTextHUD(@"请输入完成的汇款帐号名称");
        return;
    }
    //    isValidateEmail
    else if (![self isValidateEmail:_resqustModel.cny_email]){
        showDefaultPromptTextHUD(@"请输入正确的邮箱地址");
        return;
    }
    
    [self requestData];
    
}

-(void)selctAction:(UIButton*)sender{
    if (_chongzhiModel.cnys.count > 0) {
        [self showDataPicker];
        __weak __typeof(sender)weaksend = sender;
        [_dataPicker getDataWithArray:_chongzhiModel.cnys andFinsihBlock:^(NSString *resurltStr, NSInteger rowOne, NSInteger rowTwo) {
            
            [weaksend setTitle:resurltStr forState:UIControlStateNormal];
            _resqustModel.cny_type = weaksend.currentTitle;
            
        }];
        
    }
    
}

-(void)buttonFooterViewAction:(UIButton*)sender{
    [_listTableView setTableFooterView:[self buildFooterView]];
    
}
#pragma mark - ---- private methods 🍊🌜


-(BOOL)isValidateEmail:(NSString *)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(void)showDataPicker{
    //选择车辆
    if (!_dataPicker) {
        _dataPicker = [[TimeAndDataPicker alloc]  initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth,KScreenHeight) andType:TimePickerDataSelectMode];
        [self.view.window addSubview:_dataPicker];
    }
    [_dataPicker timePickerAnimations];
}

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    [_listTableView setTableFooterView:[[UIView alloc]init ]];
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    //[_listTableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    [_listTableView layoutIfNeeded];
    [_listTableView setTableHeaderView:[self buildTabelHeadView]];
    [_listTableView setTableFooterView:[self buildButtonFooterView]];
    
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}

-(__kindof UIView*)buildButtonFooterView{
    UIView *tableFooterView = [[UIView alloc] init];
    tableFooterView.frame = CGRectMake(0, 0, screen_width, 70);
    
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = KColorRGB(229, 229, 229);
    [tableFooterView addSubview:topView];
    topView.frame = CGRectMake(0, 0, screen_width, 10);
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"我已经汇款到上方的银行账户,开始填写汇款信息" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 13.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    [button addTarget:self action:@selector(buttonFooterViewAction:) forControlEvents:UIControlEventTouchUpInside];
    [tableFooterView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(tableFooterView).offset(20);
        make.right.equalTo(tableFooterView.mas_right).offset(-10);
        make.left.equalTo(tableFooterView).offset(10);
        make.height.equalTo(@40);
    }];
    
    return tableFooterView;
}

-(__kindof UIView*)buildFooterView{
    UIView *tableFooterView = [[UIView alloc] init];
    tableFooterView.frame = CGRectMake(0, 0, screen_width, 400);
    
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = KColorRGB(229, 229, 229);
    [tableFooterView addSubview:topView];
    topView.frame = CGRectMake(0, 0, screen_width, 10);
    
    
    const NSInteger num = 30;
    [self creatCellInOneUI:@"汇款币种(*)" andSuperView:tableFooterView andTopNumber:0+num];
    
    _textField_Money = [self creatCellInUI:@"汇款金额(*)" andSuperView:tableFooterView andTopNumber:50 +num];
    _textField_Money.keyboardType = UIKeyboardTypeDecimalPad;
    _textField_Money.delegate = self;
    
    _textField_BankName = [self creatCellInUI:@"汇款银行名称(*)" andSuperView:tableFooterView andTopNumber:100 +num];
    
    [self creatCellInTwoUI:@"汇报帐号名称(*)" andSuperView:tableFooterView andTopNumber:150+num ];
    
    _textField_Email = [self creatCellInUI:@"Email" andSuperView:tableFooterView andTopNumber:200 +num];
    _textField_Email.keyboardType = UIKeyboardTypeEmailAddress;
    
    _textField_PhoneNumber = [self creatCellInUI:@"手机号码" andSuperView:tableFooterView andTopNumber:250 +num];
    _textField_PhoneNumber.keyboardType = UIKeyboardTypePhonePad;
    
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"确定" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    [button addTarget:self action:@selector(SurebuttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [tableFooterView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_textField_PhoneNumber.mas_bottom).offset(20);
        make.right.equalTo(tableFooterView.mas_right).offset(-10);
        make.left.equalTo(tableFooterView).offset(10);
        make.height.equalTo(@40);
    }];
    
    
    
    return  tableFooterView;
}



-(__kindof UIView*)buildTabelHeadView{
    
    
    UIView *tabelHeadView = [[UIView alloc] init];
    tabelHeadView.frame = CGRectMake(0, 0, screen_width, 260);
    if (IPHONE_4_OR_LESS) {
        tabelHeadView.frame = CGRectMake(0, 0, screen_width, 280);
    }
    
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = KColorRGB(229, 229, 229);
    [tabelHeadView addSubview:topView];
    topView.frame = CGRectMake(0, 0, screen_width, 10);
    
    
    UIView* bgView = [[UIView alloc] init];
    [tabelHeadView addSubview:bgView];
    bgView.backgroundColor = KColorRGB(255, 255, 223);
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(tabelHeadView).offset(20);
        make.left.equalTo(tabelHeadView).offset(10);
        make.right.equalTo(tabelHeadView).offset(-10);
        make.bottom.equalTo(tabelHeadView).offset(-10);
    }];
    bgView.layer.borderWidth = 1;
    bgView.layer.borderColor = [UIColor colorWithHexString:gray].CGColor;
    bgView.layer.cornerRadius = allCornerRadius;
    
    
    
    UILabel * myLabel = [PxlTextView createLabel];
    myLabel.numberOfLines = 0;
    myLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    [myLabel setFont:[UIFont systemFontOfSize:14]];
    myLabel.text = @"您需要先会汇款到指定的以下的银行:";
    [bgView addSubview:myLabel];
    [myLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(10);
        make.left.equalTo(bgView).offset(5);
    }];
    
    
    
    UILabel *label11 = [PxlTextView createLabel];
    label11.textAlignment = NSTextAlignmentLeft;
    label11.text = _chongzhiModel.bank_account_name;
    label11.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [bgView addSubview:label11];
    [label11 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(80);
        make.top.equalTo(myLabel.mas_bottom).offset(13);
        make.right.equalTo(bgView).offset(-5);
        //        make.height.equalTo(@30);
    }];
    
    
    UILabel *label12 = [PxlTextView createLabel];
    label12.textAlignment = NSTextAlignmentLeft;
    label12.text = _chongzhiModel.bank_account_number;
    label12.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [bgView addSubview:label12];
    [label12 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(80);
        make.top.equalTo(label11.mas_bottom).offset(13);
        make.right.equalTo(bgView).offset(-5);
    }];
    
    UILabel *label13 = [PxlTextView createLabel];
    label13.textAlignment = NSTextAlignmentLeft;
    label13.text = _chongzhiModel.bank_name;
    label13.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [bgView addSubview:label13];
    [label13 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(80);
        make.top.equalTo(label12.mas_bottom).offset(13);
        make.right.equalTo(bgView).offset(-5);
    }];
    
    
    UILabel *label14 = [PxlTextView createLabel];
    label14.textAlignment = NSTextAlignmentLeft;
    label14.text = _chongzhiModel.bank_address;
    label14.numberOfLines = 0;
    label14.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [bgView addSubview:label14];
    [label14 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(80);
        make.top.equalTo(label13.mas_bottom).offset(13);
        make.right.equalTo(bgView).offset(-5);
    }];
    
    UILabel *label15 = [PxlTextView createLabel];
    label15.textAlignment = NSTextAlignmentLeft;
    label15.text = _chongzhiModel.bank_code;
    label15.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [bgView addSubview:label15];
    [label15 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(80);
        make.top.equalTo(label14.mas_bottom).offset(13);
        make.right.equalTo(bgView).offset(-5);
    }];
    
    UILabel *label16 = [PxlTextView createLabel];
    label16.textAlignment = NSTextAlignmentLeft;
    label16.text = _chongzhiModel.transfer_reason;
    label16.textColor = [UIColor colorWithHexString:text_red];
    [bgView addSubview:label16];
    [label16 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(80);
        make.top.equalTo(label15.mas_bottom).offset(13);
        make.right.equalTo(bgView).offset(-5);
    }];
    
    
    UILabel *label1 = [PxlTextView createLabel];
    label1.text = @"收款人:";
    label1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    [bgView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(0);
        make.centerY.equalTo(label11);
        make.right.equalTo(label11.mas_left).offset(-3);
    }];
    
    UILabel *label2 = [PxlTextView createLabel];
    label2.text = @"帐号:";
    label2.textColor = [UIColor colorWithHexString:text_color_gray_black];
    [bgView addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(0);
        make.centerY.equalTo(label12);
        make.right.equalTo(label12.mas_left).offset(-3);
    }];
    
    
    UILabel *label3 = [PxlTextView createLabel];
    label3.text = @"开户行:";
    label3.textColor = [UIColor colorWithHexString:text_color_gray_black];
    [bgView addSubview:label3];
    [label3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(0);
        make.centerY.equalTo(label13);
        make.right.equalTo(label13.mas_left).offset(-3);
    }];
    
    
    UILabel *label4 = [PxlTextView createLabel];
    label4.text = @"开户行地址:";
    label4.textColor = [UIColor colorWithHexString:text_color_gray_black];
    [bgView addSubview:label4];
    [label4 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(0);
        make.centerY.equalTo(label14);
        make.right.equalTo(label14.mas_left).offset(-3);
    }];
    
    
    UILabel *label5 = [PxlTextView createLabel];
    label5.text = @"银行代码:";
    label5.textColor = [UIColor colorWithHexString:text_color_gray_black];
    [bgView addSubview:label5];
    [label5 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(0);
        make.centerY.equalTo(label15);
        make.right.equalTo(label15.mas_left).offset(-3);
    }];
    
    UILabel *label6 = [PxlTextView createLabel];
    label6.text = @"汇款理由:";
    label6.textColor = [UIColor colorWithHexString:text_red];
    [bgView addSubview:label6];
    [label6 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(0);
        make.centerY.equalTo(label16);
        make.right.equalTo(label16.mas_left).offset(-3);
    }];
    
    
    return  tabelHeadView;
}



-(void) requestData
{
    NSString *user_token = [SQUser loadmodel].user_token;
    _resqustModel.user_token = user_token;
    
    
    NSDictionary *dic = _resqustModel.mj_keyValues;
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_recharge_banktransfer WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    showDefaultPromptTextHUD(result[@"msg"]);
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}
//#pragma mark - ---- delegate 🍎🌝

-(__kindof MBZPKTextField*)creatCellInUI:(NSString*)Str andSuperView:(UIView*)superView andTopNumber:(NSInteger)num{
    
    MBZPKTextField *textfield = [PxlTextView createMBZPKTextField];
    [superView addSubview:textfield];
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView).offset(-10);
        make.left.equalTo(superView).offset(110);
        make.top.mas_equalTo(num);
        make.height.equalTo(@35);
    }];
    
    UILabel *label1 = [PxlTextView createLabel];
    [superView addSubview:label1];
    label1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    label1.text = Str;
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-5);
        make.centerY.equalTo(textfield);
    }];
    
    return textfield;
    
}


-(__kindof MBZPKTextField*)creatCellInTwoUI:(NSString*)Str andSuperView:(UIView*)superView andTopNumber:(NSInteger)num{
    
    _textFieldName1 = [PxlTextView createMBZPKTextField];
    [superView addSubview:_textFieldName1];
    
    
    _textFieldName2 = [PxlTextView createMBZPKTextField];
    [superView addSubview:_textFieldName2];
    
    
    [_textFieldName1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView).offset(110);
        make.top.mas_equalTo(num);
        make.height.equalTo(@35);
    }];
    
    [_textFieldName2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView).offset(-10);
        make.left.equalTo(_textFieldName1.mas_right).offset(5);
        make.top.mas_equalTo(num);
        make.width.equalTo(_textFieldName1);
        make.height.equalTo(@35);
    }];
    
    
    UILabel *label1 = [PxlTextView createLabel];
    [superView addSubview:label1];
    label1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    label1.text = Str;
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_textFieldName1.mas_left).offset(-5);
        make.centerY.equalTo(_textFieldName1);
    }];
    
    return _textFieldName1;
    
}

-(__kindof UIButton*)creatCellInOneUI:(NSString*)Str andSuperView:(UIView*)superView andTopNumber:(NSInteger)num{
    
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitle:@"" forState:UIControlStateNormal];
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    //    button.titleLabel.textAlignment = NSTextAlignmentLeft;
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    button.titleLabel.font = [UIFont systemFontOfSize: 14.0];
    
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor colorWithHexString:text_gray].CGColor;
    button.layer.cornerRadius = allCornerRadius;
    
    [button addTarget:self action:@selector(selctAction:) forControlEvents:UIControlEventTouchUpInside];
    [superView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView).offset(-10);
        make.left.equalTo(superView).offset(110);
        make.top.mas_equalTo(num);
        make.height.equalTo(@35);
    }];
    
    
    
    UILabel *label1 = [PxlTextView createLabel];
    [superView addSubview:label1];
    label1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    label1.text = Str;
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(button.mas_left).offset(-5);
        make.centerY.equalTo(button);
    }];
    
    return button;
    
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian = YES;
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            
            //首字母不能为0和小数点
            if([textField.text length] == 0){
                if(single == '.') {
                    //                    [self showError:@"亲，第一个数字不能为小数点"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
                
            }
            
            if ([textField.text isEqualToString:@"0"]) {
                if([textField.text length] == 1){
                    if(single != '.') {
                        //                    [self showError:@"亲，第一个数字不能为小数点"];
                        [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        return NO;
                    }else{
                        isHaveDian = YES;
                        return YES;
                        
                    }
                    
                }
                
            }
            
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian = YES;
                    return YES;
                    
                }else{
                    //                    [self showError:@"亲，您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                
                if (isHaveDian) {//存在小数点
                    
                    //判断小数点的位数
                    //                    NSRange ran = [textField.text rangeOfString:@"."];
                    return YES;
                    //                    if (range.location - ran.location <= 2) {
                    //                        return YES;
                    //                    }else{
                    //                        //                        [self showError:@"亲，您最多输入两位小数"];
                    //                        return NO;
                    //
                }else{
                    return YES;
                }
            }
            
            
        }else{//输入的数据格式不正确
            //            [self showError:@"亲，您输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}


#pragma mark - ---- getters and setters 🍋🌞

@end
