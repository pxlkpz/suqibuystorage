//
//  SQWoChongzhiThreeView.h
//  suqibuyApp
//
//  Created by apple on 16/6/13.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"

//paypal
#import "PayPalMobile.h"

@class SQWoChongzhiThreeModel;
typedef enum : NSUInteger {
    PxlChongzhiModelalipay,
    PxlChongzhiModelWeixin,
    PxlChongzhiModelGuoji,
} PxlChongzhiModel;


@interface SQWoChongzhiThreeView : SQBaseViewController<PayPalPaymentDelegate>

//@property (nonatomic, assign) PxlChongzhiModel typeModel;

@property(nonatomic, strong) SQWoChongzhiThreeModel*model;


@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, assign, readwrite) BOOL acceptCreditCards;
@property(nonatomic, strong, readwrite) NSString *resultText;

@end
