//
//  SQWochongzhiForeRequstModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/14.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWochongzhiForeRequstModel : NSObject


/** 汇款币种 */
@property (nonatomic, copy) NSString* cny_type;
/** 汇款金额 */
@property (nonatomic, copy) NSString* cny_amount;
/** 汇款银行名称 */
@property (nonatomic, copy) NSString* cny_bankname;
/** 汇款账号名称 */
@property (nonatomic, copy) NSString* cny_firstname;
 @property (nonatomic, copy) NSString* cny_lastname;

/** Email */
@property (nonatomic, copy) NSString* cny_email;
/** 手机号码 */
@property (nonatomic, copy) NSString* cny_mobile;
/** user_token */
@property (nonatomic, copy) NSString* user_token;


@end
