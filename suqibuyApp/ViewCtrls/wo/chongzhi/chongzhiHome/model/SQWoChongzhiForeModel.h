//
//  SQWoChongzhiForeModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/13.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWoChongzhiForeModel : NSObject
//{
//    "id": "6",
//    "amount_label": "人民币",
//    "payment_code": "banktransfer",
//    "payment_method": "国际汇款",
//    "payment_note": "目前使用银行转账",
//    "bank_account_name": "DAI LINJIE",
//    "bank_account_number": "6013 8208 0009 3011 656",
//    "bank_name": "Bank of China Shanghai Branch",
//    "bank_address": "200 Mid. Yincheng Rd.,Pudong new District, Shanghai 200121, China",
//    "bank_code": "BKCH CN BJ 300",
//    "cnys": [
//             "人民币 CNY",
//             "美元 USD",
//             "欧元 EUR",
//             ],
//    "payment_description": null,
//    "payment_icon": null
//}

@property (nonatomic, copy) NSString* my_id;
@property (nonatomic, copy) NSString* amount_label;
@property (nonatomic, copy) NSString* payment_code;
@property (nonatomic, copy) NSString* payment_method;
@property (nonatomic, copy) NSString* payment_note;
@property (nonatomic, copy) NSString* bank_account_name;
@property (nonatomic, copy) NSString* bank_account_number;
@property (nonatomic, copy) NSString* bank_name;
@property (nonatomic, copy) NSString* bank_address;
@property (nonatomic, copy) NSString* bank_code;
@property (nonatomic, copy) NSString* transfer_reason;
@property (nonatomic, copy) NSArray* cnys;
 @property (nonatomic, copy) NSString* payment_description;
@property (nonatomic, copy) NSString* payment_icon;

@end
