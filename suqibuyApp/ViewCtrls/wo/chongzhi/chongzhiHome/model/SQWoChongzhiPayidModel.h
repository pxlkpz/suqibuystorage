//
//  SQWoChongzhiPayidModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/14.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWoChongzhiPayidModel : NSObject
//                    {
//                        command = "recharge.prepaid";
//                        "request_code" = "";
//                        result =     {
//                            amount = 5;
//                            "amount_symbol" = RMB;
//                            "banktransfer_desc" = "<null>";
//                            "created_at" = "2016-06-14 10:04:46";
//                            id = 38;
//                            note = "<null>";
//                            "pay_id" = "<null>";
//                            "payment_code" = "alipay_mormal";
//                            "payment_method" = "\U652f\U4ed8\U5b9d";
//                            "recharge_amount" = "5.00 CNY";
//                            "return_desc" = "<null>";
//                            status = pendding;
//                            "status_label" = "\U672a\U652f\U4ed8";
//                            "transaction_id" = TSCZ1053;
//                            "transfered_amount" = "&yen;5.00";
//                        };
//                        success = 1;
//                    }

@property (nonatomic, copy) NSString* amount;
@property (nonatomic, copy) NSString* amount_symbol;
@property (nonatomic, copy) NSString* banktransfer_desc;
@property (nonatomic, copy) NSString* created_at;
@property (nonatomic, copy) NSString* my_id;
@property (nonatomic, copy) NSString* note;
@property (nonatomic, copy) NSString* pay_id;
@property (nonatomic, copy) NSString* payment_code;
@property (nonatomic, copy) NSString* payment_method;
@property (nonatomic, copy) NSString* recharge_amount;
@property (nonatomic, copy) NSString* return_desc;
@property (nonatomic, copy) NSString* status;
@property (nonatomic, copy) NSString* status_label;
@property (nonatomic, copy) NSString* transaction_id;
@property (nonatomic, copy) NSString* transfered_amount;


@end
