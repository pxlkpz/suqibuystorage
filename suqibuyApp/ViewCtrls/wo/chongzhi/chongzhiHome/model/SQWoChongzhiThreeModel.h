//
//  SQWoChongzhiThreeModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/13.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWoChongzhiThreeModel : NSObject
//{
//    "id": "1",
//    "amount_label": "美元",
//    "payment_code": "payal_normal",
//    "payment_method": "PAYPAL",
//    "payment_note": "外币充值汇兑损失约为：2.9% 当前美元-人民币汇率为(1: 6.1480)",
//    "fixed_rate": 0.044,
//    "fixed_amount": 0.3,
//    "transfer_rate": 6.148,
//    "max_amount_limit": 10000,
//    "payment_description": "Paypal全球最大的在线支付平台，可通过国际信用卡和各国银行卡，实现即时付款！所有使用的货币均以美元进行折算。",
//    "payment_icon": null
//},
@property (nonatomic, copy) NSString* my_id;
@property (nonatomic, copy) NSString* amount_label;
@property (nonatomic, copy) NSString* payment_code;
@property (nonatomic, copy) NSString* payment_method;
@property (nonatomic, copy) NSString* payment_note;
@property (nonatomic, copy) NSString* fixed_amount;
@property (nonatomic, copy) NSString* fixed_rate;
@property (nonatomic, copy) NSString* max_amount_limit;
@property (nonatomic, copy) NSString* payment_description;
@property (nonatomic, copy) NSString* payment_icon;
@property (nonatomic, copy) NSString* transfer_rate;
@end
