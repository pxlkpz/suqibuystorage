//
//  SQWoChongzhiSQ.m
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoChongzhiVC.h"
#import "SQWoChongzhiCell.h"
#import "SQWoChongzhiHistoryVC.h"
#import "SQWoChongzhiThreeView.h"
#import "SQWoChongzhiThreeModel.h"
#import "SQWoChongzhiForeView.h"
#import "SQWoChongzhiForeModel.h"

@interface SQWoChongzhiVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *dataArray;
    NSArray *resquestDataArray;
}
@property(nonatomic, strong) UITableView* listTableView;
@end

@implementation SQWoChongzhiVC


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"余额充值";
    resquestDataArray = [NSArray array];
    dataArray = @[@{@"imageName":@"chong_zhi_paypal_icon", @"title":@"使用PAYPAL充值"},
                  @{@"imageName":@"chong_zhi_alipay_icon", @"title":@"使用支付宝充值"},
                  @{@"imageName":@"chong_zhi_weixin_icon", @"title":@"使用微信充值"},
                  @{@"imageName":@"chong_zhi_visa_icon", @"title":@"使用国际汇款充值"}];
    
    [self setRightButton:@"充值历史" action:@selector(chongzhiRightAction:)];
    
    [self buildTableView];
    
    [self requestData];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
}
#pragma mark - ---- event response 🍐🌛
-(void)chongzhiRightAction:(UIButton*)sender{
    SQWoChongzhiHistoryVC *controller = [[SQWoChongzhiHistoryVC alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - ---- private methods 🍊🌜
-(__kindof UIButton*)setRightButton:(NSString*)buttonTitle action:(SEL)action
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    
    
    CGSize size = [buttonTitle sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:16.0]}];
    [button setFrame:CGRectMake(0, 0, MAX((size.width+15),40), MAX(30,size.height))];
    [button setTitle:buttonTitle forState:UIControlStateNormal];
    if (action != nil) {
        [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    }
    [self setRightNavigationItemWithCustomView:button];
    return button;
}
- (void)setRightNavigationItemWithCustomView:(UIView*)cusView
{
    UIBarButtonItem *m_buttonItem = [[UIBarButtonItem alloc] initWithCustomView:cusView];
    
    self.navigationItem.rightBarButtonItem = m_buttonItem;
}


-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.view);
    }];
    [_listTableView setTableFooterView:[ [UIView alloc]init]];
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}

-(void) requestData
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_recharge_list WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    resquestDataArray = result[@"items"];
                    [_listTableView reloadData];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


#pragma mark - ---- delegate 🍎🌝
#pragma mark- tableviewdelegate
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return resquestDataArray.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"SQWoChongzhiCell";
    
    SQWoChongzhiCell *cell = (SQWoChongzhiCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQWoChongzhiCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQWoChongzhiCell class]])
            {
                cell = (SQWoChongzhiCell*)nib;
                break;
            }
        }
    }
    
    SQWoChongzhiThreeModel* model = [SQWoChongzhiThreeModel mj_objectWithKeyValues:resquestDataArray[indexPath.row]];
    
    NSDictionary* dic = [NSDictionary dictionary];
    if ([model.payment_code isEqualToString:@"payal_normal"]) {
        dic = dataArray[0];
    }else if ([model.payment_code isEqualToString:@"alipay_mormal"]) {
        dic = dataArray[1];
    }
    else if ([model.payment_code isEqualToString:@"weixin"]) {
        dic = dataArray[2];
    }
    else if ([model.payment_code isEqualToString:@"banktransfer"]) {
        dic = dataArray[3];
    }
    
    [cell setupData:dic];
    
    return cell;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < 3) {
        SQWoChongzhiThreeView *vc = [[SQWoChongzhiThreeView alloc] init];
        vc.model = [SQWoChongzhiThreeModel mj_objectWithKeyValues:resquestDataArray[indexPath.row]];

        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 3){
        SQWoChongzhiForeView *vc = [[SQWoChongzhiForeView alloc] init];
        vc.chongzhiModel = [SQWoChongzhiForeModel mj_objectWithKeyValues:resquestDataArray[indexPath.row]];
        
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // iOS 8 的Self-sizing特性
    return 60;
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

#pragma mark - ---- getters and setters 🍋🌞


@end
