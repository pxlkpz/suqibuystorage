//
//  SQWoChongzhiHistoryOrderDetail.m
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoChongzhiHistoryOrderDetail.h"
#import "SQWoChongzhiHOrderDetailCell.h"

@interface SQWoChongzhiHistoryOrderDetail ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataArray;
}
@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQWoChongzhiHistoryOrderDetail



#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"%@充值订单详细", _dataModel.transaction_id];
    
    [self buildTableView];
    
    [self dataReload];
    
}
#pragma mark - ---- event response 🍐🌛

#pragma mark - ---- private methods 🍊🌜

-(void)dataReload{
    if (_dataModel.note == nil || [_dataModel.note isEqualToString:@""]) {
        _dataModel.note = @"  ";
    }
    if (_dataModel.banktransfer_desc == nil || [_dataModel.banktransfer_desc isEqualToString:@""]) {
        _dataModel.banktransfer_desc = @" ";
    }
    dataArray = @[@{@"订单号 :":_dataModel.transaction_id},
                  @{@"订单状态 :":_dataModel.status_label},
                  @{@"充值方式 :":_dataModel.payment_method},
                  @{@"充值金额 :":[_dataModel.recharge_amount gtm_stringByUnescapingFromHTML]},
                  @{@"到账金额 :":[_dataModel.transfered_amount gtm_stringByUnescapingFromHTML]},
                  @{@"充值日期 :":_dataModel.created_at},
                  @{@"备注 :":_dataModel.note},
                  @{@"转账信息 :":_dataModel.banktransfer_desc},
                  ];
    [_listTableView reloadData];
    
}

-(__kindof UIView*)buildTabelHeadView{
    UIView* bgView = [[UIView alloc] init];
    bgView.frame = CGRectMake(0, 0, screen_width, 80);
    
    UIImageView*imageView = [[UIImageView alloc] init ];
    [bgView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgView).offset(-10);
        make.centerY.equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(82, 28));
    }];
    
    if ([_dataModel.payment_code isEqualToString:@"payal_normal"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_paypal_icon"];
    }
    if ([_dataModel.payment_code isEqualToString:@"alipay_mormal"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_alipay_icon"];
    }
    if ([_dataModel.payment_code isEqualToString:@"alipaydirect"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_alipaydirect_icon"];
    }
    if ([_dataModel.payment_code isEqualToString:@"weixin"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_weixin_icon"];
    }
    if ([_dataModel.payment_code isEqualToString:@"banktransfer"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_visa_icon"];
    }
    
    
    
    
    
    
    UILabel *label1 = [UILabel new];
    [label1 sizeToFit];
    [label1 setFont:[UIFont systemFontOfSize:16]];
    label1.textColor = [UIColor colorWithHexString:text_gray];
    [bgView addSubview:label1];
    label1.text = @"账户余额充值订单详情";
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(imageView.mas_left).offset(-1);
        make.centerY.equalTo(bgView);
    }];
    
    return bgView;
}

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [_listTableView setTableFooterView:[[UIView alloc] init]];
    [_listTableView setTableHeaderView:[self buildTabelHeadView]];
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    if ([UIDevice currentDevice].systemVersion.integerValue > 7) {
        _listTableView.rowHeight = UITableViewAutomaticDimension;
    }
    _listTableView.estimatedRowHeight = 80.0f;
    
    
    @weakify(self);
    
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:self.dataModel.transaction_id];
    }];
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}




-(__kindof UIColor*)statusLabelBg:(SQWoChongzhiHistroyModel*)dataEE{
    UIColor *coler = [UIColor whiteColor];
    if ([dataEE.status isEqualToString:@"pendding"] ||
        [dataEE.status isEqualToString:@"paid_less"]) {
        coler = KColorFromRGB(0xFF4081);
    }
    
    else if ([dataEE.status isEqualToString:@"paid_confirmed"] ||
             [dataEE.status isEqualToString:@"finished"]) {
        coler = KColorFromRGB(0x83d79b);
    }
    
    else if ([dataEE.status isEqualToString:@"waiting_confirm"]
             ) {
        coler = KColorFromRGB(0x008000);
    }
    
    else if ([dataEE.status isEqualToString:@"failed"] ||
             [dataEE.status isEqualToString:@"overdue"] ||
             [dataEE.status isEqualToString:@"canceled"]) {
        coler = KColorFromRGB(0xd9dada);
    }
    
    return coler;
}




-(void) requestData:(NSString*) order_no;
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token , @"order_no":order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_recharge_chongzhiorderdetail WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _dataModel = [SQWoChongzhiHistroyModel mj_objectWithKeyValues:result];
                    [self dataReload];
                    
                }
                
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        [_listTableView.mj_header endRefreshing];
        
        
    } WithErrorBlock:^(id errorCode) {
        [_listTableView.mj_header endRefreshing];
        
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [_listTableView.mj_header endRefreshing];
        
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


#pragma mark - ---- delegate 🍎🌝
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SQWoChongzhiHOrderDetailCell";
    
    SQWoChongzhiHOrderDetailCell *cell = (SQWoChongzhiHOrderDetailCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQWoChongzhiHOrderDetailCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQWoChongzhiHOrderDetailCell class]])
            {
                cell = (SQWoChongzhiHOrderDetailCell*)nib;
                break;
            }
        }
    }
    
    if (indexPath.row == 0) {
        cell.rightLabel.textColor = [UIColor colorWithHexString:text_yellow];
    }
    else if (indexPath.row == 1) {
        cell.rightLabel.textColor = [self statusLabelBg:_dataModel];
        cell.line.hidden = NO;
    }
    
    [cell setupData:dataArray[indexPath.row]];
    
    
    
    return cell;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        return 43;
    }
    return UITableViewAutomaticDimension;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}
#pragma mark - ---- getters and setters 🍋🌞


@end
