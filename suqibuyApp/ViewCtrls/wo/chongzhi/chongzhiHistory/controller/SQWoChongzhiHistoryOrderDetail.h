//
//  SQWoChongzhiHistoryOrderDetail.h
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"
#import "SQWoChongzhiHistroyModel.h"
@interface SQWoChongzhiHistoryOrderDetail : SQBaseViewController

@property(nonatomic , strong) SQWoChongzhiHistroyModel *dataModel;

@end
