//
//  SQWoChongzhiHOrderDetailCell.h
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQWoChongzhiHOrderDetailCell : UITableViewCell

- (void)setupData:(id)dataEntity ;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@property (weak, nonatomic) IBOutlet UIView *line;
-(void)setupData:(id)model andRow:(NSInteger)row;
-(void)setupDataInDaichongOrderPay:(id)model andRow:(NSInteger)row;
-(void)setupDataInSQDaiChongOrderPayJieGuo:(id)model andRow:(NSInteger)row;
@end
