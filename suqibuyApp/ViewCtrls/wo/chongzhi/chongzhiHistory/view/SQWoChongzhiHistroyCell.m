//
//  SQWoChongzhiHistroyCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoChongzhiHistroyCell.h"
#import "SQWoChongzhiHistroyModel.h"
@interface SQWoChongzhiHistroyCell()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftBottomLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightTopLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightBottomLabel;

@end


@implementation SQWoChongzhiHistroyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setupData:(SQWoChongzhiHistroyModel *)dataEntity {
    self.titleLabel.text = [NSString stringWithFormat:@"订单号:%@",dataEntity.transaction_id];
    self.leftTopLabel.text = [NSString stringWithFormat:@"充值方式:%@",dataEntity.payment_method];
    self.leftBottomLabel.text = [NSString stringWithFormat:@"时间:%@",dataEntity.created_at];
    self.rightTopLabel.text = [NSString stringWithFormat:@"充值金额:%@", [dataEntity.recharge_amount gtm_stringByUnescapingFromHTML]];
    self.rightBottomLabel.text = [NSString stringWithFormat:@"到账金额:%@",[dataEntity.transfered_amount gtm_stringByUnescapingFromHTML]];
    self.stateLabel.text = dataEntity.status_label;
    
    self.stateLabel.backgroundColor = [self statusLabelBg:dataEntity];
}

-(__kindof UIColor*)statusLabelBg:(SQWoChongzhiHistroyModel*)dataEE{
    UIColor *coler = [UIColor whiteColor];
    if ([dataEE.status isEqualToString:@"pendding"] ||
        [dataEE.status isEqualToString:@"paid_less"]) {
        coler = KColorFromRGB(0xFF4081);
    }
    
    else if ([dataEE.status isEqualToString:@"paid_confirmed"] ||
             [dataEE.status isEqualToString:@"finished"]) {
        coler = KColorFromRGB(0x83d79b);
    }
    
    else if ([dataEE.status isEqualToString:@"waiting_confirm"]
             ) {
        coler = KColorFromRGB(0x008000);
    }
    
    else if ([dataEE.status isEqualToString:@"failed"] ||
             [dataEE.status isEqualToString:@"overdue"] ||
             [dataEE.status isEqualToString:@"canceled"]) {
        coler = KColorFromRGB(0xd9dada);
    }
    
    return coler;
}


@end
