//
//  SQWoChongzhiHOrderDetailCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoChongzhiHOrderDetailCell.h"
#import "SQWoDaichongHistoryModel.h"
#import "SQWoDaichongHistroyOrderDetailModel.h"
@interface SQWoChongzhiHOrderDetailCell()

@end

@implementation SQWoChongzhiHOrderDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width - 4 * 3 - 80; // 44 = avatar宽度，4 * 3为padding
    self.rightLabel.preferredMaxLayoutWidth = preferredMaxWidth;
    [self.rightLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setupData:(NSDictionary *)dataEntity {
    self.leftLabel.text =[dataEntity allKeys][0];
    self.rightLabel.text = [dataEntity allValues][0];
    
    if ([self.rightLabel.text isEqualToString:@""] || !self.rightLabel.text) {
        self.rightLabel.text  =@"  ";
    }
    
}

-(void)setupData:(SQWoDaichongHistoryModel*)model andRow:(NSInteger)row{
    self.rightLabel.text = @"";
    self.line.hidden = YES;
    switch (row) {
        case 0:
        {
            self.leftLabel.text =@"订单号 :";
            self.rightLabel.text = model.order_no;
            self.rightLabel.textColor = [UIColor colorWithHexString:text_yellow];
            
        }
            break;
            
        case 1:
        {
            self.leftLabel.text =@"订单状态 :";
            self.rightLabel.text = model.status_label;
            self.rightLabel.textColor = [self statusLabelBg:model.status];
            self.line.hidden = NO;
        }
            break;
            
        case 2:
        {
            self.leftLabel.text =@"代充平台 :";
            self.rightLabel.text = model.type_name;
            
        }
            break;
            
        case 3:
        {
            self.leftLabel.text =@"帐号 :";
            self.rightLabel.text = model.account;
            
        }
            break;
            
        case 4:
        {
            self.leftLabel.text =@"帐号名 :";
            self.rightLabel.text = model.account_name;
            
        }
            break;
            
        case 5:
        {
            self.leftLabel.text =@"金额 :";
            self.rightLabel.text = [model.sub_amount gtm_stringByUnescapingFromHTML];
            
        }
            break;
            
        case 6:
        {
            self.leftLabel.text =@"备注 :";
            self.rightLabel.text = model.remark;
            
        }
            break;
            
        case 7:
        {
            self.leftLabel.text =@"服务费 :";
            self.rightLabel.text = [model.service_fee gtm_stringByUnescapingFromHTML];
            
        }
            break;
        case 8:
        {
            self.leftLabel.text =@"总金额 :";
            self.rightLabel.text = [model.grand_total gtm_stringByUnescapingFromHTML];
            
        }
            break;
            
        case 9:
        {
            self.leftLabel.text =@"已支付 :";
            self.rightLabel.text = [model.paid_total gtm_stringByUnescapingFromHTML];
            
        }
            break;
            
        case 10:
        {
            self.leftLabel.text =@"创建时间 :";
            self.rightLabel.text = model.created_at;
            
        }
            break;
            
        case 11:
        {
            self.leftLabel.text =@"支付日期 :";
            self.rightLabel.text = model.paid_at;
            
        }
            break;
        case 12:
        {
            self.leftLabel.text =@"充值截图 :";
            //            self.rightLabel.text = model.order_no;
            
        }
            break;
            
        default:
            break;
    }
    if ([self.rightLabel.text isEqualToString:@""] || !self.rightLabel.text) {
        self.rightLabel.text  =@"  ";
    }
    
    
}


-(void)setupDataInDaichongOrderPay:(SQWoDaichongHistoryModel*)model andRow:(NSInteger)row{
    self.rightLabel.text = @"";
    self.rightLabel.textAlignment = NSTextAlignmentLeft;
    self.line.hidden = YES;
    switch (row) {
        case 0:
        {
            self.leftLabel.text =@"订单号 :";
            self.rightLabel.text = model.order_no;
            self.rightLabel.textColor = [UIColor colorWithHexString:text_yellow];
            
        }
            break;
            
        case 1:
        {
            self.leftLabel.text =@"订单状态 :";
            self.rightLabel.text = model.status_label;
            self.rightLabel.textColor = [self statusLabelBg:model.status];
            self.line.hidden = NO;
        }
            break;
            
        case 2:
        {
            self.leftLabel.text =@"代充平台 :";
            self.rightLabel.text = model.type_name;
            
        }
            break;
            
        case 3:
        {
            self.leftLabel.text =@"帐号 :";
            self.rightLabel.text = model.account;
            
        }
            break;
            
        case 4:
        {
            self.leftLabel.text =@"帐号名 :";
            self.rightLabel.text = model.account_name;
            
        }
            break;
            
        case 5:
        {
            self.leftLabel.text =@"金额 :";
            self.rightLabel.text = [model.sub_amount gtm_stringByUnescapingFromHTML];
            
        }
            break;
            
        case 6:
        {
            self.leftLabel.text =@"备注 :";
            self.rightLabel.text = model.remark;
            
        }
            break;
            
        case 7:
        {
            self.leftLabel.text =@"服务费 :";
            self.rightLabel.text = [model.service_fee gtm_stringByUnescapingFromHTML];
            
        }
            break;
        case 8:
        {
            self.leftLabel.text =@"总金额 :";
            self.rightLabel.text = [model.grand_total gtm_stringByUnescapingFromHTML];
            
        }
            break;
            
        case 9:
        {
            self.leftLabel.text =@"已支付 :";
            self.rightLabel.text = [model.paid_total gtm_stringByUnescapingFromHTML];
            self.line.hidden = NO;
        }
            break;
            
        case 10:
        {
            self.leftLabel.text =@"  ";
            self.rightLabel.text = [model.grand_total gtm_stringByUnescapingFromHTML];
            self.rightLabel.textAlignment = NSTextAlignmentRight;
            self.rightLabel.textColor = [UIColor colorWithHexString:text_yellow];
            
        }
            break;
            
            
        default:
            break;
    }
    if ([self.rightLabel.text isEqualToString:@""] || !self.rightLabel.text) {
        self.rightLabel.text  =@"  ";
    }
    
}


-(void)setupDataInSQDaiChongOrderPayJieGuo:(SQWoDaichongHistroyOrderDetailModel*)model andRow:(NSInteger)row{
    self.line.hidden = YES;
    
    self.rightLabel.text = @"";
    switch (row) {
        case 0:
        {
            self.leftLabel.text =@"订单号 :";
            self.rightLabel.text = model.order_no;
            self.rightLabel.textColor = [UIColor colorWithHexString:text_yellow];
            
        }
            break;
            
        case 1:
        {
            self.leftLabel.text =@"订单状态 :";
            self.rightLabel.text = model.status_label;
            self.rightLabel.textColor = [self statusLabelBg:model.status];
            self.line.hidden = NO;
            
        }
            break;
            
        case 2:
        {
            self.leftLabel.text =@"代充平台 :";
            self.rightLabel.text = model.type_name;
            
        }
            break;
            
        case 3:
        {
            self.leftLabel.text =@"帐号 :";
            self.rightLabel.text = model.account;
            
        }
            break;
            
        case 4:
        {
            self.leftLabel.text =@"帐号名 :";
            self.rightLabel.text = model.account_name;
            
        }
            break;
            
        case 5:
        {
            self.leftLabel.text =@"金额 :";
            self.rightLabel.text = [model.sub_amount gtm_stringByUnescapingFromHTML];
            
        }
            break;
            
        case 6:
        {
            self.leftLabel.text =@"备注 :";
            self.rightLabel.text = model.remark;
            
        }
            break;
            
        case 7:
        {
            self.leftLabel.text =@"服务费 :";
            self.rightLabel.text = [model.service_fee gtm_stringByUnescapingFromHTML];
            
        }
            break;
        case 8:
        {
            self.leftLabel.text =@"总金额 :";
            self.rightLabel.text = [model.grand_total gtm_stringByUnescapingFromHTML];
            
        }
            break;
            
        case 9:
        {
            self.leftLabel.text =@"已支付 :";
            self.rightLabel.text = [model.paid_total gtm_stringByUnescapingFromHTML];
            
        }
            break;
            
        case 10:
        {
            self.leftLabel.text =@"付款时期 :";
            self.rightLabel.text = model.paid_at;
            self.line.hidden = NO;
            
        }
            break;
            
        default:
            break;
    }
    if ([self.rightLabel.text isEqualToString:@""] || !self.rightLabel.text) {
        self.rightLabel.text  =@"  ";
    }
    
    
}


-(__kindof UIColor*)statusLabelBg:(NSString*)status{
    UIColor *coler = [UIColor whiteColor];
    if ([status isEqualToString:@"pendding"] ||
        [status isEqualToString:@"paid_less"]) {
        coler = KColorFromRGB(0xFF4081);
    }
    
    else if ([status isEqualToString:@"paid_confirmed"] ||
             [status isEqualToString:@"finished"]||
             [status isEqualToString:@"complete"]) {
        coler = KColorFromRGB(0x83d79b);
    }
    
    else if ([status isEqualToString:@"waiting_confirm"]
             ) {
        coler = KColorFromRGB(0x008000);
    }else if([status isEqualToString:@"waiting_payment_confirm"]){
        coler = KColorFromRGB(0x008000);
        
    }
    
    else if ([status isEqualToString:@"failed"] ||
             [status isEqualToString:@"overdue"] ||
             [status isEqualToString:@"canceled"]) {
        coler = KColorFromRGB(0xd9dada);
    }
    
    return coler;
}





@end
