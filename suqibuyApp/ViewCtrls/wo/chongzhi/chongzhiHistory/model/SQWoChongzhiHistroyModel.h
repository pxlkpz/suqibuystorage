//
//  SQWoChongzhiHistroyModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWoChongzhiHistroyModel : NSObject

/*
 "": "32",
 "transaction_id": "TSCZ1047",
 "": "payal_normal",
 "": "PAYPAL",
 "": "pendding",
 "": "未支付",
 "": "11.00",
 "": "USD",
 "": "11.00 USD",
 "": "¥62.81",
 "": "2016-06-03 14:12:08",
 "": null,
 "": null,
 "": null,
 "": null
 */

/*
 
 amount = "0.00";
 "amount_symbol" = RMB;
 "banktransfer_desc" = "\U6c47\U6b3e\U5e01\U79cd\Uff1a\U4eba\U6c11\U5e01 CNY|\U6c47\U6b3e\U91d1\U989d\Uff1a111|\U6c47\U6b3e\U94f6\U884c\U540d\U79f0\Uff1a111|\U6c47\U6b3e\U8d26\U53f7\U540d\U79f0\Uff1a1111|Email\Uff1a187681278@126.COM|\U624b\U673a\U53f7\U7801\Uff1a";
 "created_at" = "2016-06-20 10:51:28";
 id = 77;
 note = "<null>";
 "pay_id" = "<null>";
 "payment_code" = banktransfer;
 "payment_method" = "\U56fd\U9645\U6c47\U6b3e";
 "recharge_amount" = "\U5f85\U786e\U8ba4";
 "return_desc" = "<null>";
 status = "waiting_confirm";
 "status_label" = "\U7b49\U5f85\U64cd\U4f5c";
 "transaction_id" = TSCZ1092;
 "transfered_amount" = "";

 */

@property (nonatomic, copy) NSString* my_id;
@property (nonatomic, copy) NSString* transaction_id;
@property (nonatomic, copy) NSString* payment_code;
@property (nonatomic, copy) NSString* payment_method;
@property (nonatomic, copy) NSString* status;
@property (nonatomic, copy) NSString* status_label;
@property (nonatomic, copy) NSString* amount;
@property (nonatomic, copy) NSString* amount_symbol;
@property (nonatomic, copy) NSString* recharge_amount;
@property (nonatomic, copy) NSString* transfered_amount;
@property (nonatomic, copy) NSString* created_at;
@property (nonatomic, copy) NSString* pay_id;
@property (nonatomic, copy) NSString* note;
@property (nonatomic, copy) NSString* banktransfer_desc;
@property (nonatomic, copy) NSString* return_desc;

@end
