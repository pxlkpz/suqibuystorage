//
//  SQWoMyNotiModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWoMyNotiModel : NSObject

//{
//    "id": "129",
//    "subject": "来自<<速尔快递>>的快递：2314512351sad已收货",
//    "message": "来自>的快递：2314512351sad ，小二已经收到，如果你有要求小二待为验货的话，请登录网站查看验货情况。",
//    "is_new": false,
//    "created_at": "2 周 以前"
//
//},
@property (nonatomic, copy) NSString* my_id;
@property (nonatomic, copy) NSString* subject;
@property (nonatomic, copy) NSString* message;
@property (nonatomic, copy) NSString* is_new;
@property (nonatomic, copy) NSString* created_at;
@end
