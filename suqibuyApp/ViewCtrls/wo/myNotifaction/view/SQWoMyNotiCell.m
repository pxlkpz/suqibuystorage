//
//  SQWoMyNotiCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoMyNotiCell.h"
#import "SQWoMyNotiModel.h"

#define jiacu text_heit_Blod
#define xide text_heit

@interface SQWoMyNotiCell ()
@property (nonatomic, strong) UILabel *topLabel;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;

@end

@implementation SQWoMyNotiCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}


- (void)setupData:(SQWoMyNotiModel *)dataEntity {
    _topLabel.text = dataEntity.subject;
    _titleLabel.text = [NSString stringWithFormat:@"发布日期: %@",dataEntity.created_at];
    _contentLabel.text = [dataEntity.message gtm_stringByUnescapingFromHTML];
    if ([dataEntity.is_new isEqualToString:@"0"]) {
        _topLabel.font = [UIFont fontWithName:xide size:15.f];
        _titleLabel.font = [UIFont fontWithName:xide size:12.f];
        _contentLabel.font = [UIFont fontWithName:xide size:14.f];
        
    }else{
        _topLabel.font = [UIFont fontWithName:jiacu size:15.f];
        _titleLabel.font = [UIFont fontWithName:jiacu size:12.f];
        _contentLabel.font = [UIFont fontWithName:jiacu size:14.f];
        
        
        
    }
}

- (void)initView {
    self.tag = 1000;
    
    // Avatar头像
    //    _avatarImageView = [UIImageView new];
    //    [self.contentView addSubview:_avatarImageView];
    //    [_avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.width.and.height.equalTo(@44);
    //        make.left.and.top.equalTo(self.contentView).with.offset(4);
    //        
    //    }];
    // Title - 单行
    _topLabel = [UILabel new];
    [self.contentView addSubview:_topLabel];
    _topLabel.font = [UIFont fontWithName:xide size:14.f];
    
    //    _topLabel.font = [UIFont boldSystemFontOfSize:15];
    _topLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    //    [_topLabel setFont:[UIFont systemFontOfSize:14]];
    [_topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@22);
        make.top.equalTo(self.contentView).with.offset(10);
        make.left.equalTo(self.contentView).with.offset(15);
        make.right.equalTo(self.contentView).with.offset(-10);
    }];
    
    
    // Title - 单行
    _titleLabel = [UILabel new];
    //    [_titleLabel setFont:[UIFont systemFontOfSize:12]];
    _titleLabel.font = [UIFont fontWithName:xide size:12.f];
    
    _titleLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    [self.contentView addSubview:_titleLabel];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@22);
        make.top.equalTo(_topLabel.mas_bottom).with.offset(5);
        make.left.equalTo(self.contentView).with.offset(15);
        make.right.equalTo(self.contentView).with.offset(-10);
    }];
    
    // 计算UILabel的preferredMaxLayoutWidth值，多行时必须设置这个值，否则系统无法决定Label的宽度
    
    // Content - 多行
    _contentLabel = [UILabel new];
    _contentLabel.numberOfLines = 0;
    _contentLabel.font = [UIFont fontWithName:xide size:14.f];
    
    _contentLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    _contentLabel.adjustsFontSizeToFitWidth = YES;
    
    CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width - 4 * 3; // 44 = avatar宽度，4 * 3为padding
    
    _contentLabel.preferredMaxLayoutWidth = preferredMaxWidth; // 多行时必须设置
    
    [_contentLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    
    [self.contentView addSubview:_contentLabel];
    
    
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_titleLabel.mas_bottom).with.offset(5);
        make.left.equalTo(self.contentView).with.offset(15);
        make.right.equalTo(self.contentView).with.offset(-10);
        make.bottom.equalTo(self.contentView).with.offset(-10);
    }];
    
}


@end
