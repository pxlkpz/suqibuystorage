//
//  SQWoDaichongVC.m
//  suqibuyApp
//
//  Created by apple on 16/6/3.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoDaichongVC.h"
#import "SQWoChongzhiCell.h"
//#import "SQWodaichongSelectModel.h"
#import "SQwoDaichongAlipay.h"
#import "SQwoDaichongWixin.h"
#import "SQWoDaichongVCModel.h"
#import "SQWoDaichongHistoryList.h"

@interface SQWoDaichongVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *dataArray;
    UILabel *_showlabel;
    SQWoDaichongVCModel *_model;
}
@property(nonatomic, strong) UITableView* listTableView;

@end

@implementation SQWoDaichongVC


#pragma mark - ---- lefe cycle 🍏🌚

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"代充服务";
    
    dataArray = @[
                  @{@"imageName":@"chong_zhi_alipay_icon", @"title":@"为您的支付宝充值"},
                  @{@"imageName":@"chong_zhi_weixin_icon", @"title":@"为您的微信充值"}
                  ];
    
    [self setRightButton:@"代充历史" action:@selector(chongzhiRightAction:)];
    
    [self buildTableView];
    
    [self request];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - ---- event response 🍐🌛
-(void)chongzhiRightAction:(UIButton*)sender{
    SQWoDaichongHistoryList *controller = [[SQWoDaichongHistoryList alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - ---- private methods 🍊🌜

-(__kindof UIButton*)setRightButton:(NSString*)buttonTitle action:(SEL)action
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    
    CGSize size = [buttonTitle sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:16.0]}];
    [button setFrame:CGRectMake(0, 0, MAX((size.width+15),40), MAX(30,size.height))];
    [button setTitle:buttonTitle forState:UIControlStateNormal];
    if (action != nil) {
        [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    }
    [self setRightNavigationItemWithCustomView:button];
    return button;
}
- (void)setRightNavigationItemWithCustomView:(UIView*)cusView
{
    UIBarButtonItem *m_buttonItem = [[UIBarButtonItem alloc] initWithCustomView:cusView];
    self.navigationItem.rightBarButtonItem = m_buttonItem;
}

-(void)showData{
    _showlabel.attributedText = [self changeLineSpacingString:[NSString stringWithFormat:@"单笔最高限额:%@\n充值服务费:%@", [_model.max_limit gtm_stringByUnescapingFromHTML], _model.fee] andSpacing:7];
    
    if ([_model.weixin_method isEqualToString:@"1"] && [_model.alipay_method isEqualToString:@"1"]) {
        
    }else if (![_model.weixin_method isEqualToString:@"1"] && ![_model.alipay_method isEqualToString:@"1"]){
        dataArray = @[];
        [_listTableView reloadData];
    }else if(![_model.weixin_method isEqualToString:@"1"] && [_model.alipay_method isEqualToString:@"1"]){
        dataArray = @[
                      @{@"imageName":@"chong_zhi_alipay_icon", @"title":@"为您的支付宝充值"},
                      ];
        [_listTableView reloadData];
    }else if(![_model.alipay_method isEqualToString:@"1"] && [_model.weixin_method isEqualToString:@"1"]){
        dataArray = @[
                      @{@"imageName":@"chong_zhi_weixin_icon", @"title":@"为您的微信充值"}
                      ];
        [_listTableView reloadData];
    }
    
}

-(void) request
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    
    NSDictionary *dic = @{@"user_token":user_token};
    
    [self showLoadingHUD]; 
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_Wo_daichong_intro WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _model = [SQWoDaichongVCModel mj_objectWithKeyValues:result];
                    [self showData];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    //[_listTableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.view);
    }];
    [_listTableView layoutIfNeeded];
    [_listTableView setTableFooterView:[self buildTableFooterView]];
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}

-(__kindof UIView *)buildTableFooterView{
    //    充值成功后，无法退款！请注意风险！\n充值为人工充值，2小时内到账，北京时间22.00后, 次日到账。\n充值完成，我司会上传充值图片到后台，您可以后台下载。\n代充服务不支持优惠券，积分付款。
    UIView *bgView = [[UIView alloc] init];
    bgView.frame = CGRectMake(0, 5, screen_width, 200);
    
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:text_shallow_gray];
    label1.textAlignment = NSTextAlignmentLeft;
    label1.numberOfLines = 0;
    [label1 sizeToFit];
    //    label1.text =
    NSString *stringText = @"充值成功后，无法退款！请注意风险！\n充值为人工充值，2小时内到账，北京时间17:00后, 次日到账。\n充值完成，我司会上传充值图片到后台，您可以后台下载。\n代充服务不支持优惠券，积分付款。";
    
    label1.attributedText = [self changeLineSpacingString:stringText andSpacing:7];
    
    
    
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [bgView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(25);
        make.left.equalTo(bgView).offset(10);
        make.right.equalTo(bgView).offset(-10);
    }];
    
    _showlabel = [[UILabel alloc] init];
    _showlabel.textColor = [UIColor colorWithHexString:text_shallow_gray];
    _showlabel.textAlignment = NSTextAlignmentLeft;
    [_showlabel sizeToFit];
    _showlabel.numberOfLines = 0;
    //    _showlabel.text = @"单笔最高限额:\n充值服务费:";
    
    
    
    [_showlabel setFont:[UIFont systemFontOfSize:14]];
    [bgView addSubview:_showlabel];
    [_showlabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label1.mas_bottom).offset(20);
        make.left.equalTo(bgView).offset(10);
        make.right.equalTo(bgView).offset(-10);
    }];
    
    return bgView;
    
}

-(NSMutableAttributedString *)changeLineSpacingString:(NSString*)str andSpacing:(CGFloat)num{
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    
    [paragraphStyle setLineSpacing:num];//调整行间距
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [str length])];
    //    label1.attributedText = attributedString;
    
    return attributedString;
}

#pragma mark - ---- delegate 🍎🌝
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"SQWoChongzhiCell";
    
    SQWoChongzhiCell *cell = (SQWoChongzhiCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQWoChongzhiCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQWoChongzhiCell class]])
            {
                cell = (SQWoChongzhiCell*)nib;
                break;
            }
        }
    }
    
    [cell setupData:dataArray[indexPath.row]];
    
    return cell;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dic = dataArray[indexPath.row];
    if ([dic[@"imageName"] isEqualToString:@"chong_zhi_alipay_icon"]) {
        SQwoDaichongAlipay *vc = [[SQwoDaichongAlipay alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        SQwoDaichongWixin *vc = [[SQwoDaichongWixin alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    // iOS 8 的Self-sizing特性
    return 60;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

#pragma mark - ---- getters and setters 🍋🌞


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
