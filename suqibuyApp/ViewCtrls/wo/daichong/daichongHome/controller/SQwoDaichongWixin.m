//
//  SQwoDaichongWixin.m
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQwoDaichongWixin.h"
#import "PxlTextView.h"
#import "SQDaichongOrderPayVC.h"

@interface SQwoDaichongWixin ()<UITextFieldDelegate>
{
    PxlTextView *viewPxl1;
    PxlTextView *viewPxl2;
    PxlTextView *viewPxl3;
    PxlTextView *viewPxl4;
}
@end

@implementation SQwoDaichongWixin

#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"代充服务";
    
    [self topView];
    
    [self buildUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - ---- event response 🍐🌛
-(void)sureButtonAction:(UIButton*)sender{
    if ([viewPxl1.textfield.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入代充帐号");
    }
    else if ([viewPxl2.textfield.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入代充帐号名");
    }
    else if ([viewPxl3.textfield.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入代充金额");
    }else{
        [self request];
    }
}
#pragma mark - ---- private methods 🍊🌜


-(void)buildUI{
    const NSInteger num = 20;
    viewPxl1 = [[PxlTextView alloc] initWithTwoFrame:CGRectMake(0, 50 +num, screen_width, 35) andString:@""];
    viewPxl1.titleLabel.attributedText = [self setAttributeString:@"微信账户(*)"];
    
    viewPxl1.textfield.keyboardType = UIKeyboardTypeASCIICapable;
    [self.view addSubview:viewPxl1];
    
    viewPxl2 = [[PxlTextView alloc] initWithTwoFrame:CGRectMake(0, 100+num, screen_width, 35) andString:@""];
    viewPxl2.titleLabel.attributedText = [self setAttributeString:@"微信账号名(*)"];
    
    [self.view addSubview:viewPxl2];
    
    viewPxl3 = [[PxlTextView alloc] initWithTwoFrame:CGRectMake(0, 150+num, screen_width, 35) andString:@""];
    viewPxl3.titleLabel.attributedText = [self setAttributeString:@"充值金额(*)"];
    
    viewPxl3.textfield.keyboardType = UIKeyboardTypeDecimalPad;
    viewPxl3.textfield.delegate = self;
    
    
    
    [self.view addSubview:viewPxl3];
    
    viewPxl4 = [[PxlTextView alloc] initWithTwoFrame:CGRectMake(0, 200+num, screen_width, 35) andString:@"备注"];
    [self.view addSubview:viewPxl4];
    
    UIButton* button = [PxlTextView createButton:@"确认"];
    [self.view addSubview:button];
    [button addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-5);
        make.left.equalTo(@5);
        make.height.equalTo(@40);
        make.top.equalTo(viewPxl4.mas_bottom).offset(30);
    }];
    
    
}
//@"支付宝账户(*)"
-(NSMutableAttributedString *)setAttributeString:(NSString*)AttrString1{
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:AttrString1];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:text_color] range:NSMakeRange(0, string.length-3)];
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:simple_button_yellow] range:NSMakeRange(string.length-3, 3)];
    
    [string addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, string.length)];
    //    total_qtyLabel.text = nil;
    
    return string;
}


-(void)topView{
    UIView *bgView = [[UIView alloc] init];
    bgView.frame = CGRectMake(0, 5, screen_width, 60);
    [self.view addSubview:bgView];
    
    UIImageView *imageview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chong_zhi_weixin_icon"]];
    [bgView addSubview:imageview];
    [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(10);
        make.centerY.equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(82, 28));
    }];
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:text_shallow_gray];
    label1.textAlignment = NSTextAlignmentLeft;
    [label1 setFont:[UIFont systemFontOfSize:16]];
    label1.text = @"为您的微信充值";
    [bgView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView);
        make.left.equalTo(imageview.mas_right).offset(20);
        make.right.equalTo(bgView.mas_right).offset(-5);
    }];
    
    
    
    
}


//<div>
//<label>微信账户:</label>
//<input type="text" name="account" value=""/>
//</div>
//<div>
//<label>微信账号名:</label>
//<input type="text" name="account_name" value=""/>
//</div>
//<div>
//<label>充值金额:</label>
//<input type="text" name="amount" value=""/>
//</div>
//<div>
//<label>备注:</label>
//<input type="text" name="remark" value=""/>
//</div>
//<div>

-(void) request
{
    //    微信账户:
    //    微信账号名:
    //    充值金额:
    //    备注:  
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"account":viewPxl1.textfield.text, @"account_name":viewPxl2.textfield.text, @"amount":viewPxl3.textfield.text, @"remark":viewPxl4.textfield.text, @"type":@"2"};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_Wo_daichong_weixin WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    
                    SQDaichongOrderPayVC *vc = [[SQDaichongOrderPayVC alloc] init];
                    SQWoDaichongHistoryModel *model = [SQWoDaichongHistoryModel mj_objectWithKeyValues:result];
                    vc.model = model;
                    
                    [self.navigationController pushViewController:vc animated:YES];
                    
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}



#pragma mark - ---- delegate 🍎🌝
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    return [PxlTextView validateNumber:string];
//}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isHaveDian = YES;
    if ([textField.text rangeOfString:@"."].location == NSNotFound) {
        isHaveDian = NO;
    }
    if ([string length] > 0) {
        
        unichar single = [string characterAtIndex:0];//当前输入的字符
        if ((single >= '0' && single <= '9') || single == '.') {//数据格式正确
            
            //首字母不能为0和小数点
            if([textField.text length] == 0){
                if(single == '.') {
                    //                    [self showError:@"亲，第一个数字不能为小数点"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
                
            }
            
            if ([textField.text isEqualToString:@"0"]) {
                if([textField.text length] == 1){
                    if(single != '.') {
                        //                    [self showError:@"亲，第一个数字不能为小数点"];
                        [textField.text stringByReplacingCharactersInRange:range withString:@""];
                        return NO;
                    }else{
                        isHaveDian = YES;
                        return YES;
                        
                    }
                    
                }
                
            }
            
            
            //输入的字符是否是小数点
            if (single == '.') {
                if(!isHaveDian)//text中还没有小数点
                {
                    isHaveDian = YES;
                    return YES;
                    
                }else{
                    //                    [self showError:@"亲，您已经输入过小数点了"];
                    [textField.text stringByReplacingCharactersInRange:range withString:@""];
                    return NO;
                }
            }else{
                
                if (isHaveDian) {//存在小数点
                    
                    //判断小数点的位数
                    //                    NSRange ran = [textField.text rangeOfString:@"."];
                    return YES;
                    //                    if (range.location - ran.location <= 2) {
                    //                        return YES;
                    //                    }else{
                    //                        //                        [self showError:@"亲，您最多输入两位小数"];
                    //                        return NO;
                    //
                }else{
                    return YES;
                }
            }
            
            
        }else{//输入的数据格式不正确
            //            [self showError:@"亲，您输入的格式不正确"];
            [textField.text stringByReplacingCharactersInRange:range withString:@""];
            return NO;
        }
    }
    else
    {
        return YES;
    }
}

#pragma mark - ---- getters and setters 🍋🌞

@end
