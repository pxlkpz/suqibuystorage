//
//  SQWodaichongSelectModel.m
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWodaichongSelectModel.h"

@implementation SQWodaichongSelectModel
-(void)setdata:(NSInteger)type{
    if (type == 0) {//支付宝
        self.imageName = @"chong_zhi_alipay_icon";
        self.topTitel = @"为您的支付宝充值";
        self.zhanghu = @"支付宝账户(*)";
        self.zhanghaoName = @"支付宝账号名(*)";
        self.type = @"1";
    }else{
        self.type = @"2";
        self.topTitel = @"为您的微信充值";
        self.imageName = @"chong_zhi_weixin_icon";
        self.zhanghu = @"微信账户(*)";
        self.zhanghaoName = @"微信账号名(*)";
    }
}
@end
