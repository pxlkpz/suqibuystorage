//
//  SQWoDaichongVCModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWoDaichongVCModel : NSObject
//            "fee": "5%",
//            "base_fee": "¥0.05",
//            "max_limit": "¥2,000.00",
//            "alipay_method": true,
//            "weixin_method": true

@property (nonatomic, copy) NSString* fee;
@property (nonatomic, copy) NSString* base_fee;
@property (nonatomic, copy) NSString* max_limit;

@property (nonatomic, copy) NSString* alipay_method;
@property (nonatomic, copy) NSString* weixin_method;


@end
