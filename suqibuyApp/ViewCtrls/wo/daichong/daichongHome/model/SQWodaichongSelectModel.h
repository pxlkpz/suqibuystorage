//
//  SQWodaichongSelectModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWodaichongSelectModel : NSObject

@property (nonatomic, copy) NSString* imageName;
@property (nonatomic, copy) NSString* topTitel;

@property (nonatomic, copy) NSString* zhanghu;

@property (nonatomic, copy) NSString* zhanghaoName;

@property (nonatomic, copy) NSString* type;
@property (nonatomic, copy) NSString* url;


-(void)setdata:(NSInteger)type;

@end
