//
//  SQWoDaichongHistoryTableCell.h
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQWoDaichongHistoryTableCell : UITableViewCell
- (void)setupData:(id)dataEntity ;

@end
