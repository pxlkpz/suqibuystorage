//
//  SQWoDaichongHistoryTableCell.m
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoDaichongHistoryTableCell.h"
#import "SQWoDaichongHistoryModel.h"
@interface SQWoDaichongHistoryTableCell ()
@property (weak, nonatomic) IBOutlet UILabel *leftNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *leftOneLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftTwoLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftThreeLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftFourLabel;

@property (weak, nonatomic) IBOutlet UILabel *rightStuesLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightMoneyLabel;

@end

@implementation SQWoDaichongHistoryTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)setupData:(SQWoDaichongHistoryModel *)dataEntity {
//
    self.leftNumberLabel.text = [NSString stringWithFormat:@"订单号:%@", dataEntity.order_no];
    self.leftOneLabel.text = [NSString stringWithFormat:@"充值平台:%@",dataEntity.type_name];
    self.leftTwoLabel.text = [NSString stringWithFormat:@"充值帐号:%@",dataEntity.account];
    self.leftThreeLabel.text = [NSString stringWithFormat:@"充值帐号名:%@",dataEntity.account_name];
    self.leftFourLabel.text = [NSString stringWithFormat:@"充值时间:%@",dataEntity.created_at];
    
    self.rightMoneyLabel.text = [dataEntity.grand_total gtm_stringByUnescapingFromHTML];
    self.rightMoneyLabel.textColor = [UIColor colorWithHexString:text_yellow];
    
    self.rightStuesLabel.text = dataEntity.status_label;
    self.rightStuesLabel.backgroundColor = [self statusLabelBg:dataEntity];
}

-(__kindof UIColor*)statusLabelBg:(SQWoDaichongHistoryModel*)dataEE{
    UIColor *coler = [UIColor whiteColor];
    if ([dataEE.status isEqualToString:@"pendding"]){
        coler = KColorFromRGB(0xFF4081);
    }
    
    else if ([dataEE.status isEqualToString:@"complete"]) {
        coler = KColorFromRGB(0x83d79b);
    }
    
    else if ([dataEE.status isEqualToString:@"waiting_payment_confirm"]
             ) {
        coler = KColorFromRGB(0x008000);
    }
    
    else if ([dataEE.status isEqualToString:@"canceled"]) {
        coler = KColorFromRGB(0xd9dada);
    }
    
    return coler;
}

@end
