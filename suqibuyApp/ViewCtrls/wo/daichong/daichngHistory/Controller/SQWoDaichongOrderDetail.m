//
//  SQWoDaichongOrderDetail.m
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoDaichongOrderDetail.h"

#import "SQWoChongzhiHOrderDetailCell.h"
#import "UIAlertView+BlocksKit.h"
#import "SQDaichongOrderPayVC.h"

@interface SQWoDaichongOrderDetail ()<UITableViewDelegate,UITableViewDataSource>
{
    NSArray *dataArray;
    UIView* _bottomView;
    UIImageView* imageviewgroal;
}
@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQWoDaichongOrderDetail



#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"%@代充订单详细", _dataModel.order_no];
    
    [self buildTableView];
    
    [self buildBottomView];
    
    imageviewgroal = [[UIImageView alloc]init];

    [self getImageView];
    
    if ([_dataModel.status_label isEqualToString:@"待付款"]) {
        _bottomView.hidden = NO;
        [_listTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view).offset(-50);
        }];
    }
    
    
}
#pragma mark - ---- event response 🍐🌛
-(void)cencelButtonAction:(UIButton*)sender{
    [UIAlertView bk_showAlertViewWithTitle:nil message:@"您确定要取消该订单吗?\n取消后不可以恢复,不过可以重新下单。" cancelButtonTitle:@"否" otherButtonTitles:@[@"是"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            [self requestDataCancelOrder:_dataModel.order_no];
        }
    }];
}
-(void)sureButtonAction:(UIButton*)sender{
    SQDaichongOrderPayVC *vc = [[SQDaichongOrderPayVC alloc] init];
    vc.model = _dataModel;
    [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0],self.navigationController.viewControllers[1],self.navigationController.viewControllers[2],vc ]];
}

#pragma mark - ---- private methods 🍊🌜

-(void) requestDataCancelOrder:(NSString*)orderNo
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"order_no":orderNo};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_Wo_daichong_cancel WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}



-(void) requestData:(NSString*)Order_no
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"order_no":Order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_Wo_daichong_orderdetail WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    //                    _detailmodel = [SQWoDaichongHistroyOrderDetailModel mj_objectWithKeyValues:result];
                    //                    [self.listTableView reloadData];
                    _dataModel = [SQWoDaichongHistoryModel mj_objectWithKeyValues:result];
                    [self.listTableView reloadData];
                    [self getImageView];
                    
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        [_listTableView.mj_header endRefreshing];
        
    } WithErrorBlock:^(id errorCode) {
        [_listTableView.mj_header endRefreshing];
        
        [self hideLoadingHUD];
        
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [_listTableView.mj_header endRefreshing];
        
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


-(void)getImageView{
    [imageviewgroal sd_setImageWithURL:[NSURL URLWithString:_dataModel.thumbnail] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            [self buildTableViewFooterViewImage:image];
        }
    }];
    
}

-(void)buildTableViewFooterViewImage:(UIImage*)image{
    UIView* view1 = [[UIView alloc] init];
    CGFloat ratio = (screen_width-30) / image.size.width;
    [view1 setFrame:CGRectMake(0, 0, screen_width, ratio*image.size.height+20)];
    
    UIImageView* imageview = [[UIImageView alloc]init];
    imageview.image = image;
    [view1 addSubview:imageview];
    [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@10);
        make.bottom.mas_equalTo(-10);
        make.left.equalTo(@15);
        make.right.mas_equalTo(0);
    }];
    
    [_listTableView setTableFooterView:view1];
    
}

-(void)buildBottomView{
    _bottomView = [[UIView alloc] init];
    _bottomView.backgroundColor = KColorFromRGB(0xE2E2E2);
    [self.view addSubview:_bottomView];
    _bottomView.hidden = YES;
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.height.equalTo(@50);
    }];
    
    UIView* leftView = [[UIView alloc] init];
    [_bottomView addSubview:leftView];
    [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(_bottomView);
        make.width.equalTo(_bottomView.mas_width).multipliedBy(0.5);
    }];
    
    
    
    UIButton* cancelButton = [[UIButton alloc] init];
    [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [cancelButton setTitle:@"取消订单" forState:UIControlStateNormal];
    [cancelButton setBackgroundColor:KColorFromRGB(0xE2E2E2)];
    [cancelButton addTarget:self action:@selector(cencelButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:cancelButton];
    [cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(_bottomView);
        make.width.equalTo(_bottomView.mas_width).multipliedBy(0.5);
    }];
    
    
    
    UIButton* sureButton = [[UIButton alloc] init];
    [sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureButton setTitle:@"支付订单" forState:UIControlStateNormal];
    [sureButton setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    [sureButton addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomView addSubview:sureButton];
    [sureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(_bottomView);
        make.width.equalTo(_bottomView.mas_width).multipliedBy(0.5);
    }];
    
    
    
}


-(__kindof UIView*)buildTabelHeadView{
    UIView* bgView = [[UIView alloc] init];
    bgView.frame = CGRectMake(0, 0, screen_width, 80);
    
    UIImageView*imageView = [[UIImageView alloc] init ];
    //                             WithImage:[UIImage imageNamed:@""]];
    [bgView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgView).offset(-10);
        make.centerY.equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(82, 28));
    }];
    
    if ([_dataModel.type_name isEqualToString:@"微信"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_weixin_icon"];
    }
    if ([_dataModel.type_name isEqualToString:@"支付宝"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_alipay_icon"];
    }
    
    UILabel *label1 = [UILabel new];
    [label1 sizeToFit];
    [label1 setFont:[UIFont systemFontOfSize:16]];
    label1.textColor = [UIColor colorWithHexString:text_gray];
    [bgView addSubview:label1];
    label1.text = @"代充订单详情";
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(imageView.mas_left).offset(-1);
        make.centerY.equalTo(bgView);
    }];
    
    return bgView;
}
//
-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _listTableView.delegate = self;
    [_listTableView setTableFooterView:[[UIView alloc] init]];
    [_listTableView setTableHeaderView:[self buildTabelHeadView]];
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    if ([UIDevice currentDevice].systemVersion.integerValue > 7) {
        _listTableView.rowHeight = UITableViewAutomaticDimension;
    }
    _listTableView.estimatedRowHeight = 80.0f;
    
    @weakify(self);
    
    
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:self.dataModel.order_no];
    }];
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}




#pragma mark - ---- delegate 🍎🌝
-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 13;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SQWoChongzhiHOrderDetailCell";
    
    SQWoChongzhiHOrderDetailCell *cell = (SQWoChongzhiHOrderDetailCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQWoChongzhiHOrderDetailCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQWoChongzhiHOrderDetailCell class]])
            {
                cell = (SQWoChongzhiHOrderDetailCell*)nib;
                break;
            }
        }
    }
    
    
    
    [cell setupData:_dataModel andRow:indexPath.row];
    
    
    
    return cell;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        return 43;
    }
    
    return UITableViewAutomaticDimension;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}
#pragma mark - ---- getters and setters 🍋🌞

@end
