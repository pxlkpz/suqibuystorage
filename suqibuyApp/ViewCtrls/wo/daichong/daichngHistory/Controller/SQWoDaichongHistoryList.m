
//
//  SQWoDaichongHistoryList.m
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoDaichongHistoryList.h"

//#import "SQWoChongzhiHistoryVC.h"
//#import "SQWoChongzhiHistroyCell.h"
//#import "SQWoChongzhiHistroyModel.h"
//#import "SQWoChongzhiHistoryOrderDetail.h"
#import "SQWoDaichongHistoryModel.h"
#import "SQWoDaichongHistoryTableCell.h"
#import "SQWoDaichongOrderDetail.h"

@interface SQWoDaichongHistoryList ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArray;
}
@property (nonatomic, strong) UITableView *listTableView;

@property (nonatomic, assign) NSInteger currentPage;

@end

@implementation SQWoDaichongHistoryList


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"代充历史记录";
    dataArray = [NSMutableArray array];
    
    [self buildclearView];
    
    [self buildTableView];
    
    
    self.currentPage = 1;
    [self requestData:1];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

#pragma mark - ---- event response 🍐🌛

#pragma mark - ---- private methods 🍊🌜
-(void)buildclearView{
    
    UILabel* noItemLabel = [[UILabel alloc] init];
    noItemLabel.text = @"当前列表为空\n马上开始创建吧！";
    noItemLabel.textColor = [UIColor colorWithHexString:text_gray];
    noItemLabel.textAlignment = NSTextAlignmentCenter;
    noItemLabel.font = [UIFont systemFontOfSize:14];
    noItemLabel.numberOfLines = 2;
    [self.view addSubview:noItemLabel];
    [noItemLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
    }];
    
}
-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.hidden = YES;
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    [_listTableView setTableFooterView:[[UIView alloc] init]];
    //[_listTableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    
    @weakify(self);
    
    
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        self.currentPage = 1;
        [self requestData:1];
    }];
    
    _listTableView.mj_footer  = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:self.currentPage+1];
    }];
    _listTableView.mj_footer.hidden = YES;
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}
-(void) requestData:(NSInteger ) page
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSString *pageString = [NSString stringWithFormat:@"%ld",(long)page];
    NSDictionary *dic = @{@"user_token":user_token,@"page":pageString};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_Wo_daichong_list WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if (page == 1)
                        {
                            [dataArray removeAllObjects];
                        }
                        else
                        {
                            if ([items count] == 0)
                            {
                                showDefaultPromptTextHUD(@"没有更多内容了");
                                [self.listTableView.mj_footer endRefreshingWithNoMoreData];
                                
                                return ;

                            }
                            else
                            {
                                self.currentPage++;
                            }
                        }
                        
                        _listTableView.hidden = NO;
                        [dataArray addObjectsFromArray:[SQWoDaichongHistoryModel mj_objectArrayWithKeyValuesArray:items]];
                        
                        
                        [_listTableView reloadData];
                        _listTableView.mj_footer.hidden = NO;
                        
                    }else{
                        if ([result[@"total_results"] integerValue] != 0) {
                            showDefaultPromptTextHUD(@"没有更多内容了");
                            [self.listTableView.mj_footer endRefreshingWithNoMoreData];
                            
                            return ;

                        }
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark - ---- delegate 🍎🌝

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dataArray count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SQWoDaichongHistoryTableCell";
    
    SQWoDaichongHistoryTableCell *cell = (SQWoDaichongHistoryTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQWoDaichongHistoryTableCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQWoDaichongHistoryTableCell class]])
            {
                cell = (SQWoDaichongHistoryTableCell*)nib;
                break;
            }
        }
    }
    
    [cell setupData:dataArray[indexPath.row]];
    
    return cell;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SQWoDaichongHistoryModel* model = dataArray[indexPath.row];
    SQWoDaichongOrderDetail *CV = [[SQWoDaichongOrderDetail alloc] init];
    CV.dataModel = model;
    [self.navigationController pushViewController:CV animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

#pragma mark - ---- getters and setters 🍋🌞


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
