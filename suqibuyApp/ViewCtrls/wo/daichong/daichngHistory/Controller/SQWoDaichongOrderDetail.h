//
//  SQWoDaichongOrderDetail.h
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"
#import "SQWoDaichongHistoryModel.h"
@interface SQWoDaichongOrderDetail : SQBaseViewController

@property(nonatomic , strong) SQWoDaichongHistoryModel *dataModel;

@end
