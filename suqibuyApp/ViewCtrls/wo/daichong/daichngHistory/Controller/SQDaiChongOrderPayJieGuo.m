//
//  SQDaiChongOrderPayJieGuo.m
//  suqibuyApp
//
//  Created by apple on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQDaiChongOrderPayJieGuo.h"
#import "SQWoChongzhiHOrderDetailCell.h"
#import "PxlTextView.h"
#import "SQWoDaichongHistoryModel.h"
#import "SQWoDaichongOrderDetail.h"
@interface SQDaiChongOrderPayJieGuo ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQDaiChongOrderPayJieGuo


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"%@代充订单支付结果",_model.order_no];
    
    [self buildTableView];
    
}


#pragma mark - ---- event response 🍐🌛
-(void)sureButtonAction{
    SQWoDaichongHistoryModel *detailmodel = [SQWoDaichongHistoryModel mj_objectWithKeyValues:_model.mj_keyValues];
    SQWoDaichongOrderDetail* vc = [[SQWoDaichongOrderDetail alloc] init];
    vc.dataModel = detailmodel;
    [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0],self.navigationController.viewControllers[1], self.navigationController.viewControllers[2],vc]];
}
#pragma mark - ---- private methods 🍊🌜
-(__kindof UIView*)buildTabelHeadView{
    UIView* bgView = [[UIView alloc] init];
    bgView.frame = CGRectMake(0, 0, screen_width, 80);
    
    UIImageView*imageView = [[UIImageView alloc] init ];
    [bgView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgView).offset(-10);
        make.centerY.equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(82, 28));
    }];
    
    if ([_model.type_name isEqualToString:@"微信"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_weixin_icon"];
    }
    if ([_model.type_name isEqualToString:@"支付宝"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_alipay_icon"];
    }
    
    UILabel *label1 = [UILabel new];
    [label1 sizeToFit];
    [label1 setFont:[UIFont systemFontOfSize:16]];
    label1.textColor = [UIColor colorWithHexString:text_gray];
    [bgView addSubview:label1];
    label1.text = @"代充订单详情";
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(imageView.mas_left).offset(-1);
        make.centerY.equalTo(bgView);
    }];
    
    return bgView;
}

-(__kindof UIView*)buildTableViewFooter{
    UIView* bgView = [[UIView alloc] init];
    bgView.frame = CGRectMake(0, 0, screen_width, 140);
    
    
    UILabel *labelBottom= [PxlTextView createLabel];
    labelBottom.textAlignment = NSTextAlignmentLeft;
    
    labelBottom.text = @"我们的服务人员将在确认成功付款后,第一时间为您完成充值。\n如果有调整,请第一时间通知我们做拦截处理。";
    labelBottom.numberOfLines = 0;
    labelBottom.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [bgView addSubview:labelBottom];
    [labelBottom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(10);
        make.right.equalTo(bgView).offset(-10);
        
        make.left.equalTo(bgView).offset(10);
    }];
    
    UIButton* button = [PxlTextView createButton:@"查看订单详情以及更多操作"];
    [bgView addSubview:button];
    [button addTarget:self action:@selector(sureButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelBottom.mas_bottom).offset(30);
        make.left.equalTo(bgView).offset(10);
        make.right.equalTo(bgView).offset(-10);
        make.height.equalTo(@35);
    }];
    return bgView;
    
}

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _listTableView.dataSource = self;
    _listTableView.tag = 5;
    _listTableView.delegate = self;
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 44.0f;
    
    
    [_listTableView setTableFooterView:[[UIView alloc] init]];
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    [_listTableView layoutIfNeeded];
    
    [_listTableView setTableHeaderView:[self buildTabelHeadView]];
    [_listTableView setTableFooterView:[self buildTableViewFooter]];
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}

#pragma mark - ---- delegate 🍎🌝
#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 11;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //    if (indexPath.row < 11) {
    static NSString *CellIdentifier = @"SQWoChongzhiHOrderDetailCell";
    
    SQWoChongzhiHOrderDetailCell *cell = (SQWoChongzhiHOrderDetailCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQWoChongzhiHOrderDetailCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQWoChongzhiHOrderDetailCell class]])
            {
                cell = (SQWoChongzhiHOrderDetailCell*)nib;
                break;
            }
        }
    }
    [cell setupDataInSQDaiChongOrderPayJieGuo:_model andRow:indexPath.row];
    
    return cell;
}




-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (indexPath.row < 13) {
    //        return UITableViewAutomaticDimension;
    //        
    //    }else if (indexPath.row == 12){
    //        return 150;
    //    }
    
    return UITableViewAutomaticDimension;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}
#pragma mark - ---- getters and setters 🍋🌞


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
