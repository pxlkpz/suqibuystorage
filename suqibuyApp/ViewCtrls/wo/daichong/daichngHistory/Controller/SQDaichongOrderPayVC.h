//
//  SQDaichongOrderPayVC.h
//  suqibuyApp
//
//  Created by apple on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"
#import "SQWoDaichongHistoryModel.h"
@interface SQDaichongOrderPayVC : SQBaseViewController

@property(nonatomic , strong) SQWoDaichongHistoryModel *model;

@end
