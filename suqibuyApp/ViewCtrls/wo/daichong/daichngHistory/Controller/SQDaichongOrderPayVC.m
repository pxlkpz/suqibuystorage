//
//  SQDaichongOrderPayVC.m
//  suqibuyApp
//
//  Created by apple on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQDaichongOrderPayVC.h"


#import "SQzhuanyunListOrderDetailCell.h"
#import "SQzhuanyunPayMoneyCell.h"
#import "SQzhuanyunPayPassword.h"
#import "SQzhuanyunPayJieGuo.h"
#import "SQWoChongzhiHOrderDetailCell.h"
#import "SQWoDaichongHistroyOrderDetailModel.h"
#import "SQDaiChongOrderPayJieGuo.h"

@interface SQDaichongOrderPayVC ()<UITableViewDataSource,UITableViewDelegate>{
    SQWoDaichongHistroyOrderDetailModel*_detailmodel;
    
}
@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQDaichongOrderPayVC

#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"%@代充订单支付",_model.order_no];
    _detailmodel = [SQWoDaichongHistroyOrderDetailModel mj_objectWithKeyValues:_model.mj_keyValues];
    [self buildTableView];
    
    [self requestData:_model.order_no];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationAction:) name:@"SQzhuanyunPayNoti" object:nil];
    
    
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - ---- event response 🍐🌛
-(void)notificationAction:(NSNotification*)noti{
    
    NSString* password = [noti object];
    if ([password isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入您账户的登陆密码");
        return;
    }
    [self requestDataPassWord:password];
    
}
#pragma mark - ---- private methods 🍊🌜


-(void) requestData:(NSString*)Order_no
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"order_no":Order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_Wo_daichong_orderdetail WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _detailmodel = [SQWoDaichongHistroyOrderDetailModel mj_objectWithKeyValues:result];
                    [self.listTableView reloadData];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        [_listTableView.mj_header endRefreshing];
        
    } WithErrorBlock:^(id errorCode) {
        [_listTableView.mj_header endRefreshing];
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [_listTableView.mj_header endRefreshing];
        
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void)requestDataPassWord:(NSString*)password
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"order_no":_detailmodel.order_no, @"confirm_password":password};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_Wo_daichong_orderpayment WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _detailmodel = [SQWoDaichongHistroyOrderDetailModel mj_objectWithKeyValues:result];
                    SQDaiChongOrderPayJieGuo* vc = [[SQDaiChongOrderPayJieGuo alloc] init];
                    vc.model = _detailmodel;
                    [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0],self.navigationController.viewControllers[1], self.navigationController.viewControllers[2],vc]];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}



-(__kindof UIView*)buildTabelHeadView{
    UIView* bgView = [[UIView alloc] init];
    bgView.frame = CGRectMake(0, 0, screen_width, 80);
    
    UIImageView*imageView = [[UIImageView alloc] init ];
    [bgView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgView).offset(-10);
        make.centerY.equalTo(bgView);
        make.size.mas_equalTo(CGSizeMake(82, 28));
    }];
    
    if ([_model.type_name isEqualToString:@"微信"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_weixin_icon"];
    }
    if ([_model.type_name isEqualToString:@"支付宝"]) {
        imageView.image = [UIImage imageNamed:@"chong_zhi_alipay_icon"];
    }
    
    UILabel *label1 = [UILabel new];
    [label1 sizeToFit];
    [label1 setFont:[UIFont systemFontOfSize:16]];
    label1.textColor = [UIColor colorWithHexString:text_gray];
    [bgView addSubview:label1];
    label1.text = @"代充订单详情";
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(imageView.mas_left).offset(-1);
        make.centerY.equalTo(bgView);
    }];
    
    return bgView;
}

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.tag = 5;
    _listTableView.delegate = self;
    _listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [_listTableView setTableHeaderView:[self buildTabelHeadView]];
    
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 44.0f;
    
    UINib *nib = [UINib nibWithNibName:@"SQzhuanyunPayMoneyCell" bundle:nil];
    [_listTableView registerNib:nib forCellReuseIdentifier:@"SQzhuanyunPayMoneyCell"];
    
    
    UINib *nib3 = [UINib nibWithNibName:@"SQzhuanyunPayPassword" bundle:nil];
    [_listTableView registerNib:nib3 forCellReuseIdentifier:@"SQzhuanyunPayPassword"];
    
    
    
    [_listTableView setTableFooterView:[[UIView alloc] init]];
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    
    @weakify(self);
    
    
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:self.model.order_no];
    }];
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}

#pragma mark - ---- delegate 🍎🌝
#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 13;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row < 11) {
        static NSString *CellIdentifier = @"SQWoChongzhiHOrderDetailCell";
        
        SQWoChongzhiHOrderDetailCell *cell = (SQWoChongzhiHOrderDetailCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQWoChongzhiHOrderDetailCell"
                                                          owner:nil
                                                        options:nil];
            for (id nib in nibs)
            {
                if ([nib isKindOfClass:[SQWoChongzhiHOrderDetailCell class]])
                {
                    cell = (SQWoChongzhiHOrderDetailCell*)nib;
                    break;
                }
            }
        }
        [cell setupDataInDaichongOrderPay:_detailmodel andRow:indexPath.row];
        
        return cell;
    }
    
    if (indexPath.row == 11) {
        NSString *identifier = @"SQzhuanyunPayMoneyCell";
        SQzhuanyunPayMoneyCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        cell.rightMoneyLabel.text = [_detailmodel.user_cash_amount gtm_stringByUnescapingFromHTML];
        if ([_detailmodel.show_chongzhi_link boolValue]) {
            cell.rightButton.hidden = NO;
        }else{
            cell.rightButton.hidden = YES;
        }
        //        [cell setdata:_detailmodel andindexRow:indexPath.row];
        return cell;
        
    }else if(indexPath.row == 12){
        
        NSString *identifier = @"SQzhuanyunPayPassword";
        SQzhuanyunPayPassword *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        [cell setdata:_model andindexRow:indexPath.row];
        return cell;
        
    }
    
    
    
    return nil;
}




-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1 || indexPath.row == 9) {
        return 43;
    }
    
    if (indexPath.row < 12) {
        return UITableViewAutomaticDimension;
        
    }else if (indexPath.row == 12){
        return 160;
    }
    
    return UITableViewAutomaticDimension;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

#pragma mark - ---- getters and setters 🍋🌞



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
