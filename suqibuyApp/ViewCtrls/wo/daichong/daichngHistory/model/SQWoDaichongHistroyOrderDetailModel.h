//
//  SQWoDaichongHistroyOrderDetailModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWoDaichongHistroyOrderDetailModel : NSObject

//"order_no": "DC10002",
//"can_cancel": false,
//"can_payment": false,
//"type_id": "1",
//"type_name": "支付宝",
//"status": "complete",
//"status_label": "已完成",
//"user_cash_amount": "¥99,256.21",
//"account": "1",
//"account_name": "1",
//"remark": "1",
//"sub_amount": "¥1.00",
//"service_fee": "¥0.03",
//"grand_total": "¥1.03",
//"paid_total": "¥1.03",
//"created_at": "2015-06-29 11:47:16",
//"paid_at": "2015-06-29 11:47:22",
//"thumbnail": "https://t1.shtag.com/uploads/daichong/20150629/1435549679_9854.png",
//"show_chongzhi_link": false,
//"remain_waiting_payment": "¥0.00"
//},
//"success": true
//}
@property (nonatomic, copy) NSString* order_no;
@property (nonatomic, copy) NSString* can_cancel;
@property (nonatomic, copy) NSString* can_payment;
@property (nonatomic, copy) NSString* type_id;
@property (nonatomic, copy) NSString* type_name;
@property (nonatomic, copy) NSString* status;
@property (nonatomic, copy) NSString* status_label;
@property (nonatomic, copy) NSString* user_cash_amount;
@property (nonatomic, copy) NSString* account;
@property (nonatomic, copy) NSString* account_name;
@property (nonatomic, copy) NSString* remark;
@property (nonatomic, copy) NSString* sub_amount;
@property (nonatomic, copy) NSString* service_fee;
@property (nonatomic, copy) NSString* grand_total;
@property (nonatomic, copy) NSString* paid_total;
@property (nonatomic, copy) NSString* created_at;
@property (nonatomic, copy) NSString* paid_at;
@property (nonatomic, copy) NSString* show_chongzhi_link;
@property (nonatomic, copy) NSString* thumbnail;
@property (nonatomic, copy) NSString* remain_waiting_payment;

@end
