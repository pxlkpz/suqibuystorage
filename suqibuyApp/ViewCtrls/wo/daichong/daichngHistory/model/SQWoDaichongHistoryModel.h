//
//  SQWoDaichongHistoryModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/6.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWoDaichongHistoryModel : NSObject
//{
//account = "\U652f\U4ed8\U5b9d\U8d26\U6237";
//"account_name" = "\U652f\U4ed8\U5b9d\U8d26\U53f7\U540d";
//"can_cancel" = 0;
//"can_payment" = 0;
//"created_at" = "2015-06-29 12:24:36";
//"grand_total" = "&yen;3.09";
//"order_no" = DC10004;
//"paid_at" = "<null>";
//"paid_total" = "&yen;0.00";
//"remain_waiting_payment" = "&yen;0.00";
//remark = "";
//"service_fee" = "&yen;0.09";
//"show_chongzhi_link" = 0;
//status = canceled;
//"status_label" = "\U5df2\U53d6\U6d88";
//"sub_amount" = "&yen;3.00";
//thumbnail = "https://t1.shtag.com";
//"type_id" = 1;
//"type_name" = "\U652f\U4ed8\U5b9d";
//"user_cash_amount" = "&yen;13.27";
//},


/*
 {
 account = Ratty;
 "account_name" = Fgh;
 "can_cancel" = 1;
 "can_payment" = 1;
 "created_at" = "2016-06-17 23:18:13";
 "grand_total" = "&yen;1.05";
 "order_no" = DC10028;
 "paid_at" = "<null>";
 "paid_total" = "&yen;0.00";
 "remain_waiting_payment" = "&yen;1.05";
 remark = "";
 "service_fee" = "&yen;0.05";
 "show_chongzhi_link" = 0;
 status = pendding;
 "status_label" = "\U5f85\U4ed8\U6b3e";
 "sub_amount" = "&yen;1.00";
 thumbnail = "https://t1.shtag.com";
 "type_id" = 1;
 "type_name" = "\U652f\U4ed8\U5b9d";
 "user_cash_amount" = "&yen;94,257.44";
 }

 */

@property (nonatomic, copy) NSString* order_no;
@property (nonatomic, copy) NSString* can_cancel;
@property (nonatomic, copy) NSString* can_payment;
@property (nonatomic, copy) NSString* type_id;
@property (nonatomic, copy) NSString* type_name;
@property (nonatomic, copy) NSString* status;
@property (nonatomic, copy) NSString* status_label;
@property (nonatomic, copy) NSString* user_cash_amount;
@property (nonatomic, copy) NSString* account;
@property (nonatomic, copy) NSString* account_name;
@property (nonatomic, copy) NSString* remark;
@property (nonatomic, copy) NSString* sub_amount;
@property (nonatomic, copy) NSString* service_fee;
@property (nonatomic, copy) NSString* grand_total;
@property (nonatomic, copy) NSString* paid_total;
@property (nonatomic, copy) NSString* created_at;
@property (nonatomic, copy) NSString* paid_at;
@property (nonatomic, copy) NSString* thumbnail;
@property (nonatomic, copy) NSString* show_chongzhi_link;
@property (nonatomic, copy) NSString* remain_waiting_payment;




@end
