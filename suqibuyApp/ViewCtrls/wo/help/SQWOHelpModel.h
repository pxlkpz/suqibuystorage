//
//  SQWOHelpModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/2.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWOHelpModel : NSObject
//result =     {
//    "help_email" = "service@suqibuy.com";
//    "help_phone" = "0086-021-59890868";
//    "help_qq" = 2822304263;
//    "help_weixin" = li808604;
//};

@property (nonatomic, copy) NSString* help_email;
@property (nonatomic, copy) NSString* help_phone;

@property (nonatomic, copy) NSString* help_qq;

@property (nonatomic, copy) NSString* help_weixin;

@end
