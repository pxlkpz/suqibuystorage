//
//  SQWoHelpVC.m
//  suqibuyApp
//
//  Created by apple on 16/6/2.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoHelpVC.h"
#import "SQWOHelpModel.h"

@interface SQWoHelpVC ()
{
    UILabel *showLabel1;
    UILabel *showLabel2;
    UILabel *showLabel3;
    UILabel *showLabel4;
    SQWOHelpModel *_model;
    UIView* centerLine;
}
@end

@implementation SQWoHelpVC



#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"帮助中心";
    
    [self buildUI];
    
    [self requstHelpdata];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - ---- event response 🍐🌛
-(void)sureButtonAction:(UIButton*)sender{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    switch (sender.tag) {
        case 1:
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",_model.help_phone]]];
            
        }
            break;
        case 2:
        {
            //#warning 邮件
            //            NSString *url = [NSString stringWithString: @"mailto:foo@example.com?cc=bar@example.com& subject=Greetings%20from%20Cupertino!& body=Wish%20you%20were%20here!"];
            NSString *url = [NSString stringWithFormat:@"mailto:%@",_model.help_email];
            [[UIApplication sharedApplication]
             openURL: [NSURL URLWithString: url]];
        }
            break;
        case 3:
        {
            showDefaultPromptTextHUD(@"复制QQ号成功");
            pasteboard.string = _model.help_qq;
            
        }
            break;
        case 4:
        {
            showDefaultPromptTextHUD(@"复制微信号成功");
            pasteboard.string = _model.help_weixin;
            
        }
            break;
        default:
            break;
    }
    
    
}
#pragma mark - ---- private methods 🍊🌜
-(void)buildUI{
    
    centerLine = [[UIView alloc]init];
    centerLine.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:centerLine];
    
    [centerLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.view);
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    
    
    UIView* centerLine2 = [[UIView alloc]init];
    centerLine2.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:centerLine2];
    
    [centerLine2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.width.mas_equalTo(0.5);
        make.top.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
    showLabel1 = [self buildOneView:1 andImageName:@"phone" andLabel1Text:@"客服热线"];
    showLabel2 = [self buildOneView:2 andImageName:@"email" andLabel1Text:@"客服邮箱"];
    showLabel3 = [self buildOneView:3 andImageName:@"qq" andLabel1Text:@"QQ在线"];
    showLabel4 = [self buildOneView:4 andImageName:@"weixin" andLabel1Text:@"微信公众号"];

    
}


-(__kindof UILabel *)buildOneView:(NSInteger)number andImageName:(NSString*)imageName andLabel1Text:(NSString*)label1text{
    
    
    CGFloat numX = 0;
    CGFloat numY = 0;
    NSString *buttonName = @"";
    switch (number) {
        case 1:
        {
            buttonName = @"立即拨打";
            numX = 0;
            numY = 0;
        }
            break;
        case 2:
        {
            buttonName = @"邮件咨询";
            numX = KScreenWidth/2;
            numY = 0;
            
        }
            break;
        case 3:
        {
            buttonName = @"复制QQ";
            numX = 0;
            numY = 1;
            
        }
            break;
        case 4:
        {
            buttonName = @"复制微信";
            numX = KScreenWidth/2;
            numY = 1;
            
        }
            break;
        default:
            break;
    }
    
    
    UIView*bgView = [[UIView alloc] init];
    //    bgView.frame = CGRectMake(numX, numY, KScreenWidth/2, self.view.frame.size.height/2);
    [self.view addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(numX);
        if (numY == 1) {
            make.top.equalTo(centerLine.mas_bottom);
        }else{
            make.top.mas_equalTo(0);
   
        }
        make.height.equalTo(self.view).multipliedBy(0.5);
        make.width.equalTo(self.view).multipliedBy(0.5);
    }];
    
    UIView* centerview = [[UIView alloc] init];
    [bgView addSubview:centerview];
    [centerview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.centerY.equalTo(bgView);
        //        .offset(self.view.frame.size.height/4 - 100);
        make.height.equalTo(@200);
        //        make.width.equalTo(self.view).multipliedBy(0.5);
        
    }];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
    [centerview addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(centerview).offset(0);
        make.size.mas_equalTo(CGSizeMake(100, 100));
        make.centerX.equalTo(bgView);
    }];
    
    UILabel *label1 = [[UILabel alloc] init];
    [label1 sizeToFit];
    [label1 setFont:[UIFont systemFontOfSize:15 ]];
    label1.text = label1text;
    [centerview addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_bottom).offset(5);
        make.centerX.equalTo(centerview);
    }];
    
    
    UILabel *label2 = [[UILabel alloc] init];
    [label2 sizeToFit];
    [label2 setFont:[UIFont systemFontOfSize:14 ]];
    //    label2.text = @"111";
    
    [centerview addSubview:label2];
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label1.mas_bottom).offset(5);
        make.centerX.equalTo(centerview);
    }];
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:buttonName forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    [button addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [centerview addSubview:button];
    button.tag = number;
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(centerview.mas_width).multipliedBy(0.6);
        make.centerX.equalTo(centerview);
        make.height.equalTo(@35);
        make.top.equalTo(label2.mas_bottom).offset(10);
    }];
    
    
    
    return label2;
}

-(void) showdata:(NSDictionary*)dic{
    _model = [SQWOHelpModel mj_objectWithKeyValues:dic];
    showLabel1.text  = _model.help_phone;
    showLabel2.text = _model.help_email;
    showLabel3.text  = _model.help_qq;
    showLabel4.text = _model.help_weixin;
}

-(void)requstHelpdata{
    [self showLoadingHUD];
    
    
    @weakify(self);
    
    NSDictionary *dic = @{};
    
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_Softhelp_softhelp_helpdata WithParameter:dic WithFinishBlock:^(id returnValue) {
        @strongify(self);
        
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                [self showdata:dic[@"result"]];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}

#pragma mark - ---- delegate 🍎🌝

#pragma mark - ---- getters and setters 🍋🌞


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
