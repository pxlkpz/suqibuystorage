//
//  SQWoUserInfo.m
//  suqibuyApp
//
//  Created by apple on 16/6/5.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoUserInfo.h"
#import "PxlTextView.h"
#import "UIActionSheet+BlocksKit.h"
#import "CameraTakeManager.h"
#import "HTTPManager.h"
#import "SQAddressSearchViewCtrl.h"

@interface SQWoUserInfo ()<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,SQAddressSearchViewDelegate>
{
    PxlTextView* viewPxl1;
    PxlTextView* viewPxl2;
    PxlTextView* viewPxl3;
    PxlTextView* viewPxl4;
    PxlTextView* viewPxl5;
    PxlTextView* viewPxl6;
    PxlTextView* viewPxl7;
    
    UIButton *betweenButton;
    
    MBZPKTextField *countryTextfield;
}


@property (nonatomic, strong) UIImageView* headImageView;
@property(nonatomic, strong) UITableView *listTableView;
@end

@implementation SQWoUserInfo


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.userInfomodel) {
        self.userInfomodel = [SQWoUserInfoModel mj_objectWithKeyValues:self.dataDic];
    }
    self.title = @"我的信息修改";
    
    

    [self buildTableView];
    [self showData];
    
}


-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - ---- event response 🍐🌛

#pragma mark === 更换头像 ===

-(void)userTapRateView{
    @weakify(self);
    
    [[CameraTakeManager sharedInstance] cameraSheetInController:self handler:^(UIImage *image, NSString *imagePath) {
        @strongify(self);
        
        [self requstHeadImage:image];
        
    } cancelHandler:^{}];
    
}


-(void)sureButtonAction:(UIButton*)sender{
    if ([viewPxl1.textfield.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入昵称");
    }
    _userInfomodel.user_data.nickname = viewPxl1.textfield.text;
    _userInfomodel.user_data.full_name =viewPxl2.textfield.text;
     _userInfomodel.user_data.location = viewPxl3.textfield.text;
    _userInfomodel.user_data.tel_cell = viewPxl4.textfield.text;
    _userInfomodel.user_data.weixin = viewPxl5.textfield.text;
    _userInfomodel.user_data.weibo = viewPxl6.textfield.text;
    _userInfomodel.user_data.qq = viewPxl7.textfield.text;
    
 
    [self requestData];
}
-(void)sexSelectButtonAction:(UIButton*)sender{
    if (betweenButton) {
        betweenButton.selected = NO;
    }
    if (sender.tag == 10) {
        _userInfomodel.user_data.sex = @"男";
    }else{
        _userInfomodel.user_data.sex = @"女";
    }
    
    sender.selected = YES;
    betweenButton = sender;
}

-(void)buttonAction:(UIButton*)sender{
    SQAddressSearchViewCtrl *controller = [[SQAddressSearchViewCtrl alloc] init];
    controller.delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - ---- private methods 🍊🌜

-(void)showData{
    viewPxl1.textfield.text = _userInfomodel.user_data.nickname;
    viewPxl2.textfield.text = _userInfomodel.user_data.full_name;
    viewPxl3.textfield.text = _userInfomodel.user_data.location;
    viewPxl4.textfield.text = _userInfomodel.user_data.tel_cell;
    viewPxl5.textfield.text = _userInfomodel.user_data.weixin;
    viewPxl6.textfield.text = _userInfomodel.user_data.weibo;
    viewPxl7.textfield.text = _userInfomodel.user_data.qq;
    
}
-(__kindof UIView*)buildTabelHeadView{
    if ([_userInfomodel.user_data.info_score isEqualToString:@""] || !_userInfomodel.user_data.info_score) {
        return [[UIView alloc] init];
    }
    
    UIView *tabelHeadView = [[UIView alloc] init];
    tabelHeadView.frame = CGRectMake(0, 0, screen_width, 70);
    
    
    UIView* bgView = [[UIView alloc] init];
    [tabelHeadView addSubview:bgView];
    bgView.backgroundColor = KColorFromRGB(0xf7f7f7);
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(tabelHeadView).offset(10);
        make.left.equalTo(tabelHeadView).offset(10);
        make.right.equalTo(tabelHeadView).offset(-10);
        make.height.equalTo(@50);
    }];
    bgView.layer.borderWidth = 1;
    bgView.layer.borderColor = [UIColor colorWithHexString:border_color].CGColor;
    bgView.layer.cornerRadius = allCornerRadius;
    
    
    UIImageView *imageview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"notice_icon"]];
    [bgView addSubview:imageview];
    [imageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView);
        make.left.equalTo(@5);
        make.size.mas_equalTo(CGSizeMake(21, 21));
    }];
    
    
    
    
    
    NSString * htmlString = _userInfomodel.user_data.info_score;
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    UILabel * myLabel = [[UILabel alloc] init];
    myLabel.numberOfLines = 0;
    [myLabel setFont:[UIFont systemFontOfSize:15]];
    myLabel.attributedText = attrStr;
    [bgView addSubview:myLabel];
    [myLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imageview.mas_right).offset(5);
        make.top.equalTo(@5);
        make.right.equalTo(bgView).offset(-5);
        make.bottom.equalTo(bgView).offset(-5);
    }];
    
    
    return  tabelHeadView;
    
    
}

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    _listTableView.bounces = NO;
    _listTableView.showsVerticalScrollIndicator = NO;
    _listTableView.frame = CGRectMake(0, 0, screen_width, screen_height);

    
    _listTableView.estimatedRowHeight = 80.0f;
    
    
    [self.view addSubview:_listTableView];
    [_listTableView setTableHeaderView:[self buildTabelHeadView]];
    [_listTableView setTableFooterView:[self buildFootView]];
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}


-(__kindof UIView*) buildFootView{
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 780)];
    
    
    _headImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"user_bar"]];
    [bgView addSubview:_headImageView];
    self.headImageView.layer.borderWidth = 2;
    self.headImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bgView);
        make.top.equalTo(@10);
        make.size.mas_equalTo(CGSizeMake(100, 100));
    }];
    
    [_headImageView sd_setImageWithURL:[NSURL URLWithString:_userInfomodel.avatar_url]];
    
    _headImageView.layer.cornerRadius = 50;
    _headImageView.layer.masksToBounds = YES;
    _headImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTapRateView)];
    [_headImageView addGestureRecognizer:tapGesture];
    
    
    UIImageView *imageview1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"profile_edit_icon"]];
    [bgView addSubview:imageview1];
    [imageview1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bgView).offset(25);
        make.top.equalTo(@73);
        make.size.mas_equalTo(CGSizeMake(33, 33));
    }];
    imageview1.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTapRateView)];
    [imageview1 addGestureRecognizer:tapGesture1];
    
    
    
    
    const NSInteger num = 133;
    
    
    UIView *line1 = [[UIView alloc]init];
    [bgView addSubview:line1];
    line1.alpha = 0.6;
    line1.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headImageView.mas_bottom).offset(25);
        make.height.equalTo(@1);
        make.left.mas_equalTo(10);
        make.right.mas_equalTo(-10);
        
    }];
    
    
    /**
     *  email
     */
    [self createEmailCell:bgView andNum:num];
    
    viewPxl1 = [[PxlTextView alloc] initWithFrame:CGRectMake(0, 60 + num, screen_width, 35) andString:@"昵 称(*)"];
    [bgView addSubview:viewPxl1];
    
    viewPxl2 = [[PxlTextView alloc] initWithFrame:CGRectMake(0, 110 + num, screen_width, 35) andString:@"真实名字"];
    [bgView addSubview:viewPxl2];
    
    /**
     *  性别
     */
    [self createSexCell:bgView andNum:num];
    /**
     *  国家
     */
    [self createCountryCell:bgView andNum:num];
    
    
    viewPxl3 = [[PxlTextView alloc] initWithFrame:CGRectMake(0, 260 + num, screen_width, 35) andString:@"所在城市"];
    [bgView addSubview:viewPxl3];
    
    viewPxl4 = [[PxlTextView alloc] initWithFrame:CGRectMake(0, 310 + num, screen_width, 35) andString:@"手机号码"];
    viewPxl4.textfield.keyboardType = UIKeyboardTypePhonePad;
    [bgView addSubview:viewPxl4];
    
    viewPxl5 = [[PxlTextView alloc] initWithFrame:CGRectMake(0, 360 + num, screen_width, 35) andString:@"微信账号"];
    viewPxl5.textfield.keyboardType = UIKeyboardTypeASCIICapable;
    [bgView addSubview:viewPxl5];
    
    viewPxl6 = [[PxlTextView alloc] initWithFrame:CGRectMake(0, 410 + num, screen_width, 35) andString:@"新浪微博"];
    viewPxl6.textfield.keyboardType = UIKeyboardTypeASCIICapable;
    [bgView addSubview:viewPxl6];
    
    viewPxl7 = [[PxlTextView alloc] initWithFrame:CGRectMake(0, 460 + num, screen_width, 35) andString:@"QQ帐号"];
    viewPxl7.textfield.keyboardType = UIKeyboardTypeASCIICapable;
    [bgView addSubview:viewPxl7];
    
    UIButton* button = [PxlTextView createButton:@"确认修改"];
    [bgView addSubview:button];
    [button addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgView.mas_right).offset(-5);
        make.left.equalTo(@5);
        make.height.equalTo(@40);
        make.top.equalTo(viewPxl7.mas_bottom).offset(20);
    }];
    
    
    
    return bgView;
}

-(__kindof MBZPKTextField*)createCountryCell:(UIView*)superView andNum:(NSInteger)num{
    
    countryTextfield = [PxlTextView createMBZPKTextField];
    countryTextfield.enabled = NO;
    [superView addSubview:countryTextfield];
    countryTextfield.text = _userInfomodel.user_data.country;
    [countryTextfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(superView).offset(-70);
        make.left.equalTo(superView).offset(85);
        make.top.mas_equalTo(210+num);
        make.height.equalTo(@35);
    }];
    
    UILabel *label1 = [PxlTextView createLabel];
    [superView addSubview:label1];
    label1.text =@"国家";
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(countryTextfield.mas_left).offset(-5);
        make.centerY.equalTo(countryTextfield);
    }];
    
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"选择" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [superView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(countryTextfield);
        make.right.equalTo(superView.mas_right).offset(-5);
        make.left.equalTo(countryTextfield.mas_right).offset(5);
        make.bottom.equalTo(countryTextfield.mas_bottom);
    }];
    
    return countryTextfield;
    
}

-(void)createEmailCell:(UIView*)superView andNum:(NSInteger)num{
    
    UILabel *label1 = [PxlTextView createLabel];
    label1.text = @"邮箱";
    [superView addSubview:label1];
    
    
    UILabel *emailLabel= [PxlTextView createLabel];
    emailLabel.text = _userInfomodel.email;
    [superView addSubview:emailLabel];
    emailLabel.textAlignment = NSTextAlignmentLeft;
    
    [emailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView).offset(85);
        make.top.mas_equalTo(20 + num);
        make.right.equalTo(superView).offset(-10);
        make.height.equalTo(@30);
    }];
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(emailLabel.mas_left).offset(-5);
        make.centerY.equalTo(emailLabel);
        make.left.equalTo(superView);
    }];
}

-(void)createSexCell:(UIView*)superView andNum:(NSInteger)num{
    
    UILabel *label = [PxlTextView createLabel];
    label.text = @"性别";
    [superView addSubview:label];
    
    
    UIButton *button1 = [[UIButton alloc] init];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button1 setTitle:@"男" forState:UIControlStateNormal];
    [button1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button1.titleLabel.font = [UIFont systemFontOfSize: 14.0];
    [button1 setImage:[UIImage imageNamed:@"unselect"] forState:UIControlStateNormal];
    [button1 setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
    button1.tag = 10;
    [button1 addTarget:self action:@selector(sexSelectButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [superView addSubview:button1];
    
    
    UIButton *button2 = [[UIButton alloc] init];
    [button2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button2 setTitle:@"女" forState:UIControlStateNormal];
    [button2 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont systemFontOfSize: 14.0];
    [button2 addTarget:self action:@selector(sexSelectButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    button2.tag = 20;
    [button2 setImage:[UIImage imageNamed:@"unselect"] forState:UIControlStateNormal];
    [button2 setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
    [superView addSubview:button2];
    [button1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(superView).offset(85);
        make.top.mas_equalTo(160 + num);
        make.width.equalTo(@40);
        make.height.equalTo(@30);
    }];
    
    [button2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(button1.mas_right).offset(5);
        make.top.mas_equalTo(160 + num);
        make.width.equalTo(@40);
        make.height.equalTo(@30);
    }];
    
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(button1.mas_left).offset(-5);
        make.centerY.equalTo(button1);
        make.left.equalTo(superView);
    }];
    
    if ([_userInfomodel.user_data.sex isEqualToString:@"男"]) {
        button1.selected = YES;
        betweenButton = button1;
    }else{
        button2.selected = YES;
        betweenButton = button2;
        
    }
    
}

-(void) requestData
{
    if (!_userInfomodel.user_data.country_id) {
        _userInfomodel.user_data.country_id =@"";
    }
    if (!_userInfomodel.user_data.sex) {
        _userInfomodel.user_data.sex =@"";
    }
    if (!_userInfomodel.user_data.location) {
        _userInfomodel.user_data.location =@"";
    }
    if (!_userInfomodel.user_data.tel_cell) {
        _userInfomodel.user_data.tel_cell =@"";
    }
    if (!_userInfomodel.user_data.weixin) {
        _userInfomodel.user_data.weixin =@"";
    }
    if (!_userInfomodel.user_data.weibo) {
        _userInfomodel.user_data.sex =@"";
    }
    if (!_userInfomodel.user_data.qq) {
        _userInfomodel.user_data.qq =@"";
    }
    if (!_userInfomodel.user_data.full_name) {
        _userInfomodel.user_data.full_name =@"";
    }
    if (!_userInfomodel.user_data.nickname) {
        _userInfomodel.user_data.nickname =@"";
    }
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token,
                          @"user_data[nickname]":_userInfomodel.user_data.nickname,
                          @"user_data[full_name]":_userInfomodel.user_data.full_name,
                          @"user_data[sex]":_userInfomodel.user_data.sex,
                          @"user_data[country_id]":_userInfomodel.user_data.country_id,
                          @"user_data[location]":_userInfomodel.user_data.location,
                          @"user_data[tel_cell]":_userInfomodel.user_data.tel_cell,
                          @"user_data[weixin]":_userInfomodel.user_data.weixin,
                          @"user_data[weibo]":_userInfomodel.user_data.weibo,
                          @"user_data[qq]":_userInfomodel.user_data.qq};
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_Softhelp_user_editprofile WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"SQwoVCIsRequst" object:nil];
                showDefaultPromptTextHUD(@"信息修改成功");
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void)requstHeadImage:(UIImage*)image{
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    NSString *user_token = [SQUser loadmodel].user_token;
    
    NSDictionary *dic = @{@"user_token":user_token};
    [self showLoadingHUD];
    @weakify(self);
    [HTTPManager publishCarCirclWithUrl:API_WO_user_uploadavatar andPostParameters:dic andImageDic:@{@"pics":@[imageData]} andBlocks:^(NSDictionary *result) {
        @strongify(self);
        [self hideLoadingHUD];
        
        self.headImageView.image = image;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"SQwoVCIsRequst" object:nil];
    }];
    
}



#pragma mark - ---- delegate 🍎🌝

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex NS_DEPRECATED_IOS(2_0, 8_3) __TVOS_PROHIBITED{
    
}

- (void)addressSearchViewDelegate:(NSDictionary *)data{
    countryTextfield.text = data[@"name"];
    _userInfomodel.user_data.country_id = data[@"id"];
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}
#pragma mark - ---- getters and setters 🍋🌞
-(SQWoUserInfoModel *)userInfomodel{
    if (!_userInfomodel) {
        _userInfomodel = [SQWoUserInfoModel mj_objectWithKeyValues:self.dataDic];
    }
    return _userInfomodel;
}

@end
