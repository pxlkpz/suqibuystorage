//
//  SQWoUserInfo.h
//  suqibuyApp
//
//  Created by apple on 16/6/5.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"
#import "SQWoUserInfoModel.h"
@interface SQWoUserInfo : SQBaseViewController

@property (strong, nonatomic) SQWoUserInfoModel *userInfomodel;
@property (weak, nonatomic) NSDictionary *dataDic;

@end
