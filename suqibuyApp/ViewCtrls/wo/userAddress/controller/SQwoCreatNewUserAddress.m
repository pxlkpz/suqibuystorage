//
//  SQwoCreatNewUserAddress.m
//  suqibuyApp
//
//  Created by apple on 16/6/1.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQwoCreatNewUserAddress.h"
#import "MBZPKTextField.h"
#import "MBZPKTextView.h"
#import "QCheckBox.h"
#import "SQAddressSearchViewCtrl.h"
#import "SQwoCreateUserAddressModel.h"

@interface SQwoCreatNewUserAddress ()<QCheckBoxDelegate,SQAddressSearchViewDelegate>
{
    SQwoCreateUserAddressModel *_myModel;
    QCheckBox* _checkBox;
}
@property(nonatomic, strong) UIView *myView;
@property(nonatomic, strong) UIScrollView *myScrollView;

@property(nonatomic, strong) MBZPKTextField *textField_country;

@property(nonatomic, strong) MBZPKTextField *textField_express_no;

@property(nonatomic, strong) MBZPKTextField *textField_prefecture;

@property(nonatomic, strong) MBZPKTextField *textField_city;

@property(nonatomic, strong) MBZPKTextField *textField_name;

@property(nonatomic, strong) MBZPKTextField *textField_phoneNumber;

@property(nonatomic, strong) MBZPKTextView *textView_description;

@end

@implementation SQwoCreatNewUserAddress


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"创建地址";
    
    _myModel = [[SQwoCreateUserAddressModel alloc] init];
    
    [self buildUI];
    
    [self requstDetail:_otherModel.my_id];
}



#pragma mark - ---- event response 🍐🌛
-(void)buttonAction:(UIButton*)sender{
    SQAddressSearchViewCtrl *controller = [[SQAddressSearchViewCtrl alloc] init];
    controller.delegate = self;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)sureButtonAction:(UIButton*)sender{
    if ([self sureButtonOpinion]) {
        [self requst];
    }
}
#pragma mark - ---- private methods 🍊🌜



-(void)showDataInUI{
    _textField_country.text = _otherModel.country_name_label;
    _textField_express_no.text = _myModel.zipcode;
    _textField_prefecture.text = _myModel.region;
    _textField_city.text = _myModel.city;
    _textField_name.text = _myModel.name;
    _textField_phoneNumber.text = _myModel.telephone;
    _textView_description.text = _myModel.detail_address;
    [_checkBox setChecked:[_myModel.is_default boolValue]];
}

-(BOOL)sureButtonOpinion{
    
    if ([_textField_country.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请选择国家");
        return NO;
    }
    else if ([_textField_express_no.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入邮政编码");
        return NO;
    }
    
    else if ([_textView_description.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入详细地址");
        return NO;
    }
    
    if ([_textField_name.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入收件人");
        return NO;
    }
    else if ([_textField_phoneNumber.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入电话号码");
        return NO;
    }
    
    _myModel.zipcode = _textField_express_no.text;
    _myModel.region = _textField_prefecture.text;
    _myModel.city = _textField_city.text;
    _myModel.detail_address = _textView_description.text;
    _myModel.name = _textField_name.text;
    _myModel.telephone = _textField_phoneNumber.text;
    
    return YES;
}

-(void)buildUI{
    
    _myScrollView = [[UIScrollView alloc] init];
    [self.view addSubview:_myScrollView];
    [_myScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    _myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 530)];
    [_myScrollView addSubview:_myView];
    
    [_myView layoutIfNeeded];
    
    _myScrollView.contentSize = _myView.bounds.size;
    
    
    UILabel *label1 = [[UILabel alloc] init];
    //    label1.font = [UIFont fontWithName:text_heit_Blod size:15];
    label1.textColor = [UIColor colorWithHexString:simple_button_yellow];
    label1.text = @"友情提示：请填写英文地址(日本除外)";
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont boldSystemFontOfSize:14]];
    
    [label1 sizeToFit];
    [_myView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_myView).offset(20);
        make.left.equalTo(_myView).offset(10);
    }];
    
    const NSInteger num = 30;
    
    
    _textField_country = [self creatCellUI:@"国家(*)" andTopNumber:30+num];
    _textField_country.enabled = NO;
    
    _textField_express_no = [self creatCellInTwoUI:@"邮政编码(*)" andTopNumber:80+num];
    
    _textField_prefecture = [self creatCellInTwoUI:@"州" andTopNumber:130+num];
    
    _textField_city = [self creatCellInTwoUI:@"城市" andTopNumber:180+num];
    
    _textView_description = [self creatCellINTextViewUI:@"详细地址(*)" andTopNumber:230+num];
    
    _textField_name = [self creatCellInTwoUI:@"收货人(*)" andTopNumber:305+num];
    
    _textField_phoneNumber = [self creatCellInTwoUI:@"电话号码(*)" andTopNumber:355+num];
    _textField_phoneNumber.keyboardType = UIKeyboardTypePhonePad;
    
    _checkBox = [[QCheckBox alloc] initWithDelegate:self];
    [_checkBox setTitle:@"设为默认地址" forState:UIControlStateNormal];
    [_checkBox setTitleColor:[UIColor colorWithHexString:text_color_gray_black] forState:UIControlStateNormal];
    [_checkBox.titleLabel setFont:[UIFont boldSystemFontOfSize:15.0]];
    [_checkBox setChecked:NO];
    [_myView addSubview:_checkBox];
    [_checkBox mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@85);
        make.width.equalTo(@120);
        make.height.equalTo(@40);
        make.top.equalTo(_myView).offset(405 + num);
    }];
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"保  存" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    [button addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_myView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView.mas_right).offset(-5);
        make.left.equalTo(@5);
        make.height.equalTo(@40);
        make.top.equalTo(_myView).offset(460 + num);
    }];
    
}


-(__kindof MBZPKTextField*)creatCellUI:(NSString*)Str andTopNumber:(NSInteger)num{
    
    MBZPKTextField *textfield = [self createMBZPKTextField];
    
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView).offset(-70);
        make.left.equalTo(_myView).offset(85);
        make.top.mas_equalTo(num);
        make.height.equalTo(@35);
    }];
    
    UILabel *label1 = [self createLabel];
    label1.text = Str;
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-5);
        make.centerY.equalTo(textfield);
    }];
    
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"选择" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    //    button.tag = num;
    [_myScrollView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(textfield);
        make.right.equalTo(_myView.mas_right).offset(-5);
        make.left.equalTo(textfield.mas_right).offset(5);
        make.bottom.equalTo(textfield.mas_bottom);
    }];
    
    return textfield;
    
}


-(__kindof MBZPKTextField*)creatCellInTwoUI:(NSString*)Str andTopNumber:(NSInteger)num{
    
    MBZPKTextField *textfield = [self createMBZPKTextField];
    
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView).offset(-5);
        make.left.equalTo(_myView).offset(85);
        make.top.mas_equalTo(num);
        make.height.equalTo(@35);
    }];
    
    UILabel *label1 = [self createLabel];
    label1.text = Str;
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-5);
        make.centerY.equalTo(textfield);
    }];
    
    return textfield;
    
}

-(__kindof MBZPKTextView*)creatCellINTextViewUI:(NSString*)Str andTopNumber:(NSInteger)num{
    
    MBZPKTextView *textfield = [[MBZPKTextView alloc] init];
    [textfield setFont:[UIFont systemFontOfSize:14]];
    textfield.textColor = [UIColor blackColor];
    [textfield setTextAlignment:NSTextAlignmentLeft];
    [_myView addSubview:textfield];
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView).offset(-5);
        make.left.equalTo(_myView).offset(85);
        make.top.mas_equalTo(num);
        make.height.equalTo(@60);
    }];
    
    UILabel *label1 = [self createLabel];
    label1.text = Str;
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-5);
        make.top.equalTo(textfield).offset(5);
    }];
    return textfield;
    
}



-(__kindof MBZPKTextField*)createMBZPKTextField{
    MBZPKTextField *textfield1 = [[MBZPKTextField alloc] init];
    [textfield1 setFont:[UIFont systemFontOfSize:14]];
    textfield1.textColor = [UIColor blackColor];
    [textfield1 setTextAlignment:NSTextAlignmentLeft];
    [_myView addSubview:textfield1];
    
    return textfield1;
}

-(__kindof UILabel*)createLabel{
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [label1 sizeToFit];
    [_myView addSubview:label1];
    
    return label1;
}


-(void)requst{
    [self showLoadingHUD];
    _myModel.user_token = [SQUser loadmodel].user_token;
    
    NSDictionary *dic = _myModel.mj_keyValues;
    
    @weakify(self);
    
    
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_UserAddress_Save WithParameter:dic WithFinishBlock:^(id returnValue) {
        @strongify(self);
        
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                NSDictionary* result = dic[@"result"];
                showDefaultPromptTextHUD(result[@"msg"]);
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}



-(void)requstDetail:(NSString*)detailId{
    if (detailId == nil) {
        return;
    }
    [self showLoadingHUD];
    
    NSDictionary *dic = @{@"user_token":[SQUser loadmodel].user_token,@"id":detailId};
    
    @weakify(self);
    
    
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_useraddress_detail WithParameter:dic WithFinishBlock:^(id returnValue) {
        @strongify(self);
        
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                NSDictionary* result = dic[@"result"];
                _myModel = [SQwoCreateUserAddressModel mj_objectWithKeyValues:result];
                [self showDataInUI];

            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}

#pragma mark - ---- delegate 🍎🌝
- (void)didSelectedCheckBox:(QCheckBox *)checkbox checked:(BOOL)checked;
{
    _myModel.is_default = [NSString stringWithFormat:@"%d",checked];
    
    NSLog(@"%d",checked);
}

-(void)addressSearchViewDelegate:(NSDictionary *)data{
    _myModel.country_id = data[@"id"];
    _textField_country.text = data[@"name"];
}
#pragma mark - ---- getters and setters 🍋🌞



@end
