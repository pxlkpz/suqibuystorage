//
//  SQwoUserAddress.m
//  suqibuyApp
//
//  Created by apple on 16/6/1.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQwoUserAddress.h"
#import "UIAlertView+BlocksKit.h"
#import "SQzhuanyunQuoteVC.h"
#import "SQwoUserAddressCell.h"
#import "SQwoCreatNewUserAddress.h"
#import "SQwoUserAddressModel.h"

@interface SQwoUserAddress ()<UITableViewDelegate,UITableViewDataSource>
{
    UIButton *addItemButton;
    
    NSMutableArray *dataArray;
    
    UIView *_clearView;
    
    UIButton* _closeButton;
    
    
    
    
    
}
@property (nonatomic, strong) UITableView *listTableView;

@property (nonatomic, assign) NSInteger currentPage;


@end

@implementation SQwoUserAddress

#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"用户地址";
    
    dataArray = [[NSMutableArray alloc] init];
    
    [self buildclearView];
    
    [self buildTableView];
    
    [self buildRightItem];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
    
    self.currentPage = 1;
    [self requestData:1];
    
}

#pragma mark - ---- event response 🍐🌛
-(void)addItem{
    
}

-(void)closeButtonAction:(UIButton*)sender{
    SQzhuanyunQuoteVC *controller = [[SQzhuanyunQuoteVC alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)addNewButtonAction:(UIButton*)sender{
    SQwoCreatNewUserAddress *controller =[[SQwoCreatNewUserAddress alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma mark - ---- private methods 🍊🌜
-(__kindof UIButton*)setRightButton:(NSString*)buttonTitle action:(SEL)action
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:14.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 20);
    
    CGSize size = [buttonTitle sizeWithAttributes:
                   @{NSFontAttributeName:
                         [UIFont systemFontOfSize:16.0]}];
    [button setFrame:CGRectMake(0, 0, MAX((size.width+15),40), MAX(30,size.height))];
    [button setTitle:buttonTitle forState:UIControlStateNormal];
    if (action != nil) {
        [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    }
    [self setRightNavigationItemWithCustomView:button];
    return button;
}
- (void)setRightNavigationItemWithCustomView:(UIView*)cusView
{
    UIBarButtonItem *m_buttonItem = [[UIBarButtonItem alloc] initWithCustomView:cusView];
    self.navigationItem.rightBarButtonItem = m_buttonItem;
}



-(void)buildRightItem
{
    [self setRightButton:@"新增" action:@selector(addNewButtonAction:)];
}

-(void) requestData:(NSInteger ) page
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSString *pageString = [NSString stringWithFormat:@"%ld",(long)page];
    NSDictionary *dic = @{@"user_token":user_token,@"page":pageString};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_Useraddress_list WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if (page == 1)
                        {
                            [dataArray removeAllObjects];
                        }
                        else
                        {
                            if ([items count] == 0)
                            {
                                showDefaultPromptTextHUD(@"没有更多内容了");
                                [self.listTableView.mj_footer endRefreshingWithNoMoreData];
                                
                                return ;
                            }
                            else
                            {
                                self.currentPage++;
                            }
                        }
                        
                        NSArray* array = [SQwoUserAddressModel mj_objectArrayWithKeyValuesArray:items];
                        NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
                        
                        NSString *bgId = @"";
                        for (NSInteger i = 0; i < array.count; i++)
                        {
                            SQwoUserAddressModel *model = array[i];
                            if ([model.country_name_label isEqualToString:bgId] && (![bgId isEqualToString:@""])) {
                                [[mutableArray lastObject] addObject:model];
                            }else{
                                NSMutableArray *array = [NSMutableArray arrayWithObjects:model, nil];
                                [mutableArray addObject:array];
                            }
                            bgId = model.country_name_label;
                        }
                        [dataArray addObjectsFromArray:mutableArray];
                        
                        
                        [_listTableView reloadData];
                        //                        _listTableView.mj_footer.hidden = NO;
                        if (dataArray.count != 0) {
                            _listTableView.hidden = NO;
                        }else{
                            _listTableView.hidden = YES;
                        }
                        
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

-(void) requestSelectAddress:(NSString* )myid
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token,@"user_address_id":myid};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_ZHUANYUN_TransshipSelectaddress WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


-(void)buildclearView{
    
    UILabel* noItemLabel = [[UILabel alloc] init];
    noItemLabel.text = @"当前列表为空\n马上开始创建吧！";
    noItemLabel.textColor = [UIColor colorWithHexString:text_gray];
    noItemLabel.textAlignment = NSTextAlignmentCenter;
    noItemLabel.font = [UIFont systemFontOfSize:14];
    noItemLabel.numberOfLines = 2;
    [self.view addSubview:noItemLabel];
    [noItemLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
    }];
    
}

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.hidden = YES;
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    [_listTableView setTableFooterView:[[UIView alloc] init]];
    //[_listTableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    
    
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 60.0f;
    
    @weakify(self);
    
    
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        self.currentPage = 1;
        [self requestData:1];
    }];
    
    _listTableView.mj_footer  = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:self.currentPage+1];
    }];
    //    _listTableView.mj_footer.hidden = YES;
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
}

-(void)requstDelete:(NSString*)Id{
    [self showLoadingHUD];
    
    NSDictionary *dic = @{@"user_token":[SQUser loadmodel].user_token, @"id":Id};
    
    @weakify(self);
    
    
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_Useraddress_Delete WithParameter:dic WithFinishBlock:^(id returnValue) {
        @strongify(self);
        
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                NSDictionary* result = dic[@"result"];
                id items =[result objectForKey:@"items"];
                if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                {
                    [dataArray removeAllObjects];
                    
                    NSArray* array = [SQwoUserAddressModel mj_objectArrayWithKeyValuesArray:items];
                    NSMutableArray *mutableArray = [[NSMutableArray alloc]init];
                    
                    NSString *bgId = @"";
                    for (NSInteger i = 0; i < array.count; i++)
                    {
                        SQwoUserAddressModel *model = array[i];
                        if ([model.country_name_label isEqualToString:bgId] && (![bgId isEqualToString:@""])) {
                            [[mutableArray lastObject] addObject:model];
                        }else{
                            NSMutableArray *array = [NSMutableArray arrayWithObjects:model, nil];
                            [mutableArray addObject:array];
                        }
                        bgId = model.country_name_label;
                    }
                    [dataArray addObjectsFromArray:mutableArray];
                    
                    
                    [_listTableView reloadData];
                    if (dataArray.count != 0) {
                        _listTableView.hidden = NO;
                    }else{
                        _listTableView.hidden = YES;
                    }

                    
                    
                }
                
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}

#pragma mark - ---- delegate 🍎🌝
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [dataArray count];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *modelArr = dataArray[section];
    return modelArr.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SQwoUserAddressCell";
    
    SQwoUserAddressCell *cell = (SQwoUserAddressCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQwoUserAddressCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQwoUserAddressCell class]])
            {
                cell = (SQwoUserAddressCell*)nib;
                break;
            }
        }
    }
    
    NSArray *modelArr = dataArray[indexPath.section];
    [cell setData:modelArr[indexPath.row]];
    
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SQwoUserAddressModel *model = dataArray[section][0];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 45)];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UILabel *label= [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@"%@",model.country_name];
    label.font = [UIFont boldSystemFontOfSize:14];
    //    label.font = [UIFont fontWithName:text_heit_Blod size:14.0f];
    label.textColor = [UIColor blackColor];
    [view addSubview:label];
    [label sizeToFit];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(@15);
        make.height.equalTo(@30);
    }];
    UIView *view1 = [[UIView alloc]init];
    [view addSubview:view1];
    view1.alpha = 0.6;
    view1.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    return view;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *modelArr = dataArray[indexPath.section];
    SQwoUserAddressModel* model = modelArr[indexPath.row];
    
    if (_type == 10) {
//        @weakify(self);
//        [UIAlertView bk_showAlertViewWithTitle:nil message:@"请选择您要进行的操作" cancelButtonTitle:@"取消" otherButtonTitles:@[@"修改",@"选择"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
//            @strongify(self);
//            if (buttonIndex == 1) {
//                SQwoCreatNewUserAddress *controller = [[SQwoCreatNewUserAddress alloc] init];
//                controller.otherModel = model;
//                [self.navigationController pushViewController:controller animated:YES];
//            }else if(buttonIndex == 2){
//                //                [self requstDelete:model.my_id];
//                [self requestSelectAddress:model.my_id];
//            }
//        }];
        
        [self requestSelectAddress:model.my_id];

        
        return;
    }
    
//    @weakify(self);
//    [UIAlertView bk_showAlertViewWithTitle:nil message:@"请选择您要进行的操作" cancelButtonTitle:@"取消" otherButtonTitles:@[@"修改",@"删除"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
//        @strongify(self);
//        if (buttonIndex == 1) {
//            SQwoCreatNewUserAddress *controller = [[SQwoCreatNewUserAddress alloc] init];
//            controller.otherModel = model;
//            [self.navigationController pushViewController:controller animated:YES];
//        }else if(buttonIndex == 2){
//            [self requstDelete:model.my_id];
//        }
//    }];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // add a button to delete
    NSArray *modelArr = dataArray[indexPath.section];
    SQwoUserAddressModel* model = modelArr[indexPath.row];

//    if (_type == 10) {
//
//        // add a buutoon to reName DeviceName
//        UITableViewRowAction *reNameAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"修改" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
//                                              {
//                                                  SQwoCreatNewUserAddress *controller = [[SQwoCreatNewUserAddress alloc] init];
//                                                  controller.otherModel = model;
//                                                  [self.navigationController pushViewController:controller animated:YES];
//                                                  
//                                                  
//                                              }];
//        reNameAction.backgroundColor = [UIColor blueColor];
//
//        
//        return @[reNameAction];
//
//    }
    
    @weakify(self);
    UITableViewRowAction *deleteRowAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"删除" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                             {
                                                 @strongify(self);
                                                 [self requstDelete:model.my_id];
                                             }];
    
    // add a buutoon to reName DeviceName
    UITableViewRowAction *reNameAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"修改" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                          {
                                              SQwoCreatNewUserAddress *controller = [[SQwoCreatNewUserAddress alloc] init];
                                              controller.otherModel = model;
                                              [self.navigationController pushViewController:controller animated:YES];

                                              
                                          }];  
    reNameAction.backgroundColor = [UIColor grayColor];
    // return the array for showButtons ...  
    return @[deleteRowAction,reNameAction];  
    
}


#pragma mark - ---- getters and setters 🍋🌞



@end
