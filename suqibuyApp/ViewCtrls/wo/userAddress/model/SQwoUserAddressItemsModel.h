//
//  SQwoUserAddressItemsModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/1.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQwoUserAddressItemsModel : NSObject

/*
 "items": [
 {
 "id": "3",
 "": "1",
 "": "America 美国",
 "": "America 美国",
 "": "1231",
 "": true,
 "": "3123",
 "": "12313",
 "": "",
 "": "313",
 "": "1321",
 "": " 313 1321 1231"
 },
 */

@property (nonatomic, copy) NSString* my_id;
@property (nonatomic, copy) NSString* country_id;
@property (nonatomic, copy) NSString* country_name;
@property (nonatomic, copy) NSString* country_name_label;
@property (nonatomic, copy) NSString* zipcode;
@property (nonatomic, copy) NSString* is_default;
@property (nonatomic, copy) NSString* receiver;
@property (nonatomic, copy) NSString* telephone;
@property (nonatomic, copy) NSString* region;
@property (nonatomic, copy) NSString* city;
@property (nonatomic, copy) NSString* detail_address;
@property (nonatomic, copy) NSString* full_detail_address;


@end
