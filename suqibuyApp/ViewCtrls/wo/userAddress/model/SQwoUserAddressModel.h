//
//  SQwoUserAddressModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/1.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQwoUserAddressModel : NSObject


/*
 {
 "id": "11",
 "country_id": "2",
 "country_name": "Afghanistan 阿富汗",
 "country_name_label": "Afghanistan 阿富汗",
 "is_default": false,
 "receiver": "aaa",
 "telephone": "aaa",
 "region": "a",
 "city": "aa",
 "detail_address": "aaa",
 "zipcode": "23123\t1",
 "full_detail_address": "a aa aaa 23123\t1"
 },
 */

@property (nonatomic, copy) NSString* my_id;
@property (nonatomic, copy) NSString* country_id;

@property (nonatomic, copy) NSString* country_name;

@property (nonatomic, copy) NSString* country_name_label;

@property (nonatomic, copy) NSString* is_default;
@property (nonatomic, copy) NSString* receiver;
@property (nonatomic, copy) NSString* telephone;
@property (nonatomic, copy) NSString* region;
@property (nonatomic, copy) NSString* city;
@property (nonatomic, copy) NSString* detail_address;
@property (nonatomic, copy) NSString* zipcode;
@property (nonatomic, copy) NSString* full_detail_address;

@end
