//
//  SQwoUserAddress.m
//  suqibuyApp
//
//  Created by apple on 16/6/1.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQwoUserAddressCell.h"
#import "SQwoUserAddressModel.h"

@implementation SQwoUserAddressCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width - 76; // 44 = avatar宽度，4 * 3为padding
    
    self.bottomLabel.preferredMaxLayoutWidth = preferredMaxWidth; // 多行时必须设置
    
    [self.bottomLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(void)setData:(SQwoUserAddressModel*)model{
    
    self.topLabel.text = [NSString stringWithFormat:@"%@ %@",model.receiver, model.telephone];
    self.bottomLabel.text = [NSString stringWithFormat:@"%@",model.full_detail_address];
    
    self.headImageView.hidden = ![model.is_default boolValue];
    //    self.topLabel.text = model.item_name;
    //    self.centerLaebl.text = model.item_description;
    //    self.bottomLabel.text = [NSString stringWithFormat:@"%@g",model.weight];
}

@end
