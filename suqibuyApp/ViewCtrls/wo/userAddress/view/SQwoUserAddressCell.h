//
//  SQwoUserAddress.h
//  suqibuyApp
//
//  Created by apple on 16/6/1.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SQwoUserAddressModel;
@interface SQwoUserAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;

@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;

-(void)setData:(SQwoUserAddressModel*)model;
@end
