//
//  SearchResultViewController.m
//  searchController
//
//  Created by 王涛 on 16/3/7.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "SearchResultViewController.h"

@interface SearchResultViewController ()


@end

@implementation SearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.navigationController setNavigationBarHidden:YES];
}


#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.resultsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"RESULT_CELL"];
    if (cell == nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"RESULT_CELL"];
    }
    cell.textLabel.text = self.resultsArray[indexPath.row][@"name"];
    cell.textLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1;
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SQAddressSearch" object:self.resultsArray[indexPath.row]];

    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
