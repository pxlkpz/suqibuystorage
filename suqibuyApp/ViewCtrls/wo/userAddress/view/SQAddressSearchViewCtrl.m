//
//  SearchViewCtrl.m
//  CarService
//
//  Created by Apple on 12/1/14.
//  Copyright (c) 2014 fenglun. All rights reserved.
//

#import "SQAddressSearchViewCtrl.h"
#import "ChineseInclude.h"
#import "PinYinForObjc.h"

#import "SearchResultViewController.h"


@interface SQAddressSearchViewCtrl ()<UITableViewDataSource,UITableViewDelegate,UISearchResultsUpdating,UISearchBarDelegate>
@property (strong, nonatomic) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *resultsArray;
@property (nonatomic, strong) NSMutableArray *rightShwoArray;
@property (nonatomic, strong) NSMutableArray *itmesArray;

@property (nonatomic, strong) NSMutableArray *tempsArray;


@property (nonatomic, strong) UISearchController *searchController;

//@property (nonatomic, strong) NSMutableArray *resultsArray;



@end

@implementation SQAddressSearchViewCtrl

#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _resultsArray = [NSMutableArray array];
    _rightShwoArray = [NSMutableArray array];
    _itmesArray = [NSMutableArray array];
    self.title = @"请选择国家";
    
    self.tableView = [UITableView new];
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    
    [self initSearchController];
    
    [self requst];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationAction:) name:@"SQAddressSearch" object:nil];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = YES;
//    navi1.navigationBar.translucent = NO;
    
}
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.navigationController.navigationBar.translucent = NO;
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - ---- event response 🍐🌛

-(void)notificationAction:(NSNotification*)noti{
    [self.searchController setActive:NO];
    NSDictionary*dic = [noti object];
    if ([self.delegate respondsToSelector:@selector(addressSearchViewDelegate:)]) {
        [self.delegate addressSearchViewDelegate:dic];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - ---- private methods 🍊🌜
- (void)initSearchController{
    

    self.searchController = [[UISearchController alloc]initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;

    
    if (@available(iOS 11.0, *)) {
        self.navigationItem.hidesSearchBarWhenScrolling = NO;
        self.navigationItem.searchController = self.searchController;
    } else {

        self.tableView.tableHeaderView = self.searchController.searchBar;
    }
    
    self.searchController.searchBar.delegate = self;
}

-(void)dataChangeWithShow:(NSArray*)itemsArray{
    
    
    
    [_itmesArray addObjectsFromArray:itemsArray];
    NSString *ifStr = @"";
    for (NSDictionary*dic in itemsArray) {
        NSString *str2 = [dic[@"name"] substringToIndex:1];
        if (![[str2 uppercaseString] isEqualToString:ifStr]) {
            [_rightShwoArray addObject:str2];
            ifStr = str2;
            NSMutableArray*array = [NSMutableArray array];
            [array addObject:dic];
            [_resultsArray addObject:array];
        }else{
            [[_resultsArray lastObject] addObject:dic];
        }
    }
    
    [_tableView reloadData];
}

-(void)changeAfterWithShow:(NSArray*)itemsArray{
    if (itemsArray.count == 0) {
        return;
    }
    
    [_resultsArray removeAllObjects];
    [_rightShwoArray removeAllObjects];
    NSString *ifStr = @"";
    for (NSDictionary*dic in itemsArray) {
        NSString *str2 = [dic[@"name"] substringToIndex:1];
        if (![[str2 uppercaseString] isEqualToString:ifStr]) {
            [_rightShwoArray addObject:str2];
            ifStr = str2;
            NSMutableArray*array = [NSMutableArray array];
            [array addObject:dic];
            [_resultsArray addObject:array];
        }else{
            [[_resultsArray lastObject] addObject:dic];
        }
    }
    
    [_tableView reloadData];
}



-(void)requst{
    [self showLoadingHUD];
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token};
    
    @weakify(self);
    
    
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_Softhelp_GetCountries WithParameter:dic WithFinishBlock:^(id returnValue) {
        @strongify(self);
        
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                
                [self dataChangeWithShow:dic[@"result"][@"items"] ];
                
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}

#pragma mark - ---- delegate 🍎🌝

#pragma mark - Table view data source

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    tableView.sectionIndexColor = KColorRGB(60, 60, 60);

    tableView.sectionIndexBackgroundColor = [UIColor clearColor];

    
    return self.rightShwoArray;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if ( self.rightShwoArray.count>0) {
        NSString *key = [self.rightShwoArray objectAtIndex:section];
        return key;
    }
    return nil;
}

#pragma mark - Section header view
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.rightShwoArray.count>0) {
        UIView  *lab = [[UIView alloc] initWithFrame:CGRectZero];
        lab.backgroundColor = [UIColor colorWithHexString:bg_gray];
        
        UILabel *numLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 52, 20)];
        numLab.textAlignment = NSTextAlignmentCenter;

        [numLab setFont:[UIFont boldSystemFontOfSize:13]];
        numLab.text = [self.rightShwoArray objectAtIndex:(section)];

        numLab.textColor = [UIColor colorWithHexString:text_color_gray_black];
        [lab addSubview:numLab];
        return lab;
        
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

#pragma UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.rightShwoArray.count > 0) {
        return self.rightShwoArray.count;
    }
    return 0;
}// Default is 1 if not implemented

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.resultsArray.count > 0) {
        NSArray *array =self.resultsArray[section];
        return array.count;
    }
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSArray *array =self.resultsArray[indexPath.section];
    NSDictionary *dic = array[indexPath.row];
    cell.textLabel.text = dic[@"name"];
    cell.textLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *array =self.resultsArray[indexPath.section];
    NSDictionary *dic = array[indexPath.row];
    if ([self.delegate respondsToSelector:@selector(addressSearchViewDelegate:)]) {
        [self.delegate addressSearchViewDelegate:dic];
    }
//    self.searchController.isActive = NO;
    [self.searchController setActive:NO];
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma UISearchDisplayDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    cell.frame = CGRectMake(-320, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
//    [UIView animateWithDuration:0.7 animations:^{
//        cell.frame = CGRectMake(0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
//    } completion:^(BOOL finished) {
//        ;
//    }];
}

#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
//    UINavigationController *navController = (UINavigationController *)self.searchController.searchResultsController;
//    SearchResultViewController *resultVC = (SearchResultViewController *)navController.topViewController;
    [self filterContentForSearchText:self.searchController.searchBar.text];
    [self.tableView reloadData];
    
    [self changeAfterWithShow:self.tempsArray];
//    resultVC.resultsArray = self.tempsArray;
//    [resultVC.tableView reloadData];
}
//_resultsArray
#pragma mark - Private Method
- (void)filterContentForSearchText:(NSString *)searchText{
    NSLog(@"%@",searchText);
    NSUInteger searchOptions = NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch;
    [self.tempsArray removeAllObjects];
    for (int i = 0; i < self.itmesArray.count; i++) {
        NSString *title = self.itmesArray[i][@"name"];
        NSRange storeRange = NSMakeRange(0, title.length);
        NSRange foundRange = [title rangeOfString:searchText options:searchOptions range:storeRange];
        if (foundRange.length) {
            [self.tempsArray addObject:self.itmesArray[i]];
        }
    }
}


#pragma mark - ---- getters and setters 🍋🌞

- (NSMutableArray *)tempsArray{
    if (!_tempsArray) {
        _tempsArray = [NSMutableArray array];
    }
    return _tempsArray;
}






@end
