//
//  SearchViewCtrl.h
//  CarService
//
//  Created by Apple on 12/1/14.
//  Copyright (c) 2014 fenglun. All rights reserved.
//

#import "SQBaseViewController.h"
@protocol SQAddressSearchViewDelegate;

@interface SQAddressSearchViewCtrl : SQBaseViewController <UISearchBarDelegate>

@property (strong, nonatomic) NSArray *dataArray;

@property(nonatomic, assign)id<SQAddressSearchViewDelegate> delegate;

//@property (assign, nonatomic) TypeWithSearch tyepInter;
@end

@protocol SQAddressSearchViewDelegate <NSObject>

@required

- (void)addressSearchViewDelegate:(NSDictionary *)data;

@end
