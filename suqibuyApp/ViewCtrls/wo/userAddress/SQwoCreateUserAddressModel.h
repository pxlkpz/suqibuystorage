//
//  SQwoCreateUserAddressModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/1.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQwoCreateUserAddressModel : NSObject
/*
 参数1：user_token
 参数2：api_token
 参数3：id =》 地址ID, 当新增地址时，该参数为空
 参数：country_id =》　国家ＩＤ，国家数据请见全局接口
 参数：zipcode　＝》 邮政编码
 参数：region =》　州
 参数：city　＝》 城市
 参数：detail_address =》　详细地址
 参数：name　＝》 收货人姓名
 参数：telephone =》　电话号码
 参数：is_default　＝》 设为默认
 */

@property (nonatomic, copy) NSString* user_token;

/** 地址ID, 当新增地址时，该参数为空 */
@property (nonatomic, copy) NSString* my_id;
/** 国家ＩＤ，国家数据请见全局接口 */
@property (nonatomic, copy) NSString* country_id;
/** 邮政编码 */
@property (nonatomic, copy) NSString* zipcode;
/** 州 */
@property (nonatomic, copy) NSString* region;
/** 城市 */
@property (nonatomic, copy) NSString* city;
/** 详细地址 */
@property (nonatomic, copy) NSString* detail_address;
/** 收货人姓名 */
@property (nonatomic, copy) NSString* name;
/** 电话号码 */
@property (nonatomic, copy) NSString* telephone;
/** 设为默认 */
@property (nonatomic, copy) NSString* is_default;

@end
