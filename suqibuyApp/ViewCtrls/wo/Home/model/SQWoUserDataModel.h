//
//  SQWoUserDataModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/2.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQWoUserDataModel : NSObject

@property (nonatomic, copy) NSString* info_score;
@property (nonatomic, copy) NSString* nickname;
@property (nonatomic, copy) NSString* full_name;
@property (nonatomic, copy) NSString* sex;
@property (nonatomic, copy) NSString* country_id;
@property (nonatomic, copy) NSString* country;
@property (nonatomic, copy) NSString* location;
@property (nonatomic, copy) NSString* online_pref;
@property (nonatomic, copy) NSString* tel_cell;
@property (nonatomic, copy) NSString* weixin;
@property (nonatomic, copy) NSString* weibo;
@property (nonatomic, copy) NSString* qq;
@end
