//
//  SQWoUserInfoModel.h
//  suqibuyApp
//
//  Created by apple on 16/6/2.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SQWoUserDataModel.h"
@interface SQWoUserInfoModel : NSObject

@property (nonatomic, copy) NSString* user_id;
@property (nonatomic, copy) NSString* user_token;
@property (nonatomic, copy) NSString* displayname;
@property (nonatomic, copy) NSString* email;
@property (nonatomic, copy) NSString* mobile;
@property (nonatomic, copy) NSString* remain_cash;
@property (nonatomic, copy) NSString* freeze_amount;
@property (nonatomic, copy) NSString* group_name;
@property (nonatomic, copy) NSString* group_id;
@property (nonatomic, copy) NSString* credits_amount;
@property (nonatomic, copy) NSString* growth_score;
@property (nonatomic, copy) NSString* my_new_msg_count;
@property (nonatomic, copy) NSString* avatar_url;
@property (nonatomic, strong) SQWoUserDataModel* user_data;


@end
