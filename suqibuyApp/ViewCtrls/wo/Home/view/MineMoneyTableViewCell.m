//
//  MineMoneyTableViewCell.m
//  CarService
//
//  Created by apple on 15/10/28.
//  Copyright © 2015年 fenglun. All rights reserved.
//

#import "MineMoneyTableViewCell.h"
#import <IQKeyboardManager/IQUIView+Hierarchy.h>
#import "SQwoUserAddress.h"
#define itemTag  6666
#define ALERT_CHONGZHI  998

@interface MineMoneyTableViewCellItem ()<UIAlertViewDelegate>

@property (nonatomic , strong) UIImageView *centerImageView;
@property (nonatomic , strong) UILabel     *infoLabel;

@property (nonatomic , strong) UILabel     *notiNumberLabel;

@end


@implementation MineMoneyTableViewCellItem

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _centerImageView = [[UIImageView alloc]init];
        [self addSubview:_centerImageView];
        
        [_centerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.top.equalTo(@10);
            make.size.mas_equalTo(CGSizeMake(43, 43));
        }];
        
        
        _notiNumberLabel = [[UILabel alloc]init];
        _notiNumberLabel.font = [UIFont systemFontOfSize:12];
        _notiNumberLabel.layer.cornerRadius = 12;
        _notiNumberLabel.layer.masksToBounds = YES;
        _notiNumberLabel.textAlignment = NSTextAlignmentCenter;
        _notiNumberLabel.backgroundColor = [UIColor redColor];
        _notiNumberLabel.text = @"99";
        _notiNumberLabel.adjustsFontSizeToFitWidth = YES;
        _notiNumberLabel.textColor = [UIColor whiteColor];
        _notiNumberLabel.hidden = YES;
        [self addSubview:_notiNumberLabel];
        [_notiNumberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            //            make.centerX.equalTo(self).offset(17);
            make.top.equalTo(@5);
            make.right.mas_equalTo(-5);
            make.size.mas_equalTo(CGSizeMake(24, 24));
            
        }];
        
        _infoLabel = [[UILabel alloc]init];
        _infoLabel.font = [UIFont systemFontOfSize:15];
        _infoLabel.textColor = [UIColor colorWithHexString:text_color_gray_black];
        [_infoLabel sizeToFit];
        [self addSubview:_infoLabel];
        
        
        [_infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.top.equalTo(_centerImageView.mas_bottom).offset(5);
            
        }];
        
        _rightLine = [[UIView alloc]init];
        _rightLine.backgroundColor = [UIColor colorWithHexString:text_white];
        [self addSubview:_rightLine];
        
        [_rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(0);
            make.bottom.mas_equalTo(0);
            make.width.mas_equalTo(0.5);
            make.right.mas_equalTo(0);
            
        }];
        
    }
    return self;
}


-(void)setImageNameStr:(NSString *)imageNameStr
{
    _centerImageView.image = [UIImage imageNamed:imageNameStr];
}

-(void)setInfoLabelStr:(NSString *)infoLabelStr
{
    _infoLabel.text = infoLabelStr;
}

@end

@interface MineMoneyTableViewCell ()<UIGestureRecognizerDelegate>

@property (nonatomic , strong) NSMutableArray *itmeArr;

@property (nonatomic, strong) NSArray* dataArr;

@property (nonatomic, strong)MineMoneyTableViewCellItem *first_item;

@end

@implementation MineMoneyTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _dataArr = @[@{@"imageName":@"user_center_msg", @"title":@"我的通知", @"VC":@"SQWoMyNotiVC"},
                     @{@"imageName":@"user_center_cashhistory", @"title":@"余额历史", @"VC":@"SQWoCashHistoryVC"},
                     @{@"imageName":@"user_center_exchange", @"title":@"充值", @"VC":@"SQWoChongzhiVC"},
                     @{@"imageName":@"user_center_useraddress", @"title":@"收货地址", @"VC":@"SQwoUserAddress"},
                     @{@"imageName":@"user_center_password", @"title":@"修改密码", @"VC":@"SQWoChangePasswordVC"},
                     @{@"imageName":@"user_center_daichong", @"title":@"代充业务", @"VC":@"SQWoDaichongVC"},
                     ];
        _itmeArr = [NSMutableArray array];
        for (int i = 0; i< _dataArr.count; i++) {
            MineMoneyTableViewCellItem *item = [[MineMoneyTableViewCellItem alloc]init];
            item.imageNameStr = _dataArr[i][@"imageName"];
            
            //            item.notiNumberLabel.hidden = YES;
            
            if (i == 0) {
                _first_item = item;
                NSString *count = [SQUser loadmodel].msg_count;
                if (count.length != 0 && [count integerValue] > 0)
                {
                    if ([count  integerValue]> 99) {
                        count = @"99+";
                    }
                    item.notiNumberLabel.text = count;
                    item.notiNumberLabel.hidden = NO;
                }else{
                    item.notiNumberLabel.hidden = YES;
                }
            }
            
            [self.contentView addSubview:item];
            [_itmeArr addObject:item];
            item.tag = i + itemTag;
            item.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(congzhiTap:)];
            
            [item addGestureRecognizer:tap];
            item.infoLabelStr = _dataArr[i][@"title"];
            if (i==2 || i == 5) {
                item.rightLine.hidden = YES;
            }
        }
        
        
        for (int i = 0 ; i < _itmeArr.count ;i ++) {
            
            [_itmeArr[i] mas_makeConstraints:^(MASConstraintMaker *make) {
                if (i==0 || i == 3) {
                    make.left.equalTo(@0);
                }else {
                    make.left.equalTo([_itmeArr[i-1] mas_right]);
                }
                
                if (i>2) {
                    make.top.equalTo(@85);
                }else{
                    make.top.equalTo(@0);
                }
                make.width.mas_equalTo(KScreenWidth/3);
                make.height.equalTo(self.contentView.mas_height).multipliedBy(0.5);
            }];
        }
        
        UIView* centerLine = [[UIView alloc]init];
        centerLine.backgroundColor = [UIColor colorWithHexString:text_white];;
        [self.contentView addSubview:centerLine];
        
        [centerLine mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@85);
            make.height.mas_equalTo(0.5);
            make.right.mas_equalTo(0);
            make.left.mas_equalTo(0);
        }];
        
        
    }
    return self;
}


-(void)congzhiTap:(UITapGestureRecognizer*)tap
{
    
    if (tap.view.tag == itemTag) {
        SQUserModel *model = [SQUser loadmodel];
        model.msg_count = @"";
        [SQUser save:model];
        
        MineMoneyTableViewCellItem* item =  (MineMoneyTableViewCellItem*)tap.view;
        item.notiNumberLabel.hidden = YES;
    }
    
    NSDictionary *dic = [_dataArr objectAtIndex:tap.view.tag - itemTag];
    NSString *VCStr = [dic objectForKey:@"VC"];
    UIViewController *controller = [[NSClassFromString(VCStr) alloc] init];
    controller.hidesBottomBarWhenPushed = YES;
    [controller.navigationController setNavigationBarHidden:NO];
    [self.viewController.navigationController pushViewController:controller animated:YES];
    
}


-(void)configMoneyCellWithDic:(NSDictionary *)dic
{
    if (_first_item) {
        NSString *count = [SQUser loadmodel].msg_count;
        if (count.length != 0 && [count integerValue] > 0)
        {
            if ([count  integerValue]> 99) {
                count = @"99+";
            }
            _first_item.notiNumberLabel.text = count;
            _first_item.notiNumberLabel.hidden = NO;
        }else{
            _first_item.notiNumberLabel.hidden = YES;
        }
        
    }
}


#pragma mark ===各种充值 ===

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
