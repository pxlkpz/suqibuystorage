

//
//  SQwoHeadView.m
//  suqibuyApp
//
//  Created by apple on 16/6/2.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQwoHeadView.h"
#import "SQWoUserInfoModel.h"
#import "SQWoUserInfo.h"
#import <IQKeyboardManager/IQUIView+Hierarchy.h>

@implementation SQwoHeadView

- (id)initWithNibName:(NSString *)nibNameOrNil {
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil];
    self = [nibs objectAtIndex:0];
    
    self.headImageView.layer.cornerRadius = 50;
    self.headImageView.layer.masksToBounds = YES;
    self.headImageView.userInteractionEnabled = YES;
    self.headImageView.layer.borderWidth = 2;
    self.headImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    //    self.headImageView.layer.
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTapRateView)];
    [self.headImageView addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTapRateView)];
    [self.headImageIcoView addGestureRecognizer:tapGesture1];
    
    
    UIView* centerLine = [[UIView alloc]init];
    centerLine.backgroundColor = [UIColor colorWithHexString:text_white];
    [self addSubview:centerLine];
    
    [centerLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(@0);
        make.height.mas_equalTo(0.5);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(0);
    }];
    
    
    return self;
}
-(void)setdataShow:(SQWoUserInfoModel*)data{
    [self.headImageView sd_setImageWithURL:[NSURL URLWithString:data.avatar_url]];
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    
    self.topLabel.text = [NSString stringWithFormat:@"版本号: V%@",app_Version];
    self.centerLabel.text =data.displayname;
    self.bottomLabel.text = [NSString stringWithFormat:@"邮箱地址 : %@",data.email];
    self.bottomLabel2.text = [NSString stringWithFormat:@"等级 : %@ 积分 : %@ 经验值 : %@",data.group_name, data.credits_amount,data.growth_score];
    self.bottomLabel3.text = [NSString stringWithFormat:@"可用余额 : %@ 冻结余额 : %@",[data.remain_cash gtm_stringByUnescapingFromHTML], [data.freeze_amount gtm_stringByUnescapingFromHTML]];
    
}
-(void)userTapRateView{
    
    if (!_isLeave) {
        _isLeave = YES;
        
        SQWoUserInfo *CV = [[SQWoUserInfo alloc] init];
        CV.userInfomodel = self.model;
        CV.hidesBottomBarWhenPushed = YES;
        [self.viewController.navigationController pushViewController:CV animated:YES];
        _isLeave = NO;
    }
    
}


@end
