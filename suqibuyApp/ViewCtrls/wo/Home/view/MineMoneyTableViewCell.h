//
//  MineMoneyTableViewCell.h
//  CarService
//
//  Created by apple on 15/10/28.
//  Copyright © 2015年 fenglun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface  MineMoneyTableViewCellItem : UIView

@property (nonatomic ,copy) NSString *imageNameStr;
@property (nonatomic ,copy) NSString *infoLabelStr;
@property (nonatomic , strong) UIView *rightLine;

@end

@interface MineMoneyTableViewCell : UITableViewCell

-(void)configMoneyCellWithDic:(NSDictionary *)dic;


@end
