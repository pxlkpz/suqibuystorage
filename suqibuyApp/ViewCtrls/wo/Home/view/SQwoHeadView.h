//
//  SQwoHeadView.h
//  suqibuyApp
//
//  Created by apple on 16/6/2.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQWoUserInfoModel.h"
@interface SQwoHeadView : UIView
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UILabel *centerLabel;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UIImageView *headImageIcoView;

@property (weak, nonatomic) IBOutlet UILabel *bottomLabel2;
@property (weak, nonatomic) IBOutlet UILabel *bottomLabel3;
@property (weak, nonatomic) SQWoUserInfoModel *model;

@property (assign, nonatomic) BOOL isLeave;

-(void)setdataShow:(id)data;
- (id)initWithNibName:(NSString *)nibNameOrNil;

@end
