//
//  SQWoChangePasswordVC.m
//  suqibuyApp
//
//  Created by apple on 16/6/2.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQWoChangePasswordVC.h"
#import "MBZPKTextField.h"
@interface SQWoChangePasswordVC ()

@property(nonatomic, strong) MBZPKTextField *textField_originally_password;

@property(nonatomic, strong) MBZPKTextField *textField_new_password;

@property(nonatomic, strong) MBZPKTextField *textField_newtwo_password;

@end

@implementation SQWoChangePasswordVC


#pragma mark - ---- lefe cycle 🍏🌚

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"修改密码";
    
    [self buildUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - ---- event response 🍐🌛
-(void)sureButtonAction:(UIButton*)sender{
    //#warning 还要加入限制 密码长度必须大于6和小于18
    
    if ([_textField_originally_password.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入原密码");
        return;
    }
    
    if ([_textField_new_password.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入密码");
        return;
    }
    if ([_textField_newtwo_password.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入确认密码");
        return;
    }
    //    参数1：user_token
    //    参数2：old_password
    //    参数3：password
    //    参数4：password_again
    //    参数5：api_token
    if (![_textField_new_password.text isEqualToString:_textField_newtwo_password.text]) {
        showDefaultPromptTextHUD(@"两次输入的密码不一致");
        return;
    }else{
        if (_textField_new_password.text.length < 5 || _textField_new_password.text.length>19){
            showDefaultPromptTextHUD(@"密码长度必须大于4和小于20");
            return;
        }
        
    }
    
    NSDictionary *dic =@{@"user_token":[SQUser loadmodel].user_token, @"old_password":_textField_originally_password.text, @"password":_textField_new_password.text, @"password_again":_textField_newtwo_password.text};
    [self requstChangePassword:dic];
    
}
#pragma mark - ---- private methods 🍊🌜
-(void)buildUI{
    
    const NSInteger num = 50;
    
    _textField_originally_password = [self creatCellInTwoUI:@"原密码(*)" andTopNumber:0+num];
    _textField_originally_password.placeholder = @"请输入原密码";
    _textField_originally_password.keyboardType = UIKeyboardTypeASCIICapable;
    
    
    _textField_new_password = [self creatCellInTwoUI:@"新密码(*)" andTopNumber:60+num];
    _textField_new_password.placeholder = @"请输入密码";
    _textField_new_password.keyboardType = UIKeyboardTypeASCIICapable;
    
    
    _textField_newtwo_password = [self creatCellInTwoUI:@"确认密码(*)" andTopNumber:120+num];
    _textField_newtwo_password.placeholder = @"请重复输入密码";
    _textField_newtwo_password.keyboardType = UIKeyboardTypeASCIICapable;
    
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"确认修改" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor colorWithHexString:simple_button_yellow]];
    button.layer.cornerRadius = allCornerRadius;
    [button addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view.mas_right).offset(-5);
        make.left.equalTo(@5);
        make.height.equalTo(@40);
        make.top.equalTo(self.view).offset(190+num);
    }];
    
}

-(void)requstChangePassword:(NSDictionary*)dic{
    [self showLoadingHUD];
    
    
    @weakify(self);
    
    
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_WO_user_resetpass WithParameter:dic WithFinishBlock:^(id returnValue) {
        @strongify(self);
        
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                showDefaultPromptTextHUD(dic[@"result"][@"msg"]);
                
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}


-(__kindof MBZPKTextField*)creatCellInTwoUI:(NSString*)Str andTopNumber:(NSInteger)num{
    
    MBZPKTextField *textfield = [self createMBZPKTextField];
    
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-5);
        make.left.equalTo(self.view).offset(85);
        make.top.mas_equalTo(num);
        make.height.equalTo(@35);
    }];
    
    UILabel *label1 = [self createLabel];
    label1.text = Str;
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-5);
        make.centerY.equalTo(textfield);
    }];
    
    return textfield;
    
}
-(__kindof MBZPKTextField*)createMBZPKTextField{
    MBZPKTextField *textfield1 = [[MBZPKTextField alloc] init];
    [textfield1 setFont:[UIFont systemFontOfSize:14]];
    textfield1.textColor = [UIColor blackColor];
    [textfield1 setTextAlignment:NSTextAlignmentLeft];
    textfield1.secureTextEntry = YES;
    [self.view addSubview:textfield1];
    
    return textfield1;
}

-(__kindof UILabel*)createLabel{
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [label1 sizeToFit];
    [self.view addSubview:label1];
    
    return label1;
}

#pragma mark - ---- delegate 🍎🌝

#pragma mark - ---- getters and setters 🍋🌞


@end
