//
//  SQdaishouForecastVC.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQdaishouForecastVC.h"
#import "MBZPKTextField.h"
#import "MBZPKTextView.h"
#import "TimeAndDataPicker.h"
#import "SearchViewCtrl.h"
#import "SQdaishouForecastModel.h"
#import "UIAlertView+BlocksKit.h"
#import "SQdaishouListVC.h"
#import "CameraTakeManager.h"
#import "HTTPManager.h"
#import "SQSuqibuyStorageAddriessView.h"
#import "suqibuyApp-Swift.h"
@interface SQdaishouForecastVC ()
{
    TimeAndDataPicker* _dataPicker;
    
}

@property(nonatomic, strong) UIScrollView *myScrollView;

@property(nonatomic, strong) UIView *myView;

@property(nonatomic, strong) NSArray *requstGetExpressArray;

@property(nonatomic, strong) MBZPKTextField *textField_express_company;

@property(nonatomic, strong) MBZPKTextField *textField_express_no;

@property(nonatomic, strong) MBZPKTextField *textField_item_url;

@property(nonatomic, strong) MBZPKTextField *textField_item_name;

@property(nonatomic, strong) MBZPKTextField *textView_item_description;

@property(nonatomic, strong) MBZPKTextField *textField_item_color;

@property(nonatomic, strong) MBZPKTextField *textField_item_size;

@property(nonatomic, strong) MBZPKTextField *textField_item_capacity;

//@property(nonatomic, strong) MBZPKTextField *textField_item_version;

//@property(nonatomic, strong) MBZPKTextField *textField_item_other;

@property (nonatomic, strong) SQdaishouForecastModel* requstModel;

@property (nonatomic, strong) UIButton* imageButton1;
@property (nonatomic, strong) UIButton* imageButton2;
@property (nonatomic, strong) UIButton* imageButton3;

@property (nonatomic, strong) UIImageView *companyRightImageView;

@property (nonatomic, strong) UIButton* yubaoSureButton;

@end

@implementation SQdaishouForecastVC

#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"转运预报";
    _requstModel = [[SQdaishouForecastModel alloc] init];
    _requstModel.user_token = [SQUser loadmodel].user_token;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationGetexpress:) name:@"PxlGetexpress" object:nil];
    _requstGetExpressArray = [[NSArray alloc] init];
    [self buildUI];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - ---- event response 🍐🌛

-(void)notificationGetexpress:(NSNotification*)noti{
    NSDictionary *dic = [noti object];
    
    _requstModel.express_company_id = dic[@"id"];
    _textField_express_company.text = dic[@"name"];
    
    self.companyRightImageView.image = [UIImage imageNamed:@"sq_zhuanyun_mine_yubao_company_yes"];
}

-(void)sureButtonAction:(UIButton*)sender{
    if ([self sureButtonOpinion]) {
        [self requst];
    }
}

/** 选择 */
-(void)buttonAction{
    [self.view endEditing:YES];
    if (_requstGetExpressArray.count < 1) {
        [self requstGetExpress];
    }else{
        SearchViewCtrl *searchViewCtrl = [[SearchViewCtrl alloc] init];
        searchViewCtrl.dataArray = _requstGetExpressArray;
        [self.navigationController pushViewController:searchViewCtrl animated:YES];
    }
    
}

/**
 图片选择
 
 @param sender button
 */
-(void)imageButtonAction:(UIButton*)sender{
    @weakify(self);
    
    [[CameraTakeManager sharedInstance] cameraSheetInController:self handler:^(UIImage *image, NSString *imagePath) {
        @strongify(self);
        
        [self requstHeadImage:image button:sender];
        
    } cancelHandler:^{}];
}
#pragma mark - ---- private methods 🍊🌜
-(BOOL)sureButtonOpinion{
    
    if ([_textField_express_company.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请选择快递公司");
        return NO;
    }
    else if ([_textField_express_no.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入快递单号");
        return NO;
    }
    
    else if ([_textField_item_name.text isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请输入商品名称");
        return NO;
    }
    
    else if ([_requstModel.image_one isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请选择商品图片");
        return NO;
    }
    
    else if ([_requstModel.image_two isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请选择商品图片");
        return NO;
    }
    else if ([_requstModel.image_three isEqualToString:@""]) {
        showDefaultPromptTextHUD(@"请选择商品图片");
        return NO;
    }
    
    _requstModel.express_no = _textField_express_no.text;
    _requstModel.item_url = _textField_item_url.text;
    _requstModel.item_name = _textField_item_name.text;
    _requstModel.item_color = _textField_item_color.text;
    _requstModel.item_size = _textField_item_size.text;
    _requstModel.item_capacity = _textField_item_capacity.text;
    //    _requstModel.item_version = _textField_item_version.text;
    //    _requstModel.item_other = _textField_item_other.text;
    _requstModel.item_description = _textView_item_description.text;
    return YES;
}


-(void)showDataPicker{
    //选择车辆
    if (!_dataPicker) {
        _dataPicker = [[TimeAndDataPicker alloc]  initWithFrame:CGRectMake(0, KScreenHeight, KScreenWidth,KScreenHeight) andType:TimePickerDataSelectMode];
        [self.view.window addSubview:_dataPicker];
    }
    [_dataPicker timePickerAnimations];
}

-(void)buildUI{
    
    _myScrollView = [[UIScrollView alloc] init];
    [self.view addSubview:_myScrollView];
    [_myScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    const NSInteger num = 37;
    
    _myView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 550+num)];
    [_myScrollView addSubview:_myView];
    
    
    SQSuqibuyStorageAddriessView *topView = [[SQSuqibuyStorageAddriessView alloc] initWithNibName:@"SQSuqibuyStorageAddriessView"];
    [_myView addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.mas_equalTo(26.5);
    }];
    
    _textField_express_company = [self creatCellUI:@"国内快递" andTopNumber:20 +num];
    
    _textField_express_company.userInteractionEnabled = NO;

    _textField_express_no = [self creatCellInTwoUI:@"快递单号" andTopNumber:70 +num];
    _textField_express_no.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    _textField_item_name = [self creatCellInTwoUI:@"商品名称" andTopNumber:120+num];
    
    
    _textField_item_url = [self creatCellInTwoUI:@"商品链接" andTopNumber:170+num];
    _textField_item_url.keyboardType = UIKeyboardTypeURL;
    
    [self creatCellInImageViewTopNumber:238+num];
    
    
    //--- 商品图片
    
    [self creatCellInThreeUI:@"商品属性" andTopNumber:338+num];
    
    _textView_item_description = [self creatCellInTwoUI:@"验货备注" andTopNumber:388 + num];
    
    
    UIView *bottomView = [[UIView alloc] init];
    [_myView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(_myView).offset(5);
        make.height.equalTo(@136);
        make.top.equalTo(@(441 + num));
    }];
    
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    [bottomView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bottomView);
        make.height.equalTo(@1);
        make.top.equalTo(bottomView);
    }];
    
    
    
    _yubaoSureButton = [[UIButton alloc] init];
    [_yubaoSureButton setTitle:@"确认保存" forState:UIControlStateNormal];
    [_yubaoSureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_yubaoSureButton setBackgroundImage:[UIImage imageNamed:@"sq_zhuanyun_mine_yubao_sureButton"] forState:UIControlStateNormal];
    [_yubaoSureButton addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:_yubaoSureButton];
    [_yubaoSureButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView.mas_right).offset(-15);
        make.left.equalTo(@15);
        make.height.equalTo(@40);
        make.top.equalTo(bottomView).offset(25);
    }];
    //    如果无需验货，无需填写。分配人名张三，李四等每个客户不一样，不填写也可以入库
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:@"#333333"];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"特别提醒：以上信息没有可以不填写；如若没有填写任何验货信息，对于包裹里面的物品情况仓库默认为不需要验货；对于商品质量问题我们不"];
    
    [attributedString addAttributes:@{
                                      NSFontAttributeName: [UIFont systemFontOfSize:13.0f],
                                      NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#F10808"]
                                      } range:NSMakeRange(0, 5)];
    label1.attributedText = attributedString;
    label1.numberOfLines = 0;
    label1.textAlignment = NSTextAlignmentLeft;
    [label1 setFont:[UIFont systemFontOfSize:12]];
    [bottomView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_yubaoSureButton.mas_bottom).offset(9);
        make.left.right.equalTo(_yubaoSureButton);
    }];
    
    
    [_myView layoutIfNeeded];
    
    _myScrollView.contentSize = _myView.bounds.size;
    
}

-(__kindof MBZPKTextField*)creatCellUI:(NSString*)Str andTopNumber:(NSInteger)num{
    
    MBZPKTextField *textfield = [self createMBZPKTextField];
    
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView).offset(-15);
        make.left.equalTo(_myView).offset(85);
        make.top.mas_equalTo(num);
        make.height.equalTo(@30);
    }];
    
    UILabel *label1 = [self createLabel];
    label1.text = Str;
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-5);
        make.centerY.equalTo(textfield);
    }];
    
    UIButton *button = [[UIButton alloc] init];
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    button.tag = num;
    [_myView addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.equalTo(textfield);
    }];
    
    self.companyRightImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sq_zhuanyun_mine_yubao_company_no"]];
    [textfield addSubview:self.companyRightImageView];
    [self.companyRightImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(13, 8));
        make.right.equalTo(textfield.mas_right).offset(-8);
        make.centerY.equalTo(textfield);
    }];
    
    return textfield;
    
}


-(__kindof MBZPKTextField*)creatCellInTwoUI:(NSString*)Str andTopNumber:(NSInteger)num{
    
    MBZPKTextField *textfield = [self createMBZPKTextField];
    
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView).offset(-15);
        make.left.equalTo(_myView).offset(87);
        make.top.mas_equalTo(num);
        make.height.equalTo(@30);
    }];
    
    UILabel *label1 = [self createLabel];
    label1.text = Str;
    
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-11);
        make.centerY.equalTo(textfield);
    }];
    
    return textfield;
    
}


-(void)creatCellInImageViewTopNumber:(NSInteger)num{
    
//    self.imageButton1
    
    self.imageButton1 = [self creatImageButton:1];
    self.imageButton2 = [self creatImageButton:2];
    self.imageButton3 = [self creatImageButton:3];
    self.imageButton1.hidden = NO;
    
    
    [self.imageButton1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@85);
        make.size.mas_equalTo(CGSizeMake(75, 75));
        make.top.mas_equalTo(num);
    }];
    
    [self.imageButton2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imageButton1.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(75, 75));
        make.top.mas_equalTo(num);
    }];
    [self.imageButton3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.imageButton2.mas_right).offset(5);
        make.size.mas_equalTo(CGSizeMake(75, 75));
        make.top.mas_equalTo(num);
    }];
    
    UILabel *label1 = [self createLabel];
    label1.text = @"商品图片";
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.imageButton1.mas_top).offset(-11);
        make.right.equalTo(self.imageButton1.mas_left).offset(-10);
    }];
    
}



-(__kindof MBZPKTextField*)creatCellInThreeUI:(NSString*)Str andTopNumber:(NSInteger)num{
    
    _textField_item_color = [self createMBZPKTextField];
    _textField_item_color.placeholder = @"颜色";
    
    _textField_item_size = [self createMBZPKTextField];
    _textField_item_size.placeholder = @"尺寸";
    
    _textField_item_capacity = [self createMBZPKTextField];
    _textField_item_capacity.placeholder = @"数量";
    
    [_textField_item_color mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_myView).offset(87);
        make.top.mas_equalTo(num);
        make.height.equalTo(@30);
        
    }];
    
    [_textField_item_size mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_textField_item_color.mas_right).offset(25);
        make.top.mas_equalTo(_textField_item_color.mas_top);
        make.width.equalTo(_textField_item_color);
        make.height.equalTo(@30);
    }];
    
    [_textField_item_capacity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView).offset(-14);
        make.width.equalTo(_textField_item_size);
        make.left.equalTo(_textField_item_size.mas_right).offset(25);
        make.top.mas_equalTo(_textField_item_size);
        make.height.equalTo(@30);
    }];
    
    UILabel *label1 = [self createLabel];
    label1.text = Str;
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_textField_item_color.mas_left).offset(-11);
        make.centerY.equalTo(_textField_item_color);
    }];
    
    
    UILabel *label11 = [self createLabel];
    label11.textColor = [UIColor colorWithHexString:@"#333333"];
    label11.text = @"×";
    [label11 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_textField_item_color.mas_right).offset(9);
        make.centerY.equalTo(_textField_item_color);
    }];
    
    UILabel *label12 = [self createLabel];
    label12.textColor = [UIColor colorWithHexString:@"#333333"];
    
    label12.text = @"×";
    [label12 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_textField_item_size.mas_right).offset(9);
        make.centerY.equalTo(_textField_item_color);
    }];
    
    
    return _textField_item_size;
    
}


-(__kindof MBZPKTextView*)creatCellINTextViewUI:(NSString*)Str andTopNumber:(NSInteger)num{
    
    MBZPKTextView *textfield = [[MBZPKTextView alloc] init];
    [textfield setFont:[UIFont systemFontOfSize:14]];
    textfield.textColor = [UIColor blackColor];
    [textfield setTextAlignment:NSTextAlignmentLeft];
    [_myView addSubview:textfield];
    [textfield mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_myView).offset(-5);
        make.left.equalTo(_myView).offset(85);
        make.top.mas_equalTo(num);
        make.height.equalTo(@60);
    }];
    
    UILabel *label1 = [self createLabel];
    label1.text = Str;
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(textfield.mas_left).offset(-5);
        make.top.equalTo(textfield).offset(5);
    }];
    return textfield;
    
}



-(__kindof MBZPKTextField*)createMBZPKTextField{
    MBZPKTextField *textfield1 = [[MBZPKTextField alloc] init];
    [textfield1 setFont:[UIFont systemFontOfSize:14]];
    textfield1.textColor = [UIColor blackColor];
    [textfield1 setTextAlignment:NSTextAlignmentLeft];
    [_myView addSubview:textfield1];
    
    return textfield1;
}

-(__kindof UILabel*)createLabel{
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:text_color_gray_black];
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [label1 sizeToFit];
    [_myView addSubview:label1];
    
    return label1;
}


-(__kindof UIButton*)creatImageButton:(NSInteger)num{
    UIButton *button = [[UIButton alloc] init];
    button.tag = num;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor colorWithHexString:text_gray_666666].CGColor;

    button.backgroundColor = [UIColor colorWithHexString:@"#F4FAFF"];
    button.hidden = YES;
    [button setImage:[UIImage imageNamed:@"sq_zhuanyun_mine_yubao_imageSelect"] forState:UIControlStateNormal];

    [button addTarget:self action:@selector(imageButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [_myView addSubview:button];
    return button;
    
}


-(void)clearTextfile{
    [_requstModel clearData];
    _textField_express_no.text = @"";
    _textField_item_url.text = @"";
    _textField_item_name.text = @"";
    _textView_item_description.text = @"";
    _textField_item_color.text = @"";
    _textField_item_size.text = @"";
    _textField_item_capacity.text = @"";
    //    _textField_item_version.text = @"";
    //    _textField_item_other.text = @"";
    
}
//提交地址：/daishou/save
//参数：
//参数1：user_token
//参数2：api_token
//参数：express_company_id :: 国内快递名称
//参数：express_no :: 快递单号
//参数：item_url :: 商品链接
//参数：item_name :: 商品名称
//参数：item_color :: 颜色
//参数：item_size :: 尺寸
//参数：item_capacity :: 数量
//参数：item_version :: 版本
//参数：item_other :: 其它
//参数：item_description :: 商品描述


/**
 *    快递公司:提示:请选择快递公司   快递单号 :提示:请输入快递单号   商品名称:提示:请输入商品名称
 */
-(void)requstHeadImage:(UIImage*)image button:(UIButton*) sender{
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    NSString *user_token = [SQUser loadmodel].user_token;
    
    NSDictionary *dic = @{@"user_token":user_token};
    [self showLoadingHUD];
    @weakify(self);
    [HTTPManager publishCarCirclWithUrl:API_Zhuanyun_Dishou_uploadImage andPostParameters:dic andImageDic:@{@"pics":imageData} andBlocks:^(NSDictionary *result) {
        @strongify(self);
        [self hideLoadingHUD];
        NSLog(@"%@", result);
        if (result) {
            [sender setBackgroundImage:image forState:UIControlStateNormal];
            
            if (sender.tag == 1){
                _requstModel.image_one = result[@"imagePath"];
                self.imageButton2.hidden = NO;
            } else if(sender.tag == 2) {
                _requstModel.image_two = result[@"imagePath"];
                self.imageButton3.hidden = NO;
            } else {
                _requstModel.image_three = result[@"imagePath"];
            }
        }
    }];
    
}


-(void)requst{
    [self showLoadingHUD];
    NSDictionary *dic = _requstModel.mj_keyValues;
    
    @weakify(self);
    
    
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAISHOU_SAVE WithParameter:dic WithFinishBlock:^(id returnValue) {
        @strongify(self);
        
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                [UIAlertView bk_showAlertViewWithTitle:@"创建成功!" message:@"您是否需要继续创建新的包裹?" cancelButtonTitle:@"取消" otherButtonTitles:@[@"确定"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    @strongify(self);
                    
                    if (buttonIndex == 1) {
                        [self clearTextfile];
                    }else{
                        SQMangerWaitPutInStorageController* VC = [[SQMangerWaitPutInStorageController alloc] init];
                        [self.navigationController pushViewController:VC animated:YES];
                    }
                    
                }];
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}



-(void)requstGetExpress{
    //    softhelp/getexpress
    [self showLoadingHUD];
    NSDictionary *dic = [[NSDictionary alloc] init];
    
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAISHOU_Getexpress WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                _requstGetExpressArray = dic[@"result"][@"items"];
                
                SearchViewCtrl *searchViewCtrl = [[SearchViewCtrl alloc] init];
                searchViewCtrl.dataArray = _requstGetExpressArray;
                [self.navigationController pushViewController:searchViewCtrl animated:YES];
                
                
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
    
}



#pragma mark - ---- delegate 🍎🌝

#pragma mark - ---- getters and setters 🍋🌞


@end

