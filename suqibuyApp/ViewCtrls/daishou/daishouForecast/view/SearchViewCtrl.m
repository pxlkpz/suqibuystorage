//
//  SearchViewCtrl.m
//  CarService
//
//  Created by Apple on 12/1/14.
//  Copyright (c) 2014 fenglun. All rights reserved.
//

#import "SearchViewCtrl.h"

@interface SearchViewCtrl ()

@end

@implementation SearchViewCtrl




- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self navTitle];
    
    
    self.tableView = [UITableView new];
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.tableView.tableFooterView = [[UIView alloc] init];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}





- (void)navTitle
{
    self.title = @"请选择快递公司";
    
}

- (void)navLeftBtnAction:(id)sener
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.row >_dataArray.count) {
        return nil;
    }
    cell.textLabel.text = [[_dataArray objectAtIndex:indexPath.row] objectForKey:@"name"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PxlGetexpress" object:_dataArray[indexPath.row] ];
    [self performSelector:@selector(navLeftBtnAction:) withObject:nil afterDelay:0.4f];
}

#pragma UISearchDisplayDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    cell.frame = CGRectMake(-320, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
    [UIView animateWithDuration:0.7 animations:^{
        cell.frame = CGRectMake(0, cell.frame.origin.y, cell.frame.size.width, cell.frame.size.height);
    } completion:^(BOOL finished) {
        ;
    }];
}
@end

