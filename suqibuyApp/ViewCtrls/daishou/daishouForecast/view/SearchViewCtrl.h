//
//  SearchViewCtrl.h
//  CarService
//
//  Created by Apple on 12/1/14.
//  Copyright (c) 2014 fenglun. All rights reserved.
//

#import "SQBaseViewController.h"
typedef enum : NSUInteger {
    TypeWithSearchComm,
    TypeWithSearchParkName
} TypeWithSearch;
@interface SearchViewCtrl : SQBaseViewController <UITableViewDataSource,UITableViewDelegate> {
    NSMutableArray *searchResults;
    
    
}
@property (strong, nonatomic) NSArray *dataArray;
@property (strong, nonatomic) UITableView *tableView;

@property (assign, nonatomic) TypeWithSearch tyepInter;
@end
