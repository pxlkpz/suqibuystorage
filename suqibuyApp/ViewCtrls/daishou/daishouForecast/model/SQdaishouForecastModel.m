//
//  SQdaishouForecastModel.m
//  suqibuyApp
//
//  Created by apple on 16/5/30.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQdaishouForecastModel.h"

@implementation SQdaishouForecastModel

///** 快递单号 */
//@property (nonatomic, copy) NSString* express_no;
///** 商品链接 */
//@property (nonatomic, copy) NSString* item_url;
///** 商品名称 */
//@property (nonatomic, copy) NSString* item_name;
///** 颜色 */
//@property (nonatomic, copy) NSString* item_color;
///** 尺寸 */
//@property (nonatomic, copy) NSString* item_size;
///** 数量 */
//@property (nonatomic, copy) NSString* item_capacity;
///** 版本 */
//@property (nonatomic, copy) NSString* item_version;
///** 其它 */
//@property (nonatomic, copy) NSString* item_other;
///** 商品描述 */
//@property (nonatomic, copy) NSString* item_description;

-(void)clearData{
    self.express_no = @"";
    self.item_url = @"";
    self.item_name = @"";
    self.item_color = @"";
    self.item_size = @"";
    self.item_capacity = @"";
    self.item_version = @"";
    self.item_other = @"";
    self.item_description = @"";

}


@end
