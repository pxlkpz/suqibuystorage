//
//  SQdaishouForecastModel.h
//  suqibuyApp
//
//  Created by apple on 16/5/30.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQdaishouForecastModel : NSObject

//参数1：user_token
//参数2：api_token
//参数：express_company_id :: 国内快递名称
//参数：express_no :: 快递单号
//参数：item_url :: 商品链接
//参数：item_name :: 商品名称
//参数：item_color :: 颜色
//参数：item_size :: 尺寸
//参数：item_capacity :: 数量
//参数：item_version :: 版本
//参数：item_other :: 其它
//参数：item_description :: 商品描述

@property (nonatomic, copy) NSString* user_token;
/** 国内快递名称 */
@property (nonatomic, copy) NSString* express_company_id;
/** 快递单号 */
@property (nonatomic, copy) NSString* express_no;
/** 商品链接 */
@property (nonatomic, copy) NSString* item_url;
/** 商品名称 */
@property (nonatomic, copy) NSString* item_name;
/** 颜色 */
@property (nonatomic, copy) NSString* item_color;
/** 尺寸 */
@property (nonatomic, copy) NSString* item_size;
/** 数量 */
@property (nonatomic, copy) NSString* item_capacity;
/** 版本 */
@property (nonatomic, copy) NSString* item_version;
/** 其它 */
@property (nonatomic, copy) NSString* item_other;
/** 商品描述 */
@property (nonatomic, copy) NSString* item_description;

/** 图片地址1 */
@property (nonatomic, copy) NSString* image_one;

/** 图片地址2 */
@property (nonatomic, copy) NSString* image_two;

/** 图片地址3 */
@property (nonatomic, copy) NSString* image_three;





-(void)clearData;
@end
