//
//  SQdaishouFlowVC.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQdaishouFlowVC.h"
#import "SQdaishouForecastVC.h"

@interface SQdaishouFlowVC ()

@end

@implementation SQdaishouFlowVC



#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"转运流程";

    
    //    [self buildUI];
    [self buildui];
}
#pragma mark - ---- event response 🍐🌛
-(void)buttonAction{
    SQdaishouForecastVC *controller = [[SQdaishouForecastVC alloc] init];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0], controller]];
}
#pragma mark - ---- private methods 🍊🌜

-(void)buildui{
    UIView* bgview = [UIView new];
    bgview.backgroundColor = KColorFromRGB(0xEFEFEF);//efefef
    
    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"zhuanyun_steps"];
    CGSize size1 = imageView.image.size;
    CGFloat r = (screen_width/size1.width);
     bgview.frame = CGRectMake(0, 0, KScreenWidth, r*size1.height +100);
    
    [bgview addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bgview);
        make.top.equalTo(bgview);
        
        make.size.mas_equalTo(CGSizeMake(screen_width, r*size1.height ));
    }];
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitleColor:[UIColor colorWithHexString:text_yellow] forState:UIControlStateNormal];
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor colorWithHexString:text_yellow].CGColor;
    [button setTitle:@"立刻转运" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize: 15.0];
    [button setBackgroundColor:[UIColor whiteColor]];
    
    button.layer.cornerRadius = allCornerRadius;
    
    [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [bgview addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imageView.mas_bottom).offset(20);
        make.right.equalTo(bgview.mas_right).offset(-20);
        make.left.equalTo(@20);
        make.height.equalTo(@40);
    }];
    
    UITableView *tabel1 = [[UITableView alloc] init];
    [self.view addSubview:tabel1];
    [tabel1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.view);
    }];
    
    [tabel1 setTableHeaderView:bgview];

    
}


/*
 <string name="daishou_flow_step_one">将包裹邮寄到suiqbuy仓库</string>
 <string name="daishou_flow_step_two">选择“代收包裹预报”填写包裹详情</string>
 <string name="daishou_flow_step_three">等待suqibuy收货，验货，入库</string>
 <string name="daishou_flow_step_four">包裹入库，选择”转运“填写收货地址，支付国际运费</string>
 <string name="start_create_daishou">马上开启您的代收之旅吧！</string>
 
 */




#pragma mark - ---- delegate 🍎🌝

#pragma mark - ---- getters and setters 🍋🌞




@end
