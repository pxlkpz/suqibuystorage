//
//  SQdaishouListModel.h
//  suqibuyApp
//
//  Created by apple on 16/5/30.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQdaishouListModel : NSObject
/*
 "company_name" = "\U7533\U901a\U5feb\U9012";
 "created_at" = "2016-05-30";
 "daishou_package_id" = 51;
 description = "Q32\t32[ \U989c\U8272 \Uff1a123; \U5c3a\U5bf8 \Uff1a123; \U6570\U91cf \Uff1a313; \U7248\U672c \Uff1a123; \U5176\U5b83 \Uff1aQ31 ]";
 id = 57;
 "is_need_check" = 1;
 "item_thumb" = "https://t1.shtag.com/images/default/package_icon.png";
 "package_express_no" = 1231111111111;
 status = waiting;
 "status_label" = "\U5f85\U6536\U8d27";
 weight = "\U5c1a\U672a\U5f55\U5165";

 

 */
/** 快递名 */
@property (nonatomic, copy) NSString* company_name;
/** 创建时间 */
@property (nonatomic, copy) NSString* created_at;
/** 包裹id */
@property (nonatomic, copy) NSString* daishou_package_id;
/** 类型 */
@property (nonatomic, copy) NSString* My_description;
/** Id */
@property (nonatomic, copy) NSString* My_id;
/** 是否需要核对 */
@property (nonatomic, copy) NSString* is_need_check;
/** 图片 */
@property (nonatomic, copy) NSString* item_thumb;
/** 快递号 */
@property (nonatomic, copy) NSString* package_express_no;
/** 状态 */
@property (nonatomic, copy) NSString* status;
/** 状态名 */
@property (nonatomic, copy) NSString* status_label;
/** 重量 */
@property (nonatomic, copy) NSString* weight;
@end
