//
//  SQdaishouListItemsModel.h
//  suqibuyApp
//
//  Created by apple on 16/5/30.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQdaishouListItemsModel : NSObject

    /*{
     "can_delete" = 1;
     "can_payment" = 0;
     "check_result" = "<null>";
     "company_name" = "\U987a\U4e30";
     "created_at" = "2016-11-07 10:13:27";
     "daishou_package_id" = 77;
     description = "<null>";
     "grand_total" = "&yen;0.00";
     "is_need_check" = 1;
    
     
     "need_incharge" = 0;
     "package_express_no" = asdfasdfaf;
     "paid_total" = "&yen;0.00";
     "pay_daifu_amount" = "&yen;0.00";
     status = daichuli;
     "status_label" = "\U7b49\U5f85\U4ed3\U5e93\U5165\U5e93";
     "sub_qty" = 2;
     "user_remain_cash" = "&yen;68,860.26";
     "waiting_pay_daifu" = 0;
     "waiting_payment_total" = "&yen;0.00";
     weight = "<null>";

*/

/**
 是否可以删除 true 在详细页面 显示删除按钮
 */
@property (nonatomic, copy) NSString* can_delete;
/**
 在支付页面 判断 是否需要显示 充值按钮
 */
@property (nonatomic, copy) NSString* can_payment;
/**
 核对结果
 */
//@property (nonatomic, copy) NSString* check_result;

/**
 总金额
 */
@property (nonatomic, copy) NSString* grand_total;
/**
 在支付页面 判断是否需要显示 充值按钮， 和 其他支付一样
 */
@property (nonatomic, copy) NSString* need_incharge;
/**
 已支付的金额
 */
@property (nonatomic, copy) NSString* paid_total;
/**
 在支付页面显示
 */
@property (nonatomic, copy) NSString* user_remain_cash;
/**
 需要支付的金额
 */
@property (nonatomic, copy) NSString* waiting_payment_total;
/** 快递公司名字 */
@property (nonatomic, copy) NSString* company_name;
/** id */
@property (nonatomic, copy) NSString* daishou_package_id;
/** 物品数组 */
@property (nonatomic, copy) NSArray* items;
/** 快递单号 */
@property (nonatomic, copy) NSString* package_express_no;

/**  */
@property (nonatomic, copy) NSString* pay_daifu_amount;
/** 快递状态 */
@property (nonatomic, copy) NSString* status;
/** 快递状态  中文显示 */
@property (nonatomic, copy) NSString* status_label;
/** 数量 */
@property (nonatomic, copy) NSString* sub_qty;

//@property (nonatomic, copy) NSString* data;
/** 等待支付——代付 */
@property (nonatomic, copy) NSString* waiting_pay_daifu;
/** 重量 */
@property (nonatomic, copy) NSString* weight;
/** 验货结果 */
@property (nonatomic, copy) NSString* check_result;
/** 创建时间 */
@property (nonatomic, copy) NSString* created_at;
/** 是否需要验货 */
@property (nonatomic, copy) NSString* is_need_check;

@property (nonatomic, copy) NSString* size_total;

@property (nonatomic, copy) NSString* descriptionDS;

@end
