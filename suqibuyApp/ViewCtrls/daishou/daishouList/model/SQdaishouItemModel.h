//
//  SQdaishouItemModel.h
//  suqibuyApp
//
//  Created by apple on 16/5/30.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQdaishouItemModel : NSObject

/*
 {
 "created_at" = "2016-09-23";
 description = "[  ]";
 id = 84;
 "is_need_check" = 1;
 "item_code" = "<null>";
 "item_name" = Jejdjdjdjd;
 "item_qty" = 1;
 "item_thumb" = "https://t1.shtag.com/images/default/package_icon.png";
 status = waiting;
 "status_label" = "\U5f85\U6536\U8d27";
 weight = "\U5c1a\U672a\U5f55\U5165";
 }
 */


/*
 {
 "created_at" = "2016-05-13";
 description = "\U5546\U54c1\U63cf\U8ff0\U989c\U8272 \Uff1a\U7ea2\U82721";
 id = 48;
 "is_need_check" = 1;
 "item_code" = "";
 "item_name" = "\U5546\U54c1\U540d\U79f0\U5546\U54c1\U540d\U79f0\U5546\U54c1\U540d\U79f01";
 "item_thumb" = "https://t1.shtag.com/images/default/package_icon.png";
 status = signed;
 "status_label" = "\U5df2\U6536\U8d27";
 weight = "11.00";
 }
 
 @property (nonatomic, copy) NSString* item_name;
 @property (nonatomic, copy) NSString* item_code;
 @property (nonatomic, copy) NSString* item_qty;

 */
/** 创建时间 */
@property (nonatomic, copy) NSString* created_at;
/** 物品详情 */
@property (nonatomic, copy) NSString* my_description;
/** id */
@property (nonatomic, copy) NSString* my_id;
/** 是否 */
@property (nonatomic, copy) NSString* is_need_check;
/**  */
@property (nonatomic, copy) NSString* item_code;
/** 物品名字 */
@property (nonatomic, copy) NSString* item_name;
/** 图片 */
@property (nonatomic, copy) NSString* item_thumb;

/** 状态 */
@property (nonatomic, copy) NSString* status;

/** 名字 */
@property (nonatomic, copy) NSString* status_label;
/** 重量 */
@property (nonatomic, copy) NSString* weight;

/** 数量 */
@property (nonatomic, copy) NSString* item_qty;



@end
