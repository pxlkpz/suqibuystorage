
//
//  SQdaishouListVC.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQdaishouListVC.h"

#import "SQdaishouListCell.h"
#import "SQdaishouListItemsModel.h"
#import "SQdaishouItemModel.h"
#import "SQdaishouListDetailsView.h"

@interface SQdaishouListVC ()<UITableViewDataSource,UITableViewDelegate>
{
    NSInteger currentPage;
    
    NSMutableArray *dataArray;
    
    UILabel *_headLabel;
    
    //    NSString *_last_daishou_package_id;
    
    UIView *_navigationBgview;
    
    NSString *_total_weight;
    
    NSInteger selectSections;
    
}
@property(nonatomic, strong) UITableView *listTableView;

@end


@implementation SQdaishouListVC



#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"我的转运";
    
    _total_weight = @"";
    
    [self buildclearView];
    
    
    
    [self buildTableView];
    
    
    dataArray = [[NSMutableArray alloc] init];
    
    currentPage = 1;
    [self requestData:1];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteCellNum) name:@"PXL_deleteCellNum" object:nil];
    
    
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self buildHeadView];
    
    if (![_total_weight isEqualToString:@""]) {
        _headLabel.text = [NSString stringWithFormat:@"未发货物品总重量 : %@",_total_weight];

    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [_navigationBgview removeFromSuperview];
    
}
#pragma mark - ---- event response 🍐🌛

-(void)deleteCellNum{
    [dataArray removeObjectAtIndex:selectSections];
    [self.listTableView deleteSections:[NSIndexSet indexSetWithIndex:selectSections] withRowAnimation:UITableViewRowAnimationRight];
    
}


#pragma mark -进入订单详情
-(void) orderInformation:(UITapGestureRecognizer *) tap
{
    
    selectSections = tap.view.tag;
    SQdaishouListItemsModel *model = dataArray[tap.view.tag];
//_
    
    SQdaishouListDetailsView *controller = [[SQdaishouListDetailsView alloc] init];
    controller.model = model;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - ---- private methods 🍊🌜

-(void)buildclearView{
    
    UILabel* noItemLabel = [[UILabel alloc] init];
    noItemLabel.text = @"当前列表为空\n马上开始创建吧！";
    noItemLabel.textColor = [UIColor colorWithHexString:text_gray];
    noItemLabel.textAlignment = NSTextAlignmentCenter;
    noItemLabel.font = [UIFont systemFontOfSize:14];
    noItemLabel.numberOfLines = 2;
    [self.view addSubview:noItemLabel];
    [noItemLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
    }];
    
}

-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.delegate = self;
    _listTableView.hidden = YES;
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 68.0f;

    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(@30);
    }];
    [_listTableView setTableFooterView:[ [UIView alloc]init]];
    
    @weakify(self);
    
    
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:1];
        currentPage = 1;
    }];
    
    _listTableView.mj_footer  = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData:currentPage+1];
    }];
    _listTableView.mj_footer.hidden = YES;
    
//    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
        
//    }
    
    
}


-(void)buildHeadView{
    _navigationBgview = [[UIView alloc]initWithFrame:CGRectMake(0, 44, KScreenWidth, 30)];
    _navigationBgview.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar addSubview:_navigationBgview];
    
    
    UIView* bgView = [[UIView alloc] init];
    [_navigationBgview addSubview:bgView];
    bgView.backgroundColor = KColorRGB(250, 60, 6);
    //    [UIColor colorWithHexString:mainBg];
    
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(_navigationBgview);
        make.height.equalTo(@30);
        //        make.top.equalTo(_navigationBgview).offset(48);
    }];
    _headLabel = [[UILabel alloc] init];
    _headLabel.text = @"未发货物品总重量:";
    _headLabel.textColor = [UIColor whiteColor];
    _headLabel.textAlignment = NSTextAlignmentRight;
    [_headLabel setFont:[UIFont systemFontOfSize:13]];
    [_headLabel sizeToFit];
    [bgView addSubview:_headLabel];
    [_headLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bgView);
        make.right.mas_equalTo(-5);
    }];
    
    
}

#pragma mark -请求列表数据
-(void) requestData:(NSInteger ) page
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSString *pageString = [NSString stringWithFormat:@"%ld",(long)page];
    NSDictionary *dic = @{@"user_token":user_token,@"page":pageString};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_CANGKU_WarehousePackageList WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    _headLabel.text = [NSString stringWithFormat:@"未发货物品总重量 : %@",result[@"total_weight"]];
                    _total_weight = result[@"total_weight"];
                    id items =[result objectForKey:@"items"];
                    if (items != nil && items != [NSNull null] && [items isKindOfClass:[NSArray class]])
                    {
                        if (page == 1)
                        {
                            [dataArray removeAllObjects];
                            if ([items count] != 0){
                                _listTableView.hidden = NO;
                            }
                            
                        }
                        else
                        {
                            if ([items count] == 0)
                            {
                                showDefaultPromptTextHUD(@"没有更多内容了");
                                [self.listTableView.mj_footer endRefreshingWithNoMoreData];
                                
                                return ;
                            }
                            else
                            {
                                currentPage++;
                            }
                        }
                        [dataArray addObjectsFromArray:[SQdaishouListItemsModel mj_objectArrayWithKeyValuesArray:items]];
                        _listTableView.mj_footer.hidden = NO;
                    }
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        
        [_listTableView reloadData];
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}

#pragma mark - ---- delegate 🍎🌝

#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark- tableviewdelegate
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [dataArray count];
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    SQdaishouListItemsModel *model = dataArray[section];
    return [model.items count];
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"SQdaishouListCell";
    
    SQdaishouListCell *cell = (SQdaishouListCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"SQdaishouListCell"
                                                      owner:nil
                                                    options:nil];
        for (id nib in nibs)
        {
            if ([nib isKindOfClass:[SQdaishouListCell class]])
            {
                cell = (SQdaishouListCell*)nib;
                break;
            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    SQdaishouListItemsModel *model = dataArray[indexPath.section];
    SQdaishouItemModel *listModel = model.items[indexPath.row];
    
    [cell setData:listModel];
    
    return cell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SQdaishouListItemsModel *model = dataArray[section];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 45)];
    view.backgroundColor = [UIColor whiteColor];
    
    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 10)];
    view1.backgroundColor = [UIColor colorWithHexString:bg_gray];
    [view addSubview:view1];
    
    
    view.tag = section;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(orderInformation:)];
    
    [view addGestureRecognizer:tap];

    
    UILabel *label= [[UILabel alloc] init];
    label.text = [NSString stringWithFormat:@" 来自: %@  %@ ",model.company_name,model.package_express_no];
    [label setFont:[UIFont boldSystemFontOfSize:14]];
//    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor colorWithHexString:text_color_gray_black];
//    label.backgroundColor = [UIColor colorWithHexString:order_back_color];
    label.backgroundColor = [UIColor whiteColor];
    [view addSubview:label];
    [label sizeToFit];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(@15);
        make.right.mas_equalTo(0);
        make.height.equalTo(@35);
    }];
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 1;
    view2.backgroundColor = [UIColor colorWithRed:0.78 green:0.78 blue:0.8 alpha:1];

//    [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view);
        make.height.mas_equalTo(0.33);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    

    
    return view;
}


-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];
    view.tag = section;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(orderInformation:)];
    [view addGestureRecognizer:tap];
    
    
    UILabel *statusLabel= [[UILabel alloc] init];
     statusLabel.text = @"查看详情";
    statusLabel.textAlignment = NSTextAlignmentRight;
    statusLabel.font = [UIFont systemFontOfSize:14];
    [statusLabel sizeToFit];
    statusLabel.textColor = [UIColor colorWithHexString:gray];
    [view addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-8);
        make.bottom.equalTo(view);
        make.top.equalTo(view);
    }];
    
    
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 1;
    view2.backgroundColor = [UIColor colorWithRed:0.78 green:0.78 blue:0.8 alpha:1];
//    [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view);
        make.height.mas_equalTo(0.33);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    return view;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 45;
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 35;
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    return 67;
    SQdaishouListItemsModel *model = dataArray[indexPath.section];
    SQdaishouItemModel *listModel = model.items[indexPath.row];

    if(listModel.item_name.length > 54){
        return UITableViewAutomaticDimension;
    }
    
    return 67.0f;

    
     
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
//    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 0)];
    }

    
    
//    }
}

#pragma mark - ---- getters and setters 🍋🌞


@end
