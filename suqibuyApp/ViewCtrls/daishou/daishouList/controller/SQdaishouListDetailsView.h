//
//  SQdaishouListDetailsView.h
//  suqibuyApp
//
//  Created by pxl on 2016/9/22.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQBaseViewController.h"
#import "SQdaishouListItemsModel.h"
@interface SQdaishouListDetailsView : SQBaseViewController


@property(nonatomic, strong) SQdaishouListItemsModel * model;
@property(nonatomic, strong) NSDictionary * dataDic;

@end
