//
//  SQdaishouListDetailsView.m
//  suqibuyApp
//
//  Created by pxl on 2016/9/22.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//  我的快递

#import "SQdaishouListDetailsView.h"


#import "SQZhuanyunQuoteVCCell.h"
#import "SQzhuanyunListOrderDetailCell.h"
#import "SQzhuanyunPayMoneyCell.h"
#import "SQzhuanyunPayPassword.h"
#import "SQzhuanyunPayJieGuo.h"

#import "SQdaishouListDetailCell.h"
#import "SQdaishouListCell.h"
#import "SQdaishouListDetailsBottomView.h"
#import "SQdaishouItemModel.h"
@interface SQdaishouListDetailsView ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQdaishouListDetailsView

#pragma mark - ---- lefe cycle 🍏
- (void)viewDidLoad {
    [super viewDidLoad];

    if (!self.model) {
        SQdaishouListItemsModel* listModel = [[SQdaishouListItemsModel alloc] init];
        listModel.package_express_no = self.dataDic[@"express_no"];
        listModel.daishou_package_id = self.dataDic[@"daishou_package_id"];
        self.model = listModel;
    }
    
    [self requestData];
    
    self.title = [NSString stringWithFormat:@"%@ 快递详情", self.model.package_express_no];
    
    [self buildTableView];
    
    //[self requestData];
    
}
#pragma mark - ---- event response 🍐

#pragma mark - ---- private methods 🍊

-(void) requestData
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token,@"package_id":self.model.daishou_package_id};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_CANGKU_WarehousePackagedetail WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    SQdaishouListItemsModel* model1 = [SQdaishouListItemsModel mj_objectWithKeyValues:result];
                    self.model = model1;
                    
                    if ([self.model.waiting_pay_daifu isEqualToString:@"1"]) {
                        [_listTableView setTableFooterView:[self buildBottomView]];
                    }else{
                        [_listTableView setTableFooterView:[UIView new]];
                        
                    }

                    
                    [self.listTableView reloadData];
                    
                    [self buildBottomTwoButton];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        
        [_listTableView reloadData];
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


-(void)buildBottomTwoButton{

    if (!([self.model.can_payment isEqualToString:@"0"])) {
         SQdaishouListDetailsBottomView *buttonView = [[SQdaishouListDetailsBottomView alloc] init];

        [self.view addSubview:buttonView];
        [buttonView setData:self.model];
        [buttonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            
            if(IsiPhoneX)
            {
                make.height.equalTo(@74);
            }
            else
            {
                make.height.equalTo(@50);
            }
            
            
        }];

        
        [_listTableView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.view);
            make.top.equalTo(@0);      
            if(IsiPhoneX)
            {
                make.bottom.equalTo(self.view).offset(74);
            }
            else
            {
                make.bottom.equalTo(self.view).offset(50);
            }
            
        }];

    }
    
    
    
    
}


-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    _listTableView.dataSource = self;
    _listTableView.tag = 5;
    _listTableView.delegate = self;
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 60.0f;
 
    
    UINib *nib10 = [UINib nibWithNibName:@"SQdaishouListDetailCell" bundle:nil];
    [_listTableView registerNib:nib10 forCellReuseIdentifier:@"SQdaishouListDetailCell"];
    
    
    UINib *nib20 = [UINib nibWithNibName:@"SQdaishouListCell" bundle:nil];
    [_listTableView registerNib:nib20 forCellReuseIdentifier:@"SQdaishouListCell"];

    
    [_listTableView registerClass:[SQZhuanyunQuoteVCCell class] forCellReuseIdentifier:@"SQZhuanyunQuoteVCCell"];
    
    
    if ([self.model.waiting_pay_daifu isEqualToString:@"1"]) {
        [_listTableView setTableFooterView:[self buildBottomView]];
    }else{
        [_listTableView setTableFooterView:[UIView new]];
    }

    
    [self.view addSubview:_listTableView];
    
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    @weakify(self);
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self requestData];
    }];
    
    
    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}


-(UIView*)buildBottomView{
    
    UIView *bgView = [UIView new];
    bgView.frame = CGRectMake(0, 0, KScreenWidth, 100);
    
    
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = KColorFromRGB(0xF7F7F7);
    bottomView.layer.borderColor = [UIColor colorWithHexString:border_color].CGColor;
    bottomView.layer.borderWidth = 1;
    bottomView.layer.cornerRadius = allCornerRadius;
    [bgView addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(8);
        make.right.equalTo(bgView).offset(-8);
        make.centerY.equalTo(bgView);

         make.height.equalTo(@90);
    }];
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"daishou_notice_icon"]];
    [bottomView addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(@7);
        make.centerY.equalTo(bottomView);
        make.size.mas_equalTo(CGSizeMake(25, 25));
    }];
    
    //    如果无需验货，无需填写。分配人名张三，李四等每个客户不一样，不填写也可以入库
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = [UIColor colorWithHexString:text_shallow_gray];
    label1.text = @"尊敬地用户，你好!\n本快递为到付快递，为了不耽误您的快递送达日期，我们已先期为您垫付了到付费用，请登录网站完成到付费用的 补款，即可进行转运!";
    label1.numberOfLines = 0;
    label1.textAlignment = NSTextAlignmentLeft;
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [bottomView addSubview:label1];
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(bottomView);
        make.left.equalTo(imageView.mas_right).offset(7);
        make.right.equalTo(bottomView).offset(-7);
    }];
    
    return bgView;

}

#pragma mark - ---- delegate 🍎
#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 2;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 9;
    }else if (section == 1){
        return self.model.items.count;
    }
    return 0;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        NSString *identf;
        identf = @"SQdaishouListDetailCell";
        SQdaishouListDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:identf];
        if (nil == cell) {
            cell = [[SQdaishouListDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identf];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        [cell configInOrderDetail:_model andIndexRow:indexPath.row];
        
        return cell;
    }else if (indexPath.section == 1){
        NSString *identf;
        identf = @"SQdaishouListCell";
        SQdaishouListCell *cell = [tableView dequeueReusableCellWithIdentifier:identf];
        if (nil == cell) {
            cell = [[SQdaishouListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identf];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        SQdaishouItemModel *listModel = self.model.items[indexPath.row];
        
        [cell setData:listModel];
        
        return cell;
        
        
    }
    return nil;
}




-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}



-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return UITableViewAutomaticDimension;
        
    }else if (indexPath.section == 1){
        SQdaishouItemModel *listModel = self.model.items[indexPath.row];
        
        if(listModel.item_name.length > 54){
            return UITableViewAutomaticDimension;
        }
        
        return 67.0f;
    }
    
    
    return UITableViewAutomaticDimension;
}

-(UIView *) tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectZero];
    view.backgroundColor = [UIColor whiteColor];
    view.tag = section;
    
    UILabel *statusLabel= [[UILabel alloc] init];
    statusLabel.text = [NSString stringWithFormat:@"共 %@ 样物品", self.model.sub_qty];
    statusLabel.textAlignment = NSTextAlignmentRight;
    statusLabel.font = [UIFont systemFontOfSize:13];
    statusLabel.textColor = KColorFromRGB(0x717171);
    [statusLabel sizeToFit];
//    statusLabel.textColor = [UIColor colorWithHexString:button_blue];
    [view addSubview:statusLabel];
    [statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(view).offset(-8);
        make.bottom.equalTo(view).offset(-5);
    }];
    
    
    
    UIView *view2 = [[UIView alloc]init];
    [view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(0);
        make.left.mas_equalTo(15);
    }];
    
    return view;
}


-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    return 30;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

#pragma mark - ---- getters and setters 🍋


@end
