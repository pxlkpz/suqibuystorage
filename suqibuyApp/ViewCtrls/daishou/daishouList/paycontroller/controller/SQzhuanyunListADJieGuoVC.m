
//
//  SQzhuanyunListADJieGuoVC.m
//  suqibuyApp
//
//  Created by pxl on 2016/11/8.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunListADJieGuoVC.h"
#import "PxlTextView.h"
//#import "SQzhuanyunListOrderDetails.h"
#import "SQdaishouListDetailsView.h"
@interface SQzhuanyunListADJieGuoVC ()

@end

@implementation SQzhuanyunListADJieGuoVC


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"%@到付款支付完成", _model.package_express_no];
    
    [self buildUI];
}


#pragma mark - ---- event response 🍐🌛
-(void)sureButtonAction:(UIButton*)sender{
    
    SQdaishouListDetailsView*vc = [[SQdaishouListDetailsView alloc] init];
    vc.model = _model;
//
    [self.navigationController setViewControllers:@[self.navigationController.viewControllers[0], self.navigationController.viewControllers[1], vc]];
}
#pragma mark - ---- private methods 🍊🌜
-(void)buildUI{
    const NSInteger num = 0;
    UILabel * label =[self createCell:20 + num andStr1:@"快递公司:" andStr2:_model.company_name];
    UIView *view2 = [[UIView alloc]init];
    [self.view addSubview:view2];
    view2.alpha = 0.6;
    view2.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label.mas_bottom).offset(7);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(-15);
        make.left.mas_equalTo(15);
    }];
    
    
    UILabel * label1 =[self createCell:50 + num andStr1:@"快递单号:" andStr2:_model.package_express_no];
    UIView *view3 = [[UIView alloc]init];
    [self.view addSubview:view3];
    view3.alpha = 0.6;
    view3.backgroundColor = [UIColor colorWithHexString:order_back_color];
    [view3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label1.mas_bottom).offset(7);
        make.height.mas_equalTo(0.7);
        make.right.mas_equalTo(-15);
        make.left.mas_equalTo(15);
    }];

    
    
    UILabel*label2 = [self createCell:90 + num andStr1:@"付款金额:" andStr2:[_model.paid_total gtm_stringByUnescapingFromHTML]];
    label2.textColor = [UIColor colorWithHexString:pink_color];
    
    UILabel*label3 =  [self createCell:115 + num andStr1:@"快递状态:" andStr2:_model.status_label];
    label3.textColor = [UIColor colorWithHexString:pink_color];
    
    
    
    
    UILabel *labelBottom= [PxlTextView createLabel];
    labelBottom.text = @"我们已收到的代付到付款,我们将继续后面的操作。";
    labelBottom.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [self.view addSubview:labelBottom];
    [labelBottom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(label3.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(13);
    }];
    
    UIButton* button = [PxlTextView createButton:@"查看详情以及更多操作"];
    [self.view addSubview:button];
    [button addTarget:self action:@selector(sureButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(labelBottom.mas_bottom).offset(35);
        make.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.equalTo(@40);
    }];
    
}

-(UILabel*)createCell:(NSInteger)num andStr1:(NSString*)str1 andStr2:(NSString*)str2{
    UILabel *label1= [PxlTextView createLabel];
    label1.textColor = [UIColor colorWithHexString:text_shallow_gray];
    [self.view addSubview:label1];
    label1.textAlignment = NSTextAlignmentLeft;
    
    label1.text = str1;
    [label1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(15);
        make.top.equalTo(self.view).offset(num);
        make.size.mas_equalTo(CGSizeMake(70, 21));
    }];
    
    UILabel *label2= [PxlTextView createLabel];
    label2.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:label2];
    label2.textColor = [UIColor colorWithHexString:text_shallow_gray];
    label2.text = str2;
    [label2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(label1.mas_right).offset(5);
        make.centerY.equalTo(label1);
        make.right.equalTo(self.view).offset(10);
        //        make.size.mas_equalTo(CGSizeMake(70, 21));
    }];
    
    
    
    
    return label2;
    
}
#pragma mark - ---- delegate 🍎🌝

#pragma mark - ---- getters and setters 🍋🌞


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
