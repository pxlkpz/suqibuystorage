//
//  SQzhuanyunListADPayVC.m
//  suqibuyApp
//
//  Created by pxl on 2016/11/8.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQzhuanyunListADPayVC.h"
//#import "SQZhuanyunQuoteVCCell.h"
#import "SQzhuanyunListOrderDetailCell.h"
#import "SQzhuanyunPayMoneyCell.h"
#import "SQzhuanyunPayPassword.h"
#import "SQzhuanyunPayJieGuo.h"
#import "SQzhuanyunListADJieGuoVC.h"
@interface SQzhuanyunListADPayVC ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) UITableView *listTableView;

@end

@implementation SQzhuanyunListADPayVC


#pragma mark - ---- lefe cycle 🍏🌚
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"%@支付到付款",_model.package_express_no];

    [self buildTableView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationAction:) name:@"SQzhuanyunPayNoti" object:nil];
    
    
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - ---- event response 🍐🌛
-(void)notificationAction:(NSNotification*)noti{
    NSString* password = [noti object];
    if ([password isEqualToString:@""] || !password) {
        showDefaultPromptTextHUD(@"请输入您的帐号登录密码");
        return;
    }
//    /warehouse/payment
    [self requestDataPassword:password andorderNo:_model.daishou_package_id];
}
#pragma mark - ---- private methods 🍊🌜
-(void) requestDataPassword:(NSString*)confirm_password andorderNo:(NSString*)order_no
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token, @"confirm_password":confirm_password, @"package_id":order_no};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_DAISHOU_warehouse_payment WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    SQdaishouListItemsModel *model = [SQdaishouListItemsModel mj_objectWithKeyValues:result];
                    SQzhuanyunListADJieGuoVC *vc = [[SQzhuanyunListADJieGuoVC alloc] init];
                    vc.model = model;
                    [self.navigationController pushViewController:vc animated:YES];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


-(void)buildTableView{
    _listTableView = [[UITableView alloc] init];
    //[_listTableView setSeparatorInset:UIEdgeInsetsMake(0, 15, 0, 15)];
    _listTableView.dataSource = self;
    _listTableView.tag = 5;
    _listTableView.delegate = self;
    _listTableView.rowHeight = UITableViewAutomaticDimension;
    _listTableView.estimatedRowHeight = 44.0f;
    
    UINib *nib = [UINib nibWithNibName:@"SQzhuanyunPayMoneyCell" bundle:nil];
    [_listTableView registerNib:nib forCellReuseIdentifier:@"SQzhuanyunPayMoneyCell"];
    
    UINib *nib2 = [UINib nibWithNibName:@"SQzhuanyunListOrderDetailCell" bundle:nil];
    [_listTableView registerNib:nib2 forCellReuseIdentifier:@"SQzhuanyunListOrderDetailCell"];
    
    UINib *nib3 = [UINib nibWithNibName:@"SQzhuanyunPayPassword" bundle:nil];
    [_listTableView registerNib:nib3 forCellReuseIdentifier:@"SQzhuanyunPayPassword"];
    
    [_listTableView setTableFooterView:[[UIView alloc] init]];
    [self.view addSubview:_listTableView];
    [_listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(@0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    @weakify(self);
    _listTableView.mj_header  = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        
        [self requestData];
     }];

    if (iPhone6P_D) {
        if ([self.listTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.listTableView setSeparatorInset:UIEdgeInsetsZero];
            
        }
        if ([self.listTableView respondsToSelector:@selector(setLayoutMargins:)])  {
            [self.listTableView setLayoutMargins:UIEdgeInsetsZero];
        }
        
    }
    
}

-(void) requestData
{
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token,@"package_id":self.model.daishou_package_id};
    
    [self showLoadingHUD];
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_CANGKU_WarehousePackagedetail WithParameter:dic WithFinishBlock:^(id returnValue) {
        [self hideLoadingHUD];
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    SQdaishouListItemsModel* model1 = [SQdaishouListItemsModel mj_objectWithKeyValues:result];
                    self.model = model1;
                    
//                    if ([self.model.waiting_pay_daifu isEqualToString:@"1"]) {
////                        [_listTableView setTableFooterView:[self buildBottomView]];
//                    }else{
//                        [_listTableView setTableFooterView:[UIView new]];
//                        
//                    }
                    
                    
                    [self.listTableView reloadData];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        
        [_listTableView reloadData];
    } WithErrorBlock:^(id errorCode) {
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
        [self hideLoadingHUD];
        [_listTableView.mj_header endRefreshing];
        [_listTableView.mj_footer endRefreshing];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}


#pragma mark - ---- delegate 🍎🌝
#pragma mark - 去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat sectionHeaderHeight = 45;
    
    if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    }
    else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 7;
    }
    
    return 0;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0){
        if (indexPath.row<5) {
            NSString *identifier = @"SQzhuanyunListOrderDetailCell";
            SQzhuanyunListOrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            [cell setdataInzhuanyunADPay:_model andindexRow:indexPath.row];
            return cell;
        }else if (indexPath.row == 5){
            NSString *identifier = @"SQzhuanyunPayMoneyCell";
            SQzhuanyunPayMoneyCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            //            [cell setdataInTwo:_model andindexRow:indexPath.row];
            [cell setdata:_model andindexRow:indexPath.row];
            return cell;
            
        }else if (indexPath.row == 6){
            NSString *identifier = @"SQzhuanyunPayPassword";
            SQzhuanyunPayPassword *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            //            [cell setdataInTwo:_model andindexRow:indexPath.row];
            [cell setdata:_model andindexRow:indexPath.row];
            return cell;
            
        }
    }
    return nil;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screen_width, 20)];
    view.backgroundColor = [UIColor colorWithHexString:bg_gray];
    return view;
    
    
    return view;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 2) {
        return 45;
    }
    return 15;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row < 5){
        
        return UITableViewAutomaticDimension;
    }else if (indexPath.row == 6){
        return 160;
    }
    
    return UITableViewAutomaticDimension;
    
    
    return 44;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPat{
    if (iPhone6P_D) {
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsMake(0, 15, 0, 0)];
        }
    }
}

#pragma mark - ---- getters and setters 🍋🌞



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


@end
