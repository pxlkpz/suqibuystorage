//
//  SQdaishouListCell.h
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SQdaishouItemModel;
@interface SQdaishouListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *stausLabel;
@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *qtyLabel;
-(void)setData:(SQdaishouItemModel*)model;
@end
