//
//  SQdaishouListDetailsBottomView.h
//  suqibuyApp
//
//  Created by pxl on 2016/11/7.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQdaishouListDetailsBottomView : UIView



@property (strong, nonatomic) IBOutlet UIButton *bottomButton_OneDeleteLeft;
@property (strong, nonatomic) IBOutlet UIButton *bottomButton_OnePayRight;

@property (strong, nonatomic) IBOutlet UIButton *bottomButton_TwoDeleteCenter;



- (IBAction)buttonAction:(UIButton*)sender;




- (id)initWithNibName:(NSString *)nibNameOrNil;

-(void)setData:(id)data;
@end
