//
//  SQdaishouListDetailCell.m
//  suqibuyApp
//
//  Created by pxl on 2016/9/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQdaishouListDetailCell.h"
#import "SQdaishouListItemsModel.h"

@implementation SQdaishouListDetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
    
    CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width - 120; // 44 = avatar宽度，4 * 3为padding
    
    self.rightLabel.preferredMaxLayoutWidth = preferredMaxWidth; // 多行时必须设置
    
    [self.rightLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];

    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configInOrderDetail:(SQdaishouListItemsModel*)model andIndexRow:(NSInteger)row{
    self.leftLabel.text = @"";
    self.rightLabel.text = @"";

    switch (row) {
        case 0:
        {
            self.leftLabel.text = @"快递状态:";
            self.rightLabel.text = model.status_label;
        }
            break;
        case 1:
        {
            self.leftLabel.text = @"快递公司:";
            self.rightLabel.text = model.company_name;
        }
            break;
        case 2:
        {
            self.leftLabel.text = @"快递单号:";
            self.rightLabel.text = model.package_express_no;
        }
            break;
        case 3:
        {
            self.leftLabel.text = @"快递备注:";
            self.rightLabel.text = model.descriptionDS;
        }
            break;
        case 4:
        {
            self.leftLabel.text = @"代付到付费:";
            self.rightLabel.text = [model.pay_daifu_amount gtm_stringByUnescapingFromHTML];
            self.rightLabel.textColor = KColorFromRGB(0xf14b25);
        }
            break;
        case 5:
        {
            self.leftLabel.text = @"需要验货:";
            if ([model.is_need_check isEqualToString:@"1"]) {
                self.rightLabel.text = @"需要";
            }else{
                self.rightLabel.text = @"不需要";

            }
        }
            break;
        case 6:
        {
            self.leftLabel.text = @"验货结果:";
            self.rightLabel.text = model.check_result;
        }
            break;
        case 7:
        {
            self.leftLabel.text = @"创建时间:";
            self.rightLabel.text = model.created_at;
        }
            break;

        case 8:
        {
            self.leftLabel.text = @"物品清单";
            self.rightLabel.text = @"";
        }
            break;

            
        default:
            break;
    }
}


@end
