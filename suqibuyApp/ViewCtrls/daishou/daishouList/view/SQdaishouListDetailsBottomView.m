//
//  SQdaishouListDetailsBottomView.m
//  suqibuyApp
//
//  Created by pxl on 2016/11/7.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQdaishouListDetailsBottomView.h"
#import "SQdaishouListItemsModel.h"
//#import "KeyboardManager.h"
#import <IQKeyboardManager/IQUIView+Hierarchy.h>
#import "SQzhuanyunListADPayVC.h"
@implementation SQdaishouListDetailsBottomView

{
    SQdaishouListItemsModel *data1;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self buildUI];
    }
    return self;
}

-(void)buildUI{
    
    _bottomButton_TwoDeleteCenter = [[UIButton alloc] init];
    [_bottomButton_TwoDeleteCenter addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _bottomButton_TwoDeleteCenter.tag = 10;
    [_bottomButton_TwoDeleteCenter setTitle:@"删除记录" forState:UIControlStateNormal];
    [_bottomButton_TwoDeleteCenter.titleLabel setFont:[UIFont systemFontOfSize:18]];
    _bottomButton_TwoDeleteCenter.backgroundColor = KColorRGB(204, 204, 204);
    [self addSubview:_bottomButton_TwoDeleteCenter];
    _bottomButton_TwoDeleteCenter.hidden = YES;
    [_bottomButton_TwoDeleteCenter mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    
    _bottomButton_OneDeleteLeft = [UIButton new];
    [_bottomButton_OneDeleteLeft addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _bottomButton_OneDeleteLeft.tag = 10;

    [_bottomButton_OneDeleteLeft setTitle:@"删除记录" forState:UIControlStateNormal];
    [_bottomButton_OneDeleteLeft.titleLabel setFont:[UIFont systemFontOfSize:18]];
    _bottomButton_OneDeleteLeft.backgroundColor = KColorRGB(204, 204, 204);
    _bottomButton_OneDeleteLeft.hidden = YES;
    [self addSubview:_bottomButton_OneDeleteLeft];
    
    
    
    _bottomButton_OnePayRight = [UIButton new];
    [_bottomButton_OnePayRight addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    _bottomButton_OnePayRight.tag = 20;

    [_bottomButton_OnePayRight setTitle:@"支付到付款" forState:UIControlStateNormal];
    [_bottomButton_OnePayRight.titleLabel setFont:[UIFont systemFontOfSize:18]];
    _bottomButton_OnePayRight.backgroundColor = KColorRGB(239, 76, 48);
    _bottomButton_OnePayRight.hidden = YES;
    [self addSubview:_bottomButton_OnePayRight];
    
    [_bottomButton_OneDeleteLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self);
        
        if(IsiPhoneX)
        {
            make.bottom.equalTo(self).offset(-24);
        }
        else
        {
            make.bottom.equalTo(self);
        }
        
    }];

    
    [_bottomButton_OnePayRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.equalTo(self);
        make.left.equalTo(_bottomButton_OneDeleteLeft.mas_right);
        make.width.equalTo(_bottomButton_OneDeleteLeft.mas_width);
        
        if(IsiPhoneX)
        {
            make.bottom.equalTo(self).offset(-24);
        }
        else
        {
            make.bottom.equalTo(self);
        }
        
        
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil {
    
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:nibNameOrNil owner:nil options:nil];
    self = [nibs objectAtIndex:0];
//支付到付款 2. 239 76 48
    
    return self;
}
-(void)setData:(SQdaishouListItemsModel*)data{
    data1 = data;
    if ([data.can_payment isEqualToString:@"1"] && [data.can_delete isEqualToString:@"1"]) {
//        self.bottomButton_TwoDeleteCenter.hidden = YES;
        self.bottomButton_OnePayRight.hidden = NO;
        self.bottomButton_OneDeleteLeft.hidden = NO;
        
    }else if([data.can_payment isEqualToString:@"1"]){//支付
    
        [self.bottomButton_TwoDeleteCenter setTitle:@"支付到付款" forState:UIControlStateNormal];
        self.bottomButton_TwoDeleteCenter.hidden = NO;
        self.bottomButton_TwoDeleteCenter.backgroundColor = KColorRGB(239, 76, 48);
        self.bottomButton_TwoDeleteCenter.tag = 20;
    }else if([data.can_delete isEqualToString:@"1"]){//删除
        self.bottomButton_TwoDeleteCenter.hidden = NO;
        
    }
}

- (IBAction)buttonAction:(UIButton*)sender {
    if (sender.tag == 10) {//删除
//        self.viewController
        UIAlertController *alterC = [UIAlertController alertControllerWithTitle:nil message:@"您真的要删除该代收记录吗？" preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"否" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"是" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self requestDataDelete];
        } ];
        [alterC addAction:okAction];
        [alterC addAction:cancelAction];

        [self.viewController presentViewController:alterC animated:YES completion:nil];

    }else if (sender.tag == 20){//付款
        SQzhuanyunListADPayVC *vc = [[SQzhuanyunListADPayVC alloc] init];
        vc.model = data1;
        [self.viewController.navigationController pushViewController:vc animated:YES];
        
    }
}


#pragma mark -请求列表数据
-(void) requestDataDelete
{
    
    NSString *user_token = [SQUser loadmodel].user_token;
    NSDictionary *dic = @{@"user_token":user_token,@"package_id":data1.daishou_package_id};
    [MBBaseRequestClass MBRequestPOSTWithRequestURL:API_CANGKU_WarhouseDelete WithParameter:dic WithFinishBlock:^(id returnValue) {
        if (returnValue != nil && [returnValue isKindOfClass:[NSDictionary class]])
        {
            NSDictionary * dic = returnValue;
            id success = [dic objectForKey:@"success"];
            if (success != nil && [success isKindOfClass:[NSNumber class]] && [success integerValue] == 1)
            {
                id result = [dic objectForKey:@"result"];
                if (result != nil && [result isKindOfClass:[NSDictionary class]])
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PXL_deleteCellNum" object:nil];
                    [self.viewController.navigationController popViewControllerAnimated:YES];
                }
            }
            else
            {
                id error_msg = [dic objectForKey:@"error_msg"];
                if (error_msg != nil && [error_msg isKindOfClass:[NSString class]])
                {
                    showDefaultPromptTextHUD(error_msg);
                }
                else
                {
                    showDefaultPromptTextHUD(@"网络异常");
                }
            }
        }
        
    } WithErrorBlock:^(id errorCode) {
//        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    } WithFailedBlock:^{
//        [self hideLoadingHUD];
        showDefaultPromptTextHUD(@"网络异常");
    }];
}





@end
