//
//  SQdaishouListDetailCell.h
//  suqibuyApp
//
//  Created by pxl on 2016/9/23.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SQdaishouListDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;


-(void)configInOrderDetail:(id)model andIndexRow:(NSInteger)row;

@end
