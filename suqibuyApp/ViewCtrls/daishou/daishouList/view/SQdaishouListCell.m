//
//  SQdaishouListCell.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQdaishouListCell.h"
#import "SQdaishouItemModel.h"

@implementation SQdaishouListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    CGFloat preferredMaxWidth = [UIScreen mainScreen].bounds.size.width - 130; // 44 = avatar宽度，4 * 3为padding
    
    self.numberLabel.preferredMaxLayoutWidth = preferredMaxWidth; // 多行时必须设置
    [self.numberLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setData:(SQdaishouItemModel*)model{
    
    
    NSURL *url = [NSURL URLWithString:model.item_thumb];
    [self.headImageView sd_setImageWithURL:url placeholderImage:nil];
    
    self.stausLabel.text = model.status_label;
    self.numberLabel.text = model.item_name;
    if (model.item_qty) {
        self.qtyLabel.text = [NSString stringWithFormat:@"x%@",model.item_qty];
    }
    self.stausLabel.backgroundColor = [UIColor colorWithHexString:@"#69a0c5"];
    if ([model.status isEqualToString:@"signed"]) {
        self.stausLabel.backgroundColor = [UIColor colorWithHexString:@"#69a0c5"];
    }else if([model.status isEqualToString:@"waiting"]){
        self.stausLabel.backgroundColor = [UIColor colorWithHexString:@"#83d79b"];
        
    }else if ([model.status isEqualToString:@"sent"]){
        self.stausLabel.backgroundColor = [UIColor colorWithHexString:@"#e49a72"];
    }else if ([model.status isEqualToString:@"canceled"]){
        self.stausLabel.backgroundColor = [UIColor colorWithHexString:@"#d9dada"];
    }
}


@end
