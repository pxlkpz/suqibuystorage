//
//  SQDaishouCollectionCell.h
//  suqibuyApp
//
//  Created by pxl on 2017/4/13.
//  Copyright © 2017年 Peike Zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SQDaishouCellDataModel :NSObject

@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* buttonId;
@property (nonatomic, copy) NSString* imageName;

@end



@interface SQDaishouCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) SQDaishouCellDataModel* dataModel;

@end
