
//
//  SQDaishouCollectionViewCell.swift
//  suqibuyApp
//
//  Created by pxl on 2017/4/13.
//  Copyright © 2017年 Peike Zhang. All rights reserved.
//

import UIKit

class SQDaishouCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
