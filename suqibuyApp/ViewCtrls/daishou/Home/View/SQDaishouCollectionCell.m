//
//  SQDaishouCollectionCell.m
//  suqibuyApp
//
//  Created by pxl on 2017/4/13.
//  Copyright © 2017年 Peike Zhang. All rights reserved.
//

#import "SQDaishouCollectionCell.h"

@implementation SQDaishouCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.layer.borderWidth = 0.5;
    self.layer.borderColor = [UIColor colorWithHexString:seperator_color].CGColor;

}

/*
 @"title":@"转运流程演示", 
 @"buttonId":@"SQdaishouFlowVC",
 @"imageName":@"daishou_flow",
 */


-(void)setDataModel:(SQDaishouCellDataModel *)dataModel{
    self.titleLabel.text = dataModel.title;
    self.imageView.image = [UIImage imageNamed:dataModel.imageName];

    _dataModel = dataModel;
    

}






@end


@implementation SQDaishouCellDataModel

@end
