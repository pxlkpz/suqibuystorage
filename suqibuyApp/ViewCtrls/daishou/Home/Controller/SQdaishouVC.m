//
//  SQdaishouVC.m
//  suqibuyApp
//
//  Created by Peike Zhang on 16/5/15.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "SQdaishouVC.h"
#import "MBSingleWebViewController.h"
#import "PxlCommonViewCreate.h"
#import "suqibuyApp-Swift.h"
@interface SQdaishouVC ()<PxlCommonViewCreateDelegate>
@property (nonatomic, strong) PxlCommonViewCreate *pxlTool;
@end

@implementation SQdaishouVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *array = @[@{@"title":@"转运流程演示", @"buttonId":@"SQdaishouFlowVC",@"imageName":@"daishou_flow",},
                       @{@"title":@"suqibuy\n仓库地址", @"buttonId":@"SQSuqibuyStorageAddriessVC",@"imageName":@"daishou_suqiaddress",},
                       @{@"title":@"转运包裹预报", @"buttonId":@"SQdaishouForecastVC",@"imageName":@"daishou_yubao",},
                       @{@"title":@"转运包裹清单", @"buttonId":@"SQdaishouListVC",@"imageName":@"daishou_list",},
                       @{@"title":@"转运包裹限制", @"buttonId":@"SQdaishouLimitVC",@"imageName":@"daishou_forbit",},
                       @{@"title":@"费用估算", @"buttonId":@"SQzhuanyunFeeVC",@"imageName":@"zhuanyun_fee",}];
//[PxlCommonViewCreate pxl_getPlistData:@"SQdaishouUIData"]
    _pxlTool = [[PxlCommonViewCreate alloc] init];
    _pxlTool.delegate = self;
    [_pxlTool pxl_buildUI:array andSuperView:self.view];

    self.title = @"转运";
}


#pragma mark - ---- lefe cycle 🍏🌚

#pragma mark - ---- event response 🍐🌛



#pragma mark - ---- private methods 🍊🌜


#pragma mark - ---- delegate 🍎🌝

-(void)pxl_CommonViewCreateDelegate:(NSString *)pushString{
    if ([pushString isEqualToString:@"SQdaishouLimitVC"]) {
        ShipguideViewController *controller = [[ShipguideViewController alloc] init];

        controller.title = @"转运物品限制";
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }else{
        UIViewController *controller = [[NSClassFromString(pushString) alloc] init];
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
}

#pragma mark - ---- getters and setters 🍋🌞



@end
