//
//  CPMainHomeViewController.m
//  yunbo2017
//
//  Created by pxl on 2017/9/18.
//  Copyright © 2017年 pxl. All rights reserved.
//

#import "SQTransferMainHomeViewController.h"


#import "CPMainTitleView.h"
//#import "CPAPPInitConfig.h"
//#import "CPWashViewController.h"
//#import "CPParkNaviGuideViewController.h"

//#import "MMDrawerBarButtonItem.h"
//#import "CPMainViewModel.h"

//#import "CPParkNaviViewController.h"
//#import "CPRegisterViewController.h"
//
//#import "CPHomeNotiViewController.h"

//#import "CPBaseDataOnUserInfo.h"
//#import "UIBarButtonItem+Badge.h"
#import "suqibuyApp-Swift.h"

@interface SQTransferMainHomeViewController () <CPMainTitleViewDataSource, CPMainTitleViewDelelgate>
{
    UIViewController * currentViewController;
    
//    SQTransferMineController *_leftParkHomeViewController;
    
//    CPParkNaviViewController *_parkListVC;
//    CPParkNaviGuideViewController * _parkGuideViewController;
}

//@property (nonatomic,strong) SQTransferManagerController *washListVC;
//@property (strong, nonatomic) CPMainViewModel *viewModel;
@property (strong, nonatomic) CPMainTitleView *titleView;

@end

@implementation SQTransferMainHomeViewController


#pragma mark - ---- lefe cycle 🍏
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // http request  获取基本信息
//    [self.viewModel  requestAppGetInitConfig];
//    [self.viewModel  requestGetBaseInfo];
//    [self.viewModel  requestUserGetApprovalStatus];
    
    // Noti
//    [self buildNoti];
    [self buildUI];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    if (extern_is_showLeftController == YES) {
//        [self.viewDeckController openSide:IIViewDeckSideLeft animated:YES];
//    }

//    NSString *pointNumber = KgetUserValueByParaName(kNotiRedPointNumber);
//    if (NOEmptyStr(pointNumber) && [pointNumber integerValue] > 0) {
//        self.navigationItem.rightBarButtonItem.badgeValue = pointNumber;
//    }
}

#pragma mark - ---- event response 🍐

#pragma mark notification

//- (void)notiMessage
//{
//    NSString *pointNumber = KgetUserValueByParaName(kNotiRedPointNumber);
//    if (NOEmptyStr(pointNumber) && [pointNumber integerValue] > 0) {
//        self.navigationItem.rightBarButtonItem.badgeValue = pointNumber;
//    }
//}

//- (void)notiMainPushRegister:(NSNotification*)sender
//{
//    [self.navigationController pushViewController:[CPRegisterViewController new] animated:NO];
//}

//- (void)notiTransitionViewControllerForPark:(NSNotification*)sender
//{
//    if ([sender.object isKindOfClass:[CPParkNaviGuideData class]]) {
//        [self setCPParkNaviGuideViewController];
//        _parkGuideViewController.viewModel.dataModel = sender.object;
//
//        [_leftParkHomeViewController transitionFromViewController:_parkListVC toViewController:_parkGuideViewController duration:0 options:UIViewAnimationOptionTransitionNone animations:^{
//        }  completion:^(BOOL finished) {
//
//        }];
//    }else{
//
//        [_leftParkHomeViewController transitionFromViewController:_parkGuideViewController toViewController:_parkListVC duration:0 options:UIViewAnimationOptionTransitionNone animations:^{
//        }  completion:^(BOOL finished) {
//            if (finished) {
//
//                [_parkListVC resetUI];
//            }
//        }];
//    }
//}


///1/1/1
#pragma mark top switch button 切换
- (void)navi:(BOOL)isOpen
{
//    if ((currentViewController == _leftParkHomeViewController&&isOpen == YES )||(currentViewController == self.washListVC&&isOpen == NO)) {
//        return;
//    }
//    
//    UIViewController *oldViewController=currentViewController;
//    
//    if (isOpen == NO) {//洗车
//        [self transitionFromViewController:currentViewController toViewController:self.washListVC duration:0 options:UIViewAnimationOptionTransitionNone animations:^{
//        }  completion:^(BOOL finished) {
//            if (finished) {
//                currentViewController = self.washListVC;
//            }
//            else {
//                currentViewController = oldViewController;
//            }
//        }];
//        
//    }
//    else {
//        [self transitionFromViewController:currentViewController toViewController:_leftParkHomeViewController duration:0 options:UIViewAnimationOptionTransitionNone animations:^{
//        }  completion:^(BOOL finished) {
//            if (finished) {
//                currentViewController = _leftParkHomeViewController;
//            }
//            else {
//                currentViewController = oldViewController;
//            }
//        }];
//        
//    }
    
}

//- (void)leftDrawerButtonPress:(id)sender
//{
//    [self.viewDeckController openSide:IIViewDeckSideLeft animated:YES];
//}
//- (void)rightDrawerButtonPress:(id)sender
//{
//    if (IsEmptyStr(kUserID)) {
//        [self.navigationController pushViewController:[CPRegisterViewController new] animated:YES];
//        return;
//    }
//    self.navigationItem.rightBarButtonItem.badgeValue = @"";
//    KsetUserValueByParaName(@"0", kNotiRedPointNumber);
//    CPHomeNotiViewController *notiController = [CPHomeNotiViewController new];
//    notiController.style = CPHomeNotiViewControllerStyleNormal;
//    [self.navigationController pushViewController:notiController animated:YES];
//
//}
#pragma mark - ---- private methods 🍊

//- (void)buildNoti
//{
//    KAddobserverNotifiCation(@selector(notiTransitionViewControllerForPark:), kNotiParkTransitionViewController);
//    KAddobserverNotifiCation(@selector(notiMainPushRegister:), kNotiMainPushRegister);
//    KAddobserverNotifiCation(@selector(notiMessage), kNotiMessagePointRefresh);
//
//}

//- (void)viewModelBlocks
//{
//    @weakify(self);
//    self.viewModel.viewModelBlock = ^(NSUInteger Type, id returnValue) {
//        @strongify(self);
//
//        if (Type == CPMainViewModelGetBaseInfo) {
//
//            [self.titleView reloadData];
//            [self titleView:self.titleView didSelectTitleAtIndex:0];
//        }
//    };
//}

- (void)buildUI
{
//    MMDrawerBarButtonItem * leftDrawerButton = [[MMDrawerBarButtonItem alloc] initWithTarget:self action:@selector(leftDrawerButtonPress:)];
//    [leftDrawerButton setImage:[UIImage imageNamed:@"cp_home_LeftNavi"]];
//    [self.navigationItem setLeftBarButtonItem:leftDrawerButton animated:YES];
    
    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    [button setImage:[UIImage imageNamed:@"cp_home_RightNavi1"] forState:UIControlStateNormal];
//    button.frame = CGRectMake(0,100,button.currentImage.size.width, button.currentImage.size.height);
//    [button addTarget:self action:@selector(rightDrawerButtonPress:) forControlEvents:UIControlEventTouchDown];
    
    // 添加角标
//    UIBarButtonItem *navLeftButton = [[UIBarButtonItem alloc] initWithCustomView:button];
//    self.navigationItem.rightBarButtonItem = navLeftButton;
//    self.navigationItem.rightBarButtonItem.badgeBGColor = [UIColor redColor];
    
    //左controller
//    _leftParkHomeViewController = [[SQTransferMineController alloc] init];
//    _leftParkHomeViewController.view.frame = self.view.bounds;
//    [self addChildViewController:_leftParkHomeViewController];
//    [self.view addSubview:_leftParkHomeViewController.view];
//    currentViewController = _leftParkHomeViewController;
    
//    _parkListVC = [[CPParkNaviViewController alloc] init];
//    _parkListVC.view.frame = _leftParkHomeViewController.view.bounds;
//    [_leftParkHomeViewController addChildViewController:_parkListVC];
//    [_leftParkHomeViewController.view addSubview:_parkListVC.view];
    
    self.navigationItem.titleView = self.titleView;
}

#pragma mark - ---- delegate 🍎

#pragma mark - CPMainTitleViewDataSource

- (NSArray *)titlesForMainTitleView:(CPMainTitleView *)titleView
{
//    NSMutableArray *titles = [NSMutableArray array];
//    CPAPPInitConfig *model = [CPAPPInitConfig dataModel];
//    if (model.parking.show) {
//        [titles addObject:model.parking.text.tab];
//    }
//    if (model.cleaning.show) {
//        [titles addObject:model.cleaning.text.tab];
//    }
    return @[@"我的转运", @"发货管理"];
}


#pragma mark - CPMainTitleViewDelelgate

- (void)titleView:(CPMainTitleView *)titleView didSelectTitleAtIndex:(NSInteger)index
{
    NSArray *titles = [self titlesForMainTitleView:(CPMainTitleView *)self.titleView];
    NSString *title = titles[index];

    if ([title containsString:@"我的转运"]) {
        [self navi:YES];
    }
    if ([title containsString:@"发货管理"]) {
        [self navi:NO];
    }
}


#pragma mark - ---- getters and setters 🍋

//- (CPMainViewModel *)viewModel {
//    if (!_viewModel) {
//        _viewModel = [CPMainViewModel new];
//    }
//    return _viewModel;
//}

- (CPMainTitleView *)titleView {
    if (!_titleView) {
        _titleView = [[CPMainTitleView alloc] initWithFrame:CGRectZero];
        _titleView.dataSource = self;
        _titleView.delegate = self;
    }
    return _titleView;
}

//- (SQTransferManagerController *)washListVC
//{
//    if (!_washListVC) {
//        _washListVC = [[SQTransferManagerController alloc] init];
//        [self addChildViewController:_washListVC];
//        _washListVC.view.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight - KTopHeight);
//
//    }
//    return _washListVC;
//}


//- (CPParkNaviGuideViewController* )setCPParkNaviGuideViewController
//{
//
//    if (!_parkGuideViewController) {
//        _parkGuideViewController = [[CPParkNaviGuideViewController alloc] init];
//        [_leftParkHomeViewController addChildViewController:_parkGuideViewController];
//        _parkGuideViewController.view.frame = _leftParkHomeViewController.view.bounds;
//    }
//
//    return _parkGuideViewController;
//
//}

@end

