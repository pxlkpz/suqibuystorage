//
//  ViewFactoryModel.m
//  suqibuyApp
//
//  Created by apple on 16/5/27.
//  Copyright © 2016年 Peike Zhang. All rights reserved.
//

#import "ViewFactoryModel.h"
#import "MBZPKTextField.h"
@implementation ViewFactoryModel



+(__kindof MBZPKTextField*)buildMBZPKTextField{
    MBZPKTextField *textfield1 = [[MBZPKTextField alloc] init];
    [textfield1 setFont:[UIFont systemFontOfSize:14]];
    textfield1.textColor = [UIColor blackColor];
    [textfield1 setTextAlignment:NSTextAlignmentLeft];
    return textfield1;
}

+(__kindof UILabel*)buildGrayLabel{
    UILabel *label1 = [[UILabel alloc] init];
    label1.textColor = KColorFromRGB(0xcccccc);
    label1.textAlignment = NSTextAlignmentCenter;
    [label1 setFont:[UIFont systemFontOfSize:14]];
    [label1 sizeToFit];
    return label1;
}


@end
